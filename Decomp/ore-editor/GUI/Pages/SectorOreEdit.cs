﻿// Decompiled with JetBrains decompiler
// Type: OreEditor.GUI.Pages.SectorOreEdit
// Assembly: OreEditor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C0E9A1B-E579-45C0-B2C2-63BCDF738A55
// Assembly location: D:\Server\eab\Tools\OreEditor\OreEditor\OreEditor.exe

using DevExpress.Data;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using OreEditor.MySQL;
using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace OreEditor.GUI.Pages
{
  public class SectorOreEdit : XtraUserControl
  {
    private IContainer components;
    private GroupControl groupControl1;
    private GroupControl groupControl2;
    private SimpleButton bAddRow;
    private SimpleButton bDelRow;
    private SimpleButton bDelAll;
    private SimpleButton bAddAll;
    private GridControl gcCurrentOre;
    private GridView gvCurrentOre;
    private GridView gridView4;
    private GridControl gcAdd;
    private GridView gvAdd;
    private GridView gridView2;
    private GridColumn gridColumn1;
    private GridColumn gridColumn2;
    private GridColumn gridColumn3;
    private GridColumn gridColumn4;
    private GridColumn gridColumn5;
    private GridColumn gridColumn6;
    private GridColumn gridColumn7;
    private int m_SecotrID;
    private DataTable m_OreList;
    private DataTable m_CurrentOres;
    private DataTable m_AddOres;

    protected override void Dispose(bool disposing)
    {
      if (disposing && components != null)
        components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      groupControl1 = new GroupControl();
      gcCurrentOre = new GridControl();
      gvCurrentOre = new GridView();
      gridColumn1 = new GridColumn();
      gridColumn2 = new GridColumn();
      gridColumn3 = new GridColumn();
      gridColumn7 = new GridColumn();
      gridView4 = new GridView();
      groupControl2 = new GroupControl();
      gcAdd = new GridControl();
      gvAdd = new GridView();
      gridColumn4 = new GridColumn();
      gridColumn5 = new GridColumn();
      gridColumn6 = new GridColumn();
      gridView2 = new GridView();
      bAddRow = new SimpleButton();
      bDelRow = new SimpleButton();
      bDelAll = new SimpleButton();
      bAddAll = new SimpleButton();
      ((ISupportInitialize) groupControl1).BeginInit();
      ((Control) groupControl1).SuspendLayout();
      ((ISupportInitialize) gcCurrentOre).BeginInit();
      ((ISupportInitialize) gvCurrentOre).BeginInit();
      ((ISupportInitialize) gridView4).BeginInit();
      ((ISupportInitialize) groupControl2).BeginInit();
      ((Control) groupControl2).SuspendLayout();
      ((ISupportInitialize) gcAdd).BeginInit();
      ((ISupportInitialize) gvAdd).BeginInit();
      ((ISupportInitialize) gridView2).BeginInit();
      SuspendLayout();
      ((Control) groupControl1).Controls.Add((Control) gcCurrentOre);
      ((Control) groupControl1).Dock = DockStyle.Left;
      ((Control) groupControl1).Location = new Point(0, 0);
      ((Control) groupControl1).Name = "groupControl1";
      ((Control) groupControl1).Size = new Size(252, 369);
      ((Control) groupControl1).TabIndex = 0;
      ((Control) groupControl1).Text = "Current";
      ((Control) gcCurrentOre).Dock = DockStyle.Fill;
      ((Control) gcCurrentOre).Location = new Point(2, 22);
      gcCurrentOre.MainView = ((BaseView) gvCurrentOre);
      ((Control) gcCurrentOre).Name = "gcCurrentOre";
      ((Control) gcCurrentOre).Size = new Size(248, 345);
      ((Control) gcCurrentOre).TabIndex = 1;
      gcCurrentOre.ViewCollection.AddRange(new BaseView[2]
      {
        (BaseView) gvCurrentOre,
        (BaseView) gridView4
      });
      ((ColumnView) gvCurrentOre).Columns.AddRange(new GridColumn[4]
      {
        gridColumn1,
        gridColumn2,
        gridColumn3,
        gridColumn7
      });
      ((BaseView) gvCurrentOre).GridControl = (gcCurrentOre);
      ((ColumnView) gvCurrentOre).GroupCount = 1;
      ((BaseView) gvCurrentOre).Name = "gvCurrentOre";
      gvCurrentOre.OptionsView.ColumnAutoWidth = false;
      ((ColumnView) gvCurrentOre).SortInfo.AddRange(new GridColumnSortInfo[1]
      {
        new GridColumnSortInfo(gridColumn7, (ColumnSortOrder) 1)
      });
      gridColumn1.Caption = "ItemID";
      gridColumn1.FieldName = "item_id";
      gridColumn1.Name = "gridColumn1";
      gridColumn1.OptionsColumn.AllowEdit = false;
      gridColumn1.Visible = true;
      gridColumn1.VisibleIndex = 0;
      gridColumn1.Width = 61;
      gridColumn2.Caption = "Name";
      gridColumn2.FieldName = "name";
      gridColumn2.Name = "gridColumn2";
      gridColumn2.OptionsColumn.AllowEdit = false;
      gridColumn2.Visible = true;
      gridColumn2.VisibleIndex = 1;
      gridColumn2.Width = 117;
      gridColumn3.Caption = "Frequency";
      gridColumn3.FieldName = "frequency";
      gridColumn3.Name = "gridColumn3";
      gridColumn3.Visible = true;
      gridColumn3.VisibleIndex = 2;
      gridColumn3.Width = 49;
      gridColumn7.Caption = "Level";
      gridColumn7.FieldName = "level";
      gridColumn7.Name = "gridColumn7";
      gridColumn7.OptionsColumn.AllowEdit = false;
      gridColumn7.Visible = true;
      gridColumn7.VisibleIndex = 3;
      ((BaseView) gridView4).GridControl = gcCurrentOre;
      ((BaseView) gridView4).Name =  "gridView4";
      ((Control) groupControl2).Controls.Add((Control) gcAdd);
      ((Control) groupControl2).Dock = DockStyle.Right;
      ((Control) groupControl2).Location = new Point(309, 0);
      ((Control) groupControl2).Name = "groupControl2";
      ((Control) groupControl2).Size = new Size(252, 369);
      ((Control) groupControl2).TabIndex = 1;
      ((Control) groupControl2).Text = "Avalable";
      ((Control) gcAdd).Dock = DockStyle.Fill;
      ((Control) gcAdd).Location = new Point(2, 22);
      gcAdd.MainView = ((BaseView) gvAdd);
      ((Control) gcAdd).Name = "gcAdd";
      ((Control) gcAdd).Size = new Size(248, 345);
      ((Control) gcAdd).TabIndex = 0;
      gcAdd.ViewCollection.AddRange(new BaseView[2]
      {
        (BaseView) gvAdd,
        (BaseView) gridView2
      });
      ((ColumnView) gvAdd).Columns.AddRange(new GridColumn[3]
      {
        gridColumn4,
        gridColumn5,
        gridColumn6
      });
      ((BaseView) gvAdd).GridControl = gcAdd;
      ((ColumnView) gvAdd).GroupCount = 1;
      ((BaseView) gvAdd).Name = "gvAdd";
      gvAdd.OptionsView.ColumnAutoWidth = false;
      ((ColumnView) gvAdd).SortInfo.AddRange(new GridColumnSortInfo[1]
      {
        new GridColumnSortInfo(gridColumn6, (ColumnSortOrder) 1)
      });
      gridColumn4.Caption = "ItemID";
      gridColumn4.FieldName = "id";
      gridColumn4.Name = "gridColumn4";
      gridColumn4.OptionsColumn.AllowEdit = false;
      gridColumn4.Visible = true;
      gridColumn4.VisibleIndex = 0;
      gridColumn4.Width = 48;
      gridColumn5.Caption = "Name";
      gridColumn5.FieldName = "name";
      gridColumn5.Name = "gridColumn5";
      gridColumn5.OptionsColumn.AllowEdit = false;
      gridColumn5.Visible = true;
      gridColumn5.VisibleIndex = 1;
      gridColumn5.Width = 118;
      gridColumn6.Caption = "Level";
      gridColumn6.FieldName = "level";
      gridColumn6.Name = "gridColumn6";
      gridColumn6.OptionsColumn.AllowEdit = false;
      gridColumn6.Visible = true;
      gridColumn6.VisibleIndex = 2;
      gridColumn6.Width = 42;
      ((BaseView) gridView2).GridControl = (gcAdd);
      ((BaseView) gridView2).Name = "gridView2";
      ((Control) bAddRow).Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      ((Control) bAddRow).Location = new Point(258, 52);
      ((Control) bAddRow).Name = "bAddRow";
      ((Control) bAddRow).Size = new Size(45, 23);
      ((Control) bAddRow).TabIndex = 3;
      ((Control) bAddRow).Text = "<";
      ((Control) bAddRow).Click += new EventHandler(bAddRow_Click);
      ((Control) bDelRow).Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      ((Control) bDelRow).Location = new Point(258, 81);
      ((Control) bDelRow).Name = "bDelRow";
      ((Control) bDelRow).Size = new Size(45, 23);
      ((Control) bDelRow).TabIndex = 4;
      ((Control) bDelRow).Text = ">";
      ((Control) bDelRow).Click += new EventHandler(bDelRow_Click);
      ((Control) bDelAll).Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      ((Control) bDelAll).Location = new Point(258, 139);
      ((Control) bDelAll).Name = "bDelAll";
      ((Control) bDelAll).Size = new Size(45, 23);
      ((Control) bDelAll).TabIndex = 6;
      ((Control) bDelAll).Text = ">>";
      ((Control) bDelAll).Click += new EventHandler(bDelAll_Click);
      ((Control) bAddAll).Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      ((Control) bAddAll).Location = new Point(258, 110);
      ((Control) bAddAll).Name = "bAddAll";
      ((Control) bAddAll).Size = new Size(45, 23);
      ((Control) bAddAll).TabIndex = 5;
      ((Control) bAddAll).Text = "<<";
      ((Control) bAddAll).Click += new EventHandler(bAddAll_Click);
      AutoScaleDimensions = new SizeF(6f, 13f);
      AutoScaleMode = AutoScaleMode.Font;
      Controls.Add((Control) bDelAll);
      Controls.Add((Control) bAddAll);
      Controls.Add((Control) bDelRow);
      Controls.Add((Control) bAddRow);
      Controls.Add((Control) groupControl2);
      Controls.Add((Control) groupControl1);
      Name = nameof (SectorOreEdit);
      Size = new Size(561, 369);
      ((UserControl) this).Load += new EventHandler(SectorOreEdit_Load);
      ((ISupportInitialize) groupControl1).EndInit();
      ((Control) groupControl1).ResumeLayout(false);
      ((ISupportInitialize) gcCurrentOre).EndInit();
      ((ISupportInitialize) gvCurrentOre).EndInit();
      ((ISupportInitialize) gridView4).EndInit();
      ((ISupportInitialize) groupControl2).EndInit();
      ((Control) groupControl2).ResumeLayout(false);
      ((ISupportInitialize) gcAdd).EndInit();
      ((ISupportInitialize) gvAdd).EndInit();
      ((ISupportInitialize) gridView2).EndInit();
      ResumeLayout(false);
    }

    public SectorOreEdit()
    {
      InitializeComponent();
    }

    public void ChangeSector(int sectorID)
    {
      m_SecotrID = sectorID;
      string Query = "SELECT base_ore_list.item_id, base_ore_list.name, base_ore_list.sector_id, base_ore_list.frequency,1 as level  FROM base_ore_list WHERE sector_id = '" + sectorID.ToString() + "';";
      m_CurrentOres.Clear();
      m_CurrentOres.Merge(SQL.ExecuteQuery(Query, "net7"));
      gcCurrentOre.DataSource = ((object) m_CurrentOres);
      m_AddOres.Clear();
      m_AddOres.Merge(m_OreList);
      foreach (DataRow row in (InternalDataCollectionBase) m_CurrentOres.Rows)
      {
        foreach (DataRow dataRow in m_AddOres.Select("[id]='" + row["item_id"].ToString() + "'"))
        {
          row["level"] = dataRow["level"];
          dataRow.Delete();
        }
      }
      gcAdd.DataSource = ((object) m_AddOres);
    }

    private void SectorOreEdit_Load(object sender, EventArgs e)
    {
      m_OreList.Merge(SQL.ExecuteQuery("SELECT item_base.name,item_base.id,item_base.level FROM item_base WHERE item_base.category =  '81'", "net7"));
    }

    private void bAddRow_Click(object sender, EventArgs e)
    {
      GridView gridView = gcAdd.DefaultView as GridView;
      int focusedRowHandle = ((ColumnView) gridView).FocusedRowHandle;
      if (focusedRowHandle < 0)
        return;
      int num1 = int.Parse(((ColumnView) gridView).GetRowCellValue(focusedRowHandle, ((ColumnView) gvAdd).Columns.ColumnByName("id")).ToString());
      int num2 = int.Parse(((ColumnView) gridView).GetRowCellValue(focusedRowHandle, ((ColumnView) gvAdd).Columns.ColumnByName("level")).ToString());
      string str = ((ColumnView) gridView).GetRowCellValue(focusedRowHandle, ((ColumnView) gvAdd).Columns.ColumnByName("name")).ToString();
      DataRow row = m_CurrentOres.NewRow();
      row["sector_id"] = (object) m_SecotrID;
      row["item_id"] = (object) num1;
      row["name"] = (object) str;
      row["frequency"] = (object) 0;
      row["level"] = (object) num2;
      m_CurrentOres.Rows.Add(row);
      foreach (DataRow dataRow in m_AddOres.Select("[id]='" + num1.ToString() + "'"))
        dataRow.Delete();
    }

    private void bDelRow_Click(object sender, EventArgs e)
    {
      GridView gridView = gcCurrentOre.DefaultView as GridView;
      int focusedRowHandle = ((ColumnView) gridView).FocusedRowHandle;
      if (focusedRowHandle < 0)
        return;
      int num = int.Parse(((ColumnView) gridView).GetRowCellValue(focusedRowHandle, ((ColumnView) gvCurrentOre).Columns.ColumnByName("item_id")).ToString());
      DataRow[] dataRowArray = m_OreList.Select("[id]='" + num.ToString() + "'");
      if (dataRowArray.Length == 1)
      {
        DataRow dataRow1 = dataRowArray[0];
        DataRow row = m_AddOres.NewRow();
        row["name"] = dataRow1["name"];
        row["level"] = dataRow1["level"];
        row["id"] = dataRow1["id"];
        m_AddOres.Rows.Add(row);
        foreach (DataRow dataRow2 in m_CurrentOres.Select("[item_id]='" + num.ToString() + "'"))
          dataRow2.Delete();
      }
      else
      {
        foreach (DataRow dataRow in m_CurrentOres.Select("[item_id]='" + num.ToString() + "'"))
          dataRow.Delete();
      }
    }

    private void bAddAll_Click(object sender, EventArgs e)
    {
      foreach (DataRow row1 in (InternalDataCollectionBase) m_AddOres.Rows)
      {
        if (row1.RowState != DataRowState.Deleted)
        {
          DataRow row2 = m_CurrentOres.NewRow();
          row2["sector_id"] = (object) m_SecotrID;
          row2["item_id"] = row1["id"];
          row2["name"] = row1["name"];
          row2["frequency"] = (object) 0;
          row2["level"] = row1["level"];
          m_CurrentOres.Rows.Add(row2);
          row1.Delete();
        }
      }
    }

    private void bDelAll_Click(object sender, EventArgs e)
    {
      foreach (DataRow row1 in (InternalDataCollectionBase) m_CurrentOres.Rows)
      {
        DataRow[] dataRowArray = m_OreList.Select("[id]='" + int.Parse(row1["item_id"].ToString()).ToString() + "'");
        if (dataRowArray.Length == 1)
        {
          DataRow dataRow = dataRowArray[0];
          DataRow row2 = m_AddOres.NewRow();
          row2["name"] = dataRow["name"];
          row2["level"] = dataRow["level"];
          row2["id"] = dataRow["id"];
          m_AddOres.Rows.Add(row2);
          row1.Delete();
        }
        else
          row1.Delete();
      }
    }

    public void ReloadData()
    {
      if (m_SecotrID == -1)
        return;
      ChangeSector(m_SecotrID);
    }

    public void SaveData()
    {
      SQL.UpdateDT(m_CurrentOres, "SELECT base_ore_list.item_id, base_ore_list.name, base_ore_list.sector_id, base_ore_list.frequency FROM base_ore_list WHERE sector_id = '" + m_SecotrID.ToString() + "';");
      int num = (int) MessageBox.Show("Saved Data");
    }
  }
}
