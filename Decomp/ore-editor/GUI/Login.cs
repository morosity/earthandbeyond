﻿// Decompiled with JetBrains decompiler
// Type: OreEditor.GUI.Login
// Assembly: OreEditor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C0E9A1B-E579-45C0-B2C2-63BCDF738A55
// Assembly location: D:\Server\eab\Tools\OreEditor\OreEditor\OreEditor.exe

using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using OreEditor.MySQL;
using System;
using System.ComponentModel;
using System.Data.Common;
using System.Drawing;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using ToolAuthDLL;

namespace OreEditor.GUI
{
    public class Login : Form
    {
        private IContainer components;
        private Label label1;
        private Label label2;
        public TextBox LoginUsername;
        public TextBox LoginPassword;
        private Button ExitLogin;
        private Button LoginButton;
        private Label label3;
        public TextBox SQLServer;
        public TextBox SQLPort;
        private Label lable9;
        private CheckEdit cMySQL;
        public bool m_Cancel;
        public bool m_HasChanged;

        protected virtual void Dispose(bool disposing)
        {
            if (disposing && components != null)
                components.Dispose();
            base.Dispose(disposing);
        }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (Login));
      this.label1 = new Label();
      this.label2 = new Label();
      this.LoginUsername = new TextBox();
      this.LoginPassword = new TextBox();
      this.ExitLogin = new Button();
      this.LoginButton = new Button();
      this.label3 = new Label();
      this.SQLServer = new TextBox();
      this.SQLPort = new TextBox();
      this.lable9 = new Label();
      this.SuspendLayout();
      this.label1.AutoSize = true;
      this.label1.Location = new Point(37, 15);
      this.label1.Name = "label1";
      this.label1.Size = new Size(58, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Username:";
      this.label2.AutoSize = true;
      this.label2.Location = new Point(39, 41);
      this.label2.Name = "label2";
      this.label2.Size = new Size(56, 13);
      this.label2.TabIndex = 1;
      this.label2.Text = "Password:";
      this.LoginUsername.Location = new Point(101, 12);
      this.LoginUsername.Name = "LoginUsername";
      this.LoginUsername.Size = new Size(142, 20);
      this.LoginUsername.TabIndex = 3;
      this.LoginUsername.TextChanged += new EventHandler(this.LoginChange);
      this.LoginUsername.KeyPress += new KeyPressEventHandler(this.onEnterKey);
      this.LoginPassword.Location = new Point(101, 38);
      this.LoginPassword.Name = "LoginPassword";
      this.LoginPassword.PasswordChar = '*';
      this.LoginPassword.Size = new Size(142, 20);
      this.LoginPassword.TabIndex = 4;
      this.LoginPassword.TextChanged += new EventHandler(this.LoginChange);
      this.LoginPassword.KeyPress += new KeyPressEventHandler(this.onEnterKey);
      this.ExitLogin.Location = new Point(18, 123);
      this.ExitLogin.Name = "ExitLogin";
      this.ExitLogin.Size = new Size(77, 28);
      this.ExitLogin.TabIndex = 8;
      this.ExitLogin.Text = "Cancel";
      this.ExitLogin.UseVisualStyleBackColor = true;
      this.ExitLogin.Click += new EventHandler(this.ExitLogin_Click);
      this.LoginButton.Location = new Point(189, 123);
      this.LoginButton.Name = "LoginButton";
      this.LoginButton.Size = new Size(77, 28);
      this.LoginButton.TabIndex = 7;
      this.LoginButton.Text = nameof (Login);
      this.LoginButton.UseVisualStyleBackColor = true;
      this.LoginButton.Click += new EventHandler(this.LoginButton_Click);
      this.label3.AutoSize = true;
      this.label3.Location = new Point(35, 67);
      this.label3.Name = "label3";
      this.label3.Size = new Size(56, 13);
      this.label3.TabIndex = 2;
      this.label3.Text = "SQL Host:";
      this.SQLServer.Location = new Point(101, 64);
      this.SQLServer.Name = "SQLServer";
      this.SQLServer.Size = new Size(142, 20);
      this.SQLServer.TabIndex = 5;
      this.SQLServer.TextChanged += new EventHandler(this.LoginChange);
      this.SQLServer.KeyPress += new KeyPressEventHandler(this.onEnterKey);
      this.SQLPort.Location = new Point(101, 90);
      this.SQLPort.Name = "SQLPort";
      this.SQLPort.Size = new Size(61, 20);
      this.SQLPort.TabIndex = 6;
      this.SQLPort.TextChanged += new EventHandler(this.LoginChange);
      this.SQLPort.KeyPress += new KeyPressEventHandler(this.onEnterKey);
      this.lable9.AutoSize = true;
      this.lable9.Location = new Point(35, 93);
      this.lable9.Name = "lable9";
      this.lable9.Size = new Size(53, 13);
      this.lable9.TabIndex = 8;
      this.lable9.Text = "SQL Port:";
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(278, 158);
      this.Controls.Add((Control) this.SQLPort);
      this.Controls.Add((Control) this.lable9);
      this.Controls.Add((Control) this.LoginButton);
      this.Controls.Add((Control) this.ExitLogin);
      this.Controls.Add((Control) this.SQLServer);
      this.Controls.Add((Control) this.LoginPassword);
      this.Controls.Add((Control) this.LoginUsername);
      this.Controls.Add((Control) this.label3);
      this.Controls.Add((Control) this.label2);
      this.Controls.Add((Control) this.label1);
      this.FormBorderStyle = FormBorderStyle.FixedDialog;
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = nameof (Login);
      this.StartPosition = FormStartPosition.CenterScreen;
      this.Text = "N7 Sector Editor Login";
      this.Load += new EventHandler(this.Login_Load);
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public Login()
    {
      this.InitializeComponent();
    }

    private void LoginButton_Click(object sender, EventArgs e)
    {
      try
      {
        this.updateData();
        MySqlConnection mySqlConnection = new MySqlConnection(SQL.ConnStr("net7"));
        mySqlConnection.Open();
        mySqlConnection.Close();
        this.Close();
      }
      catch (MySqlException ex)
      {
        int num = (int) MessageBox.Show("N7 Login Encoutered a problem: \n\nBegin Message:\n" + ex.Message);
      }
    }

    private void ExitLogin_Click(object sender, EventArgs e)
    {
      this.m_Cancel = true;
      this.Close();
    }

    private void LoginChange(object sender, EventArgs e)
    {
      this.m_HasChanged = true;
    }

    private void onEnterKey(object sender, KeyPressEventArgs e)
    {
      if (!e.KeyChar.Equals('\r'))
        return;
      try
      {
        this.updateData();
        MySqlConnection mySqlConnection = new MySqlConnection(SQL.ConnStr("net7"));
        mySqlConnection.Open();
        mySqlConnection.Close();
        this.Close();
      }
      catch (MySqlException ex)
      {
        int num = (int) MessageBox.Show("N7 Login Encoutered a problem: \n\nBegin Message:\n" + ex.Message);
      }
    }

    private void Login_Load(object sender, EventArgs e)
    {
    }

        private void updateData()
        {
            SQL.User = LoginUsername.Text;
            SQL.Pass = LoginPassword.Text;
            SQL.Host = SQLServer.Text;
            SQL.Port = Convert.ToInt32(SQLPort.Text, 10);
            XmlTextWriter xmlTextWriter = new XmlTextWriter(Application.StartupPath + "\\Config.xml", (Encoding)null);
            xmlTextWriter.Formatting = Formatting.Indented;
            xmlTextWriter.WriteStartDocument();
            xmlTextWriter.WriteStartElement("Config");
            xmlTextWriter.WriteStartElement("Host");
            xmlTextWriter.WriteString(SQL.Host.ToString());
            xmlTextWriter.WriteEndElement();
            xmlTextWriter.WriteStartElement("Port");
            xmlTextWriter.WriteString(SQL.Port.ToString());
            xmlTextWriter.WriteEndElement();
            xmlTextWriter.WriteStartElement("User");
            xmlTextWriter.WriteString(LoginUsername.Text);
            xmlTextWriter.WriteEndElement();
            xmlTextWriter.WriteStartElement("Pass");
            xmlTextWriter.WriteString(LoginPassword.Text);
            xmlTextWriter.WriteEndElement();
            xmlTextWriter.WriteEndElement();
            xmlTextWriter.WriteEndDocument();
            xmlTextWriter.Close();
        }
    }
}
