
//I added row 3 and 4. Veg

//#ifndef _ABILITY_WORMHOLE_H_INCLUDED_                these 2 are original.
//#define _ABILITY_WORMHOLE_H_INCLUDED_


#ifndef _ABILITY_WORMHOLE_EXTENDED_H_INCLUDED_
#define _ABILITY_WORMHOLE_EXTENDED_H_INCLUDED_

#include "AbilityBase.h"

class AWormHoleExtended : public AbilityBase
{
public:
	AWormHoleExtended(CMob *me) : AbilityBase(me) {}
public:
	bool UseSkill(long ChargeTime);
	void Confirmation(bool Confirm, long AbilityID, long GameID);
	bool CanUse(long TargetID, long AbilityID, long SkillID);
	bool Update(long activation_ID);

	// Skill level calculators
private:
	int   GetSectorID( long SkillRank );
	float CalculateEnergy ( float SkillLevel, long SkillRank );
	float CalculateChargeUpTime ( float SkillLevel, long SkillRank );
	float CalculateRange ( float SkillLevel, long SkillRank );
	long DetermineSkillRank(int SkillID);	
	bool IsUsedOnEnemies() { return false; };
	bool IsGroupSkill() { return true; };
};

#endif