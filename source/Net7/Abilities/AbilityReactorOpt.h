


#ifndef _ABILITY_REACTOR_OPTIMISATION_H_INCLUDED_
#define _ABILITY_REACTOR_OPTIMISATION_H_INCLUDED_

#include "AbilityBase.h"

class AReactorOptimisation : public AbilityBase
{
public:
	AReactorOptimisation(CMob *me) : AbilityBase(me, STAT_SKILL_REACTOR_OPTIMISATION) {}
public:
	bool UseSkill(long ChargeTime);
	bool CanUse(long TargetID, long AbilityID, long SkillID);
	bool Update(long activation_ID);

	bool SkillInterruptable(bool* OnMotion, bool* OnDamage, bool* OnAction);
	bool Interrupts(int Type) { return (Type == OTHER || Type == WARPING || Type == INCAPACITATE); };

	// Calculate skill level Data
private:
	float CalculateEnergy ( float SkillLevel, long SkillRank );
	float CalculateChargeUpTime ( float SkillLevel, long SkillRank );
	float CalculateRange ( float SkillLevel, long SkillRank );
	float CalculateDuration ( float SkillLevel, long SkillRank );
	long DetermineSkillRank(int SkillID);
	long DetermineMinimumLevel(int SkillID);
	bool IsUsedOnEnemies()  { return false; };
	bool SelfOnly();
	bool IsGroupSkill()		{ return false; };
};

#endif