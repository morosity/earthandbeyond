


#ifndef _ABILITY_FOLDSPACE_H_INCLUDED_
#define _ABILITY_FOLDSPACE_H_INCLUDED_

#include "AbilityBase.h"

class AFoldSpace : public AbilityBase
{
public:
	AFoldSpace(CMob *me) : AbilityBase(me, STAT_SKILL_FOLD_SPACE) {}
public:
	bool UseSkill(long ChargeTime);	
	bool CanUse(long TargetID, long AbilityID, long SkillID);
	bool Update(long activation_ID);

	bool SkillInterruptable(bool* OnMotion, bool* OnDamage, bool* OnAction);
	bool Interrupts(int Type) { return (Type == OTHER || Type == WARPING || Type == INCAPACITATE); };

	// Calculate skill level Data
private:
	float CalculateEnergy ( float SkillLevel, long SkillRank );
	float CalculateChargeUpTime ( float SkillLevel, long SkillRank );
	float CalculateRange ( float SkillLevel, long SkillRank );
	float CalculateTeleportDistance ( float SkillLevel, long SkillRank );
	bool IsUsedOnEnemies();
	bool RequiresTarget();
	bool IsGroupSkill();
	long DetermineSkillRank(int SkillID);
	void Fold();
};

#endif