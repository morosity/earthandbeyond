; Listing generated by Microsoft (R) Optimizing Compiler Version 19.15.26726.0 

	TITLE	D:\Repos\veg1\morveg\Net7\AuxClasses\AuxShake.cpp
	.686P
	.XMM
	include listing.inc
	.model	flat

INCLUDELIB MSVCRTD
INCLUDELIB OLDNAMES

PUBLIC	___local_stdio_printf_options
PUBLIC	__vfprintf_l
PUBLIC	_printf
PUBLIC	?Clear@AuxShake@@QAEXXZ				; AuxShake::Clear
PUBLIC	?ClearFlags@AuxShake@@QAEXXZ			; AuxShake::ClearFlags
PUBLIC	?BuildPacket@AuxShake@@QAEXPAEAAJ@Z		; AuxShake::BuildPacket
PUBLIC	?BuildExtendedPacket@AuxShake@@QAEXPAEAAJ@Z	; AuxShake::BuildExtendedPacket
PUBLIC	?GetData@AuxShake@@QAEPAU_Shake@@XZ		; AuxShake::GetData
PUBLIC	?GetForceX@AuxShake@@QAEMXZ			; AuxShake::GetForceX
PUBLIC	?GetForceY@AuxShake@@QAEMXZ			; AuxShake::GetForceY
PUBLIC	?GetForceZ@AuxShake@@QAEMXZ			; AuxShake::GetForceZ
PUBLIC	?GetDamage@AuxShake@@QAEMXZ			; AuxShake::GetDamage
PUBLIC	?SetData@AuxShake@@QAEXPAU_Shake@@@Z		; AuxShake::SetData
PUBLIC	?SetForceX@AuxShake@@QAEXM@Z			; AuxShake::SetForceX
PUBLIC	?SetForceY@AuxShake@@QAEXM@Z			; AuxShake::SetForceY
PUBLIC	?SetForceZ@AuxShake@@QAEXM@Z			; AuxShake::SetForceZ
PUBLIC	?SetDamage@AuxShake@@QAEXM@Z			; AuxShake::SetDamage
PUBLIC	??$AddData@M@AuxBase@@IAEXPAEMAAJ@Z		; AuxBase::AddData<float>
PUBLIC	??$ReplaceData@M@AuxBase@@IAEXAAMMI@Z		; AuxBase::ReplaceData<float>
PUBLIC	?_OptionsStorage@?1??__local_stdio_printf_options@@9@4_KA ; `__local_stdio_printf_options'::`2'::_OptionsStorage
PUBLIC	??_C@_0BO@FPOJMAIA@Error?3?5Bufferoverflow?5in?5Aux?$CB@ ; `string'
PUBLIC	__real@00000000
EXTRN	_memset:PROC
EXTRN	__imp____acrt_iob_func:PROC
EXTRN	__imp____stdio_common_vfprintf:PROC
EXTRN	?Lock@Mutex@@QAEXXZ:PROC			; Mutex::Lock
EXTRN	?Unlock@Mutex@@QAEXXZ:PROC			; Mutex::Unlock
EXTRN	?AddFlags@AuxBase@@IAEXPAEI0AAJ@Z:PROC		; AuxBase::AddFlags
EXTRN	?SetParentFlag@AuxBase@@IAEXXZ:PROC		; AuxBase::SetParentFlag
EXTRN	?SetParentExtendedFlag@AuxBase@@IAEXI@Z:PROC	; AuxBase::SetParentExtendedFlag
EXTRN	?SetAuxBit@AuxBase@@AAEXPAEI@Z:PROC		; AuxBase::SetAuxBit
EXTRN	?UnsetAuxBit@AuxBase@@AAEXPAEI@Z:PROC		; AuxBase::UnsetAuxBit
EXTRN	__RTC_CheckEsp:PROC
EXTRN	__RTC_InitBase:PROC
EXTRN	__RTC_Shutdown:PROC
EXTRN	__fltused:DWORD
;	COMDAT ?_OptionsStorage@?1??__local_stdio_printf_options@@9@4_KA
_BSS	SEGMENT
?_OptionsStorage@?1??__local_stdio_printf_options@@9@4_KA DQ 01H DUP (?) ; `__local_stdio_printf_options'::`2'::_OptionsStorage
_BSS	ENDS
;	COMDAT __real@00000000
CONST	SEGMENT
__real@00000000 DD 000000000r			; 0
CONST	ENDS
;	COMDAT rtc$TMZ
rtc$TMZ	SEGMENT
__RTC_Shutdown.rtc$TMZ DD FLAT:__RTC_Shutdown
rtc$TMZ	ENDS
;	COMDAT rtc$IMZ
rtc$IMZ	SEGMENT
__RTC_InitBase.rtc$IMZ DD FLAT:__RTC_InitBase
rtc$IMZ	ENDS
;	COMDAT ??_C@_0BO@FPOJMAIA@Error?3?5Bufferoverflow?5in?5Aux?$CB@
CONST	SEGMENT
??_C@_0BO@FPOJMAIA@Error?3?5Bufferoverflow?5in?5Aux?$CB@ DB 'Error: Buffe'
	DB	'roverflow in Aux!', 00H			; `string'
CONST	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxbase.h
;	COMDAT ??$ReplaceData@M@AuxBase@@IAEXAAMMI@Z
_TEXT	SEGMENT
_this$ = -8						; size = 4
_orig$ = 8						; size = 4
_src$ = 12						; size = 4
_flagNum$ = 16						; size = 4
??$ReplaceData@M@AuxBase@@IAEXAAMMI@Z PROC		; AuxBase::ReplaceData<float>, COMDAT
; _this$ = ecx

; 51   : 	{

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 52   :         m_Mutex->Lock();

	mov	eax, DWORD PTR _this$[ebp]
	mov	ecx, DWORD PTR [eax+28]
	call	?Lock@Mutex@@QAEXXZ			; Mutex::Lock

; 53   : 
; 54   : 		if (orig != src)

	mov	eax, DWORD PTR _orig$[ebp]
	movss	xmm0, DWORD PTR [eax]
	ucomiss	xmm0, DWORD PTR _src$[ebp]
	lahf
	test	ah, 68					; 00000044H
	jnp	$LN2@ReplaceDat

; 55   : 		{
; 56   : 			/* The data is different, set the flags */
; 57   : 			SetAuxBit(m_Flags, flagNum);

	mov	eax, DWORD PTR _flagNum$[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	mov	edx, DWORD PTR [ecx+12]
	push	edx
	mov	ecx, DWORD PTR _this$[ebp]
	call	?SetAuxBit@AuxBase@@AAEXPAEI@Z		; AuxBase::SetAuxBit

; 58   : 
; 59   :             /* This eliminates useless recursion */
; 60   :             if ((m_Flags[0] & 0x02) == 0)

	mov	eax, 1
	imul	ecx, eax, 0
	mov	edx, DWORD PTR _this$[ebp]
	mov	eax, DWORD PTR [edx+12]
	movzx	ecx, BYTE PTR [eax+ecx]
	and	ecx, 2
	jne	SHORT $LN3@ReplaceDat

; 61   :             {
; 62   : 			    m_Flags[0] |= 0x02;

	mov	eax, 1
	imul	ecx, eax, 0
	mov	edx, DWORD PTR _this$[ebp]
	mov	eax, DWORD PTR [edx+12]
	movzx	ecx, BYTE PTR [eax+ecx]
	or	ecx, 2
	mov	edx, 1
	imul	eax, edx, 0
	mov	edx, DWORD PTR _this$[ebp]
	mov	edx, DWORD PTR [edx+12]
	mov	BYTE PTR [eax+edx], cl

; 63   : 			    SetParentFlag();

	mov	ecx, DWORD PTR _this$[ebp]
	call	?SetParentFlag@AuxBase@@IAEXXZ		; AuxBase::SetParentFlag

; 64   : 				SetParentExtendedFlag(1);

	push	1
	mov	ecx, DWORD PTR _this$[ebp]
	call	?SetParentExtendedFlag@AuxBase@@IAEXI@Z	; AuxBase::SetParentExtendedFlag
$LN3@ReplaceDat:

; 65   :             }
; 66   : 
; 67   : 			/* Change the extended flags for this bit if needed */
; 68   : 			if (!orig && src)

	mov	eax, DWORD PTR _orig$[ebp]
	movss	xmm0, DWORD PTR [eax]
	ucomiss	xmm0, DWORD PTR __real@00000000
	lahf
	test	ah, 68					; 00000044H
	jp	SHORT $LN4@ReplaceDat
	movss	xmm0, DWORD PTR _src$[ebp]
	ucomiss	xmm0, DWORD PTR __real@00000000
	lahf
	test	ah, 68					; 00000044H
	jnp	SHORT $LN4@ReplaceDat

; 69   : 			{
; 70   : 				UnsetAuxBit(m_ExtendedFlags, flagNum + m_FlagCount);

	mov	eax, DWORD PTR _this$[ebp]
	mov	ecx, DWORD PTR _flagNum$[ebp]
	add	ecx, DWORD PTR [eax+8]
	push	ecx
	mov	edx, DWORD PTR _this$[ebp]
	mov	eax, DWORD PTR [edx+16]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	call	?UnsetAuxBit@AuxBase@@AAEXPAEI@Z	; AuxBase::UnsetAuxBit

; 71   : 				SetAuxBit(m_ExtendedFlags, flagNum);

	mov	eax, DWORD PTR _flagNum$[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	mov	edx, DWORD PTR [ecx+16]
	push	edx
	mov	ecx, DWORD PTR _this$[ebp]
	call	?SetAuxBit@AuxBase@@AAEXPAEI@Z		; AuxBase::SetAuxBit
	jmp	SHORT $LN6@ReplaceDat
$LN4@ReplaceDat:

; 72   : 			}
; 73   : 			else if (!src && orig)

	movss	xmm0, DWORD PTR _src$[ebp]
	ucomiss	xmm0, DWORD PTR __real@00000000
	lahf
	test	ah, 68					; 00000044H
	jp	SHORT $LN6@ReplaceDat
	mov	eax, DWORD PTR _orig$[ebp]
	movss	xmm0, DWORD PTR [eax]
	ucomiss	xmm0, DWORD PTR __real@00000000
	lahf
	test	ah, 68					; 00000044H
	jnp	SHORT $LN6@ReplaceDat

; 74   : 			{
; 75   : 				SetAuxBit(m_ExtendedFlags, flagNum + m_FlagCount);

	mov	eax, DWORD PTR _this$[ebp]
	mov	ecx, DWORD PTR _flagNum$[ebp]
	add	ecx, DWORD PTR [eax+8]
	push	ecx
	mov	edx, DWORD PTR _this$[ebp]
	mov	eax, DWORD PTR [edx+16]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	call	?SetAuxBit@AuxBase@@AAEXPAEI@Z		; AuxBase::SetAuxBit

; 76   : 				UnsetAuxBit(m_ExtendedFlags, flagNum);

	mov	eax, DWORD PTR _flagNum$[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	mov	edx, DWORD PTR [ecx+16]
	push	edx
	mov	ecx, DWORD PTR _this$[ebp]
	call	?UnsetAuxBit@AuxBase@@AAEXPAEI@Z	; AuxBase::UnsetAuxBit
$LN6@ReplaceDat:

; 77   : 			}
; 78   : 
; 79   : 			/* Copy the data */
; 80   : 			orig = src;

	mov	eax, DWORD PTR _orig$[ebp]
	movss	xmm0, DWORD PTR _src$[ebp]
	movss	DWORD PTR [eax], xmm0
$LN2@ReplaceDat:

; 81   : 		}
; 82   : 
; 83   :         m_Mutex->Unlock();

	mov	eax, DWORD PTR _this$[ebp]
	mov	ecx, DWORD PTR [eax+28]
	call	?Unlock@Mutex@@QAEXXZ			; Mutex::Unlock

; 84   : 	}

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 204				; 000000ccH
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	12					; 0000000cH
??$ReplaceData@M@AuxBase@@IAEXAAMMI@Z ENDP		; AuxBase::ReplaceData<float>
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxbase.h
;	COMDAT ??$AddData@M@AuxBase@@IAEXPAEMAAJ@Z
_TEXT	SEGMENT
_this$ = -8						; size = 4
_buffer$ = 8						; size = 4
_data$ = 12						; size = 4
_index$ = 16						; size = 4
??$AddData@M@AuxBase@@IAEXPAEMAAJ@Z PROC		; AuxBase::AddData<float>, COMDAT
; _this$ = ecx

; 34   : 	{

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 35   : 		if (sizeof(T) + index < m_Max_Buffer)

	mov	eax, DWORD PTR _index$[ebp]
	mov	ecx, DWORD PTR [eax]
	add	ecx, 4
	mov	edx, DWORD PTR _this$[ebp]
	cmp	ecx, DWORD PTR [edx+20]
	jae	SHORT $LN2@AddData

; 36   : 		{
; 37   : 			*((T *) &buffer[index]) = data;

	mov	eax, DWORD PTR _index$[ebp]
	mov	ecx, DWORD PTR [eax]
	mov	edx, DWORD PTR _buffer$[ebp]
	movss	xmm0, DWORD PTR _data$[ebp]
	movss	DWORD PTR [edx+ecx], xmm0

; 38   : 			index += sizeof(T);

	mov	eax, DWORD PTR _index$[ebp]
	mov	ecx, DWORD PTR [eax]
	add	ecx, 4
	mov	edx, DWORD PTR _index$[ebp]
	mov	DWORD PTR [edx], ecx

; 39   : 		}
; 40   : 		else

	jmp	SHORT $LN3@AddData
$LN2@AddData:

; 41   : 		{
; 42   : 			printf("Error: Bufferoverflow in Aux!");

	push	OFFSET ??_C@_0BO@FPOJMAIA@Error?3?5Bufferoverflow?5in?5Aux?$CB@
	call	_printf
	add	esp, 4
$LN3@AddData:

; 43   : 		}
; 44   : 	}

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 204				; 000000ccH
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	12					; 0000000cH
??$AddData@M@AuxBase@@IAEXPAEMAAJ@Z ENDP		; AuxBase::AddData<float>
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxshake.cpp
;	COMDAT ?SetDamage@AuxShake@@QAEXM@Z
_TEXT	SEGMENT
_this$ = -8						; size = 4
_NewDamage$ = 8						; size = 4
?SetDamage@AuxShake@@QAEXM@Z PROC			; AuxShake::SetDamage, COMDAT
; _this$ = ecx

; 113  : {

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 114  : 	ReplaceData(Data->Damage, NewDamage, 3);

	push	3
	push	ecx
	movss	xmm0, DWORD PTR _NewDamage$[ebp]
	movss	DWORD PTR [esp], xmm0
	mov	eax, DWORD PTR _this$[ebp]
	mov	ecx, DWORD PTR [eax+32]
	add	ecx, 12					; 0000000cH
	push	ecx
	mov	ecx, DWORD PTR _this$[ebp]
	call	??$ReplaceData@M@AuxBase@@IAEXAAMMI@Z	; AuxBase::ReplaceData<float>

; 115  : }

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 204				; 000000ccH
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	4
?SetDamage@AuxShake@@QAEXM@Z ENDP			; AuxShake::SetDamage
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxshake.cpp
;	COMDAT ?SetForceZ@AuxShake@@QAEXM@Z
_TEXT	SEGMENT
_this$ = -8						; size = 4
_NewForceZ$ = 8						; size = 4
?SetForceZ@AuxShake@@QAEXM@Z PROC			; AuxShake::SetForceZ, COMDAT
; _this$ = ecx

; 108  : {

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 109  : 	ReplaceData(Data->ForceZ, NewForceZ, 2);

	push	2
	push	ecx
	movss	xmm0, DWORD PTR _NewForceZ$[ebp]
	movss	DWORD PTR [esp], xmm0
	mov	eax, DWORD PTR _this$[ebp]
	mov	ecx, DWORD PTR [eax+32]
	add	ecx, 8
	push	ecx
	mov	ecx, DWORD PTR _this$[ebp]
	call	??$ReplaceData@M@AuxBase@@IAEXAAMMI@Z	; AuxBase::ReplaceData<float>

; 110  : }

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 204				; 000000ccH
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	4
?SetForceZ@AuxShake@@QAEXM@Z ENDP			; AuxShake::SetForceZ
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxshake.cpp
;	COMDAT ?SetForceY@AuxShake@@QAEXM@Z
_TEXT	SEGMENT
_this$ = -8						; size = 4
_NewForceY$ = 8						; size = 4
?SetForceY@AuxShake@@QAEXM@Z PROC			; AuxShake::SetForceY, COMDAT
; _this$ = ecx

; 103  : {

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 104  : 	ReplaceData(Data->ForceY, NewForceY, 1);

	push	1
	push	ecx
	movss	xmm0, DWORD PTR _NewForceY$[ebp]
	movss	DWORD PTR [esp], xmm0
	mov	eax, DWORD PTR _this$[ebp]
	mov	ecx, DWORD PTR [eax+32]
	add	ecx, 4
	push	ecx
	mov	ecx, DWORD PTR _this$[ebp]
	call	??$ReplaceData@M@AuxBase@@IAEXAAMMI@Z	; AuxBase::ReplaceData<float>

; 105  : }

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 204				; 000000ccH
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	4
?SetForceY@AuxShake@@QAEXM@Z ENDP			; AuxShake::SetForceY
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxshake.cpp
;	COMDAT ?SetForceX@AuxShake@@QAEXM@Z
_TEXT	SEGMENT
_this$ = -8						; size = 4
_NewForceX$ = 8						; size = 4
?SetForceX@AuxShake@@QAEXM@Z PROC			; AuxShake::SetForceX, COMDAT
; _this$ = ecx

; 98   : {

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 99   : 	ReplaceData(Data->ForceX, NewForceX, 0);

	push	0
	push	ecx
	movss	xmm0, DWORD PTR _NewForceX$[ebp]
	movss	DWORD PTR [esp], xmm0
	mov	eax, DWORD PTR _this$[ebp]
	mov	ecx, DWORD PTR [eax+32]
	push	ecx
	mov	ecx, DWORD PTR _this$[ebp]
	call	??$ReplaceData@M@AuxBase@@IAEXAAMMI@Z	; AuxBase::ReplaceData<float>

; 100  : }

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 204				; 000000ccH
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	4
?SetForceX@AuxShake@@QAEXM@Z ENDP			; AuxShake::SetForceX
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxshake.cpp
;	COMDAT ?SetData@AuxShake@@QAEXPAU_Shake@@@Z
_TEXT	SEGMENT
_this$ = -8						; size = 4
_NewData$ = 8						; size = 4
?SetData@AuxShake@@QAEXPAU_Shake@@@Z PROC		; AuxShake::SetData, COMDAT
; _this$ = ecx

; 90   : {

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 91   : 	ReplaceData(Data->ForceX, NewData->ForceX, 0);

	push	0
	mov	eax, DWORD PTR _NewData$[ebp]
	push	ecx
	movss	xmm0, DWORD PTR [eax]
	movss	DWORD PTR [esp], xmm0
	mov	ecx, DWORD PTR _this$[ebp]
	mov	edx, DWORD PTR [ecx+32]
	push	edx
	mov	ecx, DWORD PTR _this$[ebp]
	call	??$ReplaceData@M@AuxBase@@IAEXAAMMI@Z	; AuxBase::ReplaceData<float>

; 92   : 	ReplaceData(Data->ForceY, NewData->ForceY, 1);

	push	1
	mov	eax, DWORD PTR _NewData$[ebp]
	push	ecx
	movss	xmm0, DWORD PTR [eax+4]
	movss	DWORD PTR [esp], xmm0
	mov	ecx, DWORD PTR _this$[ebp]
	mov	edx, DWORD PTR [ecx+32]
	add	edx, 4
	push	edx
	mov	ecx, DWORD PTR _this$[ebp]
	call	??$ReplaceData@M@AuxBase@@IAEXAAMMI@Z	; AuxBase::ReplaceData<float>

; 93   : 	ReplaceData(Data->ForceZ, NewData->ForceZ, 2);

	push	2
	mov	eax, DWORD PTR _NewData$[ebp]
	push	ecx
	movss	xmm0, DWORD PTR [eax+8]
	movss	DWORD PTR [esp], xmm0
	mov	ecx, DWORD PTR _this$[ebp]
	mov	edx, DWORD PTR [ecx+32]
	add	edx, 8
	push	edx
	mov	ecx, DWORD PTR _this$[ebp]
	call	??$ReplaceData@M@AuxBase@@IAEXAAMMI@Z	; AuxBase::ReplaceData<float>

; 94   : 	ReplaceData(Data->Damage, NewData->Damage, 3);

	push	3
	mov	eax, DWORD PTR _NewData$[ebp]
	push	ecx
	movss	xmm0, DWORD PTR [eax+12]
	movss	DWORD PTR [esp], xmm0
	mov	ecx, DWORD PTR _this$[ebp]
	mov	edx, DWORD PTR [ecx+32]
	add	edx, 12					; 0000000cH
	push	edx
	mov	ecx, DWORD PTR _this$[ebp]
	call	??$ReplaceData@M@AuxBase@@IAEXAAMMI@Z	; AuxBase::ReplaceData<float>

; 95   : }

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 204				; 000000ccH
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	4
?SetData@AuxShake@@QAEXPAU_Shake@@@Z ENDP		; AuxShake::SetData
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxshake.cpp
;	COMDAT ?GetDamage@AuxShake@@QAEMXZ
_TEXT	SEGMENT
_this$ = -8						; size = 4
?GetDamage@AuxShake@@QAEMXZ PROC			; AuxShake::GetDamage, COMDAT
; _this$ = ecx

; 83   : float AuxShake::GetDamage()			{return Data->Damage;}

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx
	mov	eax, DWORD PTR _this$[ebp]
	mov	ecx, DWORD PTR [eax+32]
	fld	DWORD PTR [ecx+12]
	pop	edi
	pop	esi
	pop	ebx
	mov	esp, ebp
	pop	ebp
	ret	0
?GetDamage@AuxShake@@QAEMXZ ENDP			; AuxShake::GetDamage
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxshake.cpp
;	COMDAT ?GetForceZ@AuxShake@@QAEMXZ
_TEXT	SEGMENT
_this$ = -8						; size = 4
?GetForceZ@AuxShake@@QAEMXZ PROC			; AuxShake::GetForceZ, COMDAT
; _this$ = ecx

; 82   : float AuxShake::GetForceZ()			{return Data->ForceZ;}

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx
	mov	eax, DWORD PTR _this$[ebp]
	mov	ecx, DWORD PTR [eax+32]
	fld	DWORD PTR [ecx+8]
	pop	edi
	pop	esi
	pop	ebx
	mov	esp, ebp
	pop	ebp
	ret	0
?GetForceZ@AuxShake@@QAEMXZ ENDP			; AuxShake::GetForceZ
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxshake.cpp
;	COMDAT ?GetForceY@AuxShake@@QAEMXZ
_TEXT	SEGMENT
_this$ = -8						; size = 4
?GetForceY@AuxShake@@QAEMXZ PROC			; AuxShake::GetForceY, COMDAT
; _this$ = ecx

; 81   : float AuxShake::GetForceY()			{return Data->ForceY;}

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx
	mov	eax, DWORD PTR _this$[ebp]
	mov	ecx, DWORD PTR [eax+32]
	fld	DWORD PTR [ecx+4]
	pop	edi
	pop	esi
	pop	ebx
	mov	esp, ebp
	pop	ebp
	ret	0
?GetForceY@AuxShake@@QAEMXZ ENDP			; AuxShake::GetForceY
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxshake.cpp
;	COMDAT ?GetForceX@AuxShake@@QAEMXZ
_TEXT	SEGMENT
_this$ = -8						; size = 4
?GetForceX@AuxShake@@QAEMXZ PROC			; AuxShake::GetForceX, COMDAT
; _this$ = ecx

; 80   : float AuxShake::GetForceX()			{return Data->ForceX;}

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx
	mov	eax, DWORD PTR _this$[ebp]
	mov	ecx, DWORD PTR [eax+32]
	fld	DWORD PTR [ecx]
	pop	edi
	pop	esi
	pop	ebx
	mov	esp, ebp
	pop	ebp
	ret	0
?GetForceX@AuxShake@@QAEMXZ ENDP			; AuxShake::GetForceX
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxshake.cpp
;	COMDAT ?GetData@AuxShake@@QAEPAU_Shake@@XZ
_TEXT	SEGMENT
_this$ = -8						; size = 4
?GetData@AuxShake@@QAEPAU_Shake@@XZ PROC		; AuxShake::GetData, COMDAT
; _this$ = ecx

; 78   : _Shake * AuxShake::GetData()		{return Data;}

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx
	mov	eax, DWORD PTR _this$[ebp]
	mov	eax, DWORD PTR [eax+32]
	pop	edi
	pop	esi
	pop	ebx
	mov	esp, ebp
	pop	ebp
	ret	0
?GetData@AuxShake@@QAEPAU_Shake@@XZ ENDP		; AuxShake::GetData
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxshake.cpp
;	COMDAT ?BuildExtendedPacket@AuxShake@@QAEXPAEAAJ@Z
_TEXT	SEGMENT
_this$ = -8						; size = 4
_buffer$ = 8						; size = 4
_index$ = 12						; size = 4
?BuildExtendedPacket@AuxShake@@QAEXPAEAAJ@Z PROC	; AuxShake::BuildExtendedPacket, COMDAT
; _this$ = ecx

; 50   : {

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 51   : 	AddFlags(ExtendedFlags, sizeof(ExtendedFlags), buffer, index);

	mov	eax, DWORD PTR _index$[ebp]
	push	eax
	mov	ecx, DWORD PTR _buffer$[ebp]
	push	ecx
	push	2
	mov	edx, DWORD PTR _this$[ebp]
	add	edx, 37					; 00000025H
	push	edx
	mov	ecx, DWORD PTR _this$[ebp]
	call	?AddFlags@AuxBase@@IAEXPAEI0AAJ@Z	; AuxBase::AddFlags

; 52   : 
; 53   : 	if (ExtendedFlags[0] & 0x10)	//ExtendedFlags[1] & 0x01

	mov	eax, 1
	imul	ecx, eax, 0
	mov	edx, DWORD PTR _this$[ebp]
	movzx	eax, BYTE PTR [edx+ecx+37]
	and	eax, 16					; 00000010H
	je	SHORT $LN2@BuildExten

; 54   : 	{
; 55   : 		AddData(buffer, Data->ForceX, index);

	mov	eax, DWORD PTR _index$[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	mov	edx, DWORD PTR [ecx+32]
	push	ecx
	movss	xmm0, DWORD PTR [edx]
	movss	DWORD PTR [esp], xmm0
	mov	eax, DWORD PTR _buffer$[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	call	??$AddData@M@AuxBase@@IAEXPAEMAAJ@Z	; AuxBase::AddData<float>
$LN2@BuildExten:

; 56   : 	}
; 57   : 
; 58   : 	if (ExtendedFlags[0] & 0x20)	//ExtendedFlags[1] & 0x02

	mov	eax, 1
	imul	ecx, eax, 0
	mov	edx, DWORD PTR _this$[ebp]
	movzx	eax, BYTE PTR [edx+ecx+37]
	and	eax, 32					; 00000020H
	je	SHORT $LN3@BuildExten

; 59   : 	{
; 60   : 		AddData(buffer, Data->ForceY, index);

	mov	eax, DWORD PTR _index$[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	mov	edx, DWORD PTR [ecx+32]
	push	ecx
	movss	xmm0, DWORD PTR [edx+4]
	movss	DWORD PTR [esp], xmm0
	mov	eax, DWORD PTR _buffer$[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	call	??$AddData@M@AuxBase@@IAEXPAEMAAJ@Z	; AuxBase::AddData<float>
$LN3@BuildExten:

; 61   : 	}
; 62   : 
; 63   : 	if (ExtendedFlags[0] & 0x40)	//ExtendedFlags[1] & 0x04

	mov	eax, 1
	imul	ecx, eax, 0
	mov	edx, DWORD PTR _this$[ebp]
	movzx	eax, BYTE PTR [edx+ecx+37]
	and	eax, 64					; 00000040H
	je	SHORT $LN4@BuildExten

; 64   : 	{
; 65   : 		AddData(buffer, Data->ForceZ, index);

	mov	eax, DWORD PTR _index$[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	mov	edx, DWORD PTR [ecx+32]
	push	ecx
	movss	xmm0, DWORD PTR [edx+8]
	movss	DWORD PTR [esp], xmm0
	mov	eax, DWORD PTR _buffer$[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	call	??$AddData@M@AuxBase@@IAEXPAEMAAJ@Z	; AuxBase::AddData<float>
$LN4@BuildExten:

; 66   : 	}
; 67   : 
; 68   : 	if (ExtendedFlags[0] & 0x80)	//ExtendedFlags[1] & 0x08

	mov	eax, 1
	imul	ecx, eax, 0
	mov	edx, DWORD PTR _this$[ebp]
	movzx	eax, BYTE PTR [edx+ecx+37]
	and	eax, 128				; 00000080H
	je	SHORT $LN5@BuildExten

; 69   : 	{
; 70   : 		AddData(buffer, Data->Damage, index);

	mov	eax, DWORD PTR _index$[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	mov	edx, DWORD PTR [ecx+32]
	push	ecx
	movss	xmm0, DWORD PTR [edx+12]
	movss	DWORD PTR [esp], xmm0
	mov	eax, DWORD PTR _buffer$[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	call	??$AddData@M@AuxBase@@IAEXPAEMAAJ@Z	; AuxBase::AddData<float>
$LN5@BuildExten:

; 71   : 	}
; 72   : }

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 204				; 000000ccH
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	8
?BuildExtendedPacket@AuxShake@@QAEXPAEAAJ@Z ENDP	; AuxShake::BuildExtendedPacket
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxshake.cpp
;	COMDAT ?BuildPacket@AuxShake@@QAEXPAEAAJ@Z
_TEXT	SEGMENT
_this$ = -8						; size = 4
_buffer$ = 8						; size = 4
_index$ = 12						; size = 4
?BuildPacket@AuxShake@@QAEXPAEAAJ@Z PROC		; AuxShake::BuildPacket, COMDAT
; _this$ = ecx

; 21   : {

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 22   : 	AddFlags(Flags, sizeof(Flags), buffer, index);

	mov	eax, DWORD PTR _index$[ebp]
	push	eax
	mov	ecx, DWORD PTR _buffer$[ebp]
	push	ecx
	push	1
	mov	edx, DWORD PTR _this$[ebp]
	add	edx, 36					; 00000024H
	push	edx
	mov	ecx, DWORD PTR _this$[ebp]
	call	?AddFlags@AuxBase@@IAEXPAEI0AAJ@Z	; AuxBase::AddFlags

; 23   : 
; 24   : 	if (Flags[0] & 0x10)	//ExtendedFlags[1] & 0x01

	mov	eax, 1
	imul	ecx, eax, 0
	mov	edx, DWORD PTR _this$[ebp]
	movzx	eax, BYTE PTR [edx+ecx+36]
	and	eax, 16					; 00000010H
	je	SHORT $LN2@BuildPacke

; 25   : 	{
; 26   : 		AddData(buffer, Data->ForceX, index);

	mov	eax, DWORD PTR _index$[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	mov	edx, DWORD PTR [ecx+32]
	push	ecx
	movss	xmm0, DWORD PTR [edx]
	movss	DWORD PTR [esp], xmm0
	mov	eax, DWORD PTR _buffer$[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	call	??$AddData@M@AuxBase@@IAEXPAEMAAJ@Z	; AuxBase::AddData<float>
$LN2@BuildPacke:

; 27   : 	}
; 28   : 
; 29   : 	if (Flags[0] & 0x20)	//ExtendedFlags[1] & 0x02

	mov	eax, 1
	imul	ecx, eax, 0
	mov	edx, DWORD PTR _this$[ebp]
	movzx	eax, BYTE PTR [edx+ecx+36]
	and	eax, 32					; 00000020H
	je	SHORT $LN3@BuildPacke

; 30   : 	{
; 31   : 		AddData(buffer, Data->ForceY, index);

	mov	eax, DWORD PTR _index$[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	mov	edx, DWORD PTR [ecx+32]
	push	ecx
	movss	xmm0, DWORD PTR [edx+4]
	movss	DWORD PTR [esp], xmm0
	mov	eax, DWORD PTR _buffer$[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	call	??$AddData@M@AuxBase@@IAEXPAEMAAJ@Z	; AuxBase::AddData<float>
$LN3@BuildPacke:

; 32   : 	}
; 33   : 
; 34   : 	if (Flags[0] & 0x40)	//ExtendedFlags[1] & 0x04

	mov	eax, 1
	imul	ecx, eax, 0
	mov	edx, DWORD PTR _this$[ebp]
	movzx	eax, BYTE PTR [edx+ecx+36]
	and	eax, 64					; 00000040H
	je	SHORT $LN4@BuildPacke

; 35   : 	{
; 36   : 		AddData(buffer, Data->ForceZ, index);

	mov	eax, DWORD PTR _index$[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	mov	edx, DWORD PTR [ecx+32]
	push	ecx
	movss	xmm0, DWORD PTR [edx+8]
	movss	DWORD PTR [esp], xmm0
	mov	eax, DWORD PTR _buffer$[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	call	??$AddData@M@AuxBase@@IAEXPAEMAAJ@Z	; AuxBase::AddData<float>
$LN4@BuildPacke:

; 37   : 	}
; 38   : 
; 39   : 	if (Flags[0] & 0x80)	//ExtendedFlags[1] & 0x08

	mov	eax, 1
	imul	ecx, eax, 0
	mov	edx, DWORD PTR _this$[ebp]
	movzx	eax, BYTE PTR [edx+ecx+36]
	and	eax, 128				; 00000080H
	je	SHORT $LN5@BuildPacke

; 40   : 	{
; 41   : 		AddData(buffer, Data->Damage, index);

	mov	eax, DWORD PTR _index$[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	mov	edx, DWORD PTR [ecx+32]
	push	ecx
	movss	xmm0, DWORD PTR [edx+12]
	movss	DWORD PTR [esp], xmm0
	mov	eax, DWORD PTR _buffer$[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	call	??$AddData@M@AuxBase@@IAEXPAEMAAJ@Z	; AuxBase::AddData<float>
$LN5@BuildPacke:

; 42   : 	}
; 43   : 
; 44   :     Clear();

	mov	ecx, DWORD PTR _this$[ebp]
	call	?Clear@AuxShake@@QAEXXZ			; AuxShake::Clear

; 45   : 
; 46   : 	memset(Flags,0,sizeof(Flags));

	push	1
	push	0
	mov	eax, DWORD PTR _this$[ebp]
	add	eax, 36					; 00000024H
	push	eax
	call	_memset
	add	esp, 12					; 0000000cH

; 47   : }

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 204				; 000000ccH
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	8
?BuildPacket@AuxShake@@QAEXPAEAAJ@Z ENDP		; AuxShake::BuildPacket
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxshake.cpp
;	COMDAT ?ClearFlags@AuxShake@@QAEXXZ
_TEXT	SEGMENT
_this$ = -8						; size = 4
?ClearFlags@AuxShake@@QAEXXZ PROC			; AuxShake::ClearFlags, COMDAT
; _this$ = ecx

; 134  : {

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 135  : 	memset(Flags,0,sizeof(Flags));

	push	1
	push	0
	mov	eax, DWORD PTR _this$[ebp]
	add	eax, 36					; 00000024H
	push	eax
	call	_memset
	add	esp, 12					; 0000000cH

; 136  : }

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 204				; 000000ccH
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	0
?ClearFlags@AuxShake@@QAEXXZ ENDP			; AuxShake::ClearFlags
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxshake.cpp
;	COMDAT ?Clear@AuxShake@@QAEXXZ
_TEXT	SEGMENT
_this$ = -8						; size = 4
?Clear@AuxShake@@QAEXXZ PROC				; AuxShake::Clear, COMDAT
; _this$ = ecx

; 122  : {

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 123  : 	Data->ForceX = 0;

	mov	eax, DWORD PTR _this$[ebp]
	mov	ecx, DWORD PTR [eax+32]
	xorps	xmm0, xmm0
	movss	DWORD PTR [ecx], xmm0

; 124  : 	Data->ForceY = 0;

	mov	eax, DWORD PTR _this$[ebp]
	mov	ecx, DWORD PTR [eax+32]
	xorps	xmm0, xmm0
	movss	DWORD PTR [ecx+4], xmm0

; 125  : 	Data->ForceZ = 0;

	mov	eax, DWORD PTR _this$[ebp]
	mov	ecx, DWORD PTR [eax+32]
	xorps	xmm0, xmm0
	movss	DWORD PTR [ecx+8], xmm0

; 126  : 	Data->Damage = 0;

	mov	eax, DWORD PTR _this$[ebp]
	mov	ecx, DWORD PTR [eax+32]
	xorps	xmm0, xmm0
	movss	DWORD PTR [ecx+12], xmm0

; 127  : }

	pop	edi
	pop	esi
	pop	ebx
	mov	esp, ebp
	pop	ebp
	ret	0
?Clear@AuxShake@@QAEXXZ ENDP				; AuxShake::Clear
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File c:\program files (x86)\windows kits\10\include\10.0.17134.0\ucrt\stdio.h
;	COMDAT _printf
_TEXT	SEGMENT
__ArgList$ = -20					; size = 4
__Result$ = -8						; size = 4
__Format$ = 8						; size = 4
_printf	PROC						; COMDAT

; 954  :     {

	push	ebp
	mov	ebp, esp
	sub	esp, 228				; 000000e4H
	push	ebx
	push	esi
	push	edi
	lea	edi, DWORD PTR [ebp-228]
	mov	ecx, 57					; 00000039H
	mov	eax, -858993460				; ccccccccH
	rep stosd

; 955  :         int _Result;
; 956  :         va_list _ArgList;
; 957  :         __crt_va_start(_ArgList, _Format);

	lea	eax, DWORD PTR __Format$[ebp+4]
	mov	DWORD PTR __ArgList$[ebp], eax

; 958  :         _Result = _vfprintf_l(stdout, _Format, NULL, _ArgList);

	mov	eax, DWORD PTR __ArgList$[ebp]
	push	eax
	push	0
	mov	ecx, DWORD PTR __Format$[ebp]
	push	ecx
	mov	esi, esp
	push	1
	call	DWORD PTR __imp____acrt_iob_func
	add	esp, 4
	cmp	esi, esp
	call	__RTC_CheckEsp
	push	eax
	call	__vfprintf_l
	add	esp, 16					; 00000010H
	mov	DWORD PTR __Result$[ebp], eax

; 959  :         __crt_va_end(_ArgList);

	mov	DWORD PTR __ArgList$[ebp], 0

; 960  :         return _Result;

	mov	eax, DWORD PTR __Result$[ebp]

; 961  :     }

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 228				; 000000e4H
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	0
_printf	ENDP
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File c:\program files (x86)\windows kits\10\include\10.0.17134.0\ucrt\stdio.h
;	COMDAT __vfprintf_l
_TEXT	SEGMENT
__Stream$ = 8						; size = 4
__Format$ = 12						; size = 4
__Locale$ = 16						; size = 4
__ArgList$ = 20						; size = 4
__vfprintf_l PROC					; COMDAT

; 642  :     {

	push	ebp
	mov	ebp, esp
	sub	esp, 192				; 000000c0H
	push	ebx
	push	esi
	push	edi
	lea	edi, DWORD PTR [ebp-192]
	mov	ecx, 48					; 00000030H
	mov	eax, -858993460				; ccccccccH
	rep stosd

; 643  :         return __stdio_common_vfprintf(_CRT_INTERNAL_LOCAL_PRINTF_OPTIONS, _Stream, _Format, _Locale, _ArgList);

	mov	esi, esp
	mov	eax, DWORD PTR __ArgList$[ebp]
	push	eax
	mov	ecx, DWORD PTR __Locale$[ebp]
	push	ecx
	mov	edx, DWORD PTR __Format$[ebp]
	push	edx
	mov	eax, DWORD PTR __Stream$[ebp]
	push	eax
	call	___local_stdio_printf_options
	mov	ecx, DWORD PTR [eax+4]
	push	ecx
	mov	edx, DWORD PTR [eax]
	push	edx
	call	DWORD PTR __imp____stdio_common_vfprintf
	add	esp, 24					; 00000018H
	cmp	esi, esp
	call	__RTC_CheckEsp

; 644  :     }

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 192				; 000000c0H
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	0
__vfprintf_l ENDP
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File c:\program files (x86)\windows kits\10\include\10.0.17134.0\ucrt\corecrt_stdio_config.h
;	COMDAT ___local_stdio_printf_options
_TEXT	SEGMENT
___local_stdio_printf_options PROC			; COMDAT

; 85   :     {

	push	ebp
	mov	ebp, esp
	sub	esp, 192				; 000000c0H
	push	ebx
	push	esi
	push	edi
	lea	edi, DWORD PTR [ebp-192]
	mov	ecx, 48					; 00000030H
	mov	eax, -858993460				; ccccccccH
	rep stosd

; 86   :         static unsigned __int64 _OptionsStorage;
; 87   :         return &_OptionsStorage;

	mov	eax, OFFSET ?_OptionsStorage@?1??__local_stdio_printf_options@@9@4_KA ; `__local_stdio_printf_options'::`2'::_OptionsStorage

; 88   :     }

	pop	edi
	pop	esi
	pop	ebx
	mov	esp, ebp
	pop	ebp
	ret	0
___local_stdio_printf_options ENDP
_TEXT	ENDS
END
