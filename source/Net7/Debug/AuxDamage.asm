; Listing generated by Microsoft (R) Optimizing Compiler Version 19.15.26726.0 

	TITLE	D:\Repos\veg1\morveg\Net7\AuxClasses\AuxDamage.cpp
	.686P
	.XMM
	include listing.inc
	.model	flat

INCLUDELIB MSVCRTD
INCLUDELIB OLDNAMES

PUBLIC	?Clear@AuxDamage@@QAEXXZ			; AuxDamage::Clear
PUBLIC	?ClearFlags@AuxDamage@@QAEXXZ			; AuxDamage::ClearFlags
PUBLIC	?BuildPacket@AuxDamage@@QAEXPAEAAJ@Z		; AuxDamage::BuildPacket
PUBLIC	?BuildExtendedPacket@AuxDamage@@QAEXPAEAAJ@Z	; AuxDamage::BuildExtendedPacket
PUBLIC	?BuildSpecialPacket@AuxDamage@@QAEXPAEAAJ@Z	; AuxDamage::BuildSpecialPacket
PUBLIC	?GetData@AuxDamage@@QAEPAU_Damage@@XZ		; AuxDamage::GetData
PUBLIC	?SetData@AuxDamage@@QAEXPAU_Damage@@@Z		; AuxDamage::SetData
EXTRN	_memset:PROC
EXTRN	@_RTC_CheckStackVars@8:PROC
EXTRN	__RTC_CheckEsp:PROC
EXTRN	__RTC_InitBase:PROC
EXTRN	__RTC_Shutdown:PROC
;	COMDAT rtc$TMZ
rtc$TMZ	SEGMENT
__RTC_Shutdown.rtc$TMZ DD FLAT:__RTC_Shutdown
rtc$TMZ	ENDS
;	COMDAT rtc$IMZ
rtc$IMZ	SEGMENT
__RTC_InitBase.rtc$IMZ DD FLAT:__RTC_InitBase
rtc$IMZ	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxdamage.cpp
;	COMDAT ?SetData@AuxDamage@@QAEXPAU_Damage@@@Z
_TEXT	SEGMENT
_this$ = -8						; size = 4
_NewData$ = 8						; size = 4
?SetData@AuxDamage@@QAEXPAU_Damage@@@Z PROC		; AuxDamage::SetData, COMDAT
; _this$ = ecx

; 54   : {

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 55   : 	/*
; 56   : 	ReplaceData(Data.Slot1, NewData->Slot1, 0);
; 57   : 	ReplaceData(Data.Slot2, NewData->Slot2, 1);
; 58   : 	ReplaceData(Data.Slot3, NewData->Slot3, 2);
; 59   : 	ReplaceData(Data.Slot4, NewData->Slot4, 3);
; 60   : 	*/
; 61   : }

	pop	edi
	pop	esi
	pop	ebx
	mov	esp, ebp
	pop	ebp
	ret	4
?SetData@AuxDamage@@QAEXPAU_Damage@@@Z ENDP		; AuxDamage::SetData
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxdamage.cpp
;	COMDAT ?GetData@AuxDamage@@QAEPAU_Damage@@XZ
_TEXT	SEGMENT
_this$ = -8						; size = 4
?GetData@AuxDamage@@QAEPAU_Damage@@XZ PROC		; AuxDamage::GetData, COMDAT
; _this$ = ecx

; 47   : _Damage * AuxDamage::GetData()			{return Data;}

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx
	mov	eax, DWORD PTR _this$[ebp]
	mov	eax, DWORD PTR [eax+32]
	pop	edi
	pop	esi
	pop	ebx
	mov	esp, ebp
	pop	ebp
	ret	0
?GetData@AuxDamage@@QAEPAU_Damage@@XZ ENDP		; AuxDamage::GetData
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxdamage.cpp
;	COMDAT ?BuildSpecialPacket@AuxDamage@@QAEXPAEAAJ@Z
_TEXT	SEGMENT
_this$ = -8						; size = 4
_buffer$ = 8						; size = 4
_index$ = 12						; size = 4
?BuildSpecialPacket@AuxDamage@@QAEXPAEAAJ@Z PROC	; AuxDamage::BuildSpecialPacket, COMDAT
; _this$ = ecx

; 37   : {

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 38   : 	/*
; 39   : 	AddFlags(Flags, sizeof(Flags), buffer, index);
; 40   :     */
; 41   : }

	pop	edi
	pop	esi
	pop	ebx
	mov	esp, ebp
	pop	ebp
	ret	8
?BuildSpecialPacket@AuxDamage@@QAEXPAEAAJ@Z ENDP	; AuxDamage::BuildSpecialPacket
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxdamage.cpp
;	COMDAT ?BuildExtendedPacket@AuxDamage@@QAEXPAEAAJ@Z
_TEXT	SEGMENT
_this$ = -8						; size = 4
_buffer$ = 8						; size = 4
_index$ = 12						; size = 4
?BuildExtendedPacket@AuxDamage@@QAEXPAEAAJ@Z PROC	; AuxDamage::BuildExtendedPacket, COMDAT
; _this$ = ecx

; 30   : {

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 31   :     /*
; 32   :     AddFlags(ExtendedFlags, sizeof(ExtendedFlags), buffer, index);
; 33   :     */
; 34   : }

	pop	edi
	pop	esi
	pop	ebx
	mov	esp, ebp
	pop	ebp
	ret	8
?BuildExtendedPacket@AuxDamage@@QAEXPAEAAJ@Z ENDP	; AuxDamage::BuildExtendedPacket
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxdamage.cpp
;	COMDAT ?BuildPacket@AuxDamage@@QAEXPAEAAJ@Z
_TEXT	SEGMENT
_this$ = -8						; size = 4
_buffer$ = 8						; size = 4
_index$ = 12						; size = 4
?BuildPacket@AuxDamage@@QAEXPAEAAJ@Z PROC		; AuxDamage::BuildPacket, COMDAT
; _this$ = ecx

; 21   : {

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 22   : 	/*
; 23   : 	AddFlags(Flags, sizeof(Flags), buffer, index);
; 24   : 
; 25   : 	memset(Flags,0,sizeof(Flags));
; 26   : 	*/
; 27   : }

	pop	edi
	pop	esi
	pop	ebx
	mov	esp, ebp
	pop	ebp
	ret	8
?BuildPacket@AuxDamage@@QAEXPAEAAJ@Z ENDP		; AuxDamage::BuildPacket
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxdamage.cpp
;	COMDAT ?ClearFlags@AuxDamage@@QAEXXZ
_TEXT	SEGMENT
_this$ = -8						; size = 4
?ClearFlags@AuxDamage@@QAEXXZ PROC			; AuxDamage::ClearFlags, COMDAT
; _this$ = ecx

; 77   : {

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 78   : 	memset(Flags,0,sizeof(Flags));

	push	2
	push	0
	mov	eax, DWORD PTR _this$[ebp]
	add	eax, 36					; 00000024H
	push	eax
	call	_memset
	add	esp, 12					; 0000000cH

; 79   : }

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 204				; 000000ccH
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	0
?ClearFlags@AuxDamage@@QAEXXZ ENDP			; AuxDamage::ClearFlags
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxdamage.cpp
;	COMDAT ?Clear@AuxDamage@@QAEXXZ
_TEXT	SEGMENT
_ClearData$ = -20					; size = 4
_this$ = -8						; size = 4
?Clear@AuxDamage@@QAEXXZ PROC				; AuxDamage::Clear, COMDAT
; _this$ = ecx

; 68   : {

	push	ebp
	mov	ebp, esp
	sub	esp, 216				; 000000d8H
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-216]
	mov	ecx, 54					; 00000036H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 69   :     _Damage ClearData;
; 70   :     
; 71   :     ClearData.NoClueWhatSoEver = 0;

	mov	DWORD PTR _ClearData$[ebp], 0

; 72   : 
; 73   :     SetData(&ClearData);

	lea	eax, DWORD PTR _ClearData$[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	call	?SetData@AuxDamage@@QAEXPAU_Damage@@@Z	; AuxDamage::SetData

; 74   : }

	push	edx
	mov	ecx, ebp
	push	eax
	lea	edx, DWORD PTR $LN5@Clear
	call	@_RTC_CheckStackVars@8
	pop	eax
	pop	edx
	pop	edi
	pop	esi
	pop	ebx
	add	esp, 216				; 000000d8H
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	0
	npad	1
$LN5@Clear:
	DD	1
	DD	$LN4@Clear
$LN4@Clear:
	DD	-20					; ffffffecH
	DD	4
	DD	$LN3@Clear
$LN3@Clear:
	DB	67					; 00000043H
	DB	108					; 0000006cH
	DB	101					; 00000065H
	DB	97					; 00000061H
	DB	114					; 00000072H
	DB	68					; 00000044H
	DB	97					; 00000061H
	DB	116					; 00000074H
	DB	97					; 00000061H
	DB	0
?Clear@AuxDamage@@QAEXXZ ENDP				; AuxDamage::Clear
_TEXT	ENDS
END
