; Listing generated by Microsoft (R) Optimizing Compiler Version 19.15.26726.0 

	TITLE	D:\Repos\veg1\morveg\Net7\AuxClasses\AuxPrimaryCategories.cpp
	.686P
	.XMM
	include listing.inc
	.model	flat

INCLUDELIB MSVCRTD
INCLUDELIB OLDNAMES

PUBLIC	___local_stdio_printf_options
PUBLIC	__vfprintf_l
PUBLIC	_printf
PUBLIC	?ClearFlags@AuxPrimaryCategories@@QAEXXZ	; AuxPrimaryCategories::ClearFlags
PUBLIC	?BuildPacket@AuxPrimaryCategories@@QAEXPAEAAJ_N@Z ; AuxPrimaryCategories::BuildPacket
PUBLIC	?GetData@AuxPrimaryCategories@@QAEPAU_PrimaryCategories@@XZ ; AuxPrimaryCategories::GetData
PUBLIC	?SetData@AuxPrimaryCategories@@QAEXPAU_PrimaryCategories@@@Z ; AuxPrimaryCategories::SetData
PUBLIC	?CheckData@AuxPrimaryCategories@@MAEXXZ		; AuxPrimaryCategories::CheckData
PUBLIC	?HasData@AuxPrimaryCategories@@AAEHXZ		; AuxPrimaryCategories::HasData
PUBLIC	??$AddData@D@AuxBase@@IAEXPAEDAAJ@Z		; AuxBase::AddData<char>
PUBLIC	?_OptionsStorage@?1??__local_stdio_printf_options@@9@4_KA ; `__local_stdio_printf_options'::`2'::_OptionsStorage
PUBLIC	??_C@_0BO@FPOJMAIA@Error?3?5Bufferoverflow?5in?5Aux?$CB@ ; `string'
EXTRN	_memcpy:PROC
EXTRN	_memset:PROC
EXTRN	__imp____acrt_iob_func:PROC
EXTRN	__imp____stdio_common_vfprintf:PROC
EXTRN	?AddFlags@AuxBase@@IAEXPAEI0AAJ@Z:PROC		; AuxBase::AddFlags
EXTRN	?SetParentExtendedFlag@AuxBase@@IAEXI@Z:PROC	; AuxBase::SetParentExtendedFlag
EXTRN	?ClearFlags@AuxPrimaryCategory@@QAEXXZ:PROC	; AuxPrimaryCategory::ClearFlags
EXTRN	?BuildPacket@AuxPrimaryCategory@@QAEXPAEAAJ_N@Z:PROC ; AuxPrimaryCategory::BuildPacket
EXTRN	?SetData@AuxPrimaryCategory@@QAEXPAU_PrimaryCategory@@@Z:PROC ; AuxPrimaryCategory::SetData
EXTRN	__RTC_CheckEsp:PROC
EXTRN	__RTC_InitBase:PROC
EXTRN	__RTC_Shutdown:PROC
;	COMDAT ?_OptionsStorage@?1??__local_stdio_printf_options@@9@4_KA
_BSS	SEGMENT
?_OptionsStorage@?1??__local_stdio_printf_options@@9@4_KA DQ 01H DUP (?) ; `__local_stdio_printf_options'::`2'::_OptionsStorage
_BSS	ENDS
;	COMDAT rtc$TMZ
rtc$TMZ	SEGMENT
__RTC_Shutdown.rtc$TMZ DD FLAT:__RTC_Shutdown
rtc$TMZ	ENDS
;	COMDAT rtc$IMZ
rtc$IMZ	SEGMENT
__RTC_InitBase.rtc$IMZ DD FLAT:__RTC_InitBase
rtc$IMZ	ENDS
;	COMDAT ??_C@_0BO@FPOJMAIA@Error?3?5Bufferoverflow?5in?5Aux?$CB@
CONST	SEGMENT
??_C@_0BO@FPOJMAIA@Error?3?5Bufferoverflow?5in?5Aux?$CB@ DB 'Error: Buffe'
	DB	'roverflow in Aux!', 00H			; `string'
CONST	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxbase.h
;	COMDAT ??$AddData@D@AuxBase@@IAEXPAEDAAJ@Z
_TEXT	SEGMENT
_this$ = -8						; size = 4
_buffer$ = 8						; size = 4
_data$ = 12						; size = 1
_index$ = 16						; size = 4
??$AddData@D@AuxBase@@IAEXPAEDAAJ@Z PROC		; AuxBase::AddData<char>, COMDAT
; _this$ = ecx

; 34   : 	{

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 35   : 		if (sizeof(T) + index < m_Max_Buffer)

	mov	eax, DWORD PTR _index$[ebp]
	mov	ecx, DWORD PTR [eax]
	add	ecx, 1
	mov	edx, DWORD PTR _this$[ebp]
	cmp	ecx, DWORD PTR [edx+20]
	jae	SHORT $LN2@AddData

; 36   : 		{
; 37   : 			*((T *) &buffer[index]) = data;

	mov	eax, DWORD PTR _index$[ebp]
	mov	ecx, DWORD PTR [eax]
	mov	edx, DWORD PTR _buffer$[ebp]
	mov	al, BYTE PTR _data$[ebp]
	mov	BYTE PTR [edx+ecx], al

; 38   : 			index += sizeof(T);

	mov	eax, DWORD PTR _index$[ebp]
	mov	ecx, DWORD PTR [eax]
	add	ecx, 1
	mov	edx, DWORD PTR _index$[ebp]
	mov	DWORD PTR [edx], ecx

; 39   : 		}
; 40   : 		else

	jmp	SHORT $LN1@AddData
$LN2@AddData:

; 41   : 		{
; 42   : 			printf("Error: Bufferoverflow in Aux!");

	push	OFFSET ??_C@_0BO@FPOJMAIA@Error?3?5Bufferoverflow?5in?5Aux?$CB@
	call	_printf
	add	esp, 4
$LN1@AddData:

; 43   : 		}
; 44   : 	}

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 204				; 000000ccH
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	12					; 0000000cH
??$AddData@D@AuxBase@@IAEXPAEDAAJ@Z ENDP		; AuxBase::AddData<char>
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxprimarycategories.cpp
;	COMDAT ?HasData@AuxPrimaryCategories@@AAEHXZ
_TEXT	SEGMENT
_this$ = -8						; size = 4
?HasData@AuxPrimaryCategories@@AAEHXZ PROC		; AuxPrimaryCategories::HasData, COMDAT
; _this$ = ecx

; 81   : {

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 82   : 	return (ExtendedFlags[0] & 0x30);

	mov	eax, 1
	imul	ecx, eax, 0
	mov	edx, DWORD PTR _this$[ebp]
	movzx	eax, BYTE PTR [edx+ecx+37]
	and	eax, 48					; 00000030H

; 83   : }

	pop	edi
	pop	esi
	pop	ebx
	mov	esp, ebp
	pop	ebp
	ret	0
?HasData@AuxPrimaryCategories@@AAEHXZ ENDP		; AuxPrimaryCategories::HasData
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxprimarycategories.cpp
;	COMDAT ?CheckData@AuxPrimaryCategories@@MAEXXZ
_TEXT	SEGMENT
_this$ = -8						; size = 4
?CheckData@AuxPrimaryCategories@@MAEXXZ PROC		; AuxPrimaryCategories::CheckData, COMDAT
; _this$ = ecx

; 76   : {

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 77   : 	SetParentExtendedFlag(HasData());

	mov	ecx, DWORD PTR _this$[ebp]
	call	?HasData@AuxPrimaryCategories@@AAEHXZ	; AuxPrimaryCategories::HasData
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	call	?SetParentExtendedFlag@AuxBase@@IAEXI@Z	; AuxBase::SetParentExtendedFlag

; 78   : }

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 204				; 000000ccH
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	0
?CheckData@AuxPrimaryCategories@@MAEXXZ ENDP		; AuxPrimaryCategories::CheckData
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxprimarycategories.cpp
;	COMDAT ?SetData@AuxPrimaryCategories@@QAEXPAU_PrimaryCategories@@@Z
_TEXT	SEGMENT
_this$ = -8						; size = 4
_NewData$ = 8						; size = 4
?SetData@AuxPrimaryCategories@@QAEXPAU_PrimaryCategories@@@Z PROC ; AuxPrimaryCategories::SetData, COMDAT
; _this$ = ecx

; 64   : {

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 65   : 	PrimaryCategory[0].SetData(&NewData->PrimaryCategory[0]);

	mov	eax, 1125				; 00000465H
	imul	ecx, eax, 0
	add	ecx, DWORD PTR _NewData$[ebp]
	push	ecx
	mov	edx, 1448				; 000005a8H
	imul	eax, edx, 0
	mov	ecx, DWORD PTR _this$[ebp]
	lea	ecx, DWORD PTR [ecx+eax+38]
	call	?SetData@AuxPrimaryCategory@@QAEXPAU_PrimaryCategory@@@Z ; AuxPrimaryCategory::SetData

; 66   : 	PrimaryCategory[1].SetData(&NewData->PrimaryCategory[1]);

	mov	eax, 1125				; 00000465H
	shl	eax, 0
	add	eax, DWORD PTR _NewData$[ebp]
	push	eax
	mov	ecx, 1448				; 000005a8H
	shl	ecx, 0
	mov	edx, DWORD PTR _this$[ebp]
	lea	ecx, DWORD PTR [edx+ecx+38]
	call	?SetData@AuxPrimaryCategory@@QAEXPAU_PrimaryCategory@@@Z ; AuxPrimaryCategory::SetData

; 67   : 
; 68   :     CheckData();

	mov	eax, DWORD PTR _this$[ebp]
	mov	edx, DWORD PTR [eax]
	mov	esi, esp
	mov	ecx, DWORD PTR _this$[ebp]
	mov	eax, DWORD PTR [edx]
	call	eax
	cmp	esi, esp
	call	__RTC_CheckEsp

; 69   : }

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 204				; 000000ccH
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	4
?SetData@AuxPrimaryCategories@@QAEXPAU_PrimaryCategories@@@Z ENDP ; AuxPrimaryCategories::SetData
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxprimarycategories.cpp
;	COMDAT ?GetData@AuxPrimaryCategories@@QAEPAU_PrimaryCategories@@XZ
_TEXT	SEGMENT
_this$ = -8						; size = 4
?GetData@AuxPrimaryCategories@@QAEPAU_PrimaryCategories@@XZ PROC ; AuxPrimaryCategories::GetData, COMDAT
; _this$ = ecx

; 57   : _PrimaryCategories * AuxPrimaryCategories::GetData()    {return Data;}

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx
	mov	eax, DWORD PTR _this$[ebp]
	mov	eax, DWORD PTR [eax+32]
	pop	edi
	pop	esi
	pop	ebx
	mov	esp, ebp
	pop	ebp
	ret	0
?GetData@AuxPrimaryCategories@@QAEPAU_PrimaryCategories@@XZ ENDP ; AuxPrimaryCategories::GetData
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxprimarycategories.cpp
;	COMDAT ?BuildPacket@AuxPrimaryCategories@@QAEXPAEAAJ_N@Z
_TEXT	SEGMENT
_this$ = -8						; size = 4
_buffer$ = 8						; size = 4
_index$ = 12						; size = 4
_TwoBitFlags$ = 16					; size = 1
?BuildPacket@AuxPrimaryCategories@@QAEXPAEAAJ_N@Z PROC	; AuxPrimaryCategories::BuildPacket, COMDAT
; _this$ = ecx

; 21   : {

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 22   : 	if (TwoBitFlags)

	movzx	eax, BYTE PTR _TwoBitFlags$[ebp]
	test	eax, eax
	je	SHORT $LN2@BuildPacke

; 23   : 	{
; 24   : 		AddFlags(ExtendedFlags, sizeof(ExtendedFlags), buffer, index);

	mov	eax, DWORD PTR _index$[ebp]
	push	eax
	mov	ecx, DWORD PTR _buffer$[ebp]
	push	ecx
	push	1
	mov	edx, DWORD PTR _this$[ebp]
	add	edx, 37					; 00000025H
	push	edx
	mov	ecx, DWORD PTR _this$[ebp]
	call	?AddFlags@AuxBase@@IAEXPAEI0AAJ@Z	; AuxBase::AddFlags

; 25   : 		memcpy(Flags, ExtendedFlags, sizeof(Flags));

	push	1
	mov	eax, DWORD PTR _this$[ebp]
	add	eax, 37					; 00000025H
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	add	ecx, 36					; 00000024H
	push	ecx
	call	_memcpy
	add	esp, 12					; 0000000cH

; 26   : 	}
; 27   : 	else

	jmp	SHORT $LN3@BuildPacke
$LN2@BuildPacke:

; 28   : 	{
; 29   : 		AddFlags(Flags, sizeof(Flags), buffer, index);

	mov	eax, DWORD PTR _index$[ebp]
	push	eax
	mov	ecx, DWORD PTR _buffer$[ebp]
	push	ecx
	push	1
	mov	edx, DWORD PTR _this$[ebp]
	add	edx, 36					; 00000024H
	push	edx
	mov	ecx, DWORD PTR _this$[ebp]
	call	?AddFlags@AuxBase@@IAEXPAEI0AAJ@Z	; AuxBase::AddFlags
$LN3@BuildPacke:

; 30   : 	}
; 31   : 
; 32   : 	if (Flags[0] & 0x10)	//ExtendedFlags[0] & 0x40

	mov	eax, 1
	imul	ecx, eax, 0
	mov	edx, DWORD PTR _this$[ebp]
	movzx	eax, BYTE PTR [edx+ecx+36]
	and	eax, 16					; 00000010H
	je	SHORT $LN4@BuildPacke

; 33   : 	{
; 34   : 		PrimaryCategory[0].BuildPacket(buffer, index, TwoBitFlags);

	movzx	eax, BYTE PTR _TwoBitFlags$[ebp]
	push	eax
	mov	ecx, DWORD PTR _index$[ebp]
	push	ecx
	mov	edx, DWORD PTR _buffer$[ebp]
	push	edx
	mov	eax, 1448				; 000005a8H
	imul	ecx, eax, 0
	mov	edx, DWORD PTR _this$[ebp]
	lea	ecx, DWORD PTR [edx+ecx+38]
	call	?BuildPacket@AuxPrimaryCategory@@QAEXPAEAAJ_N@Z ; AuxPrimaryCategory::BuildPacket
	jmp	SHORT $LN5@BuildPacke
$LN4@BuildPacke:

; 35   : 	}
; 36   : 	else if (TwoBitFlags && ExtendedFlags[0] & 0x40)

	movzx	eax, BYTE PTR _TwoBitFlags$[ebp]
	test	eax, eax
	je	SHORT $LN5@BuildPacke
	mov	eax, 1
	imul	ecx, eax, 0
	mov	edx, DWORD PTR _this$[ebp]
	movzx	eax, BYTE PTR [edx+ecx+37]
	and	eax, 64					; 00000040H
	je	SHORT $LN5@BuildPacke

; 37   : 	{
; 38   : 		AddData(buffer, char(0x05), index);

	mov	eax, DWORD PTR _index$[ebp]
	push	eax
	push	5
	mov	ecx, DWORD PTR _buffer$[ebp]
	push	ecx
	mov	ecx, DWORD PTR _this$[ebp]
	call	??$AddData@D@AuxBase@@IAEXPAEDAAJ@Z	; AuxBase::AddData<char>
$LN5@BuildPacke:

; 39   : 	}
; 40   : 
; 41   : 	if (Flags[0] & 0x20)	//ExtendedFlags[0] & 0x80

	mov	eax, 1
	imul	ecx, eax, 0
	mov	edx, DWORD PTR _this$[ebp]
	movzx	eax, BYTE PTR [edx+ecx+36]
	and	eax, 32					; 00000020H
	je	SHORT $LN7@BuildPacke

; 42   : 	{
; 43   : 		PrimaryCategory[1].BuildPacket(buffer, index, TwoBitFlags);

	movzx	eax, BYTE PTR _TwoBitFlags$[ebp]
	push	eax
	mov	ecx, DWORD PTR _index$[ebp]
	push	ecx
	mov	edx, DWORD PTR _buffer$[ebp]
	push	edx
	mov	eax, 1448				; 000005a8H
	shl	eax, 0
	mov	ecx, DWORD PTR _this$[ebp]
	lea	ecx, DWORD PTR [ecx+eax+38]
	call	?BuildPacket@AuxPrimaryCategory@@QAEXPAEAAJ_N@Z ; AuxPrimaryCategory::BuildPacket
	jmp	SHORT $LN8@BuildPacke
$LN7@BuildPacke:

; 44   : 	}
; 45   : 	else if (TwoBitFlags && ExtendedFlags[0] & 0x80)

	movzx	eax, BYTE PTR _TwoBitFlags$[ebp]
	test	eax, eax
	je	SHORT $LN8@BuildPacke
	mov	eax, 1
	imul	ecx, eax, 0
	mov	edx, DWORD PTR _this$[ebp]
	movzx	eax, BYTE PTR [edx+ecx+37]
	and	eax, 128				; 00000080H
	je	SHORT $LN8@BuildPacke

; 46   : 	{
; 47   : 		AddData(buffer, char(0x05), index);

	mov	eax, DWORD PTR _index$[ebp]
	push	eax
	push	5
	mov	ecx, DWORD PTR _buffer$[ebp]
	push	ecx
	mov	ecx, DWORD PTR _this$[ebp]
	call	??$AddData@D@AuxBase@@IAEXPAEDAAJ@Z	; AuxBase::AddData<char>
$LN8@BuildPacke:

; 48   : 	}
; 49   : 
; 50   : 	memset(Flags,0,sizeof(Flags));

	push	1
	push	0
	mov	eax, DWORD PTR _this$[ebp]
	add	eax, 36					; 00000024H
	push	eax
	call	_memset
	add	esp, 12					; 0000000cH

; 51   : }

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 204				; 000000ccH
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	12					; 0000000cH
?BuildPacket@AuxPrimaryCategories@@QAEXPAEAAJ_N@Z ENDP	; AuxPrimaryCategories::BuildPacket
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxprimarycategories.cpp
;	COMDAT ?ClearFlags@AuxPrimaryCategories@@QAEXXZ
_TEXT	SEGMENT
_i$1 = -20						; size = 4
_this$ = -8						; size = 4
?ClearFlags@AuxPrimaryCategories@@QAEXXZ PROC		; AuxPrimaryCategories::ClearFlags, COMDAT
; _this$ = ecx

; 90   : {

	push	ebp
	mov	ebp, esp
	sub	esp, 216				; 000000d8H
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-216]
	mov	ecx, 54					; 00000036H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 91   : 	memset(Flags,0,sizeof(Flags));

	push	1
	push	0
	mov	eax, DWORD PTR _this$[ebp]
	add	eax, 36					; 00000024H
	push	eax
	call	_memset
	add	esp, 12					; 0000000cH

; 92   : 
; 93   : 	for (int i=0;i<2;i++)

	mov	DWORD PTR _i$1[ebp], 0
	jmp	SHORT $LN4@ClearFlags
$LN2@ClearFlags:
	mov	eax, DWORD PTR _i$1[ebp]
	add	eax, 1
	mov	DWORD PTR _i$1[ebp], eax
$LN4@ClearFlags:
	cmp	DWORD PTR _i$1[ebp], 2
	jge	SHORT $LN1@ClearFlags

; 94   : 	{
; 95   :         PrimaryCategory[i].ClearFlags();

	imul	eax, DWORD PTR _i$1[ebp], 1448
	mov	ecx, DWORD PTR _this$[ebp]
	lea	ecx, DWORD PTR [ecx+eax+38]
	call	?ClearFlags@AuxPrimaryCategory@@QAEXXZ	; AuxPrimaryCategory::ClearFlags

; 96   : 	}

	jmp	SHORT $LN2@ClearFlags
$LN1@ClearFlags:

; 97   : }

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 216				; 000000d8H
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	0
?ClearFlags@AuxPrimaryCategories@@QAEXXZ ENDP		; AuxPrimaryCategories::ClearFlags
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File c:\program files (x86)\windows kits\10\include\10.0.17134.0\ucrt\stdio.h
;	COMDAT _printf
_TEXT	SEGMENT
__ArgList$ = -20					; size = 4
__Result$ = -8						; size = 4
__Format$ = 8						; size = 4
_printf	PROC						; COMDAT

; 954  :     {

	push	ebp
	mov	ebp, esp
	sub	esp, 228				; 000000e4H
	push	ebx
	push	esi
	push	edi
	lea	edi, DWORD PTR [ebp-228]
	mov	ecx, 57					; 00000039H
	mov	eax, -858993460				; ccccccccH
	rep stosd

; 955  :         int _Result;
; 956  :         va_list _ArgList;
; 957  :         __crt_va_start(_ArgList, _Format);

	lea	eax, DWORD PTR __Format$[ebp+4]
	mov	DWORD PTR __ArgList$[ebp], eax

; 958  :         _Result = _vfprintf_l(stdout, _Format, NULL, _ArgList);

	mov	eax, DWORD PTR __ArgList$[ebp]
	push	eax
	push	0
	mov	ecx, DWORD PTR __Format$[ebp]
	push	ecx
	mov	esi, esp
	push	1
	call	DWORD PTR __imp____acrt_iob_func
	add	esp, 4
	cmp	esi, esp
	call	__RTC_CheckEsp
	push	eax
	call	__vfprintf_l
	add	esp, 16					; 00000010H
	mov	DWORD PTR __Result$[ebp], eax

; 959  :         __crt_va_end(_ArgList);

	mov	DWORD PTR __ArgList$[ebp], 0

; 960  :         return _Result;

	mov	eax, DWORD PTR __Result$[ebp]

; 961  :     }

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 228				; 000000e4H
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	0
_printf	ENDP
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File c:\program files (x86)\windows kits\10\include\10.0.17134.0\ucrt\stdio.h
;	COMDAT __vfprintf_l
_TEXT	SEGMENT
__Stream$ = 8						; size = 4
__Format$ = 12						; size = 4
__Locale$ = 16						; size = 4
__ArgList$ = 20						; size = 4
__vfprintf_l PROC					; COMDAT

; 642  :     {

	push	ebp
	mov	ebp, esp
	sub	esp, 192				; 000000c0H
	push	ebx
	push	esi
	push	edi
	lea	edi, DWORD PTR [ebp-192]
	mov	ecx, 48					; 00000030H
	mov	eax, -858993460				; ccccccccH
	rep stosd

; 643  :         return __stdio_common_vfprintf(_CRT_INTERNAL_LOCAL_PRINTF_OPTIONS, _Stream, _Format, _Locale, _ArgList);

	mov	esi, esp
	mov	eax, DWORD PTR __ArgList$[ebp]
	push	eax
	mov	ecx, DWORD PTR __Locale$[ebp]
	push	ecx
	mov	edx, DWORD PTR __Format$[ebp]
	push	edx
	mov	eax, DWORD PTR __Stream$[ebp]
	push	eax
	call	___local_stdio_printf_options
	mov	ecx, DWORD PTR [eax+4]
	push	ecx
	mov	edx, DWORD PTR [eax]
	push	edx
	call	DWORD PTR __imp____stdio_common_vfprintf
	add	esp, 24					; 00000018H
	cmp	esi, esp
	call	__RTC_CheckEsp

; 644  :     }

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 192				; 000000c0H
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	0
__vfprintf_l ENDP
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File c:\program files (x86)\windows kits\10\include\10.0.17134.0\ucrt\corecrt_stdio_config.h
;	COMDAT ___local_stdio_printf_options
_TEXT	SEGMENT
___local_stdio_printf_options PROC			; COMDAT

; 85   :     {

	push	ebp
	mov	ebp, esp
	sub	esp, 192				; 000000c0H
	push	ebx
	push	esi
	push	edi
	lea	edi, DWORD PTR [ebp-192]
	mov	ecx, 48					; 00000030H
	mov	eax, -858993460				; ccccccccH
	rep stosd

; 86   :         static unsigned __int64 _OptionsStorage;
; 87   :         return &_OptionsStorage;

	mov	eax, OFFSET ?_OptionsStorage@?1??__local_stdio_printf_options@@9@4_KA ; `__local_stdio_printf_options'::`2'::_OptionsStorage

; 88   :     }

	pop	edi
	pop	esi
	pop	ebx
	mov	esp, ebp
	pop	ebp
	ret	0
___local_stdio_printf_options ENDP
_TEXT	ENDS
END
