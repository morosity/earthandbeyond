; Listing generated by Microsoft (R) Optimizing Compiler Version 19.15.26726.0 

	TITLE	D:\Repos\veg1\morveg\Net7\AuxClasses\AuxMountBoneNames.cpp
	.686P
	.XMM
	include listing.inc
	.model	flat

INCLUDELIB MSVCRTD
INCLUDELIB OLDNAMES

PUBLIC	?ClearFlags@AuxMountBoneNames@@QAEXXZ		; AuxMountBoneNames::ClearFlags
PUBLIC	?BuildPacket@AuxMountBoneNames@@QAEXPAEAAJ@Z	; AuxMountBoneNames::BuildPacket
PUBLIC	?BuildExtendedPacket@AuxMountBoneNames@@QAEXPAEAAJ@Z ; AuxMountBoneNames::BuildExtendedPacket
PUBLIC	?GetData@AuxMountBoneNames@@QAEPAU_MountBones@@XZ ; AuxMountBoneNames::GetData
PUBLIC	?GetMountBoneName@AuxMountBoneNames@@QAEPADI@Z	; AuxMountBoneNames::GetMountBoneName
PUBLIC	?SetData@AuxMountBoneNames@@QAEXPAU_MountBones@@@Z ; AuxMountBoneNames::SetData
PUBLIC	?SetMountBoneName@AuxMountBoneNames@@QAEXIPAD@Z	; AuxMountBoneNames::SetMountBoneName
EXTRN	_memset:PROC
EXTRN	?AddString@AuxBase@@IAEXPAEPADAAJ@Z:PROC	; AuxBase::AddString
EXTRN	?AddFlags@AuxBase@@IAEXPAEI0AAJ@Z:PROC		; AuxBase::AddFlags
EXTRN	?ReplaceString@AuxBase@@IAEXPAD0II@Z:PROC	; AuxBase::ReplaceString
EXTRN	?CheckFlagBit@AuxBase@@IAEII@Z:PROC		; AuxBase::CheckFlagBit
EXTRN	?CheckExtendedFlagBit1@AuxBase@@IAEII@Z:PROC	; AuxBase::CheckExtendedFlagBit1
EXTRN	__RTC_CheckEsp:PROC
EXTRN	__RTC_InitBase:PROC
EXTRN	__RTC_Shutdown:PROC
;	COMDAT rtc$TMZ
rtc$TMZ	SEGMENT
__RTC_Shutdown.rtc$TMZ DD FLAT:__RTC_Shutdown
rtc$TMZ	ENDS
;	COMDAT rtc$IMZ
rtc$IMZ	SEGMENT
__RTC_InitBase.rtc$IMZ DD FLAT:__RTC_InitBase
rtc$IMZ	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxmountbonenames.cpp
;	COMDAT ?SetMountBoneName@AuxMountBoneNames@@QAEXIPAD@Z
_TEXT	SEGMENT
_this$ = -8						; size = 4
_Index$ = 8						; size = 4
_NewMount$ = 12						; size = 4
?SetMountBoneName@AuxMountBoneNames@@QAEXIPAD@Z PROC	; AuxMountBoneNames::SetMountBoneName, COMDAT
; _this$ = ecx

; 69   : {

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 70   : 	ReplaceString(Data->MountBoneName[Index], NewMount, Index, 64);

	push	64					; 00000040H
	mov	eax, DWORD PTR _Index$[ebp]
	push	eax
	mov	ecx, DWORD PTR _NewMount$[ebp]
	push	ecx
	mov	edx, DWORD PTR _this$[ebp]
	mov	eax, DWORD PTR [edx+32]
	mov	ecx, DWORD PTR _Index$[ebp]
	shl	ecx, 6
	add	eax, ecx
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	call	?ReplaceString@AuxBase@@IAEXPAD0II@Z	; AuxBase::ReplaceString

; 71   : }

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 204				; 000000ccH
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	8
?SetMountBoneName@AuxMountBoneNames@@QAEXIPAD@Z ENDP	; AuxMountBoneNames::SetMountBoneName
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxmountbonenames.cpp
;	COMDAT ?SetData@AuxMountBoneNames@@QAEXPAU_MountBones@@@Z
_TEXT	SEGMENT
_i$1 = -20						; size = 4
_this$ = -8						; size = 4
_NewData$ = 8						; size = 4
?SetData@AuxMountBoneNames@@QAEXPAU_MountBones@@@Z PROC	; AuxMountBoneNames::SetData, COMDAT
; _this$ = ecx

; 61   : {

	push	ebp
	mov	ebp, esp
	sub	esp, 216				; 000000d8H
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-216]
	mov	ecx, 54					; 00000036H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 62   : 	for (int i=0;i<20;i++)

	mov	DWORD PTR _i$1[ebp], 0
	jmp	SHORT $LN4@SetData
$LN2@SetData:
	mov	eax, DWORD PTR _i$1[ebp]
	add	eax, 1
	mov	DWORD PTR _i$1[ebp], eax
$LN4@SetData:
	cmp	DWORD PTR _i$1[ebp], 20			; 00000014H
	jge	SHORT $LN1@SetData

; 63   : 	{
; 64   : 		ReplaceString(Data->MountBoneName[i], NewData->MountBoneName[i], i, 64);

	push	64					; 00000040H
	mov	eax, DWORD PTR _i$1[ebp]
	push	eax
	mov	ecx, DWORD PTR _i$1[ebp]
	shl	ecx, 6
	add	ecx, DWORD PTR _NewData$[ebp]
	push	ecx
	mov	edx, DWORD PTR _this$[ebp]
	mov	eax, DWORD PTR [edx+32]
	mov	ecx, DWORD PTR _i$1[ebp]
	shl	ecx, 6
	add	eax, ecx
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	call	?ReplaceString@AuxBase@@IAEXPAD0II@Z	; AuxBase::ReplaceString

; 65   : 	}

	jmp	SHORT $LN2@SetData
$LN1@SetData:

; 66   : }

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 216				; 000000d8H
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	4
?SetData@AuxMountBoneNames@@QAEXPAU_MountBones@@@Z ENDP	; AuxMountBoneNames::SetData
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxmountbonenames.cpp
;	COMDAT ?GetMountBoneName@AuxMountBoneNames@@QAEPADI@Z
_TEXT	SEGMENT
_this$ = -8						; size = 4
_Index$ = 8						; size = 4
?GetMountBoneName@AuxMountBoneNames@@QAEPADI@Z PROC	; AuxMountBoneNames::GetMountBoneName, COMDAT
; _this$ = ecx

; 54   : char * AuxMountBoneNames::GetMountBoneName(unsigned int Index)		{return Data->MountBoneName[Index];}

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx
	mov	eax, DWORD PTR _this$[ebp]
	mov	eax, DWORD PTR [eax+32]
	mov	ecx, DWORD PTR _Index$[ebp]
	shl	ecx, 6
	add	eax, ecx
	pop	edi
	pop	esi
	pop	ebx
	mov	esp, ebp
	pop	ebp
	ret	4
?GetMountBoneName@AuxMountBoneNames@@QAEPADI@Z ENDP	; AuxMountBoneNames::GetMountBoneName
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxmountbonenames.cpp
;	COMDAT ?GetData@AuxMountBoneNames@@QAEPAU_MountBones@@XZ
_TEXT	SEGMENT
_this$ = -8						; size = 4
?GetData@AuxMountBoneNames@@QAEPAU_MountBones@@XZ PROC	; AuxMountBoneNames::GetData, COMDAT
; _this$ = ecx

; 52   : _MountBones * AuxMountBoneNames::GetData()					        {return Data;}

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx
	mov	eax, DWORD PTR _this$[ebp]
	mov	eax, DWORD PTR [eax+32]
	pop	edi
	pop	esi
	pop	ebx
	mov	esp, ebp
	pop	ebp
	ret	0
?GetData@AuxMountBoneNames@@QAEPAU_MountBones@@XZ ENDP	; AuxMountBoneNames::GetData
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxmountbonenames.cpp
;	COMDAT ?BuildExtendedPacket@AuxMountBoneNames@@QAEXPAEAAJ@Z
_TEXT	SEGMENT
_i$1 = -20						; size = 4
_this$ = -8						; size = 4
_buffer$ = 8						; size = 4
_index$ = 12						; size = 4
?BuildExtendedPacket@AuxMountBoneNames@@QAEXPAEAAJ@Z PROC ; AuxMountBoneNames::BuildExtendedPacket, COMDAT
; _this$ = ecx

; 36   : {

	push	ebp
	mov	ebp, esp
	sub	esp, 216				; 000000d8H
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-216]
	mov	ecx, 54					; 00000036H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 37   : 	AddFlags(ExtendedFlags, sizeof(ExtendedFlags), buffer, index);

	mov	eax, DWORD PTR _index$[ebp]
	push	eax
	mov	ecx, DWORD PTR _buffer$[ebp]
	push	ecx
	push	6
	mov	edx, DWORD PTR _this$[ebp]
	add	edx, 39					; 00000027H
	push	edx
	mov	ecx, DWORD PTR _this$[ebp]
	call	?AddFlags@AuxBase@@IAEXPAEI0AAJ@Z	; AuxBase::AddFlags

; 38   : 
; 39   : 	for (int i=0;i<20;i++)

	mov	DWORD PTR _i$1[ebp], 0
	jmp	SHORT $LN4@BuildExten
$LN2@BuildExten:
	mov	eax, DWORD PTR _i$1[ebp]
	add	eax, 1
	mov	DWORD PTR _i$1[ebp], eax
$LN4@BuildExten:
	cmp	DWORD PTR _i$1[ebp], 20			; 00000014H
	jge	SHORT $LN1@BuildExten

; 40   : 	{
; 41   : 		if (CheckExtendedFlagBit1(i))

	mov	eax, DWORD PTR _i$1[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	call	?CheckExtendedFlagBit1@AuxBase@@IAEII@Z	; AuxBase::CheckExtendedFlagBit1
	test	eax, eax
	je	SHORT $LN5@BuildExten

; 42   : 		{
; 43   : 			AddString(buffer, Data->MountBoneName[i], index);

	mov	eax, DWORD PTR _index$[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	mov	edx, DWORD PTR [ecx+32]
	mov	eax, DWORD PTR _i$1[ebp]
	shl	eax, 6
	add	edx, eax
	push	edx
	mov	ecx, DWORD PTR _buffer$[ebp]
	push	ecx
	mov	ecx, DWORD PTR _this$[ebp]
	call	?AddString@AuxBase@@IAEXPAEPADAAJ@Z	; AuxBase::AddString
$LN5@BuildExten:

; 44   : 		}
; 45   : 	}

	jmp	SHORT $LN2@BuildExten
$LN1@BuildExten:

; 46   : }

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 216				; 000000d8H
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	8
?BuildExtendedPacket@AuxMountBoneNames@@QAEXPAEAAJ@Z ENDP ; AuxMountBoneNames::BuildExtendedPacket
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxmountbonenames.cpp
;	COMDAT ?BuildPacket@AuxMountBoneNames@@QAEXPAEAAJ@Z
_TEXT	SEGMENT
_i$1 = -20						; size = 4
_this$ = -8						; size = 4
_buffer$ = 8						; size = 4
_index$ = 12						; size = 4
?BuildPacket@AuxMountBoneNames@@QAEXPAEAAJ@Z PROC	; AuxMountBoneNames::BuildPacket, COMDAT
; _this$ = ecx

; 21   : {

	push	ebp
	mov	ebp, esp
	sub	esp, 216				; 000000d8H
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-216]
	mov	ecx, 54					; 00000036H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 22   : 	AddFlags(Flags, sizeof(Flags), buffer, index);

	mov	eax, DWORD PTR _index$[ebp]
	push	eax
	mov	ecx, DWORD PTR _buffer$[ebp]
	push	ecx
	push	3
	mov	edx, DWORD PTR _this$[ebp]
	add	edx, 36					; 00000024H
	push	edx
	mov	ecx, DWORD PTR _this$[ebp]
	call	?AddFlags@AuxBase@@IAEXPAEI0AAJ@Z	; AuxBase::AddFlags

; 23   : 
; 24   : 	for (int i=0;i<20;i++)

	mov	DWORD PTR _i$1[ebp], 0
	jmp	SHORT $LN4@BuildPacke
$LN2@BuildPacke:
	mov	eax, DWORD PTR _i$1[ebp]
	add	eax, 1
	mov	DWORD PTR _i$1[ebp], eax
$LN4@BuildPacke:
	cmp	DWORD PTR _i$1[ebp], 20			; 00000014H
	jge	SHORT $LN3@BuildPacke

; 25   : 	{
; 26   : 		if (CheckFlagBit(i))

	mov	eax, DWORD PTR _i$1[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	call	?CheckFlagBit@AuxBase@@IAEII@Z		; AuxBase::CheckFlagBit
	test	eax, eax
	je	SHORT $LN5@BuildPacke

; 27   : 		{
; 28   : 			AddString(buffer, Data->MountBoneName[i], index);

	mov	eax, DWORD PTR _index$[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	mov	edx, DWORD PTR [ecx+32]
	mov	eax, DWORD PTR _i$1[ebp]
	shl	eax, 6
	add	edx, eax
	push	edx
	mov	ecx, DWORD PTR _buffer$[ebp]
	push	ecx
	mov	ecx, DWORD PTR _this$[ebp]
	call	?AddString@AuxBase@@IAEXPAEPADAAJ@Z	; AuxBase::AddString
$LN5@BuildPacke:

; 29   : 		}
; 30   : 	}

	jmp	SHORT $LN2@BuildPacke
$LN3@BuildPacke:

; 31   : 
; 32   : 	memset(Flags,0,sizeof(Flags));

	push	3
	push	0
	mov	eax, DWORD PTR _this$[ebp]
	add	eax, 36					; 00000024H
	push	eax
	call	_memset
	add	esp, 12					; 0000000cH

; 33   : }

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 216				; 000000d8H
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	8
?BuildPacket@AuxMountBoneNames@@QAEXPAEAAJ@Z ENDP	; AuxMountBoneNames::BuildPacket
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxmountbonenames.cpp
;	COMDAT ?ClearFlags@AuxMountBoneNames@@QAEXXZ
_TEXT	SEGMENT
_this$ = -8						; size = 4
?ClearFlags@AuxMountBoneNames@@QAEXXZ PROC		; AuxMountBoneNames::ClearFlags, COMDAT
; _this$ = ecx

; 78   : {

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 79   : 	memset(Flags,0,sizeof(Flags));

	push	3
	push	0
	mov	eax, DWORD PTR _this$[ebp]
	add	eax, 36					; 00000024H
	push	eax
	call	_memset
	add	esp, 12					; 0000000cH

; 80   : }

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 204				; 000000ccH
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	0
?ClearFlags@AuxMountBoneNames@@QAEXXZ ENDP		; AuxMountBoneNames::ClearFlags
_TEXT	ENDS
END
