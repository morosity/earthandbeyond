; Listing generated by Microsoft (R) Optimizing Compiler Version 19.15.26726.0 

	TITLE	D:\Repos\veg1\morveg\Net7\AuxClasses\AuxLego.cpp
	.686P
	.XMM
	include listing.inc
	.model	flat

INCLUDELIB MSVCRTD
INCLUDELIB OLDNAMES

PUBLIC	___local_stdio_printf_options
PUBLIC	__vfprintf_l
PUBLIC	_printf
PUBLIC	?ClearFlags@AuxLego@@QAEXXZ			; AuxLego::ClearFlags
PUBLIC	?BuildPacket@AuxLego@@QAEXPAEAAJ@Z		; AuxLego::BuildPacket
PUBLIC	?BuildExtendedPacket@AuxLego@@QAEXPAEAAJ@Z	; AuxLego::BuildExtendedPacket
PUBLIC	?BuildSpecialPacket@AuxLego@@QAEXPAEAAJ@Z	; AuxLego::BuildSpecialPacket
PUBLIC	?GetData@AuxLego@@QAEPAU_Lego@@XZ		; AuxLego::GetData
PUBLIC	?GetScale@AuxLego@@QAEMXZ			; AuxLego::GetScale
PUBLIC	?SetData@AuxLego@@QAEXPAU_Lego@@@Z		; AuxLego::SetData
PUBLIC	?SetScale@AuxLego@@QAEXM@Z			; AuxLego::SetScale
PUBLIC	??$AddData@M@AuxBase@@IAEXPAEMAAJ@Z		; AuxBase::AddData<float>
PUBLIC	??$AddData@D@AuxBase@@IAEXPAEDAAJ@Z		; AuxBase::AddData<char>
PUBLIC	??$ReplaceData@M@AuxBase@@IAEXAAMMI@Z		; AuxBase::ReplaceData<float>
PUBLIC	?_OptionsStorage@?1??__local_stdio_printf_options@@9@4_KA ; `__local_stdio_printf_options'::`2'::_OptionsStorage
PUBLIC	??_C@_0BO@FPOJMAIA@Error?3?5Bufferoverflow?5in?5Aux?$CB@ ; `string'
PUBLIC	__real@00000000
EXTRN	_memset:PROC
EXTRN	__imp____acrt_iob_func:PROC
EXTRN	__imp____stdio_common_vfprintf:PROC
EXTRN	?Lock@Mutex@@QAEXXZ:PROC			; Mutex::Lock
EXTRN	?Unlock@Mutex@@QAEXXZ:PROC			; Mutex::Unlock
EXTRN	?AddFlags@AuxBase@@IAEXPAEI0AAJ@Z:PROC		; AuxBase::AddFlags
EXTRN	?SetParentFlag@AuxBase@@IAEXXZ:PROC		; AuxBase::SetParentFlag
EXTRN	?SetParentExtendedFlag@AuxBase@@IAEXI@Z:PROC	; AuxBase::SetParentExtendedFlag
EXTRN	?SetAuxBit@AuxBase@@AAEXPAEI@Z:PROC		; AuxBase::SetAuxBit
EXTRN	?UnsetAuxBit@AuxBase@@AAEXPAEI@Z:PROC		; AuxBase::UnsetAuxBit
EXTRN	?ClearFlags@AuxAttachments@@QAEXXZ:PROC		; AuxAttachments::ClearFlags
EXTRN	?BuildPacket@AuxAttachments@@QAEXPAEAAJ@Z:PROC	; AuxAttachments::BuildPacket
EXTRN	?BuildExtendedPacket@AuxAttachments@@QAEXPAEAAJ@Z:PROC ; AuxAttachments::BuildExtendedPacket
EXTRN	?BuildSpecialPacket@AuxAttachments@@QAEXPAEAAJ@Z:PROC ; AuxAttachments::BuildSpecialPacket
EXTRN	?SetData@AuxAttachments@@QAEXPAU_Attachments@@@Z:PROC ; AuxAttachments::SetData
EXTRN	__RTC_CheckEsp:PROC
EXTRN	__RTC_InitBase:PROC
EXTRN	__RTC_Shutdown:PROC
EXTRN	__fltused:DWORD
;	COMDAT ?_OptionsStorage@?1??__local_stdio_printf_options@@9@4_KA
_BSS	SEGMENT
?_OptionsStorage@?1??__local_stdio_printf_options@@9@4_KA DQ 01H DUP (?) ; `__local_stdio_printf_options'::`2'::_OptionsStorage
_BSS	ENDS
;	COMDAT __real@00000000
CONST	SEGMENT
__real@00000000 DD 000000000r			; 0
CONST	ENDS
;	COMDAT rtc$TMZ
rtc$TMZ	SEGMENT
__RTC_Shutdown.rtc$TMZ DD FLAT:__RTC_Shutdown
rtc$TMZ	ENDS
;	COMDAT rtc$IMZ
rtc$IMZ	SEGMENT
__RTC_InitBase.rtc$IMZ DD FLAT:__RTC_InitBase
rtc$IMZ	ENDS
;	COMDAT ??_C@_0BO@FPOJMAIA@Error?3?5Bufferoverflow?5in?5Aux?$CB@
CONST	SEGMENT
??_C@_0BO@FPOJMAIA@Error?3?5Bufferoverflow?5in?5Aux?$CB@ DB 'Error: Buffe'
	DB	'roverflow in Aux!', 00H			; `string'
CONST	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxbase.h
;	COMDAT ??$ReplaceData@M@AuxBase@@IAEXAAMMI@Z
_TEXT	SEGMENT
_this$ = -8						; size = 4
_orig$ = 8						; size = 4
_src$ = 12						; size = 4
_flagNum$ = 16						; size = 4
??$ReplaceData@M@AuxBase@@IAEXAAMMI@Z PROC		; AuxBase::ReplaceData<float>, COMDAT
; _this$ = ecx

; 51   : 	{

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 52   :         m_Mutex->Lock();

	mov	eax, DWORD PTR _this$[ebp]
	mov	ecx, DWORD PTR [eax+28]
	call	?Lock@Mutex@@QAEXXZ			; Mutex::Lock

; 53   : 
; 54   : 		if (orig != src)

	mov	eax, DWORD PTR _orig$[ebp]
	movss	xmm0, DWORD PTR [eax]
	ucomiss	xmm0, DWORD PTR _src$[ebp]
	lahf
	test	ah, 68					; 00000044H
	jnp	$LN2@ReplaceDat

; 55   : 		{
; 56   : 			/* The data is different, set the flags */
; 57   : 			SetAuxBit(m_Flags, flagNum);

	mov	eax, DWORD PTR _flagNum$[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	mov	edx, DWORD PTR [ecx+12]
	push	edx
	mov	ecx, DWORD PTR _this$[ebp]
	call	?SetAuxBit@AuxBase@@AAEXPAEI@Z		; AuxBase::SetAuxBit

; 58   : 
; 59   :             /* This eliminates useless recursion */
; 60   :             if ((m_Flags[0] & 0x02) == 0)

	mov	eax, 1
	imul	ecx, eax, 0
	mov	edx, DWORD PTR _this$[ebp]
	mov	eax, DWORD PTR [edx+12]
	movzx	ecx, BYTE PTR [eax+ecx]
	and	ecx, 2
	jne	SHORT $LN3@ReplaceDat

; 61   :             {
; 62   : 			    m_Flags[0] |= 0x02;

	mov	eax, 1
	imul	ecx, eax, 0
	mov	edx, DWORD PTR _this$[ebp]
	mov	eax, DWORD PTR [edx+12]
	movzx	ecx, BYTE PTR [eax+ecx]
	or	ecx, 2
	mov	edx, 1
	imul	eax, edx, 0
	mov	edx, DWORD PTR _this$[ebp]
	mov	edx, DWORD PTR [edx+12]
	mov	BYTE PTR [eax+edx], cl

; 63   : 			    SetParentFlag();

	mov	ecx, DWORD PTR _this$[ebp]
	call	?SetParentFlag@AuxBase@@IAEXXZ		; AuxBase::SetParentFlag

; 64   : 				SetParentExtendedFlag(1);

	push	1
	mov	ecx, DWORD PTR _this$[ebp]
	call	?SetParentExtendedFlag@AuxBase@@IAEXI@Z	; AuxBase::SetParentExtendedFlag
$LN3@ReplaceDat:

; 65   :             }
; 66   : 
; 67   : 			/* Change the extended flags for this bit if needed */
; 68   : 			if (!orig && src)

	mov	eax, DWORD PTR _orig$[ebp]
	movss	xmm0, DWORD PTR [eax]
	ucomiss	xmm0, DWORD PTR __real@00000000
	lahf
	test	ah, 68					; 00000044H
	jp	SHORT $LN4@ReplaceDat
	movss	xmm0, DWORD PTR _src$[ebp]
	ucomiss	xmm0, DWORD PTR __real@00000000
	lahf
	test	ah, 68					; 00000044H
	jnp	SHORT $LN4@ReplaceDat

; 69   : 			{
; 70   : 				UnsetAuxBit(m_ExtendedFlags, flagNum + m_FlagCount);

	mov	eax, DWORD PTR _this$[ebp]
	mov	ecx, DWORD PTR _flagNum$[ebp]
	add	ecx, DWORD PTR [eax+8]
	push	ecx
	mov	edx, DWORD PTR _this$[ebp]
	mov	eax, DWORD PTR [edx+16]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	call	?UnsetAuxBit@AuxBase@@AAEXPAEI@Z	; AuxBase::UnsetAuxBit

; 71   : 				SetAuxBit(m_ExtendedFlags, flagNum);

	mov	eax, DWORD PTR _flagNum$[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	mov	edx, DWORD PTR [ecx+16]
	push	edx
	mov	ecx, DWORD PTR _this$[ebp]
	call	?SetAuxBit@AuxBase@@AAEXPAEI@Z		; AuxBase::SetAuxBit
	jmp	SHORT $LN6@ReplaceDat
$LN4@ReplaceDat:

; 72   : 			}
; 73   : 			else if (!src && orig)

	movss	xmm0, DWORD PTR _src$[ebp]
	ucomiss	xmm0, DWORD PTR __real@00000000
	lahf
	test	ah, 68					; 00000044H
	jp	SHORT $LN6@ReplaceDat
	mov	eax, DWORD PTR _orig$[ebp]
	movss	xmm0, DWORD PTR [eax]
	ucomiss	xmm0, DWORD PTR __real@00000000
	lahf
	test	ah, 68					; 00000044H
	jnp	SHORT $LN6@ReplaceDat

; 74   : 			{
; 75   : 				SetAuxBit(m_ExtendedFlags, flagNum + m_FlagCount);

	mov	eax, DWORD PTR _this$[ebp]
	mov	ecx, DWORD PTR _flagNum$[ebp]
	add	ecx, DWORD PTR [eax+8]
	push	ecx
	mov	edx, DWORD PTR _this$[ebp]
	mov	eax, DWORD PTR [edx+16]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	call	?SetAuxBit@AuxBase@@AAEXPAEI@Z		; AuxBase::SetAuxBit

; 76   : 				UnsetAuxBit(m_ExtendedFlags, flagNum);

	mov	eax, DWORD PTR _flagNum$[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	mov	edx, DWORD PTR [ecx+16]
	push	edx
	mov	ecx, DWORD PTR _this$[ebp]
	call	?UnsetAuxBit@AuxBase@@AAEXPAEI@Z	; AuxBase::UnsetAuxBit
$LN6@ReplaceDat:

; 77   : 			}
; 78   : 
; 79   : 			/* Copy the data */
; 80   : 			orig = src;

	mov	eax, DWORD PTR _orig$[ebp]
	movss	xmm0, DWORD PTR _src$[ebp]
	movss	DWORD PTR [eax], xmm0
$LN2@ReplaceDat:

; 81   : 		}
; 82   : 
; 83   :         m_Mutex->Unlock();

	mov	eax, DWORD PTR _this$[ebp]
	mov	ecx, DWORD PTR [eax+28]
	call	?Unlock@Mutex@@QAEXXZ			; Mutex::Unlock

; 84   : 	}

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 204				; 000000ccH
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	12					; 0000000cH
??$ReplaceData@M@AuxBase@@IAEXAAMMI@Z ENDP		; AuxBase::ReplaceData<float>
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxbase.h
;	COMDAT ??$AddData@D@AuxBase@@IAEXPAEDAAJ@Z
_TEXT	SEGMENT
_this$ = -8						; size = 4
_buffer$ = 8						; size = 4
_data$ = 12						; size = 1
_index$ = 16						; size = 4
??$AddData@D@AuxBase@@IAEXPAEDAAJ@Z PROC		; AuxBase::AddData<char>, COMDAT
; _this$ = ecx

; 34   : 	{

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 35   : 		if (sizeof(T) + index < m_Max_Buffer)

	mov	eax, DWORD PTR _index$[ebp]
	mov	ecx, DWORD PTR [eax]
	add	ecx, 1
	mov	edx, DWORD PTR _this$[ebp]
	cmp	ecx, DWORD PTR [edx+20]
	jae	SHORT $LN2@AddData

; 36   : 		{
; 37   : 			*((T *) &buffer[index]) = data;

	mov	eax, DWORD PTR _index$[ebp]
	mov	ecx, DWORD PTR [eax]
	mov	edx, DWORD PTR _buffer$[ebp]
	mov	al, BYTE PTR _data$[ebp]
	mov	BYTE PTR [edx+ecx], al

; 38   : 			index += sizeof(T);

	mov	eax, DWORD PTR _index$[ebp]
	mov	ecx, DWORD PTR [eax]
	add	ecx, 1
	mov	edx, DWORD PTR _index$[ebp]
	mov	DWORD PTR [edx], ecx

; 39   : 		}
; 40   : 		else

	jmp	SHORT $LN1@AddData
$LN2@AddData:

; 41   : 		{
; 42   : 			printf("Error: Bufferoverflow in Aux!");

	push	OFFSET ??_C@_0BO@FPOJMAIA@Error?3?5Bufferoverflow?5in?5Aux?$CB@
	call	_printf
	add	esp, 4
$LN1@AddData:

; 43   : 		}
; 44   : 	}

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 204				; 000000ccH
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	12					; 0000000cH
??$AddData@D@AuxBase@@IAEXPAEDAAJ@Z ENDP		; AuxBase::AddData<char>
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxbase.h
;	COMDAT ??$AddData@M@AuxBase@@IAEXPAEMAAJ@Z
_TEXT	SEGMENT
_this$ = -8						; size = 4
_buffer$ = 8						; size = 4
_data$ = 12						; size = 4
_index$ = 16						; size = 4
??$AddData@M@AuxBase@@IAEXPAEMAAJ@Z PROC		; AuxBase::AddData<float>, COMDAT
; _this$ = ecx

; 34   : 	{

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 35   : 		if (sizeof(T) + index < m_Max_Buffer)

	mov	eax, DWORD PTR _index$[ebp]
	mov	ecx, DWORD PTR [eax]
	add	ecx, 4
	mov	edx, DWORD PTR _this$[ebp]
	cmp	ecx, DWORD PTR [edx+20]
	jae	SHORT $LN2@AddData

; 36   : 		{
; 37   : 			*((T *) &buffer[index]) = data;

	mov	eax, DWORD PTR _index$[ebp]
	mov	ecx, DWORD PTR [eax]
	mov	edx, DWORD PTR _buffer$[ebp]
	movss	xmm0, DWORD PTR _data$[ebp]
	movss	DWORD PTR [edx+ecx], xmm0

; 38   : 			index += sizeof(T);

	mov	eax, DWORD PTR _index$[ebp]
	mov	ecx, DWORD PTR [eax]
	add	ecx, 4
	mov	edx, DWORD PTR _index$[ebp]
	mov	DWORD PTR [edx], ecx

; 39   : 		}
; 40   : 		else

	jmp	SHORT $LN3@AddData
$LN2@AddData:

; 41   : 		{
; 42   : 			printf("Error: Bufferoverflow in Aux!");

	push	OFFSET ??_C@_0BO@FPOJMAIA@Error?3?5Bufferoverflow?5in?5Aux?$CB@
	call	_printf
	add	esp, 4
$LN3@AddData:

; 43   : 		}
; 44   : 	}

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 204				; 000000ccH
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	12					; 0000000cH
??$AddData@M@AuxBase@@IAEXPAEMAAJ@Z ENDP		; AuxBase::AddData<float>
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxlego.cpp
;	COMDAT ?SetScale@AuxLego@@QAEXM@Z
_TEXT	SEGMENT
_this$ = -8						; size = 4
_NewScale$ = 8						; size = 4
?SetScale@AuxLego@@QAEXM@Z PROC				; AuxLego::SetScale, COMDAT
; _this$ = ecx

; 90   : {

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 91   : 	ReplaceData(Data->Scale, NewScale, 0);

	push	0
	push	ecx
	movss	xmm0, DWORD PTR _NewScale$[ebp]
	movss	DWORD PTR [esp], xmm0
	mov	eax, DWORD PTR _this$[ebp]
	mov	ecx, DWORD PTR [eax+32]
	push	ecx
	mov	ecx, DWORD PTR _this$[ebp]
	call	??$ReplaceData@M@AuxBase@@IAEXAAMMI@Z	; AuxBase::ReplaceData<float>

; 92   : }

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 204				; 000000ccH
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	4
?SetScale@AuxLego@@QAEXM@Z ENDP				; AuxLego::SetScale
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxlego.cpp
;	COMDAT ?SetData@AuxLego@@QAEXPAU_Lego@@@Z
_TEXT	SEGMENT
_this$ = -8						; size = 4
_NewData$ = 8						; size = 4
?SetData@AuxLego@@QAEXPAU_Lego@@@Z PROC			; AuxLego::SetData, COMDAT
; _this$ = ecx

; 84   : {

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 85   : 	ReplaceData(Data->Scale, NewData->Scale, 0);

	push	0
	mov	eax, DWORD PTR _NewData$[ebp]
	push	ecx
	movss	xmm0, DWORD PTR [eax]
	movss	DWORD PTR [esp], xmm0
	mov	ecx, DWORD PTR _this$[ebp]
	mov	edx, DWORD PTR [ecx+32]
	push	edx
	mov	ecx, DWORD PTR _this$[ebp]
	call	??$ReplaceData@M@AuxBase@@IAEXAAMMI@Z	; AuxBase::ReplaceData<float>

; 86   : 	Attachments.SetData(&NewData->Attachments);

	mov	eax, DWORD PTR _NewData$[ebp]
	add	eax, 4
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	add	ecx, 38					; 00000026H
	call	?SetData@AuxAttachments@@QAEXPAU_Attachments@@@Z ; AuxAttachments::SetData

; 87   : }

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 204				; 000000ccH
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	4
?SetData@AuxLego@@QAEXPAU_Lego@@@Z ENDP			; AuxLego::SetData
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxlego.cpp
;	COMDAT ?GetScale@AuxLego@@QAEMXZ
_TEXT	SEGMENT
_this$ = -8						; size = 4
?GetScale@AuxLego@@QAEMXZ PROC				; AuxLego::GetScale, COMDAT
; _this$ = ecx

; 77   : float AuxLego::GetScale()		{return Data->Scale;}

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx
	mov	eax, DWORD PTR _this$[ebp]
	mov	ecx, DWORD PTR [eax+32]
	fld	DWORD PTR [ecx]
	pop	edi
	pop	esi
	pop	ebx
	mov	esp, ebp
	pop	ebp
	ret	0
?GetScale@AuxLego@@QAEMXZ ENDP				; AuxLego::GetScale
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxlego.cpp
;	COMDAT ?GetData@AuxLego@@QAEPAU_Lego@@XZ
_TEXT	SEGMENT
_this$ = -8						; size = 4
?GetData@AuxLego@@QAEPAU_Lego@@XZ PROC			; AuxLego::GetData, COMDAT
; _this$ = ecx

; 75   : _Lego * AuxLego::GetData()      {return Data;}

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx
	mov	eax, DWORD PTR _this$[ebp]
	mov	eax, DWORD PTR [eax+32]
	pop	edi
	pop	esi
	pop	ebx
	mov	esp, ebp
	pop	ebp
	ret	0
?GetData@AuxLego@@QAEPAU_Lego@@XZ ENDP			; AuxLego::GetData
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxlego.cpp
;	COMDAT ?BuildSpecialPacket@AuxLego@@QAEXPAEAAJ@Z
_TEXT	SEGMENT
_this$ = -8						; size = 4
_buffer$ = 8						; size = 4
_index$ = 12						; size = 4
?BuildSpecialPacket@AuxLego@@QAEXPAEAAJ@Z PROC		; AuxLego::BuildSpecialPacket, COMDAT
; _this$ = ecx

; 57   : {

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 58   : 	AddFlags(Flags, sizeof(Flags), buffer, index);

	mov	eax, DWORD PTR _index$[ebp]
	push	eax
	mov	ecx, DWORD PTR _buffer$[ebp]
	push	ecx
	push	1
	mov	edx, DWORD PTR _this$[ebp]
	add	edx, 36					; 00000024H
	push	edx
	mov	ecx, DWORD PTR _this$[ebp]
	call	?AddFlags@AuxBase@@IAEXPAEI0AAJ@Z	; AuxBase::AddFlags

; 59   : 
; 60   : 	if (Flags[0] & 0x10)

	mov	eax, 1
	imul	ecx, eax, 0
	mov	edx, DWORD PTR _this$[ebp]
	movzx	eax, BYTE PTR [edx+ecx+36]
	and	eax, 16					; 00000010H
	je	SHORT $LN2@BuildSpeci

; 61   : 	{
; 62   : 		AddData(buffer, Data->Scale, index);

	mov	eax, DWORD PTR _index$[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	mov	edx, DWORD PTR [ecx+32]
	push	ecx
	movss	xmm0, DWORD PTR [edx]
	movss	DWORD PTR [esp], xmm0
	mov	eax, DWORD PTR _buffer$[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	call	??$AddData@M@AuxBase@@IAEXPAEMAAJ@Z	; AuxBase::AddData<float>
$LN2@BuildSpeci:

; 63   : 	}
; 64   : 
; 65   : 	if (Flags[0] & 0x20)

	mov	eax, 1
	imul	ecx, eax, 0
	mov	edx, DWORD PTR _this$[ebp]
	movzx	eax, BYTE PTR [edx+ecx+36]
	and	eax, 32					; 00000020H
	je	SHORT $LN3@BuildSpeci

; 66   : 	{
; 67   : 		Attachments.BuildSpecialPacket(buffer, index);

	mov	eax, DWORD PTR _index$[ebp]
	push	eax
	mov	ecx, DWORD PTR _buffer$[ebp]
	push	ecx
	mov	ecx, DWORD PTR _this$[ebp]
	add	ecx, 38					; 00000026H
	call	?BuildSpecialPacket@AuxAttachments@@QAEXPAEAAJ@Z ; AuxAttachments::BuildSpecialPacket
$LN3@BuildSpeci:

; 68   : 	}
; 69   : }

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 204				; 000000ccH
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	8
?BuildSpecialPacket@AuxLego@@QAEXPAEAAJ@Z ENDP		; AuxLego::BuildSpecialPacket
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxlego.cpp
;	COMDAT ?BuildExtendedPacket@AuxLego@@QAEXPAEAAJ@Z
_TEXT	SEGMENT
_this$ = -8						; size = 4
_buffer$ = 8						; size = 4
_index$ = 12						; size = 4
?BuildExtendedPacket@AuxLego@@QAEXPAEAAJ@Z PROC		; AuxLego::BuildExtendedPacket, COMDAT
; _this$ = ecx

; 38   : {

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 39   : 	AddFlags(ExtendedFlags, sizeof(ExtendedFlags), buffer, index);

	mov	eax, DWORD PTR _index$[ebp]
	push	eax
	mov	ecx, DWORD PTR _buffer$[ebp]
	push	ecx
	push	1
	mov	edx, DWORD PTR _this$[ebp]
	add	edx, 37					; 00000025H
	push	edx
	mov	ecx, DWORD PTR _this$[ebp]
	call	?AddFlags@AuxBase@@IAEXPAEI0AAJ@Z	; AuxBase::AddFlags

; 40   : 
; 41   : 	if (ExtendedFlags[0] & 0x10)

	mov	eax, 1
	imul	ecx, eax, 0
	mov	edx, DWORD PTR _this$[ebp]
	movzx	eax, BYTE PTR [edx+ecx+37]
	and	eax, 16					; 00000010H
	je	SHORT $LN2@BuildExten

; 42   : 	{
; 43   : 		AddData(buffer, Data->Scale, index);

	mov	eax, DWORD PTR _index$[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	mov	edx, DWORD PTR [ecx+32]
	push	ecx
	movss	xmm0, DWORD PTR [edx]
	movss	DWORD PTR [esp], xmm0
	mov	eax, DWORD PTR _buffer$[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	call	??$AddData@M@AuxBase@@IAEXPAEMAAJ@Z	; AuxBase::AddData<float>
$LN2@BuildExten:

; 44   : 	}
; 45   : 
; 46   : 	if (ExtendedFlags[0] & 0x20)

	mov	eax, 1
	imul	ecx, eax, 0
	mov	edx, DWORD PTR _this$[ebp]
	movzx	eax, BYTE PTR [edx+ecx+37]
	and	eax, 32					; 00000020H
	je	SHORT $LN3@BuildExten

; 47   : 	{
; 48   : 		Attachments.BuildExtendedPacket(buffer, index);

	mov	eax, DWORD PTR _index$[ebp]
	push	eax
	mov	ecx, DWORD PTR _buffer$[ebp]
	push	ecx
	mov	ecx, DWORD PTR _this$[ebp]
	add	ecx, 38					; 00000026H
	call	?BuildExtendedPacket@AuxAttachments@@QAEXPAEAAJ@Z ; AuxAttachments::BuildExtendedPacket
	jmp	SHORT $LN5@BuildExten
$LN3@BuildExten:

; 49   : 	}
; 50   : 	else if (ExtendedFlags[0] & 0x80)

	mov	eax, 1
	imul	ecx, eax, 0
	mov	edx, DWORD PTR _this$[ebp]
	movzx	eax, BYTE PTR [edx+ecx+37]
	and	eax, 128				; 00000080H
	je	SHORT $LN5@BuildExten

; 51   : 	{
; 52   : 		AddData(buffer, char(0x05), index);

	mov	eax, DWORD PTR _index$[ebp]
	push	eax
	push	5
	mov	ecx, DWORD PTR _buffer$[ebp]
	push	ecx
	mov	ecx, DWORD PTR _this$[ebp]
	call	??$AddData@D@AuxBase@@IAEXPAEDAAJ@Z	; AuxBase::AddData<char>
$LN5@BuildExten:

; 53   : 	}
; 54   : }

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 204				; 000000ccH
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	8
?BuildExtendedPacket@AuxLego@@QAEXPAEAAJ@Z ENDP		; AuxLego::BuildExtendedPacket
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxlego.cpp
;	COMDAT ?BuildPacket@AuxLego@@QAEXPAEAAJ@Z
_TEXT	SEGMENT
_this$ = -8						; size = 4
_buffer$ = 8						; size = 4
_index$ = 12						; size = 4
?BuildPacket@AuxLego@@QAEXPAEAAJ@Z PROC			; AuxLego::BuildPacket, COMDAT
; _this$ = ecx

; 21   : {

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 22   : 	AddFlags(Flags, sizeof(Flags), buffer, index);

	mov	eax, DWORD PTR _index$[ebp]
	push	eax
	mov	ecx, DWORD PTR _buffer$[ebp]
	push	ecx
	push	1
	mov	edx, DWORD PTR _this$[ebp]
	add	edx, 36					; 00000024H
	push	edx
	mov	ecx, DWORD PTR _this$[ebp]
	call	?AddFlags@AuxBase@@IAEXPAEI0AAJ@Z	; AuxBase::AddFlags

; 23   : 
; 24   : 	if (Flags[0] & 0x10)

	mov	eax, 1
	imul	ecx, eax, 0
	mov	edx, DWORD PTR _this$[ebp]
	movzx	eax, BYTE PTR [edx+ecx+36]
	and	eax, 16					; 00000010H
	je	SHORT $LN2@BuildPacke

; 25   : 	{
; 26   : 		AddData(buffer, Data->Scale, index);

	mov	eax, DWORD PTR _index$[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	mov	edx, DWORD PTR [ecx+32]
	push	ecx
	movss	xmm0, DWORD PTR [edx]
	movss	DWORD PTR [esp], xmm0
	mov	eax, DWORD PTR _buffer$[ebp]
	push	eax
	mov	ecx, DWORD PTR _this$[ebp]
	call	??$AddData@M@AuxBase@@IAEXPAEMAAJ@Z	; AuxBase::AddData<float>
$LN2@BuildPacke:

; 27   : 	}
; 28   : 
; 29   : 	if (Flags[0] & 0x20)

	mov	eax, 1
	imul	ecx, eax, 0
	mov	edx, DWORD PTR _this$[ebp]
	movzx	eax, BYTE PTR [edx+ecx+36]
	and	eax, 32					; 00000020H
	je	SHORT $LN3@BuildPacke

; 30   : 	{
; 31   : 		Attachments.BuildPacket(buffer, index);

	mov	eax, DWORD PTR _index$[ebp]
	push	eax
	mov	ecx, DWORD PTR _buffer$[ebp]
	push	ecx
	mov	ecx, DWORD PTR _this$[ebp]
	add	ecx, 38					; 00000026H
	call	?BuildPacket@AuxAttachments@@QAEXPAEAAJ@Z ; AuxAttachments::BuildPacket
$LN3@BuildPacke:

; 32   : 	}
; 33   : 	
; 34   :     memset(Flags,0,sizeof(Flags));

	push	1
	push	0
	mov	eax, DWORD PTR _this$[ebp]
	add	eax, 36					; 00000024H
	push	eax
	call	_memset
	add	esp, 12					; 0000000cH

; 35   : }

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 204				; 000000ccH
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	8
?BuildPacket@AuxLego@@QAEXPAEAAJ@Z ENDP			; AuxLego::BuildPacket
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File d:\repos\veg1\morveg\net7\auxclasses\auxlego.cpp
;	COMDAT ?ClearFlags@AuxLego@@QAEXXZ
_TEXT	SEGMENT
_this$ = -8						; size = 4
?ClearFlags@AuxLego@@QAEXXZ PROC			; AuxLego::ClearFlags, COMDAT
; _this$ = ecx

; 99   : {

	push	ebp
	mov	ebp, esp
	sub	esp, 204				; 000000ccH
	push	ebx
	push	esi
	push	edi
	push	ecx
	lea	edi, DWORD PTR [ebp-204]
	mov	ecx, 51					; 00000033H
	mov	eax, -858993460				; ccccccccH
	rep stosd
	pop	ecx
	mov	DWORD PTR _this$[ebp], ecx

; 100  : 	memset(Flags,0,sizeof(Flags));

	push	1
	push	0
	mov	eax, DWORD PTR _this$[ebp]
	add	eax, 36					; 00000024H
	push	eax
	call	_memset
	add	esp, 12					; 0000000cH

; 101  :     Attachments.ClearFlags();

	mov	ecx, DWORD PTR _this$[ebp]
	add	ecx, 38					; 00000026H
	call	?ClearFlags@AuxAttachments@@QAEXXZ	; AuxAttachments::ClearFlags

; 102  : }

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 204				; 000000ccH
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	0
?ClearFlags@AuxLego@@QAEXXZ ENDP			; AuxLego::ClearFlags
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File c:\program files (x86)\windows kits\10\include\10.0.17134.0\ucrt\stdio.h
;	COMDAT _printf
_TEXT	SEGMENT
__ArgList$ = -20					; size = 4
__Result$ = -8						; size = 4
__Format$ = 8						; size = 4
_printf	PROC						; COMDAT

; 954  :     {

	push	ebp
	mov	ebp, esp
	sub	esp, 228				; 000000e4H
	push	ebx
	push	esi
	push	edi
	lea	edi, DWORD PTR [ebp-228]
	mov	ecx, 57					; 00000039H
	mov	eax, -858993460				; ccccccccH
	rep stosd

; 955  :         int _Result;
; 956  :         va_list _ArgList;
; 957  :         __crt_va_start(_ArgList, _Format);

	lea	eax, DWORD PTR __Format$[ebp+4]
	mov	DWORD PTR __ArgList$[ebp], eax

; 958  :         _Result = _vfprintf_l(stdout, _Format, NULL, _ArgList);

	mov	eax, DWORD PTR __ArgList$[ebp]
	push	eax
	push	0
	mov	ecx, DWORD PTR __Format$[ebp]
	push	ecx
	mov	esi, esp
	push	1
	call	DWORD PTR __imp____acrt_iob_func
	add	esp, 4
	cmp	esi, esp
	call	__RTC_CheckEsp
	push	eax
	call	__vfprintf_l
	add	esp, 16					; 00000010H
	mov	DWORD PTR __Result$[ebp], eax

; 959  :         __crt_va_end(_ArgList);

	mov	DWORD PTR __ArgList$[ebp], 0

; 960  :         return _Result;

	mov	eax, DWORD PTR __Result$[ebp]

; 961  :     }

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 228				; 000000e4H
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	0
_printf	ENDP
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File c:\program files (x86)\windows kits\10\include\10.0.17134.0\ucrt\stdio.h
;	COMDAT __vfprintf_l
_TEXT	SEGMENT
__Stream$ = 8						; size = 4
__Format$ = 12						; size = 4
__Locale$ = 16						; size = 4
__ArgList$ = 20						; size = 4
__vfprintf_l PROC					; COMDAT

; 642  :     {

	push	ebp
	mov	ebp, esp
	sub	esp, 192				; 000000c0H
	push	ebx
	push	esi
	push	edi
	lea	edi, DWORD PTR [ebp-192]
	mov	ecx, 48					; 00000030H
	mov	eax, -858993460				; ccccccccH
	rep stosd

; 643  :         return __stdio_common_vfprintf(_CRT_INTERNAL_LOCAL_PRINTF_OPTIONS, _Stream, _Format, _Locale, _ArgList);

	mov	esi, esp
	mov	eax, DWORD PTR __ArgList$[ebp]
	push	eax
	mov	ecx, DWORD PTR __Locale$[ebp]
	push	ecx
	mov	edx, DWORD PTR __Format$[ebp]
	push	edx
	mov	eax, DWORD PTR __Stream$[ebp]
	push	eax
	call	___local_stdio_printf_options
	mov	ecx, DWORD PTR [eax+4]
	push	ecx
	mov	edx, DWORD PTR [eax]
	push	edx
	call	DWORD PTR __imp____stdio_common_vfprintf
	add	esp, 24					; 00000018H
	cmp	esi, esp
	call	__RTC_CheckEsp

; 644  :     }

	pop	edi
	pop	esi
	pop	ebx
	add	esp, 192				; 000000c0H
	cmp	ebp, esp
	call	__RTC_CheckEsp
	mov	esp, ebp
	pop	ebp
	ret	0
__vfprintf_l ENDP
_TEXT	ENDS
; Function compile flags: /Odtp /RTCsu /ZI
; File c:\program files (x86)\windows kits\10\include\10.0.17134.0\ucrt\corecrt_stdio_config.h
;	COMDAT ___local_stdio_printf_options
_TEXT	SEGMENT
___local_stdio_printf_options PROC			; COMDAT

; 85   :     {

	push	ebp
	mov	ebp, esp
	sub	esp, 192				; 000000c0H
	push	ebx
	push	esi
	push	edi
	lea	edi, DWORD PTR [ebp-192]
	mov	ecx, 48					; 00000030H
	mov	eax, -858993460				; ccccccccH
	rep stosd

; 86   :         static unsigned __int64 _OptionsStorage;
; 87   :         return &_OptionsStorage;

	mov	eax, OFFSET ?_OptionsStorage@?1??__local_stdio_printf_options@@9@4_KA ; `__local_stdio_printf_options'::`2'::_OptionsStorage

; 88   :     }

	pop	edi
	pop	esi
	pop	ebx
	mov	esp, ebp
	pop	ebp
	ret	0
___local_stdio_printf_options ENDP
_TEXT	ENDS
END
