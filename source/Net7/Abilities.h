

#ifndef _ABILITY_LIST_H_INCLUDED_
#define _ABILITY_LIST_H_INCLUDED_


// TODO: Add all Abilities
#include "Abilities\AbilityRally.h"
#include "Abilities\AbilityCloak.h"
#include "Abilities\AbilityHullPatch.h"
#include "Abilities\AbilityJumpStart.h"
#include "Abilities\AbilityRechargeShields.h"
#include "Abilities\AbilityShieldInversion.h"
#include "Abilities\AbilityShieldSap.h"
#include "Abilities\AbilityWormHole.h"
#include "Abilities\AbilityPowerDown.h"
#include "Abilities\AbilitySummon.h"
#include "Abilities\AbilityFoldSpace.h"
#include "Abilities\AbilityRepairEquipment.h"
#include "Abilities\AbilityPsionicShield.h"
#include "Abilities\AbilitySelfDestruct.h"
#include "Abilities\AbilityGravityLink.h"
#include "Abilities\AbilityEnergyLeech.h"
#include "Abilities\AbilityShieldLeech.h"
#include "Abilities\AbilityEnvironmentShield.h"
#include "Abilities\AbilityMenace.h"
#include "Abilities\AbilityEnrage.h"
#include "Abilities\AbilityRepulsorField.h"
#include "Abilities\AbilityCombatTrance.h"
#include "Abilities\AbilityHacking.h"
#include "Abilities\AbilityAfterburn.h"
#include "Abilities\AbilityShieldCharging.h"
#include "Abilities\AbilityBioRepression.h"
#include "Abilities\AbilityBefriend.h"
#include "Abilities\AbilityReactorOpt.h"
#include "Abilities\AbilityWormHoleExtended.h"  

#endif