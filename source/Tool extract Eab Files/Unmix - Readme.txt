How to use unmix.exe


What easier is. Copy the folder C:\Program Files (x86)\EA GAMES\Earth & Beyond\Data\client to another location.

and drop the file in the unmix.exe That way it will be extraced automatic and the maps are created.


Harder way

Usage:

  unmix MixFile.mix [OutPath]

  where MixFile.mix is the name of the .MIX file and OutPath is an optional
  output folder name.  If OutPath is not specified, a folder with the same
  name as the filename (less the .MIX extension will be used.  If the output
  folder does not exist, it will be created

Examples:

To extract all files from mixfile_art_01.mix to the 'mixfile_art_01' folder
  unmix mixfile_art_01.mix

To extract all files from mixfile_art_01.mix to the 'Art' folder:
  unmix mixfile_art_01.mix Art

To extract all files from all mix files:
  unmix all
  (Note: this will create three folders: Art, Sound, and Talktree)