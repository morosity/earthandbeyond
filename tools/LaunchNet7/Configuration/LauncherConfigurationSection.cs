﻿// Decompiled with JetBrains decompiler
// Type: LaunchNet7.Configuration.LauncherConfigurationSection
// Assembly: LaunchNet7, Version=1.9.9.10, Culture=neutral, PublicKeyToken=null
// MVID: AF666469-2FA2-4BE0-A9D5-7003BDE93D8C
// Assembly location: D:\Games\Net-7\bin\LaunchNet7.exe

using System.Configuration;

namespace LaunchNet7.Configuration
{
  public class LauncherConfigurationSection : ConfigurationSection
  {
    public static string DefaultSectionName
    {
      get
      {
        return "launchNet7";
      }
    }

    [ConfigurationProperty("defaultWebsite")]
    public string DefaultWebsite
    {
      get
      {
        return (string) this["defaultWebsite"];
      }
      set
      {
        this["defaultWebsite"] = (object) value;
      }
    }

    [ConfigurationProperty("servers")]
    public ServerConfigurationElementCollection Servers
    {
      get
      {
        return (ServerConfigurationElementCollection) this["servers"];
      }
    }

    [ConfigurationProperty("autoUpdate")]
    public AutoUpdateConfigurationElement AutoUpdate
    {
      get
      {
        return (AutoUpdateConfigurationElement) this["autoUpdate"];
      }
    }
  }
}
