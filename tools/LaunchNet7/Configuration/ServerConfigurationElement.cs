﻿// Decompiled with JetBrains decompiler
// Type: LaunchNet7.Configuration.ServerConfigurationElement
// Assembly: LaunchNet7, Version=1.9.9.10, Culture=neutral, PublicKeyToken=null
// MVID: AF666469-2FA2-4BE0-A9D5-7003BDE93D8C
// Assembly location: D:\Games\Net-7\bin\LaunchNet7.exe

using System.Configuration;

namespace LaunchNet7.Configuration
{
  public class ServerConfigurationElement : ConfigurationElement
  {
    [ConfigurationProperty("name", IsRequired = true)]
    public string Name
    {
      get
      {
        return (string) this["name"];
      }
      set
      {
        this["name"] = (object) value;
      }
    }

    [ConfigurationProperty("displayName")]
    public string DisplayName
    {
      get
      {
        return (string) this["displayName"];
      }
      set
      {
        this["displayName"] = (object) value;
      }
    }

    [ConfigurationProperty("launchName")]
    public string LaunchName
    {
      get
      {
        return (string) this["launchName"];
      }
      set
      {
        this["launchName"] = (object) value;
      }
    }

    public string GetDisplayName()
    {
      if (string.IsNullOrEmpty(this.DisplayName))
        return this.Name;
      return this.DisplayName;
    }

    public string GetLaunchName()
    {
      if (string.IsNullOrEmpty(this.LaunchName))
        return this.Name;
      return this.LaunchName;
    }

    [ConfigurationProperty("", IsDefaultCollection = true)]
    public HostConfigurationElementCollection Hosts
    {
      get
      {
        return (HostConfigurationElementCollection) this[""];
      }
    }

    [ConfigurationProperty("allowProtocolSelection")]
    public bool AllowProtocolSelection
    {
      get
      {
        return (bool) this["allowProtocolSelection"];
      }
      set
      {
        this["allowProtocolSelection"] = (object) value;
      }
    }

    [ConfigurationProperty("defaultProtocol", DefaultValue = NetworkProtocol.Auto)]
    public NetworkProtocol DefaultProtocol
    {
      get
      {
        return (NetworkProtocol) this["defaultProtocol"];
      }
      set
      {
        this["defaultProtocol"] = (object) value;
      }
    }

    [ConfigurationProperty("isSinglePlayer")]
    public bool IsSinglePlayer
    {
      get
      {
        return (bool) this["isSinglePlayer"];
      }
      set
      {
        this["isSinglePlayer"] = (object) value;
      }
    }

    [ConfigurationProperty("enableAdvancedSettings", DefaultValue = false)]
    public bool EnableAdvancedSettings
    {
      get
      {
        return (bool) this["enableAdvancedSettings"];
      }
      set
      {
        this["enableAdvancedSettings"] = (object) value;
      }
    }

    public override string ToString()
    {
      return this.GetDisplayName();
    }
  }
}
