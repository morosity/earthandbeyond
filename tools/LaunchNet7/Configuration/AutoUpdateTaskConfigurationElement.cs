﻿// Decompiled with JetBrains decompiler
// Type: LaunchNet7.Configuration.AutoUpdateTaskConfigurationElement
// Assembly: LaunchNet7, Version=1.9.9.10, Culture=neutral, PublicKeyToken=null
// MVID: AF666469-2FA2-4BE0-A9D5-7003BDE93D8C
// Assembly location: D:\Games\Net-7\bin\LaunchNet7.exe

using LaunchNet7.Updateing;
using System.Configuration;

namespace LaunchNet7.Configuration
{
  public class AutoUpdateTaskConfigurationElement : ConfigurationElement
  {
    [ConfigurationProperty("name", IsRequired = true)]
    public string Name
    {
      get
      {
        return (string) this["name"];
      }
      set
      {
        this["name"] = (object) value;
      }
    }

    [ConfigurationProperty("type")]
    public string Type
    {
      get
      {
        return (string) this["type"];
      }
      set
      {
        this["type"] = (object) value;
      }
    }

    [ConfigurationProperty("restartOnUpdate", DefaultValue = false)]
    public bool RestartOnUpdate
    {
      get
      {
        return (bool) this["restartOnUpdate"];
      }
      set
      {
        this["restartOnUpdate"] = (object) value;
      }
    }

    [ConfigurationProperty("filesBaseUrl")]
    public string FilesBaseUrl
    {
      get
      {
        return (string) this["filesBaseUrl"];
      }
      set
      {
        this["filesBaseUrl"] = (object) value;
      }
    }

    public string GetFilesBaseUrl()
    {
      if (string.IsNullOrEmpty(this.FilesBaseUrl))
        return this.BaseUrl;
      return this.FilesBaseUrl;
    }

    [ConfigurationProperty("baseUrl", IsRequired = true)]
    public string BaseUrl
    {
      get
      {
        return (string) this["baseUrl"];
      }
      set
      {
        this["baseUrl"] = (object) value;
      }
    }

    [ConfigurationProperty("fileListName", DefaultValue = "Files.txt")]
    public string FileListName
    {
      get
      {
        return (string) this["fileListName"];
      }
      set
      {
        this["fileListName"] = (object) value;
      }
    }

    [ConfigurationProperty("fileListUrl")]
    public string FileListUrl
    {
      get
      {
        return (string) this["fileListUrl"];
      }
      set
      {
        this["fileListUrl"] = (object) value;
      }
    }

    public string GetFileListUrl()
    {
      if (string.IsNullOrEmpty(this.FileListUrl))
        return WebPath.Combine(this.BaseUrl, this.FileListName);
      return this.FileListUrl;
    }

    [ConfigurationProperty("versionFileName", DefaultValue = "Version.txt")]
    public string VersionFileName
    {
      get
      {
        return (string) this["versionFileName"];
      }
      set
      {
        this["versionFileName"] = (object) value;
      }
    }

    [ConfigurationProperty("versionFileUrl")]
    public string VersionFileUrl
    {
      get
      {
        return (string) this["versionFileUrl"];
      }
      set
      {
        this["versionFileUrl"] = (object) value;
      }
    }

    public string GetVersionFileUrl()
    {
      if (string.IsNullOrEmpty(this.VersionFileUrl))
        return WebPath.Combine(this.BaseUrl, this.VersionFileName);
      return this.VersionFileUrl;
    }

    [ConfigurationProperty("changelogFileUrl")]
    public string ChangelogFileUrl
    {
      get
      {
        return (string) this["changelogFileUrl"];
      }
      set
      {
        this["changelogFileUrl"] = (object) value;
      }
    }

    [ConfigurationProperty("versionCompareMode", DefaultValue = VersionCompareMode.Version)]
    public VersionCompareMode VersionCompareMode
    {
      get
      {
        return (VersionCompareMode) this["versionCompareMode"];
      }
      set
      {
        this["versionCompareMode"] = (object) value;
      }
    }
  }
}
