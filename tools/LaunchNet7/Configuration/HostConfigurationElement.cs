﻿// Decompiled with JetBrains decompiler
// Type: LaunchNet7.Configuration.HostConfigurationElement
// Assembly: LaunchNet7, Version=1.9.9.10, Culture=neutral, PublicKeyToken=null
// MVID: AF666469-2FA2-4BE0-A9D5-7003BDE93D8C
// Assembly location: D:\Games\Net-7\bin\LaunchNet7.exe

using System.Configuration;

namespace LaunchNet7.Configuration
{
  public class HostConfigurationElement : ConfigurationElement
  {
    [ConfigurationProperty("hostname")]
    public string Hostname
    {
      get
      {
        return (string) this["hostname"];
      }
      set
      {
        this["hostname"] = (object) value;
      }
    }

    [ConfigurationProperty("supportSecureAuthentication", DefaultValue = true)]
    public bool SupportsSecureAuthentication
    {
      get
      {
        return (bool) this["supportSecureAuthentication"];
      }
      set
      {
        this["supportSecureAuthentication"] = (object) value;
      }
    }

    [ConfigurationProperty("secureAuthenticationPort", DefaultValue = 443)]
    [IntegerValidator(ExcludeRange = false, MaxValue = 65536, MinValue = 1)]
    public int SecureAuthenticationPort
    {
      get
      {
        return (int) this["secureAuthenticationPort"];
      }
      set
      {
        this["secureAuthenticationPort"] = (object) value;
      }
    }

    [ConfigurationProperty("authenticationPort", DefaultValue = 80)]
    [IntegerValidator(ExcludeRange = false, MaxValue = 65536, MinValue = 1)]
    public int AuthenticationPort
    {
      get
      {
        return (int) this["authenticationPort"];
      }
      set
      {
        this["authenticationPort"] = (object) value;
      }
    }

    [ConfigurationProperty("globalServerPort", DefaultValue = 3805)]
    [IntegerValidator(ExcludeRange = false, MaxValue = 65536, MinValue = 1)]
    public int GlobalServerPort
    {
      get
      {
        return (int) this["globalServerPort"];
      }
      set
      {
        this["globalServerPort"] = (object) value;
      }
    }
  }
}
