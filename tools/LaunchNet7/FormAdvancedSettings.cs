﻿// Decompiled with JetBrains decompiler
// Type: LaunchNet7.FormAdvancedSettings
// Assembly: LaunchNet7, Version=1.9.9.10, Culture=neutral, PublicKeyToken=null
// MVID: AF666469-2FA2-4BE0-A9D5-7003BDE93D8C
// Assembly location: D:\Games\Net-7\bin\LaunchNet7.exe

using LaunchNet7.Properties;
using LaunchNet7.Updateing;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace LaunchNet7
{
  public class FormAdvancedSettings : Form
  {
    private IContainer components;
    private Button ForceUpdate;
    private TextBox AuthLog;
    private Label label1;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.ForceUpdate = new Button();
      this.AuthLog = new TextBox();
      this.label1 = new Label();
      this.SuspendLayout();
      this.ForceUpdate.Location = new Point(12, 12);
      this.ForceUpdate.Name = "ForceUpdate";
      this.ForceUpdate.Size = new Size(117, 35);
      this.ForceUpdate.TabIndex = 0;
      this.ForceUpdate.Text = "Force Update";
      this.ForceUpdate.UseVisualStyleBackColor = true;
      this.ForceUpdate.Click += new EventHandler(this.ForceUpdate_Click);
      this.AuthLog.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.AuthLog.Location = new Point(12, 69);
      this.AuthLog.Multiline = true;
      this.AuthLog.Name = "AuthLog";
      this.AuthLog.ScrollBars = ScrollBars.Both;
      this.AuthLog.Size = new Size(468, 135);
      this.AuthLog.TabIndex = 1;
      this.label1.AutoSize = true;
      this.label1.Location = new Point(12, 53);
      this.label1.Name = "label1";
      this.label1.Size = new Size(53, 13);
      this.label1.TabIndex = 2;
      this.label1.Text = "Auth Log:";
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(494, 215);
      this.Controls.Add((Control) this.label1);
      this.Controls.Add((Control) this.AuthLog);
      this.Controls.Add((Control) this.ForceUpdate);
      this.Name = nameof (FormAdvancedSettings);
      this.Text = "Advanced Settings";
      this.Load += new EventHandler(this.FormAdvancedSettings_Load);
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public FormAdvancedSettings()
    {
      this.InitializeComponent();
    }

    private void ForceUpdate_Click(object sender, EventArgs e)
    {
      try
      {
        UpdateUtility.ResetAllUpdates();
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show((IWin32Window) this, ex.Message, Program.TitleForErrorMessages, MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
    }

    private void FormAdvancedSettings_Load(object sender, EventArgs e)
    {
      this.BeginInvoke((Delegate) new EventHandler(this.Form_LoadComplete));
    }

    private void Form_LoadComplete(object sender, EventArgs e)
    {
      this.DoDisplayAuthloginLog();
    }

    private void DoDisplayAuthloginLog()
    {
      try
      {
        string path = Path.Combine(Path.GetDirectoryName(Settings.Default.ClientPath), "authlogin.log");
        if (!File.Exists(path))
          return;
        this.AuthLog.Text = File.ReadAllText(path);
      }
      catch (Exception ex)
      {
        Program.LogException(ex, "Could not load AuthLogin.log.");
        int num = (int) MessageBox.Show((IWin32Window) this, "Could not load AuthLogin.log.\nDetails: " + ex.Message, Program.TitleForErrorMessages, MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
    }
  }
}
