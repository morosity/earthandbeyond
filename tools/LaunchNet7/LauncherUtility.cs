﻿// Decompiled with JetBrains decompiler
// Type: LaunchNet7.LauncherUtility
// Assembly: LaunchNet7, Version=1.9.9.10, Culture=neutral, PublicKeyToken=null
// MVID: AF666469-2FA2-4BE0-A9D5-7003BDE93D8C
// Assembly location: D:\Games\Net-7\bin\LaunchNet7.exe

using System;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;

namespace LaunchNet7
{
  public static class LauncherUtility
  {
    [DllImport("kernel32.dll")]
    private static extern int GetShortPathName(string longPath, StringBuilder buffer, int bufferSize);

    public static string GetShortPathName(string path)
    {
      StringBuilder buffer = new StringBuilder(256);
      LauncherUtility.GetShortPathName(path, buffer, buffer.Capacity);
      return buffer.ToString();
    }

    public static string DownloadString(string url)
    {
      return LauncherUtility.DownloadString(url, 2500);
    }

    public static string DownloadString(string url, int timeoutInMilliseconds)
    {
      for (int index = 0; index < 4; ++index)
      {
        try
        {
          using (WebClient webClient = new WebClient())
            return webClient.DownloadString(url);
        }
        catch (Exception ex)
        {
          if (index >= 3)
            throw ex;
        }
      }
      return (string) null;
    }

    public static string GetParentDirectoryName(string directoryName)
    {
      return Directory.GetParent(directoryName).FullName;
    }
  }
}
