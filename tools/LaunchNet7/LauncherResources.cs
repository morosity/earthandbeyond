﻿// Decompiled with JetBrains decompiler
// Type: LaunchNet7.LauncherResources
// Assembly: LaunchNet7, Version=1.9.9.10, Culture=neutral, PublicKeyToken=null
// MVID: AF666469-2FA2-4BE0-A9D5-7003BDE93D8C
// Assembly location: D:\Games\Net-7\bin\LaunchNet7.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace LaunchNet7
{
  [DebuggerNonUserCode]
  [CompilerGenerated]
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
  internal class LauncherResources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal LauncherResources()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (object.ReferenceEquals((object) LauncherResources.resourceMan, (object) null))
          LauncherResources.resourceMan = new ResourceManager("LaunchNet7.LauncherResources", typeof (LauncherResources).Assembly);
        return LauncherResources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return LauncherResources.resourceCulture;
      }
      set
      {
        LauncherResources.resourceCulture = value;
      }
    }

    internal static Bitmap CheckFile
    {
      get
      {
        return (Bitmap) LauncherResources.ResourceManager.GetObject(nameof (CheckFile), LauncherResources.resourceCulture);
      }
    }

    internal static Bitmap Error_16x16x32
    {
      get
      {
        return (Bitmap) LauncherResources.ResourceManager.GetObject(nameof (Error_16x16x32), LauncherResources.resourceCulture);
      }
    }

    internal static Bitmap Error_48x48x32
    {
      get
      {
        return (Bitmap) LauncherResources.ResourceManager.GetObject(nameof (Error_48x48x32), LauncherResources.resourceCulture);
      }
    }

    internal static byte[] ExeUpdater
    {
      get
      {
        return (byte[]) LauncherResources.ResourceManager.GetObject(nameof (ExeUpdater), LauncherResources.resourceCulture);
      }
    }

    internal static Bitmap LaunchNet7
    {
      get
      {
        return (Bitmap) LauncherResources.ResourceManager.GetObject(nameof (LaunchNet7), LauncherResources.resourceCulture);
      }
    }

    internal static Bitmap NavBack
    {
      get
      {
        return (Bitmap) LauncherResources.ResourceManager.GetObject(nameof (NavBack), LauncherResources.resourceCulture);
      }
    }

    internal static Bitmap NavForward
    {
      get
      {
        return (Bitmap) LauncherResources.ResourceManager.GetObject(nameof (NavForward), LauncherResources.resourceCulture);
      }
    }

    internal static Bitmap Run
    {
      get
      {
        return (Bitmap) LauncherResources.ResourceManager.GetObject(nameof (Run), LauncherResources.resourceCulture);
      }
    }

    internal static Bitmap Setup_48x48x32
    {
      get
      {
        return (Bitmap) LauncherResources.ResourceManager.GetObject(nameof (Setup_48x48x32), LauncherResources.resourceCulture);
      }
    }

    internal static Bitmap StatusInformation
    {
      get
      {
        return (Bitmap) LauncherResources.ResourceManager.GetObject(nameof (StatusInformation), LauncherResources.resourceCulture);
      }
    }

    internal static Bitmap StatusOk
    {
      get
      {
        return (Bitmap) LauncherResources.ResourceManager.GetObject(nameof (StatusOk), LauncherResources.resourceCulture);
      }
    }

    internal static Bitmap StatusUpdated
    {
      get
      {
        return (Bitmap) LauncherResources.ResourceManager.GetObject(nameof (StatusUpdated), LauncherResources.resourceCulture);
      }
    }

    internal static Bitmap StatusWorking
    {
      get
      {
        return (Bitmap) LauncherResources.ResourceManager.GetObject(nameof (StatusWorking), LauncherResources.resourceCulture);
      }
    }

    internal static Bitmap Warning_16x16x32
    {
      get
      {
        return (Bitmap) LauncherResources.ResourceManager.GetObject(nameof (Warning_16x16x32), LauncherResources.resourceCulture);
      }
    }
  }
}
