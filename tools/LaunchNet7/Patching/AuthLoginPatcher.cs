﻿// Decompiled with JetBrains decompiler
// Type: LaunchNet7.Patching.AuthLoginPatcher
// Assembly: LaunchNet7, Version=1.9.9.10, Culture=neutral, PublicKeyToken=null
// MVID: AF666469-2FA2-4BE0-A9D5-7003BDE93D8C
// Assembly location: D:\Games\Net-7\bin\LaunchNet7.exe

using System;
using System.IO;

namespace LaunchNet7.Patching
{
  public class AuthLoginPatcher
  {
    private static readonly byte Https = 192;
    private static readonly byte Http = 64;

    public static AuthPatcherInfo ReadInformation(string fileName)
    {
      if (!File.Exists(fileName))
        throw new FileNotFoundException();
      AuthPatcherInfo authPatcherInfo = new AuthPatcherInfo();
      using (FileStream fileStream = File.OpenRead(fileName))
      {
        fileStream.Seek(33576L, SeekOrigin.Begin);
        byte num = (byte) fileStream.ReadByte();
        if ((int) num == (int) AuthLoginPatcher.Https)
        {
          authPatcherInfo.UseHttps = true;
        }
        else
        {
          if ((int) num != (int) AuthLoginPatcher.Http)
            throw new InvalidDataException();
          authPatcherInfo.UseHttps = false;
        }
        fileStream.Seek(33453L, SeekOrigin.Begin);
        byte[] buffer1 = new byte[2];
        fileStream.Read(buffer1, 0, 2);
        authPatcherInfo.Port = BitConverter.ToUInt16(buffer1, 0);
        fileStream.Seek(33426L, SeekOrigin.Begin);
        byte[] buffer2 = new byte[2];
        fileStream.Read(buffer2, 0, 2);
        authPatcherInfo.TimeOut = BitConverter.ToUInt16(buffer2, 0);
      }
      return authPatcherInfo;
    }

    public static void WriteInformation(string fileName, AuthPatcherInfo infos)
    {
      if (infos == null)
        throw new ArgumentNullException(nameof (infos));
      if (!File.Exists(fileName))
        throw new FileNotFoundException();
      using (FileStream fileStream = File.OpenWrite(fileName))
      {
        fileStream.Seek(33576L, SeekOrigin.Begin);
        fileStream.WriteByte(infos.UseHttps ? AuthLoginPatcher.Https : AuthLoginPatcher.Http);
        fileStream.Seek(33453L, SeekOrigin.Begin);
        byte[] bytes1 = BitConverter.GetBytes(infos.Port);
        fileStream.Write(bytes1, 0, 2);
        fileStream.Seek(33426L, SeekOrigin.Begin);
        byte[] bytes2 = BitConverter.GetBytes(infos.TimeOut);
        fileStream.Write(bytes2, 0, 2);
      }
    }
  }
}
