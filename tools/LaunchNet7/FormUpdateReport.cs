﻿// Decompiled with JetBrains decompiler
// Type: LaunchNet7.FormUpdateReport
// Assembly: LaunchNet7, Version=1.9.9.10, Culture=neutral, PublicKeyToken=null
// MVID: AF666469-2FA2-4BE0-A9D5-7003BDE93D8C
// Assembly location: D:\Games\Net-7\bin\LaunchNet7.exe

using LaunchNet7.Updateing;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace LaunchNet7
{
  public class FormUpdateReport : Form
  {
    private IContainer components;
    private ListView c_ListView;
    private ColumnHeader c_ColumnHeader_File;
    private ColumnHeader c_ColumnHeader_Url;
    private ColumnHeader c_ColumnHeader_LocalPath;
    private ColumnHeader c_ColumnHeader_Status;
    private Button c_Button_Close;
    private ColumnHeader c_ColumnHeader_Comment;
    private ImageList c_ImageList;
    private Button c_Button_Retry;
    private ColumnHeader c_ColumnHeader_Length;
    private Updater m_Updater;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      this.c_ListView = new ListView();
      this.c_ColumnHeader_File = new ColumnHeader();
      this.c_ColumnHeader_Length = new ColumnHeader();
      this.c_ColumnHeader_Status = new ColumnHeader();
      this.c_ColumnHeader_Url = new ColumnHeader();
      this.c_ColumnHeader_LocalPath = new ColumnHeader();
      this.c_ColumnHeader_Comment = new ColumnHeader();
      this.c_ImageList = new ImageList(this.components);
      this.c_Button_Close = new Button();
      this.c_Button_Retry = new Button();
      this.SuspendLayout();
      this.c_ListView.AllowColumnReorder = true;
      this.c_ListView.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.c_ListView.Columns.AddRange(new ColumnHeader[6]
      {
        this.c_ColumnHeader_File,
        this.c_ColumnHeader_Length,
        this.c_ColumnHeader_Status,
        this.c_ColumnHeader_Url,
        this.c_ColumnHeader_LocalPath,
        this.c_ColumnHeader_Comment
      });
      this.c_ListView.FullRowSelect = true;
      this.c_ListView.Location = new Point(12, 12);
      this.c_ListView.Name = "c_ListView";
      this.c_ListView.ShowItemToolTips = true;
      this.c_ListView.Size = new Size(860, 447);
      this.c_ListView.SmallImageList = this.c_ImageList;
      this.c_ListView.TabIndex = 0;
      this.c_ListView.UseCompatibleStateImageBehavior = false;
      this.c_ListView.View = View.Details;
      this.c_ListView.SelectedIndexChanged += new EventHandler(this.c_ListView_SelectedIndexChanged);
      this.c_ColumnHeader_File.Text = "File";
      this.c_ColumnHeader_File.Width = 240;
      this.c_ColumnHeader_Length.Text = "Length";
      this.c_ColumnHeader_Length.TextAlign = HorizontalAlignment.Right;
      this.c_ColumnHeader_Status.Text = "Status";
      this.c_ColumnHeader_Status.Width = 180;
      this.c_ColumnHeader_Url.Text = "Url";
      this.c_ColumnHeader_Url.Width = 300;
      this.c_ColumnHeader_LocalPath.Text = "Local Path";
      this.c_ColumnHeader_LocalPath.Width = 300;
      this.c_ColumnHeader_Comment.Text = "Comment";
      this.c_ColumnHeader_Comment.Width = 240;
      this.c_ImageList.ColorDepth = ColorDepth.Depth32Bit;
      this.c_ImageList.ImageSize = new Size(16, 16);
      this.c_ImageList.TransparentColor = Color.Magenta;
      this.c_Button_Close.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.c_Button_Close.Location = new Point(797, 465);
      this.c_Button_Close.Name = "c_Button_Close";
      this.c_Button_Close.Size = new Size(75, 23);
      this.c_Button_Close.TabIndex = 1;
      this.c_Button_Close.Text = "&Close";
      this.c_Button_Close.UseVisualStyleBackColor = true;
      this.c_Button_Close.Click += new EventHandler(this.c_Button_Close_Click);
      this.c_Button_Retry.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.c_Button_Retry.Location = new Point(716, 465);
      this.c_Button_Retry.Name = "c_Button_Retry";
      this.c_Button_Retry.Size = new Size(75, 23);
      this.c_Button_Retry.TabIndex = 2;
      this.c_Button_Retry.Text = "&Retry";
      this.c_Button_Retry.UseVisualStyleBackColor = true;
      this.c_Button_Retry.Visible = false;
      this.c_Button_Retry.Click += new EventHandler(this.c_Button_Retry_Click);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(884, 500);
      this.Controls.Add((Control) this.c_Button_Retry);
      this.Controls.Add((Control) this.c_Button_Close);
      this.Controls.Add((Control) this.c_ListView);
      this.Name = nameof (FormUpdateReport);
      this.Text = "[Title]";
      this.Load += new EventHandler(this.FormUpdateReport_Load);
      this.ResumeLayout(false);
    }

    public FormUpdateReport()
    {
      this.InitializeComponent();
      this.c_ImageList.Images.Add((Image) LauncherResources.StatusInformation);
      this.c_ImageList.Images.Add((Image) LauncherResources.StatusOk);
      this.c_ImageList.Images.Add((Image) LauncherResources.StatusUpdated);
      this.c_ImageList.Images.Add((Image) LauncherResources.Warning_16x16x32);
      this.c_ImageList.Images.Add((Image) LauncherResources.Error_16x16x32);
    }

    private void FormUpdateReport_Load(object sender, EventArgs e)
    {
      if (this.m_Updater == null)
        return;
      this.DoDisplayUpdateReport();
    }

    private void DoDisplayUpdateReport()
    {
      this.c_ListView.BeginUpdate();
      try
      {
        this.c_ListView.Items.Clear();
        foreach (UpdateItem updateItem in (Collection<UpdateItem>) this.Updater.Items)
        {
          ListViewItem listViewItem = new ListViewItem();
          listViewItem.SubItems.Add(string.Empty);
          listViewItem.SubItems.Add(string.Empty);
          listViewItem.SubItems.Add(string.Empty);
          listViewItem.SubItems.Add(string.Empty);
          listViewItem.SubItems[this.c_ColumnHeader_File.Index].Text = updateItem.FileNameRelative;
          listViewItem.SubItems[this.c_ColumnHeader_Url.Index].Text = updateItem.FileUrl;
          listViewItem.SubItems[this.c_ColumnHeader_LocalPath.Index].Text = updateItem.FileName;
          string str1;
          if (updateItem.LengthExpected.HasValue)
          {
            long num = updateItem.LengthExpected.Value;
            str1 = num != 0L ? (num % 1024L != 0L ? (num / 1024L + 1L).ToString("N0") + " KB" : (num / 1024L).ToString("N0") + " KB") : "0 KB";
          }
          else
            str1 = "?";
          listViewItem.SubItems[this.c_ColumnHeader_Length.Index].Text = str1;
          listViewItem.BackColor = this.c_ListView.Items.Count % 2 == 1 ? Color.WhiteSmoke : this.c_ListView.BackColor;
          string str2;
          switch (updateItem.CheckStatus)
          {
            case UpdateCheckStatus.NotExisiting:
            case UpdateCheckStatus.LengthMismatch:
            case UpdateCheckStatus.HashMismatch:
              switch (updateItem.UpdateError)
              {
                case UpdateError.None:
                  str2 = updateItem.CheckStatus != UpdateCheckStatus.NotExisiting ? "Updated" : "Created";
                  listViewItem.ImageIndex = 2;
                  break;
                case UpdateError.UrlNotFound:
                  str2 = "Url not found.";
                  listViewItem.ImageIndex = 3;
                  break;
                case UpdateError.CannotUpdateFile:
                  str2 = "Update failed.";
                  listViewItem.ImageIndex = 4;
                  break;
                default:
                  str2 = updateItem.UpdateError.ToString();
                  break;
              }
            default:
              str2 = "No Update Required";
              listViewItem.ImageIndex = 1;
              break;
          }
          listViewItem.SubItems[this.c_ColumnHeader_Status.Index].Text = str2;
          if (updateItem.Error != null)
          {
            string message = updateItem.Error.Message;
            listViewItem.SubItems[this.c_ColumnHeader_Comment.Index].Text = "Error: " + message;
            listViewItem.ImageIndex = 4;
          }
          this.c_ListView.Items.Add(listViewItem);
        }
      }
      finally
      {
        this.c_ListView.EndUpdate();
      }
      this.Text = "Update Report: " + this.Updater.Name;
      this.c_Button_Retry.Visible = this.Updater.UpdateErrorsOccured;
    }

    public Updater Updater
    {
      get
      {
        return this.m_Updater;
      }
      set
      {
        this.m_Updater = value;
      }
    }

    private void c_Button_Close_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.None;
      this.Close();
    }

    private void c_Button_Retry_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.Retry;
      this.Close();
    }

    private void c_ListView_SelectedIndexChanged(object sender, EventArgs e)
    {
    }
  }
}
