﻿// Decompiled with JetBrains decompiler
// Type: LaunchNet7.Properties.Settings
// Assembly: LaunchNet7, Version=1.9.9.10, Culture=neutral, PublicKeyToken=null
// MVID: AF666469-2FA2-4BE0-A9D5-7003BDE93D8C
// Assembly location: D:\Games\Net-7\bin\LaunchNet7.exe

using System.CodeDom.Compiler;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.CompilerServices;

namespace LaunchNet7.Properties
{
  [CompilerGenerated]
  [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "10.0.0.0")]
  internal sealed class Settings : ApplicationSettingsBase
  {
    private static Settings defaultInstance = (Settings) SettingsBase.Synchronized((SettingsBase) new Settings());

    public static Settings Default
    {
      get
      {
        return Settings.defaultInstance;
      }
    }

    [DefaultSettingValue("")]
    [UserScopedSetting]
    [DebuggerNonUserCode]
    public string ClientPath
    {
      get
      {
        return (string) this[nameof (ClientPath)];
      }
      set
      {
        this[nameof (ClientPath)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    public Point FormMainPosition
    {
      get
      {
        return (Point) this[nameof (FormMainPosition)];
      }
      set
      {
        this[nameof (FormMainPosition)] = (object) value;
      }
    }

    [DefaultSettingValue("False")]
    [DebuggerNonUserCode]
    [UserScopedSetting]
    public bool UseClientDetours
    {
      get
      {
        return (bool) this[nameof (UseClientDetours)];
      }
      set
      {
        this[nameof (UseClientDetours)] = (object) value;
      }
    }

    [DefaultSettingValue("False")]
    [DebuggerNonUserCode]
    [UserScopedSetting]
    public bool UseLocalCert
    {
      get
      {
        return (bool) this[nameof (UseLocalCert)];
      }
      set
      {
        this[nameof (UseLocalCert)] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    [DefaultSettingValue("False")]
    [UserScopedSetting]
    public bool UsePacketOpt
    {
      get
      {
        return (bool) this[nameof (UsePacketOpt)];
      }
      set
      {
        this[nameof (UsePacketOpt)] = (object) value;
      }
    }

    [DefaultSettingValue("False")]
    [DebuggerNonUserCode]
    [UserScopedSetting]
    public bool UseExperimentalReorder
    {
      get
      {
        return (bool) this[nameof (UseExperimentalReorder)];
      }
      set
      {
        this[nameof (UseExperimentalReorder)] = (object) value;
      }
    }

    [DefaultSettingValue("")]
    [UserScopedSetting]
    [DebuggerNonUserCode]
    public string LastEmulatorName
    {
      get
      {
        return (string) this[nameof (LastEmulatorName)];
      }
      set
      {
        this[nameof (LastEmulatorName)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DefaultSettingValue("")]
    [DebuggerNonUserCode]
    public string LastServerName
    {
      get
      {
        return (string) this[nameof (LastServerName)];
      }
      set
      {
        this[nameof (LastServerName)] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    [DefaultSettingValue("")]
    [UserScopedSetting]
    public string AuthenticationPort
    {
      get
      {
        return (string) this[nameof (AuthenticationPort)];
      }
      set
      {
        this[nameof (AuthenticationPort)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("True")]
    public bool UseSecureAuthentication
    {
      get
      {
        return (bool) this[nameof (UseSecureAuthentication)];
      }
      set
      {
        this[nameof (UseSecureAuthentication)] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    [DefaultSettingValue("False")]
    [UserScopedSetting]
    public bool DebugLaunch
    {
      get
      {
        return (bool) this[nameof (DebugLaunch)];
      }
      set
      {
        this[nameof (DebugLaunch)] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    [DefaultSettingValue("http://www.net-7.org/LauncherUpdates/")]
    [UserScopedSetting]
    public string WebsiteURL
    {
      get
      {
        return (string) this[nameof (WebsiteURL)];
      }
      set
      {
        this[nameof (WebsiteURL)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DefaultSettingValue("0")]
    [DebuggerNonUserCode]
    public int SelectedIP
    {
      get
      {
        return (int) this[nameof (SelectedIP)];
      }
      set
      {
        this[nameof (SelectedIP)] = (object) value;
      }
    }

    [DefaultSettingValue("<?xml version=\"1.0\" encoding=\"utf-16\"?>\r\n<ArrayOfString xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\r\n  <string>sunrise.net-7.org</string>\r\n  <string>ptr.net-7.org</string>\r\n  <string>dev.net-7.org</string>\r\n</ArrayOfString>")]
    [UserScopedSetting]
    [DebuggerNonUserCode]
    public StringCollection ServerList
    {
      get
      {
        return (StringCollection) this[nameof (ServerList)];
      }
      set
      {
        this[nameof (ServerList)] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    [UserScopedSetting]
    [DefaultSettingValue("0")]
    public int ServerIndex
    {
      get
      {
        return (int) this[nameof (ServerIndex)];
      }
      set
      {
        this[nameof (ServerIndex)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("False")]
    public bool LockPort
    {
      get
      {
        return (bool) this[nameof (LockPort)];
      }
      set
      {
        this[nameof (LockPort)] = (object) value;
      }
    }
  }
}
