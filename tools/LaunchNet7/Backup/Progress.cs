﻿// Decompiled with JetBrains decompiler
// Type: LaunchNet7.Progress
// Assembly: LaunchNet7, Version=1.9.9.10, Culture=neutral, PublicKeyToken=null
// MVID: AF666469-2FA2-4BE0-A9D5-7003BDE93D8C
// Assembly location: D:\Games\Net-7\bin\LaunchNet7.exe

namespace LaunchNet7
{
  public class Progress
  {
    private long m_MinimumValue;
    private long m_MaximumValue;
    private long m_CurrentValue;

    public Progress()
      : this(0L, 100L)
    {
    }

    public Progress(long minimumValue, long maximumValue)
    {
      this.Reset(minimumValue, maximumValue);
    }

    public long MinimumValue
    {
      get
      {
        return this.m_MinimumValue;
      }
      set
      {
        this.m_MinimumValue = value;
      }
    }

    public long MaximumValue
    {
      get
      {
        return this.m_MaximumValue;
      }
      set
      {
        this.m_MaximumValue = value;
      }
    }

    public long CurrentValue
    {
      get
      {
        return this.m_CurrentValue;
      }
      set
      {
        this.m_CurrentValue = value;
      }
    }

    public void Increment()
    {
      ++this.m_CurrentValue;
    }

    public void Increment(long value)
    {
      this.m_CurrentValue += value;
    }

    public void Reset()
    {
      this.m_CurrentValue = this.m_MinimumValue;
    }

    public void Reset(long maximumValue)
    {
      this.m_MaximumValue = maximumValue;
      this.m_CurrentValue = this.m_MinimumValue;
    }

    public void Reset(long minimumValue, long maximumValue)
    {
      this.m_MinimumValue = minimumValue;
      this.m_MaximumValue = maximumValue;
      this.m_CurrentValue = this.m_MinimumValue;
    }

    public int ToProgressPercentage()
    {
      if (this.m_CurrentValue == 0L || this.m_CurrentValue < this.m_MinimumValue)
        return 0;
      if (this.m_CurrentValue > this.m_MaximumValue)
        return 100;
      float num = (float) (this.m_MaximumValue - this.m_MinimumValue);
      if ((double) num == 0.0)
        return 0;
      return (int) ((double) this.m_CurrentValue / (double) num * 100.0);
    }
  }
}
