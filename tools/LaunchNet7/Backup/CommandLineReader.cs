﻿// Decompiled with JetBrains decompiler
// Type: LaunchNet7.CommandLineReader
// Assembly: LaunchNet7, Version=1.9.9.10, Culture=neutral, PublicKeyToken=null
// MVID: AF666469-2FA2-4BE0-A9D5-7003BDE93D8C
// Assembly location: D:\Games\Net-7\bin\LaunchNet7.exe

using System;

namespace LaunchNet7
{
  public class CommandLineReader
  {
    private int m_CurrentIndex = -1;
    private string[] m_Args;

    public CommandLineReader()
      : this(Environment.GetCommandLineArgs())
    {
    }

    public CommandLineReader(string[] args)
    {
      if (args == null)
        throw new ArgumentNullException(nameof (args));
      this.m_Args = args;
    }

    public int Count
    {
      get
      {
        return this.m_Args.Length;
      }
    }

    public int CurrentIndex
    {
      get
      {
        return this.m_CurrentIndex;
      }
      set
      {
        if (value < 0)
          throw new ArgumentOutOfRangeException();
        this.m_CurrentIndex = value;
      }
    }

    public bool Read()
    {
      if (this.m_CurrentIndex == -1)
      {
        this.m_CurrentIndex = 0;
        return true;
      }
      ++this.m_CurrentIndex;
      return !this.IsOutOfRange;
    }

    private bool IsOutOfRange
    {
      get
      {
        if (this.m_CurrentIndex >= 0)
          return this.m_CurrentIndex >= this.m_Args.Length;
        return true;
      }
    }

    public string CurrentArgument
    {
      get
      {
        if (!this.IsOutOfRange)
          return this.m_Args[this.CurrentIndex];
        return (string) null;
      }
    }

    public bool TryGetString(out string value)
    {
      if (this.IsOutOfRange)
      {
        value = (string) null;
        return false;
      }
      value = this.CurrentArgument;
      return true;
    }

    public bool TryReadNextAsString(out string value)
    {
      if (this.IsOutOfRange)
      {
        value = (string) null;
        return false;
      }
      if (!this.Read())
      {
        value = (string) null;
        return false;
      }
      value = this.CurrentArgument;
      return true;
    }

    public bool TryGetInt32(out int value)
    {
      if (!this.IsOutOfRange)
        return int.TryParse(this.CurrentArgument, out value);
      value = 0;
      return false;
    }

    public bool TryReadNextAsInt32(out int value)
    {
      if (!this.IsOutOfRange && this.Read())
        return int.TryParse(this.CurrentArgument, out value);
      value = 0;
      return false;
    }

    public int RemainingCount
    {
      get
      {
        return this.m_Args.Length - this.m_CurrentIndex;
      }
    }
  }
}
