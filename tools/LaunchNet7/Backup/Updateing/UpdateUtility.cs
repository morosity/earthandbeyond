﻿// Decompiled with JetBrains decompiler
// Type: LaunchNet7.Updateing.UpdateUtility
// Assembly: LaunchNet7, Version=1.9.9.10, Culture=neutral, PublicKeyToken=null
// MVID: AF666469-2FA2-4BE0-A9D5-7003BDE93D8C
// Assembly location: D:\Games\Net-7\bin\LaunchNet7.exe

using LaunchNet7.Configuration;
using System;
using System.Configuration;
using System.IO;

namespace LaunchNet7.Updateing
{
  public static class UpdateUtility
  {
    private static readonly char[] InvalidFileNameChars = Path.GetInvalidFileNameChars();

    static UpdateUtility()
    {
      Array.Sort<char>(UpdateUtility.InvalidFileNameChars);
    }

    public static bool CanWriteToDirectory(string directoryName)
    {
      string path = Path.Combine(directoryName, "WriteAccessTest.tmp");
      try
      {
        File.WriteAllBytes(path, new byte[0]);
        return true;
      }
      catch
      {
        return false;
      }
      finally
      {
        try
        {
          if (File.Exists(path))
            File.Delete(path);
        }
        catch
        {
        }
      }
    }

    public static Updater GetUpdaterForConfiguration(AutoUpdateTaskConfigurationElement updateTask)
    {
      Updater updater;
      if (!string.IsNullOrEmpty(updateTask.Type))
      {
        try
        {
          updater = (Updater) Activator.CreateInstance(Type.GetType(updateTask.Type));
        }
        catch
        {
          throw new ApplicationException(string.Format("Could not instanciate updater object for update task.\nName: {0}\nType: {1}", (object) updateTask.Name, (object) updateTask.Type));
        }
      }
      else
        updater = new Updater();
      updater.Name = updateTask.Name;
      updater.TargetDirectoryName = !string.Equals(updater.Name, "Client", StringComparison.CurrentCultureIgnoreCase) ? string.Empty : Program.ClientRootDirectoryName;
      string str = UpdateUtility.ReplaceInvalidFileNameCharacters(updater.Name) + "-";
      string path = updateTask.VersionFileName;
      if (!string.IsNullOrEmpty(path))
      {
        if (path.StartsWith("$(TargetDirectory)", StringComparison.CurrentCultureIgnoreCase))
          path = Path.Combine(updater.TargetDirectoryName, path.Substring("$(TargetDirectory)".Length));
        else if (path.StartsWith("$(ClientRootDirectory)", StringComparison.CurrentCultureIgnoreCase))
          path = Path.Combine(updater.TargetDirectoryName, path.Substring("$(ClientRootDirectory)".Length));
        else if (path.StartsWith("$(Net7BinDirectory)", StringComparison.CurrentCultureIgnoreCase))
          path = Path.Combine(Program.ProgramDirectoryName, path.Substring("$(Net7BinDirectory)".Length));
        else if (path.StartsWith("$(Net7Directory)", StringComparison.CurrentCultureIgnoreCase))
          path = Path.Combine(LauncherUtility.GetParentDirectoryName(Program.ProgramDirectoryName), path.Substring("$(Net7Directory)".Length));
      }
      updater.VersionFileName = Path.Combine(Path.GetDirectoryName(path), str + Path.GetFileName(path));
      updater.VersionCompareMode = updateTask.VersionCompareMode;
      updater.RestartOnUpdate = updateTask.RestartOnUpdate;
      updater.ChangelogFileUrl = updateTask.ChangelogFileUrl;
      updater.FilesBaseUrl = updateTask.GetFilesBaseUrl();
      updater.FileListUrl = updateTask.GetFileListUrl();
      updater.VersionFileUrl = updateTask.GetVersionFileUrl();
      return updater;
    }

    public static void ResetAllUpdates()
    {
      foreach (AutoUpdateTaskConfigurationElement autoUpdateTask in (ConfigurationElementCollection) Program.LauncherConfiguration.AutoUpdate.AutoUpdateTasks)
      {
        Updater forConfiguration = UpdateUtility.GetUpdaterForConfiguration(autoUpdateTask);
        string versionFileName = forConfiguration.VersionFileName;
        try
        {
          if (File.Exists(versionFileName))
            File.Delete(versionFileName);
          if (File.Exists(forConfiguration.Name + "-FileList.txt"))
            File.Delete(forConfiguration.Name + "-FileList.txt");
        }
        catch (Exception ex)
        {
          string message = string.Format("Could not delete version file for update \"{0}\".\nFileName: {1}\nDetails: {2}", (object) autoUpdateTask.Name, (object) versionFileName, (object) ex.Message);
          Program.LogException(ex, message);
          throw new ApplicationException(message, ex);
        }
      }
    }

    public static string ReplaceInvalidFileNameCharacters(string value)
    {
      int length = 0;
      char[] charArray = value.ToCharArray();
      for (int index = 0; index < value.Length; ++index)
      {
        char ch = value[index];
        if (Array.BinarySearch<char>(UpdateUtility.InvalidFileNameChars, ch) < 0)
          charArray[length++] = ch;
      }
      return new string(charArray, 0, length);
    }
  }
}
