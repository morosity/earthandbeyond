﻿// Decompiled with JetBrains decompiler
// Type: LaunchNet7.Updateing.Updater
// Assembly: LaunchNet7, Version=1.9.9.10, Culture=neutral, PublicKeyToken=null
// MVID: AF666469-2FA2-4BE0-A9D5-7003BDE93D8C
// Assembly location: D:\Games\Net-7\bin\LaunchNet7.exe

using LaunchNet7.Cryptography;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Text;

namespace LaunchNet7.Updateing
{
  public class Updater
  {
    private UpdateItemCollection m_Items = new UpdateItemCollection();
    public const int DisplayProgressText1 = -1;
    public const int DisplayProgressText2 = -2;
    public const int DisplayProgress1 = 1;
    public const int DisplayProgress2 = 2;
    private string m_ServerVersionRawText;
    private string m_ServerFileList;
    private string m_Name;
    private string m_TargetDirectoryName;
    private string m_VersionFileName;
    private string m_VersionFileUrl;
    private string m_FileListUrl;
    private string m_ChangelogFileName;
    private string m_ChangelogFileUrl;
    private string m_CurrentVersionText;
    private string m_NewVersionText;
    private bool m_RestartOnUpdate;
    private UpdateCheckResult m_CheckResult;
    private Exception m_Error;
    private VersionCompareMode m_VersionCompareMode;
    private bool m_ForceUpdate;
    private string m_FilesUrl;
    private string m_CurrentChangelog;
    private bool m_SelfUpdateDetected;
    private UpdateItem m_SelfUpdateItem;
    private bool m_UpdateErrorsOccured;

    public string Name
    {
      get
      {
        return this.m_Name;
      }
      set
      {
        this.m_Name = value;
      }
    }

    public string TargetDirectoryName
    {
      get
      {
        return this.m_TargetDirectoryName;
      }
      set
      {
        this.m_TargetDirectoryName = value;
      }
    }

    public string VersionFileName
    {
      get
      {
        return this.m_VersionFileName;
      }
      set
      {
        this.m_VersionFileName = value;
      }
    }

    public string VersionFileUrl
    {
      get
      {
        return this.m_VersionFileUrl;
      }
      set
      {
        this.m_VersionFileUrl = value;
      }
    }

    public string FileListUrl
    {
      get
      {
        return this.m_FileListUrl;
      }
      set
      {
        this.m_FileListUrl = value;
      }
    }

    public string ChangelogFileName
    {
      get
      {
        return this.m_ChangelogFileName;
      }
      set
      {
        this.m_ChangelogFileName = value;
      }
    }

    public string ChangelogFileUrl
    {
      get
      {
        return this.m_ChangelogFileUrl;
      }
      set
      {
        this.m_ChangelogFileUrl = value;
      }
    }

    public string CurrentVersionText
    {
      get
      {
        return this.m_CurrentVersionText;
      }
      set
      {
        this.m_CurrentVersionText = value;
      }
    }

    public string NewVersionText
    {
      get
      {
        return this.m_NewVersionText;
      }
      set
      {
        this.m_NewVersionText = value;
      }
    }

    public bool RestartOnUpdate
    {
      get
      {
        return this.m_RestartOnUpdate;
      }
      set
      {
        this.m_RestartOnUpdate = value;
      }
    }

    public UpdateCheckResult CheckResult
    {
      get
      {
        return this.m_CheckResult;
      }
      set
      {
        this.m_CheckResult = value;
      }
    }

    public Exception Error
    {
      get
      {
        return this.m_Error;
      }
      set
      {
        this.m_Error = value;
      }
    }

    public VersionCompareMode VersionCompareMode
    {
      get
      {
        return this.m_VersionCompareMode;
      }
      set
      {
        this.m_VersionCompareMode = value;
      }
    }

    public bool ForceUpdate
    {
      get
      {
        return this.m_ForceUpdate;
      }
      set
      {
        this.m_ForceUpdate = value;
      }
    }

    public string FilesBaseUrl
    {
      get
      {
        return this.m_FilesUrl;
      }
      set
      {
        this.m_FilesUrl = value;
      }
    }

    public string CurrentChangelog
    {
      get
      {
        return this.m_CurrentChangelog;
      }
      set
      {
        this.m_CurrentChangelog = value;
      }
    }

    public virtual UpdateCheckResult CheckForUpdates()
    {
      if (string.IsNullOrEmpty(this.VersionFileName))
        throw new InvalidOperationException("VersionFileName is not set to a value.");
      if (string.IsNullOrEmpty(this.VersionFileUrl))
        throw new InvalidOperationException("VersionFileUrl is not set to a value.");
      if (string.IsNullOrEmpty(this.FilesBaseUrl))
        throw new InvalidOperationException("FilesUrl is not set to a value.");
      if (this.ForceUpdate)
      {
        this.ForceUpdate = true;
        this.CurrentVersionText = "Update required.";
      }
      Version version1 = (Version) null;
      int num1 = 0;
      string str1 = (string) null;
      if (!File.Exists(this.VersionFileName))
      {
        this.ForceUpdate = true;
        this.CurrentVersionText = "Version cannot be determined.";
      }
      else
      {
        str1 = File.ReadAllText(this.VersionFileName);
        switch (this.VersionCompareMode)
        {
          case VersionCompareMode.SameText:
            this.CurrentVersionText = str1;
            break;
          case VersionCompareMode.Number:
            num1 = int.Parse(str1);
            this.CurrentVersionText = num1.ToString();
            break;
          case VersionCompareMode.Version:
            version1 = new Version(str1);
            this.CurrentVersionText = version1.ToString();
            break;
        }
      }
      string str2;
      try
      {
        str2 = LauncherUtility.DownloadString(this.VersionFileUrl);
        this.m_ServerVersionRawText = str2;
      }
      catch (WebException ex)
      {
        this.Error = (Exception) ex;
        Program.LogException((Exception) ex);
        this.CheckResult = ex.Status != WebExceptionStatus.Timeout ? UpdateCheckResult.Error : UpdateCheckResult.Timeout;
        return this.CheckResult;
      }
      catch (Exception ex)
      {
        this.Error = ex;
        Program.LogException(ex);
        this.CheckResult = UpdateCheckResult.Error;
        return this.CheckResult;
      }
      Version version2 = (Version) null;
      int num2 = 0;
      switch (this.VersionCompareMode)
      {
        case VersionCompareMode.SameText:
          this.NewVersionText = str2;
          break;
        case VersionCompareMode.Number:
          num2 = int.Parse(str2);
          this.NewVersionText = num2.ToString();
          break;
        case VersionCompareMode.Version:
          version2 = new Version(str2);
          this.NewVersionText = version2.ToString();
          break;
      }
      if (this.ForceUpdate)
      {
        this.CheckResult = UpdateCheckResult.DownloadRequired;
        return this.CheckResult;
      }
      switch (this.VersionCompareMode)
      {
        case VersionCompareMode.SameText:
          this.CheckResult = !string.Equals(str2, str1, StringComparison.InvariantCultureIgnoreCase) ? UpdateCheckResult.UpdateAvailable : UpdateCheckResult.NoUpdatesAvailable;
          break;
        case VersionCompareMode.Number:
          this.CheckResult = num2 > num1 ? UpdateCheckResult.UpdateAvailable : UpdateCheckResult.NoUpdatesAvailable;
          break;
        case VersionCompareMode.Version:
          this.CheckResult = version2 > version1 ? UpdateCheckResult.UpdateAvailable : UpdateCheckResult.NoUpdatesAvailable;
          break;
        default:
          this.CheckResult = UpdateCheckResult.UpdateAvailable;
          break;
      }
      return this.CheckResult;
    }

    public void UpdateWithWorker(BackgroundWorker worker)
    {
      this.m_UpdateErrorsOccured = false;
      Progress progress1 = new Progress(0L, 3L);
      Progress progress2 = new Progress();
      byte[] buffer = new byte[4096];
      worker.ReportProgress(1, (object) progress1);
      using (WebClient webClient = new WebClient())
      {
        worker.ReportProgress(-1, (object) "Downloading File List ...");
        worker.ReportProgress(-2, (object) string.Empty);
        string s;
        try
        {
          s = webClient.DownloadString(this.FileListUrl);
        }
        catch (Exception ex)
        {
          throw new ApplicationException("Could not download file-list.\nDetails: " + ex.Message, ex);
        }
        progress1.Increment();
        worker.ReportProgress(1, (object) progress1);
        worker.ReportProgress(-1, (object) "Validating File List ...");
        worker.ReportProgress(-2, (object) string.Empty);
        this.Items.Clear();
        this.m_ServerFileList = s;
        UpdateItemCollection updateItemCollection = new UpdateItemCollection();
        if (File.Exists(this.Name + "-FileList.txt"))
        {
          StringReader stringReader = new StringReader(File.ReadAllText(this.Name + "-FileList.txt"));
          while (true)
          {
            string[] strArray;
            UpdateItem updateItem;
            do
            {
              string str1 = stringReader.ReadLine();
              if (str1 != null)
              {
                strArray = str1.Split('\t');
                updateItem = new UpdateItem();
                if (strArray.Length >= 1)
                {
                  string str2 = strArray[0];
                  if (!string.IsNullOrEmpty(str2) && str2.StartsWith("./"))
                    str2 = str2.Substring(2);
                  if (!string.Equals(Path.GetFileName(str2), "Thumbs.db", StringComparison.CurrentCultureIgnoreCase))
                  {
                    long result;
                    if (strArray.Length >= 2 && long.TryParse(strArray[2], out result))
                      updateItem.LengthExpected = new long?(result);
                    updateItem.FileNameRelative = str2;
                    updateItem.FileUrl = WebPath.Combine(this.FilesBaseUrl, str2);
                    updateItem.FileName = Path.GetFullPath(Path.Combine(this.TargetDirectoryName, str2));
                    if (string.Equals(Path.GetFullPath(updateItem.FileName), Path.GetFullPath(Program.ProgramFileName), StringComparison.CurrentCultureIgnoreCase))
                    {
                      updateItem.TargetFileName = updateItem.FileName + ".tmp";
                      this.SelfUpdateDetected = true;
                      this.SelfUpdateItem = updateItem;
                    }
                    if (!string.IsNullOrEmpty(updateItem.FileName))
                      updateItemCollection.Add(updateItem);
                  }
                }
              }
              else
                goto label_19;
            }
            while (strArray.Length < 2);
            updateItem.Hash = strArray[1];
          }
        }
label_19:
        StringReader stringReader1 = new StringReader(s);
label_20:
        string[] strArray1;
        UpdateItem updateItem1;
        do
        {
          string str1 = stringReader1.ReadLine();
          if (str1 != null)
          {
            strArray1 = str1.Split('\t');
            updateItem1 = new UpdateItem();
            if (strArray1.Length >= 1)
            {
              string str2 = strArray1[0];
              if (!string.IsNullOrEmpty(str2) && str2.StartsWith("./"))
                str2 = str2.Substring(2);
              if (!string.Equals(Path.GetFileName(str2), "Thumbs.db", StringComparison.CurrentCultureIgnoreCase))
              {
                long result;
                if (strArray1.Length >= 2 && long.TryParse(strArray1[2], out result))
                  updateItem1.LengthExpected = new long?(result);
                updateItem1.FileNameRelative = str2;
                updateItem1.FileUrl = WebPath.Combine(this.FilesBaseUrl, str2);
                updateItem1.FileName = Path.GetFullPath(Path.Combine(this.TargetDirectoryName, str2));
                if (string.Equals(Path.GetFullPath(updateItem1.FileName), Path.GetFullPath(Program.ProgramFileName), StringComparison.CurrentCultureIgnoreCase))
                {
                  updateItem1.TargetFileName = updateItem1.FileName + ".tmp";
                  this.SelfUpdateDetected = true;
                  this.SelfUpdateItem = updateItem1;
                }
                if (!string.IsNullOrEmpty(updateItem1.FileName))
                  this.m_Items.Add(updateItem1);
              }
            }
          }
          else
            goto label_40;
        }
        while (strArray1.Length < 2);
        updateItem1.Hash = strArray1[1];
        using (IEnumerator<UpdateItem> enumerator = updateItemCollection.GetEnumerator())
        {
          while (enumerator.MoveNext())
          {
            UpdateItem current = enumerator.Current;
            if (current.FileName == updateItem1.FileName && current.Hash == updateItem1.Hash)
            {
              this.m_Items.Remove(updateItem1);
              break;
            }
          }
          goto label_20;
        }
label_40:
        progress1.Increment();
        worker.ReportProgress(1, (object) progress1);
        if (worker.CancellationPending)
          return;
        long maximumValue = 0;
        foreach (UpdateItem updateItem2 in (Collection<UpdateItem>) this.Items)
          maximumValue += updateItem2.Length;
        progress2.Reset(0L, maximumValue);
        worker.ReportProgress(2, (object) progress2);
        worker.ReportProgress(-1, (object) "Checking Files ...");
        Crc32Processor crc32Processor = new Crc32Processor();
        foreach (UpdateItem updateItem2 in (Collection<UpdateItem>) this.Items)
        {
          if (worker.CancellationPending)
            return;
          worker.ReportProgress(-2, (object) updateItem2.FileName);
          if (!File.Exists(updateItem2.FileName))
          {
            updateItem2.CheckStatus = UpdateCheckStatus.NotExisiting;
            updateItem2.IsUpdateRequired = true;
          }
          else
          {
            if (updateItem2.LengthExpected.HasValue)
            {
              long? lengthExpected = updateItem2.LengthExpected;
              long length = updateItem2.Length;
              if ((lengthExpected.GetValueOrDefault() != length ? 1 : (!lengthExpected.HasValue ? 1 : 0)) != 0)
              {
                updateItem2.CheckStatus = UpdateCheckStatus.LengthMismatch;
                updateItem2.IsUpdateRequired = true;
                goto label_65;
              }
            }
            using (FileStream fileStream = File.OpenRead(updateItem2.FileName))
            {
              int length;
              do
              {
                length = fileStream.Read(buffer, 0, buffer.Length);
                crc32Processor.Process(buffer, 0, length);
              }
              while (length == buffer.Length);
            }
            if (updateItem2.Hash != crc32Processor.Current.ToString("X8"))
            {
              updateItem2.CheckStatus = UpdateCheckStatus.HashMismatch;
              updateItem2.IsUpdateRequired = true;
            }
            else
              updateItem2.CheckStatus = UpdateCheckStatus.Ok;
            crc32Processor.Reset();
          }
label_65:
          progress2.Increment(updateItem2.Length);
          worker.ReportProgress(2, (object) progress2);
        }
        progress1.Increment();
        worker.ReportProgress(1, (object) progress1);
        worker.ReportProgress(-1, (object) "Downloading Files ...");
        foreach (UpdateItem updateItem2 in (Collection<UpdateItem>) this.Items)
        {
          if (worker.CancellationPending)
            break;
          if (updateItem2.IsUpdateRequired)
          {
            string fileName = updateItem2.FileName;
            string directoryName = Path.GetDirectoryName(updateItem2.TargetFileName);
            if (!Directory.Exists(directoryName))
              Directory.CreateDirectory(directoryName);
            try
            {
              using (Stream stream = webClient.OpenRead(updateItem2.FileUrl))
              {
                try
                {
                  using (FileStream fileStream = File.Create(updateItem2.TargetFileName))
                  {
                    int num1 = int.Parse(webClient.ResponseHeaders[HttpResponseHeader.ContentLength]);
                    progress2.Reset(0L, (long) num1);
                    worker.ReportProgress(2, (object) progress2);
                    int num2 = 0;
                    do
                    {
                      worker.ReportProgress(-2, (object) string.Format("Downloading: {0} - {1:N0}/{2:N0}", (object) fileName, (object) num2, (object) num1));
                      int count = stream.Read(buffer, 0, buffer.Length);
                      fileStream.Write(buffer, 0, count);
                      num2 += count;
                      progress2.CurrentValue = (long) num2;
                      worker.ReportProgress(2, (object) progress2);
                    }
                    while (num2 < num1);
                  }
                }
                catch (Exception ex)
                {
                  updateItem2.UpdateError = UpdateError.CannotUpdateFile;
                  updateItem2.Error = ex;
                  this.m_UpdateErrorsOccured = true;
                  continue;
                }
              }
            }
            catch (Exception ex)
            {
              updateItem2.UpdateError = UpdateError.UrlNotFound;
              updateItem2.Error = ex;
              this.m_UpdateErrorsOccured = true;
              continue;
            }
            if (!string.IsNullOrEmpty(updateItem2.Hash))
            {
              progress2.Reset();
              worker.ReportProgress(-2, (object) string.Format("Validating: {0}", (object) fileName));
              using (FileStream fileStream = File.OpenRead(updateItem2.TargetFileName))
              {
                int length;
                do
                {
                  length = fileStream.Read(buffer, 0, buffer.Length);
                  crc32Processor.Process(buffer, 0, length);
                }
                while (length != 0);
              }
              if (updateItem2.Hash != crc32Processor.Current.ToString("X8"))
                throw new ApplicationException("Downloaded file is invalid. Hash of downloaded file and hash in server manifest does not match.\nFile: " + updateItem2.TargetFileName);
              crc32Processor.Reset();
            }
          }
        }
      }
    }

    public void SaveVersion()
    {
      File.WriteAllText(this.VersionFileName, this.m_ServerVersionRawText, Encoding.Default);
      File.WriteAllText(this.Name + "-FileList.txt", this.m_ServerFileList, Encoding.Default);
      this.m_CurrentVersionText = this.m_NewVersionText;
    }

    public UpdateItemCollection Items
    {
      get
      {
        return this.m_Items;
      }
    }

    public bool SelfUpdateDetected
    {
      get
      {
        return this.m_SelfUpdateDetected;
      }
      set
      {
        this.m_SelfUpdateDetected = value;
      }
    }

    public UpdateItem SelfUpdateItem
    {
      get
      {
        return this.m_SelfUpdateItem;
      }
      set
      {
        this.m_SelfUpdateItem = value;
      }
    }

    public void DownloadPatchlog()
    {
      if (string.IsNullOrEmpty(this.ChangelogFileUrl))
        return;
      try
      {
        this.CurrentChangelog = LauncherUtility.DownloadString(this.ChangelogFileUrl);
      }
      catch (Exception ex)
      {
        Program.LogException(ex);
      }
    }

    public bool UpdateErrorsOccured
    {
      get
      {
        return this.m_UpdateErrorsOccured;
      }
      set
      {
        this.m_UpdateErrorsOccured = value;
      }
    }
  }
}
