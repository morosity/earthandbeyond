﻿// Decompiled with JetBrains decompiler
// Type: LaunchNet7.Updateing.UpdateItem
// Assembly: LaunchNet7, Version=1.9.9.10, Culture=neutral, PublicKeyToken=null
// MVID: AF666469-2FA2-4BE0-A9D5-7003BDE93D8C
// Assembly location: D:\Games\Net-7\bin\LaunchNet7.exe

using System;
using System.IO;

namespace LaunchNet7.Updateing
{
  public class UpdateItem
  {
    private string m_FileUrl;
    private string m_FileNameRelative;
    private string m_FileName;
    private string m_TargetFileName;
    private string m_Hash;
    private bool m_IsUpdateRequired;
    private UpdateCheckStatus m_CheckStatus;
    private long? m_LengthExpected;
    private long? m_Length;
    private UpdateError m_UpdateError;
    private Exception m_Error;

    public string FileUrl
    {
      get
      {
        return this.m_FileUrl;
      }
      set
      {
        this.m_FileUrl = value;
      }
    }

    public string FileNameRelative
    {
      get
      {
        return this.m_FileNameRelative;
      }
      set
      {
        this.m_FileNameRelative = value;
      }
    }

    public string FileName
    {
      get
      {
        return this.m_FileName;
      }
      set
      {
        this.m_FileName = value;
      }
    }

    public string TargetFileName
    {
      get
      {
        if (this.m_TargetFileName == null)
          return this.m_FileName;
        return this.m_TargetFileName;
      }
      set
      {
        this.m_TargetFileName = value;
      }
    }

    public string Hash
    {
      get
      {
        return this.m_Hash;
      }
      set
      {
        this.m_Hash = value;
      }
    }

    public bool IsUpdateRequired
    {
      get
      {
        return this.m_IsUpdateRequired;
      }
      set
      {
        this.m_IsUpdateRequired = value;
      }
    }

    public UpdateCheckStatus CheckStatus
    {
      get
      {
        return this.m_CheckStatus;
      }
      set
      {
        this.m_CheckStatus = value;
      }
    }

    public long? LengthExpected
    {
      get
      {
        return this.m_LengthExpected;
      }
      set
      {
        this.m_LengthExpected = value;
      }
    }

    public long Length
    {
      get
      {
        if (!this.m_Length.HasValue)
        {
          FileInfo fileInfo = new FileInfo(this.FileName);
          this.m_Length = !fileInfo.Exists ? new long?(0L) : new long?(fileInfo.Length);
        }
        return this.m_Length.Value;
      }
    }

    public UpdateError UpdateError
    {
      get
      {
        return this.m_UpdateError;
      }
      set
      {
        this.m_UpdateError = value;
      }
    }

    public Exception Error
    {
      get
      {
        return this.m_Error;
      }
      set
      {
        this.m_Error = value;
      }
    }
  }
}
