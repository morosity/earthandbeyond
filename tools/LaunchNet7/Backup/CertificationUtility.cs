﻿// Decompiled with JetBrains decompiler
// Type: LaunchNet7.CertificationUtility
// Assembly: LaunchNet7, Version=1.9.9.10, Culture=neutral, PublicKeyToken=null
// MVID: AF666469-2FA2-4BE0-A9D5-7003BDE93D8C
// Assembly location: D:\Games\Net-7\bin\LaunchNet7.exe

using System;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace LaunchNet7
{
  public static class CertificationUtility
  {
    public static bool IsSslCertificateValid(string hostName, int port)
    {
      bool? isCertificateValid = new bool?();
      try
      {
        ServicePointManager.ServerCertificateValidationCallback = (RemoteCertificateValidationCallback) ((sender, cert, chain, errors) =>
        {
          isCertificateValid = new bool?(errors == SslPolicyErrors.None);
          return true;
        });
        HttpWebResponse response = (HttpWebResponse) WebRequest.Create("https://" + hostName + ":" + (object) port + "/certificate.html").GetResponse();
        return isCertificateValid.GetValueOrDefault(false);
      }
      finally
      {
        ServicePointManager.ServerCertificateValidationCallback = (RemoteCertificateValidationCallback) null;
      }
    }

    public static void InstallCertificate(string hostName, int port)
    {
      try
      {
        X509Certificate2 certificate = (X509Certificate2) null;
        ServicePointManager.ServerCertificateValidationCallback = (RemoteCertificateValidationCallback) ((sender, cert, chain, errors) =>
        {
          certificate = new X509Certificate2(cert);
          return true;
        });
        try
        {
          HttpWebResponse response = (HttpWebResponse) WebRequest.Create("https://" + hostName + ":" + (object) port + "/certificate.html").GetResponse();
        }
        catch
        {
        }
        if (certificate == null)
          throw new ApplicationException("Could not retrieve server certificate.");
        try
        {
          X509Store x509Store = new X509Store(StoreName.Root);
          x509Store.Open(OpenFlags.ReadWrite);
          x509Store.Add(certificate);
          x509Store.Close();
        }
        catch (Exception ex)
        {
          throw new ApplicationException("Could not add certificate.\nDetails: " + ex.Message, ex);
        }
      }
      finally
      {
        ServicePointManager.ServerCertificateValidationCallback = (RemoteCertificateValidationCallback) null;
      }
    }

    internal static void InstallCertificate()
    {
      throw new NotImplementedException();
    }
  }
}
