﻿// Decompiled with JetBrains decompiler
// Type: LaunchNet7.LaunchSetting
// Assembly: LaunchNet7, Version=1.9.9.10, Culture=neutral, PublicKeyToken=null
// MVID: AF666469-2FA2-4BE0-A9D5-7003BDE93D8C
// Assembly location: D:\Games\Net-7\bin\LaunchNet7.exe

using System.IO;

namespace LaunchNet7
{
  public class LaunchSetting
  {
    private int m_AuthenticationPort = 443;
    private bool m_UseSecureAuthentication = true;
    private string m_ClientPath;
    private string m_BaseFolder;
    private string m_Hostname;
    private string m_LocalIP;
    private string m_RegistrationHostname;
    private string m_LaunchName;
    private bool m_UseClientDetours;
    private bool m_UseLocalCert;
    private bool m_UsePacketOpt;
    private bool m_DebugLaunch;
    private bool m_UseExperimentalReorder;
    private bool m_LockPort;

    public string ClientPath
    {
      get
      {
        return this.m_ClientPath;
      }
      set
      {
        this.m_ClientPath = value;
        if (string.IsNullOrEmpty(value))
          return;
        this.m_BaseFolder = Directory.GetParent(Path.GetDirectoryName(value)).FullName;
      }
    }

    public string BaseFolder
    {
      get
      {
        return this.m_BaseFolder;
      }
    }

    public string AuthLoginFileName
    {
      get
      {
        return Path.Combine(this.BaseFolder, "release\\authlogin.dll");
      }
    }

    public string IniDirectoryName
    {
      get
      {
        return Path.Combine(this.BaseFolder, "Data\\client\\ini");
      }
    }

    public string CommonDirectoryName
    {
      get
      {
        return Path.Combine(this.BaseFolder, "Data\\common");
      }
    }

    public int AuthenticationPort
    {
      get
      {
        return this.m_AuthenticationPort;
      }
      set
      {
        this.m_AuthenticationPort = value;
      }
    }

    public bool UseSecureAuthentication
    {
      get
      {
        return this.m_UseSecureAuthentication;
      }
      set
      {
        this.m_UseSecureAuthentication = value;
      }
    }

    public string Hostname
    {
      get
      {
        return this.m_Hostname;
      }
      set
      {
        this.m_Hostname = value;
      }
    }

    public string LocalIP
    {
      get
      {
        return this.m_LocalIP;
      }
      set
      {
        this.m_LocalIP = value;
      }
    }

    public string RegistrationHostname
    {
      get
      {
        if (string.IsNullOrEmpty(this.m_RegistrationHostname))
          return this.m_Hostname;
        return this.m_RegistrationHostname;
      }
      set
      {
        this.m_RegistrationHostname = value;
      }
    }

    public string LaunchName
    {
      get
      {
        return this.m_LaunchName;
      }
      set
      {
        this.m_LaunchName = value;
      }
    }

    public bool UseClientDetours
    {
      get
      {
        return this.m_UseClientDetours;
      }
      set
      {
        this.m_UseClientDetours = value;
      }
    }

    public bool UseLocalCert
    {
      get
      {
        return this.m_UseLocalCert;
      }
      set
      {
        this.m_UseLocalCert = value;
      }
    }

    public bool UsePacketOpt
    {
      get
      {
        return this.m_UsePacketOpt;
      }
      set
      {
        this.m_UsePacketOpt = value;
      }
    }

    public bool DebugLaunch
    {
      get
      {
        return this.m_DebugLaunch;
      }
      set
      {
        this.m_DebugLaunch = value;
      }
    }

    public bool UseExperimentalReorder
    {
      get
      {
        return this.m_UseExperimentalReorder;
      }
      set
      {
        this.m_UseExperimentalReorder = value;
      }
    }

    public bool LockPort
    {
      get
      {
        return this.m_LockPort;
      }
      set
      {
        this.m_LockPort = value;
      }
    }
  }
}
