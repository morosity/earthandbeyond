﻿// Decompiled with JetBrains decompiler
// Type: LaunchNet7.Configuration.AutoUpdateConfigurationElement
// Assembly: LaunchNet7, Version=1.9.9.10, Culture=neutral, PublicKeyToken=null
// MVID: AF666469-2FA2-4BE0-A9D5-7003BDE93D8C
// Assembly location: D:\Games\Net-7\bin\LaunchNet7.exe

using System;
using System.Configuration;

namespace LaunchNet7.Configuration
{
  public class AutoUpdateConfigurationElement : ConfigurationElement
  {
    [ConfigurationProperty("", IsDefaultCollection = true)]
    public AutoUpdateTaskConfigurationElementCollection AutoUpdateTasks
    {
      get
      {
        return (AutoUpdateTaskConfigurationElementCollection) this[""];
      }
    }

    [ConfigurationProperty("timeout", DefaultValue = "0:00:10")]
    public TimeSpan Timeout
    {
      get
      {
        return (TimeSpan) this["timeout"];
      }
      set
      {
        this["timeout"] = (object) value;
      }
    }
  }
}
