﻿// Decompiled with JetBrains decompiler
// Type: LaunchNet7.Configuration.AutoUpdateTaskConfigurationElementCollection
// Assembly: LaunchNet7, Version=1.9.9.10, Culture=neutral, PublicKeyToken=null
// MVID: AF666469-2FA2-4BE0-A9D5-7003BDE93D8C
// Assembly location: D:\Games\Net-7\bin\LaunchNet7.exe

using System.Configuration;

namespace LaunchNet7.Configuration
{
  public class AutoUpdateTaskConfigurationElementCollection : ConfigurationElementCollection
  {
    protected override ConfigurationElement CreateNewElement()
    {
      return (ConfigurationElement) new AutoUpdateTaskConfigurationElement();
    }

    protected override object GetElementKey(ConfigurationElement element)
    {
      return (object) ((AutoUpdateTaskConfigurationElement) element).Name;
    }

    public override ConfigurationElementCollectionType CollectionType
    {
      get
      {
        return ConfigurationElementCollectionType.BasicMap;
      }
    }

    protected override string ElementName
    {
      get
      {
        return "autoUpdateTask";
      }
    }
  }
}
