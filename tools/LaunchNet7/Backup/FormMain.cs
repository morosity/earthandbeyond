﻿// Decompiled with JetBrains decompiler
// Type: LaunchNet7.FormMain
// Assembly: LaunchNet7, Version=1.9.9.10, Culture=neutral, PublicKeyToken=null
// MVID: AF666469-2FA2-4BE0-A9D5-7003BDE93D8C
// Assembly location: D:\Games\Net-7\bin\LaunchNet7.exe

using LaunchNet7.Configuration;
using LaunchNet7.Properties;
using LaunchNet7.Updateing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace LaunchNet7
{
  public class FormMain : Form
  {
    private LaunchSetting m_CurrentSetting = new LaunchSetting();
    private const int CHECK_PORT = 3806;
    private const int MVAS_VER = 300;
    private HostConfigurationElement m_LastSelectedServer;
    private BackgroundWorker m_Worker;
    private string m_FullErrorText;
    private DateTime m_LastCheck;
    private IContainer components;
    private Button c_Button_Play;
    private Button c_Button_Cancel;
    private ProgressBar c_ProgressBar;
    private Label c_Status;
    private GroupBox c_GroupBox_Server;
    private ComboBox c_ComboBox_Servers;
    private Button c_Button_Check;
    private Label label3;
    private Label c_ServerStatus;
    private GroupBox c_GroupBox_Authentication;
    private Label label1;
    private TextBox c_TextBox_Port;
    private CheckBox c_CheckBox_SecureAuthentication;
    private CheckBox c_CheckBox_ClientDetours;
    private GroupBox c_GroupBox_Misc;
    private StatusStrip statusStrip1;
    private ToolStripStatusLabel c_ToolStripStatusLabel;
    private ToolStripStatusLabel c_ToolStripStatusLabel_Version;
    private ToolStripStatusLabel c_ToolStripStatusLabel_Filler;
    private Panel c_Panel_WebBrowser;
    private ToolStrip toolStrip1;
    private ToolStripButton c_ToolStripButton_Browser_OpenInNewWindow;
    private ToolStripButton c_ToolStripButton_WebBrowser_Back;
    private ToolStripButton c_ToolStripButton_WebBrowser_Forward;
    private ToolStripSeparator toolStripSeparator1;
    private ToolStripMenuItem c_ToolStripMenuItem_Main_Launcher;
    private ToolStripMenuItem c_ToolStripMenuItem_Main_Launcher_CheckForUpdates;
    private ToolStripSeparator toolStripMenuItem1;
    private ToolStripMenuItem c_ToolStripMenuItem_Main_Launcher_Quit;
    private ToolStripMenuItem helpToolStripMenuItem;
    private ToolStripMenuItem helpPageToolStripMenuItem;
    private ToolStripMenuItem commonErrorsToolStripMenuItem;
    private ToolStripMenuItem helpForumToolStripMenuItem;
    private ToolStripMenuItem forumsToolStripMenuItem;
    private ToolStripSeparator toolStripSeparator2;
    private ToolStripMenuItem checkCertificateToolStripMenuItem;
    private ToolStripMenuItem iNV300ToolStripMenuItem;
    private ToolStripMenuItem advancedToolStripMenuItem;
    private MenuStrip c_MenuStrip_Main;
    private CheckBox c_CheckBox_LocalCert;
    private ToolStripMenuItem c_Menu_Main_Help_BrowseUserFolder_ToolStripMenuItem;
    private ToolStripSeparator toolStripMenuItem2;
    private ToolStripMenuItem c_Menu_Main_Help_ShowErrorLog_ToolStripMenuItem;
    private ToolStripMenuItem installCertificateToolStripMenuItem;
    private ToolStripMenuItem c_MainMenu_Launcher_ResetUpdates_ToolStripMenuItem;
    private TextBox c_TextBox_Client;
    private Button c_Button_Browse;
    private GroupBox c_GroupBox_Client;
    private WebBrowser c_WebBrowser;
    private CheckBox c_CheckBox_PacketOpt;
    private ToolStripMenuItem supportTicketToolStripMenuItem;
    private ToolStripSeparator toolStripSeparator3;
    private ToolStripMenuItem flushDNSToolStripMenuItem;
    private ToolStripMenuItem dNSLookupToolStripMenuItem;
    private CheckBox cbDebugLaunch;
    private ComboBox cbIPAddresses;
    private PictureBox c_Background;
    private Label label4;
    private CheckBox c_CheckBox_ExperimentalReorder;
    private CheckBox chkLockPort;

    public FormMain()
    {
      this.InitializeComponent();
    }

    private void FormMain_Load(object sender, EventArgs e)
    {
      FormMain formMain = this;
      formMain.Text = formMain.Text + " v" + AssemblyFileInfo.Current.FileVersion.ToString(3);
      this.c_ToolStripStatusLabel.Text = string.Empty;
      this.c_Background.Image = (Image) LauncherResources.LaunchNet7;
      this.c_Status.Text = string.Empty;
      this.c_ServerStatus.Text = string.Empty;
      this.c_CheckBox_ClientDetours.Enabled = false;
      this.DoDisplayWebpage("http://www.net-7.org/LauncherUpdates/");
      this.DoRestoreWindowPosition();
      this.DoFillClientPath();
      try
      {
        this.c_CheckBox_LocalCert.Checked = Settings.Default.UseLocalCert;
        this.c_CheckBox_PacketOpt.Checked = Settings.Default.UsePacketOpt;
        this.c_CheckBox_ExperimentalReorder.Checked = Settings.Default.UseExperimentalReorder;
        this.cbDebugLaunch.Checked = Settings.Default.DebugLaunch;
        if (!string.IsNullOrEmpty(Settings.Default.AuthenticationPort))
          this.c_TextBox_Port.Text = Settings.Default.AuthenticationPort;
        this.c_CheckBox_SecureAuthentication.Checked = Settings.Default.UseSecureAuthentication;
        this.chkLockPort.Checked = Settings.Default.LockPort;
      }
      catch
      {
      }
      foreach (object address in Dns.GetHostByName(Dns.GetHostName()).AddressList)
        this.cbIPAddresses.Items.Add((object) address.ToString());
      int selectedIp = Settings.Default.SelectedIP;
      if (this.cbIPAddresses.Items.Count > Settings.Default.SelectedIP)
        this.cbIPAddresses.SelectedIndex = Settings.Default.SelectedIP;
      else
        this.cbIPAddresses.SelectedIndex = 0;
      this.DoUpdateUI();
      this.DoCheckForUpdates();
    }

    private void DoUpdateUI()
    {
    }

    private void c_Button_Cancel_Click(object sender, EventArgs e)
    {
      if (this.m_Worker != null && this.m_Worker.IsBusy)
        this.m_Worker.CancelAsync();
      else
        this.Close();
    }

    private void c_Button_Browse_Click(object sender, EventArgs e)
    {
      string clientPath;
      if (!this.DoTryBrowseClient(out clientPath) || !File.Exists(clientPath))
        return;
      this.SetClientPathAndStoreSettings(clientPath);
    }

    private void c_ComboBox_Emulators_SelectedIndexChanged(object sender, EventArgs e)
    {
      this.DoDisplayHosts();
    }

    private void DisplayErrorMessage(string message, Exception e)
    {
      int num = (int) MessageBox.Show((IWin32Window) this, message + "\nDetails: " + e.Message, Program.Title + " - Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
    }

    private void DisplayWarningMessage(string message)
    {
      int num = (int) MessageBox.Show((IWin32Window) this, message, Program.Title + " - Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
    }

    private void DisplayInformalMessage(string message)
    {
      int num = (int) MessageBox.Show((IWin32Window) this, message, Program.Title + " - Information", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
    }

    private void c_ComboBox_Servers_SelectedIndexChanged(object sender, EventArgs e)
    {
      this.m_CurrentSetting.Hostname = this.c_ComboBox_Servers.Text;
      this.DoDisplayServerStatus();
    }

    private void c_Button_Check_Click(object sender, EventArgs e)
    {
      if ((DateTime.Now - this.m_LastCheck).Seconds <= 30 && !(this.c_ServerStatus.Text == "OFFLINE") && !(this.c_ServerStatus.Text == ""))
        return;
      this.DoCheckServerStatus();
    }

    private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
    {
      this.DoSaveWindowPosition();
    }

    private void c_Button_Play_Click(object sender, EventArgs e)
    {
      this.DoLaunch(this.m_CurrentSetting);
    }

    private void c_CheckBox_SecureAuthentication_CheckedChanged(object sender, EventArgs e)
    {
      if (this.c_CheckBox_SecureAuthentication.Checked)
        this.c_TextBox_Port.Text = "443";
      else
        this.c_TextBox_Port.Text = "443";
    }

    private void c_ToolStripStatusLabel_Click(object sender, EventArgs e)
    {
      if (string.IsNullOrEmpty(this.m_FullErrorText))
        return;
      int num = (int) MessageBox.Show((IWin32Window) this, "Following errors occured while checking for updates:\n\n" + this.m_FullErrorText, Program.TitleForErrorMessages, MessageBoxButtons.OK, MessageBoxIcon.Hand);
    }

    private void c_ToolStripMenuItem_Main_File_Quit_Click(object sender, EventArgs e)
    {
      this.DoQuit();
    }

    private void c_ToolStripMenuItem_Main_Help_CheckForUpdates_Click(object sender, EventArgs e)
    {
      this.DoCheckForUpdates();
    }

    private void c_ToolStripButton_Browser_OpenInNewWindow_Click(object sender, EventArgs e)
    {
      Process.Start(this.c_WebBrowser.Url.ToString());
    }

    private void c_ToolStripButton_WebBrowser_Back_Click(object sender, EventArgs e)
    {
      this.c_WebBrowser.GoBack();
    }

    private void c_ToolStripButton_WebBrowser_Forward_Click(object sender, EventArgs e)
    {
      this.c_WebBrowser.GoForward();
    }

    private void c_ToolStripMenuItem_Main_Launcher_CheckForUpdates_Click(object sender, EventArgs e)
    {
      this.DoCheckForUpdates();
    }

    private void DoLaunch(LaunchSetting setting)
    {
      if (string.IsNullOrEmpty(this.m_CurrentSetting.ClientPath))
        this.DisplayInformalMessage("The location of the client.exe could not be determined automatically. Please specify the location manually.");
      else if (!File.Exists(this.m_CurrentSetting.ClientPath))
      {
        this.DisplayInformalMessage("The location of the client.exe is invalid. The file cannot be found. Please set the location.");
      }
      else
      {
        int result;
        if (!int.TryParse(this.c_TextBox_Port.Text, out result))
        {
          this.DisplayWarningMessage("Please enter a valid port number.");
          this.c_TextBox_Port.SelectAll();
          this.c_TextBox_Port.Focus();
        }
        else
        {
          if (result > 0)
          {
            if (result <= (int) ushort.MaxValue)
            {
              try
              {
                setting.UseSecureAuthentication = this.c_CheckBox_SecureAuthentication.Checked;
                setting.AuthenticationPort = result;
                setting.DebugLaunch = this.cbDebugLaunch.Checked;
                setting.LockPort = this.chkLockPort.Checked;
                Settings.Default.ServerIndex = this.c_ComboBox_Servers.SelectedIndex;
                Settings.Default.ServerList.Clear();
                for (int index = 0; index < this.c_ComboBox_Servers.Items.Count; ++index)
                  Settings.Default.ServerList.Add(this.c_ComboBox_Servers.Items[index].ToString());
                if (!Settings.Default.ServerList.Contains(this.c_ComboBox_Servers.Text))
                  Settings.Default.ServerList.Add(this.c_ComboBox_Servers.Text);
                if (this.c_ComboBox_Servers.SelectedIndex == -1)
                {
                  setting.Hostname = this.c_ComboBox_Servers.Text;
                  Settings.Default.ServerIndex = 0;
                }
                else
                {
                  Settings.Default.ServerIndex = this.c_ComboBox_Servers.SelectedIndex;
                  setting.Hostname = this.c_ComboBox_Servers.Items[this.c_ComboBox_Servers.SelectedIndex].ToString();
                }
                setting.UseLocalCert = this.c_CheckBox_LocalCert.Checked;
                setting.UsePacketOpt = this.c_CheckBox_PacketOpt.Checked;
                setting.UseExperimentalReorder = this.c_CheckBox_ExperimentalReorder.Checked;
                if (this.cbIPAddresses.SelectedIndex > 0)
                  setting.LocalIP = this.cbIPAddresses.Items[this.cbIPAddresses.SelectedIndex].ToString();
                Settings.Default.UseLocalCert = this.c_CheckBox_LocalCert.Checked;
                Settings.Default.UsePacketOpt = this.c_CheckBox_PacketOpt.Checked;
                Settings.Default.UseExperimentalReorder = this.c_CheckBox_ExperimentalReorder.Checked;
                Settings.Default.DebugLaunch = this.cbDebugLaunch.Checked;
                Settings.Default.SelectedIP = this.cbIPAddresses.SelectedIndex;
                Settings.Default.LockPort = this.chkLockPort.Checked;
                Settings.Default.LastServerName = this.c_ComboBox_Servers.Items.Count <= 0 || this.c_ComboBox_Servers.SelectedIndex != -1 ? this.c_ComboBox_Servers.Items[this.c_ComboBox_Servers.SelectedIndex].ToString() : this.c_ComboBox_Servers.Text;
                Settings.Default.UseSecureAuthentication = this.c_CheckBox_SecureAuthentication.Checked;
                Settings.Default.AuthenticationPort = this.c_TextBox_Port.Text;
                Settings.Default.Save();
                try
                {
                  if (!CertificationUtility.IsSslCertificateValid(setting.Hostname, setting.AuthenticationPort))
                    CertificationUtility.InstallCertificate(setting.Hostname, setting.AuthenticationPort);
                }
                catch
                {
                }
                setting.LaunchName = "NET7MP";
                new Launcher(setting).Launch();
                this.Close();
                return;
              }
              catch (UnauthorizedAccessException ex)
              {
                Program.LogException((Exception) ex, "Error launching client.");
                this.DisplayErrorMessage("Could not launch client due to security reasons.\nPlease start this application with administrative rights.", (Exception) ex);
                return;
              }
              catch (Exception ex)
              {
                Program.LogException(ex, "Could not launch client.");
                this.DisplayErrorMessage("Error launching client.", ex);
                return;
              }
            }
          }
          this.DisplayWarningMessage("Please enter a valid port number between 1 and 65535.");
          this.c_TextBox_Port.SelectAll();
          this.c_TextBox_Port.Focus();
        }
      }
    }

    private void DoDisplayServerStatus()
    {
      try
      {
        this.c_ServerStatus.Text = "CHECKING";
        this.c_Button_Check.Enabled = true;
        this.DoCheckServerStatus(this.c_ComboBox_Servers.Text);
      }
      catch (Exception ex)
      {
        Program.LogException(ex);
        this.DisplayErrorMessage("Could not retrieve server status.", ex);
        this.c_ServerStatus.Text = string.Empty;
      }
    }

    private void DoCheckServerStatus()
    {
      this.m_LastCheck = DateTime.Now;
      if (!(this.c_ServerStatus.Text != "ONLINE"))
        return;
      string text = this.c_ComboBox_Servers.Text;
      int result;
      if (!int.TryParse(this.c_TextBox_Port.Text, out result))
      {
        this.c_ServerStatus.Text = string.Empty;
      }
      else
      {
        int port = 3806;
        this.DoCheckServerStatus(text, port);
      }
    }

    private void DoCheckServerStatus(string Hostname)
    {
      this.DoCheckServerStatus(Hostname, 3806);
    }

    private void DoCheckServerStatus(string hostname, int port)
    {
      BackgroundWorker backgroundWorker = new BackgroundWorker();
      backgroundWorker.WorkerReportsProgress = false;
      backgroundWorker.WorkerSupportsCancellation = false;
      backgroundWorker.DoWork += (DoWorkEventHandler) ((sender, e) =>
      {
        IPAddress address = (IPAddress) null;
        try
        {
          address = Dns.GetHostAddresses(hostname)[0];
        }
        catch
        {
          e.Result = (object) 1;
        }
        IPEndPoint ipEndPoint = new IPEndPoint(address, port);
        using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp))
        {
          try
          {
            int socketOption = (int) socket.GetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout);
            socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 3000);
            socket.Connect((EndPoint) ipEndPoint);
            int num1 = 0;
            byte[] buffer1 = new byte[16];
            MemoryStream memoryStream = new MemoryStream(buffer1);
            int num2 = 16;
            int num3 = 1073741824 + num1;
            int num4 = 0;
            int num5 = 4096;
            int num6 = 0;
            memoryStream.Write(BitConverter.GetBytes(num2), 0, 2);
            memoryStream.Write(BitConverter.GetBytes(num5), 0, 2);
            memoryStream.Write(BitConverter.GetBytes(num4), 0, 4);
            memoryStream.Write(BitConverter.GetBytes(num6), 0, 4);
            memoryStream.Write(BitConverter.GetBytes(num3), 0, 4);
            socket.Send(buffer1, buffer1.Length, SocketFlags.None);
            for (int index = 0; index < 10; ++index)
            {
              Thread.Sleep(500);
              if (socket.Available > 0)
                break;
            }
            byte[] buffer2 = new byte[1024];
            socket.Receive(buffer2);
            if (BitConverter.ToInt16(buffer2, 2) == (short) 4097)
              e.Result = (object) 0;
            else if (BitConverter.ToInt16(buffer2, 2) == (short) 4107)
              e.Result = (object) 3;
            else
              e.Result = (object) 2;
          }
          catch
          {
            e.Result = (object) 2;
          }
          finally
          {
            socket.Close();
          }
        }
      });
      backgroundWorker.ProgressChanged += (ProgressChangedEventHandler) ((sender, e) => {});
      backgroundWorker.RunWorkerCompleted += (RunWorkerCompletedEventHandler) ((sender, e) =>
      {
        if (e.Error != null)
        {
          this.DisplayErrorMessage("Error on checking server status.", e.Error);
          this.c_ServerStatus.Text = "OFFLINE";
        }
        else
        {
          if (e.Cancelled)
            return;
          switch ((int) e.Result)
          {
            case 0:
              this.c_ServerStatus.Text = "ONLINE";
              this.DoGetPlayerCount(hostname, port);
              break;
            case 1:
              this.c_ServerStatus.Text = "ERROR";
              this.DisplayWarningMessage("Hostname cannot be resolved.");
              this.c_ComboBox_Servers.SelectAll();
              this.c_ComboBox_Servers.Focus();
              break;
            case 2:
              this.c_ServerStatus.Text = "OFFLINE";
              break;
            case 3:
              this.c_ServerStatus.Text = "STARTING";
              break;
            default:
              this.c_ServerStatus.Text = "???";
              break;
          }
        }
      });
      this.c_ServerStatus.Text = "CHECKING";
      backgroundWorker.RunWorkerAsync();
    }

    private void DoGetPlayerCount(string hostname, int port)
    {
      BackgroundWorker backgroundWorker = new BackgroundWorker();
      backgroundWorker.WorkerReportsProgress = false;
      backgroundWorker.WorkerSupportsCancellation = false;
      backgroundWorker.DoWork += (DoWorkEventHandler) ((sender, e) =>
      {
        IPAddress address = (IPAddress) null;
        try
        {
          address = Dns.GetHostAddresses(hostname)[0];
        }
        catch
        {
          e.Result = (object) -1;
        }
        IPEndPoint ipEndPoint = new IPEndPoint(address, port);
        using (Socket socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp))
        {
          try
          {
            int socketOption = (int) socket.GetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout);
            socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 3000);
            socket.Connect((EndPoint) ipEndPoint);
            byte[] buffer1 = new byte[16];
            MemoryStream memoryStream = new MemoryStream(buffer1);
            int num1 = 16;
            int num2 = 300;
            int num3 = 0;
            int num4 = 20480;
            int num5 = 0;
            memoryStream.Write(BitConverter.GetBytes(num1), 0, 2);
            memoryStream.Write(BitConverter.GetBytes(num4), 0, 2);
            memoryStream.Write(BitConverter.GetBytes(num3), 0, 4);
            memoryStream.Write(BitConverter.GetBytes(num5), 0, 4);
            memoryStream.Write(BitConverter.GetBytes(num2), 0, 4);
            socket.Send(buffer1, buffer1.Length, SocketFlags.None);
            for (int index = 0; index < 10; ++index)
            {
              Thread.Sleep(500);
              if (socket.Available > 0)
                break;
            }
            byte[] buffer2 = new byte[1024];
            socket.Receive(buffer2);
            e.Result = (object) BitConverter.ToInt16(buffer2, 12);
          }
          catch
          {
            e.Result = (object) -1;
          }
          finally
          {
            socket.Close();
          }
        }
      });
      backgroundWorker.ProgressChanged += (ProgressChangedEventHandler) ((sender, e) => {});
      backgroundWorker.RunWorkerCompleted += (RunWorkerCompletedEventHandler) ((sender, e) =>
      {
        if (e.Error != null || (short) e.Result == (short) -1)
          return;
        this.c_ServerStatus.Text += string.Format("-{0}", (object) (short) e.Result);
      });
      backgroundWorker.RunWorkerAsync();
    }

    private void DoDisplayHosts()
    {
      try
      {
        this.c_GroupBox_Misc.Enabled = true;
        foreach (object server in Settings.Default.ServerList)
          this.c_ComboBox_Servers.Items.Add(server);
        if (Settings.Default.ServerIndex > Settings.Default.ServerList.Count)
          this.c_ComboBox_Servers.SelectedIndex = 0;
        else
          this.c_ComboBox_Servers.SelectedIndex = Settings.Default.ServerIndex < 0 ? 0 : Settings.Default.ServerIndex;
        this.c_ComboBox_Servers.Items.Clear();
      }
      catch (Exception ex)
      {
        this.DisplayErrorMessage("Could not retrieve servers for emulator.", ex);
      }
    }

    private void DoDisplayWebpage(string url)
    {
      this.c_Background.Visible = false;
      this.c_Panel_WebBrowser.Visible = true;
      try
      {
        this.c_WebBrowser.Navigate(url);
      }
      catch (Exception ex)
      {
        Program.LogException(ex);
        this.c_Background.Visible = true;
        this.c_Panel_WebBrowser.Visible = false;
        int num = (int) MessageBox.Show((IWin32Window) this, ex.Message, Program.TitleForErrorMessages, MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
    }

    private void DoCheckForUpdates()
    {
      if (this.m_Worker != null && this.m_Worker.IsBusy)
      {
        int num1 = (int) MessageBox.Show((IWin32Window) this, "Another update process is already running. Please end the current update before starting a new one.", Program.TitleForWarningMessages, MessageBoxButtons.OK);
      }
      else
      {
        string fullName = new DirectoryInfo(Path.GetDirectoryName(this.c_TextBox_Client.Text)).Parent.FullName;
        BackgroundWorker backgroundWorker1 = new BackgroundWorker();
        backgroundWorker1.WorkerReportsProgress = true;
        backgroundWorker1.WorkerSupportsCancellation = true;
        backgroundWorker1.DoWork += (DoWorkEventHandler) ((sender, e) =>
        {
          BackgroundWorker backgroundWorker = (BackgroundWorker) sender;
          backgroundWorker.ReportProgress(0, (object) "Checking Configuration");
          List<Updater> updaterList = new List<Updater>();
          Updater updater1 = new Updater();
          updater1.Name = "Client";
          updater1.VersionFileUrl = "http://patch.net-7.org/Client/Version.txt";
          updater1.VersionCompareMode = VersionCompareMode.SameText;
          updater1.FilesBaseUrl = "http://patch.net-7.org/Client/Files";
          updater1.FileListUrl = "http://patch.net-7.org/Client/Files.txt";
          updater1.VersionFileName = "Version.txt";
          string str1 = UpdateUtility.ReplaceInvalidFileNameCharacters(updater1.Name) + "-";
          string versionFileName1 = updater1.VersionFileName;
          updater1.VersionFileName = Path.Combine(Path.GetDirectoryName(versionFileName1), str1 + Path.GetFileName(versionFileName1));
          updater1.TargetDirectoryName = Program.ClientRootDirectoryName;
          Updater updater2 = new Updater();
          updater2.Name = "Net7";
          updater2.VersionFileUrl = "http://patch.net-7.org/Net7/Version.txt";
          updater2.VersionCompareMode = VersionCompareMode.SameText;
          updater2.FilesBaseUrl = "http://patch.net-7.org/Net7/Files";
          updater2.FileListUrl = "http://patch.net-7.org/Net7/Files.txt";
          updater2.VersionFileName = "Version.txt";
          string str2 = UpdateUtility.ReplaceInvalidFileNameCharacters(updater2.Name) + "-";
          string versionFileName2 = updater2.VersionFileName;
          updater2.VersionFileName = Path.Combine(Path.GetDirectoryName(versionFileName2), str2 + Path.GetFileName(versionFileName2));
          updater2.TargetDirectoryName = "";
          updaterList.Add(updater2);
          updaterList.Add(updater1);
          Progress progress = new Progress(0L, (long) (updaterList.Count * 3));
          foreach (Updater updater3 in updaterList)
          {
            if (backgroundWorker.CancellationPending)
            {
              e.Cancel = true;
              return;
            }
            backgroundWorker.ReportProgress(progress.ToProgressPercentage());
            backgroundWorker.ReportProgress(0, (object) ("Checking Updates For " + updater3.Name));
            switch (updater3.CheckForUpdates())
            {
              case UpdateCheckResult.UpdateAvailable:
              case UpdateCheckResult.DownloadRequired:
                progress.Increment();
                backgroundWorker.ReportProgress(progress.ToProgressPercentage());
                backgroundWorker.ReportProgress(0, (object) string.Format("Downloading Changelog for '{0}'", (object) updater3.Name));
                updater3.DownloadPatchlog();
                progress.Increment();
                backgroundWorker.ReportProgress(progress.ToProgressPercentage());
                backgroundWorker.ReportProgress(0, (object) ("Updating " + updater3.Name));
                if (((DialogResult?) this.Invoke((Delegate) new FormMain.DoDisplayUpdateCallback(this.DoDisplayUpdate), (object) updater3)).GetValueOrDefault() == DialogResult.Cancel)
                {
                  e.Cancel = true;
                  return;
                }
                progress.Increment();
                backgroundWorker.ReportProgress(progress.ToProgressPercentage());
                if (updater3.SelfUpdateDetected && updater3.SelfUpdateItem.CheckStatus != UpdateCheckStatus.Ok)
                {
                  this.DoRunSelfUpdate(updater3.SelfUpdateItem.TargetFileName);
                  e.Cancel = true;
                  return;
                }
                continue;
              default:
                progress.Increment();
                progress.Increment();
                progress.Increment();
                backgroundWorker.ReportProgress(progress.ToProgressPercentage());
                continue;
            }
          }
          e.Result = (object) updaterList;
        });
        backgroundWorker1.ProgressChanged += (ProgressChangedEventHandler) ((sender, e) =>
        {
          if (e.UserState != null)
            this.c_Status.Text = e.UserState.ToString();
          else
            this.c_ProgressBar.Value = e.ProgressPercentage;
        });
        backgroundWorker1.RunWorkerCompleted += (RunWorkerCompletedEventHandler) ((sender, e) =>
        {
          this.DoEnableUI();
          if (e.Error != null)
          {
            this.DisplayErrorMessage("Error on client validation.", e.Error);
            this.Close();
          }
          else if (e.Cancelled)
          {
            this.Close();
          }
          else
          {
            this.DoFillServerList();
            this.m_FullErrorText = string.Empty;
            List<Updater> result = e.Result as List<Updater>;
            if (result != null)
            {
              int num2 = 0;
              StringBuilder stringBuilder = new StringBuilder();
              foreach (Updater updater in result)
              {
                if (string.Equals(updater.Name, "Net7", StringComparison.InvariantCultureIgnoreCase))
                {
                  string str = string.Empty;
                  if (!string.IsNullOrEmpty(updater.CurrentVersionText))
                    str = str + "Current Version: " + updater.CurrentVersionText;
                  if (!string.Equals(updater.CurrentVersionText, updater.NewVersionText, StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(updater.NewVersionText))
                  {
                    if (str.Length > 0)
                      str += ", ";
                    str = str + "Server Version: " + updater.NewVersionText;
                  }
                  this.c_ToolStripStatusLabel_Version.Text = str;
                  this.c_ToolStripStatusLabel_Version.Visible = str.Length > 0;
                }
                if (updater.CheckResult == UpdateCheckResult.Timeout || updater.CheckResult == UpdateCheckResult.Error)
                {
                  ++num2;
                  if (stringBuilder.Length > 0)
                    stringBuilder.Append("\n\n");
                  if (updater.CheckResult == UpdateCheckResult.Timeout)
                    stringBuilder.Append("Timeout while checking updates for '" + updater.Name + "'");
                  else if (updater.CheckResult == UpdateCheckResult.Error)
                    stringBuilder.Append("Error while checking updates for '" + updater.Name + "'");
                  if (updater.Error != null)
                    stringBuilder.Append("\nDetails: " + updater.Error.Message);
                }
              }
              if (num2 > 0)
              {
                this.m_FullErrorText = stringBuilder.ToString();
                this.c_ToolStripStatusLabel.Text = "Errors occured while checking for updates (click here for details)";
                this.c_ToolStripStatusLabel.Image = (Image) LauncherResources.Warning_16x16x32;
              }
              else
              {
                this.m_FullErrorText = (string) null;
                this.c_ToolStripStatusLabel.Text = string.Empty;
                this.c_ToolStripStatusLabel.Image = (Image) null;
              }
            }
          }
          this.DoDisplayDefaultStatus();
        });
        this.m_Worker = backgroundWorker1;
        this.DoDisableUI();
        backgroundWorker1.RunWorkerAsync();
      }
    }

    private void DoRunSelfUpdate(string replaceFileName)
    {
      string tempFileName = Path.GetTempFileName();
      string str = Path.ChangeExtension(tempFileName, ".exe");
      if (File.Exists(str))
        File.Delete(str);
      File.Move(tempFileName, str);
      File.WriteAllBytes(str, LauncherResources.ExeUpdater);
      Process.Start(str, string.Format("-exeFileName \"{0}\" -replaceFileName \"{1}\" -waitForPid {2}", (object) Path.GetFullPath(Application.ExecutablePath), (object) Path.GetFullPath(replaceFileName), (object) Process.GetCurrentProcess().Id));
    }

    private void DoDisplayDefaultStatus()
    {
      this.c_Status.Text = "Please select a server and hit play.";
    }

    private DialogResult DoDisplayUpdate(Updater updater)
    {
      if (updater == null)
        return DialogResult.None;
      using (FormUpdate formUpdate = new FormUpdate(updater))
        return formUpdate.ShowDialog((IWin32Window) this);
    }

    private void DoRestoreWindowPosition()
    {
      Settings.Default.Properties["FormMainPosition"].DefaultValue = (object) Point.Empty;
      Point formMainPosition = Settings.Default.FormMainPosition;
      if (!(formMainPosition != Point.Empty))
        return;
      this.Top = formMainPosition.Y;
      this.Left = formMainPosition.X;
    }

    private void DoFillServerList()
    {
      foreach (object server in Settings.Default.ServerList)
        this.c_ComboBox_Servers.Items.Add(server);
      if (Settings.Default.ServerList.Count < Settings.Default.ServerIndex)
        this.c_ComboBox_Servers.SelectedIndex = 0;
      else
        this.c_ComboBox_Servers.SelectedIndex = Settings.Default.ServerIndex;
    }

    private void DoFillClientPath()
    {
      string str = (string) null;
      try
      {
        str = Settings.Default.ClientPath;
      }
      catch
      {
      }
      if (!string.IsNullOrEmpty(str) && !File.Exists(str))
        str = (string) null;
      if (string.IsNullOrEmpty(str) || !File.Exists(str))
      {
        string clientPath1 = (string) null;
        string path1 = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles), "EA GAMES\\Earth & Beyond\\release\\client.exe");
        if (File.Exists(path1))
        {
          clientPath1 = path1;
        }
        else
        {
          string pathRoot = Path.GetPathRoot(Environment.GetFolderPath(Environment.SpecialFolder.System));
          string path2 = Path.Combine(pathRoot, "Program Files\\EA GAMES\\Earth & Beyond\\release\\client.exe");
          if (File.Exists(path2))
          {
            clientPath1 = path2;
          }
          else
          {
            string clientPath2 = Path.Combine(pathRoot, "Program Files (x86)\\EA GAMES\\Earth & Beyond\\release\\client.exe");
            if (File.Exists(clientPath2))
              clientPath1 = clientPath2;
            else if (this.DoTryBrowseClient(out clientPath2))
              clientPath1 = clientPath2;
          }
        }
        this.SetClientPathAndStoreSettings(clientPath1);
      }
      else
        this.SetClientPath(str);
    }

    public void SetClientPath(string clientPath)
    {
      Program.ClientFileName = clientPath;
      this.c_TextBox_Client.Text = clientPath;
      this.m_CurrentSetting.ClientPath = clientPath;
    }

    public void SetClientPathAndStoreSettings(string clientPath)
    {
      this.SetClientPath(clientPath);
      try
      {
        Settings.Default.ClientPath = clientPath;
        Settings.Default.Save();
      }
      catch
      {
      }
    }

    private void DoDisableUI()
    {
      this.c_MenuStrip_Main.Enabled = false;
      this.c_GroupBox_Authentication.Enabled = false;
      this.c_GroupBox_Misc.Enabled = false;
      this.c_GroupBox_Client.Enabled = false;
      this.c_GroupBox_Server.Enabled = false;
      this.c_Button_Play.Enabled = false;
    }

    private void DoEnableUI()
    {
      this.c_MenuStrip_Main.Enabled = true;
      this.c_GroupBox_Misc.Enabled = true;
      this.c_GroupBox_Client.Enabled = true;
      this.c_GroupBox_Server.Enabled = true;
      this.c_Button_Play.Enabled = true;
    }

    private void DoQuit()
    {
      this.Close();
    }

    private bool DoTryBrowseClient(out string clientPath)
    {
      string currentDirectory = Environment.CurrentDirectory;
      try
      {
        using (OpenFileDialog openFileDialog = new OpenFileDialog())
        {
          openFileDialog.Title = "Select Earth & Beyond Client (<InstallDir>\\Release\\client.exe)";
          openFileDialog.Filter = "Earth & Beyond Client (client.exe)|client.exe|All Files (*.*)|*.*";
          openFileDialog.Multiselect = false;
          if (openFileDialog.ShowDialog() == DialogResult.OK)
          {
            clientPath = openFileDialog.FileName;
            return true;
          }
          clientPath = (string) null;
          return false;
        }
      }
      finally
      {
        Environment.CurrentDirectory = currentDirectory;
      }
    }

    private void DoSaveWindowPosition()
    {
      Settings.Default.FormMainPosition = new Point(this.Left, this.Top);
      Settings.Default.Save();
    }

    private void c_ComboBox_Servers_TextUpdate(object sender, EventArgs e)
    {
      this.c_ServerStatus.Text = string.Empty;
      this.c_Button_Check.Enabled = true;
    }

    private void commonErrorsToolStripMenuItem_Click(object sender, EventArgs e)
    {
      try
      {
        Process.Start("http://forum.enb-emulator.com/index.php?s=&showtopic=3610&view=findpost&p=34465");
      }
      catch
      {
      }
    }

    private void helpForumToolStripMenuItem_Click(object sender, EventArgs e)
    {
      try
      {
        Process.Start("http://forum.enb-emulator.com/index.php?showforum=175");
      }
      catch
      {
      }
    }

    private void forumsToolStripMenuItem_Click(object sender, EventArgs e)
    {
      try
      {
        Process.Start("http://forum.enb-emulator.com/");
      }
      catch
      {
      }
    }

    private void iNV300ToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.DoCheckCertificate();
    }

    private void checkCertificateToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.DoCheckCertificate();
    }

    private void DoCheckCertificate()
    {
      int result;
      if (!int.TryParse(this.c_TextBox_Port.Text, out result))
      {
        this.c_TextBox_Port.SelectAll();
        int num = (int) MessageBox.Show((IWin32Window) this, "Please enter a valid port number.", Program.TitleForWarningMessages, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
      }
      else
      {
        string text = this.c_ComboBox_Servers.Text;
        if (string.IsNullOrEmpty(text))
        {
          this.c_ComboBox_Servers.SelectAll();
          int num = (int) MessageBox.Show((IWin32Window) this, "Please enter a hostname.", Program.TitleForWarningMessages, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        else
        {
          try
          {
            if (!CertificationUtility.IsSslCertificateValid(text, result))
            {
              if (MessageBox.Show((IWin32Window) this, string.Format("Certificate for \"{0}\" is not valid or not installed.\nInstall certificate now?", (object) text), Program.TitleForWarningMessages, MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation) != DialogResult.OK)
                return;
              this.DoInstallCertificate(text, result);
            }
            else
            {
              int num = (int) MessageBox.Show((IWin32Window) this, "Certificate is valid.", Program.TitleForInformationMessages, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
          }
          catch (Exception ex)
          {
            Program.LogException(ex, "Could not check ssl certificate status.");
            this.DisplayErrorMessage("Could not check ssl certificate status.", ex);
          }
        }
      }
    }

    private void DoInstallCertificate()
    {
      int result;
      if (!int.TryParse(this.c_TextBox_Port.Text, out result))
      {
        this.c_TextBox_Port.SelectAll();
        int num = (int) MessageBox.Show((IWin32Window) this, "Please enter a valid port number.", Program.TitleForWarningMessages, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
      }
      else
      {
        string text = this.c_ComboBox_Servers.Text;
        if (string.IsNullOrEmpty(text))
        {
          this.c_ComboBox_Servers.SelectAll();
          int num = (int) MessageBox.Show((IWin32Window) this, "Please enter a hostname.", Program.TitleForWarningMessages, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        else
          this.DoInstallCertificate(text, result);
      }
    }

    private void DoInstallCertificate(string hostname, int port)
    {
      try
      {
        CertificationUtility.InstallCertificate(hostname, port);
      }
      catch (Exception ex)
      {
        string message = string.Format("Could not install certificate. HostName: \"{0}\", Port: {1}", (object) hostname, (object) port);
        Program.LogException(ex, message);
        this.DisplayErrorMessage(message, ex);
      }
    }

    private void advancedToolStripMenuItem_Click(object sender, EventArgs e)
    {
      int num = (int) new FormAdvancedSettings().ShowDialog();
    }

    private void LocalCert_CheckedChanged(object sender, EventArgs e)
    {
    }

    private void c_Menu_Main_Help_BrowseUserFolder_ToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.DoBrowseUserDirectory();
    }

    private void DoBrowseUserDirectory()
    {
      try
      {
        Program.EnsureUserDirectory();
        Process.Start(Program.UserDirectoryName);
      }
      catch (Exception ex)
      {
        Program.LogException(ex, "Could not open UserDirectory.");
      }
    }

    private void c_Menu_Main_Help_ShowErrorLog_ToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.DoOpenErrorLog();
    }

    private void DoOpenErrorLog()
    {
      string errorLogFileName = Program.ErrorLogFileName;
      if (!File.Exists(errorLogFileName))
      {
        int num = (int) MessageBox.Show((IWin32Window) this, "No error log available.", Program.TitleForInformationMessages, MessageBoxButtons.OK);
      }
      else
      {
        try
        {
          Process.Start(errorLogFileName);
        }
        catch (Win32Exception ex)
        {
          try
          {
            Process.Start("Notepad.exe", errorLogFileName);
          }
          catch
          {
          }
        }
        catch (Exception ex)
        {
          Program.LogException(ex, "Could not open errorlog.");
        }
      }
    }

    private void installCertificateToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.DoInstallCertificate();
    }

    private void c_MainMenu_Launcher_ResetUpdates_ToolStripMenuItem_Click(object sender, EventArgs e)
    {
      try
      {
        UpdateUtility.ResetAllUpdates();
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show((IWin32Window) this, ex.Message, Program.TitleForErrorMessages, MessageBoxButtons.OK, MessageBoxIcon.Hand);
        return;
      }
      if (MessageBox.Show((IWin32Window) this, "All updates are resetted. Do you want to start an update check now?", Program.TitleForInformationMessages, MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk) != DialogResult.OK)
        return;
      this.DoCheckForUpdates();
    }

    private void c_GroupBox_Client_Enter(object sender, EventArgs e)
    {
    }

    private void c_CheckBox_ClientDetours_CheckedChanged(object sender, EventArgs e)
    {
    }

    private void c_CheckBox_PacketOpt_CheckedChanged(object sender, EventArgs e)
    {
    }

    private void helpPageToolStripMenuItem_Click(object sender, EventArgs e)
    {
    }

    private void supportTicketToolStripMenuItem_Click(object sender, EventArgs e)
    {
      try
      {
        Process.Start("http://support.net-7.org/");
      }
      catch
      {
      }
    }

    private void flushDNSToolStripMenuItem_Click(object sender, EventArgs e)
    {
      try
      {
        Process.Start("ipconfig /flushdns");
      }
      catch
      {
      }
      int num = (int) MessageBox.Show("Your DNS catch has been flushed!");
    }

    private void dNSLookupToolStripMenuItem_Click(object sender, EventArgs e)
    {
      string hostNameOrAddress = "play.net-7.org";
      int num1 = (int) this.InputBox("DNS Lookup", "Enter Hostname", ref hostNameOrAddress);
      try
      {
        int num2 = (int) MessageBox.Show("The IP is: " + Dns.GetHostEntry(hostNameOrAddress).AddressList[0].ToString());
      }
      catch
      {
        int num2 = (int) MessageBox.Show("Could not find Host");
      }
    }

    public DialogResult InputBox(string title, string promptText, ref string value)
    {
      Form form = new Form();
      Label label = new Label();
      TextBox textBox = new TextBox();
      Button button1 = new Button();
      Button button2 = new Button();
      form.Text = title;
      label.Text = promptText;
      textBox.Text = value;
      button1.Text = "OK";
      button2.Text = "Cancel";
      button1.DialogResult = DialogResult.OK;
      button2.DialogResult = DialogResult.Cancel;
      label.SetBounds(9, 20, 372, 13);
      textBox.SetBounds(12, 36, 372, 20);
      button1.SetBounds(228, 72, 75, 23);
      button2.SetBounds(309, 72, 75, 23);
      label.AutoSize = true;
      textBox.Anchor |= AnchorStyles.Right;
      button1.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      button2.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      form.ClientSize = new Size(396, 107);
      form.Controls.AddRange(new Control[4]
      {
        (Control) label,
        (Control) textBox,
        (Control) button1,
        (Control) button2
      });
      form.ClientSize = new Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
      form.FormBorderStyle = FormBorderStyle.FixedDialog;
      form.StartPosition = FormStartPosition.CenterScreen;
      form.MinimizeBox = false;
      form.MaximizeBox = false;
      form.AcceptButton = (IButtonControl) button1;
      form.CancelButton = (IButtonControl) button2;
      DialogResult dialogResult = form.ShowDialog();
      value = textBox.Text;
      return dialogResult;
    }

    private void c_CheckBox_ExperimentalReorder_CheckedChanged(object sender, EventArgs e)
    {
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (FormMain));
      this.c_Button_Play = new Button();
      this.c_Button_Cancel = new Button();
      this.c_ProgressBar = new ProgressBar();
      this.c_Status = new Label();
      this.c_GroupBox_Server = new GroupBox();
      this.c_CheckBox_ExperimentalReorder = new CheckBox();
      this.c_CheckBox_PacketOpt = new CheckBox();
      this.c_ServerStatus = new Label();
      this.c_ComboBox_Servers = new ComboBox();
      this.c_Button_Check = new Button();
      this.label3 = new Label();
      this.c_CheckBox_ClientDetours = new CheckBox();
      this.c_GroupBox_Authentication = new GroupBox();
      this.label1 = new Label();
      this.c_TextBox_Port = new TextBox();
      this.c_CheckBox_SecureAuthentication = new CheckBox();
      this.c_GroupBox_Misc = new GroupBox();
      this.label4 = new Label();
      this.cbIPAddresses = new ComboBox();
      this.cbDebugLaunch = new CheckBox();
      this.c_CheckBox_LocalCert = new CheckBox();
      this.chkLockPort = new CheckBox();
      this.statusStrip1 = new StatusStrip();
      this.c_ToolStripStatusLabel = new ToolStripStatusLabel();
      this.c_ToolStripStatusLabel_Filler = new ToolStripStatusLabel();
      this.c_ToolStripStatusLabel_Version = new ToolStripStatusLabel();
      this.c_Panel_WebBrowser = new Panel();
      this.c_WebBrowser = new WebBrowser();
      this.toolStrip1 = new ToolStrip();
      this.c_ToolStripButton_WebBrowser_Back = new ToolStripButton();
      this.c_ToolStripButton_WebBrowser_Forward = new ToolStripButton();
      this.toolStripSeparator1 = new ToolStripSeparator();
      this.c_ToolStripButton_Browser_OpenInNewWindow = new ToolStripButton();
      this.c_Background = new PictureBox();
      this.c_ToolStripMenuItem_Main_Launcher = new ToolStripMenuItem();
      this.c_ToolStripMenuItem_Main_Launcher_CheckForUpdates = new ToolStripMenuItem();
      this.c_MainMenu_Launcher_ResetUpdates_ToolStripMenuItem = new ToolStripMenuItem();
      this.toolStripMenuItem1 = new ToolStripSeparator();
      this.c_ToolStripMenuItem_Main_Launcher_Quit = new ToolStripMenuItem();
      this.helpToolStripMenuItem = new ToolStripMenuItem();
      this.supportTicketToolStripMenuItem = new ToolStripMenuItem();
      this.helpPageToolStripMenuItem = new ToolStripMenuItem();
      this.commonErrorsToolStripMenuItem = new ToolStripMenuItem();
      this.helpForumToolStripMenuItem = new ToolStripMenuItem();
      this.forumsToolStripMenuItem = new ToolStripMenuItem();
      this.toolStripSeparator2 = new ToolStripSeparator();
      this.checkCertificateToolStripMenuItem = new ToolStripMenuItem();
      this.installCertificateToolStripMenuItem = new ToolStripMenuItem();
      this.iNV300ToolStripMenuItem = new ToolStripMenuItem();
      this.advancedToolStripMenuItem = new ToolStripMenuItem();
      this.toolStripMenuItem2 = new ToolStripSeparator();
      this.c_Menu_Main_Help_BrowseUserFolder_ToolStripMenuItem = new ToolStripMenuItem();
      this.c_Menu_Main_Help_ShowErrorLog_ToolStripMenuItem = new ToolStripMenuItem();
      this.toolStripSeparator3 = new ToolStripSeparator();
      this.flushDNSToolStripMenuItem = new ToolStripMenuItem();
      this.dNSLookupToolStripMenuItem = new ToolStripMenuItem();
      this.c_MenuStrip_Main = new MenuStrip();
      this.c_TextBox_Client = new TextBox();
      this.c_Button_Browse = new Button();
      this.c_GroupBox_Client = new GroupBox();
      this.c_GroupBox_Server.SuspendLayout();
      this.c_GroupBox_Authentication.SuspendLayout();
      this.c_GroupBox_Misc.SuspendLayout();
      this.statusStrip1.SuspendLayout();
      this.c_Panel_WebBrowser.SuspendLayout();
      this.toolStrip1.SuspendLayout();
      ((ISupportInitialize) this.c_Background).BeginInit();
      this.c_MenuStrip_Main.SuspendLayout();
      this.c_GroupBox_Client.SuspendLayout();
      this.SuspendLayout();
      this.c_Button_Play.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.c_Button_Play.Enabled = false;
      this.c_Button_Play.Location = new Point(710, 583);
      this.c_Button_Play.Name = "c_Button_Play";
      this.c_Button_Play.Size = new Size(75, 23);
      this.c_Button_Play.TabIndex = 7;
      this.c_Button_Play.Text = "&Play";
      this.c_Button_Play.UseVisualStyleBackColor = true;
      this.c_Button_Play.Click += new EventHandler(this.c_Button_Play_Click);
      this.c_Button_Cancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
      this.c_Button_Cancel.DialogResult = DialogResult.Cancel;
      this.c_Button_Cancel.Location = new Point(12, 583);
      this.c_Button_Cancel.Name = "c_Button_Cancel";
      this.c_Button_Cancel.Size = new Size(75, 23);
      this.c_Button_Cancel.TabIndex = 5;
      this.c_Button_Cancel.Text = "&Cancel";
      this.c_Button_Cancel.UseVisualStyleBackColor = true;
      this.c_Button_Cancel.Click += new EventHandler(this.c_Button_Cancel_Click);
      this.c_ProgressBar.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.c_ProgressBar.Location = new Point(93, 583);
      this.c_ProgressBar.Name = "c_ProgressBar";
      this.c_ProgressBar.Size = new Size(611, 23);
      this.c_ProgressBar.TabIndex = 6;
      this.c_Status.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.c_Status.AutoEllipsis = true;
      this.c_Status.BorderStyle = BorderStyle.Fixed3D;
      this.c_Status.Location = new Point(12, 554);
      this.c_Status.Margin = new Padding(3);
      this.c_Status.Name = "c_Status";
      this.c_Status.Padding = new Padding(3);
      this.c_Status.Size = new Size(773, 23);
      this.c_Status.TabIndex = 4;
      this.c_Status.Text = "[Status]";
      this.c_Status.TextAlign = ContentAlignment.MiddleCenter;
      this.c_GroupBox_Server.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.c_GroupBox_Server.Controls.Add((Control) this.c_CheckBox_ExperimentalReorder);
      this.c_GroupBox_Server.Controls.Add((Control) this.c_CheckBox_PacketOpt);
      this.c_GroupBox_Server.Controls.Add((Control) this.c_ServerStatus);
      this.c_GroupBox_Server.Controls.Add((Control) this.c_ComboBox_Servers);
      this.c_GroupBox_Server.Controls.Add((Control) this.c_Button_Check);
      this.c_GroupBox_Server.Controls.Add((Control) this.label3);
      this.c_GroupBox_Server.Enabled = false;
      this.c_GroupBox_Server.Location = new Point(12, 83);
      this.c_GroupBox_Server.Name = "c_GroupBox_Server";
      this.c_GroupBox_Server.Size = new Size(377, 81);
      this.c_GroupBox_Server.TabIndex = 1;
      this.c_GroupBox_Server.TabStop = false;
      this.c_GroupBox_Server.Text = "Server";
      this.c_CheckBox_ExperimentalReorder.AutoSize = true;
      this.c_CheckBox_ExperimentalReorder.Location = new Point((int) byte.MaxValue, 42);
      this.c_CheckBox_ExperimentalReorder.Name = "c_CheckBox_ExperimentalReorder";
      this.c_CheckBox_ExperimentalReorder.Size = new Size(112, 17);
      this.c_CheckBox_ExperimentalReorder.TabIndex = 6;
      this.c_CheckBox_ExperimentalReorder.Text = "Prototype Reorder";
      this.c_CheckBox_ExperimentalReorder.UseVisualStyleBackColor = true;
      this.c_CheckBox_ExperimentalReorder.CheckedChanged += new EventHandler(this.c_CheckBox_ExperimentalReorder_CheckedChanged);
      this.c_CheckBox_PacketOpt.AutoSize = true;
      this.c_CheckBox_PacketOpt.Location = new Point((int) byte.MaxValue, 18);
      this.c_CheckBox_PacketOpt.Name = "c_CheckBox_PacketOpt";
      this.c_CheckBox_PacketOpt.Size = new Size(120, 17);
      this.c_CheckBox_PacketOpt.TabIndex = 2;
      this.c_CheckBox_PacketOpt.Text = "Packet Optimization";
      this.c_CheckBox_PacketOpt.UseVisualStyleBackColor = true;
      this.c_CheckBox_PacketOpt.UseWaitCursor = true;
      this.c_CheckBox_PacketOpt.CheckedChanged += new EventHandler(this.c_CheckBox_PacketOpt_CheckedChanged);
      this.c_ServerStatus.BorderStyle = BorderStyle.Fixed3D;
      this.c_ServerStatus.Location = new Point(66, 43);
      this.c_ServerStatus.Name = "c_ServerStatus";
      this.c_ServerStatus.Size = new Size(84, 21);
      this.c_ServerStatus.TabIndex = 4;
      this.c_ServerStatus.Text = "[Status]";
      this.c_ServerStatus.TextAlign = ContentAlignment.MiddleCenter;
      this.c_ComboBox_Servers.FormattingEnabled = true;
      this.c_ComboBox_Servers.Location = new Point(41, 17);
      this.c_ComboBox_Servers.Name = "c_ComboBox_Servers";
      this.c_ComboBox_Servers.Size = new Size(208, 21);
      this.c_ComboBox_Servers.TabIndex = 3;
      this.c_ComboBox_Servers.SelectedIndexChanged += new EventHandler(this.c_ComboBox_Servers_SelectedIndexChanged);
      this.c_ComboBox_Servers.TextUpdate += new EventHandler(this.c_ComboBox_Servers_TextUpdate);
      this.c_Button_Check.Location = new Point(157, 42);
      this.c_Button_Check.Name = "c_Button_Check";
      this.c_Button_Check.Size = new Size(59, 23);
      this.c_Button_Check.TabIndex = 5;
      this.c_Button_Check.Text = "&Check";
      this.c_Button_Check.UseVisualStyleBackColor = true;
      this.c_Button_Check.Click += new EventHandler(this.c_Button_Check_Click);
      this.label3.AutoSize = true;
      this.label3.Location = new Point(6, 20);
      this.label3.Name = "label3";
      this.label3.Size = new Size(29, 13);
      this.label3.TabIndex = 2;
      this.label3.Text = "&Host";
      this.c_CheckBox_ClientDetours.AutoSize = true;
      this.c_CheckBox_ClientDetours.Enabled = false;
      this.c_CheckBox_ClientDetours.Location = new Point(15, 36);
      this.c_CheckBox_ClientDetours.Name = "c_CheckBox_ClientDetours";
      this.c_CheckBox_ClientDetours.Size = new Size(92, 17);
      this.c_CheckBox_ClientDetours.TabIndex = 0;
      this.c_CheckBox_ClientDetours.Text = "Client Detours";
      this.c_CheckBox_ClientDetours.UseVisualStyleBackColor = true;
      this.c_CheckBox_ClientDetours.CheckedChanged += new EventHandler(this.c_CheckBox_ClientDetours_CheckedChanged);
      this.c_GroupBox_Authentication.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.c_GroupBox_Authentication.Controls.Add((Control) this.label1);
      this.c_GroupBox_Authentication.Controls.Add((Control) this.c_TextBox_Port);
      this.c_GroupBox_Authentication.Controls.Add((Control) this.c_CheckBox_SecureAuthentication);
      this.c_GroupBox_Authentication.Enabled = false;
      this.c_GroupBox_Authentication.Location = new Point(395, 83);
      this.c_GroupBox_Authentication.Name = "c_GroupBox_Authentication";
      this.c_GroupBox_Authentication.Size = new Size(131, 81);
      this.c_GroupBox_Authentication.TabIndex = 2;
      this.c_GroupBox_Authentication.TabStop = false;
      this.c_GroupBox_Authentication.Text = "Authentication";
      this.label1.AutoSize = true;
      this.label1.Location = new Point(6, 22);
      this.label1.Name = "label1";
      this.label1.Size = new Size(29, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Port:";
      this.c_TextBox_Port.Location = new Point(41, 19);
      this.c_TextBox_Port.Name = "c_TextBox_Port";
      this.c_TextBox_Port.Size = new Size(47, 20);
      this.c_TextBox_Port.TabIndex = 1;
      this.c_CheckBox_SecureAuthentication.AutoSize = true;
      this.c_CheckBox_SecureAuthentication.Location = new Point(9, 48);
      this.c_CheckBox_SecureAuthentication.Name = "c_CheckBox_SecureAuthentication";
      this.c_CheckBox_SecureAuthentication.Size = new Size(114, 17);
      this.c_CheckBox_SecureAuthentication.TabIndex = 2;
      this.c_CheckBox_SecureAuthentication.Text = "Secure Auth (SSL)";
      this.c_CheckBox_SecureAuthentication.UseVisualStyleBackColor = true;
      this.c_CheckBox_SecureAuthentication.CheckedChanged += new EventHandler(this.c_CheckBox_SecureAuthentication_CheckedChanged);
      this.c_GroupBox_Misc.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.c_GroupBox_Misc.Controls.Add((Control) this.label4);
      this.c_GroupBox_Misc.Controls.Add((Control) this.cbIPAddresses);
      this.c_GroupBox_Misc.Controls.Add((Control) this.cbDebugLaunch);
      this.c_GroupBox_Misc.Controls.Add((Control) this.c_CheckBox_LocalCert);
      this.c_GroupBox_Misc.Controls.Add((Control) this.c_CheckBox_ClientDetours);
      this.c_GroupBox_Misc.Controls.Add((Control) this.chkLockPort);
      this.c_GroupBox_Misc.Enabled = false;
      this.c_GroupBox_Misc.Location = new Point(532, 83);
      this.c_GroupBox_Misc.Name = "c_GroupBox_Misc";
      this.c_GroupBox_Misc.Size = new Size(216, 81);
      this.c_GroupBox_Misc.TabIndex = 3;
      this.c_GroupBox_Misc.TabStop = false;
      this.c_GroupBox_Misc.Text = "Debug";
      this.label4.AutoSize = true;
      this.label4.Location = new Point(12, 57);
      this.label4.Name = "label4";
      this.label4.Size = new Size(49, 13);
      this.label4.TabIndex = 4;
      this.label4.Text = "Local IP:";
      this.cbIPAddresses.DropDownStyle = ComboBoxStyle.DropDownList;
      this.cbIPAddresses.FormattingEnabled = true;
      this.cbIPAddresses.Location = new Point(67, 54);
      this.cbIPAddresses.Name = "cbIPAddresses";
      this.cbIPAddresses.Size = new Size(131, 21);
      this.cbIPAddresses.TabIndex = 3;
      this.cbDebugLaunch.AutoSize = true;
      this.cbDebugLaunch.Location = new Point(111, 15);
      this.cbDebugLaunch.Name = "cbDebugLaunch";
      this.cbDebugLaunch.Size = new Size(97, 17);
      this.cbDebugLaunch.TabIndex = 2;
      this.cbDebugLaunch.Text = "Debug Launch";
      this.cbDebugLaunch.UseVisualStyleBackColor = true;
      this.c_CheckBox_LocalCert.AutoSize = true;
      this.c_CheckBox_LocalCert.Location = new Point(15, 15);
      this.c_CheckBox_LocalCert.Name = "c_CheckBox_LocalCert";
      this.c_CheckBox_LocalCert.Size = new Size(74, 17);
      this.c_CheckBox_LocalCert.TabIndex = 1;
      this.c_CheckBox_LocalCert.Text = "Local Cert";
      this.c_CheckBox_LocalCert.UseVisualStyleBackColor = true;
      this.chkLockPort.AutoSize = true;
      this.chkLockPort.Location = new Point(111, 36);
      this.chkLockPort.Name = "chkLockPort";
      this.chkLockPort.Size = new Size(72, 17);
      this.chkLockPort.TabIndex = 5;
      this.chkLockPort.Text = "Lock Port";
      this.chkLockPort.UseVisualStyleBackColor = true;
      this.statusStrip1.Items.AddRange(new ToolStripItem[3]
      {
        (ToolStripItem) this.c_ToolStripStatusLabel,
        (ToolStripItem) this.c_ToolStripStatusLabel_Filler,
        (ToolStripItem) this.c_ToolStripStatusLabel_Version
      });
      this.statusStrip1.Location = new Point(0, 612);
      this.statusStrip1.Name = "statusStrip1";
      this.statusStrip1.Size = new Size(805, 22);
      this.statusStrip1.TabIndex = 9;
      this.statusStrip1.Text = "statusStrip1";
      this.c_ToolStripStatusLabel.Name = "c_ToolStripStatusLabel";
      this.c_ToolStripStatusLabel.Size = new Size(47, 17);
      this.c_ToolStripStatusLabel.Text = "[Status]";
      this.c_ToolStripStatusLabel.TextAlign = ContentAlignment.MiddleLeft;
      this.c_ToolStripStatusLabel.Click += new EventHandler(this.c_ToolStripStatusLabel_Click);
      this.c_ToolStripStatusLabel_Filler.Name = "c_ToolStripStatusLabel_Filler";
      this.c_ToolStripStatusLabel_Filler.Size = new Size(743, 17);
      this.c_ToolStripStatusLabel_Filler.Spring = true;
      this.c_ToolStripStatusLabel_Version.BorderSides = ToolStripStatusLabelBorderSides.Left;
      this.c_ToolStripStatusLabel_Version.Name = "c_ToolStripStatusLabel_Version";
      this.c_ToolStripStatusLabel_Version.Size = new Size(58, 17);
      this.c_ToolStripStatusLabel_Version.Text = "[Version]";
      this.c_ToolStripStatusLabel_Version.TextAlign = ContentAlignment.MiddleRight;
      this.c_ToolStripStatusLabel_Version.Visible = false;
      this.c_Panel_WebBrowser.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.c_Panel_WebBrowser.AutoScroll = true;
      this.c_Panel_WebBrowser.Controls.Add((Control) this.c_WebBrowser);
      this.c_Panel_WebBrowser.Controls.Add((Control) this.toolStrip1);
      this.c_Panel_WebBrowser.Controls.Add((Control) this.c_Background);
      this.c_Panel_WebBrowser.Location = new Point(12, 170);
      this.c_Panel_WebBrowser.Name = "c_Panel_WebBrowser";
      this.c_Panel_WebBrowser.Size = new Size(773, 378);
      this.c_Panel_WebBrowser.TabIndex = 12;
      this.c_Panel_WebBrowser.Visible = false;
      this.c_WebBrowser.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.c_WebBrowser.Location = new Point(0, 28);
      this.c_WebBrowser.MinimumSize = new Size(20, 20);
      this.c_WebBrowser.Name = "c_WebBrowser";
      this.c_WebBrowser.Size = new Size(773, 350);
      this.c_WebBrowser.TabIndex = 1;
      this.toolStrip1.Dock = DockStyle.None;
      this.toolStrip1.Items.AddRange(new ToolStripItem[4]
      {
        (ToolStripItem) this.c_ToolStripButton_WebBrowser_Back,
        (ToolStripItem) this.c_ToolStripButton_WebBrowser_Forward,
        (ToolStripItem) this.toolStripSeparator1,
        (ToolStripItem) this.c_ToolStripButton_Browser_OpenInNewWindow
      });
      this.toolStrip1.Location = new Point(0, 0);
      this.toolStrip1.Name = "toolStrip1";
      this.toolStrip1.Size = new Size(191, 25);
      this.toolStrip1.TabIndex = 0;
      this.toolStrip1.Text = "c_ToolStrip_Webbrowser";
      this.c_ToolStripButton_WebBrowser_Back.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.c_ToolStripButton_WebBrowser_Back.Image = (Image) LauncherResources.NavBack;
      this.c_ToolStripButton_WebBrowser_Back.ImageTransparentColor = Color.Magenta;
      this.c_ToolStripButton_WebBrowser_Back.Name = "c_ToolStripButton_WebBrowser_Back";
      this.c_ToolStripButton_WebBrowser_Back.Size = new Size(23, 22);
      this.c_ToolStripButton_WebBrowser_Back.Text = "Back";
      this.c_ToolStripButton_WebBrowser_Back.Click += new EventHandler(this.c_ToolStripButton_WebBrowser_Back_Click);
      this.c_ToolStripButton_WebBrowser_Forward.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.c_ToolStripButton_WebBrowser_Forward.Image = (Image) LauncherResources.NavForward;
      this.c_ToolStripButton_WebBrowser_Forward.ImageTransparentColor = Color.Magenta;
      this.c_ToolStripButton_WebBrowser_Forward.Name = "c_ToolStripButton_WebBrowser_Forward";
      this.c_ToolStripButton_WebBrowser_Forward.Size = new Size(23, 22);
      this.c_ToolStripButton_WebBrowser_Forward.Text = "Forward";
      this.c_ToolStripButton_WebBrowser_Forward.Click += new EventHandler(this.c_ToolStripButton_WebBrowser_Forward_Click);
      this.toolStripSeparator1.Name = "toolStripSeparator1";
      this.toolStripSeparator1.Size = new Size(6, 25);
      this.c_ToolStripButton_Browser_OpenInNewWindow.DisplayStyle = ToolStripItemDisplayStyle.Text;
      this.c_ToolStripButton_Browser_OpenInNewWindow.Image = (Image) componentResourceManager.GetObject("c_ToolStripButton_Browser_OpenInNewWindow.Image");
      this.c_ToolStripButton_Browser_OpenInNewWindow.ImageTransparentColor = Color.Magenta;
      this.c_ToolStripButton_Browser_OpenInNewWindow.Name = "c_ToolStripButton_Browser_OpenInNewWindow";
      this.c_ToolStripButton_Browser_OpenInNewWindow.Size = new Size((int) sbyte.MaxValue, 22);
      this.c_ToolStripButton_Browser_OpenInNewWindow.Text = "Open in New Window";
      this.c_ToolStripButton_Browser_OpenInNewWindow.Click += new EventHandler(this.c_ToolStripButton_Browser_OpenInNewWindow_Click);
      this.c_Background.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.c_Background.Location = new Point(0, 28);
      this.c_Background.MaximumSize = new Size(740, 380);
      this.c_Background.Name = "c_Background";
      this.c_Background.Size = new Size(354, 333);
      this.c_Background.TabIndex = 0;
      this.c_Background.TabStop = false;
      this.c_ToolStripMenuItem_Main_Launcher.DropDownItems.AddRange(new ToolStripItem[4]
      {
        (ToolStripItem) this.c_ToolStripMenuItem_Main_Launcher_CheckForUpdates,
        (ToolStripItem) this.c_MainMenu_Launcher_ResetUpdates_ToolStripMenuItem,
        (ToolStripItem) this.toolStripMenuItem1,
        (ToolStripItem) this.c_ToolStripMenuItem_Main_Launcher_Quit
      });
      this.c_ToolStripMenuItem_Main_Launcher.Name = "c_ToolStripMenuItem_Main_Launcher";
      this.c_ToolStripMenuItem_Main_Launcher.Size = new Size(68, 20);
      this.c_ToolStripMenuItem_Main_Launcher.Text = "&Launcher";
      this.c_ToolStripMenuItem_Main_Launcher_CheckForUpdates.Name = "c_ToolStripMenuItem_Main_Launcher_CheckForUpdates";
      this.c_ToolStripMenuItem_Main_Launcher_CheckForUpdates.Size = new Size(185, 22);
      this.c_ToolStripMenuItem_Main_Launcher_CheckForUpdates.Text = "Check For Updates ...";
      this.c_ToolStripMenuItem_Main_Launcher_CheckForUpdates.Click += new EventHandler(this.c_ToolStripMenuItem_Main_Launcher_CheckForUpdates_Click);
      this.c_MainMenu_Launcher_ResetUpdates_ToolStripMenuItem.Name = "c_MainMenu_Launcher_ResetUpdates_ToolStripMenuItem";
      this.c_MainMenu_Launcher_ResetUpdates_ToolStripMenuItem.Size = new Size(185, 22);
      this.c_MainMenu_Launcher_ResetUpdates_ToolStripMenuItem.Text = "Reset Updates";
      this.c_MainMenu_Launcher_ResetUpdates_ToolStripMenuItem.Click += new EventHandler(this.c_MainMenu_Launcher_ResetUpdates_ToolStripMenuItem_Click);
      this.toolStripMenuItem1.Name = "toolStripMenuItem1";
      this.toolStripMenuItem1.Size = new Size(182, 6);
      this.c_ToolStripMenuItem_Main_Launcher_Quit.Name = "c_ToolStripMenuItem_Main_Launcher_Quit";
      this.c_ToolStripMenuItem_Main_Launcher_Quit.Size = new Size(185, 22);
      this.c_ToolStripMenuItem_Main_Launcher_Quit.Text = "&Quit";
      this.c_ToolStripMenuItem_Main_Launcher_Quit.Click += new EventHandler(this.c_ToolStripMenuItem_Main_File_Quit_Click);
      this.helpToolStripMenuItem.Alignment = ToolStripItemAlignment.Right;
      this.helpToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[13]
      {
        (ToolStripItem) this.supportTicketToolStripMenuItem,
        (ToolStripItem) this.helpPageToolStripMenuItem,
        (ToolStripItem) this.toolStripSeparator2,
        (ToolStripItem) this.checkCertificateToolStripMenuItem,
        (ToolStripItem) this.installCertificateToolStripMenuItem,
        (ToolStripItem) this.iNV300ToolStripMenuItem,
        (ToolStripItem) this.advancedToolStripMenuItem,
        (ToolStripItem) this.toolStripMenuItem2,
        (ToolStripItem) this.c_Menu_Main_Help_BrowseUserFolder_ToolStripMenuItem,
        (ToolStripItem) this.c_Menu_Main_Help_ShowErrorLog_ToolStripMenuItem,
        (ToolStripItem) this.toolStripSeparator3,
        (ToolStripItem) this.flushDNSToolStripMenuItem,
        (ToolStripItem) this.dNSLookupToolStripMenuItem
      });
      this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
      this.helpToolStripMenuItem.Size = new Size(44, 20);
      this.helpToolStripMenuItem.Text = "&Help";
      this.supportTicketToolStripMenuItem.Name = "supportTicketToolStripMenuItem";
      this.supportTicketToolStripMenuItem.Size = new Size(174, 22);
      this.supportTicketToolStripMenuItem.Text = "&Support Ticket...";
      this.supportTicketToolStripMenuItem.Click += new EventHandler(this.supportTicketToolStripMenuItem_Click);
      this.helpPageToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[3]
      {
        (ToolStripItem) this.commonErrorsToolStripMenuItem,
        (ToolStripItem) this.helpForumToolStripMenuItem,
        (ToolStripItem) this.forumsToolStripMenuItem
      });
      this.helpPageToolStripMenuItem.Name = "helpPageToolStripMenuItem";
      this.helpPageToolStripMenuItem.Size = new Size(174, 22);
      this.helpPageToolStripMenuItem.Text = "H&elp...";
      this.helpPageToolStripMenuItem.Click += new EventHandler(this.helpPageToolStripMenuItem_Click);
      this.commonErrorsToolStripMenuItem.Name = "commonErrorsToolStripMenuItem";
      this.commonErrorsToolStripMenuItem.Size = new Size(158, 22);
      this.commonErrorsToolStripMenuItem.Text = "&Common Errors";
      this.commonErrorsToolStripMenuItem.Click += new EventHandler(this.commonErrorsToolStripMenuItem_Click);
      this.helpForumToolStripMenuItem.Name = "helpForumToolStripMenuItem";
      this.helpForumToolStripMenuItem.Size = new Size(158, 22);
      this.helpForumToolStripMenuItem.Text = "He&lp Forum";
      this.helpForumToolStripMenuItem.Click += new EventHandler(this.helpForumToolStripMenuItem_Click);
      this.forumsToolStripMenuItem.Name = "forumsToolStripMenuItem";
      this.forumsToolStripMenuItem.Size = new Size(158, 22);
      this.forumsToolStripMenuItem.Text = "&Forums";
      this.forumsToolStripMenuItem.Click += new EventHandler(this.forumsToolStripMenuItem_Click);
      this.toolStripSeparator2.Name = "toolStripSeparator2";
      this.toolStripSeparator2.Size = new Size(171, 6);
      this.checkCertificateToolStripMenuItem.Name = "checkCertificateToolStripMenuItem";
      this.checkCertificateToolStripMenuItem.Size = new Size(174, 22);
      this.checkCertificateToolStripMenuItem.Text = "Check Certificate...";
      this.checkCertificateToolStripMenuItem.Click += new EventHandler(this.checkCertificateToolStripMenuItem_Click);
      this.installCertificateToolStripMenuItem.Name = "installCertificateToolStripMenuItem";
      this.installCertificateToolStripMenuItem.Size = new Size(174, 22);
      this.installCertificateToolStripMenuItem.Text = "Install Certificate";
      this.installCertificateToolStripMenuItem.ToolTipText = "Installs certificate for the current hostname.";
      this.installCertificateToolStripMenuItem.Click += new EventHandler(this.installCertificateToolStripMenuItem_Click);
      this.iNV300ToolStripMenuItem.Name = "iNV300ToolStripMenuItem";
      this.iNV300ToolStripMenuItem.Size = new Size(174, 22);
      this.iNV300ToolStripMenuItem.Text = "&INV-300";
      this.iNV300ToolStripMenuItem.Click += new EventHandler(this.iNV300ToolStripMenuItem_Click);
      this.advancedToolStripMenuItem.Name = "advancedToolStripMenuItem";
      this.advancedToolStripMenuItem.Size = new Size(174, 22);
      this.advancedToolStripMenuItem.Text = "&Advanced Settings";
      this.advancedToolStripMenuItem.Click += new EventHandler(this.advancedToolStripMenuItem_Click);
      this.toolStripMenuItem2.Name = "toolStripMenuItem2";
      this.toolStripMenuItem2.Size = new Size(171, 6);
      this.c_Menu_Main_Help_BrowseUserFolder_ToolStripMenuItem.Name = "c_Menu_Main_Help_BrowseUserFolder_ToolStripMenuItem";
      this.c_Menu_Main_Help_BrowseUserFolder_ToolStripMenuItem.Size = new Size(174, 22);
      this.c_Menu_Main_Help_BrowseUserFolder_ToolStripMenuItem.Text = "Browse User Folder";
      this.c_Menu_Main_Help_BrowseUserFolder_ToolStripMenuItem.Click += new EventHandler(this.c_Menu_Main_Help_BrowseUserFolder_ToolStripMenuItem_Click);
      this.c_Menu_Main_Help_ShowErrorLog_ToolStripMenuItem.Name = "c_Menu_Main_Help_ShowErrorLog_ToolStripMenuItem";
      this.c_Menu_Main_Help_ShowErrorLog_ToolStripMenuItem.Size = new Size(174, 22);
      this.c_Menu_Main_Help_ShowErrorLog_ToolStripMenuItem.Text = "Show Error Log";
      this.c_Menu_Main_Help_ShowErrorLog_ToolStripMenuItem.Click += new EventHandler(this.c_Menu_Main_Help_ShowErrorLog_ToolStripMenuItem_Click);
      this.toolStripSeparator3.Name = "toolStripSeparator3";
      this.toolStripSeparator3.Size = new Size(171, 6);
      this.flushDNSToolStripMenuItem.Name = "flushDNSToolStripMenuItem";
      this.flushDNSToolStripMenuItem.Size = new Size(174, 22);
      this.flushDNSToolStripMenuItem.Text = "&Flush DNS";
      this.flushDNSToolStripMenuItem.Click += new EventHandler(this.flushDNSToolStripMenuItem_Click);
      this.dNSLookupToolStripMenuItem.Name = "dNSLookupToolStripMenuItem";
      this.dNSLookupToolStripMenuItem.Size = new Size(174, 22);
      this.dNSLookupToolStripMenuItem.Text = "&DNS Lookup";
      this.dNSLookupToolStripMenuItem.Click += new EventHandler(this.dNSLookupToolStripMenuItem_Click);
      this.c_MenuStrip_Main.BackgroundImageLayout = ImageLayout.Center;
      this.c_MenuStrip_Main.Items.AddRange(new ToolStripItem[2]
      {
        (ToolStripItem) this.c_ToolStripMenuItem_Main_Launcher,
        (ToolStripItem) this.helpToolStripMenuItem
      });
      this.c_MenuStrip_Main.Location = new Point(0, 0);
      this.c_MenuStrip_Main.Name = "c_MenuStrip_Main";
      this.c_MenuStrip_Main.Size = new Size(805, 24);
      this.c_MenuStrip_Main.TabIndex = 10;
      this.c_MenuStrip_Main.Text = "menuStrip1";
      this.c_TextBox_Client.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.c_TextBox_Client.BorderStyle = BorderStyle.None;
      this.c_TextBox_Client.Location = new Point(9, 22);
      this.c_TextBox_Client.Name = "c_TextBox_Client";
      this.c_TextBox_Client.ReadOnly = true;
      this.c_TextBox_Client.Size = new Size(677, 13);
      this.c_TextBox_Client.TabIndex = 0;
      this.c_Button_Browse.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.c_Button_Browse.Location = new Point(692, 17);
      this.c_Button_Browse.Name = "c_Button_Browse";
      this.c_Button_Browse.Size = new Size(75, 23);
      this.c_Button_Browse.TabIndex = 1;
      this.c_Button_Browse.Text = "&Browse";
      this.c_Button_Browse.UseVisualStyleBackColor = true;
      this.c_Button_Browse.Click += new EventHandler(this.c_Button_Browse_Click);
      this.c_GroupBox_Client.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.c_GroupBox_Client.Controls.Add((Control) this.c_Button_Browse);
      this.c_GroupBox_Client.Controls.Add((Control) this.c_TextBox_Client);
      this.c_GroupBox_Client.Enabled = false;
      this.c_GroupBox_Client.Location = new Point(12, 27);
      this.c_GroupBox_Client.Name = "c_GroupBox_Client";
      this.c_GroupBox_Client.Size = new Size(773, 50);
      this.c_GroupBox_Client.TabIndex = 0;
      this.c_GroupBox_Client.TabStop = false;
      this.c_GroupBox_Client.Text = "Client";
      this.c_GroupBox_Client.Enter += new EventHandler(this.c_GroupBox_Client_Enter);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.BackgroundImageLayout = ImageLayout.None;
      this.CancelButton = (IButtonControl) this.c_Button_Cancel;
      this.ClientSize = new Size(805, 634);
      this.Controls.Add((Control) this.c_GroupBox_Misc);
      this.Controls.Add((Control) this.c_Panel_WebBrowser);
      this.Controls.Add((Control) this.statusStrip1);
      this.Controls.Add((Control) this.c_MenuStrip_Main);
      this.Controls.Add((Control) this.c_GroupBox_Authentication);
      this.Controls.Add((Control) this.c_GroupBox_Server);
      this.Controls.Add((Control) this.c_ProgressBar);
      this.Controls.Add((Control) this.c_Status);
      this.Controls.Add((Control) this.c_Button_Cancel);
      this.Controls.Add((Control) this.c_Button_Play);
      this.Controls.Add((Control) this.c_GroupBox_Client);
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.MainMenuStrip = this.c_MenuStrip_Main;
      this.MinimumSize = new Size(770, 540);
      this.Name = nameof (FormMain);
      this.Text = "LaunchNet7";
      this.FormClosing += new FormClosingEventHandler(this.FormMain_FormClosing);
      this.Load += new EventHandler(this.FormMain_Load);
      this.c_GroupBox_Server.ResumeLayout(false);
      this.c_GroupBox_Server.PerformLayout();
      this.c_GroupBox_Authentication.ResumeLayout(false);
      this.c_GroupBox_Authentication.PerformLayout();
      this.c_GroupBox_Misc.ResumeLayout(false);
      this.c_GroupBox_Misc.PerformLayout();
      this.statusStrip1.ResumeLayout(false);
      this.statusStrip1.PerformLayout();
      this.c_Panel_WebBrowser.ResumeLayout(false);
      this.c_Panel_WebBrowser.PerformLayout();
      this.toolStrip1.ResumeLayout(false);
      this.toolStrip1.PerformLayout();
      ((ISupportInitialize) this.c_Background).EndInit();
      this.c_MenuStrip_Main.ResumeLayout(false);
      this.c_MenuStrip_Main.PerformLayout();
      this.c_GroupBox_Client.ResumeLayout(false);
      this.c_GroupBox_Client.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    private delegate DialogResult DoDisplayUpdateCallback(Updater updater);
  }
}
