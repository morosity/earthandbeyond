﻿// Decompiled with JetBrains decompiler
// Type: LaunchNet7.Launcher
// Assembly: LaunchNet7, Version=1.9.9.10, Culture=neutral, PublicKeyToken=null
// MVID: AF666469-2FA2-4BE0-A9D5-7003BDE93D8C
// Assembly location: D:\Games\Net-7\bin\LaunchNet7.exe

using LaunchNet7.Patching;
using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading;
using System.Windows.Forms;

namespace LaunchNet7
{
  public class Launcher
  {
    private const string AdminRightsRequirementText = "\nPlease start this application with administrative rights.";
    private LaunchSetting m_Setting;

    public Launcher(LaunchSetting setting)
    {
      if (setting == null)
        throw new ArgumentNullException(nameof (setting));
      this.m_Setting = setting;
    }

    public LaunchSetting Setting
    {
      get
      {
        return this.m_Setting;
      }
      set
      {
        this.m_Setting = value;
      }
    }

    private int GetOSArchitecture()
    {
      string environmentVariable = Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE");
      return !string.IsNullOrEmpty(environmentVariable) && string.Compare(environmentVariable, 0, "x86", 0, 3, true) != 0 ? 64 : 32;
    }

    public void Launch()
    {
      if (this.GetOSArchitecture() == 32)
      {
        this.PatchRegistry();
        this.PatchRegistryStarShip(this.Setting.ClientPath + "..\\Character and Starship Creator");
      }
      else
      {
        this.PatchRegistry64();
        this.PatchRegistryStarShip64(this.Setting.ClientPath + "..\\Character and Starship Creator");
      }
      this.PatchAuthLoginFile();
      this.PatchRegDataFileNames();
      this.PatchRegDataFile();
      this.PatchAuthIniFile();
      this.PatchNetworkIniFile();
      switch (this.Setting.LaunchName.ToUpperInvariant())
      {
        case "NET7SP":
          this.LaunchNet7Server();
          Thread.Sleep(25000);
          this.LaunchNet7Proxy();
          break;
        case "NET7MP":
          this.LaunchNet7Proxy();
          break;
        default:
          this.LaunchClient();
          break;
      }
    }

    private void LaunchClient()
    {
      IPAddress[] hostAddresses = Dns.GetHostAddresses(this.Setting.Hostname);
      if (hostAddresses.Length == 0)
        throw new InvalidOperationException(string.Format("Could not resolve hostname '{0}'.", (object) this.Setting.Hostname));
      ProcessStartInfo startInfo = new ProcessStartInfo();
      if (this.Setting.UseClientDetours)
      {
        startInfo.WorkingDirectory = Path.Combine(Directory.GetCurrentDirectory(), "bin");
        startInfo.FileName = Path.Combine(startInfo.WorkingDirectory, "Detours.exe");
        startInfo.Arguments = string.Format("/ADDR:{0} /CLIENT:{1}", (object) hostAddresses[0].ToString(), (object) LauncherUtility.GetShortPathName(this.Setting.ClientPath));
      }
      else
      {
        startInfo.WorkingDirectory = Path.GetDirectoryName(this.Setting.ClientPath);
        startInfo.FileName = this.Setting.ClientPath;
        startInfo.Arguments = string.Format("-SERVER_ADDR {0} -PROTOCOL {1}", (object) hostAddresses[0].ToString(), (object) "TCP");
      }
      try
      {
        Process.Start(startInfo);
      }
      catch (Exception ex)
      {
        throw new ApplicationException(string.Format("Could not launch client.\nWorking Directory: {0}\nFileName: {1}\nArguments: {2}\nDetails: {3}", (object) startInfo.WorkingDirectory, (object) startInfo.FileName, (object) startInfo.Arguments, (object) ex.Message), ex);
      }
    }

    private void LaunchNet7Proxy()
    {
      IPAddress[] hostAddresses = Dns.GetHostAddresses(this.Setting.Hostname);
      if (hostAddresses.Length == 0)
        throw new InvalidOperationException(string.Format("Could not resolve hostname '{0}'.", (object) this.Setting.Hostname));
      ProcessStartInfo startInfo = new ProcessStartInfo();
      startInfo.WorkingDirectory = Path.Combine(Directory.GetCurrentDirectory(), "bin");
      startInfo.FileName = Path.Combine(startInfo.WorkingDirectory, "Net7Proxy.exe");
      startInfo.Arguments = string.Format("/ADDRESS:{0} /CLIENT:\"{1}\"", (object) hostAddresses[0].ToString(), (object) LauncherUtility.GetShortPathName(this.Setting.ClientPath));
      int num1 = this.Setting.UseClientDetours ? 1 : 0;
      if (this.Setting.LocalIP != null)
      {
        ProcessStartInfo processStartInfo = startInfo;
        processStartInfo.Arguments = processStartInfo.Arguments + " /LADDRESS:" + this.Setting.LocalIP;
      }
      if (this.Setting.UseLocalCert)
      {
        startInfo.Arguments += " /LC";
        startInfo.Arguments += string.Format(" /SSL:{0}", (object) this.Setting.AuthenticationPort.ToString());
      }
      if (this.Setting.UsePacketOpt)
        startInfo.Arguments += " /POPT";
      if (this.Setting.DebugLaunch)
        startInfo.Arguments += " /DEBUGL";
      if (this.Setting.UseExperimentalReorder)
        startInfo.Arguments += " /EXREORDER";
      if (this.Setting.LockPort)
        startInfo.Arguments += " /SP";
      try
      {
        if (this.Setting.DebugLaunch)
        {
          TextWriter textWriter = (TextWriter) new StreamWriter(startInfo.WorkingDirectory + "\\launch.bat");
          textWriter.WriteLine("cd \"" + startInfo.WorkingDirectory + "\"");
          textWriter.WriteLine("\"" + startInfo.FileName + "\" " + startInfo.Arguments);
          textWriter.WriteLine("pause");
          textWriter.Close();
          Process process = new Process();
          process.StartInfo.FileName = "launch.bat";
          process.StartInfo.WorkingDirectory = startInfo.WorkingDirectory;
          process.StartInfo.Arguments = startInfo.Arguments;
          process.StartInfo.CreateNoWindow = true;
          process.StartInfo.UseShellExecute = true;
          int num2 = (int) MessageBox.Show("WorkingDir: " + startInfo.WorkingDirectory + "\nFile Name:" + startInfo.FileName + "\nParams:" + startInfo.Arguments, "Debug Launch");
          process.Start();
        }
        else
          Process.Start(startInfo);
      }
      catch (Exception ex)
      {
        Program.LogException(ex);
        throw new ApplicationException(string.Format("Could not launch Net7Proxy.\nWorking Directory: {0}\nFileName: {1}\nArguments: {2}\nDetails: {3}", (object) startInfo.WorkingDirectory, (object) startInfo.FileName, (object) startInfo.Arguments, (object) ex.Message), ex);
      }
    }

    private void LaunchNet7Server()
    {
      ProcessStartInfo startInfo = new ProcessStartInfo();
      startInfo.WorkingDirectory = Path.Combine(Directory.GetCurrentDirectory(), "bin");
      startInfo.FileName = Path.Combine(startInfo.WorkingDirectory, "Net7.exe");
      try
      {
        Process.Start(startInfo);
      }
      catch (Exception ex)
      {
        throw new ApplicationException("Could not launch Net7 Server. Details: " + ex.Message, ex);
      }
    }

    private void PatchNetworkIniFile()
    {
      string str1 = Path.Combine(this.Setting.CommonDirectoryName, "Network.ini");
      string str2 = Path.Combine(this.Setting.CommonDirectoryName, "Network.ini.orig");
      bool flag = false;
      if (!File.Exists(str1))
      {
        if (File.Exists(str2))
        {
          try
          {
            File.Copy(str2, str1);
          }
          catch (Exception ex)
          {
            throw new ApplicationException("Could not restore backup of \"Network.ini\". Details: " + ex.Message, ex);
          }
        }
      }
      string b = this.Setting.Hostname;
      if (this.Setting.UseLocalCert)
        b = "localhost.net-7.org";
      IniUtility.GetValue(str1, "MasterServer", "Name");
      if (!string.Equals(IniUtility.GetValue(str1, "MasterServer", "Name"), b, StringComparison.InvariantCultureIgnoreCase))
        flag = true;
      else if (!string.Equals(IniUtility.GetValue(str1, "RegisterServer", "Name"), b, StringComparison.InvariantCultureIgnoreCase))
        flag = true;
      else if (!string.Equals(IniUtility.GetValue(str1, "ReporterServer", "Name"), b, StringComparison.InvariantCultureIgnoreCase))
        flag = true;
      else if (!string.Equals(IniUtility.GetValue(str1, "GlobalServer_Directory", "Name"), b, StringComparison.InvariantCultureIgnoreCase))
        flag = true;
      else if (!string.Equals(IniUtility.GetValue(str1, "GlobalServer_Client", "Name"), b, StringComparison.InvariantCultureIgnoreCase))
        flag = true;
      else if (!string.Equals(IniUtility.GetValue(str1, "GlobalServer_Register", "Name"), b, StringComparison.InvariantCultureIgnoreCase))
        flag = true;
      else if (!string.Equals(IniUtility.GetValue(str1, "GlobalServer_Parent", "Name"), b, StringComparison.InvariantCultureIgnoreCase))
        flag = true;
      else if (!string.Equals(IniUtility.GetValue(str1, "ChatServer", "Name"), b, StringComparison.InvariantCultureIgnoreCase))
        flag = true;
      else if (!string.Equals(IniUtility.GetValue(str1, "ChatServer_Basic", "Name"), b, StringComparison.InvariantCultureIgnoreCase))
        flag = true;
      else if (!string.Equals(IniUtility.GetValue(str1, "GroupServer", "Name"), b, StringComparison.InvariantCultureIgnoreCase))
        flag = true;
      else if (!string.Equals(IniUtility.GetValue(str1, "GuildServer", "Name"), b, StringComparison.InvariantCultureIgnoreCase))
        flag = true;
      if (!flag)
        return;
      try
      {
        File.Copy(str1, str2, true);
      }
      catch (Exception ex)
      {
        throw new ApplicationException("Could not create backup of \"Network.ini\". Details: " + ex.Message, ex);
      }
      try
      {
        IniUtility.SetValue(str1, "MasterServer", "Name", b);
        IniUtility.SetValue(str1, "RegisterServer", "Name", b);
        IniUtility.SetValue(str1, "ReporterServer", "Name", b);
        IniUtility.SetValue(str1, "GlobalServer_Directory", "Name", b);
        IniUtility.SetValue(str1, "GlobalServer_Client", "Name", b);
        IniUtility.SetValue(str1, "GlobalServer_Register", "Name", b);
        IniUtility.SetValue(str1, "GlobalServer_Parent", "Name", b);
        IniUtility.SetValue(str1, "ChatServer", "Name", b);
        IniUtility.SetValue(str1, "ChatServer_Basic", "Name", b);
        IniUtility.SetValue(str1, "GroupServer", "Name", b);
        IniUtility.SetValue(str1, "GuildServer", "Name", b);
      }
      catch (Exception ex)
      {
        throw new ApplicationException("Could not patch \"Network.ini\". Details: " + ex.Message, ex);
      }
    }

    private void PatchAuthIniFile()
    {
      string str1 = Path.Combine(this.Setting.IniDirectoryName, "Auth.ini");
      string str2 = Path.Combine(this.Setting.IniDirectoryName, "Auth.ini.orig");
      bool flag = false;
      if (!File.Exists(str1))
      {
        if (File.Exists(str2))
        {
          try
          {
            File.Copy(str2, str1);
          }
          catch (Exception ex)
          {
            throw new ApplicationException("Could not restore backup of \"Auth.ini\". Details: " + ex.Message, ex);
          }
        }
      }
      string b1 = this.Setting.RegistrationHostname;
      if (this.Setting.UseLocalCert)
        b1 = "localhost.net-7.org";
      string b2 = new UriBuilder()
      {
        Scheme = (this.Setting.UseSecureAuthentication ? "https" : "http"),
        Host = b1,
        Path = "misc/touchsession.jsp",
        Query = "lkey=%s"
      }.ToString();
      if (!string.Equals(IniUtility.GetValue(str1, "General", "AAIUrl"), b1, StringComparison.InvariantCultureIgnoreCase))
        flag = true;
      else if (!string.Equals(IniUtility.GetValue(str1, "General", "LKeyUrl"), b2, StringComparison.InvariantCultureIgnoreCase))
        flag = true;
      if (!flag)
        return;
      try
      {
        File.Copy(str1, str2, true);
      }
      catch (Exception ex)
      {
        throw new ApplicationException("Could not create backup of \"Auth.ini\". Details: " + ex.Message, ex);
      }
      try
      {
        IniUtility.SetValue(str1, "General", "AAIUrl", b1);
        IniUtility.SetValue(str1, "General", "LKeyUrl", b2);
      }
      catch (Exception ex)
      {
        throw new ApplicationException("Could not patch \"Auth.ini\" file. Details: " + ex.Message, ex);
      }
    }

    private void PatchRegDataFile()
    {
      string str1 = Path.Combine(this.Setting.IniDirectoryName, "rg_regdata.ini");
      string str2 = Path.Combine(this.Setting.IniDirectoryName, "rg_regdata.ini.orig");
      bool flag = false;
      string str3 = this.Setting.RegistrationHostname;
      if (this.Setting.UseLocalCert)
        str3 = "localhost.net-7.org";
      if (!File.Exists(str1))
      {
        if (File.Exists(str2))
        {
          try
          {
            File.Copy(str2, str1);
          }
          catch (Exception ex)
          {
            throw new ApplicationException("Could not restore backuped \"rg_regdata.ini\".\nDetails: " + ex.Message, ex);
          }
        }
      }
      string b = new UriBuilder()
      {
        Scheme = (this.Setting.UseSecureAuthentication ? "https" : "http"),
        Host = str3,
        Path = "subsxml"
      }.ToString();
      if (!string.Equals(IniUtility.GetValue(str1, "Connection", "regserverurl"), b, StringComparison.InvariantCultureIgnoreCase))
        flag = true;
      if (!flag)
        return;
      try
      {
        File.Copy(str1, str2, true);
      }
      catch (Exception ex)
      {
        throw new ApplicationException("Could not create backup of \"rg_regdata.ini\" file. Details: " + ex.Message, ex);
      }
      try
      {
        IniUtility.SetValue(str1, "Connection", "regserverurl", b);
      }
      catch (Exception ex)
      {
        throw new ApplicationException("Could not change ini settings within file \"rg_regdata.ini\". Details: " + ex.Message, ex);
      }
    }

    private void PatchRegDataFileNames()
    {
      string str1 = Path.Combine(this.Setting.IniDirectoryName, "rg_regdata_org");
      string str2 = Path.Combine(this.Setting.IniDirectoryName, "rg_regdata.ini");
      if (!File.Exists(str1))
        return;
      if (File.Exists(str2))
        return;
      try
      {
        File.Move(str1, str2);
      }
      catch (UnauthorizedAccessException ex)
      {
        throw new ApplicationException("Could not repair rg_regdata.ini filename due to security reasons.\nPlease start this application with administrative rights.", (Exception) ex);
      }
      catch (Exception ex)
      {
        throw new ApplicationException("Could not repair rg_regdata.ini filename.", ex);
      }
    }

    private void PatchAuthLoginFile()
    {
      if (!File.Exists(this.Setting.AuthLoginFileName))
        throw new FileNotFoundException("Could not find AuthLogin.dll.\nPlease specify the correct client.exe location.");
      FileVersionInfo versionInfo;
      try
      {
        versionInfo = FileVersionInfo.GetVersionInfo(this.Setting.AuthLoginFileName);
      }
      catch (Exception ex)
      {
        throw new ApplicationException("Could not check version information for AuthLogin.dll.", ex);
      }
      Version version1 = new Version(3, 3, 0, 6);
      Version version2 = new Version(versionInfo.ProductMajorPart, versionInfo.ProductMinorPart, versionInfo.ProductBuildPart, versionInfo.ProductPrivatePart);
      if (!version1.Equals(version2))
        throw new ApplicationException(string.Format("Wrong AuthLogin.dll version.\n\nYour Version: {0}\nRequired Version: {1}\n\nPlease update your Earth and Beyond installation with the last available patch.", (object) version2.ToString(4), (object) version1.ToString(4)));
      AuthPatcherInfo infos;
      try
      {
        infos = AuthLoginPatcher.ReadInformation(this.Setting.AuthLoginFileName);
      }
      catch (Exception ex)
      {
        throw new ApplicationException("Could not check AuthLogin.dll.", ex);
      }
      if ((int) infos.Port == this.Setting.AuthenticationPort && infos.UseHttps == this.Setting.UseSecureAuthentication)
        return;
      infos.Port = (ushort) this.Setting.AuthenticationPort;
      infos.UseHttps = this.Setting.UseSecureAuthentication;
      try
      {
        AuthLoginPatcher.WriteInformation(this.Setting.AuthLoginFileName, infos);
      }
      catch (UnauthorizedAccessException ex)
      {
        throw new ApplicationException("Could not patch AuthLogin.dll due to security reasons.\nPlease start this application with administrative rights.", (Exception) ex);
      }
      catch (Exception ex)
      {
        throw new ApplicationException("Could not patch AuthLogin.dll.", ex);
      }
    }

    private void PatchRegistryStarShip(string location)
    {
      string keyName = "HKEY_LOCAL_MACHINE\\Software\\Westwood Studios\\Earth and Beyond\\Character and Starship Creator";
      bool flag = false;
      try
      {
        flag = Convert.ToInt32(Registry.GetValue(keyName, "Version", (object) 0)) != 1;
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("Could not check registry settings.");
      }
      if (!flag)
        return;
      try
      {
        Registry.SetValue(keyName, "Version", (object) 1, RegistryValueKind.DWord);
        Registry.SetValue(keyName, "Install Path", (object) location, RegistryValueKind.DWord);
        Registry.SetValue(keyName, "RenderDeviceWindowed", (object) 1, RegistryValueKind.DWord);
      }
      catch (UnauthorizedAccessException ex)
      {
        int num = (int) MessageBox.Show("Could not patch registry settings because of a security conflict. Run LaunchNet7 again as an administrator.");
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("Could not patch registry settings due to an unkown error.");
      }
    }

    private void PatchRegistryStarShip64(string location)
    {
      string keyName = "HKEY_LOCAL_MACHINE\\Software\\WoW6432Node\\Westwood Studios\\Earth and Beyond\\Character and Starship Creator";
      bool flag = false;
      try
      {
        flag = Convert.ToInt32(Registry.GetValue(keyName, "Version", (object) 0)) != 1;
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("Could not check registry settings.");
      }
      if (!flag)
        return;
      try
      {
        Registry.SetValue(keyName, "Version", (object) 1, RegistryValueKind.DWord);
        Registry.SetValue(keyName, "Install Path", (object) location, RegistryValueKind.DWord);
        Registry.SetValue(keyName, "RenderDeviceWindowed", (object) 1, RegistryValueKind.DWord);
      }
      catch (UnauthorizedAccessException ex)
      {
        int num = (int) MessageBox.Show("Could not patch registry settings because of a security conflict. Run LaunchNet7 again as an administrator.");
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("Could not patch registry settings due to an unkown error.");
      }
    }

    private void PatchRegistry()
    {
      string keyName = "HKEY_LOCAL_MACHINE\\Software\\Westwood Studios\\Earth and Beyond\\Registration";
      bool flag = false;
      try
      {
        flag = Convert.ToInt32(Registry.GetValue(keyName, "Registered", (object) 0)) != 1;
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("Could not check registry settings.");
      }
      if (!flag)
        return;
      try
      {
        Registry.SetValue(keyName, "Registered", (object) 1, RegistryValueKind.DWord);
      }
      catch (UnauthorizedAccessException ex)
      {
        int num = (int) MessageBox.Show("Could not patch registry settings because of a security conflict. Run LaunchNet7 again as an administrator.");
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("Could not patch registry settings due to an unkown error.");
      }
    }

    private void PatchRegistry64()
    {
      string keyName = "HKEY_LOCAL_MACHINE\\SOFTWARE\\WoW6432Node\\Westwood Studios\\Earth and Beyond\\Registration";
      bool flag = false;
      try
      {
        flag = (int) Registry.GetValue(keyName, "Registered", (object) 0) != 1;
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("Could not check registry settings.");
      }
      if (!flag)
        return;
      try
      {
        Registry.SetValue(keyName, "Registered", (object) 1, RegistryValueKind.DWord);
      }
      catch (UnauthorizedAccessException ex)
      {
        int num = (int) MessageBox.Show("Could not patch registry settings because of a security conflict. Run LaunchNet7 again as an administrator.");
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("Could not patch registry settings due to an unkown error.");
      }
    }
  }
}
