﻿// Decompiled with JetBrains decompiler
// Type: LaunchNet7.IniUtility
// Assembly: LaunchNet7, Version=1.9.9.10, Culture=neutral, PublicKeyToken=null
// MVID: AF666469-2FA2-4BE0-A9D5-7003BDE93D8C
// Assembly location: D:\Games\Net-7\bin\LaunchNet7.exe

using System.Collections.Specialized;
using System.Runtime.InteropServices;
using System.Text;

namespace LaunchNet7
{
  public static class IniUtility
  {
    [DllImport("KERNEL32.DLL")]
    private static extern int GetPrivateProfileString(string lpAppName, string lpKeyName, string lpDefault, StringBuilder lpReturnedString, int nSize, string lpFileName);

    [DllImport("KERNEL32.DLL")]
    private static extern int GetPrivateProfileInt(string lpAppName, string lpKeyName, int iDefault, string lpFileName);

    [DllImport("KERNEL32.DLL")]
    private static extern bool WritePrivateProfileString(string lpAppName, string lpKeyName, string lpString, string lpFileName);

    [DllImport("KERNEL32.DLL")]
    private static extern int GetPrivateProfileSection(string lpAppName, byte[] lpReturnedString, int nSize, string lpFileName);

    [DllImport("KERNEL32.DLL")]
    private static extern bool WritePrivateProfileSection(string lpAppName, byte[] data, string lpFileName);

    [DllImport("KERNEL32.DLL")]
    private static extern int GetPrivateProfileSectionNames(byte[] lpReturnedString, int nSize, string lpFileName);

    public static string GetValue(string filename, string section, string key)
    {
      StringBuilder lpReturnedString = new StringBuilder(256);
      string lpDefault = "";
      if (IniUtility.GetPrivateProfileString(section, key, lpDefault, lpReturnedString, lpReturnedString.Capacity, filename) != 0)
        return lpReturnedString.ToString();
      return (string) null;
    }

    public static bool SetValue(string filename, string section, string key, string value)
    {
      return IniUtility.WritePrivateProfileString(section, key, value, filename);
    }

    public static int GetINIInt(string filename, string section, string key)
    {
      int iDefault = -1;
      return IniUtility.GetPrivateProfileInt(section, key, iDefault, filename);
    }

    public static StringCollection GetSection(string filename, string section)
    {
      StringCollection stringCollection = new StringCollection();
      byte[] lpReturnedString = new byte[32768];
      int privateProfileSection = IniUtility.GetPrivateProfileSection(section, lpReturnedString, lpReturnedString.GetUpperBound(0), filename);
      if (privateProfileSection > 0)
      {
        StringBuilder stringBuilder = new StringBuilder();
        for (int index = 0; index < privateProfileSection; ++index)
        {
          if (lpReturnedString[index] != (byte) 0)
            stringBuilder.Append((char) lpReturnedString[index]);
          else if (stringBuilder.Length > 0)
          {
            stringCollection.Add(stringBuilder.ToString());
            stringBuilder = new StringBuilder();
          }
        }
      }
      return stringCollection;
    }

    public static bool SetSection(string filename, string section, StringCollection items)
    {
      byte[] numArray = new byte[32768];
      int byteIndex = 0;
      foreach (string s in items)
      {
        Encoding.ASCII.GetBytes(s, 0, s.Length, numArray, byteIndex);
        byteIndex += s.Length;
        numArray[byteIndex] = (byte) 0;
        ++byteIndex;
      }
      numArray[byteIndex] = (byte) 0;
      return IniUtility.WritePrivateProfileSection(section, numArray, filename);
    }

    public static StringCollection GetSectionNames(string filename)
    {
      StringCollection stringCollection = new StringCollection();
      byte[] lpReturnedString = new byte[32768];
      int profileSectionNames = IniUtility.GetPrivateProfileSectionNames(lpReturnedString, lpReturnedString.GetUpperBound(0), filename);
      if (profileSectionNames > 0)
      {
        StringBuilder stringBuilder = new StringBuilder();
        for (int index = 0; index < profileSectionNames; ++index)
        {
          if (lpReturnedString[index] != (byte) 0)
            stringBuilder.Append((char) lpReturnedString[index]);
          else if (stringBuilder.Length > 0)
          {
            stringCollection.Add(stringBuilder.ToString());
            stringBuilder = new StringBuilder();
          }
        }
      }
      return stringCollection;
    }

    public static bool KeyExist(string filename, string section, string key)
    {
      int iDefault = -1;
      return IniUtility.GetPrivateProfileInt(section, key, iDefault, filename) != -1;
    }
  }
}
