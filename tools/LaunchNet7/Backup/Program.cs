﻿// Decompiled with JetBrains decompiler
// Type: LaunchNet7.Program
// Assembly: LaunchNet7, Version=1.9.9.10, Culture=neutral, PublicKeyToken=null
// MVID: AF666469-2FA2-4BE0-A9D5-7003BDE93D8C
// Assembly location: D:\Games\Net-7\bin\LaunchNet7.exe

using LaunchNet7.Configuration;
using LaunchNet7.Properties;
using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;

namespace LaunchNet7
{
  internal static class Program
  {
    private static string m_WorkingDirectory;
    private static System.Configuration.Configuration m_Configuration;
    private static LauncherConfigurationSection m_LauncherConfiguration;
    private static string m_ErrorLogFileName;
    private static string m_UserDirectoryName;
    private static string m_ProgramDirectoryName;
    private static string m_ProgramFileName;
    private static string m_ClientFileName;
    private static string m_ClientRootDirectoryName;

    [STAThread]
    private static void Main(string[] args)
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      Program.m_WorkingDirectory = Environment.CurrentDirectory;
      bool flag = false;
      string path = (string) null;
      CommandLineReader commandLineReader = new CommandLineReader(args);
      while (commandLineReader.Read())
      {
        if (string.Equals(commandLineReader.CurrentArgument, "-delete", StringComparison.InvariantCultureIgnoreCase))
          commandLineReader.TryReadNextAsString(out path);
        else if (string.Equals(commandLineReader.CurrentArgument, "-selfupdatetest", StringComparison.InvariantCultureIgnoreCase))
          flag = true;
      }
      if (flag)
      {
        Program.RunSelfUpdateTest();
      }
      else
      {
        try
        {
          if (!string.IsNullOrEmpty(path))
          {
            if (File.Exists(path))
            {
              int num = 0;
              while (true)
              {
                ++num;
                if (num <= 30)
                {
                  try
                  {
                    File.Delete(path);
                    break;
                  }
                  catch
                  {
                  }
                  Thread.Sleep(100);
                }
                else
                  break;
              }
            }
          }
        }
        catch (Exception ex)
        {
          Program.LogException(ex);
        }
        try
        {
          Program.InitializeConfiguration();
        }
        catch (Exception ex)
        {
          int num = (int) MessageBox.Show("Error on reading configuration.\nDetails: " + ex.Message, Program.Title + " - Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
          return;
        }
        string currentDirectory = Directory.GetCurrentDirectory();
        string[] strArray = currentDirectory.Split('\\');
        if (strArray[strArray.Length - 1] == "bin")
          Directory.SetCurrentDirectory(Directory.GetParent(currentDirectory).FullName);
        Application.Run((Form) new FormMain());
      }
    }

    private static void RunSelfUpdateTest()
    {
      string tempFileName = Path.GetTempFileName();
      string str1 = Path.ChangeExtension(tempFileName, ".exe");
      if (File.Exists(str1))
        File.Delete(str1);
      File.Move(tempFileName, str1);
      File.WriteAllBytes(str1, LauncherResources.ExeUpdater);
      string executablePath = Application.ExecutablePath;
      string str2 = executablePath + ".tmp";
      File.Copy(Application.ExecutablePath, str2, true);
      Process.Start(str1, string.Format("-exeFileName \"{0}\" -replaceFileName \"{1}\" -waitForPid {2}", (object) Path.GetFullPath(executablePath), (object) Path.GetFullPath(str2), (object) Process.GetCurrentProcess().Id));
      Thread.Sleep(1000);
    }

    private static void InitializeConfiguration()
    {
      Program.m_Configuration = ConfigurationManager.OpenMappedExeConfiguration(new ExeConfigurationFileMap()
      {
        ExeConfigFilename = "LaunchNet7.cfg"
      }, ConfigurationUserLevel.None);
      Program.m_LauncherConfiguration = (LauncherConfigurationSection) Program.m_Configuration.Sections[LauncherConfigurationSection.DefaultSectionName];
      if (Program.m_LauncherConfiguration != null)
        return;
      Program.m_LauncherConfiguration = new LauncherConfigurationSection();
      Program.m_LauncherConfiguration.SectionInformation.Type = "LaunchNet7.Configuration.LauncherConfigurationSection, LaunchNet7";
      Program.m_LauncherConfiguration.SectionInformation.ForceSave = true;
      Program.m_Configuration.Sections.Add(LauncherConfigurationSection.DefaultSectionName, (ConfigurationSection) Program.m_LauncherConfiguration);
      Program.m_Configuration.Save(ConfigurationSaveMode.Minimal);
    }

    internal static string WorkingDirectory
    {
      get
      {
        return Program.m_WorkingDirectory;
      }
    }

    public static System.Configuration.Configuration Configuration
    {
      get
      {
        return Program.m_Configuration;
      }
      set
      {
        Program.m_Configuration = value;
      }
    }

    public static LauncherConfigurationSection LauncherConfiguration
    {
      get
      {
        return Program.m_LauncherConfiguration;
      }
    }

    public static string Title
    {
      get
      {
        return "LaunchNet7";
      }
    }

    public static string TitleForErrorMessages
    {
      get
      {
        return Program.Title + " - Error";
      }
    }

    public static string TitleForWarningMessages
    {
      get
      {
        return Program.Title + " - Warning";
      }
    }

    public static string TitleForInformationMessages
    {
      get
      {
        return Program.Title + " - Information";
      }
    }

    internal static void LogException(Exception e)
    {
      Program.LogException(e, (string) null);
    }

    internal static void LogException(Exception e, string message)
    {
      try
      {
        Program.EnsureUserDirectory();
        File.AppendAllText(Program.ErrorLogFileName, string.Format("\r\n\rLaunchNet7 - Exception\r\nVersion: {0}\r\nTime: {1}\r\nMessage: {2}\r\n{3}\r\n", (object) AssemblyFileInfo.Current.FileVersion.ToString(), (object) DateTime.UtcNow.ToString("R"), string.IsNullOrEmpty(message) ? (object) e.Message : (object) message, (object) e.ToString()));
      }
      catch
      {
      }
    }

    public static string ErrorLogFileName
    {
      get
      {
        if (Program.m_ErrorLogFileName == null)
          Program.m_ErrorLogFileName = Path.Combine(Program.UserDirectoryName, "LaunchNet7.Error.log");
        return Program.m_ErrorLogFileName;
      }
    }

    public static string UserDirectoryName
    {
      get
      {
        if (Program.m_UserDirectoryName == null)
          Program.m_UserDirectoryName = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "Net7\\LaunchNet7");
        return Program.m_UserDirectoryName;
      }
    }

    public static void EnsureUserDirectory()
    {
      if (string.IsNullOrEmpty(Program.UserDirectoryName))
        return;
      if (Directory.Exists(Program.UserDirectoryName))
        return;
      try
      {
        Directory.CreateDirectory(Program.UserDirectoryName);
      }
      catch
      {
      }
    }

    public static string ProgramDirectoryName
    {
      get
      {
        if (Program.m_ProgramDirectoryName == null)
          Program.m_ProgramDirectoryName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        return Program.m_ProgramDirectoryName;
      }
    }

    public static string ProgramFileName
    {
      get
      {
        if (Program.m_ProgramFileName == null)
          Program.m_ProgramFileName = Assembly.GetExecutingAssembly().Location;
        return Program.m_ProgramFileName;
      }
    }

    public static string ClientFileName
    {
      get
      {
        if (Program.m_ClientFileName == null)
        {
          try
          {
            Program.m_ClientFileName = Settings.Default.ClientPath;
          }
          catch
          {
            Program.m_ClientFileName = (string) null;
          }
        }
        return Program.m_ClientFileName;
      }
      set
      {
        Program.m_ClientFileName = value;
        try
        {
          Settings.Default.ClientPath = value;
        }
        catch
        {
        }
      }
    }

    public static string ClientRootDirectoryName
    {
      get
      {
        if (Program.m_ClientRootDirectoryName == null)
        {
          if (Program.ClientFileName != null)
          {
            try
            {
              Program.m_ClientRootDirectoryName = Directory.GetParent(Program.ClientFileName).Parent.FullName;
            }
            catch
            {
              Program.m_ClientRootDirectoryName = (string) null;
            }
          }
        }
        return Program.m_ClientRootDirectoryName;
      }
    }
  }
}
