﻿// Decompiled with JetBrains decompiler
// Type: LaunchNet7.Patching.AuthPatcherInfo
// Assembly: LaunchNet7, Version=1.9.9.10, Culture=neutral, PublicKeyToken=null
// MVID: AF666469-2FA2-4BE0-A9D5-7003BDE93D8C
// Assembly location: D:\Games\Net-7\bin\LaunchNet7.exe

namespace LaunchNet7.Patching
{
  public class AuthPatcherInfo
  {
    private bool m_UseHttps;
    private ushort m_Port;
    private ushort m_TimeOut;

    public bool UseHttps
    {
      get
      {
        return this.m_UseHttps;
      }
      set
      {
        this.m_UseHttps = value;
      }
    }

    public ushort Port
    {
      get
      {
        return this.m_Port;
      }
      set
      {
        this.m_Port = value;
      }
    }

    public ushort TimeOut
    {
      get
      {
        return this.m_TimeOut;
      }
      set
      {
        this.m_TimeOut = value;
      }
    }
  }
}
