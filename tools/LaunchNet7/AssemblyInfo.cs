﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyConfiguration("")]
[assembly: AssemblyTitle("LaunchNet7")]
[assembly: AssemblyDescription("Launching application to start the Earth & Beyond and required components.")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("LaunchNet7")]
[assembly: AssemblyCopyright("Copyright © Enb Emulator Project 2010")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(false)]
[assembly: Guid("ec4e9f83-a320-4432-85f2-8351dd70f0e4")]
[assembly: AssemblyFileVersion("1.9.9.10")]
[assembly: AssemblyVersion("1.9.9.10")]
