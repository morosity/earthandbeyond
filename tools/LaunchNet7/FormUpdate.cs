﻿// Decompiled with JetBrains decompiler
// Type: LaunchNet7.FormUpdate
// Assembly: LaunchNet7, Version=1.9.9.10, Culture=neutral, PublicKeyToken=null
// MVID: AF666469-2FA2-4BE0-A9D5-7003BDE93D8C
// Assembly location: D:\Games\Net-7\bin\LaunchNet7.exe

using LaunchNet7.Updateing;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace LaunchNet7
{
  public class FormUpdate : Form
  {
    private IContainer components;
    private Button c_Button_Update;
    private Button c_Button_Cancel;
    private Button c_Button_MoreInfo;
    private PictureBox c_PictureBox;
    private GroupBox groupBox1;
    private GroupBox groupBox2;
    private Label c_Label_Application;
    private Label label1;
    private Label label4;
    private Label c_Label_NewVersion;
    private Label c_Label_CurrentVersion;
    private Label label3;
    private ProgressBar c_ProgressBar2;
    private ProgressBar c_ProgressBar1;
    private Label c_LabelProgress2;
    private Label c_LabelProgress1;
    private Button c_Button_Skip;
    private TextBox c_TextBox_Changelog;
    private CheckBox c_CheckBox_WordWrap;
    private Label label2;
    private Label c_Label_Path;
    private Updater m_Updater;
    private bool m_AreDetailsVisible;
    private int HeightCollapsed;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.c_Button_Update = new Button();
      this.c_Button_Cancel = new Button();
      this.c_Button_MoreInfo = new Button();
      this.groupBox1 = new GroupBox();
      this.c_ProgressBar2 = new ProgressBar();
      this.c_ProgressBar1 = new ProgressBar();
      this.c_LabelProgress2 = new Label();
      this.c_LabelProgress1 = new Label();
      this.groupBox2 = new GroupBox();
      this.label2 = new Label();
      this.c_Label_Path = new Label();
      this.label4 = new Label();
      this.c_Label_NewVersion = new Label();
      this.c_Label_CurrentVersion = new Label();
      this.label3 = new Label();
      this.c_Label_Application = new Label();
      this.label1 = new Label();
      this.c_PictureBox = new PictureBox();
      this.c_Button_Skip = new Button();
      this.c_TextBox_Changelog = new TextBox();
      this.c_CheckBox_WordWrap = new CheckBox();
      this.groupBox1.SuspendLayout();
      this.groupBox2.SuspendLayout();
      ((ISupportInitialize) this.c_PictureBox).BeginInit();
      this.SuspendLayout();
      this.c_Button_Update.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.c_Button_Update.Location = new Point(379, 286);
      this.c_Button_Update.Name = "c_Button_Update";
      this.c_Button_Update.Size = new Size(75, 23);
      this.c_Button_Update.TabIndex = 0;
      this.c_Button_Update.Text = "&Update";
      this.c_Button_Update.UseVisualStyleBackColor = true;
      this.c_Button_Update.Click += new EventHandler(this.c_Button_Update_Click);
      this.c_Button_Cancel.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.c_Button_Cancel.Location = new Point(541, 286);
      this.c_Button_Cancel.Name = "c_Button_Cancel";
      this.c_Button_Cancel.Size = new Size(75, 23);
      this.c_Button_Cancel.TabIndex = 1;
      this.c_Button_Cancel.Text = "&Cancel";
      this.c_Button_Cancel.UseVisualStyleBackColor = true;
      this.c_Button_Cancel.Click += new EventHandler(this.c_Button_Cancel_Click);
      this.c_Button_MoreInfo.Enabled = false;
      this.c_Button_MoreInfo.Location = new Point(66, 286);
      this.c_Button_MoreInfo.Name = "c_Button_MoreInfo";
      this.c_Button_MoreInfo.Size = new Size(75, 23);
      this.c_Button_MoreInfo.TabIndex = 2;
      this.c_Button_MoreInfo.Text = "Details >>";
      this.c_Button_MoreInfo.UseVisualStyleBackColor = true;
      this.c_Button_MoreInfo.Click += new EventHandler(this.c_Button_MoreInfo_Click);
      this.groupBox1.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.groupBox1.Controls.Add((Control) this.c_ProgressBar2);
      this.groupBox1.Controls.Add((Control) this.c_ProgressBar1);
      this.groupBox1.Controls.Add((Control) this.c_LabelProgress2);
      this.groupBox1.Controls.Add((Control) this.c_LabelProgress1);
      this.groupBox1.Location = new Point(66, 165);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new Size(550, 115);
      this.groupBox1.TabIndex = 4;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Progress";
      this.c_ProgressBar2.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.c_ProgressBar2.Enabled = false;
      this.c_ProgressBar2.Location = new Point(9, 80);
      this.c_ProgressBar2.Name = "c_ProgressBar2";
      this.c_ProgressBar2.Size = new Size(535, 23);
      this.c_ProgressBar2.TabIndex = 3;
      this.c_ProgressBar2.Visible = false;
      this.c_ProgressBar1.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.c_ProgressBar1.Enabled = false;
      this.c_ProgressBar1.Location = new Point(9, 35);
      this.c_ProgressBar1.Name = "c_ProgressBar1";
      this.c_ProgressBar1.Size = new Size(535, 23);
      this.c_ProgressBar1.TabIndex = 2;
      this.c_ProgressBar1.Visible = false;
      this.c_LabelProgress2.AutoSize = true;
      this.c_LabelProgress2.Location = new Point(6, 64);
      this.c_LabelProgress2.Margin = new Padding(3, 3, 3, 0);
      this.c_LabelProgress2.Name = "c_LabelProgress2";
      this.c_LabelProgress2.Size = new Size(60, 13);
      this.c_LabelProgress2.TabIndex = 1;
      this.c_LabelProgress2.Text = "[Progress2]";
      this.c_LabelProgress1.AutoSize = true;
      this.c_LabelProgress1.Location = new Point(6, 19);
      this.c_LabelProgress1.Margin = new Padding(3, 3, 3, 0);
      this.c_LabelProgress1.Name = "c_LabelProgress1";
      this.c_LabelProgress1.Size = new Size(60, 13);
      this.c_LabelProgress1.TabIndex = 0;
      this.c_LabelProgress1.Text = "[Progress1]";
      this.groupBox2.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.groupBox2.Controls.Add((Control) this.label2);
      this.groupBox2.Controls.Add((Control) this.c_Label_Path);
      this.groupBox2.Controls.Add((Control) this.label4);
      this.groupBox2.Controls.Add((Control) this.c_Label_NewVersion);
      this.groupBox2.Controls.Add((Control) this.c_Label_CurrentVersion);
      this.groupBox2.Controls.Add((Control) this.label3);
      this.groupBox2.Controls.Add((Control) this.c_Label_Application);
      this.groupBox2.Controls.Add((Control) this.label1);
      this.groupBox2.Location = new Point(66, 12);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new Size(550, 147);
      this.groupBox2.TabIndex = 5;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Info";
      this.label2.AutoSize = true;
      this.label2.Location = new Point(6, 111);
      this.label2.Name = "label2";
      this.label2.Size = new Size(29, 13);
      this.label2.TabIndex = 6;
      this.label2.Text = "Path";
      this.c_Label_Path.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.c_Label_Path.AutoEllipsis = true;
      this.c_Label_Path.BorderStyle = BorderStyle.Fixed3D;
      this.c_Label_Path.Location = new Point(91, 106);
      this.c_Label_Path.Margin = new Padding(3);
      this.c_Label_Path.Name = "c_Label_Path";
      this.c_Label_Path.Size = new Size(453, 23);
      this.c_Label_Path.TabIndex = 5;
      this.c_Label_Path.Text = "label5";
      this.c_Label_Path.TextAlign = ContentAlignment.MiddleLeft;
      this.label4.AutoSize = true;
      this.label4.Location = new Point(6, 82);
      this.label4.Name = "label4";
      this.label4.Size = new Size(67, 13);
      this.label4.TabIndex = 4;
      this.label4.Text = "New Version";
      this.c_Label_NewVersion.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.c_Label_NewVersion.AutoEllipsis = true;
      this.c_Label_NewVersion.BorderStyle = BorderStyle.Fixed3D;
      this.c_Label_NewVersion.Location = new Point(91, 77);
      this.c_Label_NewVersion.Margin = new Padding(3);
      this.c_Label_NewVersion.Name = "c_Label_NewVersion";
      this.c_Label_NewVersion.Size = new Size(453, 23);
      this.c_Label_NewVersion.TabIndex = 3;
      this.c_Label_NewVersion.Text = "label5";
      this.c_Label_NewVersion.TextAlign = ContentAlignment.MiddleLeft;
      this.c_Label_CurrentVersion.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.c_Label_CurrentVersion.AutoEllipsis = true;
      this.c_Label_CurrentVersion.BorderStyle = BorderStyle.Fixed3D;
      this.c_Label_CurrentVersion.Location = new Point(91, 48);
      this.c_Label_CurrentVersion.Margin = new Padding(3);
      this.c_Label_CurrentVersion.Name = "c_Label_CurrentVersion";
      this.c_Label_CurrentVersion.Size = new Size(453, 23);
      this.c_Label_CurrentVersion.TabIndex = 2;
      this.c_Label_CurrentVersion.Text = "label2";
      this.c_Label_CurrentVersion.TextAlign = ContentAlignment.MiddleLeft;
      this.label3.AutoSize = true;
      this.label3.Location = new Point(6, 53);
      this.label3.Name = "label3";
      this.label3.Size = new Size(79, 13);
      this.label3.TabIndex = 3;
      this.label3.Text = "Current Version";
      this.c_Label_Application.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.c_Label_Application.AutoEllipsis = true;
      this.c_Label_Application.BorderStyle = BorderStyle.Fixed3D;
      this.c_Label_Application.Location = new Point(91, 19);
      this.c_Label_Application.Margin = new Padding(3);
      this.c_Label_Application.Name = "c_Label_Application";
      this.c_Label_Application.Size = new Size(453, 23);
      this.c_Label_Application.TabIndex = 1;
      this.c_Label_Application.Text = "label2";
      this.c_Label_Application.TextAlign = ContentAlignment.MiddleLeft;
      this.label1.AutoSize = true;
      this.label1.Location = new Point(6, 24);
      this.label1.Name = "label1";
      this.label1.Size = new Size(59, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Application";
      this.c_PictureBox.Location = new Point(12, 12);
      this.c_PictureBox.Name = "c_PictureBox";
      this.c_PictureBox.Size = new Size(48, 48);
      this.c_PictureBox.TabIndex = 3;
      this.c_PictureBox.TabStop = false;
      this.c_Button_Skip.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.c_Button_Skip.Enabled = false;
      this.c_Button_Skip.Location = new Point(460, 286);
      this.c_Button_Skip.Name = "c_Button_Skip";
      this.c_Button_Skip.Size = new Size(75, 23);
      this.c_Button_Skip.TabIndex = 6;
      this.c_Button_Skip.Text = "&Skip";
      this.c_Button_Skip.UseVisualStyleBackColor = true;
      this.c_Button_Skip.Click += new EventHandler(this.c_Button_Skip_Click);
      this.c_TextBox_Changelog.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.c_TextBox_Changelog.Font = new Font("Courier New", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.c_TextBox_Changelog.Location = new Point(66, 315);
      this.c_TextBox_Changelog.Multiline = true;
      this.c_TextBox_Changelog.Name = "c_TextBox_Changelog";
      this.c_TextBox_Changelog.ReadOnly = true;
      this.c_TextBox_Changelog.ScrollBars = ScrollBars.Both;
      this.c_TextBox_Changelog.Size = new Size(550, 170);
      this.c_TextBox_Changelog.TabIndex = 8;
      this.c_CheckBox_WordWrap.AutoSize = true;
      this.c_CheckBox_WordWrap.Checked = true;
      this.c_CheckBox_WordWrap.CheckState = CheckState.Checked;
      this.c_CheckBox_WordWrap.Location = new Point(147, 290);
      this.c_CheckBox_WordWrap.Name = "c_CheckBox_WordWrap";
      this.c_CheckBox_WordWrap.Size = new Size(81, 17);
      this.c_CheckBox_WordWrap.TabIndex = 9;
      this.c_CheckBox_WordWrap.Text = "Word-Wrap";
      this.c_CheckBox_WordWrap.UseVisualStyleBackColor = true;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(628, 497);
      this.Controls.Add((Control) this.c_CheckBox_WordWrap);
      this.Controls.Add((Control) this.c_TextBox_Changelog);
      this.Controls.Add((Control) this.c_Button_Skip);
      this.Controls.Add((Control) this.groupBox2);
      this.Controls.Add((Control) this.groupBox1);
      this.Controls.Add((Control) this.c_PictureBox);
      this.Controls.Add((Control) this.c_Button_MoreInfo);
      this.Controls.Add((Control) this.c_Button_Cancel);
      this.Controls.Add((Control) this.c_Button_Update);
      this.FormBorderStyle = FormBorderStyle.FixedDialog;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.MinimumSize = new Size(634, 319);
      this.Name = nameof (FormUpdate);
      this.ShowInTaskbar = false;
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "Update available";
      this.Load += new EventHandler(this.FormUpdate_Load);
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      ((ISupportInitialize) this.c_PictureBox).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public FormUpdate(Updater updater)
    {
      this.InitializeComponent();
      this.m_Updater = updater;
    }

    public Updater Updater
    {
      get
      {
        return this.m_Updater;
      }
    }

    public void ToggleDetails()
    {
      if (this.AreDetailsVisible)
      {
        this.ClientSize = new Size(this.ClientSize.Width, this.HeightCollapsed + 200);
        this.c_Button_MoreInfo.Text = "<< Details";
        this.c_TextBox_Changelog.Visible = false;
        this.c_CheckBox_WordWrap.Visible = false;
      }
      else
      {
        this.Size = new Size(634, 500);
        this.c_Button_MoreInfo.Text = "Details >>";
        this.c_TextBox_Changelog.Visible = true;
        this.c_CheckBox_WordWrap.Visible = true;
      }
      this.AreDetailsVisible = !this.AreDetailsVisible;
    }

    public bool AreDetailsVisible
    {
      get
      {
        return this.m_AreDetailsVisible;
      }
      set
      {
        this.m_AreDetailsVisible = value;
      }
    }

    private void FormUpdate_Load(object sender, EventArgs e)
    {
      this.HeightCollapsed = this.c_Button_Cancel.Bottom + 6;
      this.ClientSize = new Size(this.ClientSize.Width, this.HeightCollapsed);
      this.c_PictureBox.Image = (Image) LauncherResources.Setup_48x48x32;
      this.c_Label_Application.Text = this.Updater.Name;
      this.c_Label_CurrentVersion.Text = this.Updater.CurrentVersionText;
      this.c_Label_NewVersion.Text = this.Updater.NewVersionText;
      try
      {
        if (string.IsNullOrEmpty(this.Updater.TargetDirectoryName))
          this.c_Label_Path.Text = Environment.CurrentDirectory;
        else
          this.c_Label_Path.Text = Path.GetFullPath(this.Updater.TargetDirectoryName);
      }
      catch (Exception ex)
      {
        Program.LogException(ex, "Could not get full path for target directory name.");
        this.c_Label_Path.Text = "(Error)";
      }
      this.c_LabelProgress1.Text = string.Empty;
      this.c_LabelProgress2.Text = string.Empty;
      this.c_TextBox_Changelog.DataBindings.Add("WordWrap", (object) this.c_CheckBox_WordWrap, "Checked", false, DataSourceUpdateMode.OnPropertyChanged);
      if (!string.IsNullOrEmpty(this.Updater.CurrentChangelog))
      {
        this.c_Button_MoreInfo.Enabled = true;
        this.c_TextBox_Changelog.Text = this.Updater.CurrentChangelog;
        this.ToggleDetails();
      }
      else
        this.c_CheckBox_WordWrap.Visible = false;
    }

    private void c_Button_Cancel_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.Cancel;
      this.Close();
    }

    private void c_Button_Update_Click(object sender, EventArgs e)
    {
      if (!UpdateUtility.CanWriteToDirectory(this.Updater.TargetDirectoryName))
      {
        int num = (int) MessageBox.Show((IWin32Window) this, string.Format("Cannot write to target directory \"{0}\".\nPlease start this application with administrative rights or give write permissions for the target directory before starting the update process.", (object) this.Updater.TargetDirectoryName), Program.TitleForErrorMessages, MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
      else
        this.DoUpdateAsync();
    }

    private void DoUpdateAsync()
    {
      BackgroundWorker worker = new BackgroundWorker();
      worker.WorkerReportsProgress = true;
      worker.WorkerSupportsCancellation = false;
      worker.DoWork += (DoWorkEventHandler) ((sender, e) =>
      {
        this.m_Updater.UpdateWithWorker((BackgroundWorker) sender);
        if (!worker.CancellationPending)
          return;
        e.Cancel = false;
      });
      worker.ProgressChanged += (ProgressChangedEventHandler) ((sender, e) =>
      {
        switch (e.ProgressPercentage)
        {
          case -2:
            this.c_LabelProgress2.Text = e.UserState.ToString();
            break;
          case -1:
            this.c_LabelProgress1.Text = e.UserState.ToString();
            break;
          case 1:
            this.c_ProgressBar1.Value = ((Progress) e.UserState).ToProgressPercentage();
            break;
          case 2:
            this.c_ProgressBar2.Value = ((Progress) e.UserState).ToProgressPercentage();
            break;
        }
      });
      worker.RunWorkerCompleted += (RunWorkerCompletedEventHandler) ((sender, e) =>
      {
        this.c_ProgressBar1.Visible = false;
        this.c_ProgressBar2.Visible = false;
        this.c_LabelProgress1.Text = string.Empty;
        this.c_LabelProgress2.Text = string.Empty;
        this.c_Button_Skip.Enabled = true;
        this.c_Button_Update.Enabled = true;
        if (e.Error != null)
        {
          Program.LogException(e.Error);
          int num = (int) MessageBox.Show((IWin32Window) this, !(e.Error is ApplicationException) ? "Update failed.\nDetails: " + e.Error.Message + "\nFile: " + this.c_LabelProgress1.Text : "Update failed. " + e.Error.Message, Program.TitleForErrorMessages, MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }
        else if (e.Cancelled)
        {
          this.DialogResult = DialogResult.Cancel;
          this.Close();
        }
        else
        {
          bool flag1 = false;
          bool flag2 = true;
          if (this.m_Updater.UpdateErrorsOccured)
          {
            using (FormUpdateReport formUpdateReport = new FormUpdateReport())
            {
              formUpdateReport.Updater = this.Updater;
              switch (formUpdateReport.ShowDialog())
              {
                case DialogResult.Retry:
                  this.DoUpdateAsync();
                  return;
                case DialogResult.Ignore:
                  flag2 = true;
                  break;
              }
            }
            flag1 = true;
          }
          if (flag2)
          {
            try
            {
              this.Updater.SaveVersion();
            }
            catch (Exception ex)
            {
              Program.LogException(ex, "Could not save version number.");
              int num = (int) MessageBox.Show((IWin32Window) this, "Could not save new version number.\nDetails: " + ex.Message);
            }
          }
          if (!flag1 && MessageBox.Show((IWin32Window) this, "Do you want to view the update report?", Program.TitleForInformationMessages, MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk) == DialogResult.OK)
          {
            using (FormUpdateReport formUpdateReport = new FormUpdateReport())
            {
              formUpdateReport.Updater = this.Updater;
              int num = (int) formUpdateReport.ShowDialog();
            }
          }
          this.DialogResult = DialogResult.OK;
          this.Close();
        }
      });
      this.c_ProgressBar1.Visible = true;
      this.c_ProgressBar2.Visible = true;
      this.c_Button_Skip.Enabled = false;
      this.c_Button_Update.Enabled = false;
      worker.RunWorkerAsync();
    }

    private void c_Button_Skip_Click(object sender, EventArgs e)
    {
      this.DialogResult = DialogResult.Ignore;
      this.Close();
    }

    private void c_Button_MoreInfo_Click(object sender, EventArgs e)
    {
      this.ToggleDetails();
    }
  }
}
