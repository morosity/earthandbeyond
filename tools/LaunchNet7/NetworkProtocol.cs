﻿// Decompiled with JetBrains decompiler
// Type: LaunchNet7.NetworkProtocol
// Assembly: LaunchNet7, Version=1.9.9.10, Culture=neutral, PublicKeyToken=null
// MVID: AF666469-2FA2-4BE0-A9D5-7003BDE93D8C
// Assembly location: D:\Games\Net-7\bin\LaunchNet7.exe

namespace LaunchNet7
{
  public enum NetworkProtocol
  {
    Auto,
    TCP,
    UDP,
  }
}
