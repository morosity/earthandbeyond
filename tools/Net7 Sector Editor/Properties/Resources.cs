﻿// Decompiled with JetBrains decompiler
// Type: N7.Properties.Resources
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace N7.Properties
{
  [DebuggerNonUserCode]
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "2.0.0.0")]
  [CompilerGenerated]
  internal class Resources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal Resources()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (object.ReferenceEquals((object) N7.Properties.Resources.resourceMan, (object) null))
          N7.Properties.Resources.resourceMan = new ResourceManager("N7.Properties.Resources", typeof (N7.Properties.Resources).Assembly);
        return N7.Properties.Resources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return N7.Properties.Resources.resourceCulture;
      }
      set
      {
        N7.Properties.Resources.resourceCulture = value;
      }
    }

    internal static Bitmap classSpecificGate
    {
      get
      {
        return (Bitmap) N7.Properties.Resources.ResourceManager.GetObject(nameof (classSpecificGate), N7.Properties.Resources.resourceCulture);
      }
    }

    internal static Bitmap classSpecificNav
    {
      get
      {
        return (Bitmap) N7.Properties.Resources.ResourceManager.GetObject(nameof (classSpecificNav), N7.Properties.Resources.resourceCulture);
      }
    }

    internal static Bitmap document_new
    {
      get
      {
        return (Bitmap) N7.Properties.Resources.ResourceManager.GetObject(nameof (document_new), N7.Properties.Resources.resourceCulture);
      }
    }

    internal static Icon EB
    {
      get
      {
        return (Icon) N7.Properties.Resources.ResourceManager.GetObject(nameof (EB), N7.Properties.Resources.resourceCulture);
      }
    }

    internal static Bitmap FactionSpecificGate
    {
      get
      {
        return (Bitmap) N7.Properties.Resources.ResourceManager.GetObject(nameof (FactionSpecificGate), N7.Properties.Resources.resourceCulture);
      }
    }

    internal static Bitmap friendlyMob
    {
      get
      {
        return (Bitmap) N7.Properties.Resources.ResourceManager.GetObject(nameof (friendlyMob), N7.Properties.Resources.resourceCulture);
      }
    }

    internal static Bitmap hiddenGate
    {
      get
      {
        return (Bitmap) N7.Properties.Resources.ResourceManager.GetObject(nameof (hiddenGate), N7.Properties.Resources.resourceCulture);
      }
    }

    internal static Bitmap hiddenNav
    {
      get
      {
        return (Bitmap) N7.Properties.Resources.ResourceManager.GetObject(nameof (hiddenNav), N7.Properties.Resources.resourceCulture);
      }
    }

    internal static Bitmap hostileMob
    {
      get
      {
        return (Bitmap) N7.Properties.Resources.ResourceManager.GetObject(nameof (hostileMob), N7.Properties.Resources.resourceCulture);
      }
    }

    internal static Bitmap list_add
    {
      get
      {
        return (Bitmap) N7.Properties.Resources.ResourceManager.GetObject(nameof (list_add), N7.Properties.Resources.resourceCulture);
      }
    }

    internal static Bitmap media_floppy_smaller
    {
      get
      {
        return (Bitmap) N7.Properties.Resources.ResourceManager.GetObject(nameof (media_floppy_smaller), N7.Properties.Resources.resourceCulture);
      }
    }

    internal static Bitmap nuetralMob
    {
      get
      {
        return (Bitmap) N7.Properties.Resources.ResourceManager.GetObject(nameof (nuetralMob), N7.Properties.Resources.resourceCulture);
      }
    }

    internal static Bitmap planet
    {
      get
      {
        return (Bitmap) N7.Properties.Resources.ResourceManager.GetObject(nameof (planet), N7.Properties.Resources.resourceCulture);
      }
    }

    internal static Bitmap planetLandable
    {
      get
      {
        return (Bitmap) N7.Properties.Resources.ResourceManager.GetObject(nameof (planetLandable), N7.Properties.Resources.resourceCulture);
      }
    }

    internal static Bitmap process_stop
    {
      get
      {
        return (Bitmap) N7.Properties.Resources.ResourceManager.GetObject(nameof (process_stop), N7.Properties.Resources.resourceCulture);
      }
    }

    internal static Bitmap resource
    {
      get
      {
        return (Bitmap) N7.Properties.Resources.ResourceManager.GetObject(nameof (resource), N7.Properties.Resources.resourceCulture);
      }
    }

    internal static Bitmap resourceField
    {
      get
      {
        return (Bitmap) N7.Properties.Resources.ResourceManager.GetObject(nameof (resourceField), N7.Properties.Resources.resourceCulture);
      }
    }

    internal static Bitmap space1
    {
      get
      {
        return (Bitmap) N7.Properties.Resources.ResourceManager.GetObject(nameof (space1), N7.Properties.Resources.resourceCulture);
      }
    }

    internal static Bitmap standardGate
    {
      get
      {
        return (Bitmap) N7.Properties.Resources.ResourceManager.GetObject(nameof (standardGate), N7.Properties.Resources.resourceCulture);
      }
    }

    internal static Bitmap standardNav
    {
      get
      {
        return (Bitmap) N7.Properties.Resources.ResourceManager.GetObject(nameof (standardNav), N7.Properties.Resources.resourceCulture);
      }
    }

    internal static Bitmap stations
    {
      get
      {
        return (Bitmap) N7.Properties.Resources.ResourceManager.GetObject(nameof (stations), N7.Properties.Resources.resourceCulture);
      }
    }

    internal static Bitmap view_fullscreen
    {
      get
      {
        return (Bitmap) N7.Properties.Resources.ResourceManager.GetObject(nameof (view_fullscreen), N7.Properties.Resources.resourceCulture);
      }
    }

    internal static Bitmap wb_arrow
    {
      get
      {
        return (Bitmap) N7.Properties.Resources.ResourceManager.GetObject(nameof (wb_arrow), N7.Properties.Resources.resourceCulture);
      }
    }
  }
}
