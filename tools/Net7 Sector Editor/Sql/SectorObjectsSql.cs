﻿// Decompiled with JetBrains decompiler
// Type: N7.Sql.SectorObjectsSql
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using System;
using System.Data;
using System.Windows.Forms;

namespace N7.Sql
{
  public class SectorObjectsSql
  {
    private DataTable sectorObjects;
    private string sectorID;

    public SectorObjectsSql(string sectorName)
    {
      DataTable dataTable = Database.executeQuery(Database.DatabaseName.net7, "SELECT sector_id FROM sectors where name='" + sectorName.Replace("'", "''") + "';");
      foreach (DataRow row in (InternalDataCollectionBase) dataTable.Rows)
      {
        this.sectorID = row["sector_id"].ToString();
        this.sectorObjects = Database.executeQuery(Database.DatabaseName.net7, "SELECT * FROM sector_objects left join sector_nav_points on sector_objects.sector_object_id = sector_nav_points.sector_object_id left join sector_objects_harvestable on sector_objects.sector_object_id = sector_objects_harvestable.resource_id left join sector_objects_planets on sector_objects.sector_object_id = sector_objects_planets.planet_id left join sector_objects_starbases on sector_objects.sector_object_id = sector_objects_starbases.starbase_id left join sector_objects_stargates on sector_objects.sector_object_id = sector_objects_stargates.stargate_id left join sector_objects_mob on sector_objects.sector_object_id = sector_objects_mob.mob_id where sector_objects.sector_id='" + this.sectorID + "' order by sector_objects.type;");
      }
      dataTable.Dispose();
    }

    public void updateRow(DataRow r)
    {
      int int32_1 = Convert.ToInt32(r["is_huge"]);
      int int32_2 = Convert.ToInt32(r["appears_in_radar"]);
      int num1 = int.Parse(r["type"].ToString());
      string str = r["name"].ToString().Replace("'", "''");
      string query1 = "UPDATE sector_nav_points SET nav_type='" + r["nav_type"].ToString() + "', signature='" + r["signature"].ToString() + "', is_huge='" + (object) int32_1 + "', base_xp='" + r["base_xp"].ToString() + "', exploration_range='" + r["exploration_range"].ToString() + "' where sector_object_id='" + r["sector_object_id"].ToString() + "';";
      string query2 = "UPDATE sector_objects SET base_asset_id='" + r["base_asset_id"].ToString() + "', h='" + r["h"].ToString() + "', s='" + r["s"].ToString() + "' ,v='" + r["v"].ToString() + "', type='" + r["type"].ToString() + "', scale='" + r["scale"].ToString() + "', position_x='" + r["position_x"].ToString() + "', position_y='" + r["position_y"].ToString() + "', position_z='" + r["position_z"].ToString() + "', orientation_u='" + r["orientation_u"].ToString() + "', orientation_v='" + r["orientation_v"].ToString() + "', orientation_w='" + r["orientation_w"].ToString() + "', orientation_z='" + r["orientation_z"].ToString() + "', name='" + str + "', appears_in_radar='" + (object) int32_2 + "', radar_range='" + r["radar_range"].ToString() + "', gate_to='" + r["gate_to"].ToString() + "', sound_effect_id='" + r["sound_effect_id"].ToString() + "', sound_effect_range='" + r["sound_effect_range"].ToString() + "' where sector_object_id='" + r["sector_object_id"].ToString() + "';";
      try
      {
        Database.executeQuery(Database.DatabaseName.net7, query1);
        Database.executeQuery(Database.DatabaseName.net7, query2);
        string query3 = "";
        switch (num1)
        {
          case 0:
            int int32_3 = Convert.ToInt32(r["delayed_spawn"]);
            query3 = "UPDATE sector_objects_mob SET mob_count='" + r["mob_count"].ToString() + "', mob_spawn_radius='" + r["mob_spawn_radius"].ToString() + "', respawn_time='" + r["respawn_time"].ToString() + "', delayed_spawn='" + (object) int32_3 + "' where mob_id='" + r["mob_id"].ToString() + "';";
            break;
          case 3:
            int int32_4 = Convert.ToInt32(r["is_landable"]);
            query3 = "UPDATE sector_objects_planets SET orbit_id='" + r["orbit_id"].ToString() + "', orbit_dist='" + r["orbit_dist"].ToString() + "', orbit_angle='" + r["orbit_angle"].ToString() + "', orbit_rate='" + r["orbit_rate"].ToString() + "', rotate_angle='" + r["rotate_angle"].ToString() + "', rotate_rate='" + r["rotate_rate"].ToString() + "', tilt_angle='" + r["tilt_angle"].ToString() + "', is_landable='" + (object) int32_4 + "' where planet_id='" + r["planet_id"].ToString() + "';";
            break;
          case 11:
            query3 = "UPDATE sector_objects_stargates SET classSpecific='" + (object) Convert.ToInt32(r["classSpecific"]) + "', minSecurityLevel='" + (object) Convert.ToInt32(r["minSecurityLevel"]) + "', faction_id='" + r["faction_id"].ToString() + "' where stargate_id='" + r["stargate_id"].ToString() + "';";
            break;
          case 12:
            query3 = "UPDATE sector_objects_starbases SET capShip='" + (object) Convert.ToInt32(r["capShip"]) + "', dockable='" + (object) Convert.ToInt32(r["dockable"]) + "' where starbase_id='" + r["starbase_id"].ToString() + "';";
            break;
          case 38:
            query3 = "UPDATE sector_objects_harvestable SET level='" + r["level"].ToString() + "', field='" + r["field"].ToString() + "', res_count='" + r["res_count"].ToString() + "', spawn_radius='" + r["spawn_radius"].ToString() + "', pop_rock_chance='" + r["pop_rock_chance"].ToString() + "', max_field_radius='" + r["max_field_radius"].ToString() + "', respawn_timer='" + r["respawn_timer"].ToString() + "' where resource_id='" + r["resource_id"].ToString() + "';";
            break;
        }
        if (num1 == 37 || num1 == 40 || num1 == 41)
          return;
        Database.executeQuery(Database.DatabaseName.net7, query3);
      }
      catch (Exception ex)
      {
        int num2 = (int) MessageBox.Show(ex.ToString() + "\n" + ex.StackTrace);
      }
    }

    public void deleteRow(int id, int type)
    {
      string query1 = "DELETE FROM sector_nav_points where sector_object_id='" + (object) id + "';";
      string query2 = "DELETE FROM sector_objects where sector_object_id='" + (object) id + "';";
      string query3 = "";
      string query4 = "";
      string query5 = "";
      switch (type)
      {
        case 0:
          query3 = "DELETE FROM sector_objects_mob where mob_id='" + (object) id + "';";
          query4 = "DELETE FROM mob_spawn_group where spawn_group_id='" + (object) id + "';";
          break;
        case 3:
          query3 = "DELETE FROM sector_objects_planets where planet_id='" + (object) id + "';";
          break;
        case 11:
          query3 = "DELETE FROM sector_objects_stargates where stargate_id='" + (object) id + "';";
          break;
        case 12:
          query3 = "DELETE FROM sector_objects_starbases where starbase_id='" + (object) id + "';";
          break;
        case 38:
          query3 = "DELETE FROM sector_objects_harvestable where resource_id='" + (object) id + "';";
          query5 = "DELETE FROM sector_objects_harvestable_restypes where group_id='" + (object) id + "';";
          query4 = "DELETE FROM mob_spawn_group where spawn_group_id='" + (object) id + "';";
          break;
      }
      try
      {
        if (type == 0 || type == 38)
          Database.executeQuery(Database.DatabaseName.net7, query4);
        if (type == 38)
          Database.executeQuery(Database.DatabaseName.net7, query5);
        if (type != 37 && type != 40 && type != 41)
          Database.executeQuery(Database.DatabaseName.net7, query3);
        Database.executeQuery(Database.DatabaseName.net7, query1);
        Database.executeQuery(Database.DatabaseName.net7, query2);
      }
      catch (Exception ex)
      {
        throw;
      }
    }

    public void newRow(DataRow r)
    {
      int int32_1 = Convert.ToInt32(r["is_huge"]);
      int int32_2 = Convert.ToInt32(r["appears_in_radar"]);
      int num1 = int.Parse(r["type"].ToString());
      string str = r["name"].ToString().Replace("'", "''");
      Console.Out.WriteLine(r["sound_effect_id"].ToString());
      string query1 = "INSERT INTO sector_objects SET base_asset_id='" + r["base_asset_id"].ToString() + "', h='" + r["h"].ToString() + "', s='" + r["s"].ToString() + "' ,v='" + r["v"].ToString() + "', type='" + r["type"].ToString() + "', scale='" + r["scale"].ToString() + "', position_x='" + r["position_x"].ToString() + "', position_y='" + r["position_y"].ToString() + "', position_z='" + r["position_z"].ToString() + "', orientation_u='" + r["orientation_u"].ToString() + "', orientation_v='" + r["orientation_v"].ToString() + "', orientation_w='" + r["orientation_w"].ToString() + "', orientation_z='" + r["orientation_z"].ToString() + "', name='" + str + "', appears_in_radar='" + (object) int32_2 + "', sector_id='" + r["sector_id"].ToString() + "', radar_range='" + r["radar_range"].ToString() + "', gate_to='" + r["gate_to"].ToString() + "', sound_effect_id='" + r["sound_effect_id"].ToString() + "', sound_effect_range='" + r["sound_effect_range"].ToString() + "';";
      int num2 = 0;
      try
      {
        Database.executeQuery(Database.DatabaseName.net7, query1);
        foreach (DataRow row in (InternalDataCollectionBase) Database.executeQuery(Database.DatabaseName.net7, "SELECT LAST_INSERT_ID()").Rows)
          num2 = int.Parse(row["LAST_INSERT_ID()"].ToString());
      }
      catch (Exception ex)
      {
        throw;
      }
      string query2 = "INSERT INTO sector_nav_points SET sector_object_id='" + (object) num2 + "', nav_type='" + r["nav_type"].ToString() + "', signature='" + r["signature"].ToString() + "', is_huge='" + (object) int32_1 + "', sector_id='" + r["sector_id"].ToString() + "', base_xp='" + r["base_xp"].ToString() + "', exploration_range='" + r["exploration_range"].ToString() + "';";
      try
      {
        Database.executeQuery(Database.DatabaseName.net7, query2);
        string query3 = "";
        switch (num1)
        {
          case 0:
            int int32_3 = Convert.ToInt32(r["delayed_spawn"]);
            query3 = "INSERT INTO sector_objects_mob SET mob_id='" + (object) num2 + "', mob_count='" + r["mob_count"].ToString() + "', mob_spawn_radius='" + r["mob_spawn_radius"].ToString() + "', respawn_time='" + r["respawn_time"].ToString() + "', delayed_spawn='" + (object) int32_3 + "';";
            break;
          case 3:
            int int32_4 = Convert.ToInt32(r["is_landable"]);
            query3 = "INSERT INTO sector_objects_planets SET planet_id='" + (object) num2 + "', orbit_id='" + r["orbit_id"].ToString() + "', orbit_dist='" + r["orbit_dist"].ToString() + "', orbit_angle='" + r["orbit_angle"].ToString() + "', orbit_rate='" + r["orbit_rate"].ToString() + "', rotate_angle='" + r["rotate_angle"].ToString() + "', rotate_rate='" + r["rotate_rate"].ToString() + "', tilt_angle='" + r["tilt_angle"].ToString() + "', is_landable='" + (object) int32_4 + "';";
            break;
          case 11:
            int int32_5 = Convert.ToInt32(r["classSpecific"]);
            int int32_6 = Convert.ToInt32(r["minSecurityLevel"]);
            query3 = "INSERT INTO sector_objects_stargates SET stargate_id='" + (object) num2 + "', classSpecific='" + (object) int32_5 + "', minSecurityLevel='" + (object) int32_6 + "', faction_id='" + r["faction_id"].ToString() + "';";
            break;
          case 12:
            int int32_7 = Convert.ToInt32(r["capShip"]);
            int int32_8 = Convert.ToInt32(r["dockable"]);
            query3 = "INSERT INTO sector_objects_starbases SET starbase_id='" + (object) num2 + "', capShip='" + (object) int32_7 + "', dockable='" + (object) int32_8 + "';";
            break;
          case 38:
            query3 = "INSERT INTO sector_objects_harvestable SET resource_id='" + (object) num2 + "', level='" + r["level"].ToString() + "', field='" + r["field"].ToString() + "', res_count='" + r["res_count"].ToString() + "', spawn_radius='" + r["spawn_radius"].ToString() + "', pop_rock_chance='" + r["pop_rock_chance"].ToString() + "', max_field_radius='" + r["max_field_radius"].ToString() + "', respawn_timer='" + r["respawn_timer"].ToString() + "';";
            break;
        }
        if (num1 != 37 && num1 != 40 && num1 != 41)
          Database.executeQuery(Database.DatabaseName.net7, query3);
        r["sector_object_id"] = (object) num2;
        r.AcceptChanges();
      }
      catch (Exception ex)
      {
        throw;
      }
    }

    public DataTable getSectorObject()
    {
      return this.sectorObjects;
    }
  }
}
