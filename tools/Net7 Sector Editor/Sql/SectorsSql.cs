﻿// Decompiled with JetBrains decompiler
// Type: N7.Sql.SectorsSql
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using System;
using System.Data;

namespace N7.Sql
{
  public class SectorsSql
  {
    private DataTable sectors;

    public SectorsSql()
    {
      this.sectors = Database.executeQuery(Database.DatabaseName.net7, "SELECT * FROM sectors order by system_id, name");
    }

    public DataTable getSectorTable()
    {
      return this.sectors;
    }

    public DataRow[] findRowsByName(string name)
    {
      return this.sectors.Select("name Like '" + name.Replace("'", "''") + "'");
    }

    public DataRow[] getRowsBySystemID(string systemID)
    {
      return this.sectors.Select("system_id = '" + systemID + "'");
    }

    public DataTable queryBySystemID(string systemID)
    {
      return Database.executeQuery(Database.DatabaseName.net7, "SELECT * FROM sectors where system_id='" + systemID + "' order by name");
    }

    public int getIDFromName(string RawName)
    {
      return int.Parse(this.sectors.Select("name Like '" + RawName.Replace("'", "''") + "'")[0]["sector_id"].ToString());
    }

    public void updateRow(DataRow r)
    {
      string str1 = r["name"].ToString().Replace("'", "''");
      string str2 = r["greetings"].ToString().Replace("'", "''");
      string str3 = r["notes"].ToString().Replace("'", "''");
      int int32_1 = Convert.ToInt32(r["light_backdrop"]);
      int int32_2 = Convert.ToInt32(r["swap_backdrop"]);
      int int32_3 = Convert.ToInt32(r["fog_backdrop"]);
      int int32_4 = Convert.ToInt32(r["auto_level"]);
      string query = "UPDATE sectors SET name='" + str1 + "', x_min='" + r["x_min"].ToString() + "', x_max='" + r["x_max"].ToString() + "', y_min='" + r["y_min"].ToString() + "', y_max='" + r["y_max"].ToString() + "', z_min='" + r["z_min"].ToString() + "', z_max='" + r["z_max"].ToString() + "', grid_x='" + r["grid_x"].ToString() + "', grid_y='" + r["grid_y"].ToString() + "', grid_z='" + r["grid_z"].ToString() + "', fog_near='" + r["fog_near"].ToString() + "', fog_far='" + r["fog_far"].ToString() + "', debris_mode='" + r["debris_mode"].ToString() + "', light_backdrop='" + (object) int32_1 + "', fog_backdrop='" + (object) int32_3 + "', swap_backdrop='" + (object) int32_2 + "', backdrop_fog_near='" + r["backdrop_fog_near"].ToString() + "', backdrop_fog_far='" + r["backdrop_fog_far"].ToString() + "', max_tilt='" + r["max_tilt"].ToString() + "', auto_level='" + (object) int32_4 + "', impulse_rate='" + r["impulse_rate"].ToString() + "', decay_velocity='" + r["decay_velocity"].ToString() + "', decay_spin='" + r["decay_spin"].ToString() + "', backdrop_asset='" + r["backdrop_asset"].ToString() + "', greetings='" + str2 + "', notes='" + str3 + "', system_id='" + r["system_id"].ToString() + "', galaxy_x='" + r["galaxy_x"].ToString() + "', galaxy_y='" + r["galaxy_y"].ToString() + "', galaxy_z='" + r["galaxy_z"].ToString() + "', sector_type='+" + r["sector_type"].ToString() + "' where sector_id='" + r["sector_id"].ToString() + "';";
      try
      {
        Database.executeQuery(Database.DatabaseName.net7, query);
      }
      catch (Exception ex)
      {
        throw;
      }
    }

    public void newRow(DataRow r)
    {
      string str1 = r["name"].ToString().Replace("'", "''");
      string str2 = r["greetings"].ToString().Replace("'", "''");
      string str3 = r["notes"].ToString().Replace("'", "''");
      int int32_1 = Convert.ToInt32(r["light_backdrop"]);
      int int32_2 = Convert.ToInt32(r["swap_backdrop"]);
      int int32_3 = Convert.ToInt32(r["fog_backdrop"]);
      int int32_4 = Convert.ToInt32(r["auto_level"]);
      int int32_5 = Convert.ToInt32(r["sector_type"]);
      string query = "INSERT INTO sectors SET sector_id='" + r["sector_id"] + "', name='" + str1 + "', x_min='" + r["x_min"].ToString() + "', x_max='" + r["x_max"].ToString() + "', y_min='" + r["y_min"].ToString() + "', y_max='" + r["y_max"].ToString() + "', z_min='" + r["z_min"].ToString() + "', z_max='" + r["z_max"].ToString() + "', grid_x='" + r["grid_x"].ToString() + "', grid_y='" + r["grid_y"].ToString() + "', grid_z='" + r["grid_z"].ToString() + "', fog_near='" + r["fog_near"].ToString() + "', fog_far='" + r["fog_far"].ToString() + "', debris_mode='" + r["debris_mode"].ToString() + "', light_backdrop='" + (object) int32_1 + "', fog_backdrop='" + (object) int32_3 + "', swap_backdrop='" + (object) int32_2 + "', backdrop_fog_near='" + r["backdrop_fog_near"].ToString() + "', backdrop_fog_far='" + r["backdrop_fog_far"].ToString() + "', max_tilt='" + r["max_tilt"].ToString() + "', auto_level='" + (object) int32_4 + "', impulse_rate='" + r["impulse_rate"].ToString() + "', decay_velocity='" + r["decay_velocity"].ToString() + "', decay_spin='" + r["decay_spin"].ToString() + "', backdrop_asset='" + r["backdrop_asset"].ToString() + "', greetings='" + str2 + "', notes='" + str3 + "', system_id='" + r["system_id"].ToString() + "', galaxy_x='" + r["galaxy_x"].ToString() + "', galaxy_y='" + r["galaxy_y"].ToString() + "', galaxy_z='" + r["galaxy_z"].ToString() + "', sector_type='" + int32_5.ToString() + "';";
      try
      {
        Database.executeQuery(Database.DatabaseName.net7, query);
      }
      catch (Exception ex)
      {
        throw;
      }
    }
  }
}
