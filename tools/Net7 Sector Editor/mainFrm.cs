﻿// Decompiled with JetBrains decompiler
// Type: N7.mainFrm
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using N7.GUI;
using N7.Properties;
using N7.Props;
using N7.Sql;
using N7.Utilities;
using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using TD.SandDock;
using TD.SandDock.Rendering;
using UMD.HCIL.Piccolo;
using UMD.HCIL.Piccolo.Event;

namespace N7
{
    public class mainFrm : Form
    {
        private bool firstRun = true;
        private IContainer components;
        private MenuStrip menuStrip1;
        private StatusStrip statusStrip1;
        private SandDockManager sandDockManager1;
        private DockableWindow sectorDock;
        private DockableWindow toolboxDock;
        private DockContainer dockContainer2;
        private DockableWindow PropertiesDock;
        private DockContainer dockContainer1;
        private ToolStripMenuItem fileToolStripMenuItem;
        private ToolStripMenuItem newSectorToolStripMenuItem;
        private ToolStripMenuItem saveSectorToolStripMenuItem;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripMenuItem exitToolStripMenuItem;
        private ToolStripMenuItem helpToolStripMenuItem;
        private ToolStripMenuItem aboutToolStripMenuItem;
        private ToolStripStatusLabel toolStripStatusLabel1;
        private ToolStrip toolStrip1;
        private ToolStripButton newToolbarClick;
        private ToolStripButton save;
        private TreeView treeView1;
        private PropertyGrid propertyGrid1;
        private DocumentContainer documentContainer1;
        private TabbedDocument SectorTab;
        private PCanvas pCanvas1;
        private ToolStrip toolStrip2;
        private ToolStripButton toolStripButton3;
        private ToolStripButton toolStripButton4;
        private OptionsGui optionsGui1;
        private ObjectList objList;
        private ToolStripButton deleteSelected;
        private ToolStripMenuItem systemToolStripMenuItem;
        private ToolStripMenuItem sectorToolStripMenuItem;
        private ToolStripMenuItem sectorObjectToolStripMenuItem;
        private ToolStripMenuItem editToolStripMenuItem;
        private ToolStripMenuItem settingsToolStripMenuItem;
        private ToolStripButton toolStripButton1;
        private ToolStripSeparator toolStripSeparator2;
        private ToolStripSeparator toolStripSeparator3;
        private DockContainer dockContainer3;
        private DockableWindow sectorOptionsDock;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage sectorVisOptions;
        private System.Windows.Forms.TabPage sectorTblList;
        public static FactionSql factions;
        public static SectorsSql sectors;
        public static SystemsSql systems;
        public static MobsSQL mobs;
        public static BaseAssetSQL baseAssets;
        public static SectorObjectsSql sectorObjects;
        public static SectorWindow sectorWindow;
        public static SystemWindow systemWindow;
        public static int selectedObjectID;
        private PDragEventHandler drag;
        private PPanEventHandler pan;
        private DataRow selectedSystemRow;
        public static int sectorID;

        protected override void Dispose(bool disposing)
        {
            if (disposing && this.components != null)
                this.components.Dispose();
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            DockingRules dockingRules1 = new DockingRules();
            DockingRules dockingRules2 = new DockingRules();
            ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof(mainFrm));
            this.menuStrip1 = new MenuStrip();
            this.fileToolStripMenuItem = new ToolStripMenuItem();
            this.newSectorToolStripMenuItem = new ToolStripMenuItem();
            this.systemToolStripMenuItem = new ToolStripMenuItem();
            this.sectorToolStripMenuItem = new ToolStripMenuItem();
            this.sectorObjectToolStripMenuItem = new ToolStripMenuItem();
            this.saveSectorToolStripMenuItem = new ToolStripMenuItem();
            this.toolStripSeparator1 = new ToolStripSeparator();
            this.exitToolStripMenuItem = new ToolStripMenuItem();
            this.editToolStripMenuItem = new ToolStripMenuItem();
            this.settingsToolStripMenuItem = new ToolStripMenuItem();
            this.helpToolStripMenuItem = new ToolStripMenuItem();
            this.aboutToolStripMenuItem = new ToolStripMenuItem();
            this.statusStrip1 = new StatusStrip();
            this.toolStripStatusLabel1 = new ToolStripStatusLabel();
            this.sandDockManager1 = new SandDockManager();
            this.sectorDock = new DockableWindow();
            this.treeView1 = new TreeView();
            this.PropertiesDock = new DockableWindow();
            this.toolboxDock = new DockableWindow();
            this.toolStrip2 = new ToolStrip();
            this.toolStripButton3 = new ToolStripButton();
            this.toolStripButton4 = new ToolStripButton();
            this.toolStripSeparator3 = new ToolStripSeparator();
            this.deleteSelected = new ToolStripButton();
            this.toolStripButton1 = new ToolStripButton();
            this.toolStripSeparator2 = new ToolStripSeparator();
            this.dockContainer2 = new DockContainer();
            this.dockContainer1 = new DockContainer();
            this.newToolbarClick = new ToolStripButton();
            this.save = new ToolStripButton();
            this.toolStrip1 = new ToolStrip();
            this.SectorTab = new TabbedDocument();
            this.pCanvas1 = new PCanvas();
            this.documentContainer1 = new DocumentContainer();
            this.dockContainer3 = new DockContainer();
            this.sectorOptionsDock = new DockableWindow();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.sectorVisOptions = new System.Windows.Forms.TabPage();
            this.sectorTblList = new System.Windows.Forms.TabPage();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.sectorDock.SuspendLayout();
            this.toolboxDock.SuspendLayout();
            this.toolStrip2.SuspendLayout();
            this.dockContainer2.SuspendLayout();
            this.dockContainer1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SectorTab.SuspendLayout();
            this.documentContainer1.SuspendLayout();
            this.dockContainer3.SuspendLayout();
            this.sectorOptionsDock.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            this.menuStrip1.Items.AddRange(new ToolStripItem[3]
            {
        (ToolStripItem) this.fileToolStripMenuItem,
        (ToolStripItem) this.editToolStripMenuItem,
        (ToolStripItem) this.helpToolStripMenuItem
            });
            this.menuStrip1.Location = new Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new Size(1016, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            this.fileToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[4]
            {
        (ToolStripItem) this.newSectorToolStripMenuItem,
        (ToolStripItem) this.saveSectorToolStripMenuItem,
        (ToolStripItem) this.toolStripSeparator1,
        (ToolStripItem) this.exitToolStripMenuItem
            });
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            this.newSectorToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[3]
            {
        (ToolStripItem) this.systemToolStripMenuItem,
        (ToolStripItem) this.sectorToolStripMenuItem,
        (ToolStripItem) this.sectorObjectToolStripMenuItem
            });
            this.newSectorToolStripMenuItem.Image = (Image)Resources.document_new;
            this.newSectorToolStripMenuItem.Name = "newSectorToolStripMenuItem";
            this.newSectorToolStripMenuItem.Size = new Size(98, 22);
            this.newSectorToolStripMenuItem.Text = "New";
            this.systemToolStripMenuItem.Name = "systemToolStripMenuItem";
            this.systemToolStripMenuItem.Size = new Size(145, 22);
            this.systemToolStripMenuItem.Text = "System";
            this.systemToolStripMenuItem.Click += new EventHandler(this.systemToolStripMenuItem_Click);
            this.sectorToolStripMenuItem.Name = "sectorToolStripMenuItem";
            this.sectorToolStripMenuItem.Size = new Size(145, 22);
            this.sectorToolStripMenuItem.Text = "Sector";
            this.sectorToolStripMenuItem.Click += new EventHandler(this.sectorToolStripMenuItem_Click);
            this.sectorObjectToolStripMenuItem.Name = "sectorObjectToolStripMenuItem";
            this.sectorObjectToolStripMenuItem.Size = new Size(145, 22);
            this.sectorObjectToolStripMenuItem.Text = "Sector Object";
            this.sectorObjectToolStripMenuItem.Click += new EventHandler(this.sectorObjectToolStripMenuItem_Click);
            this.saveSectorToolStripMenuItem.Image = (Image)Resources.media_floppy_smaller;
            this.saveSectorToolStripMenuItem.Name = "saveSectorToolStripMenuItem";
            this.saveSectorToolStripMenuItem.Size = new Size(98, 22);
            this.saveSectorToolStripMenuItem.Text = "Save";
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new Size(95, 6);
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new Size(98, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new EventHandler(this.exitToolStripMenuItem_Click);
            this.editToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[1]
            {
        (ToolStripItem) this.settingsToolStripMenuItem
            });
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new Size(116, 22);
            this.settingsToolStripMenuItem.Text = "Settings";
            this.settingsToolStripMenuItem.Click += new EventHandler(this.settingsToolStripMenuItem_Click);
            this.helpToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[1]
            {
        (ToolStripItem) this.aboutToolStripMenuItem
            });
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new EventHandler(this.aboutToolStripMenuItem_Click);
            this.statusStrip1.Items.AddRange(new ToolStripItem[1]
            {
        (ToolStripItem) this.toolStripStatusLabel1
            });
            this.statusStrip1.Location = new Point(0, 712);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new Size(1016, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            this.toolStripStatusLabel1.BackColor = SystemColors.Control;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new Size(42, 17);
            this.toolStripStatusLabel1.Text = "Ready.";
            this.sandDockManager1.AllowMiddleButtonClosure = false;
            this.sandDockManager1.BorderStyle = TD.SandDock.Rendering.BorderStyle.RaisedThin;
            this.sandDockManager1.DockSystemContainer = (Control)this;
            this.sandDockManager1.EnableEmptyEnvironment = true;
            this.sandDockManager1.OwnerForm = (Form)this;
            this.sandDockManager1.Renderer = (RendererBase)new Office2003Renderer();
            this.sectorDock.AllowClose = false;
            this.sectorDock.BorderStyle = TD.SandDock.Rendering.BorderStyle.RaisedThin;
            this.sectorDock.Controls.Add((Control)this.treeView1);
            dockingRules1.AllowDockBottom = true;
            dockingRules1.AllowDockLeft = true;
            dockingRules1.AllowDockRight = true;
            dockingRules1.AllowDockTop = true;
            dockingRules1.AllowFloat = false;
            dockingRules1.AllowTab = false;
            this.sectorDock.DockingRules = dockingRules1;
            this.sectorDock.Guid = new Guid("692b9042-35f3-4b24-b4ac-253564a3d5b1");
            this.sectorDock.Location = new Point(0, 25);
            this.sectorDock.Name = "sectorDock";
            this.sectorDock.Size = new Size(244, 282);
            this.sectorDock.TabIndex = 0;
            this.sectorDock.Text = "Universe";
            this.treeView1.Dock = DockStyle.Fill;
            this.treeView1.Location = new Point(1, 1);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new Size(242, 280);
            this.treeView1.TabIndex = 0;
            this.treeView1.AfterSelect += new TreeViewEventHandler(this.treeView1_AfterSelect);
            this.PropertiesDock.AllowClose = false;
            this.PropertiesDock.BorderStyle = TD.SandDock.Rendering.BorderStyle.RaisedThin;
            this.PropertiesDock.Guid = new Guid("04d72d9b-c92e-437a-ae5c-625399e065c7");
            this.PropertiesDock.Location = new Point(0, 359);
            this.PropertiesDock.Name = "PropertiesDock";
            this.PropertiesDock.Size = new Size(244, 281);
            this.PropertiesDock.TabIndex = 1;
            this.PropertiesDock.Text = "Properties";
            this.toolboxDock.AllowClose = false;
            this.toolboxDock.BorderStyle = TD.SandDock.Rendering.BorderStyle.RaisedThin;
            this.toolboxDock.Collapsed = true;
            this.toolboxDock.Controls.Add((Control)this.toolStrip2);
            this.toolboxDock.Guid = new Guid("b2270f15-5913-41e1-bbf4-3fa1b89e11d2");
            this.toolboxDock.Location = new Point(4, 25);
            this.toolboxDock.Name = "toolboxDock";
            this.toolboxDock.Size = new Size(114, 447);
            this.toolboxDock.TabIndex = 4;
            this.toolboxDock.Text = "ToolBox";
            this.toolStrip2.GripStyle = ToolStripGripStyle.Hidden;
            this.toolStrip2.Items.AddRange(new ToolStripItem[6]
            {
        (ToolStripItem) this.toolStripButton3,
        (ToolStripItem) this.toolStripButton4,
        (ToolStripItem) this.toolStripSeparator3,
        (ToolStripItem) this.deleteSelected,
        (ToolStripItem) this.toolStripButton1,
        (ToolStripItem) this.toolStripSeparator2
            });
            this.toolStrip2.LayoutStyle = ToolStripLayoutStyle.Table;
            this.toolStrip2.Location = new Point(1, 1);
            this.toolStrip2.Name = "toolStrip2";
            this.toolStrip2.RenderMode = ToolStripRenderMode.System;
            this.toolStrip2.Size = new Size(112, 122);
            this.toolStrip2.TabIndex = 0;
            this.toolStrip2.Text = "toolStrip2";
            this.toolStripButton3.Image = (Image)Resources.wb_arrow;
            this.toolStripButton3.ImageTransparentColor = Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new Size(57, 20);
            this.toolStripButton3.Text = "Move";
            this.toolStripButton3.Click += new EventHandler(this.toolStripButton3_Click);
            this.toolStripButton4.Image = (Image)Resources.view_fullscreen;
            this.toolStripButton4.ImageTransparentColor = Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new Size(47, 20);
            this.toolStripButton4.Text = "Pan";
            this.toolStripButton4.Click += new EventHandler(this.toolStripButton4_Click);
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new Size(6, 23);
            this.deleteSelected.Image = (Image)Resources.process_stop;
            this.deleteSelected.ImageTransparentColor = Color.Magenta;
            this.deleteSelected.Name = "deleteSelected";
            this.deleteSelected.Size = new Size(107, 20);
            this.deleteSelected.Text = "Delete Selected";
            this.deleteSelected.Click += new EventHandler(this.deleteSelected_Click);
            this.toolStripButton1.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.ImageTransparentColor = Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new Size(23, 4);
            this.toolStripButton1.Text = "toolStripButton1";
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new Size(6, 23);
            this.dockContainer2.ContentSize = 244;
            this.dockContainer2.Controls.Add((Control)this.sectorDock);
            this.dockContainer2.Controls.Add((Control)this.PropertiesDock);
            this.dockContainer2.Dock = DockStyle.Left;
            this.dockContainer2.LayoutSystem = new SplitLayoutSystem(new SizeF(250f, 400f), Orientation.Horizontal, new LayoutSystemBase[2]
            {
        (LayoutSystemBase) new ControlLayoutSystem(new SizeF(250f, 400f), new DockControl[1]
        {
          (DockControl) this.sectorDock
        }, (DockControl) this.sectorDock),
        (LayoutSystemBase) new ControlLayoutSystem(new SizeF(250f, 400f), new DockControl[1]
        {
          (DockControl) this.PropertiesDock
        }, (DockControl) this.PropertiesDock)
            });
            this.dockContainer2.Location = new Point(0, 49);
            this.dockContainer2.Manager = this.sandDockManager1;
            this.dockContainer2.Name = "dockContainer2";
            this.dockContainer2.Size = new Size(248, 663);
            this.dockContainer2.TabIndex = 4;
            this.dockContainer1.ContentSize = 114;
            this.dockContainer1.Controls.Add((Control)this.toolboxDock);
            this.dockContainer1.Dock = DockStyle.Right;
            this.dockContainer1.LayoutSystem = new SplitLayoutSystem(new SizeF(250f, 400f), Orientation.Horizontal, new LayoutSystemBase[1]
            {
        (LayoutSystemBase) new ControlLayoutSystem(new SizeF(250f, 400f), new DockControl[1]
        {
          (DockControl) this.toolboxDock
        }, (DockControl) this.toolboxDock)
            });
            this.dockContainer1.Location = new Point(898, 49);
            this.dockContainer1.Manager = this.sandDockManager1;
            this.dockContainer1.Name = "dockContainer1";
            this.dockContainer1.Size = new Size(118, 495);
            this.dockContainer1.TabIndex = 7;
            this.newToolbarClick.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.newToolbarClick.Image = (Image)Resources.document_new;
            this.newToolbarClick.ImageTransparentColor = Color.Magenta;
            this.newToolbarClick.Name = "newToolbarClick";
            this.newToolbarClick.Size = new Size(23, 22);
            this.newToolbarClick.Text = "New";
            this.newToolbarClick.Click += new EventHandler(this.newToolbarClick_Click);
            this.save.DisplayStyle = ToolStripItemDisplayStyle.Image;
            this.save.Image = (Image)Resources.media_floppy_smaller;
            this.save.ImageTransparentColor = Color.Magenta;
            this.save.Name = "save";
            this.save.Size = new Size(23, 22);
            this.save.Text = "Save";
            this.save.Click += new EventHandler(this.save_Click);
            this.toolStrip1.Items.AddRange(new ToolStripItem[2]
            {
        (ToolStripItem) this.newToolbarClick,
        (ToolStripItem) this.save
            });
            this.toolStrip1.Location = new Point(0, 24);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = ToolStripRenderMode.Professional;
            this.toolStrip1.Size = new Size(1016, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            this.SectorTab.AllowClose = false;
            this.SectorTab.AllowCollapse = false;
            this.SectorTab.Controls.Add((Control)this.pCanvas1);
            this.SectorTab.FloatingSize = new Size(550, 400);
            this.SectorTab.Guid = new Guid("3cdd5ebc-6aa5-43ab-a7a2-a26142602396");
            this.SectorTab.Location = new Point(5, 33);
            this.SectorTab.Name = "SectorTab";
            this.SectorTab.Size = new Size(640, 457);
            this.SectorTab.TabIndex = 0;
            this.SectorTab.Text = "SectorName";
            this.pCanvas1.AllowDrop = true;
            this.pCanvas1.BackColor = Color.Black;
            this.pCanvas1.Dock = DockStyle.Fill;
            this.pCanvas1.GridFitText = false;
            this.pCanvas1.Location = new Point(0, 0);
            this.pCanvas1.Name = "pCanvas1";
            this.pCanvas1.RegionManagement = true;
            this.pCanvas1.Size = new Size(640, 457);
            this.pCanvas1.TabIndex = 0;
            this.pCanvas1.Text = "pCanvas1";
            this.documentContainer1.ContentSize = 200;
            this.documentContainer1.Controls.Add((Control)this.SectorTab);
            this.documentContainer1.LayoutSystem = new SplitLayoutSystem(new SizeF(250f, 400f), Orientation.Horizontal, new LayoutSystemBase[1]
            {
        (LayoutSystemBase) new DocumentLayoutSystem(new SizeF(550f, 400f), new DockControl[1]
        {
          (DockControl) this.SectorTab
        }, (DockControl) this.SectorTab)
            });
            this.documentContainer1.Location = new Point(248, 49);
            this.documentContainer1.Manager = this.sandDockManager1;
            this.documentContainer1.Name = "documentContainer1";
            this.documentContainer1.Size = new Size(650, 495);
            this.documentContainer1.TabIndex = 8;
            this.dockContainer3.ContentSize = 164;
            this.dockContainer3.Controls.Add((Control)this.sectorOptionsDock);
            this.dockContainer3.Dock = DockStyle.Bottom;
            this.dockContainer3.LayoutSystem = new SplitLayoutSystem(new SizeF(250f, 400f), Orientation.Vertical, new LayoutSystemBase[1]
            {
        (LayoutSystemBase) new ControlLayoutSystem(new SizeF(250f, 400f), new DockControl[1]
        {
          (DockControl) this.sectorOptionsDock
        }, (DockControl) this.sectorOptionsDock)
            });
            this.dockContainer3.Location = new Point(248, 544);
            this.dockContainer3.Manager = (SandDockManager)null;
            this.dockContainer3.Name = "dockContainer3";
            this.dockContainer3.Size = new Size(768, 168);
            this.dockContainer3.TabIndex = 5;
            this.sectorOptionsDock.AllowClose = false;
            this.sectorOptionsDock.AutoScroll = true;
            this.sectorOptionsDock.Controls.Add((Control)this.tabControl1);
            dockingRules2.AllowDockBottom = true;
            dockingRules2.AllowDockLeft = true;
            dockingRules2.AllowDockRight = true;
            dockingRules2.AllowDockTop = true;
            dockingRules2.AllowFloat = true;
            dockingRules2.AllowTab = true;
            this.sectorOptionsDock.DockingRules = dockingRules2;
            this.sectorOptionsDock.Guid = new Guid("4b4734ea-f7f1-4bc0-9469-5a8dc25849d9");
            this.sectorOptionsDock.Location = new Point(0, 22);
            this.sectorOptionsDock.Name = "sectorOptionsDock";
            this.sectorOptionsDock.Size = new Size(768, 122);
            this.sectorOptionsDock.TabIndex = 5;
            this.sectorOptionsDock.Text = "Options";
            this.sectorOptionsDock.Closing += new DockControlClosingEventHandler(this.sectorOptionsDock_Closing);
            this.tabControl1.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
            this.tabControl1.Controls.Add((Control)this.sectorVisOptions);
            this.tabControl1.Controls.Add((Control)this.sectorTblList);
            this.tabControl1.Location = new Point(2, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new Size(763, 120);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.Resize += new EventHandler(this.tabControl1_Resize);
            this.sectorVisOptions.Location = new Point(4, 22);
            this.sectorVisOptions.Name = "sectorVisOptions";
            this.sectorVisOptions.Size = new Size(755, 94);
            this.sectorVisOptions.TabIndex = 0;
            this.sectorVisOptions.Text = "Sector Visualization Options";
            this.sectorVisOptions.UseVisualStyleBackColor = true;
            this.sectorTblList.Location = new Point(4, 22);
            this.sectorTblList.Name = "sectorTblList";
            this.sectorTblList.Size = new Size(755, 94);
            this.sectorTblList.TabIndex = 1;
            this.sectorTblList.Text = "Sector Table List";
            this.sectorTblList.UseVisualStyleBackColor = true;
            this.AutoScaleDimensions = new SizeF(6f, 13f);
            this.AutoScaleMode = AutoScaleMode.Font;
            this.BackColor = SystemColors.AppWorkspace;
            this.ClientSize = new Size(1016, 734);
            this.Controls.Add((Control)this.documentContainer1);
            this.Controls.Add((Control)this.dockContainer1);
            this.Controls.Add((Control)this.dockContainer3);
            this.Controls.Add((Control)this.dockContainer2);
            this.Controls.Add((Control)this.statusStrip1);
            this.Controls.Add((Control)this.toolStrip1);
            this.Controls.Add((Control)this.menuStrip1);
            this.Icon = (Icon)componentResourceManager.GetObject("$this.Icon");
            this.MainMenuStrip = this.menuStrip1;
            this.Name = nameof(mainFrm);
            this.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "N7 Sector Editor";
            this.WindowState = FormWindowState.Maximized;
            this.Load += new EventHandler(this.mainFrm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.sectorDock.ResumeLayout(false);
            this.toolboxDock.ResumeLayout(false);
            this.toolboxDock.PerformLayout();
            this.toolStrip2.ResumeLayout(false);
            this.toolStrip2.PerformLayout();
            this.dockContainer2.ResumeLayout(false);
            this.dockContainer1.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.SectorTab.ResumeLayout(false);
            this.documentContainer1.ResumeLayout(false);
            this.dockContainer3.ResumeLayout(false);
            this.sectorOptionsDock.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        public mainFrm()
        {
            this.InitializeComponent();
        }

        private void mainFrm_Load(object sender, EventArgs e)
        {
            this.pCanvas1.BackColor = Color.Black;
            mainFrm.factions = new FactionSql();
            mainFrm.systems = new SystemsSql();
            mainFrm.sectors = new SectorsSql();
            mainFrm.baseAssets = new BaseAssetSQL();
            mainFrm.mobs = new MobsSQL();
            this.objList = new ObjectList();
            this.treeView1.Nodes.AddRange(new TreeWindow(mainFrm.systems.getSystemTable(), mainFrm.sectors.getSectorTable()).setupInitialTree());
            HE_GlobalVars._ListofFactions = new string[mainFrm.factions.getFactionTable().Rows.Count + 1];
            int index = 1;
            HE_GlobalVars._ListofFactions[0] = "None";
            foreach (DataRow row in (InternalDataCollectionBase)mainFrm.factions.getFactionTable().Rows)
            {
                HE_GlobalVars._ListofFactions[index] = row["name"].ToString();
                ++index;
            }
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            this.checkIfChanged();
            this.SectorTab.Text = e.Node.Text;
            string str1 = e.Node.Text.ToString();
            this.pCanvas1.Dispose();
            this.pCanvas1 = new PCanvas();
            this.SectorTab.Controls.Add((Control)this.pCanvas1);
            this.pCanvas1.AllowDrop = true;
            this.pCanvas1.BackColor = Color.White;
            this.pCanvas1.Dock = DockStyle.Fill;
            this.pCanvas1.GridFitText = false;
            this.pCanvas1.Location = new Point(0, 0);
            this.pCanvas1.Name = "pCanvas1";
            this.pCanvas1.RegionManagement = true;
            this.pCanvas1.Size = new Size(554, 438);
            this.pCanvas1.TabIndex = 0;
            this.pCanvas1.Text = "pCanvas1";
            if (e.Node.Level == 0)
            {
                if (e.Node.Nodes.Count > 0)
                {
                    DataRow[] rowsByName = mainFrm.systems.findRowsByName(str1);
                    string systemID = rowsByName[0]["system_id"].ToString();
                    this.selectedSystemRow = rowsByName[0];
                    SystemProps systemProps = new SystemProps();
                    systemProps.Name = str1;
                    Color color = Color.FromArgb(int.Parse(rowsByName[0]["color_r"].ToString()), int.Parse(rowsByName[0]["color_g"].ToString()), int.Parse(rowsByName[0]["color_b"].ToString()));
                    systemProps.Color = color;
                    systemProps.GalaxyX = float.Parse(rowsByName[0]["galaxy_x"].ToString());
                    systemProps.GalaxyY = float.Parse(rowsByName[0]["galaxy_y"].ToString());
                    systemProps.GalaxyZ = float.Parse(rowsByName[0]["galaxy_z"].ToString());
                    systemProps.Notes = rowsByName[0]["notes"].ToString();
                    DataRow[] rowsBySystemId = mainFrm.sectors.getRowsBySystemID(systemID);
                    this.createPropertyGrid();
                    this.propertyGrid1.SelectedObject = (object)systemProps;
                    mainFrm.systemWindow = new SystemWindow(this.pCanvas1, str1, rowsBySystemId, this.propertyGrid1, this.selectedSystemRow);
                    this.propertyGrid1.PropertyValueChanged += new PropertyValueChangedEventHandler(mainFrm.systemWindow.pg_PropertyValueChanged);
                }
                else
                {
                    int num1 = (int)MessageBox.Show("Sorry, There are no sector's to display in the selected system! \n Please add a new sector to it, or select another system.");
                }
            }
            else
            {
                if (e.Node.Level != 1)
                    return;
                DataRow[] rowsByName = mainFrm.sectors.findRowsByName(str1);
                mainFrm.sectorID = int.Parse(rowsByName[0]["sector_id"].ToString());
                this.createPropertyGrid();
                mainFrm.sectorWindow = new SectorWindow(this.pCanvas1, rowsByName, this.propertyGrid1, this.objList.dataGridView1);
                this.createOptions(mainFrm.sectorWindow);
                mainFrm.sectorObjects = mainFrm.sectorWindow.getSectorObjectsSQL();
                this.propertyGrid1.PropertyValueChanged += new PropertyValueChangedEventHandler(mainFrm.sectorWindow.PropertyGrid1_PropertyValueChanged);
                this.objList.dataGridView1.Rows.Clear();
                DataTable sectorObject = mainFrm.sectorObjects.getSectorObject();
                object[] objArray = new object[4];
                foreach (DataRow row in (InternalDataCollectionBase)sectorObject.Rows)
                {
                    int num2 = int.Parse(row["sector_object_id"].ToString());
                    string str2 = row["name"].ToString();
                    int num3 = int.Parse(row["base_asset_id"].ToString());
                    int num4 = int.Parse(row["type"].ToString());
                    objArray[0] = (object)num2;
                    objArray[1] = (object)str2;
                    objArray[2] = (object)num3;
                    objArray[3] = (object)num4;
                    this.objList.dataGridView1.Rows.Add(objArray);
                }
            }
        }

        private void createPropertyGrid()
        {
            if (this.propertyGrid1 != null)
                this.propertyGrid1.Dispose();
            this.propertyGrid1 = new PropertyGrid();
            this.propertyGrid1.Dock = DockStyle.Fill;
            this.propertyGrid1.Location = new Point(1, 1);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.Size = new Size(242, 279);
            this.propertyGrid1.TabIndex = 0;
            this.PropertiesDock.Controls.Add((Control)this.propertyGrid1);
        }

        private void createOptions(SectorWindow sectorWindow)
        {
            if (N7.Properties.Settings.Default.KeepVisualizationSetting)
            {
                if (this.optionsGui1 == null)
                {
                    this.optionsGui1 = new OptionsGui(sectorWindow);
                    this.optionsGui1.AutoScroll = true;
                    this.optionsGui1.BackColor = Color.Transparent;
                    this.optionsGui1.Dock = DockStyle.Fill;
                    this.optionsGui1.Location = new Point(0, 0);
                    this.optionsGui1.Name = "optionsGui1";
                    this.optionsGui1.Size = new Size(768, 97);
                    this.optionsGui1.TabIndex = 0;
                    this.objList.Size = new Size(this.tabControl1.Size.Width - 7, this.tabControl1.Size.Height - 25);
                    this.sectorVisOptions.Controls.Add((Control)this.optionsGui1);
                    this.sectorTblList.Controls.Add((Control)this.objList);
                }
                else
                    this.optionsGui1.setSectorWindow(sectorWindow);
            }
            else
            {
                if (this.optionsGui1 != null)
                    this.optionsGui1.Dispose();
                this.optionsGui1 = new OptionsGui(sectorWindow);
                this.optionsGui1.AutoScroll = true;
                this.optionsGui1.BackColor = Color.Transparent;
                this.optionsGui1.Dock = DockStyle.Fill;
                this.optionsGui1.Location = new Point(0, 0);
                this.optionsGui1.Name = "optionsGui1";
                this.optionsGui1.Size = new Size(768, 97);
                this.optionsGui1.TabIndex = 0;
                this.objList.Size = new Size(this.tabControl1.Size.Width - 7, this.tabControl1.Size.Height - 25);
                this.sectorVisOptions.Controls.Add((Control)this.optionsGui1);
                this.sectorTblList.Controls.Add((Control)this.objList);
            }
            this.optionsGui1.loadAll();
        }

        private void checkIfChanged()
        {
            DataTable sectorTable = mainFrm.sectors.getSectorTable();
            DataTable systemTable = mainFrm.systems.getSystemTable();
            bool flag = false;
            foreach (DataRow row in (InternalDataCollectionBase)sectorTable.Rows)
            {
                if (row.RowState != DataRowState.Unchanged)
                    flag = true;
            }
            foreach (DataRow row in (InternalDataCollectionBase)systemTable.Rows)
            {
                if (row.RowState != DataRowState.Unchanged)
                    flag = true;
            }
            if (mainFrm.sectorWindow != null)
            {
                foreach (DataRow row in (InternalDataCollectionBase)mainFrm.sectorWindow.getSectorObjectsSQL().getSectorObject().Rows)
                {
                    if (row.RowState == DataRowState.Unchanged)
                        flag = true;
                }
            }
            if (!flag)
                return;
            switch (MessageBox.Show("All your changes will be lost, Do want to save your changes before switching?", "Loss of Information Warning", MessageBoxButtons.YesNo))
            {
                case DialogResult.Yes:
                    this.save_Click((object)null, (EventArgs)null);
                    break;
            }
        }

        private void ResetTreeview()
        {
            treeView1.Nodes.Clear();
            this.treeView1.Nodes.AddRange(new TreeWindow(mainFrm.systems.getSystemTable(), mainFrm.sectors.getSectorTable()).setupInitialTree());
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DataTable sectorTable = mainFrm.sectors.getSectorTable();
            DataTable systemTable = mainFrm.systems.getSystemTable();
            bool flag = false;
            foreach (DataRow row in (InternalDataCollectionBase)sectorTable.Rows)
            {
                if (row.RowState != DataRowState.Unchanged)
                    flag = true;
            }
            foreach (DataRow row in (InternalDataCollectionBase)systemTable.Rows)
            {
                if (row.RowState != DataRowState.Unchanged)
                    flag = true;
            }
            if (mainFrm.sectorWindow != null)
            {
                foreach (DataRow row in (InternalDataCollectionBase)mainFrm.sectorWindow.getSectorObjectsSQL().getSectorObject().Rows)
                {
                    if (row.RowState == DataRowState.Unchanged)
                        flag = true;
                }
            }
            if (flag)
            {
                switch (MessageBox.Show("All your changes will be lost, Do want to save your changes before exiting?", "Loss of Information Warning", MessageBoxButtons.YesNo))
                {
                    case DialogResult.Yes:
                        this.save_Click(sender, e);
                        break;
                }
            }
            Application.Exit();
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            if (this.pan != null)
                this.pCanvas1.RemoveInputEventListener((PInputEventListener)this.pan);
            this.pCanvas1.PanEventHandler = (PPanEventHandler)null;
            this.drag = new PDragEventHandler();
            this.pCanvas1.AddInputEventListener((PInputEventListener)this.drag);
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            if (this.drag != null)
                this.pCanvas1.RemoveInputEventListener((PInputEventListener)this.drag);
            this.pan = new PPanEventHandler();
            this.pCanvas1.AddInputEventListener((PInputEventListener)this.pan);
        }

        private void save_Click(object sender, EventArgs e)
        {
            DataTable systemTable = mainFrm.systems.getSystemTable();
            DataTable sectorTable = mainFrm.sectors.getSectorTable();
            foreach (DataRow row in (InternalDataCollectionBase)systemTable.Rows)
            {
                if (row.RowState == DataRowState.Modified)
                {
                    row.AcceptChanges();
                    mainFrm.systems.updateRow(row);
                    ResetTreeview();
                }
            }
            foreach (DataRow row in (InternalDataCollectionBase)sectorTable.Rows)
            {
                if (row.RowState == DataRowState.Modified)
                {
                    row.AcceptChanges();
                    mainFrm.sectors.updateRow(row);
                    ResetTreeview();
                }
            }
            if (mainFrm.sectorWindow != null)
            {
                foreach (DataRow row in (InternalDataCollectionBase)mainFrm.sectorWindow.getSectorObjectsSQL().getSectorObject().Rows)
                {
                    if (row.RowState == DataRowState.Modified)
                    {
                        row.AcceptChanges();
                        mainFrm.sectorWindow.getSectorObjectsSQL().updateRow(row);
                        ResetTreeview();
                    }
                }
            }
            if (mainFrm.sectorWindow == null || mainFrm.sectorWindow.getDeletedObjectsID().Count <= 0)
                return;
            for (int index = 0; index < mainFrm.sectorWindow.getDeletedObjectsID().Count; ++index)
            {
                int id = (int)mainFrm.sectorWindow.getDeletedObjectsID()[(object)index];
                int type = (int)mainFrm.sectorWindow.getDeletedObjectsType()[(object)index];
                mainFrm.sectorWindow.getSectorObjectsSQL().deleteRow(id, type);
                ResetTreeview();
            }
            mainFrm.sectorWindow.clearDeletedHashTables();
            ResetTreeview();
        }

        private void deleteSelected_Click(object sender, EventArgs e)
        {
            if (mainFrm.sectorWindow != null)
            {
                mainFrm.sectorWindow.deleteSelectedObject();
            }
            else
            {
                int num = (int)MessageBox.Show("Before Deleting an Object a Sector Must be openend \n and and object Selected. Please Try again.");
            }
        }

        private void systemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int num = (int)new NewSystem(this.treeView1).ShowDialog();
        }

        private void sectorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.treeView1.SelectedNode == null)
            {
                int num1 = (int)MessageBox.Show("You Cannot add a Sector without first \n having a system selected. Please select a system and try again!");
            }
            else if (this.treeView1.SelectedNode.Level == 0)
            {
                int num2 = (int)new NewSector(this.treeView1).ShowDialog();
            }
            else
            {
                int num3 = (int)MessageBox.Show("You Cannot add a Sector without first \n having a system selected. Please select a system and try again!");
            }
        }

        private void sectorObjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int num = (int)new NewSectorObjectType(this.treeView1, mainFrm.sectorWindow).ShowDialog();
        }

        private void newToolbarClick_Click(object sender, EventArgs e)
        {
            int num = (int)new NewFrm(this.treeView1, mainFrm.sectorWindow).ShowDialog();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int num = (int)new AboutBox2().ShowDialog();
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new N7.GUI.Settings().Show((IWin32Window)this);
        }

        private void datagridview1_rowEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (!this.firstRun)
            {
                int _id = int.Parse(this.objList.dataGridView1.Rows[e.RowIndex].Cells["id"].Value.ToString());
                mainFrm.sectorWindow.setSelected(_id);
            }
            this.firstRun = false;
        }

        private void sectorOptionsDock_Closing(object sender, DockControlClosingEventArgs e)
        {
        }

        private void tabControl1_Resize(object sender, EventArgs e)
        {
            if (this.objList == null || this.objList.IsDisposed)
                return;
            this.objList.Size = new Size(this.tabControl1.Size.Width - 7, this.tabControl1.Size.Height - 25);
        }
    }
}
