﻿// Decompiled with JetBrains decompiler
// Type: N7.Sprites.StargateSprite
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using N7.Props;
using N7.Utilities;
using System;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Windows.Forms;
using UMD.HCIL.Piccolo;
using UMD.HCIL.Piccolo.Event;
using UMD.HCIL.Piccolo.Nodes;

namespace N7.Sprites
{
  internal class StargateSprite
  {
    private StargateProps dp;
    private PropertyGrid _pg;
    private bool appearsInRadar;
    private PText pname;
    private DataRow dr;
    private PLayer _layer;
    private PImage stargateImage;
    private bool dragging;
    private DataGridView _dgv;

    public StargateSprite(PLayer layer, DataRow r, PropertyGrid pg, DataGridView dgv)
    {
      this._pg = pg;
      this.setupData(r);
      this.dr = r;
      this._layer = layer;
      this._dgv = dgv;
      string aText = r["name"].ToString();
      float num1 = float.Parse(r["position_x"].ToString());
      float num2 = -float.Parse(r["position_y"].ToString());
      float num3 = float.Parse(r["signature"].ToString());
      float num4 = float.Parse(r["radar_range"].ToString());
      float num5 = float.Parse(r["exploration_range"].ToString());
      this.appearsInRadar = (bool) r["appears_in_radar"];
      int num6 = int.Parse(r["nav_type"].ToString());
      bool flag = (bool) r["classSpecific"];
      int num7 = int.Parse(r["faction_id"].ToString());
      float num8 = (float) ((double) num3 * 2.0 / 100.0);
      float num9 = (float) ((double) num4 * 2.0 / 100.0);
      float num10 = (float) ((double) num5 * 2.0 / 100.0);
      if ((double) num8 == 0.0)
        num8 = 5f;
      if ((double) num9 == 0.0)
        num9 = 5f;
      if ((double) num10 == 0.0)
        num10 = 5f;
      string path1_1 = "Images";
      string path1_2 = Path.Combine("..", "..");
      string path2 = !this.appearsInRadar ? "hiddenGate.gif" : "standardGate.gif";
      if (flag)
        path2 = "classSpecificGate.gif";
      if (num7 != -1)
        path2 = "FactionSpecificGate.gif";
      if (File.Exists(Path.Combine(path1_1, path2)))
        path1_2 = "";
      Image newImage = Image.FromFile(Path.Combine(path1_2, Path.Combine(path1_1, path2)));
      this.stargateImage = new PImage(newImage);
      this.stargateImage.X = (float) (((double) num1 - (double) (newImage.Width / 2)) / 100.0);
      this.stargateImage.Y = (float) (((double) num2 - (double) (newImage.Height / 2)) / 100.0);
      float x1 = (float) ((double) num1 / 100.0 - ((double) num8 / 2.0 - (double) (newImage.Width / 2)));
      float y1 = (float) ((double) num2 / 100.0 - ((double) num8 / 2.0 - (double) (newImage.Height / 2)));
      float x2 = (float) ((double) num1 / 100.0 - ((double) num9 / 2.0 - (double) (newImage.Width / 2)));
      float y2 = (float) ((double) num2 / 100.0 - ((double) num9 / 2.0 - (double) (newImage.Height / 2)));
      float x3 = (float) ((double) num1 / 100.0 - ((double) num10 / 2.0 - (double) (newImage.Width / 2)));
      float y3 = (float) ((double) num2 / 100.0 - ((double) num10 / 2.0 - (double) (newImage.Height / 2)));
      Pen pen1 = new Pen(Color.ForestGreen, 3f);
      Pen pen2 = new Pen(Color.GreenYellow, 2f);
      pen2.DashStyle = DashStyle.Dash;
      Pen pen3 = new Pen(Color.LightGreen, 1f);
      pen3.DashStyle = DashStyle.DashDotDot;
      PPath ellipse1 = PPath.CreateEllipse(x1, y1, num8, num8);
      ellipse1.Pen = pen1;
      PPath ellipse2 = PPath.CreateEllipse(x2, y2, num9, num9);
      ellipse2.Pen = pen2;
      PPath ellipse3 = PPath.CreateEllipse(x3, y3, num10, num10);
      ellipse3.Pen = pen3;
      PNode child1 = (PNode) ellipse1;
      child1.Brush = Brushes.Transparent;
      PNode child2 = (PNode) ellipse2;
      child2.Brush = Brushes.Transparent;
      PNode child3 = (PNode) ellipse3;
      child3.Brush = Brushes.Transparent;
      this.pname = new PText(aText);
      this.pname.TextBrush = Brushes.White;
      this.pname.TextAlignment = StringAlignment.Center;
      this.pname.X = (float) ((double) num1 / 100.0 - (double) this.pname.Width / 2.0);
      this.pname.Y = (float) ((double) num2 / 100.0 - 20.0);
      this.stargateImage.AddChild(child1);
      this.stargateImage.AddChild(child2);
      this.stargateImage.AddChild(child3);
      this.stargateImage.AddChild((PNode) this.pname);
      for (int index = 0; index < num6; ++index)
        this.stargateImage.AddChild(new PNode());
      this.stargateImage.ChildrenPickable = false;
      this.stargateImage.Tag = (object) this;
      this.stargateImage.MouseDown += new PInputEventHandler(this.Image_MouseDown);
      this.stargateImage.MouseUp += new PInputEventHandler(this.Image_MouseUp);
      this.stargateImage.MouseDrag += new PInputEventHandler(this.Image_MouseDrag);
      layer.AddChild((PNode) this.stargateImage);
    }

    private void setupData(DataRow r)
    {
      int num = int.Parse(r["type"].ToString());
      string str = "";
      switch (num)
      {
        case 0:
          str = "Mobs";
          break;
        case 3:
          str = "Planets";
          break;
        case 11:
          str = "Stargates";
          break;
        case 12:
          str = "Starbases";
          break;
        case 37:
          str = "Decorations";
          break;
        case 38:
          str = "Harvestables";
          break;
      }
      this.dp = new StargateProps();
      this.dp.SectorID = int.Parse(r["sector_id"].ToString());
      this.dp.NavType = r["nav_type"].ToString();
      this.dp.Signature = float.Parse(r["signature"].ToString());
      this.dp.IsHuge = (bool) r["is_huge"];
      this.dp.BaseXP = int.Parse(r["base_xp"].ToString());
      this.dp.ExplorationRange = float.Parse(r["exploration_range"].ToString());
      this.dp.BaseAssetID = int.Parse(r["base_asset_id"].ToString());
      this.dp.Color = AdobeColors.HSL_to_RGB(new AdobeColors.HSL()
      {
        H = (double) float.Parse(r["h"].ToString()),
        S = (double) float.Parse(r["s"].ToString()),
        L = (double) float.Parse(r["v"].ToString())
      });
      this.dp.Type = str;
      this.dp.Scale = float.Parse(r["scale"].ToString());
      this.dp.PositionX = float.Parse(r["position_x"].ToString());
      this.dp.PositionY = float.Parse(r["position_y"].ToString());
      this.dp.PositionZ = float.Parse(r["position_z"].ToString());
      double[] angle = new QuaternionCalc().QuatToAngle(new double[4]
      {
        double.Parse(r["orientation_z"].ToString()),
        double.Parse(r["orientation_u"].ToString()),
        double.Parse(r["orientation_v"].ToString()),
        double.Parse(r["orientation_w"].ToString())
      });
      if (angle[0] == double.NaN)
        angle[0] = 0.0;
      if (angle[1] == double.NaN)
        angle[1] = 0.0;
      if (angle[2] == double.NaN)
        angle[2] = 0.0;
      this.dp.Orientation_Yaw = Math.Round(angle[0], 0);
      this.dp.Orientation_Pitch = Math.Round(angle[1], 0);
      this.dp.Orientation_Roll = Math.Round(angle[2], 0);
      this.dp.Name = r["name"].ToString();
      this.dp.AppearsInRadar = (bool) r["appears_in_radar"];
      this.dp.RadarRange = float.Parse(r["radar_range"].ToString());
      this.dp.Destination = int.Parse(r["gate_to"].ToString());
      this.dp.SoundEffect = int.Parse(r["sound_effect_id"].ToString());
      this.dp.SoundEffectRange = float.Parse(r["sound_effect_range"].ToString());
      this.dp.IsClassSpecific = (bool) r["classSpecific"];
      this.dp.MinSecurityLevel = HE_GlobalVars._ListofSecuritys[(int) ((double) int.Parse(r["minSecurityLevel"].ToString()) * 0.1)];
      this.dp.FactionID = mainFrm.factions.findNameByID(int.Parse(r["faction_id"].ToString()));
    }

    protected void Image_MouseDown(object sender, PInputEventArgs e)
    {
      this._pg.SelectedObject = (object) this.dp;
    }

    protected void Image_MouseDrag(object sender, PInputEventArgs e)
    {
      this.dragging = true;
    }

    protected void Image_MouseUp(object sender, PInputEventArgs e)
    {
      if (this.dragging)
      {
        this.dp.PositionX = e.Position.X * 100f;
        this.dp.PositionY = (float) -((double) e.Position.Y * 100.0);
        this.dr["position_x"] = (object) (float) ((double) e.Position.X * 100.0);
        this.dr["position_y"] = (object) (float) -((double) e.Position.Y * 100.0);
        this.dragging = false;
      }
      this._pg.SelectedObject = (object) this.dp;
      mainFrm.selectedObjectID = int.Parse(this.dr["sector_object_id"].ToString());
    }

    public bool getAppearsInRader()
    {
      return this.appearsInRadar;
    }

    public PText getText()
    {
      return this.pname;
    }

    public void updateChangedInfo(string propertyName, object _changedValue)
    {
      string str = _changedValue.ToString();
      if (propertyName == "SectorID")
        this.dr["sector_id"] = (object) int.Parse(str);
      else if (propertyName == "NavType")
        this.dr["nav_type"] = (object) str;
      else if (propertyName == "Signature")
      {
        this.dr["signature"] = (object) float.Parse(str);
        float width = this.stargateImage.Width;
        float height = this.stargateImage.Height;
        float x = this.stargateImage.X;
        float y = this.stargateImage.Y;
        this.stargateImage.GetChild(0).X = (float) ((double) x + (double) width / 2.0 - (double) float.Parse(str) / 100.0);
        this.stargateImage.GetChild(0).Y = (float) ((double) y + (double) height / 2.0 - (double) float.Parse(str) / 100.0);
        this.stargateImage.GetChild(0).Width = (float) ((double) float.Parse(str) * 2.0 / 100.0);
        this.stargateImage.GetChild(0).Height = (float) ((double) float.Parse(str) * 2.0 / 100.0);
      }
      else if (propertyName == "IsHuge")
        this.dr["is_huge"] = (object) bool.Parse(str);
      else if (propertyName == "BaseXP")
        this.dr["base_xp"] = (object) int.Parse(str);
      else if (propertyName == "ExplorationRange")
      {
        this.dr["exploration_range"] = (object) float.Parse(str);
        float width = this.stargateImage.Width;
        float height = this.stargateImage.Height;
        float x = this.stargateImage.X;
        float y = this.stargateImage.Y;
        this.stargateImage.GetChild(2).X = (float) ((double) x + (double) width / 2.0 - (double) float.Parse(str) / 100.0);
        this.stargateImage.GetChild(2).Y = (float) ((double) y + (double) height / 2.0 - (double) float.Parse(str) / 100.0);
        this.stargateImage.GetChild(2).Width = (float) ((double) float.Parse(str) * 2.0 / 100.0);
        this.stargateImage.GetChild(2).Height = (float) ((double) float.Parse(str) * 2.0 / 100.0);
      }
      else if (propertyName == "BaseAssetID")
      {
        this.dr["base_asset_id"] = (object) int.Parse(str);
        foreach (DataGridViewBand selectedRow in (BaseCollection) this._dgv.SelectedRows)
        {
          this._dgv.Rows[selectedRow.Index].Cells["base_asset_id"].Value = (object) int.Parse(str);
          this._dgv.Update();
          this._dgv.Refresh();
        }
      }
      else if (propertyName == "Color")
      {
        AdobeColors.HSL hsl = AdobeColors.RGB_to_HSL((Color) _changedValue);
        this.dr["h"] = (object) hsl.H;
        this.dr["s"] = (object) hsl.S;
        this.dr["v"] = (object) hsl.L;
      }
      else if (propertyName == "Type")
      {
        this._layer.RemoveChild((PNode) this.stargateImage);
        this._pg.SelectedObject = (object) null;
      }
      else if (propertyName == "Scale")
        this.dr["scale"] = (object) float.Parse(str);
      else if (propertyName == "PositionX")
      {
        this.dr["position_x"] = (object) float.Parse(str);
        this.stargateImage.TranslateBy(float.Parse(str) / 100f - this.stargateImage.X, 0.0f);
      }
      else if (propertyName == "PositionY")
      {
        this.dr["position_y"] = (object) float.Parse(str);
        this.stargateImage.TranslateBy(0.0f, float.Parse(str) / 100f - this.stargateImage.Y);
      }
      else if (propertyName == "PositionZ")
        this.dr["position_z"] = (object) float.Parse(str);
      else if (propertyName == "Orientation_Yaw" || propertyName == "Orientation_Pitch" || propertyName == "Orientation_Roll")
      {
        double[] quat = new QuaternionCalc().AngleToQuat(this.dp.Orientation_Yaw, this.dp.Orientation_Pitch, this.dp.Orientation_Roll);
        this.dr["orientation_z"] = (object) quat[0];
        this.dr["orientation_u"] = (object) quat[1];
        this.dr["orientation_v"] = (object) quat[2];
        this.dr["orientation_w"] = (object) quat[3];
      }
      else if (propertyName == "Name")
      {
        this.dr["name"] = (object) str;
        float x = this.stargateImage.X;
        float y = this.stargateImage.Y;
        PText child = (PText) this.stargateImage.GetChild(3);
        child.Text = str;
        child.TextAlignment = StringAlignment.Center;
        child.X = x - child.Width / 2f;
        child.Y = y - 20f;
        foreach (DataGridViewBand selectedRow in (BaseCollection) this._dgv.SelectedRows)
        {
          this._dgv.Rows[selectedRow.Index].Cells["name"].Value = (object) str;
          this._dgv.Update();
          this._dgv.Refresh();
        }
      }
      else if (propertyName == "AppearsInRadar")
      {
        this.dr["appears_in_radar"] = (object) bool.Parse(str);
        if (bool.Parse(str))
          this.changeImage(1);
        else
          this.changeImage(0);
      }
      else if (propertyName == "RadarRange")
      {
        this.dr["radar_range"] = (object) float.Parse(str);
        float width = this.stargateImage.Width;
        float height = this.stargateImage.Height;
        float x = this.stargateImage.X;
        float y = this.stargateImage.Y;
        this.stargateImage.GetChild(1).X = (float) ((double) x + (double) width / 2.0 - (double) float.Parse(str) / 100.0);
        this.stargateImage.GetChild(1).Y = (float) ((double) y + (double) height / 2.0 - (double) float.Parse(str) / 100.0);
        this.stargateImage.GetChild(1).Width = (float) ((double) float.Parse(str) * 2.0 / 100.0);
        this.stargateImage.GetChild(1).Height = (float) ((double) float.Parse(str) * 2.0 / 100.0);
      }
      else if (propertyName == "Destination")
        this.dr["gate_to"] = (object) int.Parse(str);
      else if (propertyName == "IsClassSpecific")
      {
        this.dr["classSpecific"] = (object) bool.Parse(str);
        if (bool.Parse(str))
        {
          this.changeImage(2);
        }
        else
        {
          if (bool.Parse(this.dr["appears_in_radar"].ToString()))
            this.changeImage(1);
          else
            this.changeImage(0);
          if (int.Parse(this.dr["faction_id"].ToString()) > 0)
            this.changeImage(3);
        }
      }
      else if (propertyName == "FactionID")
      {
        int idbyName = mainFrm.factions.findIDbyName(str);
        this.dr["faction_id"] = (object) idbyName;
        if (idbyName > 0)
        {
          Console.Out.WriteLine("test2");
          this.changeImage(3);
        }
        else
        {
          if (bool.Parse(this.dr["appears_in_radar"].ToString()))
            this.changeImage(1);
          else
            this.changeImage(0);
          if (bool.Parse(this.dr["classSpecific"].ToString()))
            this.changeImage(2);
        }
      }
      else if (propertyName == "SoundEffect")
        this.dr["sound_effect_id"] = (object) int.Parse(str);
      else if (propertyName == "SoundEffectRange")
        this.dr["sound_effect_range"] = (object) float.Parse(str);
      else if (propertyName == "MinSecurityLevel")
      {
        for (int index = 0; index < HE_GlobalVars._ListofSecuritys.Length; ++index)
        {
          if (HE_GlobalVars._ListofSecuritys[index] == str)
            this.dr["minSecurityLevel"] = (object) (index * 10);
        }
      }
      if (this.dr.RowState == DataRowState.Modified)
        return;
      this.dr.SetModified();
    }

    private void changeImage(int type)
    {
      string path1_1 = "Images";
      string path1_2 = Path.Combine("..", "..");
      string path2 = (string) null;
      switch (type)
      {
        case 0:
          path2 = "hiddenGate.gif";
          this.appearsInRadar = false;
          break;
        case 1:
          path2 = "standardGate.gif";
          this.appearsInRadar = true;
          break;
        case 2:
          path2 = "classSpecificGate.gif";
          break;
        case 3:
          path2 = "FactionSpecificGate.gif";
          break;
      }
      if (File.Exists(Path.Combine(path1_1, path2)))
        path1_2 = "";
      float x = this.stargateImage.X;
      float y = this.stargateImage.Y;
      this.stargateImage.Image = Image.FromFile(Path.Combine(path1_2, Path.Combine(path1_1, path2)));
      this.stargateImage.X = x;
      this.stargateImage.Y = y;
    }

    public DataRow getRow()
    {
      return this.dr;
    }

    public void setPropGrid()
    {
      this._pg.SelectedObject = (object) this.dp;
    }
  }
}
