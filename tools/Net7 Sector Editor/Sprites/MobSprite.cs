﻿// Decompiled with JetBrains decompiler
// Type: N7.Sprites.MobSprite
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using N7.Props;
using N7.Utilities;
using System;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Windows.Forms;
using UMD.HCIL.Piccolo;
using UMD.HCIL.Piccolo.Event;
using UMD.HCIL.Piccolo.Nodes;

namespace N7.Sprites
{
  internal class MobSprite
  {
    private MobProps dp;
    private PropertyGrid _pg;
    private bool appearsInRadar;
    private PText pname;
    private DataRow dr;
    private PLayer _layer;
    private PImage mobImage;
    private bool dragging;
    private DataGridView _dgv;

    public MobSprite(PLayer layer, DataRow r, PropertyGrid pg, DataGridView dgv)
    {
      this._pg = pg;
      this.setupData(r);
      this.dr = r;
      this._layer = layer;
      this._dgv = dgv;
      string aText = r["name"].ToString();
      float num1 = float.Parse(r["position_x"].ToString());
      float num2 = -float.Parse(r["position_y"].ToString());
      float num3 = float.Parse(r["signature"].ToString());
      float num4 = float.Parse(r["radar_range"].ToString());
      float num5 = float.Parse(r["exploration_range"].ToString());
      this.appearsInRadar = (bool) r["appears_in_radar"];
      int num6 = int.Parse(r["nav_type"].ToString());
      float num7 = float.Parse(r["mob_spawn_radius"].ToString());
      float num8 = (float) ((double) num3 * 2.0 / 100.0);
      float num9 = (float) ((double) num4 * 2.0 / 100.0);
      float num10 = (float) ((double) num5 * 2.0 / 100.0);
      float num11 = (float) ((double) num7 * 2.0 / 100.0);
      if ((double) num8 == 0.0)
        num8 = 5f;
      if ((double) num9 == 0.0)
        num9 = 5f;
      if ((double) num10 == 0.0)
        num10 = 5f;
      if ((double) num11 == 0.0)
        num11 = 5f;
      string path1_1 = "Images";
      string path1_2 = Path.Combine("..", "..");
      if (File.Exists(Path.Combine(path1_1, "hostileMob.gif")))
        path1_2 = "";
      Image newImage = Image.FromFile(Path.Combine(path1_2, Path.Combine(path1_1, "hostileMob.gif")));
      this.mobImage = new PImage(newImage);
      this.mobImage.X = (float) (((double) num1 - (double) (newImage.Width / 2)) / 100.0);
      this.mobImage.Y = (float) (((double) num2 - (double) (newImage.Height / 2)) / 100.0);
      float x1 = (float) ((double) num1 / 100.0 - ((double) num8 / 2.0 - (double) (newImage.Width / 2)));
      float y1 = (float) ((double) num2 / 100.0 - ((double) num8 / 2.0 - (double) (newImage.Height / 2)));
      float x2 = (float) ((double) num1 / 100.0 - ((double) num9 / 2.0 - (double) (newImage.Width / 2)));
      float y2 = (float) ((double) num2 / 100.0 - ((double) num9 / 2.0 - (double) (newImage.Height / 2)));
      float x3 = (float) ((double) num1 / 100.0 - ((double) num10 / 2.0 - (double) (newImage.Width / 2)));
      float y3 = (float) ((double) num2 / 100.0 - ((double) num10 / 2.0 - (double) (newImage.Height / 2)));
      float x4 = (float) ((double) num1 / 100.0 - ((double) num11 / 2.0 - (double) (newImage.Width / 2)));
      float y4 = (float) ((double) num2 / 100.0 - ((double) num11 / 2.0 - (double) (newImage.Height / 2)));
      Pen pen1 = new Pen(Color.Red, 3f);
      Pen pen2 = new Pen(Color.MistyRose, 2f);
      pen2.DashStyle = DashStyle.Dash;
      Pen pen3 = new Pen(Color.Maroon, 1f);
      pen3.DashStyle = DashStyle.DashDotDot;
      Pen pen4 = new Pen(Color.Fuchsia, 1f);
      pen4.DashStyle = DashStyle.Dot;
      PPath ellipse1 = PPath.CreateEllipse(x1, y1, num8, num8);
      ellipse1.Pen = pen1;
      PPath ellipse2 = PPath.CreateEllipse(x2, y2, num9, num9);
      ellipse2.Pen = pen2;
      PPath ellipse3 = PPath.CreateEllipse(x3, y3, num10, num10);
      ellipse3.Pen = pen3;
      PPath ellipse4 = PPath.CreateEllipse(x4, y4, num11, num11);
      ellipse4.Pen = pen4;
      PNode child1 = (PNode) ellipse1;
      child1.Brush = Brushes.Transparent;
      PNode child2 = (PNode) ellipse2;
      child2.Brush = Brushes.Transparent;
      PNode child3 = (PNode) ellipse3;
      child3.Brush = Brushes.Transparent;
      PNode child4 = (PNode) ellipse4;
      child4.Brush = Brushes.Transparent;
      this.pname = new PText(aText);
      this.pname.TextBrush = Brushes.White;
      this.pname.TextAlignment = StringAlignment.Center;
      this.pname.X = (float) ((double) num1 / 100.0 - (double) this.pname.Width / 2.0);
      this.pname.Y = (float) ((double) num2 / 100.0 - 20.0);
      this.mobImage.AddChild(child1);
      this.mobImage.AddChild(child2);
      this.mobImage.AddChild(child3);
      this.mobImage.AddChild((PNode) this.pname);
      for (int index = 0; index < num6; ++index)
        this.mobImage.AddChild(new PNode());
      this.mobImage.AddChild(child4);
      this.mobImage.ChildrenPickable = false;
      this.mobImage.Tag = (object) this;
      this.mobImage.MouseDown += new PInputEventHandler(this.Image_MouseDown);
      this.mobImage.MouseUp += new PInputEventHandler(this.Image_MouseUp);
      this.mobImage.MouseDrag += new PInputEventHandler(this.Image_MouseDrag);
      layer.AddChild((PNode) this.mobImage);
    }

    private void setupData(DataRow r)
    {
      int num = int.Parse(r["type"].ToString());
      string str = "";
      switch (num)
      {
        case 0:
          str = "Mobs";
          break;
        case 3:
          str = "Planets";
          break;
        case 11:
          str = "Stargates";
          break;
        case 12:
          str = "Starbases";
          break;
        case 37:
          str = "Decorations";
          break;
        case 38:
          str = "Harvestables";
          break;
      }
      this.dp = new MobProps();
      this.dp.SectorID = int.Parse(r["sector_id"].ToString());
      this.dp.NavType = r["nav_type"].ToString();
      this.dp.Signature = float.Parse(r["signature"].ToString());
      this.dp.IsHuge = (bool) r["is_huge"];
      this.dp.BaseXP = int.Parse(r["base_xp"].ToString());
      this.dp.ExplorationRange = float.Parse(r["exploration_range"].ToString());
      this.dp.BaseAssetID = int.Parse(r["base_asset_id"].ToString());
      this.dp.Color = AdobeColors.HSL_to_RGB(new AdobeColors.HSL()
      {
        H = (double) float.Parse(r["h"].ToString()),
        S = (double) float.Parse(r["s"].ToString()),
        L = (double) float.Parse(r["v"].ToString())
      });
      this.dp.Type = str;
      this.dp.Scale = float.Parse(r["scale"].ToString());
      this.dp.PositionX = float.Parse(r["position_x"].ToString());
      this.dp.PositionY = float.Parse(r["position_y"].ToString());
      this.dp.PositionZ = float.Parse(r["position_z"].ToString());
      double[] angle = new QuaternionCalc().QuatToAngle(new double[4]
      {
        double.Parse(r["orientation_z"].ToString()),
        double.Parse(r["orientation_u"].ToString()),
        double.Parse(r["orientation_v"].ToString()),
        double.Parse(r["orientation_w"].ToString())
      });
      if (angle[0] == double.NaN)
        angle[0] = 0.0;
      if (angle[1] == double.NaN)
        angle[1] = 0.0;
      if (angle[2] == double.NaN)
        angle[2] = 0.0;
      this.dp.Orientation_Yaw = Math.Round(angle[0], 0);
      this.dp.Orientation_Pitch = Math.Round(angle[1], 0);
      this.dp.Orientation_Roll = Math.Round(angle[2], 0);
      this.dp.Name = r["name"].ToString();
      this.dp.AppearsInRadar = (bool) r["appears_in_radar"];
      this.dp.RadarRange = float.Parse(r["radar_range"].ToString());
      this.dp.Destination = int.Parse(r["gate_to"].ToString());
      this.dp.SoundEffect = int.Parse(r["sound_effect_id"].ToString());
      this.dp.SoundEffectRange = float.Parse(r["sound_effect_range"].ToString());
      this.dp.SpawnGroup = "<Collection...>";
      this.dp.Count = int.Parse(r["mob_count"].ToString());
      this.dp.SpawnRadius = float.Parse(r["mob_spawn_radius"].ToString());
      this.dp.RespawnTime = float.Parse(r["respawn_time"].ToString());
      this.dp.DelayedSpawn = (bool) r["delayed_spawn"];
    }

    protected void Image_MouseDown(object sender, PInputEventArgs e)
    {
      this._pg.SelectedObject = (object) this.dp;
    }

    protected void Image_MouseDrag(object sender, PInputEventArgs e)
    {
      this.dragging = true;
    }

    protected void Image_MouseUp(object sender, PInputEventArgs e)
    {
      if (this.dragging)
      {
        this.dp.PositionX = e.Position.X * 100f;
        this.dp.PositionY = (float) -((double) e.Position.Y * 100.0);
        this.dr["position_x"] = (object) (float) ((double) e.Position.X * 100.0);
        this.dr["position_y"] = (object) (float) -((double) e.Position.Y * 100.0);
        this.dragging = false;
      }
      this._pg.SelectedObject = (object) this.dp;
      mainFrm.selectedObjectID = int.Parse(this.dr["sector_object_id"].ToString());
    }

    public bool getAppearsInRader()
    {
      return this.appearsInRadar;
    }

    public PText getText()
    {
      return this.pname;
    }

    public void updateChangedInfo(string propertyName, object _changedValue)
    {
      string s = _changedValue.ToString();
      if (propertyName == "SectorID")
        this.dr["sector_id"] = (object) int.Parse(s);
      else if (propertyName == "NavType")
        this.dr["nav_type"] = (object) s;
      else if (propertyName == "Signature")
      {
        this.dr["signature"] = (object) float.Parse(s);
        float width = this.mobImage.Width;
        float height = this.mobImage.Height;
        float x = this.mobImage.X;
        float y = this.mobImage.Y;
        this.mobImage.GetChild(0).X = (float) ((double) x + (double) width / 2.0 - (double) float.Parse(s) / 100.0);
        this.mobImage.GetChild(0).Y = (float) ((double) y + (double) height / 2.0 - (double) float.Parse(s) / 100.0);
        this.mobImage.GetChild(0).Width = (float) ((double) float.Parse(s) * 2.0 / 100.0);
        this.mobImage.GetChild(0).Height = (float) ((double) float.Parse(s) * 2.0 / 100.0);
      }
      else if (propertyName == "IsHuge")
        this.dr["is_huge"] = (object) bool.Parse(s);
      else if (propertyName == "BaseXP")
        this.dr["base_xp"] = (object) int.Parse(s);
      else if (propertyName == "ExplorationRange")
      {
        this.dr["exploration_range"] = (object) float.Parse(s);
        float width = this.mobImage.Width;
        float height = this.mobImage.Height;
        float x = this.mobImage.X;
        float y = this.mobImage.Y;
        this.mobImage.GetChild(2).X = (float) ((double) x + (double) width / 2.0 - (double) float.Parse(s) / 100.0);
        this.mobImage.GetChild(2).Y = (float) ((double) y + (double) height / 2.0 - (double) float.Parse(s) / 100.0);
        this.mobImage.GetChild(2).Width = (float) ((double) float.Parse(s) * 2.0 / 100.0);
        this.mobImage.GetChild(2).Height = (float) ((double) float.Parse(s) * 2.0 / 100.0);
      }
      else if (propertyName == "BaseAssetID")
      {
        this.dr["base_asset_id"] = (object) int.Parse(s);
        foreach (DataGridViewBand selectedRow in (BaseCollection) this._dgv.SelectedRows)
        {
          this._dgv.Rows[selectedRow.Index].Cells["base_asset_id"].Value = (object) int.Parse(s);
          this._dgv.Update();
          this._dgv.Refresh();
        }
      }
      else if (propertyName == "Color")
      {
        AdobeColors.HSL hsl = AdobeColors.RGB_to_HSL((Color) _changedValue);
        this.dr["h"] = (object) hsl.H;
        this.dr["s"] = (object) hsl.S;
        this.dr["v"] = (object) hsl.L;
      }
      else if (propertyName == "Type")
      {
        this._layer.RemoveChild((PNode) this.mobImage);
        this._pg.SelectedObject = (object) null;
      }
      else if (propertyName == "Scale")
        this.dr["scale"] = (object) float.Parse(s);
      else if (propertyName == "PositionX")
      {
        this.dr["position_x"] = (object) float.Parse(s);
        this.mobImage.TranslateBy(float.Parse(s) / 100f - this.mobImage.X, 0.0f);
      }
      else if (propertyName == "PositionY")
      {
        this.dr["position_y"] = (object) float.Parse(s);
        this.mobImage.TranslateBy(0.0f, float.Parse(s) / 100f - this.mobImage.Y);
      }
      else if (propertyName == "PositionZ")
        this.dr["position_z"] = (object) float.Parse(s);
      else if (propertyName == "Orientation_Yaw" || propertyName == "Orientation_Pitch" || propertyName == "Orientation_Roll")
      {
        double[] quat = new QuaternionCalc().AngleToQuat(this.dp.Orientation_Yaw, this.dp.Orientation_Pitch, this.dp.Orientation_Roll);
        this.dr["orientation_z"] = (object) quat[0];
        this.dr["orientation_u"] = (object) quat[1];
        this.dr["orientation_v"] = (object) quat[2];
        this.dr["orientation_w"] = (object) quat[3];
      }
      else if (propertyName == "Name")
      {
        this.dr["name"] = (object) s;
        float x = this.mobImage.X;
        float y = this.mobImage.Y;
        PText child = (PText) this.mobImage.GetChild(3);
        child.Text = s;
        child.TextAlignment = StringAlignment.Center;
        child.X = x - child.Width / 2f;
        child.Y = y - 20f;
        foreach (DataGridViewBand selectedRow in (BaseCollection) this._dgv.SelectedRows)
        {
          this._dgv.Rows[selectedRow.Index].Cells["name"].Value = (object) s;
          this._dgv.Update();
          this._dgv.Refresh();
        }
      }
      else if (propertyName == "AppearsInRadar")
      {
        this.dr["appears_in_radar"] = (object) bool.Parse(s);
        if (bool.Parse(s))
          this.changeImage(1);
        else
          this.changeImage(0);
      }
      else if (propertyName == "RadarRange")
      {
        this.dr["radar_range"] = (object) float.Parse(s);
        float width = this.mobImage.Width;
        float height = this.mobImage.Height;
        float x = this.mobImage.X;
        float y = this.mobImage.Y;
        this.mobImage.GetChild(1).X = (float) ((double) x + (double) width / 2.0 - (double) float.Parse(s) / 100.0);
        this.mobImage.GetChild(1).Y = (float) ((double) y + (double) height / 2.0 - (double) float.Parse(s) / 100.0);
        this.mobImage.GetChild(1).Width = (float) ((double) float.Parse(s) * 2.0 / 100.0);
        this.mobImage.GetChild(1).Height = (float) ((double) float.Parse(s) * 2.0 / 100.0);
      }
      else if (propertyName == "Destination")
        this.dr["gate_to"] = (object) int.Parse(s);
      else if (propertyName == "SpawnRadius")
      {
        this.dr["mob_spawn_radius"] = (object) float.Parse(s);
        int index = 3 + int.Parse(this.dr["nav_type"].ToString()) + 1;
        float width = this.mobImage.Width;
        float height = this.mobImage.Height;
        float x = this.mobImage.X;
        float y = this.mobImage.Y;
        this.mobImage.GetChild(index).X = (float) ((double) x + (double) width / 2.0 - (double) float.Parse(s) / 100.0);
        this.mobImage.GetChild(index).Y = (float) ((double) y + (double) height / 2.0 - (double) float.Parse(s) / 100.0);
        this.mobImage.GetChild(index).Width = (float) ((double) float.Parse(s) * 2.0 / 100.0);
        this.mobImage.GetChild(index).Height = (float) ((double) float.Parse(s) * 2.0 / 100.0);
      }
      else if (propertyName == "Count")
        this.dr["mob_count"] = (object) int.Parse(s);
      else if (propertyName == "SoundEffect")
        this.dr["sound_effect_id"] = (object) int.Parse(s);
      else if (propertyName == "SoundEffectRange")
        this.dr["sound_effect_range"] = (object) float.Parse(s);
      else if (propertyName == "RespawnTime")
        this.dr["respawn_time"] = (object) int.Parse(s);
      else if (propertyName == "DelayedSpawn")
        this.dr["delayed_spawn"] = (object) bool.Parse(s);
      if (this.dr.RowState == DataRowState.Modified)
        return;
      this.dr.SetModified();
    }

    private void changeImage(int type)
    {
      string path1_1 = "Images";
      string path1_2 = Path.Combine("..", "..");
      string path2 = (string) null;
      switch (type)
      {
        case 0:
          path2 = "hiddenNav.gif";
          this.appearsInRadar = false;
          break;
        case 1:
          path2 = "standardNav.gif";
          this.appearsInRadar = true;
          break;
      }
      if (File.Exists(Path.Combine(path1_1, path2)))
        path1_2 = "";
      float x = this.mobImage.X;
      float y = this.mobImage.Y;
      this.mobImage.Image = Image.FromFile(Path.Combine(path1_2, Path.Combine(path1_1, path2)));
      this.mobImage.X = x;
      this.mobImage.Y = y;
    }

    public DataRow getRow()
    {
      return this.dr;
    }

    public void setPropGrid()
    {
      this._pg.SelectedObject = (object) this.dp;
    }
  }
}
