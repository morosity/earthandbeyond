﻿// Decompiled with JetBrains decompiler
// Type: N7.Sprites.SectorBoundsSprite
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using System.Drawing;
using System.Drawing.Drawing2D;
using UMD.HCIL.Piccolo;
using UMD.HCIL.Piccolo.Nodes;

namespace N7.Sprites
{
  internal class SectorBoundsSprite
  {
    public SectorBoundsSprite(PLayer layer, float x_min, float y_min, float x_max, float y_max)
    {
      float width = (float) (((double) x_max - (double) x_min) / 100.0);
      float height = (float) (((double) y_max - (double) y_min) / 100.0);
      float x = (float) -((double) width / 2.0);
      float y = (float) -((double) height / 2.0);
      Pen pen1 = new Pen(Color.Red, 10f);
      pen1.DashStyle = DashStyle.DashDotDot;
      Pen pen2 = new Pen(Color.White, 2.5f);
      pen2.DashStyle = DashStyle.Solid;
      new Pen(Color.White, 2.5f).DashStyle = DashStyle.Solid;
      PPath rectangle = PPath.CreateRectangle(x, y, width, height);
      rectangle.Brush = Brushes.Transparent;
      rectangle.Pen = pen1;
      PPath ppath1 = new PPath();
      ppath1.AddLine(-50f, 0.0f, 50f, 0.0f);
      ppath1.Pen = pen2;
      PPath ppath2 = new PPath();
      ppath2.AddLine(0.0f, -50f, 0.0f, 50f);
      ppath2.Pen = pen2;
      PText ptext1 = new PText();
      ptext1.TextBrush = Brushes.White;
      ptext1.TextAlignment = StringAlignment.Center;
      ptext1.Text = "0,0";
      ptext1.X = 5f;
      ptext1.Y = 5f;
      PText ptext2 = new PText();
      ptext2.TextBrush = Brushes.White;
      ptext2.TextAlignment = StringAlignment.Center;
      ptext2.Text = "+X";
      ptext2.X = 52f;
      ptext2.Y = -9f;
      PText ptext3 = new PText();
      ptext3.TextBrush = Brushes.White;
      ptext3.TextAlignment = StringAlignment.Center;
      ptext3.Text = "+Y";
      ptext3.X = -12f;
      ptext3.Y = 52f;
      rectangle.AddChild((PNode) ptext1);
      rectangle.AddChild((PNode) ptext2);
      rectangle.AddChild((PNode) ptext3);
      rectangle.AddChild((PNode) ppath1);
      rectangle.AddChild((PNode) ppath2);
      rectangle.Pickable = false;
      layer.AddChild((PNode) rectangle);
    }
  }
}
