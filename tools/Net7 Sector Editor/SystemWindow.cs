﻿// Decompiled with JetBrains decompiler
// Type: N7.SystemWindow
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using N7.Sprites;
using N7.Utilities;
using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using UMD.HCIL.Piccolo;
using UMD.HCIL.Piccolo.Event;

namespace N7
{
  public class SystemWindow
  {
    private PCanvas canvas;
    private PLayer masterLayer;
    private PLayer sectorLayer;
    private PNode pSelectedNode;
    private DataRow dr;
    private PropertyGrid _pg;

    public SystemWindow(PCanvas pcanvas, string systemName, DataRow[] sectorTable, PropertyGrid pg, DataRow selectedRow)
    {
      this.canvas = pcanvas;
      this.dr = selectedRow;
      this._pg = pg;
      this.sectorLayer = new PLayer();
      MouseWheelZoomController wheelZoomController = new MouseWheelZoomController(this.canvas.Camera);
      this.canvas.BackColor = Color.Black;
      this.masterLayer = this.canvas.Layer;
      for (int index = 0; index < sectorTable.Length; ++index)
      {
        string name = sectorTable[index]["name"].ToString();
        float xmin = float.Parse(sectorTable[index]["x_min"].ToString());
        float xmax = float.Parse(sectorTable[index]["x_max"].ToString());
        float ymin = float.Parse(sectorTable[index]["y_min"].ToString());
        float ymax = float.Parse(sectorTable[index]["y_max"].ToString());
        float galaxy_x = float.Parse(sectorTable[index]["galaxy_x"].ToString());
        float galaxy_y = float.Parse(sectorTable[index]["galaxy_y"].ToString());
        DataRow r = sectorTable[index];
        SectorSprite sectorSprite = new SectorSprite(this.sectorLayer, pg, r, name, galaxy_x, galaxy_y, xmin, xmax, ymin, ymax);
      }
      this.masterLayer.AddChild((PNode) this.sectorLayer);
      this.masterLayer.MouseDown += new PInputEventHandler(this.MasterLayer_OnMouseDown);
    }

    public void pg_PropertyValueChanged(object sender, PropertyValueChangedEventArgs e)
    {
      string name = e.ChangedItem.PropertyDescriptor.Name;
      try
      {
        if (!this.pSelectedNode.Tag.GetType().ToString().Contains("SectorSprite"))
          return;
        ((SectorSprite) this.pSelectedNode.Tag).updateChangedInfo(name, e.ChangedItem.Value.ToString());
      }
      catch (Exception ex)
      {
        this.updateChangedInfo(name, e.ChangedItem.Value);
      }
    }

    public void MasterLayer_OnMouseDown(object sender, PInputEventArgs e)
    {
      if (e.PickedNode.Tag.GetType().ToString().Contains("SectorSprite"))
      {
        this.setOriginalText(this.pSelectedNode);
        ((SectorSprite) e.PickedNode.Tag).getText().TextBrush = Brushes.Red;
      }
      this.pSelectedNode = e.PickedNode;
    }

    public void setOriginalText(PNode pickedNode)
    {
      if (this.pSelectedNode == null || !pickedNode.Tag.GetType().ToString().Contains("SectorSprite"))
        return;
      ((SectorSprite) this.pSelectedNode.Tag).getText().TextBrush = Brushes.White;
    }

    public void updateChangedInfo(string propertyName, object _changedValue)
    {
      string s = _changedValue.ToString().Replace("'", "''");
      if (propertyName == "Name")
        this.dr["name"] = (object) s;
      else if (propertyName == "GalaxyX")
        this.dr["galaxy_x"] = (object) float.Parse(s);
      else if (propertyName == "GalaxyY")
        this.dr["galaxy_y"] = (object) float.Parse(s);
      else if (propertyName == "GalaxyZ")
        this.dr["galaxy_z"] = (object) float.Parse(s);
      else if (propertyName == "Color")
      {
        Color color = (Color) _changedValue;
        this.dr["color_r"] = (object) color.R;
        this.dr["color_g"] = (object) color.G;
        this.dr["color_b"] = (object) color.B;
      }
      else if (propertyName == "Notes")
        this.dr["notes"] = (object) s;
      if (this.dr.RowState == DataRowState.Modified)
        return;
      this.dr.SetModified();
    }

    public void newSector(DataRow ndr)
    {
      string name = ndr["name"].ToString();
      float xmin = float.Parse(ndr["x_min"].ToString());
      float xmax = float.Parse(ndr["x_max"].ToString());
      float ymin = float.Parse(ndr["y_min"].ToString());
      float ymax = float.Parse(ndr["y_max"].ToString());
      float galaxy_x = float.Parse(ndr["galaxy_x"].ToString());
      float galaxy_y = float.Parse(ndr["galaxy_y"].ToString());
      SectorSprite sectorSprite = new SectorSprite(this.sectorLayer, this._pg, ndr, name, galaxy_x, galaxy_y, xmin, xmax, ymin, ymax);
    }
  }
}
