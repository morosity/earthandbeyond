﻿// Decompiled with JetBrains decompiler
// Type: N7.Program
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using System;
using System.Data;
using System.Windows.Forms;

namespace N7
{
  internal static class Program
  {
    [STAThread]
    private static void Main()
    {
      try
      {
        DataSet dataSet = new DataSet();
        int num = (int) dataSet.ReadXml(Application.StartupPath + "\\Config.xml");
        DataRow row = dataSet.Tables[0].Rows[0];
        SQLData.Host = row.ItemArray[0].ToString();
        SQLData.Port = int.Parse(row.ItemArray[1].ToString());
        SQLData.User = row.ItemArray[2].ToString();
        SQLData.Pass = row.ItemArray[3].ToString();
      }
      catch (Exception ex)
      {
        SQLData.Host = "net-7.org";
        SQLData.Port = 3307;
        SQLData.User = "";
        SQLData.Pass = "";
      }
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      Login login = new Login();
      login.LoginUsername.Text = SQLData.User;
      login.LoginPassword.Text = SQLData.Pass;
      login.SQLServer.Text = SQLData.Host;
      login.SQLPort.Text = SQLData.Port.ToString();
      int num1 = (int) login.ShowDialog();
      if (login.m_Cancel)
        return;
      if (Database.executeQuery(Database.DatabaseName.net7, "SELECT * FROM versions where Ename='Sector';").Rows[0]["Version"].ToString() != "1.2")
      {
        int num2 = (int) MessageBox.Show("Sorry, you have an old version, please update! \n Program will now exit!", "Version Check");
        Application.Exit();
      }
      else
        Application.Run((Form) new mainFrm());
    }
  }
}
