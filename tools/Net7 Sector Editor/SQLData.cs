﻿// Decompiled with JetBrains decompiler
// Type: N7.SQLData
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

namespace N7
{
  internal static class SQLData
  {
    private static string m_Host;
    private static int m_Port;
    private static string m_User;
    private static string m_Pass;

    public static string Host
    {
      get
      {
        return SQLData.m_Host;
      }
      set
      {
        SQLData.m_Host = value;
      }
    }

    public static int Port
    {
      get
      {
        return SQLData.m_Port;
      }
      set
      {
        SQLData.m_Port = value;
      }
    }

    public static string User
    {
      get
      {
        return SQLData.m_User;
      }
      set
      {
        SQLData.m_User = value;
      }
    }

    public static string Pass
    {
      get
      {
        return SQLData.m_Pass;
      }
      set
      {
        SQLData.m_Pass = value;
      }
    }

    public static string ConnStr(string DB)
    {
      return "Connect Timeout=30;Persist Security Info=False;Database=" + DB + ";Host=" + SQLData.m_Host + ";Port=" + SQLData.m_Port.ToString() + ";Username=" + SQLData.m_User + ";Password=" + SQLData.m_Pass;
    }
  }
}
