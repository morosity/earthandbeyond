﻿// Decompiled with JetBrains decompiler
// Type: N7.GUI.NewSectorObject
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using N7.Props;
using N7.Sql;
using N7.Utilities;
using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace N7.GUI
{
  public class NewSectorObject : Form
  {
    private IContainer components;
    private PropertyGrid propertyGrid1;
    private Button button1;
    private Button button2;
    private Button button3;
    private string type;
    private SectorObjectsSql sectorObjectsSQL;
    private TreeView tree;
    private MobProps mp;
    private PlanetProps pp;
    private StarbaseProps sbp;
    private StargateProps sgp;
    private BaseProps bp;
    private HarvestableProps hp;
    private int type2;
    private PointF position;
    private SectorWindow _s1;
    private int lastInsertID;
    private DataRow newSectorObjectsRow;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (NewSectorObject));
      this.propertyGrid1 = new PropertyGrid();
      this.button1 = new Button();
      this.button2 = new Button();
      this.button3 = new Button();
      this.SuspendLayout();
      this.propertyGrid1.Dock = DockStyle.Top;
      this.propertyGrid1.Location = new Point(0, 0);
      this.propertyGrid1.Name = "propertyGrid1";
      this.propertyGrid1.Size = new Size(294, 429);
      this.propertyGrid1.TabIndex = 0;
      this.button1.Location = new Point(12, 435);
      this.button1.Name = "button1";
      this.button1.Size = new Size(75, 23);
      this.button1.TabIndex = 1;
      this.button1.Text = "Cancel";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new EventHandler(this.button1_Click);
      this.button2.Location = new Point(207, 435);
      this.button2.Name = "button2";
      this.button2.Size = new Size(75, 23);
      this.button2.TabIndex = 2;
      this.button2.Text = "Insert";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new EventHandler(this.button2_Click);
      this.button3.Location = new Point(103, 435);
      this.button3.Name = "button3";
      this.button3.Size = new Size(91, 23);
      this.button3.TabIndex = 3;
      this.button3.Text = "Select Position";
      this.button3.UseVisualStyleBackColor = true;
      this.button3.Click += new EventHandler(this.button3_Click);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.AutoValidate = AutoValidate.Disable;
      this.ClientSize = new Size(294, 470);
      this.Controls.Add((Control) this.button3);
      this.Controls.Add((Control) this.button2);
      this.Controls.Add((Control) this.button1);
      this.Controls.Add((Control) this.propertyGrid1);
      this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = nameof (NewSectorObject);
      this.SizeGripStyle = SizeGripStyle.Hide;
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "New Sector Object";
      this.Load += new EventHandler(this.NewSectorObject_Load);
      this.ResumeLayout(false);
    }

    public NewSectorObject(string typeName, TreeView t1, SectorWindow s1)
    {
      this.tree = t1;
      this.sectorObjectsSQL = mainFrm.sectorObjects;
      this.type = typeName;
      this._s1 = s1;
      mainFrm.selectedObjectID = 0;
      this.InitializeComponent();
    }

    private void NewSectorObject_Load(object sender, EventArgs e)
    {
      string text = this.tree.SelectedNode.Text;
      int idFromName = mainFrm.sectors.getIDFromName(text);
      if (this.type.Contains("Mobs"))
      {
        this.mp = new MobProps();
        this.mp.SectorID = idFromName;
        this.mp.Color = Color.Black;
        this.mp.Type = "Mobs";
        this.mp.Signature = 7000f;
        this.mp.RadarRange = 5000f;
        this.mp.ExplorationRange = 3000f;
        this.mp.SoundEffect = -1;
        this.mp.SoundEffectRange = 0.0f;
        this.mp.SpawnGroup = "<Collection...>";
        this.mp.SpawnRadius = 4000f;
        this.mp.Name = "<New Sector Object>";
        this.propertyGrid1.SelectedObject = (object) this.mp;
        this.type2 = 0;
      }
      else if (this.type.Contains("Planets"))
      {
        this.pp = new PlanetProps();
        this.pp.SectorID = idFromName;
        this.pp.Color = Color.Black;
        this.pp.Type = "Planets";
        this.pp.Signature = 7000f;
        this.pp.RadarRange = 5000f;
        this.pp.ExplorationRange = 3000f;
        this.pp.SoundEffect = -1;
        this.pp.SoundEffectRange = 0.0f;
        this.pp.Name = "<New Sector Object>";
        this.propertyGrid1.SelectedObject = (object) this.pp;
        this.type2 = 3;
      }
      else if (this.type.Contains("Stargates"))
      {
        this.sgp = new StargateProps();
        this.sgp.SectorID = idFromName;
        this.sgp.Color = Color.Black;
        this.sgp.Type = "Stargates";
        this.sgp.Signature = 7000f;
        this.sgp.RadarRange = 5000f;
        this.sgp.ExplorationRange = 3000f;
        this.sgp.SoundEffect = -1;
        this.sgp.SoundEffectRange = 0.0f;
        this.sgp.Name = "<New Sector Object>";
        this.sgp.PositionX = this.position.X * 100f;
        this.sgp.PositionY = this.position.Y * 100f;
        this.propertyGrid1.SelectedObject = (object) this.sgp;
        this.type2 = 11;
      }
      else if (this.type.Contains("Starbases"))
      {
        this.sbp = new StarbaseProps();
        this.sbp.SectorID = idFromName;
        this.sbp.Color = Color.Black;
        this.sbp.Type = "Starbases";
        this.sbp.Signature = 7000f;
        this.sbp.RadarRange = 5000f;
        this.sbp.ExplorationRange = 3000f;
        this.sbp.SoundEffect = -1;
        this.sbp.SoundEffectRange = 0.0f;
        this.sbp.Name = "<New Sector Object>";
        this.propertyGrid1.SelectedObject = (object) this.sbp;
        this.type2 = 12;
      }
      else if (this.type.Contains("Decorations"))
      {
        this.bp = new BaseProps();
        this.bp.SectorID = idFromName;
        this.bp.Color = Color.Black;
        this.bp.Type = "Decorations";
        this.bp.Signature = 7000f;
        this.bp.RadarRange = 5000f;
        this.bp.ExplorationRange = 3000f;
        this.bp.SoundEffect = -1;
        this.bp.SoundEffectRange = 0.0f;
        this.bp.Name = "<New Sector Object>";
        this.propertyGrid1.SelectedObject = (object) this.bp;
        this.type2 = 37;
      }
      else if (this.type.Contains("Harvestable"))
      {
        this.hp = new HarvestableProps();
        this.hp.SectorID = idFromName;
        this.hp.Color = Color.Black;
        this.hp.Type = "Harvestables";
        this.hp.Signature = 7000f;
        this.hp.RadarRange = 5000f;
        this.hp.Field = "Random";
        this.hp.ExplorationRange = 3000f;
        this.hp.SoundEffect = -1;
        this.hp.SoundEffectRange = 0.0f;
        this.hp.SpawnGroup = "<Collection...>";
        this.hp.MaxFieldRadius = 3000f;
        this.hp.MobSpawnRadius = 0.0f;
        this.hp.Name = "<New Sector Object>";
        this.hp.ResType = "<Collection...>";
        this.propertyGrid1.SelectedObject = (object) this.hp;
        this.type2 = 38;
      }
      else if (this.type.Contains("Radation"))
      {
        this.bp = new BaseProps();
        this.bp.SectorID = idFromName;
        this.bp.Color = Color.Black;
        this.bp.Type = "Radation";
        this.bp.Signature = 7000f;
        this.bp.RadarRange = 5000f;
        this.bp.ExplorationRange = 3000f;
        this.bp.SoundEffect = -1;
        this.bp.SoundEffectRange = 0.0f;
        this.bp.Name = "<New Sector Object>";
        this.propertyGrid1.SelectedObject = (object) this.bp;
        this.type2 = 40;
      }
      else
      {
        if (!this.type.Contains("Gravity Well"))
          return;
        this.bp = new BaseProps();
        this.bp.SectorID = idFromName;
        this.bp.Color = Color.Black;
        this.bp.Type = "Gravity Well";
        this.bp.Signature = 7000f;
        this.bp.RadarRange = 5000f;
        this.bp.ExplorationRange = 3000f;
        this.bp.SoundEffect = -1;
        this.bp.SoundEffectRange = 0.0f;
        this.bp.Name = "<New Sector Object>";
        this.propertyGrid1.SelectedObject = (object) this.bp;
        this.type2 = 41;
      }
    }

    private void button1_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void button2_Click(object sender, EventArgs e)
    {
      if (this.lastInsertID == 0)
      {
        DataTable sectorObject = this.sectorObjectsSQL.getSectorObject();
        this.newSectorObjectsRow = sectorObject.NewRow();
        QuaternionCalc quaternionCalc = new QuaternionCalc();
        switch (this.type2)
        {
          case 0:
            this.newSectorObjectsRow["sector_id"] = (object) this.mp.SectorID;
            this.newSectorObjectsRow["nav_type"] = (object) this.mp.NavType;
            this.newSectorObjectsRow["signature"] = (object) this.mp.Signature;
            this.newSectorObjectsRow["is_huge"] = (object) this.mp.IsHuge;
            this.newSectorObjectsRow["base_xp"] = (object) this.mp.BaseXP;
            this.newSectorObjectsRow["exploration_range"] = (object) this.mp.ExplorationRange;
            this.newSectorObjectsRow["base_asset_id"] = (object) this.mp.BaseAssetID;
            this.newSectorObjectsRow["h"] = (object) this.mp.Color.GetHue();
            this.newSectorObjectsRow["s"] = (object) this.mp.Color.GetSaturation();
            this.newSectorObjectsRow["v"] = (object) this.mp.Color.GetBrightness();
            this.newSectorObjectsRow["type"] = (object) 0;
            this.newSectorObjectsRow["scale"] = (object) this.mp.Scale;
            this.newSectorObjectsRow["position_x"] = (object) this.mp.PositionX;
            this.newSectorObjectsRow["position_y"] = (object) this.mp.PositionY;
            this.newSectorObjectsRow["position_z"] = (object) this.mp.PositionZ;
            double[] quat1 = quaternionCalc.AngleToQuat(this.mp.Orientation_Yaw, this.mp.Orientation_Pitch, this.mp.Orientation_Roll);
            this.newSectorObjectsRow["orientation_z"] = (object) quat1[0];
            this.newSectorObjectsRow["orientation_u"] = (object) quat1[1];
            this.newSectorObjectsRow["orientation_v"] = (object) quat1[2];
            this.newSectorObjectsRow["orientation_w"] = (object) quat1[3];
            this.newSectorObjectsRow["name"] = (object) this.mp.Name.Replace("'", "''");
            this.newSectorObjectsRow["appears_in_radar"] = (object) this.mp.AppearsInRadar;
            this.newSectorObjectsRow["radar_range"] = (object) this.mp.RadarRange;
            this.newSectorObjectsRow["gate_to"] = (object) this.mp.Destination;
            this.newSectorObjectsRow["sound_effect_id"] = (object) this.mp.SoundEffect;
            this.newSectorObjectsRow["sound_effect_range"] = (object) this.mp.SoundEffectRange;
            this.newSectorObjectsRow["mob_spawn_radius"] = (object) this.mp.SpawnRadius;
            this.newSectorObjectsRow["mob_count"] = (object) this.mp.Count;
            this.newSectorObjectsRow["respawn_time"] = (object) this.mp.RespawnTime;
            this.newSectorObjectsRow["delayed_spawn"] = (object) this.mp.DelayedSpawn;
            break;
          case 3:
            this.newSectorObjectsRow["sector_id"] = (object) this.pp.SectorID;
            this.newSectorObjectsRow["nav_type"] = (object) this.pp.NavType;
            this.newSectorObjectsRow["signature"] = (object) this.pp.Signature;
            this.newSectorObjectsRow["is_huge"] = (object) this.pp.IsHuge;
            this.newSectorObjectsRow["base_xp"] = (object) this.pp.BaseXP;
            this.newSectorObjectsRow["exploration_range"] = (object) this.pp.ExplorationRange;
            this.newSectorObjectsRow["base_asset_id"] = (object) this.pp.BaseAssetID;
            this.newSectorObjectsRow["h"] = (object) this.pp.Color.GetHue();
            this.newSectorObjectsRow["s"] = (object) this.pp.Color.GetSaturation();
            this.newSectorObjectsRow["v"] = (object) this.pp.Color.GetBrightness();
            this.newSectorObjectsRow["type"] = (object) 3;
            this.newSectorObjectsRow["scale"] = (object) this.pp.Scale;
            this.newSectorObjectsRow["position_x"] = (object) this.pp.PositionX;
            this.newSectorObjectsRow["position_y"] = (object) this.pp.PositionY;
            this.newSectorObjectsRow["position_z"] = (object) this.pp.PositionZ;
            double[] quat2 = quaternionCalc.AngleToQuat(this.pp.Orientation_Yaw, this.pp.Orientation_Pitch, this.pp.Orientation_Roll);
            this.newSectorObjectsRow["orientation_z"] = (object) quat2[0];
            this.newSectorObjectsRow["orientation_u"] = (object) quat2[1];
            this.newSectorObjectsRow["orientation_v"] = (object) quat2[2];
            this.newSectorObjectsRow["orientation_w"] = (object) quat2[3];
            this.newSectorObjectsRow["name"] = (object) this.pp.Name.Replace("'", "''");
            this.newSectorObjectsRow["appears_in_radar"] = (object) this.pp.AppearsInRadar;
            this.newSectorObjectsRow["radar_range"] = (object) this.pp.RadarRange;
            this.newSectorObjectsRow["gate_to"] = (object) this.pp.Destination;
            this.newSectorObjectsRow["sound_effect_id"] = (object) this.pp.SoundEffect;
            this.newSectorObjectsRow["sound_effect_range"] = (object) this.pp.SoundEffectRange;
            this.newSectorObjectsRow["orbit_id"] = (object) this.pp.OrbitID;
            this.newSectorObjectsRow["orbit_dist"] = (object) this.pp.OrbitDist;
            this.newSectorObjectsRow["orbit_angle"] = (object) this.pp.OrbitAngle;
            this.newSectorObjectsRow["orbit_rate"] = (object) this.pp.OrbitRate;
            this.newSectorObjectsRow["rotate_rate"] = (object) this.pp.RotateRate;
            this.newSectorObjectsRow["rotate_angle"] = (object) this.pp.RotateAngle;
            this.newSectorObjectsRow["tilt_angle"] = (object) this.pp.TiltAngle;
            this.newSectorObjectsRow["is_landable"] = (object) this.pp.IsLandable;
            break;
          case 11:
            this.newSectorObjectsRow["sector_id"] = (object) this.sgp.SectorID;
            this.newSectorObjectsRow["nav_type"] = (object) this.sgp.NavType;
            this.newSectorObjectsRow["signature"] = (object) this.sgp.Signature;
            this.newSectorObjectsRow["is_huge"] = (object) this.sgp.IsHuge;
            this.newSectorObjectsRow["base_xp"] = (object) this.sgp.BaseXP;
            this.newSectorObjectsRow["exploration_range"] = (object) this.sgp.ExplorationRange;
            this.newSectorObjectsRow["base_asset_id"] = (object) this.sgp.BaseAssetID;
            this.newSectorObjectsRow["h"] = (object) this.sgp.Color.GetHue();
            this.newSectorObjectsRow["s"] = (object) this.sgp.Color.GetSaturation();
            this.newSectorObjectsRow["v"] = (object) this.sgp.Color.GetBrightness();
            this.newSectorObjectsRow["type"] = (object) 11;
            this.newSectorObjectsRow["scale"] = (object) this.sgp.Scale;
            this.newSectorObjectsRow["position_x"] = (object) this.sgp.PositionX;
            this.newSectorObjectsRow["position_y"] = (object) this.sgp.PositionY;
            this.newSectorObjectsRow["position_z"] = (object) this.sgp.PositionZ;
            double[] quat3 = quaternionCalc.AngleToQuat(this.sgp.Orientation_Yaw, this.sgp.Orientation_Pitch, this.sgp.Orientation_Roll);
            this.newSectorObjectsRow["orientation_z"] = (object) quat3[0];
            this.newSectorObjectsRow["orientation_u"] = (object) quat3[1];
            this.newSectorObjectsRow["orientation_v"] = (object) quat3[2];
            this.newSectorObjectsRow["orientation_w"] = (object) quat3[3];
            this.newSectorObjectsRow["name"] = (object) this.sgp.Name.Replace("'", "''");
            this.newSectorObjectsRow["appears_in_radar"] = (object) this.sgp.AppearsInRadar;
            this.newSectorObjectsRow["radar_range"] = (object) this.sgp.RadarRange;
            this.newSectorObjectsRow["gate_to"] = (object) this.sgp.Destination;
            this.newSectorObjectsRow["classSpecific"] = (object) this.sgp.IsClassSpecific;
            for (int index = 0; index < HE_GlobalVars._ListofSecuritys.Length; ++index)
            {
              if (HE_GlobalVars._ListofSecuritys[index] == this.sgp.MinSecurityLevel)
                this.newSectorObjectsRow["minSecurityLevel"] = (object) (index * 10);
            }
            this.newSectorObjectsRow["sound_effect_id"] = (object) this.sgp.SoundEffect;
            this.newSectorObjectsRow["sound_effect_range"] = (object) this.sgp.SoundEffectRange;
            this.newSectorObjectsRow["faction_id"] = (object) mainFrm.factions.findIDbyName(this.sgp.FactionID);
            break;
          case 12:
            this.newSectorObjectsRow["sector_id"] = (object) this.sbp.SectorID;
            this.newSectorObjectsRow["nav_type"] = (object) this.sbp.NavType;
            this.newSectorObjectsRow["signature"] = (object) this.sbp.Signature;
            this.newSectorObjectsRow["is_huge"] = (object) this.sbp.IsHuge;
            this.newSectorObjectsRow["base_xp"] = (object) this.sbp.BaseXP;
            this.newSectorObjectsRow["exploration_range"] = (object) this.sbp.ExplorationRange;
            this.newSectorObjectsRow["base_asset_id"] = (object) this.sbp.BaseAssetID;
            this.newSectorObjectsRow["h"] = (object) this.sbp.Color.GetHue();
            this.newSectorObjectsRow["s"] = (object) this.sbp.Color.GetSaturation();
            this.newSectorObjectsRow["v"] = (object) this.sbp.Color.GetBrightness();
            this.newSectorObjectsRow["type"] = (object) 12;
            this.newSectorObjectsRow["scale"] = (object) this.sbp.Scale;
            this.newSectorObjectsRow["position_x"] = (object) this.sbp.PositionX;
            this.newSectorObjectsRow["position_y"] = (object) this.sbp.PositionY;
            this.newSectorObjectsRow["position_z"] = (object) this.sbp.PositionZ;
            double[] quat4 = quaternionCalc.AngleToQuat(this.sbp.Orientation_Yaw, this.sbp.Orientation_Pitch, this.sbp.Orientation_Roll);
            this.newSectorObjectsRow["orientation_z"] = (object) quat4[0];
            this.newSectorObjectsRow["orientation_u"] = (object) quat4[1];
            this.newSectorObjectsRow["orientation_v"] = (object) quat4[2];
            this.newSectorObjectsRow["orientation_w"] = (object) quat4[3];
            this.newSectorObjectsRow["name"] = (object) this.sbp.Name.Replace("'", "''");
            this.newSectorObjectsRow["appears_in_radar"] = (object) this.sbp.AppearsInRadar;
            this.newSectorObjectsRow["radar_range"] = (object) this.sbp.RadarRange;
            this.newSectorObjectsRow["gate_to"] = (object) this.sbp.Destination;
            this.newSectorObjectsRow["sound_effect_id"] = (object) this.sbp.SoundEffect;
            this.newSectorObjectsRow["sound_effect_range"] = (object) this.sbp.SoundEffectRange;
            this.newSectorObjectsRow["capShip"] = (object) this.sbp.IsCapShip;
            this.newSectorObjectsRow["dockable"] = (object) this.sbp.IsDockable;
            break;
          case 37:
            this.newSectorObjectsRow["sector_id"] = (object) this.bp.SectorID;
            this.newSectorObjectsRow["nav_type"] = (object) this.bp.NavType;
            this.newSectorObjectsRow["signature"] = (object) this.bp.Signature;
            this.newSectorObjectsRow["is_huge"] = (object) this.bp.IsHuge;
            this.newSectorObjectsRow["base_xp"] = (object) this.bp.BaseXP;
            this.newSectorObjectsRow["exploration_range"] = (object) this.bp.ExplorationRange;
            this.newSectorObjectsRow["base_asset_id"] = (object) this.bp.BaseAssetID;
            this.newSectorObjectsRow["h"] = (object) this.bp.Color.GetHue();
            this.newSectorObjectsRow["s"] = (object) this.bp.Color.GetSaturation();
            this.newSectorObjectsRow["v"] = (object) this.bp.Color.GetBrightness();
            this.newSectorObjectsRow["type"] = (object) 37;
            this.newSectorObjectsRow["scale"] = (object) this.bp.Scale;
            this.newSectorObjectsRow["position_x"] = (object) this.bp.PositionX;
            this.newSectorObjectsRow["position_y"] = (object) this.bp.PositionY;
            this.newSectorObjectsRow["position_z"] = (object) this.bp.PositionZ;
            double[] quat5 = quaternionCalc.AngleToQuat(this.bp.Orientation_Yaw, this.bp.Orientation_Pitch, this.bp.Orientation_Roll);
            this.newSectorObjectsRow["orientation_z"] = (object) quat5[0];
            this.newSectorObjectsRow["orientation_u"] = (object) quat5[1];
            this.newSectorObjectsRow["orientation_v"] = (object) quat5[2];
            this.newSectorObjectsRow["orientation_w"] = (object) quat5[3];
            this.newSectorObjectsRow["name"] = (object) this.bp.Name.Replace("'", "''");
            this.newSectorObjectsRow["appears_in_radar"] = (object) this.bp.AppearsInRadar;
            this.newSectorObjectsRow["radar_range"] = (object) this.bp.RadarRange;
            this.newSectorObjectsRow["gate_to"] = (object) this.bp.Destination;
            this.newSectorObjectsRow["sound_effect_id"] = (object) this.bp.SoundEffect;
            this.newSectorObjectsRow["sound_effect_range"] = (object) this.bp.SoundEffectRange;
            break;
          case 38:
            this.newSectorObjectsRow["sector_id"] = (object) this.hp.SectorID;
            this.newSectorObjectsRow["nav_type"] = (object) this.hp.NavType;
            this.newSectorObjectsRow["signature"] = (object) this.hp.Signature;
            this.newSectorObjectsRow["is_huge"] = (object) this.hp.IsHuge;
            this.newSectorObjectsRow["base_xp"] = (object) this.hp.BaseXP;
            this.newSectorObjectsRow["exploration_range"] = (object) this.hp.ExplorationRange;
            this.newSectorObjectsRow["base_asset_id"] = (object) this.hp.BaseAssetID;
            this.newSectorObjectsRow["h"] = (object) this.hp.Color.GetHue();
            this.newSectorObjectsRow["s"] = (object) this.hp.Color.GetSaturation();
            this.newSectorObjectsRow["v"] = (object) this.hp.Color.GetBrightness();
            this.newSectorObjectsRow["type"] = (object) 38;
            this.newSectorObjectsRow["scale"] = (object) this.hp.Scale;
            this.newSectorObjectsRow["position_x"] = (object) this.hp.PositionX;
            this.newSectorObjectsRow["position_y"] = (object) this.hp.PositionY;
            this.newSectorObjectsRow["position_z"] = (object) this.hp.PositionZ;
            double[] quat6 = quaternionCalc.AngleToQuat(this.hp.Orientation_Yaw, this.hp.Orientation_Pitch, this.hp.Orientation_Roll);
            this.newSectorObjectsRow["orientation_z"] = (object) quat6[0];
            this.newSectorObjectsRow["orientation_u"] = (object) quat6[1];
            this.newSectorObjectsRow["orientation_v"] = (object) quat6[2];
            this.newSectorObjectsRow["orientation_w"] = (object) quat6[3];
            this.newSectorObjectsRow["name"] = (object) this.hp.Name.Replace("'", "''");
            this.newSectorObjectsRow["appears_in_radar"] = (object) this.hp.AppearsInRadar;
            this.newSectorObjectsRow["radar_range"] = (object) this.hp.RadarRange;
            this.newSectorObjectsRow["gate_to"] = (object) this.hp.Destination;
            this.newSectorObjectsRow["sound_effect_id"] = (object) this.hp.SoundEffect;
            this.newSectorObjectsRow["sound_effect_range"] = (object) this.hp.SoundEffectRange;
            this.newSectorObjectsRow["level"] = (object) this.hp.Level;
            this.newSectorObjectsRow["res_count"] = (object) this.hp.ResCount;
            this.newSectorObjectsRow["spawn_radius"] = (object) this.hp.MobSpawnRadius;
            this.newSectorObjectsRow["pop_rock_chance"] = (object) this.hp.PopRockChance;
            this.newSectorObjectsRow["max_field_radius"] = (object) this.hp.MaxFieldRadius;
            this.newSectorObjectsRow["respawn_timer"] = (object) this.hp.RespawnTimer;
            if (this.hp.Field == "Random")
            {
              this.newSectorObjectsRow["field"] = (object) 0;
              break;
            }
            if (this.hp.Field == "Ring")
            {
              this.newSectorObjectsRow["field"] = (object) 1;
              break;
            }
            if (this.hp.Field == "Donut")
            {
              this.newSectorObjectsRow["field"] = (object) 2;
              break;
            }
            if (this.hp.Field == "Cylinder")
            {
              this.newSectorObjectsRow["field"] = (object) 3;
              break;
            }
            if (this.hp.Field == "Sphere")
            {
              this.newSectorObjectsRow["field"] = (object) 4;
              break;
            }
            if (this.hp.Field == "Gas Cloud Clump")
            {
              this.newSectorObjectsRow["field"] = (object) 5;
              break;
            }
            break;
          case 40:
          case 41:
            this.newSectorObjectsRow["sector_id"] = (object) this.bp.SectorID;
            this.newSectorObjectsRow["nav_type"] = (object) this.bp.NavType;
            this.newSectorObjectsRow["signature"] = (object) this.bp.Signature;
            this.newSectorObjectsRow["is_huge"] = (object) this.bp.IsHuge;
            this.newSectorObjectsRow["base_xp"] = (object) this.bp.BaseXP;
            this.newSectorObjectsRow["exploration_range"] = (object) this.bp.ExplorationRange;
            this.newSectorObjectsRow["base_asset_id"] = (object) this.bp.BaseAssetID;
            this.newSectorObjectsRow["h"] = (object) this.bp.Color.GetHue();
            this.newSectorObjectsRow["s"] = (object) this.bp.Color.GetSaturation();
            this.newSectorObjectsRow["v"] = (object) this.bp.Color.GetBrightness();
            this.newSectorObjectsRow["type"] = (object) this.type2;
            this.newSectorObjectsRow["scale"] = (object) this.bp.Scale;
            this.newSectorObjectsRow["position_x"] = (object) this.bp.PositionX;
            this.newSectorObjectsRow["position_y"] = (object) this.bp.PositionY;
            this.newSectorObjectsRow["position_z"] = (object) this.bp.PositionZ;
            double[] quat7 = quaternionCalc.AngleToQuat(this.bp.Orientation_Yaw, this.bp.Orientation_Pitch, this.bp.Orientation_Roll);
            this.newSectorObjectsRow["orientation_z"] = (object) quat7[0];
            this.newSectorObjectsRow["orientation_u"] = (object) quat7[1];
            this.newSectorObjectsRow["orientation_v"] = (object) quat7[2];
            this.newSectorObjectsRow["orientation_w"] = (object) quat7[3];
            this.newSectorObjectsRow["name"] = (object) this.bp.Name.Replace("'", "''");
            this.newSectorObjectsRow["appears_in_radar"] = (object) this.bp.AppearsInRadar;
            this.newSectorObjectsRow["radar_range"] = (object) this.bp.RadarRange;
            this.newSectorObjectsRow["gate_to"] = (object) this.bp.Destination;
            this.newSectorObjectsRow["sound_effect_id"] = (object) this.bp.SoundEffect;
            this.newSectorObjectsRow["sound_effect_range"] = (object) this.bp.SoundEffectRange;
            break;
        }
        int num1 = int.Parse(this.newSectorObjectsRow["sound_effect_id"].ToString());
        Console.Out.WriteLine(num1);
        float num2 = (float) int.Parse(this.newSectorObjectsRow["sound_effect_range"].ToString());
        if (num1 != -1 && (double) num2 == 0.0)
          this.newSectorObjectsRow["sound_effect_range"] = (object) 30000;
        sectorObject.Rows.Add(this.newSectorObjectsRow);
        this.sectorObjectsSQL.newRow(this.newSectorObjectsRow);
        switch (this.type2)
        {
          case 0:
            if (!this.validateMobs(0))
              break;
            mainFrm.sectorWindow.addNewObject(this.type2, this.newSectorObjectsRow);
            this.Close();
            break;
          case 3:
            mainFrm.sectorWindow.addNewObject(this.type2, this.newSectorObjectsRow);
            this.Close();
            break;
          case 11:
            mainFrm.sectorWindow.addNewObject(this.type2, this.newSectorObjectsRow);
            this.Close();
            break;
          case 12:
            mainFrm.sectorWindow.addNewObject(this.type2, this.newSectorObjectsRow);
            this.Close();
            break;
          case 37:
            mainFrm.sectorWindow.addNewObject(this.type2, this.newSectorObjectsRow);
            this.Close();
            break;
          case 38:
            if (!this.validateHarvestables(0))
              break;
            mainFrm.sectorWindow.addNewObject(this.type2, this.newSectorObjectsRow);
            this.Close();
            break;
          case 40:
          case 41:
            mainFrm.sectorWindow.addNewObject(this.type2, this.newSectorObjectsRow);
            this.Close();
            break;
        }
      }
      else if (this.type2 == 0)
      {
        if (!this.validateMobs(this.lastInsertID))
          return;
        mainFrm.sectorWindow.addNewObject(this.type2, this.newSectorObjectsRow);
        this.Close();
      }
      else
      {
        if (this.type2 != 38 || !this.validateHarvestables(this.lastInsertID))
          return;
        mainFrm.sectorWindow.addNewObject(this.type2, this.newSectorObjectsRow);
        this.Close();
      }
    }

    private bool validateMobs(int id)
    {
      this.lastInsertID = id;
      if (this.lastInsertID == 0)
      {
        foreach (DataRow row in (InternalDataCollectionBase) Database.executeQuery(Database.DatabaseName.net7, "SELECT LAST_INSERT_ID()").Rows)
        {
          this.lastInsertID = int.Parse(row["LAST_INSERT_ID()"].ToString());
          mainFrm.selectedObjectID = this.lastInsertID;
        }
      }
      if (Database.executeQuery(Database.DatabaseName.net7, "SELECT * FROM mob_spawn_group where spawn_group_id='" + (object) this.lastInsertID + "';").Rows.Count == 0)
      {
        int num = (int) MessageBox.Show("You have no mobs in your spawn group, to save this record, add some mobs.", "Mob Verification", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        return false;
      }
      if (this.type2 != 0 || this.mp.Count != 0)
        return true;
      int num1 = (int) MessageBox.Show("Your Maximum spawn count must be > 0 when you have mobs in your group ", "Mob Count Verification", MessageBoxButtons.OK, MessageBoxIcon.Hand);
      return false;
    }

    private bool validateHarvestables(int id)
    {
      this.lastInsertID = id;
      if (this.lastInsertID == 0)
      {
        foreach (DataRow row in (InternalDataCollectionBase) Database.executeQuery(Database.DatabaseName.net7, "SELECT LAST_INSERT_ID()").Rows)
        {
          this.lastInsertID = int.Parse(row["LAST_INSERT_ID()"].ToString());
          mainFrm.selectedObjectID = this.lastInsertID;
        }
      }
      if (Database.executeQuery(Database.DatabaseName.net7, "SELECT * FROM sector_objects_harvestable_restypes where group_id='" + (object) this.lastInsertID + "';").Rows.Count == 0)
      {
        int num = (int) MessageBox.Show("You have no resource types in your field, to save this record, add at least 1 restype.", "Res Type Verification", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        return false;
      }
      if ((double) this.hp.MaxFieldRadius == 0.0)
      {
        int num = (int) MessageBox.Show("Your Maximum field radius must be > 0.", "Mob Count Verification", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        return false;
      }
      if ((double) this.hp.MobSpawnRadius > 0.0)
      {
        if (!this.validateMobs(this.lastInsertID))
          return false;
        if ((double) this.hp.MobSpawnRadius == 0.0)
        {
          int num = (int) MessageBox.Show("Since you have mobs guardians your Maximum spawn radius must be > 0.", "Mob Count Verification", MessageBoxButtons.OK, MessageBoxIcon.Hand);
          return false;
        }
      }
      return true;
    }

    public void setPosition(PointF position)
    {
      switch (this.type2)
      {
        case 0:
          this.mp.PositionX = position.X * 100f;
          this.mp.PositionY = (float) -((double) position.Y * 100.0);
          break;
        case 3:
          this.pp.PositionX = position.X * 100f;
          this.pp.PositionY = (float) -((double) position.Y * 100.0);
          break;
        case 11:
          this.sgp.PositionX = position.X * 100f;
          this.sgp.PositionY = (float) -((double) position.Y * 100.0);
          break;
        case 12:
          this.sbp.PositionX = position.X * 100f;
          this.sbp.PositionY = (float) -((double) position.Y * 100.0);
          break;
        case 37:
          this.bp.PositionX = position.X * 100f;
          this.bp.PositionY = (float) -((double) position.Y * 100.0);
          break;
        case 38:
          this.hp.PositionX = position.X * 100f;
          this.hp.PositionY = (float) -((double) position.Y * 100.0);
          break;
        case 40:
        case 41:
          this.bp.PositionX = position.X * 100f;
          this.bp.PositionY = (float) -((double) position.Y * 100.0);
          break;
      }
    }

    private void button3_Click(object sender, EventArgs e)
    {
      this.Hide();
      this._s1.newSectorObject(this);
    }
  }
}
