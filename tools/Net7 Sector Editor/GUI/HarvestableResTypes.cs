﻿// Decompiled with JetBrains decompiler
// Type: N7.GUI.HarvestableResTypes
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace N7.GUI
{
  public class HarvestableResTypes : Form
  {
    public IWindowsFormsEditorService _wfes;
    private int _id;
    private DataTable loadTypes;
    private IContainer components;
    private ListBox listBox1;
    private Button button2;
    private Button button3;
    private ListBox listBox2;
    private Button button1;
    private Button button4;
    private Label label1;
    private Label label2;

    public HarvestableResTypes()
    {
      DataTable dataTable1 = Database.executeQuery(Database.DatabaseName.net7, "SELECT Auto_increment FROM information_schema.tables WHERE table_name='sector_objects' and table_schema='net7';");
      int num1 = 0;
      foreach (DataRow row in (InternalDataCollectionBase) dataTable1.Rows)
        num1 = int.Parse(row["Auto_increment"].ToString());
      DataTable dataTable2 = Database.executeQuery(Database.DatabaseName.net7, "SELECT sector_object_id FROM sector_objects where sector_object_id='" + (object) mainFrm.selectedObjectID + "';");
      int num2 = 0;
      foreach (DataRow row in (InternalDataCollectionBase) dataTable2.Rows)
        num2 = int.Parse(row["sector_object_id"].ToString());
      this._id = num2 == 0 ? num1 : mainFrm.selectedObjectID;
      this.InitializeComponent();
    }

    private void HarvestableResTypes_Load(object sender, EventArgs e)
    {
      this.loadTypes = Database.executeQuery(Database.DatabaseName.net7, "SELECT * FROM sector_objects_harvestable_restypes where group_id='" + (object) this._id + "';");
      foreach (DataRow row in (InternalDataCollectionBase) this.loadTypes.Rows)
      {
        switch (int.Parse(row["type"].ToString()))
        {
          case 1131:
            this.listBox1.Items.Remove((object) "Inorganic Hulk 01");
            this.listBox2.Items.Add((object) "Inorganic Hulk 01");
            continue;
          case 1132:
            this.listBox1.Items.Remove((object) "Organic Hulk 01");
            this.listBox2.Items.Add((object) "Organic Hulk 01");
            continue;
          case 1822:
            this.listBox1.Items.Remove((object) "Glowing Asteroid 1");
            this.listBox2.Items.Add((object) "Glowing Asteroid 1");
            continue;
          case 1823:
            this.listBox1.Items.Remove((object) "Glowing Asteroid 2");
            this.listBox2.Items.Add((object) "Glowing Asteroid 2");
            continue;
          case 1824:
            this.listBox1.Items.Remove((object) "Glowing Asteroid 3");
            this.listBox2.Items.Add((object) "Glowing Asteroid 3");
            continue;
          case 1825:
            this.listBox1.Items.Remove((object) "Asteroid 1");
            this.listBox2.Items.Add((object) "Asteroid 1");
            continue;
          case 1826:
            this.listBox1.Items.Remove((object) "Asteroid 2");
            this.listBox2.Items.Add((object) "Asteroid 2");
            continue;
          case 1827:
            this.listBox1.Items.Remove((object) "Asteroid 3");
            this.listBox2.Items.Add((object) "Asteroid 3");
            continue;
          case 1828:
            this.listBox1.Items.Remove((object) "Hydrocarbon Deposit 1");
            this.listBox2.Items.Add((object) "Hydrocarbon Deposit 1");
            continue;
          case 1829:
            this.listBox1.Items.Remove((object) "Hydrocarbon Deposit 2");
            this.listBox2.Items.Add((object) "Hydrocarbon Deposit 2");
            continue;
          case 1830:
            this.listBox1.Items.Remove((object) "Hydrocarbon Deposit 3");
            this.listBox2.Items.Add((object) "Hydrocarbon Deposit 3");
            continue;
          case 1831:
            this.listBox1.Items.Remove((object) "Crystalline Asteroid 1");
            this.listBox2.Items.Add((object) "Crystalline Asteroid 1");
            continue;
          case 1832:
            this.listBox1.Items.Remove((object) "Crystalline Asteroid 2");
            this.listBox2.Items.Add((object) "Crystalline Asteroid 2");
            continue;
          case 1833:
            this.listBox1.Items.Remove((object) "Crystalline Asteroid 3");
            this.listBox2.Items.Add((object) "Crystalline Asteroid 3");
            continue;
          case 1834:
            this.listBox1.Items.Remove((object) "Gas Cloud");
            this.listBox2.Items.Add((object) "Gas Cloud");
            continue;
          default:
            continue;
        }
      }
    }

    private void button2_Click(object sender, EventArgs e)
    {
      string str = this.listBox1.SelectedItem.ToString();
      int num = 1823;
      if (str == "Glowing Asteroid 1")
        num = 1822;
      else if (str == "Glowing Asteroid 2")
        num = 1823;
      else if (str == "Glowing Asteroid 3")
        num = 1824;
      else if (str == "Asteroid 1")
        num = 1825;
      else if (str == "Asteroid 2")
        num = 1826;
      else if (str == "Asteroid 3")
        num = 1827;
      else if (str == "Hydrocarbon Deposit 1")
        num = 1828;
      else if (str == "Hydrocarbon Deposit 2")
        num = 1829;
      else if (str == "Hydrocarbon Deposit 3")
        num = 1830;
      else if (str == "Crystalline Asteroid 1")
        num = 1831;
      else if (str == "Crystalline Asteroid 2")
        num = 1832;
      else if (str == "Crystalline Asteroid 3")
        num = 1833;
      else if (str == "Gas Cloud")
        num = 1834;
      else if (str == "Inorganic Hulk 01")
        num = 1131;
      else if (str == "Organic Hulk 01")
        num = 1132;
      this.listBox2.Items.Add((object) str);
      this.listBox1.Items.Remove((object) str);
      Database.executeQuery(Database.DatabaseName.net7, "INSERT INTO sector_objects_harvestable_restypes SET group_id='" + (object) this._id + "', type='" + (object) num + "';");
    }

    private void button3_Click(object sender, EventArgs e)
    {
      string str = this.listBox2.SelectedItem.ToString();
      int num = 1823;
      if (str == "Glowing Asteroid 1")
        num = 1822;
      else if (str == "Glowing Asteroid 2")
        num = 1823;
      else if (str == "Glowing Asteroid 3")
        num = 1824;
      else if (str == "Asteroid 1")
        num = 1825;
      else if (str == "Asteroid 2")
        num = 1826;
      else if (str == "Asteroid 3")
        num = 1827;
      else if (str == "Hydrocarbon Deposit 1")
        num = 1828;
      else if (str == "Hydrocarbon Deposit 2")
        num = 1829;
      else if (str == "Hydrocarbon Deposit 3")
        num = 1830;
      else if (str == "Crystalline Asteroid 1")
        num = 1831;
      else if (str == "Crystalline Asteroid 2")
        num = 1832;
      else if (str == "Crystalline Asteroid 3")
        num = 1833;
      else if (str == "Gas Cloud")
        num = 1834;
      else if (str == "Inorganic Hulk 01")
        num = 1131;
      else if (str == "Organic Hulk 01")
        num = 1132;
      this.listBox1.Items.Add((object) str);
      this.listBox2.Items.Remove((object) str);
      Database.executeQuery(Database.DatabaseName.net7, "DELETE FROM sector_objects_harvestable_restypes where group_id='" + (object) this._id + "' and type='" + (object) num + "';");
    }

    private void button4_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void button1_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (HarvestableResTypes));
      this.listBox1 = new ListBox();
      this.button2 = new Button();
      this.button3 = new Button();
      this.listBox2 = new ListBox();
      this.button1 = new Button();
      this.button4 = new Button();
      this.label1 = new Label();
      this.label2 = new Label();
      this.SuspendLayout();
      this.listBox1.FormattingEnabled = true;
      this.listBox1.Items.AddRange(new object[15]
      {
        (object) "Glowing Asteroid 1",
        (object) "Glowing Asteroid 2 ",
        (object) "Glowing Asteroid 3",
        (object) "Asteroid 1",
        (object) "Asteroid 2",
        (object) "Asteroid 3",
        (object) "Hydrocarbon Deposit 1",
        (object) "Hydrocarbon Deposit 2",
        (object) "Hydrocarbon Deposit 3",
        (object) "Crystalline Asteroid 1",
        (object) "Crystalline Asteroid 2",
        (object) "Crystalline Asteroid 3",
        (object) "Gas Cloud",
        (object) "Inorganic Hulk 01",
        (object) "Organic Hulk 01"
      });
      this.listBox1.Location = new Point(12, 25);
      this.listBox1.Name = "listBox1";
      this.listBox1.Size = new Size(200, 199);
      this.listBox1.TabIndex = 0;
      this.button2.Location = new Point(218, 25);
      this.button2.Name = "button2";
      this.button2.Size = new Size(75, 23);
      this.button2.TabIndex = 2;
      this.button2.Text = "Add ->";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new EventHandler(this.button2_Click);
      this.button3.Location = new Point(218, 54);
      this.button3.Name = "button3";
      this.button3.Size = new Size(75, 23);
      this.button3.TabIndex = 3;
      this.button3.Text = "<- Remove";
      this.button3.UseVisualStyleBackColor = true;
      this.button3.Click += new EventHandler(this.button3_Click);
      this.listBox2.FormattingEnabled = true;
      this.listBox2.Location = new Point(299, 25);
      this.listBox2.Name = "listBox2";
      this.listBox2.Size = new Size(200, 199);
      this.listBox2.TabIndex = 4;
      this.button1.Location = new Point(423, 230);
      this.button1.Name = "button1";
      this.button1.Size = new Size(75, 23);
      this.button1.TabIndex = 5;
      this.button1.Text = "Finish";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new EventHandler(this.button1_Click);
      this.button4.Location = new Point(12, 230);
      this.button4.Name = "button4";
      this.button4.Size = new Size(75, 23);
      this.button4.TabIndex = 6;
      this.button4.Text = "Cancel";
      this.button4.UseVisualStyleBackColor = true;
      this.button4.Click += new EventHandler(this.button4_Click);
      this.label1.AutoSize = true;
      this.label1.Location = new Point(12, 9);
      this.label1.Name = "label1";
      this.label1.Size = new Size(85, 13);
      this.label1.TabIndex = 7;
      this.label1.Text = "Available Types:";
      this.label2.AutoSize = true;
      this.label2.Location = new Point(296, 9);
      this.label2.Name = "label2";
      this.label2.Size = new Size(84, 13);
      this.label2.TabIndex = 8;
      this.label2.Text = "Selected Types:";
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(512, 260);
      this.Controls.Add((Control) this.label2);
      this.Controls.Add((Control) this.label1);
      this.Controls.Add((Control) this.button4);
      this.Controls.Add((Control) this.button1);
      this.Controls.Add((Control) this.listBox2);
      this.Controls.Add((Control) this.button3);
      this.Controls.Add((Control) this.button2);
      this.Controls.Add((Control) this.listBox1);
      this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = nameof (HarvestableResTypes);
      this.SizeGripStyle = SizeGripStyle.Hide;
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = nameof (HarvestableResTypes);
      this.TopMost = true;
      this.Load += new EventHandler(this.HarvestableResTypes_Load);
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
