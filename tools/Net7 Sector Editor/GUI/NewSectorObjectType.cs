﻿// Decompiled with JetBrains decompiler
// Type: N7.GUI.NewSectorObjectType
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace N7.GUI
{
  public class NewSectorObjectType : Form
  {
    private TreeView tree;
    private SectorWindow secWin;
    private IContainer components;
    private ComboBox comboBox1;
    private Label label1;
    private Button button1;
    private Button button2;

    public NewSectorObjectType(TreeView t1, SectorWindow s1)
    {
      this.tree = t1;
      this.secWin = s1;
      this.InitializeComponent();
    }

    private void button1_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void button2_Click(object sender, EventArgs e)
    {
      this.Hide();
      if (this.tree.SelectedNode == null)
      {
        int num1 = (int) MessageBox.Show("You Cannot add a Sector Object without first \n having a sector selected. Please select a sector and try again!");
      }
      else if (this.tree.SelectedNode.Parent != null)
      {
        int num2 = (int) new NewSectorObject(this.comboBox1.SelectedItem.ToString(), this.tree, this.secWin).ShowDialog();
      }
      else
      {
        int num3 = (int) MessageBox.Show("You Cannot add a Sector Object without first \n having a sector selected. Please select a sector and try again!");
      }
    }

    private void NewSectorObjectType_Load(object sender, EventArgs e)
    {
      this.comboBox1.Items.Add((object) "Mobs (Type 0)");
      this.comboBox1.Items.Add((object) "Planets (Type 3)");
      this.comboBox1.Items.Add((object) "Stargates (Type 11)");
      this.comboBox1.Items.Add((object) "Starbases (Type 12)");
      this.comboBox1.Items.Add((object) "Decorations (Type 37)");
      this.comboBox1.Items.Add((object) "Harvestables (Type 38)");
      this.comboBox1.Items.Add((object) "Radation  (Type 40)");
      this.comboBox1.Items.Add((object) "Gravity Well (Type 41)");
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (NewSectorObjectType));
      this.comboBox1 = new ComboBox();
      this.label1 = new Label();
      this.button1 = new Button();
      this.button2 = new Button();
      this.SuspendLayout();
      this.comboBox1.FormattingEnabled = true;
      this.comboBox1.Location = new Point(15, 25);
      this.comboBox1.Name = "comboBox1";
      this.comboBox1.Size = new Size(202, 21);
      this.comboBox1.TabIndex = 0;
      this.label1.AutoSize = true;
      this.label1.Location = new Point(12, 9);
      this.label1.Name = "label1";
      this.label1.Size = new Size(91, 13);
      this.label1.TabIndex = 1;
      this.label1.Text = "Select the Type...";
      this.button1.Location = new Point(15, 64);
      this.button1.Name = "button1";
      this.button1.Size = new Size(75, 23);
      this.button1.TabIndex = 2;
      this.button1.Text = "Cancel";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new EventHandler(this.button1_Click);
      this.button2.Location = new Point(142, 64);
      this.button2.Name = "button2";
      this.button2.Size = new Size(75, 23);
      this.button2.TabIndex = 3;
      this.button2.Text = "Next...";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new EventHandler(this.button2_Click);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(229, 94);
      this.Controls.Add((Control) this.button2);
      this.Controls.Add((Control) this.button1);
      this.Controls.Add((Control) this.label1);
      this.Controls.Add((Control) this.comboBox1);
      this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = nameof (NewSectorObjectType);
      this.SizeGripStyle = SizeGripStyle.Hide;
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "New Sector Object";
      this.Load += new EventHandler(this.NewSectorObjectType_Load);
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
