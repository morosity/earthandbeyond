﻿// Decompiled with JetBrains decompiler
// Type: N7.GUI.ObjectList
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace N7.GUI
{
  public class ObjectList : UserControl
  {
    private IContainer components;
    public DataGridView dataGridView1;
    private DataGridViewTextBoxColumn id;
    private DataGridViewTextBoxColumn name;
    private DataGridViewTextBoxColumn base_asset_id;
    private DataGridViewTextBoxColumn type;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      DataGridViewCellStyle gridViewCellStyle = new DataGridViewCellStyle();
      this.dataGridView1 = new DataGridView();
      this.id = new DataGridViewTextBoxColumn();
      this.name = new DataGridViewTextBoxColumn();
      this.base_asset_id = new DataGridViewTextBoxColumn();
      this.type = new DataGridViewTextBoxColumn();
      ((ISupportInitialize) this.dataGridView1).BeginInit();
      this.SuspendLayout();
      this.dataGridView1.AllowUserToAddRows = false;
      this.dataGridView1.AllowUserToDeleteRows = false;
      this.dataGridView1.AllowUserToOrderColumns = true;
      this.dataGridView1.AllowUserToResizeRows = false;
      gridViewCellStyle.BackColor = Color.FromArgb(224, 224, 224);
      this.dataGridView1.AlternatingRowsDefaultCellStyle = gridViewCellStyle;
      this.dataGridView1.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dataGridView1.Columns.AddRange((DataGridViewColumn) this.id, (DataGridViewColumn) this.name, (DataGridViewColumn) this.base_asset_id, (DataGridViewColumn) this.type);
      this.dataGridView1.Location = new Point(0, 0);
      this.dataGridView1.MultiSelect = false;
      this.dataGridView1.Name = "dataGridView1";
      this.dataGridView1.ReadOnly = true;
      this.dataGridView1.RowHeadersVisible = false;
      this.dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
      this.dataGridView1.Size = new Size(750, 150);
      this.dataGridView1.TabIndex = 1;
      this.id.AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
      this.id.HeaderText = "Object ID";
      this.id.Name = "id";
      this.id.ReadOnly = true;
      this.id.Width = 77;
      this.name.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
      this.name.HeaderText = "Name";
      this.name.Name = "name";
      this.name.ReadOnly = true;
      this.base_asset_id.AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
      this.base_asset_id.HeaderText = "Asset ID";
      this.base_asset_id.Name = "base_asset_id";
      this.base_asset_id.ReadOnly = true;
      this.base_asset_id.Width = 72;
      this.type.AutoSizeMode = DataGridViewAutoSizeColumnMode.ColumnHeader;
      this.type.HeaderText = "Type";
      this.type.Name = "type";
      this.type.ReadOnly = true;
      this.type.Width = 56;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.Controls.Add((Control) this.dataGridView1);
      this.Name = nameof (ObjectList);
      this.Size = new Size(750, 150);
      ((ISupportInitialize) this.dataGridView1).EndInit();
      this.ResumeLayout(false);
    }

    public ObjectList()
    {
      this.InitializeComponent();
    }
  }
}
