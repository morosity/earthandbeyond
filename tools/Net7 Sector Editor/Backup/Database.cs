﻿// Decompiled with JetBrains decompiler
// Type: N7.Database
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using MySql.Data.MySqlClient;
using System.Data;

namespace N7
{
  internal static class Database
  {
    public static DataTable executeQuery(Database.DatabaseName databaseName, string query)
    {
      DataTable dataTable = new DataTable();
      MySqlConnection connection = new MySqlConnection(SQLData.ConnStr(databaseName.ToString()));
      connection.Open();
      new MySqlDataAdapter(query, connection).Fill(dataTable);
      connection.Close();
      return dataTable;
    }

    public enum DatabaseName
    {
      net7,
      net7_db,
    }
  }
}
