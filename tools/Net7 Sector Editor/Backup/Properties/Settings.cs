﻿// Decompiled with JetBrains decompiler
// Type: N7.Properties.Settings
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace N7.Properties
{
  [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "9.0.0.0")]
  [CompilerGenerated]
  internal sealed class Settings : ApplicationSettingsBase
  {
    private static Settings defaultInstance = (Settings) SettingsBase.Synchronized((SettingsBase) new Settings());

    private void SettingChangingEventHandler(object sender, SettingChangingEventArgs e)
    {
    }

    private void SettingsSavingEventHandler(object sender, CancelEventArgs e)
    {
    }

    public static Settings Default
    {
      get
      {
        return Settings.defaultInstance;
      }
    }

    [UserScopedSetting]
    [DefaultSettingValue("False")]
    [DebuggerNonUserCode]
    public bool KeepVisualizationSetting
    {
      get
      {
        return (bool) this[nameof (KeepVisualizationSetting)];
      }
      set
      {
        this[nameof (KeepVisualizationSetting)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("0")]
    public int zoomSelection
    {
      get
      {
        return (int) this[nameof (zoomSelection)];
      }
      set
      {
        this[nameof (zoomSelection)] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    [DefaultSettingValue("False")]
    [UserScopedSetting]
    public bool MobsLayer
    {
      get
      {
        return (bool) this[nameof (MobsLayer)];
      }
      set
      {
        this[nameof (MobsLayer)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DefaultSettingValue("False")]
    [DebuggerNonUserCode]
    public bool PlanetsLayer
    {
      get
      {
        return (bool) this[nameof (PlanetsLayer)];
      }
      set
      {
        this[nameof (PlanetsLayer)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("False")]
    public bool StargatesLayer
    {
      get
      {
        return (bool) this[nameof (StargatesLayer)];
      }
      set
      {
        this[nameof (StargatesLayer)] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    [UserScopedSetting]
    [DefaultSettingValue("False")]
    public bool StarbasesLayer
    {
      get
      {
        return (bool) this[nameof (StarbasesLayer)];
      }
      set
      {
        this[nameof (StarbasesLayer)] = (object) value;
      }
    }

    [DebuggerNonUserCode]
    [UserScopedSetting]
    [DefaultSettingValue("False")]
    public bool DecorationsLayer
    {
      get
      {
        return (bool) this[nameof (DecorationsLayer)];
      }
      set
      {
        this[nameof (DecorationsLayer)] = (object) value;
      }
    }

    [UserScopedSetting]
    [DebuggerNonUserCode]
    [DefaultSettingValue("False")]
    public bool HarvestablesLayer
    {
      get
      {
        return (bool) this[nameof (HarvestablesLayer)];
      }
      set
      {
        this[nameof (HarvestablesLayer)] = (object) value;
      }
    }

    [DefaultSettingValue("False")]
    [UserScopedSetting]
    [DebuggerNonUserCode]
    public bool Setting
    {
      get
      {
        return (bool) this[nameof (Setting)];
      }
      set
      {
        this[nameof (Setting)] = (object) value;
      }
    }
  }
}
