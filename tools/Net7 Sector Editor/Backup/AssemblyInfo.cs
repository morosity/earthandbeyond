﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTrademark("")]
[assembly: AssemblyFileVersion("1.2.2.0")]
[assembly: AssemblyCopyright("Copyright © enb-emulator.com 2009")]
[assembly: ComVisible(false)]
[assembly: Guid("c0698fcc-130d-4bee-9d7a-25b23f18b931")]
[assembly: AssemblyTitle("Net7 Sector Editor")]
[assembly: AssemblyDescription("The official net7 sector editor, for more information please visit www.enb-emulator.com")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Enb Emulator")]
[assembly: AssemblyProduct("Net7 Sector Editor")]
[assembly: AssemblyVersion("1.2.2.0")]
