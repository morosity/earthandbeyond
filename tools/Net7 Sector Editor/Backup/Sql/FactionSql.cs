﻿// Decompiled with JetBrains decompiler
// Type: N7.Sql.FactionSql
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using System;
using System.Data;

namespace N7.Sql
{
  public class FactionSql
  {
    private DataTable factions;

    public FactionSql()
    {
      this.factions = Database.executeQuery(Database.DatabaseName.net7, "SELECT * FROM factions;");
    }

    public string findNameByID(int factionID)
    {
      string str;
      if (factionID > 0)
      {
        Console.Out.WriteLine(factionID);
        str = this.factions.Select("faction_id = '" + (object) factionID + "'")[0]["name"].ToString();
      }
      else
        str = "None";
      return str;
    }

    public int findIDbyName(string name)
    {
      string str = name.Replace("'", "''");
      return !(name != "None") ? -1 : int.Parse(this.factions.Select("name LIKE '" + str + "'")[0]["faction_id"].ToString());
    }

    public DataTable getFactionTable()
    {
      return this.factions;
    }
  }
}
