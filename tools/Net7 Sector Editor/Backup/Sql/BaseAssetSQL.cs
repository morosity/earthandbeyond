﻿// Decompiled with JetBrains decompiler
// Type: N7.Sql.BaseAssetSQL
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using System.Data;

namespace N7.Sql
{
  public class BaseAssetSQL
  {
    private DataTable baseAssets;

    public BaseAssetSQL()
    {
      this.baseAssets = Database.executeQuery(Database.DatabaseName.net7, "SELECT * FROM assets;");
    }

    public DataTable getAssetsTable()
    {
      return this.baseAssets;
    }

    public DataRow[] getRowsbyCategory(string name)
    {
      return this.baseAssets.Select("main_cat LIKE '" + name.Replace("'", "''") + "'");
    }
  }
}
