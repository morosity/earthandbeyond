﻿// Decompiled with JetBrains decompiler
// Type: N7.Sql.SystemsSql
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using System;
using System.Data;

namespace N7.Sql
{
  public class SystemsSql
  {
    private DataTable systems;

    public SystemsSql()
    {
      this.systems = Database.executeQuery(Database.DatabaseName.net7, "Select * from systems");
    }

    public DataTable getSystemTable()
    {
      return this.systems;
    }

    public DataRow[] findRowsByName(string name)
    {
      return this.systems.Select("name Like '" + name + "'");
    }

    public string findRowNameByID(int id)
    {
      return this.systems.Select("system_id='" + (object) id + "'")[0]["name"].ToString();
    }

    public int getIDFromName(string name)
    {
      return int.Parse(this.systems.Select("name Like '" + name + "'")[0]["system_id"].ToString());
    }

    public void updateRow(DataRow r)
    {
      string str1 = r["name"].ToString().Replace("'", "''");
      string str2 = r["notes"].ToString().Replace("'", "''");
      string query = "UPDATE systems SET name='" + str1 + "', galaxy_x='" + r["galaxy_x"].ToString() + "', galaxy_y='" + r["galaxy_y"].ToString() + "', galaxy_z='" + r["galaxy_z"].ToString() + "', color_r='" + r["color_r"].ToString() + "', color_g='" + r["color_g"].ToString() + "', color_b='" + r["color_b"].ToString() + "', notes='" + str2 + "' where system_id='" + r["system_id"].ToString() + "';";
      try
      {
        Database.executeQuery(Database.DatabaseName.net7, query);
      }
      catch (Exception ex)
      {
        throw;
      }
    }

    public void newRow(DataRow nr)
    {
      string query = "INSERT INTO systems SET name='" + nr["name"].ToString() + "', notes='" + nr["notes"].ToString() + "', galaxy_x='" + nr["galaxy_x"].ToString() + "', galaxy_y='" + nr["galaxy_y"].ToString() + "', galaxy_z='" + nr["galaxy_z"].ToString() + "', color_r='" + nr["color_r"].ToString() + "', color_g='" + nr["color_g"].ToString() + "', color_b='" + nr["color_b"].ToString() + "';";
      int num = 0;
      try
      {
        Database.executeQuery(Database.DatabaseName.net7, query);
        foreach (DataRow row in (InternalDataCollectionBase) Database.executeQuery(Database.DatabaseName.net7, "SELECT LAST_INSERT_ID()").Rows)
          num = int.Parse(row["LAST_INSERT_ID()"].ToString());
      }
      catch (Exception ex)
      {
        throw;
      }
      nr["system_id"] = (object) num;
    }
  }
}
