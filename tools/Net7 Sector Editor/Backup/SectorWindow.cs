﻿// Decompiled with JetBrains decompiler
// Type: N7.SectorWindow
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using N7.GUI;
using N7.Props;
using N7.Sprites;
using N7.Sql;
using N7.Utilities;
using System;
using System.Collections;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using UMD.HCIL.Piccolo;
using UMD.HCIL.Piccolo.Event;

namespace N7
{
  public class SectorWindow
  {
    private Hashtable deletedObjectsID = new Hashtable();
    private Hashtable deletedObjectsType = new Hashtable();
    private PCanvas canvas;
    private PLayer boundsLayer;
    private PLayer masterLayer;
    private PLayer mobsLayer;
    private PLayer planetsLayer;
    private PLayer stargatesLayer;
    private PLayer starbasesLayer;
    private PLayer decorationsLayer;
    private PLayer harvestableLayer;
    private PLayer envirementLayer;
    private PNode pSelectedNode;
    private SectorObjectsSql so;
    private DataRow dr;
    private PropertyGrid _pg;
    private NewSectorObject _nso;
    private SectorProps sp;
    private DataGridView _dgv;

    public SectorWindow(PCanvas pcanvas, DataRow[] sectorRows, PropertyGrid pg, DataGridView dgv)
    {
      this.canvas = pcanvas;
      this.dr = sectorRows[0];
      this._pg = pg;
      this._dgv = dgv;
      if (N7.Properties.Settings.Default.zoomSelection == 0)
      {
        MouseWheelZoomController wheelZoomController = new MouseWheelZoomController(this.canvas.Camera);
      }
      this.canvas.BackColor = Color.Black;
      this.masterLayer = this.canvas.Layer;
      this.boundsLayer = new PLayer();
      this.mobsLayer = new PLayer();
      this.planetsLayer = new PLayer();
      this.stargatesLayer = new PLayer();
      this.starbasesLayer = new PLayer();
      this.decorationsLayer = new PLayer();
      this.harvestableLayer = new PLayer();
      this.envirementLayer = new PLayer();
      string sectorName = sectorRows[0]["name"].ToString();
      int num1 = int.Parse(sectorRows[0]["sector_id"].ToString());
      float x_min = float.Parse(sectorRows[0]["x_min"].ToString());
      float x_max = float.Parse(sectorRows[0]["x_max"].ToString());
      float y_min = float.Parse(sectorRows[0]["y_min"].ToString());
      float y_max = float.Parse(sectorRows[0]["y_max"].ToString());
      float num2 = float.Parse(sectorRows[0]["z_min"].ToString());
      float num3 = float.Parse(sectorRows[0]["z_max"].ToString());
      int num4 = int.Parse(sectorRows[0]["grid_x"].ToString());
      int num5 = int.Parse(sectorRows[0]["grid_y"].ToString());
      int num6 = int.Parse(sectorRows[0]["grid_z"].ToString());
      float num7 = float.Parse(sectorRows[0]["fog_near"].ToString());
      float num8 = float.Parse(sectorRows[0]["fog_far"].ToString());
      int num9 = int.Parse(sectorRows[0]["debris_mode"].ToString());
      bool flag1 = (bool) sectorRows[0]["light_backdrop"];
      bool flag2 = (bool) sectorRows[0]["fog_backdrop"];
      bool flag3 = (bool) sectorRows[0]["swap_backdrop"];
      float num10 = float.Parse(sectorRows[0]["backdrop_fog_near"].ToString());
      float num11 = float.Parse(sectorRows[0]["backdrop_fog_far"].ToString());
      float num12 = float.Parse(sectorRows[0]["max_tilt"].ToString());
      bool flag4 = (bool) sectorRows[0]["auto_level"];
      float num13 = float.Parse(sectorRows[0]["impulse_rate"].ToString());
      float num14 = float.Parse(sectorRows[0]["decay_velocity"].ToString());
      float num15 = float.Parse(sectorRows[0]["decay_spin"].ToString());
      int num16 = int.Parse(sectorRows[0]["backdrop_asset"].ToString());
      string str1 = sectorRows[0]["greetings"].ToString();
      string str2 = sectorRows[0]["notes"].ToString();
      int num17 = int.Parse(sectorRows[0]["system_id"].ToString());
      float num18 = float.Parse(sectorRows[0]["galaxy_x"].ToString());
      float num19 = float.Parse(sectorRows[0]["galaxy_y"].ToString());
      float num20 = float.Parse(sectorRows[0]["galaxy_z"].ToString());
      int num21 = int.Parse(sectorRows[0]["sector_type"].ToString());
      this.so = new SectorObjectsSql(sectorName);
      DataTable sectorObject = this.so.getSectorObject();
      float num22 = x_max - x_min;
      float num23 = y_max - y_min;
      float num24 = num3 - num2;
      this.sp = new SectorProps();
      this.sp.Name = sectorName;
      this.sp.SectorID = num1;
      this.sp.Width = num22;
      this.sp.Height = num23;
      this.sp.Depth = num24;
      this.sp.GridX = num4;
      this.sp.GridY = num5;
      this.sp.GridZ = num6;
      this.sp.FogNear = num7;
      this.sp.FogFar = num8;
      this.sp.DebrisMode = num9;
      this.sp.LightBackdrop = flag1;
      this.sp.FogBackdrop = flag2;
      this.sp.SwapBackdrop = flag3;
      this.sp.BackdropFogNear = num10;
      this.sp.BackdropFogFar = num11;
      this.sp.MaxTilt = num12;
      this.sp.AutoLevel = flag4;
      this.sp.ImpulseRate = num13;
      this.sp.DecayVelocity = num14;
      this.sp.DecaySpin = num15;
      this.sp.BackdropAsset = num16;
      this.sp.Greetings = str1;
      this.sp.Notes = str2;
      this.sp.SystemID = num17;
      this.sp.GalaxyX = num18;
      this.sp.GalaxyY = num19;
      this.sp.GalaxyZ = num20;
      string str3 = "";
      switch (num21)
      {
        case 0:
          str3 = "Space Sector";
          break;
        case 1:
          str3 = "Rocky Planet Surface";
          break;
        case 2:
          str3 = "Gas Giant Surface";
          break;
      }
      this.sp.SectorType = str3;
      pg.SelectedObject = (object) this.sp;
      SectorBoundsSprite sectorBoundsSprite = new SectorBoundsSprite(this.boundsLayer, x_min, y_min, x_max, y_max);
      foreach (DataRow row in (InternalDataCollectionBase) sectorObject.Rows)
      {
        switch (int.Parse(row["type"].ToString()))
        {
          case 0:
            MobSprite mobSprite = new MobSprite(this.mobsLayer, row, pg, dgv);
            continue;
          case 3:
            PlanetSprite planetSprite = new PlanetSprite(this.planetsLayer, row, pg, dgv);
            continue;
          case 11:
            StargateSprite stargateSprite = new StargateSprite(this.stargatesLayer, row, pg, dgv);
            continue;
          case 12:
            StarbaseSprite starbaseSprite = new StarbaseSprite(this.starbasesLayer, row, pg, dgv);
            continue;
          case 37:
            DecorationSprite decorationSprite = new DecorationSprite(this.decorationsLayer, row, pg, dgv);
            continue;
          case 38:
            HarvestableSprite harvestableSprite = new HarvestableSprite(this.harvestableLayer, row, pg, dgv);
            continue;
          case 40:
          case 41:
            EEffectsSprite eeffectsSprite = new EEffectsSprite(this.envirementLayer, row, pg, dgv);
            continue;
          default:
            continue;
        }
      }
      this.masterLayer.AddChild((PNode) this.boundsLayer);
      this.masterLayer.AddChild((PNode) this.mobsLayer);
      this.masterLayer.AddChild((PNode) this.planetsLayer);
      this.masterLayer.AddChild((PNode) this.stargatesLayer);
      this.masterLayer.AddChild((PNode) this.starbasesLayer);
      this.masterLayer.AddChild((PNode) this.decorationsLayer);
      this.masterLayer.AddChild((PNode) this.harvestableLayer);
      this.masterLayer.AddChild((PNode) this.envirementLayer);
      this.masterLayer.MouseDown += new PInputEventHandler(this.MasterLayer_OnMouseDown);
      this.canvas.Camera.MouseDown += new PInputEventHandler(this.canvasCamera_MouseDown);
      this.canvas.Camera.ViewScale = 0.375f;
    }

    public SectorObjectsSql getSectorObjectsSQL()
    {
      return this.so;
    }

    public void updateChangedInfo(string propertyName, string _changedValue)
    {
      string s = _changedValue.Replace("'", "''");
      if (propertyName == "Name")
        this.dr["name"] = (object) s;
      else if (propertyName == "SectorID")
        this.dr["sector_id"] = (object) int.Parse(s);
      else if (propertyName == "Width" || propertyName == "Height" || propertyName == "Depth")
      {
        float num1 = 0.0f;
        float num2 = 0.0f;
        float num3 = 0.0f;
        float num4 = 0.0f;
        float num5 = 0.0f;
        float num6 = 0.0f;
        if ((double) this.sp.Width != 0.0)
        {
          num1 = (float) -((double) this.sp.Width / 2.0);
          num2 = this.sp.Width / 2f;
        }
        if ((double) this.sp.Height != 0.0)
        {
          num3 = (float) -((double) this.sp.Height / 2.0);
          num4 = this.sp.Height / 2f;
        }
        if ((double) this.sp.Depth != 0.0)
        {
          num5 = (float) -((double) this.sp.Depth / 2.0);
          num6 = this.sp.Depth / 2f;
        }
        this.dr["x_min"] = (object) num1;
        this.dr["x_max"] = (object) num2;
        this.dr["y_min"] = (object) num3;
        this.dr["y_max"] = (object) num4;
        this.dr["z_min"] = (object) num5;
        this.dr["z_max"] = (object) num6;
        float num7 = (float) (-((double) this.sp.Width / 2.0) / 100.0);
        float num8 = (float) (-((double) this.sp.Height / 2.0) / 100.0);
        this.boundsLayer.GetChild(0).X = num7;
        this.boundsLayer.GetChild(0).Y = num8;
        this.boundsLayer.GetChild(0).Width = this.sp.Width / 100f;
        this.boundsLayer.GetChild(0).Height = this.sp.Height / 100f;
      }
      else if (propertyName == "GridX")
        this.dr["grix_x"] = (object) int.Parse(s);
      else if (propertyName == "GridY")
        this.dr["grix_y"] = (object) int.Parse(s);
      else if (propertyName == "GridZ")
        this.dr["grix_z"] = (object) int.Parse(s);
      else if (propertyName == "FogNear")
        this.dr["fog_near"] = (object) float.Parse(s);
      else if (propertyName == "FogFar")
        this.dr["fog_far"] = (object) float.Parse(s);
      else if (propertyName == "DebrisMode")
        this.dr["debris_mode"] = (object) int.Parse(s);
      else if (propertyName == "LightBackdrop")
        this.dr["light_backdrop"] = (object) bool.Parse(s);
      else if (propertyName == "FogBackdrop")
        this.dr["fog_backdrop"] = (object) bool.Parse(s);
      else if (propertyName == "SwapBackdrop")
        this.dr["swap_backdrop"] = (object) bool.Parse(s);
      else if (propertyName == "BackdropFogNear")
        this.dr["backdrop_fog_near"] = (object) float.Parse(s);
      else if (propertyName == "BackdropFogFar")
        this.dr["backdrop_fog_far"] = (object) float.Parse(s);
      else if (propertyName == "MaxTilt")
        this.dr["mex_tilt"] = (object) float.Parse(s);
      else if (propertyName == "AutoLevel")
        this.dr["auto_level"] = (object) bool.Parse(s);
      else if (propertyName == "ImpulseRate")
        this.dr["impulse_rate"] = (object) float.Parse(s);
      else if (propertyName == "DecayVelocity")
        this.dr["decay_velocity"] = (object) float.Parse(s);
      else if (propertyName == "DecaySpin")
        this.dr["decay_spin"] = (object) float.Parse(s);
      else if (propertyName == "BackdropAsset")
        this.dr["backdrop_asset"] = (object) int.Parse(s);
      else if (propertyName == "Greetings")
        this.dr["greetings"] = (object) s;
      else if (propertyName == "Notes")
        this.dr["notes"] = (object) s;
      else if (propertyName == "SystemID")
        this.dr["system_id"] = (object) int.Parse(s);
      else if (propertyName == "GalaxyX")
        this.dr["galaxy_x"] = (object) float.Parse(s);
      else if (propertyName == "GalaxyY")
        this.dr["galaxy_y"] = (object) float.Parse(s);
      else if (propertyName == "GalaxyZ")
        this.dr["galaxy_z"] = (object) float.Parse(s);
      else if (propertyName == "SectorType")
      {
        if (s == "Space Sector")
          this.dr["sector_type"] = (object) 0;
        else if (s == "Rocky Planet Surface")
          this.dr["sector_type"] = (object) 1;
        else if (s == "Gas Giant Surface")
          this.dr["sector_type"] = (object) 2;
      }
      if (this.dr.RowState == DataRowState.Modified)
        return;
      this.dr.SetModified();
    }

    public void PropertyGrid1_PropertyValueChanged(object sender, PropertyValueChangedEventArgs e)
    {
      string name = e.ChangedItem.PropertyDescriptor.Name;
      try
      {
        if (this.pSelectedNode.Tag.GetType().ToString().Contains("MobSprite"))
          ((MobSprite) this.pSelectedNode.Tag).updateChangedInfo(name, e.ChangedItem.Value);
        else if (this.pSelectedNode.Tag.GetType().ToString().Contains("PlanetSprite"))
          ((PlanetSprite) this.pSelectedNode.Tag).updateChangedInfo(name, e.ChangedItem.Value);
        else if (this.pSelectedNode.Tag.GetType().ToString().Contains("StargateSprite"))
          ((StargateSprite) this.pSelectedNode.Tag).updateChangedInfo(name, e.ChangedItem.Value);
        else if (this.pSelectedNode.Tag.GetType().ToString().Contains("StarbaseSprite"))
          ((StarbaseSprite) this.pSelectedNode.Tag).updateChangedInfo(name, e.ChangedItem.Value);
        else if (this.pSelectedNode.Tag.GetType().ToString().Contains("HarvestableSprite"))
          ((HarvestableSprite) this.pSelectedNode.Tag).updateChangedInfo(name, e.ChangedItem.Value);
        else if (this.pSelectedNode.Tag.GetType().ToString().Contains("DecorationSprite"))
        {
          ((DecorationSprite) this.pSelectedNode.Tag).updateChangedInfo(name, e.ChangedItem.Value);
        }
        else
        {
          if (!this.pSelectedNode.Tag.GetType().ToString().Contains("EEffectsSprite"))
            return;
          ((EEffectsSprite) this.pSelectedNode.Tag).updateChangedInfo(name, e.ChangedItem.Value);
        }
      }
      catch (Exception ex)
      {
        this.updateChangedInfo(name, e.ChangedItem.Value.ToString());
      }
    }

    public void canvasCamera_MouseDown(object sender, PInputEventArgs e)
    {
      if (this._nso == null)
        return;
      this._nso.setPosition(e.Position);
      this._nso.Show();
      this._nso = (NewSectorObject) null;
    }

    public void MasterLayer_OnMouseDown(object sender, PInputEventArgs e)
    {
      int num = 0;
      if (e.Button == MouseButtons.Right || e.PickedNode.ChildrenCount < 3)
        return;
      if (e.PickedNode.Tag.GetType().ToString().Contains("MobSprite"))
      {
        this.setOriginalText(this.pSelectedNode);
        MobSprite tag = (MobSprite) e.PickedNode.Tag;
        tag.getText().TextBrush = Brushes.Red;
        num = int.Parse(tag.getRow()["sector_object_id"].ToString());
      }
      if (e.PickedNode.Tag.GetType().ToString().Contains("PlanetSprite"))
      {
        this.setOriginalText(this.pSelectedNode);
        PlanetSprite tag = (PlanetSprite) e.PickedNode.Tag;
        tag.getText().TextBrush = Brushes.Red;
        num = int.Parse(tag.getRow()["sector_object_id"].ToString());
      }
      if (e.PickedNode.Tag.GetType().ToString().Contains("StargateSprite"))
      {
        this.setOriginalText(this.pSelectedNode);
        StargateSprite tag = (StargateSprite) e.PickedNode.Tag;
        tag.getText().TextBrush = Brushes.Red;
        num = int.Parse(tag.getRow()["sector_object_id"].ToString());
      }
      if (e.PickedNode.Tag.GetType().ToString().Contains("StarbaseSprite"))
      {
        this.setOriginalText(this.pSelectedNode);
        StarbaseSprite tag = (StarbaseSprite) e.PickedNode.Tag;
        tag.getText().TextBrush = Brushes.Red;
        num = int.Parse(tag.getRow()["sector_object_id"].ToString());
      }
      if (e.PickedNode.Tag.GetType().ToString().Contains("HarvestableSprite"))
      {
        this.setOriginalText(this.pSelectedNode);
        HarvestableSprite tag = (HarvestableSprite) e.PickedNode.Tag;
        tag.getText().TextBrush = Brushes.Red;
        num = int.Parse(tag.getRow()["sector_object_id"].ToString());
      }
      if (e.PickedNode.Tag.GetType().ToString().Contains("DecorationSprite"))
      {
        this.setOriginalText(this.pSelectedNode);
        DecorationSprite tag = (DecorationSprite) e.PickedNode.Tag;
        tag.getText().TextBrush = Brushes.Red;
        num = int.Parse(tag.getRow()["sector_object_id"].ToString());
      }
      this.pSelectedNode = e.PickedNode;
      foreach (DataGridViewRow row in (IEnumerable) this._dgv.Rows)
      {
        if (int.Parse(this._dgv.Rows[row.Index].Cells["id"].Value.ToString()) == num)
        {
          this._dgv.Rows[row.Index].Selected = true;
          this._dgv.CurrentCell = this._dgv.Rows[row.Index].Cells[0];
        }
      }
    }

    public void setOriginalText(PNode pickedNode)
    {
      if (this.pSelectedNode == null)
        return;
      if (pickedNode.Tag.GetType().ToString().Contains("MobSprite"))
        ((MobSprite) this.pSelectedNode.Tag).getText().TextBrush = Brushes.White;
      if (pickedNode.Tag.GetType().ToString().Contains("PlanetSprite"))
        ((PlanetSprite) this.pSelectedNode.Tag).getText().TextBrush = Brushes.White;
      if (pickedNode.Tag.GetType().ToString().Contains("StargateSprite"))
        ((StargateSprite) this.pSelectedNode.Tag).getText().TextBrush = Brushes.White;
      if (pickedNode.Tag.GetType().ToString().Contains("StarbaseSprite"))
        ((StarbaseSprite) this.pSelectedNode.Tag).getText().TextBrush = Brushes.White;
      if (pickedNode.Tag.GetType().ToString().Contains("HarvestableSprite"))
        ((HarvestableSprite) this.pSelectedNode.Tag).getText().TextBrush = Brushes.White;
      if (!pickedNode.Tag.GetType().ToString().Contains("DecorationSprite"))
        return;
      ((DecorationSprite) this.pSelectedNode.Tag).getText().TextBrush = Brushes.White;
    }

    public void newSectorObject(NewSectorObject nso)
    {
      this._nso = nso;
    }

    public void setSelected(int _id)
    {
      for (int index1 = 1; index1 < this.masterLayer.ChildrenCount; ++index1)
      {
        for (int index2 = 0; index2 < this.masterLayer.GetChild(index1).ChildrenCount; ++index2)
        {
          PNode child = this.masterLayer.GetChild(index1).GetChild(index2);
          if (child.Tag.GetType().ToString().Contains("MobSprite"))
          {
            MobSprite tag = (MobSprite) child.Tag;
            int num = int.Parse(tag.getRow()["sector_object_id"].ToString());
            if (_id == num)
            {
              this.setOriginalText(this.pSelectedNode);
              tag.getText().TextBrush = Brushes.Red;
              this.pSelectedNode = child;
              tag.setPropGrid();
              return;
            }
          }
          if (child.Tag.GetType().ToString().Contains("PlanetSprite"))
          {
            PlanetSprite tag = (PlanetSprite) child.Tag;
            int num = int.Parse(tag.getRow()["sector_object_id"].ToString());
            if (_id == num)
            {
              this.setOriginalText(this.pSelectedNode);
              tag.getText().TextBrush = Brushes.Red;
              this.pSelectedNode = child;
              tag.setPropGrid();
              return;
            }
          }
          if (child.Tag.GetType().ToString().Contains("StargateSprite"))
          {
            StargateSprite tag = (StargateSprite) child.Tag;
            int num = int.Parse(tag.getRow()["sector_object_id"].ToString());
            if (_id == num)
            {
              this.setOriginalText(this.pSelectedNode);
              tag.getText().TextBrush = Brushes.Red;
              this.pSelectedNode = child;
              tag.setPropGrid();
              return;
            }
          }
          if (child.Tag.GetType().ToString().Contains("StarbaseSprite"))
          {
            StarbaseSprite tag = (StarbaseSprite) child.Tag;
            int num = int.Parse(tag.getRow()["sector_object_id"].ToString());
            if (_id == num)
            {
              this.setOriginalText(this.pSelectedNode);
              tag.getText().TextBrush = Brushes.Red;
              this.pSelectedNode = child;
              tag.setPropGrid();
              return;
            }
          }
          if (child.Tag.GetType().ToString().Contains("HarvestableSprite"))
          {
            HarvestableSprite tag = (HarvestableSprite) child.Tag;
            int num = int.Parse(tag.getRow()["sector_object_id"].ToString());
            if (_id == num)
            {
              this.setOriginalText(this.pSelectedNode);
              tag.getText().TextBrush = Brushes.Red;
              this.pSelectedNode = child;
              tag.setPropGrid();
              return;
            }
          }
          if (child.Tag.GetType().ToString().Contains("DecorationSprite"))
          {
            DecorationSprite tag = (DecorationSprite) child.Tag;
            int num = int.Parse(tag.getRow()["sector_object_id"].ToString());
            if (_id == num)
            {
              this.setOriginalText(this.pSelectedNode);
              tag.getText().TextBrush = Brushes.Red;
              this.pSelectedNode = child;
              tag.setPropGrid();
              return;
            }
          }
          if (child.Tag.GetType().ToString().Contains("EEffectsSprite"))
          {
            EEffectsSprite tag = (EEffectsSprite) child.Tag;
            int num = int.Parse(tag.getRow()["sector_object_id"].ToString());
            if (_id == num)
            {
              this.setOriginalText(this.pSelectedNode);
              tag.getText().TextBrush = Brushes.Red;
              this.pSelectedNode = child;
              tag.setPropGrid();
              return;
            }
          }
        }
      }
    }

    public void hideLayer(int type)
    {
      switch (type)
      {
        case 0:
          this.mobsLayer.Visible = false;
          break;
        case 3:
          this.planetsLayer.Visible = false;
          break;
        case 11:
          this.stargatesLayer.Visible = false;
          break;
        case 12:
          this.starbasesLayer.Visible = false;
          break;
        case 37:
          this.decorationsLayer.Visible = false;
          break;
        case 38:
          this.harvestableLayer.Visible = false;
          break;
      }
    }

    public void showLayer(int type)
    {
      switch (type)
      {
        case 0:
          this.mobsLayer.Visible = true;
          break;
        case 3:
          this.planetsLayer.Visible = true;
          break;
        case 11:
          this.stargatesLayer.Visible = true;
          break;
        case 12:
          this.starbasesLayer.Visible = true;
          break;
        case 37:
          this.decorationsLayer.Visible = true;
          break;
        case 38:
          this.harvestableLayer.Visible = true;
          break;
      }
    }

    public void turnOffText(int type)
    {
      switch (type)
      {
        case 0:
          for (int index = 0; index < this.mobsLayer.ChildrenCount; ++index)
            this.mobsLayer.GetChild(index).GetChild(3).Visible = false;
          break;
        case 3:
          for (int index = 0; index < this.planetsLayer.ChildrenCount; ++index)
            this.planetsLayer.GetChild(index).GetChild(3).Visible = false;
          break;
        case 11:
          for (int index = 0; index < this.stargatesLayer.ChildrenCount; ++index)
            this.stargatesLayer.GetChild(index).GetChild(3).Visible = false;
          break;
        case 12:
          for (int index = 0; index < this.starbasesLayer.ChildrenCount; ++index)
            this.starbasesLayer.GetChild(index).GetChild(3).Visible = false;
          break;
        case 37:
          for (int index = 0; index < this.decorationsLayer.ChildrenCount; ++index)
            this.decorationsLayer.GetChild(index).GetChild(3).Visible = false;
          break;
        case 38:
          for (int index = 0; index < this.harvestableLayer.ChildrenCount; ++index)
            this.harvestableLayer.GetChild(index).GetChild(3).Visible = false;
          break;
      }
    }

    public void turnOnText(int type)
    {
      switch (type)
      {
        case 0:
          for (int index = 0; index < this.mobsLayer.ChildrenCount; ++index)
            this.mobsLayer.GetChild(index).GetChild(3).Visible = true;
          break;
        case 3:
          for (int index = 0; index < this.planetsLayer.ChildrenCount; ++index)
            this.planetsLayer.GetChild(index).GetChild(3).Visible = true;
          break;
        case 11:
          for (int index = 0; index < this.stargatesLayer.ChildrenCount; ++index)
            this.stargatesLayer.GetChild(index).GetChild(3).Visible = true;
          break;
        case 12:
          for (int index = 0; index < this.starbasesLayer.ChildrenCount; ++index)
            this.starbasesLayer.GetChild(index).GetChild(3).Visible = true;
          break;
        case 37:
          for (int index = 0; index < this.decorationsLayer.ChildrenCount; ++index)
            this.decorationsLayer.GetChild(index).GetChild(3).Visible = true;
          break;
        case 38:
          for (int index = 0; index < this.harvestableLayer.ChildrenCount; ++index)
            this.harvestableLayer.GetChild(index).GetChild(3).Visible = true;
          break;
      }
    }

    public void explorationRangeOn(int type)
    {
      switch (type)
      {
        case 0:
          for (int index = 0; index < this.mobsLayer.ChildrenCount; ++index)
            this.mobsLayer.GetChild(index).GetChild(2).Visible = true;
          break;
        case 3:
          for (int index = 0; index < this.planetsLayer.ChildrenCount; ++index)
            this.planetsLayer.GetChild(index).GetChild(2).Visible = true;
          break;
        case 11:
          for (int index = 0; index < this.stargatesLayer.ChildrenCount; ++index)
            this.stargatesLayer.GetChild(index).GetChild(2).Visible = true;
          break;
        case 12:
          for (int index = 0; index < this.starbasesLayer.ChildrenCount; ++index)
            this.starbasesLayer.GetChild(index).GetChild(2).Visible = true;
          break;
        case 37:
          for (int index = 0; index < this.decorationsLayer.ChildrenCount; ++index)
            this.decorationsLayer.GetChild(index).GetChild(2).Visible = true;
          break;
        case 38:
          for (int index = 0; index < this.harvestableLayer.ChildrenCount; ++index)
            this.harvestableLayer.GetChild(index).GetChild(2).Visible = true;
          break;
      }
    }

    public void explorationRangeOff(int type)
    {
      switch (type)
      {
        case 0:
          for (int index = 0; index < this.mobsLayer.ChildrenCount; ++index)
            this.mobsLayer.GetChild(index).GetChild(2).Visible = false;
          break;
        case 3:
          for (int index = 0; index < this.planetsLayer.ChildrenCount; ++index)
            this.planetsLayer.GetChild(index).GetChild(2).Visible = false;
          break;
        case 11:
          for (int index = 0; index < this.stargatesLayer.ChildrenCount; ++index)
            this.stargatesLayer.GetChild(index).GetChild(2).Visible = false;
          break;
        case 12:
          for (int index = 0; index < this.starbasesLayer.ChildrenCount; ++index)
            this.starbasesLayer.GetChild(index).GetChild(2).Visible = false;
          break;
        case 37:
          for (int index = 0; index < this.decorationsLayer.ChildrenCount; ++index)
            this.decorationsLayer.GetChild(index).GetChild(2).Visible = false;
          break;
        case 38:
          for (int index = 0; index < this.harvestableLayer.ChildrenCount; ++index)
            this.harvestableLayer.GetChild(index).GetChild(2).Visible = false;
          break;
      }
    }

    public void radarRangeOn(int type)
    {
      switch (type)
      {
        case 0:
          for (int index = 0; index < this.mobsLayer.ChildrenCount; ++index)
            this.mobsLayer.GetChild(index).GetChild(1).Visible = true;
          break;
        case 3:
          for (int index = 0; index < this.planetsLayer.ChildrenCount; ++index)
            this.planetsLayer.GetChild(index).GetChild(1).Visible = true;
          break;
        case 11:
          for (int index = 0; index < this.stargatesLayer.ChildrenCount; ++index)
            this.stargatesLayer.GetChild(index).GetChild(1).Visible = true;
          break;
        case 12:
          for (int index = 0; index < this.starbasesLayer.ChildrenCount; ++index)
            this.starbasesLayer.GetChild(index).GetChild(1).Visible = true;
          break;
        case 37:
          for (int index = 0; index < this.decorationsLayer.ChildrenCount; ++index)
            this.decorationsLayer.GetChild(index).GetChild(1).Visible = true;
          break;
        case 38:
          for (int index = 0; index < this.harvestableLayer.ChildrenCount; ++index)
            this.harvestableLayer.GetChild(index).GetChild(1).Visible = true;
          break;
      }
    }

    public void radarRangeOff(int type)
    {
      switch (type)
      {
        case 0:
          for (int index = 0; index < this.mobsLayer.ChildrenCount; ++index)
            this.mobsLayer.GetChild(index).GetChild(1).Visible = false;
          break;
        case 3:
          for (int index = 0; index < this.planetsLayer.ChildrenCount; ++index)
            this.planetsLayer.GetChild(index).GetChild(1).Visible = false;
          break;
        case 11:
          for (int index = 0; index < this.stargatesLayer.ChildrenCount; ++index)
            this.stargatesLayer.GetChild(index).GetChild(1).Visible = false;
          break;
        case 12:
          for (int index = 0; index < this.starbasesLayer.ChildrenCount; ++index)
            this.starbasesLayer.GetChild(index).GetChild(1).Visible = false;
          break;
        case 37:
          for (int index = 0; index < this.decorationsLayer.ChildrenCount; ++index)
            this.decorationsLayer.GetChild(index).GetChild(1).Visible = false;
          break;
        case 38:
          for (int index = 0; index < this.harvestableLayer.ChildrenCount; ++index)
            this.harvestableLayer.GetChild(index).GetChild(1).Visible = false;
          break;
      }
    }

    public void SignatureOn(int type)
    {
      switch (type)
      {
        case 0:
          for (int index = 0; index < this.mobsLayer.ChildrenCount; ++index)
            this.mobsLayer.GetChild(index).GetChild(0).Visible = true;
          break;
        case 3:
          for (int index = 0; index < this.planetsLayer.ChildrenCount; ++index)
            this.planetsLayer.GetChild(index).GetChild(0).Visible = true;
          break;
        case 11:
          for (int index = 0; index < this.stargatesLayer.ChildrenCount; ++index)
            this.stargatesLayer.GetChild(index).GetChild(0).Visible = true;
          break;
        case 12:
          for (int index = 0; index < this.starbasesLayer.ChildrenCount; ++index)
            this.starbasesLayer.GetChild(index).GetChild(0).Visible = true;
          break;
        case 37:
          for (int index = 0; index < this.decorationsLayer.ChildrenCount; ++index)
            this.decorationsLayer.GetChild(index).GetChild(0).Visible = true;
          break;
        case 38:
          for (int index = 0; index < this.harvestableLayer.ChildrenCount; ++index)
            this.harvestableLayer.GetChild(index).GetChild(0).Visible = true;
          break;
      }
    }

    public void SignatureOff(int type)
    {
      switch (type)
      {
        case 0:
          for (int index = 0; index < this.mobsLayer.ChildrenCount; ++index)
            this.mobsLayer.GetChild(index).GetChild(0).Visible = false;
          break;
        case 3:
          for (int index = 0; index < this.planetsLayer.ChildrenCount; ++index)
            this.planetsLayer.GetChild(index).GetChild(0).Visible = false;
          break;
        case 11:
          for (int index = 0; index < this.stargatesLayer.ChildrenCount; ++index)
            this.stargatesLayer.GetChild(index).GetChild(0).Visible = false;
          break;
        case 12:
          for (int index = 0; index < this.starbasesLayer.ChildrenCount; ++index)
            this.starbasesLayer.GetChild(index).GetChild(0).Visible = false;
          break;
        case 37:
          for (int index = 0; index < this.decorationsLayer.ChildrenCount; ++index)
            this.decorationsLayer.GetChild(index).GetChild(0).Visible = false;
          break;
        case 38:
          for (int index = 0; index < this.harvestableLayer.ChildrenCount; ++index)
            this.harvestableLayer.GetChild(index).GetChild(0).Visible = false;
          break;
      }
    }

    public void navTypeZeroOn(int type)
    {
      switch (type)
      {
        case 0:
          for (int index = 0; index < this.mobsLayer.ChildrenCount; ++index)
          {
            PNode child = this.mobsLayer.GetChild(index);
            if (child.ChildrenCount == 4)
              child.Visible = true;
          }
          break;
        case 3:
          for (int index = 0; index < this.planetsLayer.ChildrenCount; ++index)
          {
            PNode child = this.planetsLayer.GetChild(index);
            if (child.ChildrenCount == 4)
              child.Visible = true;
          }
          break;
        case 11:
          for (int index = 0; index < this.stargatesLayer.ChildrenCount; ++index)
          {
            PNode child = this.stargatesLayer.GetChild(index);
            if (child.ChildrenCount == 4)
              child.Visible = true;
          }
          break;
        case 12:
          for (int index = 0; index < this.starbasesLayer.ChildrenCount; ++index)
          {
            PNode child = this.starbasesLayer.GetChild(index);
            if (child.ChildrenCount == 4)
              child.Visible = true;
          }
          break;
        case 37:
          for (int index = 0; index < this.decorationsLayer.ChildrenCount; ++index)
          {
            PNode child = this.decorationsLayer.GetChild(index);
            if (child.ChildrenCount == 4)
              child.Visible = true;
          }
          break;
        case 38:
          for (int index = 0; index < this.harvestableLayer.ChildrenCount; ++index)
          {
            PNode child = this.harvestableLayer.GetChild(index);
            if (child.ChildrenCount == 4)
              child.Visible = true;
          }
          break;
      }
    }

    public void navTypeOneOn(int type)
    {
      switch (type)
      {
        case 0:
          for (int index = 0; index < this.mobsLayer.ChildrenCount; ++index)
          {
            PNode child = this.mobsLayer.GetChild(index);
            if (child.ChildrenCount == 5)
              child.Visible = true;
          }
          break;
        case 3:
          for (int index = 0; index < this.planetsLayer.ChildrenCount; ++index)
          {
            PNode child = this.planetsLayer.GetChild(index);
            if (child.ChildrenCount == 5)
              child.Visible = true;
          }
          break;
        case 11:
          for (int index = 0; index < this.stargatesLayer.ChildrenCount; ++index)
          {
            PNode child = this.stargatesLayer.GetChild(index);
            if (child.ChildrenCount == 5)
              child.Visible = true;
          }
          break;
        case 12:
          for (int index = 0; index < this.starbasesLayer.ChildrenCount; ++index)
          {
            PNode child = this.starbasesLayer.GetChild(index);
            if (child.ChildrenCount == 5)
              child.Visible = true;
          }
          break;
        case 37:
          for (int index = 0; index < this.decorationsLayer.ChildrenCount; ++index)
          {
            PNode child = this.decorationsLayer.GetChild(index);
            if (child.ChildrenCount == 5)
              child.Visible = true;
          }
          break;
        case 38:
          for (int index = 0; index < this.harvestableLayer.ChildrenCount; ++index)
          {
            PNode child = this.harvestableLayer.GetChild(index);
            if (child.ChildrenCount == 5)
              child.Visible = true;
          }
          break;
      }
    }

    public void navTypeTwoOn(int type)
    {
      switch (type)
      {
        case 0:
          for (int index = 0; index < this.mobsLayer.ChildrenCount; ++index)
          {
            PNode child = this.mobsLayer.GetChild(index);
            if (child.ChildrenCount == 6)
              child.Visible = true;
          }
          break;
        case 3:
          for (int index = 0; index < this.planetsLayer.ChildrenCount; ++index)
          {
            PNode child = this.planetsLayer.GetChild(index);
            if (child.ChildrenCount == 6)
              child.Visible = true;
          }
          break;
        case 11:
          for (int index = 0; index < this.stargatesLayer.ChildrenCount; ++index)
          {
            PNode child = this.stargatesLayer.GetChild(index);
            if (child.ChildrenCount == 6)
              child.Visible = true;
          }
          break;
        case 12:
          for (int index = 0; index < this.starbasesLayer.ChildrenCount; ++index)
          {
            PNode child = this.starbasesLayer.GetChild(index);
            if (child.ChildrenCount == 6)
              child.Visible = true;
          }
          break;
        case 37:
          for (int index = 0; index < this.decorationsLayer.ChildrenCount; ++index)
          {
            PNode child = this.decorationsLayer.GetChild(index);
            if (child.ChildrenCount == 6)
              child.Visible = true;
          }
          break;
        case 38:
          for (int index = 0; index < this.harvestableLayer.ChildrenCount; ++index)
          {
            PNode child = this.harvestableLayer.GetChild(index);
            if (child.ChildrenCount == 6)
              child.Visible = true;
          }
          break;
      }
    }

    public void navTypeZeroOff(int type)
    {
      switch (type)
      {
        case 0:
          for (int index = 0; index < this.mobsLayer.ChildrenCount; ++index)
          {
            PNode child = this.mobsLayer.GetChild(index);
            if (child.ChildrenCount == 4)
              child.Visible = false;
          }
          break;
        case 3:
          for (int index = 0; index < this.planetsLayer.ChildrenCount; ++index)
          {
            PNode child = this.planetsLayer.GetChild(index);
            if (child.ChildrenCount == 4)
              child.Visible = false;
          }
          break;
        case 11:
          for (int index = 0; index < this.stargatesLayer.ChildrenCount; ++index)
          {
            PNode child = this.stargatesLayer.GetChild(index);
            if (child.ChildrenCount == 4)
              child.Visible = false;
          }
          break;
        case 12:
          for (int index = 0; index < this.starbasesLayer.ChildrenCount; ++index)
          {
            PNode child = this.starbasesLayer.GetChild(index);
            if (child.ChildrenCount == 4)
              child.Visible = false;
          }
          break;
        case 37:
          for (int index = 0; index < this.decorationsLayer.ChildrenCount; ++index)
          {
            PNode child = this.decorationsLayer.GetChild(index);
            if (child.ChildrenCount == 4)
              child.Visible = false;
          }
          break;
        case 38:
          for (int index = 0; index < this.harvestableLayer.ChildrenCount; ++index)
          {
            PNode child = this.harvestableLayer.GetChild(index);
            if (child.ChildrenCount == 4)
              child.Visible = false;
          }
          break;
      }
    }

    public void navTypeOneOff(int type)
    {
      switch (type)
      {
        case 0:
          for (int index = 0; index < this.mobsLayer.ChildrenCount; ++index)
          {
            PNode child = this.mobsLayer.GetChild(index);
            if (child.ChildrenCount == 5)
              child.Visible = false;
          }
          break;
        case 3:
          for (int index = 0; index < this.planetsLayer.ChildrenCount; ++index)
          {
            PNode child = this.planetsLayer.GetChild(index);
            if (child.ChildrenCount == 5)
              child.Visible = false;
          }
          break;
        case 11:
          for (int index = 0; index < this.stargatesLayer.ChildrenCount; ++index)
          {
            PNode child = this.stargatesLayer.GetChild(index);
            if (child.ChildrenCount == 5)
              child.Visible = false;
          }
          break;
        case 12:
          for (int index = 0; index < this.starbasesLayer.ChildrenCount; ++index)
          {
            PNode child = this.starbasesLayer.GetChild(index);
            if (child.ChildrenCount == 5)
              child.Visible = false;
          }
          break;
        case 37:
          for (int index = 0; index < this.decorationsLayer.ChildrenCount; ++index)
          {
            PNode child = this.decorationsLayer.GetChild(index);
            if (child.ChildrenCount == 5)
              child.Visible = false;
          }
          break;
        case 38:
          for (int index = 0; index < this.harvestableLayer.ChildrenCount; ++index)
          {
            PNode child = this.harvestableLayer.GetChild(index);
            if (child.ChildrenCount == 5)
              child.Visible = false;
          }
          break;
      }
    }

    public void navTypeTwoOff(int type)
    {
      switch (type)
      {
        case 0:
          for (int index = 0; index < this.mobsLayer.ChildrenCount; ++index)
          {
            PNode child = this.mobsLayer.GetChild(index);
            if (child.ChildrenCount == 6)
              child.Visible = false;
          }
          break;
        case 3:
          for (int index = 0; index < this.planetsLayer.ChildrenCount; ++index)
          {
            PNode child = this.planetsLayer.GetChild(index);
            if (child.ChildrenCount == 6)
              child.Visible = false;
          }
          break;
        case 11:
          for (int index = 0; index < this.stargatesLayer.ChildrenCount; ++index)
          {
            PNode child = this.stargatesLayer.GetChild(index);
            if (child.ChildrenCount == 6)
              child.Visible = false;
          }
          break;
        case 12:
          for (int index = 0; index < this.starbasesLayer.ChildrenCount; ++index)
          {
            PNode child = this.starbasesLayer.GetChild(index);
            if (child.ChildrenCount == 6)
              child.Visible = false;
          }
          break;
        case 37:
          for (int index = 0; index < this.decorationsLayer.ChildrenCount; ++index)
          {
            PNode child = this.decorationsLayer.GetChild(index);
            if (child.ChildrenCount == 6)
              child.Visible = false;
          }
          break;
        case 38:
          for (int index = 0; index < this.harvestableLayer.ChildrenCount; ++index)
          {
            PNode child = this.harvestableLayer.GetChild(index);
            if (child.ChildrenCount == 6)
              child.Visible = false;
          }
          break;
      }
    }

    public void appearsInRadarOn(int type)
    {
      switch (type)
      {
        case 0:
          for (int index = 0; index < this.mobsLayer.ChildrenCount; ++index)
          {
            PNode child = this.mobsLayer.GetChild(index);
            if (((MobSprite) child.Tag).getAppearsInRader())
              child.Visible = true;
          }
          break;
        case 3:
          for (int index = 0; index < this.planetsLayer.ChildrenCount; ++index)
          {
            PNode child = this.planetsLayer.GetChild(index);
            if (((PlanetSprite) child.Tag).getAppearsInRader())
              child.Visible = true;
          }
          break;
        case 11:
          for (int index = 0; index < this.stargatesLayer.ChildrenCount; ++index)
          {
            PNode child = this.stargatesLayer.GetChild(index);
            if (((StargateSprite) child.Tag).getAppearsInRader())
              child.Visible = true;
          }
          break;
        case 12:
          for (int index = 0; index < this.starbasesLayer.ChildrenCount; ++index)
          {
            PNode child = this.starbasesLayer.GetChild(index);
            if (((StarbaseSprite) child.Tag).getAppearsInRader())
              child.Visible = true;
          }
          break;
        case 37:
          for (int index = 0; index < this.decorationsLayer.ChildrenCount; ++index)
          {
            PNode child = this.decorationsLayer.GetChild(index);
            if (((DecorationSprite) child.Tag).getAppearsInRader())
              child.Visible = true;
          }
          break;
        case 38:
          for (int index = 0; index < this.harvestableLayer.ChildrenCount; ++index)
          {
            PNode child = this.harvestableLayer.GetChild(index);
            if (((HarvestableSprite) child.Tag).getAppearsInRader())
              child.Visible = true;
          }
          break;
      }
    }

    public void appearsInRadarOff(int type)
    {
      switch (type)
      {
        case 0:
          for (int index = 0; index < this.mobsLayer.ChildrenCount; ++index)
          {
            PNode child = this.mobsLayer.GetChild(index);
            if (((MobSprite) child.Tag).getAppearsInRader())
              child.Visible = false;
          }
          break;
        case 3:
          for (int index = 0; index < this.planetsLayer.ChildrenCount; ++index)
          {
            PNode child = this.planetsLayer.GetChild(index);
            if (((PlanetSprite) child.Tag).getAppearsInRader())
              child.Visible = false;
          }
          break;
        case 11:
          for (int index = 0; index < this.stargatesLayer.ChildrenCount; ++index)
          {
            PNode child = this.stargatesLayer.GetChild(index);
            if (((StargateSprite) child.Tag).getAppearsInRader())
              child.Visible = false;
          }
          break;
        case 12:
          for (int index = 0; index < this.starbasesLayer.ChildrenCount; ++index)
          {
            PNode child = this.starbasesLayer.GetChild(index);
            if (((StarbaseSprite) child.Tag).getAppearsInRader())
              child.Visible = false;
          }
          break;
        case 37:
          for (int index = 0; index < this.decorationsLayer.ChildrenCount; ++index)
          {
            PNode child = this.decorationsLayer.GetChild(index);
            if (((DecorationSprite) child.Tag).getAppearsInRader())
              child.Visible = false;
          }
          break;
        case 38:
          for (int index = 0; index < this.harvestableLayer.ChildrenCount; ++index)
          {
            PNode child = this.harvestableLayer.GetChild(index);
            if (((HarvestableSprite) child.Tag).getAppearsInRader())
              child.Visible = false;
          }
          break;
      }
    }

    public void deleteSelectedObject()
    {
      if (this.pSelectedNode != null)
      {
        this.pSelectedNode.RemoveFromParent();
        DataRow dataRow = (DataRow) null;
        if (this.pSelectedNode.Tag.GetType().ToString().Contains("MobSprite"))
          dataRow = ((MobSprite) this.pSelectedNode.Tag).getRow();
        else if (this.pSelectedNode.Tag.GetType().ToString().Contains("PlanetSprite"))
          dataRow = ((PlanetSprite) this.pSelectedNode.Tag).getRow();
        else if (this.pSelectedNode.Tag.GetType().ToString().Contains("StargateSprite"))
          dataRow = ((StargateSprite) this.pSelectedNode.Tag).getRow();
        else if (this.pSelectedNode.Tag.GetType().ToString().Contains("StarbaseSprite"))
          dataRow = ((StarbaseSprite) this.pSelectedNode.Tag).getRow();
        else if (this.pSelectedNode.Tag.GetType().ToString().Contains("HarvestableSprite"))
          dataRow = ((HarvestableSprite) this.pSelectedNode.Tag).getRow();
        else if (this.pSelectedNode.Tag.GetType().ToString().Contains("DecorationSprite"))
          dataRow = ((DecorationSprite) this.pSelectedNode.Tag).getRow();
        else if (this.pSelectedNode.Tag.GetType().ToString().Contains("EEffectsSprite"))
          dataRow = ((EEffectsSprite) this.pSelectedNode.Tag).getRow();
        this.deletedObjectsID.Add((object) this.deletedObjectsID.Count, (object) int.Parse(dataRow["sector_object_id"].ToString()));
        this.deletedObjectsType.Add((object) this.deletedObjectsType.Count, (object) int.Parse(dataRow["type"].ToString()));
        dataRow.Delete();
        dataRow.AcceptChanges();
        foreach (DataGridViewBand selectedRow in (BaseCollection) this._dgv.SelectedRows)
        {
          this._dgv.Rows.RemoveAt(selectedRow.Index);
          this._dgv.CurrentRow.Selected = false;
          this._dgv.CurrentCell.Selected = false;
          this._dgv.Update();
          this._dgv.Refresh();
        }
        this._pg.SelectedObject = (object) null;
      }
      else
      {
        int num = (int) MessageBox.Show("Sorry there are no Sector objects Selected. \n Please Try Again.");
      }
    }

    public void clearDeletedHashTables()
    {
      this.deletedObjectsID.Clear();
      this.deletedObjectsType.Clear();
    }

    public Hashtable getDeletedObjectsID()
    {
      return this.deletedObjectsID;
    }

    public Hashtable getDeletedObjectsType()
    {
      return this.deletedObjectsType;
    }

    public void addNewObject(int type, DataRow ndr)
    {
      switch (type)
      {
        case 0:
          MobSprite mobSprite = new MobSprite(this.mobsLayer, ndr, this._pg, this._dgv);
          break;
        case 3:
          PlanetSprite planetSprite = new PlanetSprite(this.planetsLayer, ndr, this._pg, this._dgv);
          break;
        case 11:
          StargateSprite stargateSprite = new StargateSprite(this.stargatesLayer, ndr, this._pg, this._dgv);
          break;
        case 12:
          StarbaseSprite starbaseSprite = new StarbaseSprite(this.starbasesLayer, ndr, this._pg, this._dgv);
          break;
        case 37:
          DecorationSprite decorationSprite = new DecorationSprite(this.decorationsLayer, ndr, this._pg, this._dgv);
          break;
        case 38:
          HarvestableSprite harvestableSprite = new HarvestableSprite(this.harvestableLayer, ndr, this._pg, this._dgv);
          break;
        case 40:
        case 41:
          EEffectsSprite eeffectsSprite = new EEffectsSprite(this.envirementLayer, ndr, this._pg, this._dgv);
          break;
      }
      this._dgv.Rows.Add((object) ndr["sector_object_id"].ToString(), (object) ndr["name"].ToString(), (object) ndr["base_asset_id"].ToString(), (object) ndr[nameof (type)].ToString());
      this._dgv.Rows[this._dgv.Rows.Count - 1].Selected = true;
      this._dgv.CurrentCell = this._dgv.Rows[this._dgv.Rows.Count - 1].Cells[0];
      this._dgv.Update();
      this._dgv.Refresh();
      this.setSelected(int.Parse(ndr["sector_object_id"].ToString()));
    }
  }
}
