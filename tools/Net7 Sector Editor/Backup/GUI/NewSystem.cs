﻿// Decompiled with JetBrains decompiler
// Type: N7.GUI.NewSystem
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using N7.Props;
using N7.Sql;
using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace N7.GUI
{
  public class NewSystem : Form
  {
    private SystemsSql _systemSQL;
    private SystemProps sp;
    private TreeView tree;
    private IContainer components;
    private PropertyGrid propertyGrid1;
    private Button button1;
    private Button button2;

    public NewSystem(TreeView t1)
    {
      this.tree = t1;
      this._systemSQL = mainFrm.systems;
      this.InitializeComponent();
    }

    private void NewSystem_Load(object sender, EventArgs e)
    {
      this.sp = new SystemProps();
      this.sp.Name = "<New System>";
      this.sp.Color = Color.Black;
      this.propertyGrid1.SelectedObject = (object) this.sp;
    }

    private void button1_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void button2_Click(object sender, EventArgs e)
    {
      DataTable systemTable = this._systemSQL.getSystemTable();
      DataRow dataRow = systemTable.NewRow();
      string text = this.sp.Name.Replace("'", "''");
      string str = "";
      if (this.sp.Notes != null)
        str = this.sp.Notes.Replace("'", "''");
      dataRow["name"] = (object) text;
      dataRow["notes"] = (object) str;
      dataRow["galaxy_x"] = (object) this.sp.GalaxyX;
      dataRow["galaxy_y"] = (object) this.sp.GalaxyY;
      dataRow["galaxy_z"] = (object) this.sp.GalaxyZ;
      dataRow["color_r"] = (object) this.sp.Color.R;
      dataRow["color_g"] = (object) this.sp.Color.G;
      dataRow["color_b"] = (object) this.sp.Color.B;
      systemTable.Rows.Add(dataRow);
      this.tree.Nodes.Add(new TreeNode(text));
      this._systemSQL.newRow(dataRow);
      this.Close();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (NewSystem));
      this.propertyGrid1 = new PropertyGrid();
      this.button1 = new Button();
      this.button2 = new Button();
      this.SuspendLayout();
      this.propertyGrid1.Dock = DockStyle.Top;
      this.propertyGrid1.Location = new Point(0, 0);
      this.propertyGrid1.Name = "propertyGrid1";
      this.propertyGrid1.Size = new Size(292, 268);
      this.propertyGrid1.TabIndex = 0;
      this.button1.Location = new Point(59, 274);
      this.button1.Name = "button1";
      this.button1.Size = new Size(75, 23);
      this.button1.TabIndex = 1;
      this.button1.Text = "Cancel";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new EventHandler(this.button1_Click);
      this.button2.Location = new Point(160, 274);
      this.button2.Name = "button2";
      this.button2.Size = new Size(75, 23);
      this.button2.TabIndex = 2;
      this.button2.Text = "Save";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new EventHandler(this.button2_Click);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(292, 301);
      this.Controls.Add((Control) this.button2);
      this.Controls.Add((Control) this.button1);
      this.Controls.Add((Control) this.propertyGrid1);
      this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = nameof (NewSystem);
      this.SizeGripStyle = SizeGripStyle.Hide;
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "New System";
      this.Load += new EventHandler(this.NewSystem_Load);
      this.ResumeLayout(false);
    }
  }
}
