﻿// Decompiled with JetBrains decompiler
// Type: N7.GUI.NewSector
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using N7.Props;
using N7.Sql;
using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace N7.GUI
{
  public class NewSector : Form
  {
    private IContainer components;
    private PropertyGrid propertyGrid1;
    private Button button1;
    private Button button2;
    private TreeView tree;
    private SectorProps sp;
    private SectorsSql sectorsSQL;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (NewSector));
      this.propertyGrid1 = new PropertyGrid();
      this.button1 = new Button();
      this.button2 = new Button();
      this.SuspendLayout();
      this.propertyGrid1.Dock = DockStyle.Top;
      this.propertyGrid1.Location = new Point(0, 0);
      this.propertyGrid1.Name = "propertyGrid1";
      this.propertyGrid1.Size = new Size(294, 429);
      this.propertyGrid1.TabIndex = 0;
      this.button1.Location = new Point(62, 435);
      this.button1.Name = "button1";
      this.button1.Size = new Size(75, 23);
      this.button1.TabIndex = 1;
      this.button1.Text = "Cancel";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new EventHandler(this.button1_Click);
      this.button2.Location = new Point(160, 435);
      this.button2.Name = "button2";
      this.button2.Size = new Size(75, 23);
      this.button2.TabIndex = 2;
      this.button2.Text = "Save";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new EventHandler(this.button2_Click);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(294, 470);
      this.Controls.Add((Control) this.button2);
      this.Controls.Add((Control) this.button1);
      this.Controls.Add((Control) this.propertyGrid1);
      this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = nameof (NewSector);
      this.SizeGripStyle = SizeGripStyle.Hide;
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "New Sector";
      this.Load += new EventHandler(this.NewSector_Load);
      this.ResumeLayout(false);
    }

    public NewSector(TreeView t1)
    {
      this.tree = t1;
      this.sectorsSQL = mainFrm.sectors;
      this.InitializeComponent();
    }

    private void NewSector_Load(object sender, EventArgs e)
    {
      this.sp = new SectorProps();
      string text = this.tree.SelectedNode.Text;
      this.sp.SystemID = mainFrm.systems.getIDFromName(text);
      this.sp.Width = 200000f;
      this.sp.Height = 200000f;
      this.sp.Depth = 50000f;
      this.sp.Name = "<New Sector>";
      this.propertyGrid1.SelectedObject = (object) this.sp;
    }

    private void button1_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void button2_Click(object sender, EventArgs e)
    {
      DataTable sectorTable = this.sectorsSQL.getSectorTable();
      bool flag = false;
      foreach (DataRow row in (InternalDataCollectionBase) sectorTable.Rows)
      {
        if (row["sector_id"].ToString() == this.sp.SectorID.ToString())
          flag = true;
        else if (this.sp.SectorID.ToString() == "0")
          flag = true;
      }
      if (flag)
      {
        int num1 = (int) MessageBox.Show("Sorry, the entered sector Id is already taken. \n Please enter a unique one");
      }
      else
      {
        DataRow dataRow = sectorTable.NewRow();
        string text = this.sp.Name.Replace("'", "''");
        string str1 = this.sp.Notes;
        string str2 = this.sp.Greetings;
        if (this.sp.Notes != null)
          str1 = this.sp.Notes.Replace("'", "''");
        if (this.sp.Greetings != null)
          str2 = this.sp.Greetings.Replace("'", "''");
        dataRow["name"] = (object) text;
        dataRow["notes"] = (object) str1;
        dataRow["greetings"] = (object) str2;
        dataRow["sector_id"] = (object) this.sp.SectorID;
        float num2 = (float) -((double) this.sp.Width / 2.0);
        float num3 = this.sp.Width / 2f;
        float num4 = (float) -((double) this.sp.Height / 2.0);
        float num5 = this.sp.Height / 2f;
        float num6 = (float) -((double) this.sp.Depth / 2.0);
        float num7 = this.sp.Depth / 2f;
        dataRow["x_min"] = (object) num2;
        dataRow["x_max"] = (object) num3;
        dataRow["y_min"] = (object) num4;
        dataRow["y_max"] = (object) num5;
        dataRow["z_min"] = (object) num6;
        dataRow["z_max"] = (object) num7;
        dataRow["grid_x"] = (object) this.sp.GridX;
        dataRow["grid_y"] = (object) this.sp.GridY;
        dataRow["grid_z"] = (object) this.sp.GridZ;
        dataRow["fog_near"] = (object) this.sp.FogNear;
        dataRow["fog_far"] = (object) this.sp.FogFar;
        dataRow["debris_mode"] = (object) this.sp.DebrisMode;
        dataRow["light_backdrop"] = (object) this.sp.LightBackdrop;
        dataRow["fog_backdrop"] = (object) this.sp.FogBackdrop;
        dataRow["swap_backdrop"] = (object) this.sp.SwapBackdrop;
        dataRow["backdrop_fog_near"] = (object) this.sp.BackdropFogNear;
        dataRow["backdrop_fog_far"] = (object) this.sp.BackdropFogFar;
        dataRow["max_tilt"] = (object) this.sp.MaxTilt;
        dataRow["auto_level"] = (object) this.sp.AutoLevel;
        dataRow["impulse_rate"] = (object) this.sp.ImpulseRate;
        dataRow["decay_velocity"] = (object) this.sp.DecayVelocity;
        dataRow["decay_spin"] = (object) this.sp.DecaySpin;
        dataRow["backdrop_asset"] = (object) this.sp.BackdropAsset;
        dataRow["system_id"] = (object) this.sp.SystemID;
        dataRow["galaxy_x"] = (object) this.sp.GalaxyX;
        dataRow["galaxy_y"] = (object) this.sp.GalaxyY;
        dataRow["galaxy_z"] = (object) this.sp.GalaxyZ;
        sectorTable.Rows.Add(dataRow);
        string rowNameById = mainFrm.systems.findRowNameByID(int.Parse(dataRow["system_id"].ToString()));
        foreach (TreeNode node1 in this.tree.Nodes)
        {
          if (node1.Text == rowNameById)
          {
            TreeNode node2 = new TreeNode(text);
            node1.Nodes.Add(node2);
          }
        }
        this.sectorsSQL.newRow(dataRow);
        mainFrm.systemWindow.newSector(dataRow);
        this.Close();
      }
    }
  }
}
