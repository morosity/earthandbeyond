﻿// Decompiled with JetBrains decompiler
// Type: N7.GUI.Destination
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace N7.GUI
{
  public class Destination : Form
  {
    public IWindowsFormsEditorService _wfes;
    public int selectedID;
    public int pgID;
    private IContainer components;
    private TableLayoutPanel tableLayoutPanel1;
    private Label label1;
    private ComboBox comboBox1;
    private DataGridView dataGridView1;
    private Button button2;
    private Button button1;

    public Destination()
    {
      this.InitializeComponent();
    }

    private void Destination_Load(object sender, EventArgs e)
    {
    }

    private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
    {
      int selectedIndex = this.comboBox1.SelectedIndex;
      string query = "";
      switch (selectedIndex)
      {
        case 0:
          query = "SELECT sector_id, name FROM sectors order by name";
          break;
        case 1:
          query = "SELECT sector_object_id, name FROM sector_objects where sector_id='" + (object) mainFrm.sectorID + "' order by name";
          break;
      }
      this.dataGridView1.DataSource = (object) Database.executeQuery(Database.DatabaseName.net7, query);
    }

    private void button2_Click(object sender, EventArgs e)
    {
      this.selectedID = this.pgID;
      this.Close();
    }

    private void button1_Click(object sender, EventArgs e)
    {
      foreach (DataGridViewRow selectedRow in (BaseCollection) this.dataGridView1.SelectedRows)
        this.selectedID = int.Parse(selectedRow.Cells[0].Value.ToString());
      this.Close();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      DataGridViewCellStyle gridViewCellStyle = new DataGridViewCellStyle();
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (Destination));
      this.tableLayoutPanel1 = new TableLayoutPanel();
      this.label1 = new Label();
      this.comboBox1 = new ComboBox();
      this.dataGridView1 = new DataGridView();
      this.button1 = new Button();
      this.button2 = new Button();
      this.tableLayoutPanel1.SuspendLayout();
      ((ISupportInitialize) this.dataGridView1).BeginInit();
      this.SuspendLayout();
      this.tableLayoutPanel1.ColumnCount = 5;
      this.tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle());
      this.tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle());
      this.tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100f));
      this.tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle());
      this.tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle());
      this.tableLayoutPanel1.Controls.Add((Control) this.label1, 0, 0);
      this.tableLayoutPanel1.Controls.Add((Control) this.comboBox1, 1, 0);
      this.tableLayoutPanel1.Controls.Add((Control) this.dataGridView1, 0, 1);
      this.tableLayoutPanel1.Controls.Add((Control) this.button2, 3, 2);
      this.tableLayoutPanel1.Controls.Add((Control) this.button1, 4, 2);
      this.tableLayoutPanel1.Dock = DockStyle.Fill;
      this.tableLayoutPanel1.Location = new Point(0, 0);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 3;
      this.tableLayoutPanel1.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 100f));
      this.tableLayoutPanel1.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel1.Size = new Size(514, 307);
      this.tableLayoutPanel1.TabIndex = 0;
      this.label1.AutoSize = true;
      this.label1.Location = new Point(5, 8);
      this.label1.Margin = new Padding(5, 8, 5, 5);
      this.label1.Name = "label1";
      this.label1.Size = new Size(67, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Select Type:";
      this.comboBox1.FormattingEnabled = true;
      this.comboBox1.Items.AddRange(new object[2]
      {
        (object) "Sectors",
        (object) "Current Sector Objects"
      });
      this.comboBox1.Location = new Point(82, 5);
      this.comboBox1.Margin = new Padding(5);
      this.comboBox1.Name = "comboBox1";
      this.comboBox1.Size = new Size(163, 21);
      this.comboBox1.TabIndex = 1;
      this.comboBox1.SelectedIndexChanged += new EventHandler(this.comboBox1_SelectedIndexChanged);
      this.dataGridView1.AllowUserToAddRows = false;
      this.dataGridView1.AllowUserToDeleteRows = false;
      this.dataGridView1.AllowUserToOrderColumns = true;
      this.dataGridView1.AllowUserToResizeRows = false;
      gridViewCellStyle.BackColor = Color.FromArgb(224, 224, 224);
      this.dataGridView1.AlternatingRowsDefaultCellStyle = gridViewCellStyle;
      this.dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
      this.dataGridView1.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.AllCells;
      this.dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.tableLayoutPanel1.SetColumnSpan((Control) this.dataGridView1, 5);
      this.dataGridView1.Dock = DockStyle.Fill;
      this.dataGridView1.Location = new Point(3, 34);
      this.dataGridView1.MultiSelect = false;
      this.dataGridView1.Name = "dataGridView1";
      this.dataGridView1.ReadOnly = true;
      this.dataGridView1.RowHeadersVisible = false;
      this.dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
      this.dataGridView1.Size = new Size(508, 241);
      this.dataGridView1.TabIndex = 2;
      this.button1.Location = new Point(436, 281);
      this.button1.Name = "button1";
      this.button1.Size = new Size(75, 23);
      this.button1.TabIndex = 3;
      this.button1.Text = "Finish";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new EventHandler(this.button1_Click);
      this.button2.Location = new Point(355, 281);
      this.button2.Name = "button2";
      this.button2.Size = new Size(75, 23);
      this.button2.TabIndex = 4;
      this.button2.Text = "Cancel";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new EventHandler(this.button2_Click);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(514, 307);
      this.Controls.Add((Control) this.tableLayoutPanel1);
      this.FormBorderStyle = FormBorderStyle.SizableToolWindow;
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.Name = nameof (Destination);
      this.SizeGripStyle = SizeGripStyle.Show;
      this.Text = nameof (Destination);
      this.Load += new EventHandler(this.Destination_Load);
      this.tableLayoutPanel1.ResumeLayout(false);
      this.tableLayoutPanel1.PerformLayout();
      ((ISupportInitialize) this.dataGridView1).EndInit();
      this.ResumeLayout(false);
    }
  }
}
