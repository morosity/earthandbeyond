﻿// Decompiled with JetBrains decompiler
// Type: N7.GUI.Settings
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace N7.GUI
{
  public class Settings : Form
  {
    private IContainer components;
    private TableLayoutPanel tableLayoutPanel1;
    private Button button1;
    private GroupBox groupBox1;
    private ComboBox comboBox1;
    private Label label1;
    private ComboBox comboBox2;
    private Label label2;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (Settings));
      this.tableLayoutPanel1 = new TableLayoutPanel();
      this.button1 = new Button();
      this.groupBox1 = new GroupBox();
      this.comboBox2 = new ComboBox();
      this.label2 = new Label();
      this.comboBox1 = new ComboBox();
      this.label1 = new Label();
      this.tableLayoutPanel1.SuspendLayout();
      this.groupBox1.SuspendLayout();
      this.SuspendLayout();
      this.tableLayoutPanel1.ColumnCount = 3;
      this.tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100f));
      this.tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle());
      this.tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle());
      this.tableLayoutPanel1.Controls.Add((Control) this.button1, 2, 1);
      this.tableLayoutPanel1.Controls.Add((Control) this.groupBox1, 0, 0);
      this.tableLayoutPanel1.Dock = DockStyle.Fill;
      this.tableLayoutPanel1.Location = new Point(0, 0);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 2;
      this.tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 100f));
      this.tableLayoutPanel1.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel1.Size = new Size(295, 128);
      this.tableLayoutPanel1.TabIndex = 0;
      this.button1.Location = new Point(217, 102);
      this.button1.Name = "button1";
      this.button1.Size = new Size(75, 23);
      this.button1.TabIndex = 0;
      this.button1.Text = "Finish";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new EventHandler(this.button1_Click);
      this.tableLayoutPanel1.SetColumnSpan((Control) this.groupBox1, 3);
      this.groupBox1.Controls.Add((Control) this.comboBox2);
      this.groupBox1.Controls.Add((Control) this.label2);
      this.groupBox1.Controls.Add((Control) this.comboBox1);
      this.groupBox1.Controls.Add((Control) this.label1);
      this.groupBox1.Dock = DockStyle.Fill;
      this.groupBox1.Location = new Point(3, 3);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new Size(289, 93);
      this.groupBox1.TabIndex = 2;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "General Settings";
      this.comboBox2.FormattingEnabled = true;
      this.comboBox2.Items.AddRange(new object[2]
      {
        (object) "Mouse Wheel",
        (object) "Right Click Drag"
      });
      this.comboBox2.Location = new Point(79, 52);
      this.comboBox2.Name = "comboBox2";
      this.comboBox2.Size = new Size(192, 21);
      this.comboBox2.TabIndex = 3;
      this.comboBox2.SelectedIndexChanged += new EventHandler(this.comboBox2_SelectedIndexChanged);
      this.label2.AutoSize = true;
      this.label2.Location = new Point(9, 55);
      this.label2.Name = "label2";
      this.label2.Size = new Size(64, 13);
      this.label2.TabIndex = 2;
      this.label2.Text = "Zoom Type:";
      this.comboBox1.FormattingEnabled = true;
      this.comboBox1.Items.AddRange(new object[2]
      {
        (object) "False",
        (object) "True"
      });
      this.comboBox1.Location = new Point(150, 25);
      this.comboBox1.Name = "comboBox1";
      this.comboBox1.Size = new Size(121, 21);
      this.comboBox1.TabIndex = 1;
      this.comboBox1.SelectedIndexChanged += new EventHandler(this.comboBox1_SelectedIndexChanged);
      this.label1.AutoSize = true;
      this.label1.Location = new Point(9, 28);
      this.label1.Name = "label1";
      this.label1.Size = new Size(135, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Keep Visualization Options:";
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(295, 128);
      this.Controls.Add((Control) this.tableLayoutPanel1);
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.Name = nameof (Settings);
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = nameof (Settings);
      this.Load += new EventHandler(this.Settings_Load);
      this.tableLayoutPanel1.ResumeLayout(false);
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.ResumeLayout(false);
    }

    public Settings()
    {
      this.InitializeComponent();
    }

    private void Settings_Load(object sender, EventArgs e)
    {
      int int32 = Convert.ToInt32(N7.Properties.Settings.Default.KeepVisualizationSetting);
      int zoomSelection = N7.Properties.Settings.Default.zoomSelection;
      this.comboBox1.SelectedIndex = int32;
      this.comboBox2.SelectedIndex = zoomSelection;
    }

    private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
    {
      switch (int.Parse(this.comboBox1.SelectedIndex.ToString()))
      {
        case 0:
          N7.Properties.Settings.Default.KeepVisualizationSetting = false;
          N7.Properties.Settings.Default.Save();
          break;
        case 1:
          N7.Properties.Settings.Default.KeepVisualizationSetting = true;
          N7.Properties.Settings.Default.Save();
          break;
      }
    }

    private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
    {
      switch (int.Parse(this.comboBox2.SelectedIndex.ToString()))
      {
        case 0:
          N7.Properties.Settings.Default.zoomSelection = 0;
          N7.Properties.Settings.Default.Save();
          break;
        case 1:
          N7.Properties.Settings.Default.zoomSelection = 1;
          N7.Properties.Settings.Default.Save();
          break;
      }
    }

    private void button1_Click(object sender, EventArgs e)
    {
      this.Close();
    }
  }
}
