﻿// Decompiled with JetBrains decompiler
// Type: N7.GUI.frmContrast
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace N7.GUI
{
  public class frmContrast : Form
  {
    public int BarValue;
    public IWindowsFormsEditorService _wfes;
    public TrackBar trackBar1;
    private Button btnTrack;
    private Container components;

    public frmContrast()
    {
      this.InitializeComponent();
      this.TopLevel = false;
      this.btnTrack.Text = this.BarValue.ToString();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.trackBar1 = new TrackBar();
      this.btnTrack = new Button();
      this.trackBar1.BeginInit();
      this.SuspendLayout();
      this.trackBar1.LargeChange = 10;
      this.trackBar1.Location = new Point(0, 8);
      this.trackBar1.Maximum = 100;
      this.trackBar1.Name = "trackBar1";
      this.trackBar1.Size = new Size(152, 45);
      this.trackBar1.TabIndex = 0;
      this.trackBar1.TickFrequency = 10;
      this.trackBar1.TickStyle = TickStyle.Both;
      this.trackBar1.ValueChanged += new EventHandler(this.trackBar1_ValueChanged);
      this.btnTrack.DialogResult = DialogResult.OK;
      this.btnTrack.Location = new Point(150, 16);
      this.btnTrack.Name = "btnTrack";
      this.btnTrack.Size = new Size(35, 23);
      this.btnTrack.TabIndex = 2;
      this.btnTrack.Text = "45";
      this.btnTrack.TextAlign = ContentAlignment.MiddleLeft;
      this.btnTrack.Click += new EventHandler(this.btnTrack_Click);
      this.AcceptButton = (IButtonControl) this.btnTrack;
      this.AutoScaleBaseSize = new Size(5, 13);
      this.ClientSize = new Size(192, 70);
      this.ControlBox = false;
      this.Controls.Add((Control) this.btnTrack);
      this.Controls.Add((Control) this.trackBar1);
      this.FormBorderStyle = FormBorderStyle.None;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = nameof (frmContrast);
      this.ShowInTaskbar = false;
      this.Closed += new EventHandler(this.frmContrast_Closed);
      this.trackBar1.EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    private void trackBar1_ValueChanged(object sender, EventArgs e)
    {
      this.BarValue = this.trackBar1.Value;
      this.btnTrack.Text = this.BarValue.ToString();
    }

    private void btnTrack_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void frmContrast_Closed(object sender, EventArgs e)
    {
      this._wfes.CloseDropDown();
    }
  }
}
