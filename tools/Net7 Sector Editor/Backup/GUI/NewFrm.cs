﻿// Decompiled with JetBrains decompiler
// Type: N7.GUI.NewFrm
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace N7.GUI
{
  public class NewFrm : Form
  {
    private TreeView tree;
    private SectorWindow sw;
    private IContainer components;
    private ComboBox comboBox1;
    private Label label1;
    private Button button1;
    private Button button2;

    public NewFrm(TreeView t1, SectorWindow s1)
    {
      this.tree = t1;
      this.sw = s1;
      this.InitializeComponent();
    }

    private void NewFrm_Load(object sender, EventArgs e)
    {
      this.comboBox1.Items.Add((object) "System");
      this.comboBox1.Items.Add((object) "Sector");
      this.comboBox1.Items.Add((object) "Sector Object");
    }

    private void button1_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void button2_Click(object sender, EventArgs e)
    {
      try
      {
        this.Hide();
        if (this.comboBox1.SelectedItem.ToString() == "System")
        {
          int num1 = (int) new NewSystem(this.tree).ShowDialog();
        }
        else if (this.comboBox1.SelectedItem.ToString() == "Sector")
        {
          int num2 = (int) new NewSector(this.tree).ShowDialog();
        }
        else if (this.comboBox1.SelectedItem.ToString() == "Sector Object")
        {
          int num3 = (int) new NewSectorObjectType(this.tree, this.sw).ShowDialog();
        }
        this.Close();
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("Please Select an option from the dropdown menu.");
      }
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (NewFrm));
      this.comboBox1 = new ComboBox();
      this.label1 = new Label();
      this.button1 = new Button();
      this.button2 = new Button();
      this.SuspendLayout();
      this.comboBox1.FormattingEnabled = true;
      this.comboBox1.Location = new Point(15, 38);
      this.comboBox1.Name = "comboBox1";
      this.comboBox1.Size = new Size(217, 21);
      this.comboBox1.TabIndex = 0;
      this.label1.AutoSize = true;
      this.label1.Location = new Point(12, 22);
      this.label1.Name = "label1";
      this.label1.Size = new Size(117, 13);
      this.label1.TabIndex = 1;
      this.label1.Text = "New Document Type...";
      this.button1.Location = new Point(29, 75);
      this.button1.Name = "button1";
      this.button1.Size = new Size(75, 23);
      this.button1.TabIndex = 2;
      this.button1.Text = "Cancel";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new EventHandler(this.button1_Click);
      this.button2.Location = new Point(137, 75);
      this.button2.Name = "button2";
      this.button2.Size = new Size(75, 23);
      this.button2.TabIndex = 3;
      this.button2.Text = "Next...";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new EventHandler(this.button2_Click);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(246, 110);
      this.Controls.Add((Control) this.button2);
      this.Controls.Add((Control) this.button1);
      this.Controls.Add((Control) this.label1);
      this.Controls.Add((Control) this.comboBox1);
      this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = nameof (NewFrm);
      this.SizeGripStyle = SizeGripStyle.Hide;
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "New Document";
      this.Load += new EventHandler(this.NewFrm_Load);
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
