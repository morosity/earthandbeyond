﻿// Decompiled with JetBrains decompiler
// Type: N7.GUI.OptionsGui
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace N7.GUI
{
  public class OptionsGui : UserControl
  {
    private SectorWindow sectorWindow;
    private IContainer components;
    private GroupBox groupBox1;
    private CheckBox mobsNT2;
    private CheckBox mobsNT1;
    private CheckBox mobsNT0;
    private Label label1;
    private CheckBox mobsTextOverlay;
    private CheckBox mobsRadarRange;
    private CheckBox mobsSignature;
    private CheckBox mobsLayer;
    private GroupBox groupBox2;
    private CheckBox planetsNT2;
    private CheckBox planetsNT1;
    private CheckBox planetsNT0;
    private Label label2;
    private CheckBox planetsTextOverlay;
    private CheckBox planetsRadarRange;
    private CheckBox planetsSignature;
    private CheckBox planetsLayer;
    private GroupBox groupBox3;
    private CheckBox stargatesNT2;
    private CheckBox stargatesNT1;
    private CheckBox stargatesNT0;
    private Label label3;
    private CheckBox stargatesTextOverlay;
    private CheckBox stargatesRadarRange;
    private CheckBox stargatesSignature;
    private CheckBox stargatesLayer;
    private GroupBox groupBox4;
    private CheckBox starbasesNT2;
    private CheckBox starbasesNT1;
    private CheckBox starbasesNT0;
    private Label label4;
    private CheckBox starbasesTextOverlay;
    private CheckBox starbasesRadarRange;
    private CheckBox starbasesSignature;
    private CheckBox starbasesLayer;
    private GroupBox groupBox5;
    private CheckBox decoNT2;
    private CheckBox decoNT1;
    private CheckBox decoNT0;
    private Label label5;
    private CheckBox decorationsTextOverlay;
    private CheckBox decorationsRadarRange;
    private CheckBox decorationsSignature;
    private CheckBox decorationsLayer;
    private GroupBox groupBox6;
    private CheckBox harvNT2;
    private CheckBox harvNT1;
    private CheckBox harvNT0;
    private Label label6;
    private CheckBox harvestablesTextOverlay;
    private CheckBox harvestablesRadarRange;
    private CheckBox harvestablesSignature;
    private CheckBox harvestablesLayer;
    private FlowLayoutPanel flowLayoutPanel1;
    private CheckBox mobsAIR;
    private CheckBox planetsAIR;
    private CheckBox stargatesAIR;
    private CheckBox starbasesAIR;
    private CheckBox decoAIR;
    private CheckBox harvAIR;
    private CheckBox mobsExpRange;
    private CheckBox planetsExpRange;
    private CheckBox stargatesExpRange;
    private CheckBox starbasesExpRange;
    private CheckBox decoExpRange;
    private CheckBox harvExpRange;

    public OptionsGui(SectorWindow sw)
    {
      this.sectorWindow = sw;
      this.InitializeComponent();
    }

    private void OptionsGui_Load(object sender, EventArgs e)
    {
      bool mobsLayer = N7.Properties.Settings.Default.MobsLayer;
      bool planetsLayer = N7.Properties.Settings.Default.PlanetsLayer;
      bool stargatesLayer = N7.Properties.Settings.Default.StargatesLayer;
      bool starbasesLayer = N7.Properties.Settings.Default.StarbasesLayer;
      bool decorationsLayer = N7.Properties.Settings.Default.DecorationsLayer;
      bool harvestablesLayer = N7.Properties.Settings.Default.HarvestablesLayer;
      this.mobsLayer.Checked = mobsLayer;
      this.planetsLayer.Checked = planetsLayer;
      this.stargatesLayer.Checked = stargatesLayer;
      this.starbasesLayer.Checked = starbasesLayer;
      this.decorationsLayer.Checked = decorationsLayer;
      this.harvestablesLayer.Checked = harvestablesLayer;
    }

    public void setSectorWindow(SectorWindow sw)
    {
      this.sectorWindow = sw;
    }

    public void loadAll()
    {
      this.mobsAIR_CheckedChanged((object) null, (EventArgs) null);
      this.mobsExpRange_CheckedChanged((object) null, (EventArgs) null);
      this.mobsNT0_CheckedChanged((object) null, (EventArgs) null);
      this.mobsNT1_CheckedChanged((object) null, (EventArgs) null);
      this.mobsNT2_CheckedChanged((object) null, (EventArgs) null);
      this.mobsRadarRange_CheckedChanged((object) null, (EventArgs) null);
      this.mobsSignature_CheckedChanged((object) null, (EventArgs) null);
      this.mobsTextOverlay_CheckedChanged((object) null, (EventArgs) null);
      this.starbasesAIR_CheckedChanged((object) null, (EventArgs) null);
      this.starbasesExpRange_CheckedChanged((object) null, (EventArgs) null);
      this.starbasesNT0_CheckedChanged((object) null, (EventArgs) null);
      this.starbasesNT1_CheckedChanged((object) null, (EventArgs) null);
      this.starbasesNT2_CheckedChanged((object) null, (EventArgs) null);
      this.starbasesRadarRange_CheckedChanged((object) null, (EventArgs) null);
      this.starbasesSignature_CheckedChanged((object) null, (EventArgs) null);
      this.starbasesTextOverlay_CheckedChanged((object) null, (EventArgs) null);
      this.stargatesAIR_CheckedChanged((object) null, (EventArgs) null);
      this.stargatesExpRange_CheckedChanged((object) null, (EventArgs) null);
      this.stargatesNT0_CheckedChanged((object) null, (EventArgs) null);
      this.stargatesNT1_CheckedChanged((object) null, (EventArgs) null);
      this.stargatesNT2_CheckedChanged((object) null, (EventArgs) null);
      this.stargatesRadarRange_CheckedChanged((object) null, (EventArgs) null);
      this.stargatesSignature_CheckedChanged((object) null, (EventArgs) null);
      this.stargatesTextOverlay_CheckedChanged((object) null, (EventArgs) null);
      this.decoAIR_CheckedChanged((object) null, (EventArgs) null);
      this.decoExpRange_CheckedChanged((object) null, (EventArgs) null);
      this.decoNT0_CheckedChanged((object) null, (EventArgs) null);
      this.decoNT1_CheckedChanged((object) null, (EventArgs) null);
      this.decoNT2_CheckedChanged((object) null, (EventArgs) null);
      this.decorationsRadarRange_CheckedChanged((object) null, (EventArgs) null);
      this.decorationsSignature_CheckedChanged((object) null, (EventArgs) null);
      this.decorationsTextOverlay_CheckedChanged((object) null, (EventArgs) null);
      this.harvAIR_CheckedChanged((object) null, (EventArgs) null);
      this.harvExpRange_CheckedChanged((object) null, (EventArgs) null);
      this.harvNT0_CheckedChanged((object) null, (EventArgs) null);
      this.harvNT1_CheckedChanged((object) null, (EventArgs) null);
      this.harvNT2_CheckedChanged((object) null, (EventArgs) null);
      this.harvestablesRadarRange_CheckedChanged((object) null, (EventArgs) null);
      this.harvestablesSignature_CheckedChanged((object) null, (EventArgs) null);
      this.harvestablesTextOverlay_CheckedChanged((object) null, (EventArgs) null);
    }

    private void mobsLayer_CheckedChanged(object sender, EventArgs e)
    {
      if (this.mobsLayer.Checked)
      {
        N7.Properties.Settings.Default.MobsLayer = true;
        N7.Properties.Settings.Default.Save();
        this.sectorWindow.showLayer(0);
      }
      else
      {
        N7.Properties.Settings.Default.MobsLayer = false;
        N7.Properties.Settings.Default.Save();
        this.sectorWindow.hideLayer(0);
      }
    }

    private void mobsTextOverlay_CheckedChanged(object sender, EventArgs e)
    {
      if (this.mobsTextOverlay.Checked)
        this.sectorWindow.turnOnText(0);
      else
        this.sectorWindow.turnOffText(0);
    }

    private void mobsRadarRange_CheckedChanged(object sender, EventArgs e)
    {
      if (this.mobsRadarRange.Checked)
        this.sectorWindow.radarRangeOn(0);
      else
        this.sectorWindow.radarRangeOff(0);
    }

    private void mobsSignature_CheckedChanged(object sender, EventArgs e)
    {
      if (this.mobsSignature.Checked)
        this.sectorWindow.SignatureOn(0);
      else
        this.sectorWindow.SignatureOff(0);
    }

    private void mobsNT0_CheckedChanged(object sender, EventArgs e)
    {
      if (this.mobsNT0.Checked)
        this.sectorWindow.navTypeZeroOn(0);
      else
        this.sectorWindow.navTypeZeroOff(0);
    }

    private void mobsNT1_CheckedChanged(object sender, EventArgs e)
    {
      if (this.mobsNT1.Checked)
        this.sectorWindow.navTypeOneOn(0);
      else
        this.sectorWindow.navTypeOneOff(0);
    }

    private void mobsNT2_CheckedChanged(object sender, EventArgs e)
    {
      if (this.mobsNT2.Checked)
        this.sectorWindow.navTypeTwoOn(0);
      else
        this.sectorWindow.navTypeTwoOff(0);
    }

    private void mobsAIR_CheckedChanged(object sender, EventArgs e)
    {
      if (this.mobsAIR.Checked)
        this.sectorWindow.appearsInRadarOn(0);
      else
        this.sectorWindow.appearsInRadarOff(0);
    }

    private void mobsExpRange_CheckedChanged(object sender, EventArgs e)
    {
      if (this.mobsExpRange.Checked)
        this.sectorWindow.explorationRangeOn(0);
      else
        this.sectorWindow.explorationRangeOff(0);
    }

    private void planetsLayer_CheckedChanged(object sender, EventArgs e)
    {
      if (this.planetsLayer.Checked)
      {
        N7.Properties.Settings.Default.PlanetsLayer = true;
        N7.Properties.Settings.Default.Save();
        this.sectorWindow.showLayer(3);
      }
      else
      {
        N7.Properties.Settings.Default.PlanetsLayer = false;
        N7.Properties.Settings.Default.Save();
        this.sectorWindow.hideLayer(3);
      }
    }

    private void planetsTextOverlay_CheckedChanged(object sender, EventArgs e)
    {
      if (this.planetsTextOverlay.Checked)
        this.sectorWindow.turnOnText(3);
      else
        this.sectorWindow.turnOffText(3);
    }

    private void planetsRadarRange_CheckedChanged(object sender, EventArgs e)
    {
      if (this.planetsRadarRange.Checked)
        this.sectorWindow.radarRangeOn(3);
      else
        this.sectorWindow.radarRangeOff(3);
    }

    private void planetsSignature_CheckedChanged(object sender, EventArgs e)
    {
      if (this.planetsSignature.Checked)
        this.sectorWindow.SignatureOn(3);
      else
        this.sectorWindow.SignatureOff(3);
    }

    private void planetsNT0_CheckedChanged(object sender, EventArgs e)
    {
      if (this.planetsNT0.Checked)
        this.sectorWindow.navTypeZeroOn(3);
      else
        this.sectorWindow.navTypeZeroOff(3);
    }

    private void planetsNT1_CheckedChanged(object sender, EventArgs e)
    {
      if (this.planetsNT1.Checked)
        this.sectorWindow.navTypeOneOn(3);
      else
        this.sectorWindow.navTypeOneOff(3);
    }

    private void planetsNT2_CheckedChanged(object sender, EventArgs e)
    {
      if (this.planetsNT2.Checked)
        this.sectorWindow.navTypeTwoOn(3);
      else
        this.sectorWindow.navTypeTwoOff(3);
    }

    private void planetsAIR_CheckedChanged(object sender, EventArgs e)
    {
      if (this.planetsAIR.Checked)
        this.sectorWindow.appearsInRadarOn(3);
      else
        this.sectorWindow.appearsInRadarOff(3);
    }

    private void planetsExpRange_CheckedChanged(object sender, EventArgs e)
    {
      if (this.planetsExpRange.Checked)
        this.sectorWindow.explorationRangeOn(3);
      else
        this.sectorWindow.explorationRangeOff(3);
    }

    private void stargatesLayer_CheckedChanged(object sender, EventArgs e)
    {
      if (this.stargatesLayer.Checked)
      {
        N7.Properties.Settings.Default.StargatesLayer = true;
        N7.Properties.Settings.Default.Save();
        this.sectorWindow.showLayer(11);
      }
      else
      {
        N7.Properties.Settings.Default.StargatesLayer = false;
        N7.Properties.Settings.Default.Save();
        this.sectorWindow.hideLayer(11);
      }
    }

    private void stargatesTextOverlay_CheckedChanged(object sender, EventArgs e)
    {
      if (this.stargatesTextOverlay.Checked)
        this.sectorWindow.turnOnText(11);
      else
        this.sectorWindow.turnOffText(11);
    }

    private void stargatesRadarRange_CheckedChanged(object sender, EventArgs e)
    {
      if (this.stargatesRadarRange.Checked)
        this.sectorWindow.radarRangeOn(11);
      else
        this.sectorWindow.radarRangeOff(11);
    }

    private void stargatesSignature_CheckedChanged(object sender, EventArgs e)
    {
      if (this.stargatesSignature.Checked)
        this.sectorWindow.SignatureOn(11);
      else
        this.sectorWindow.SignatureOff(11);
    }

    private void stargatesNT0_CheckedChanged(object sender, EventArgs e)
    {
      if (this.stargatesNT0.Checked)
        this.sectorWindow.navTypeZeroOn(11);
      else
        this.sectorWindow.navTypeZeroOff(11);
    }

    private void stargatesNT1_CheckedChanged(object sender, EventArgs e)
    {
      if (this.stargatesNT1.Checked)
        this.sectorWindow.navTypeOneOn(11);
      else
        this.sectorWindow.navTypeOneOff(11);
    }

    private void stargatesNT2_CheckedChanged(object sender, EventArgs e)
    {
      if (this.stargatesNT2.Checked)
        this.sectorWindow.navTypeTwoOn(11);
      else
        this.sectorWindow.navTypeTwoOff(11);
    }

    private void stargatesAIR_CheckedChanged(object sender, EventArgs e)
    {
      if (this.stargatesAIR.Checked)
        this.sectorWindow.appearsInRadarOn(11);
      else
        this.sectorWindow.appearsInRadarOff(11);
    }

    private void stargatesExpRange_CheckedChanged(object sender, EventArgs e)
    {
      if (this.stargatesExpRange.Checked)
        this.sectorWindow.explorationRangeOn(11);
      else
        this.sectorWindow.explorationRangeOff(11);
    }

    private void starbasesLayer_CheckedChanged(object sender, EventArgs e)
    {
      if (this.starbasesLayer.Checked)
      {
        N7.Properties.Settings.Default.StarbasesLayer = true;
        N7.Properties.Settings.Default.Save();
        this.sectorWindow.showLayer(12);
      }
      else
      {
        N7.Properties.Settings.Default.StarbasesLayer = false;
        N7.Properties.Settings.Default.Save();
        this.sectorWindow.hideLayer(12);
      }
    }

    private void starbasesTextOverlay_CheckedChanged(object sender, EventArgs e)
    {
      if (this.starbasesTextOverlay.Checked)
        this.sectorWindow.turnOnText(12);
      else
        this.sectorWindow.turnOffText(12);
    }

    private void starbasesRadarRange_CheckedChanged(object sender, EventArgs e)
    {
      if (this.starbasesRadarRange.Checked)
        this.sectorWindow.radarRangeOn(12);
      else
        this.sectorWindow.radarRangeOff(12);
    }

    private void starbasesSignature_CheckedChanged(object sender, EventArgs e)
    {
      if (this.starbasesSignature.Checked)
        this.sectorWindow.SignatureOn(12);
      else
        this.sectorWindow.SignatureOff(12);
    }

    private void starbasesNT0_CheckedChanged(object sender, EventArgs e)
    {
      if (this.starbasesNT0.Checked)
        this.sectorWindow.navTypeZeroOn(12);
      else
        this.sectorWindow.navTypeZeroOff(12);
    }

    private void starbasesNT1_CheckedChanged(object sender, EventArgs e)
    {
      if (this.starbasesNT1.Checked)
        this.sectorWindow.navTypeOneOn(12);
      else
        this.sectorWindow.navTypeOneOff(12);
    }

    private void starbasesNT2_CheckedChanged(object sender, EventArgs e)
    {
      if (this.starbasesNT2.Checked)
        this.sectorWindow.navTypeTwoOn(12);
      else
        this.sectorWindow.navTypeTwoOff(12);
    }

    private void starbasesAIR_CheckedChanged(object sender, EventArgs e)
    {
      if (this.starbasesAIR.Checked)
        this.sectorWindow.appearsInRadarOn(12);
      else
        this.sectorWindow.appearsInRadarOff(12);
    }

    private void starbasesExpRange_CheckedChanged(object sender, EventArgs e)
    {
      if (this.starbasesExpRange.Checked)
        this.sectorWindow.explorationRangeOn(12);
      else
        this.sectorWindow.explorationRangeOff(12);
    }

    private void decorationsLayer_CheckedChanged(object sender, EventArgs e)
    {
      if (this.decorationsLayer.Checked)
      {
        N7.Properties.Settings.Default.DecorationsLayer = true;
        N7.Properties.Settings.Default.Save();
        this.sectorWindow.showLayer(37);
      }
      else
      {
        N7.Properties.Settings.Default.DecorationsLayer = false;
        N7.Properties.Settings.Default.Save();
        this.sectorWindow.hideLayer(37);
      }
    }

    private void decorationsTextOverlay_CheckedChanged(object sender, EventArgs e)
    {
      if (this.decorationsTextOverlay.Checked)
        this.sectorWindow.turnOnText(37);
      else
        this.sectorWindow.turnOffText(37);
    }

    private void decorationsRadarRange_CheckedChanged(object sender, EventArgs e)
    {
      if (this.decorationsRadarRange.Checked)
        this.sectorWindow.radarRangeOn(37);
      else
        this.sectorWindow.radarRangeOff(37);
    }

    private void decorationsSignature_CheckedChanged(object sender, EventArgs e)
    {
      if (this.decorationsSignature.Checked)
        this.sectorWindow.SignatureOn(37);
      else
        this.sectorWindow.SignatureOff(37);
    }

    private void decoNT0_CheckedChanged(object sender, EventArgs e)
    {
      if (this.decoNT0.Checked)
        this.sectorWindow.navTypeZeroOn(37);
      else
        this.sectorWindow.navTypeZeroOff(37);
    }

    private void decoNT1_CheckedChanged(object sender, EventArgs e)
    {
      if (this.decoNT1.Checked)
        this.sectorWindow.navTypeOneOn(37);
      else
        this.sectorWindow.navTypeOneOff(37);
    }

    private void decoNT2_CheckedChanged(object sender, EventArgs e)
    {
      if (this.decoNT2.Checked)
        this.sectorWindow.navTypeTwoOn(37);
      else
        this.sectorWindow.navTypeTwoOff(37);
    }

    private void decoAIR_CheckedChanged(object sender, EventArgs e)
    {
      if (this.decoAIR.Checked)
        this.sectorWindow.appearsInRadarOn(37);
      else
        this.sectorWindow.appearsInRadarOff(37);
    }

    private void decoExpRange_CheckedChanged(object sender, EventArgs e)
    {
      if (this.decoExpRange.Checked)
        this.sectorWindow.explorationRangeOn(37);
      else
        this.sectorWindow.explorationRangeOff(37);
    }

    private void harvestablesLayer_CheckedChanged(object sender, EventArgs e)
    {
      if (this.harvestablesLayer.Checked)
      {
        N7.Properties.Settings.Default.HarvestablesLayer = true;
        N7.Properties.Settings.Default.Save();
        this.sectorWindow.showLayer(38);
      }
      else
      {
        N7.Properties.Settings.Default.HarvestablesLayer = false;
        N7.Properties.Settings.Default.Save();
        this.sectorWindow.hideLayer(38);
      }
    }

    private void harvestablesTextOverlay_CheckedChanged(object sender, EventArgs e)
    {
      if (this.harvestablesTextOverlay.Checked)
        this.sectorWindow.turnOnText(38);
      else
        this.sectorWindow.turnOffText(38);
    }

    private void harvestablesRadarRange_CheckedChanged(object sender, EventArgs e)
    {
      if (this.harvestablesRadarRange.Checked)
        this.sectorWindow.radarRangeOn(38);
      else
        this.sectorWindow.radarRangeOff(38);
    }

    private void harvestablesSignature_CheckedChanged(object sender, EventArgs e)
    {
      if (this.harvestablesSignature.Checked)
        this.sectorWindow.SignatureOn(38);
      else
        this.sectorWindow.SignatureOff(38);
    }

    private void harvNT0_CheckedChanged(object sender, EventArgs e)
    {
      if (this.harvNT0.Checked)
        this.sectorWindow.navTypeZeroOn(38);
      else
        this.sectorWindow.navTypeZeroOff(38);
    }

    private void harvNT1_CheckedChanged(object sender, EventArgs e)
    {
      if (this.harvNT1.Checked)
        this.sectorWindow.navTypeOneOn(38);
      else
        this.sectorWindow.navTypeOneOff(38);
    }

    private void harvNT2_CheckedChanged(object sender, EventArgs e)
    {
      if (this.harvNT2.Checked)
        this.sectorWindow.navTypeTwoOn(38);
      else
        this.sectorWindow.navTypeTwoOff(38);
    }

    private void harvAIR_CheckedChanged(object sender, EventArgs e)
    {
      if (this.harvAIR.Checked)
        this.sectorWindow.appearsInRadarOn(38);
      else
        this.sectorWindow.appearsInRadarOff(38);
    }

    private void harvExpRange_CheckedChanged(object sender, EventArgs e)
    {
      if (this.harvExpRange.Checked)
        this.sectorWindow.explorationRangeOn(38);
      else
        this.sectorWindow.explorationRangeOff(38);
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.groupBox1 = new GroupBox();
      this.mobsExpRange = new CheckBox();
      this.mobsAIR = new CheckBox();
      this.mobsNT2 = new CheckBox();
      this.mobsNT1 = new CheckBox();
      this.mobsNT0 = new CheckBox();
      this.label1 = new Label();
      this.mobsTextOverlay = new CheckBox();
      this.mobsRadarRange = new CheckBox();
      this.mobsSignature = new CheckBox();
      this.mobsLayer = new CheckBox();
      this.groupBox2 = new GroupBox();
      this.planetsExpRange = new CheckBox();
      this.planetsAIR = new CheckBox();
      this.planetsNT2 = new CheckBox();
      this.planetsNT1 = new CheckBox();
      this.planetsNT0 = new CheckBox();
      this.label2 = new Label();
      this.planetsTextOverlay = new CheckBox();
      this.planetsRadarRange = new CheckBox();
      this.planetsSignature = new CheckBox();
      this.planetsLayer = new CheckBox();
      this.groupBox3 = new GroupBox();
      this.stargatesExpRange = new CheckBox();
      this.stargatesAIR = new CheckBox();
      this.stargatesNT2 = new CheckBox();
      this.stargatesNT1 = new CheckBox();
      this.stargatesNT0 = new CheckBox();
      this.label3 = new Label();
      this.stargatesTextOverlay = new CheckBox();
      this.stargatesRadarRange = new CheckBox();
      this.stargatesSignature = new CheckBox();
      this.stargatesLayer = new CheckBox();
      this.groupBox4 = new GroupBox();
      this.starbasesExpRange = new CheckBox();
      this.starbasesAIR = new CheckBox();
      this.starbasesNT2 = new CheckBox();
      this.starbasesNT1 = new CheckBox();
      this.starbasesNT0 = new CheckBox();
      this.label4 = new Label();
      this.starbasesTextOverlay = new CheckBox();
      this.starbasesRadarRange = new CheckBox();
      this.starbasesSignature = new CheckBox();
      this.starbasesLayer = new CheckBox();
      this.groupBox5 = new GroupBox();
      this.decoExpRange = new CheckBox();
      this.decoAIR = new CheckBox();
      this.decoNT2 = new CheckBox();
      this.decoNT1 = new CheckBox();
      this.decoNT0 = new CheckBox();
      this.label5 = new Label();
      this.decorationsTextOverlay = new CheckBox();
      this.decorationsRadarRange = new CheckBox();
      this.decorationsSignature = new CheckBox();
      this.decorationsLayer = new CheckBox();
      this.groupBox6 = new GroupBox();
      this.harvExpRange = new CheckBox();
      this.harvAIR = new CheckBox();
      this.harvNT2 = new CheckBox();
      this.harvNT1 = new CheckBox();
      this.harvNT0 = new CheckBox();
      this.label6 = new Label();
      this.harvestablesTextOverlay = new CheckBox();
      this.harvestablesRadarRange = new CheckBox();
      this.harvestablesSignature = new CheckBox();
      this.harvestablesLayer = new CheckBox();
      this.flowLayoutPanel1 = new FlowLayoutPanel();
      this.groupBox1.SuspendLayout();
      this.groupBox2.SuspendLayout();
      this.groupBox3.SuspendLayout();
      this.groupBox4.SuspendLayout();
      this.groupBox5.SuspendLayout();
      this.groupBox6.SuspendLayout();
      this.flowLayoutPanel1.SuspendLayout();
      this.SuspendLayout();
      this.groupBox1.Controls.Add((Control) this.mobsExpRange);
      this.groupBox1.Controls.Add((Control) this.mobsAIR);
      this.groupBox1.Controls.Add((Control) this.mobsNT2);
      this.groupBox1.Controls.Add((Control) this.mobsNT1);
      this.groupBox1.Controls.Add((Control) this.mobsNT0);
      this.groupBox1.Controls.Add((Control) this.label1);
      this.groupBox1.Controls.Add((Control) this.mobsTextOverlay);
      this.groupBox1.Controls.Add((Control) this.mobsRadarRange);
      this.groupBox1.Controls.Add((Control) this.mobsSignature);
      this.groupBox1.Controls.Add((Control) this.mobsLayer);
      this.groupBox1.Location = new Point(3, 3);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new Size(233, 110);
      this.groupBox1.TabIndex = 0;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Mobs";
      this.mobsExpRange.AutoSize = true;
      this.mobsExpRange.Checked = true;
      this.mobsExpRange.CheckState = CheckState.Checked;
      this.mobsExpRange.Location = new Point(3, 65);
      this.mobsExpRange.Name = "mobsExpRange";
      this.mobsExpRange.Size = new Size(113, 17);
      this.mobsExpRange.TabIndex = 9;
      this.mobsExpRange.Text = "Exploration Range";
      this.mobsExpRange.UseVisualStyleBackColor = true;
      this.mobsExpRange.CheckedChanged += new EventHandler(this.mobsExpRange_CheckedChanged);
      this.mobsAIR.AutoSize = true;
      this.mobsAIR.Checked = true;
      this.mobsAIR.CheckState = CheckState.Checked;
      this.mobsAIR.Location = new Point(122, 65);
      this.mobsAIR.Name = "mobsAIR";
      this.mobsAIR.Size = new Size(109, 17);
      this.mobsAIR.TabIndex = 8;
      this.mobsAIR.Text = "Appears In Radar";
      this.mobsAIR.UseVisualStyleBackColor = true;
      this.mobsAIR.CheckedChanged += new EventHandler(this.mobsAIR_CheckedChanged);
      this.mobsNT2.AutoSize = true;
      this.mobsNT2.Checked = true;
      this.mobsNT2.CheckState = CheckState.Checked;
      this.mobsNT2.Location = new Point(139, 91);
      this.mobsNT2.Name = "mobsNT2";
      this.mobsNT2.Size = new Size(32, 17);
      this.mobsNT2.TabIndex = 7;
      this.mobsNT2.Text = "2";
      this.mobsNT2.UseVisualStyleBackColor = true;
      this.mobsNT2.CheckedChanged += new EventHandler(this.mobsNT2_CheckedChanged);
      this.mobsNT1.AutoSize = true;
      this.mobsNT1.Checked = true;
      this.mobsNT1.CheckState = CheckState.Checked;
      this.mobsNT1.Location = new Point(101, 91);
      this.mobsNT1.Name = "mobsNT1";
      this.mobsNT1.Size = new Size(32, 17);
      this.mobsNT1.TabIndex = 6;
      this.mobsNT1.Text = "1";
      this.mobsNT1.UseVisualStyleBackColor = true;
      this.mobsNT1.CheckedChanged += new EventHandler(this.mobsNT1_CheckedChanged);
      this.mobsNT0.AutoSize = true;
      this.mobsNT0.Checked = true;
      this.mobsNT0.CheckState = CheckState.Checked;
      this.mobsNT0.Location = new Point(63, 91);
      this.mobsNT0.Name = "mobsNT0";
      this.mobsNT0.Size = new Size(32, 17);
      this.mobsNT0.TabIndex = 5;
      this.mobsNT0.Text = "0";
      this.mobsNT0.UseVisualStyleBackColor = true;
      this.mobsNT0.CheckedChanged += new EventHandler(this.mobsNT0_CheckedChanged);
      this.label1.AutoSize = true;
      this.label1.Location = new Point(3, 91);
      this.label1.Name = "label1";
      this.label1.Size = new Size(57, 13);
      this.label1.TabIndex = 4;
      this.label1.Text = "Nav Type:";
      this.mobsTextOverlay.AutoSize = true;
      this.mobsTextOverlay.Checked = true;
      this.mobsTextOverlay.CheckState = CheckState.Checked;
      this.mobsTextOverlay.Location = new Point(122, 19);
      this.mobsTextOverlay.Name = "mobsTextOverlay";
      this.mobsTextOverlay.Size = new Size(86, 17);
      this.mobsTextOverlay.TabIndex = 3;
      this.mobsTextOverlay.Text = "Text Overlay";
      this.mobsTextOverlay.UseVisualStyleBackColor = true;
      this.mobsTextOverlay.CheckedChanged += new EventHandler(this.mobsTextOverlay_CheckedChanged);
      this.mobsRadarRange.AutoSize = true;
      this.mobsRadarRange.Checked = true;
      this.mobsRadarRange.CheckState = CheckState.Checked;
      this.mobsRadarRange.Location = new Point(6, 42);
      this.mobsRadarRange.Name = "mobsRadarRange";
      this.mobsRadarRange.Size = new Size(90, 17);
      this.mobsRadarRange.TabIndex = 2;
      this.mobsRadarRange.Text = "Radar Range";
      this.mobsRadarRange.UseVisualStyleBackColor = true;
      this.mobsRadarRange.CheckedChanged += new EventHandler(this.mobsRadarRange_CheckedChanged);
      this.mobsSignature.AutoSize = true;
      this.mobsSignature.Checked = true;
      this.mobsSignature.CheckState = CheckState.Checked;
      this.mobsSignature.Location = new Point(122, 42);
      this.mobsSignature.Name = "mobsSignature";
      this.mobsSignature.Size = new Size(71, 17);
      this.mobsSignature.TabIndex = 1;
      this.mobsSignature.Text = "Signature";
      this.mobsSignature.UseVisualStyleBackColor = true;
      this.mobsSignature.CheckedChanged += new EventHandler(this.mobsSignature_CheckedChanged);
      this.mobsLayer.AutoSize = true;
      this.mobsLayer.Checked = true;
      this.mobsLayer.CheckState = CheckState.Checked;
      this.mobsLayer.Location = new Point(6, 19);
      this.mobsLayer.Name = "mobsLayer";
      this.mobsLayer.Size = new Size(52, 17);
      this.mobsLayer.TabIndex = 0;
      this.mobsLayer.Text = "Layer";
      this.mobsLayer.UseVisualStyleBackColor = true;
      this.mobsLayer.CheckedChanged += new EventHandler(this.mobsLayer_CheckedChanged);
      this.groupBox2.Controls.Add((Control) this.planetsExpRange);
      this.groupBox2.Controls.Add((Control) this.planetsAIR);
      this.groupBox2.Controls.Add((Control) this.planetsNT2);
      this.groupBox2.Controls.Add((Control) this.planetsNT1);
      this.groupBox2.Controls.Add((Control) this.planetsNT0);
      this.groupBox2.Controls.Add((Control) this.label2);
      this.groupBox2.Controls.Add((Control) this.planetsTextOverlay);
      this.groupBox2.Controls.Add((Control) this.planetsRadarRange);
      this.groupBox2.Controls.Add((Control) this.planetsSignature);
      this.groupBox2.Controls.Add((Control) this.planetsLayer);
      this.groupBox2.Location = new Point(242, 3);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new Size(234, 110);
      this.groupBox2.TabIndex = 1;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Planets";
      this.planetsExpRange.AutoSize = true;
      this.planetsExpRange.Checked = true;
      this.planetsExpRange.CheckState = CheckState.Checked;
      this.planetsExpRange.Location = new Point(6, 65);
      this.planetsExpRange.Name = "planetsExpRange";
      this.planetsExpRange.Size = new Size(113, 17);
      this.planetsExpRange.TabIndex = 9;
      this.planetsExpRange.Text = "Exploration Range";
      this.planetsExpRange.UseVisualStyleBackColor = true;
      this.planetsExpRange.CheckedChanged += new EventHandler(this.planetsExpRange_CheckedChanged);
      this.planetsAIR.AutoSize = true;
      this.planetsAIR.Checked = true;
      this.planetsAIR.CheckState = CheckState.Checked;
      this.planetsAIR.Location = new Point(124, 65);
      this.planetsAIR.Name = "planetsAIR";
      this.planetsAIR.Size = new Size(109, 17);
      this.planetsAIR.TabIndex = 8;
      this.planetsAIR.Text = "Appears In Radar";
      this.planetsAIR.UseVisualStyleBackColor = true;
      this.planetsAIR.CheckedChanged += new EventHandler(this.planetsAIR_CheckedChanged);
      this.planetsNT2.AutoSize = true;
      this.planetsNT2.Checked = true;
      this.planetsNT2.CheckState = CheckState.Checked;
      this.planetsNT2.Location = new Point(142, 91);
      this.planetsNT2.Name = "planetsNT2";
      this.planetsNT2.Size = new Size(32, 17);
      this.planetsNT2.TabIndex = 7;
      this.planetsNT2.Text = "2";
      this.planetsNT2.UseVisualStyleBackColor = true;
      this.planetsNT2.CheckedChanged += new EventHandler(this.planetsNT2_CheckedChanged);
      this.planetsNT1.AutoSize = true;
      this.planetsNT1.Checked = true;
      this.planetsNT1.CheckState = CheckState.Checked;
      this.planetsNT1.Location = new Point(104, 91);
      this.planetsNT1.Name = "planetsNT1";
      this.planetsNT1.Size = new Size(32, 17);
      this.planetsNT1.TabIndex = 6;
      this.planetsNT1.Text = "1";
      this.planetsNT1.UseVisualStyleBackColor = true;
      this.planetsNT1.CheckedChanged += new EventHandler(this.planetsNT1_CheckedChanged);
      this.planetsNT0.AutoSize = true;
      this.planetsNT0.Checked = true;
      this.planetsNT0.CheckState = CheckState.Checked;
      this.planetsNT0.Location = new Point(66, 91);
      this.planetsNT0.Name = "planetsNT0";
      this.planetsNT0.Size = new Size(32, 17);
      this.planetsNT0.TabIndex = 5;
      this.planetsNT0.Text = "0";
      this.planetsNT0.UseVisualStyleBackColor = true;
      this.planetsNT0.CheckedChanged += new EventHandler(this.planetsNT0_CheckedChanged);
      this.label2.AutoSize = true;
      this.label2.Location = new Point(6, 91);
      this.label2.Name = "label2";
      this.label2.Size = new Size(57, 13);
      this.label2.TabIndex = 4;
      this.label2.Text = "Nav Type:";
      this.planetsTextOverlay.AutoSize = true;
      this.planetsTextOverlay.Checked = true;
      this.planetsTextOverlay.CheckState = CheckState.Checked;
      this.planetsTextOverlay.Location = new Point(124, 19);
      this.planetsTextOverlay.Name = "planetsTextOverlay";
      this.planetsTextOverlay.Size = new Size(86, 17);
      this.planetsTextOverlay.TabIndex = 3;
      this.planetsTextOverlay.Text = "Text Overlay";
      this.planetsTextOverlay.UseVisualStyleBackColor = true;
      this.planetsTextOverlay.CheckedChanged += new EventHandler(this.planetsTextOverlay_CheckedChanged);
      this.planetsRadarRange.AutoSize = true;
      this.planetsRadarRange.Checked = true;
      this.planetsRadarRange.CheckState = CheckState.Checked;
      this.planetsRadarRange.Location = new Point(6, 42);
      this.planetsRadarRange.Name = "planetsRadarRange";
      this.planetsRadarRange.Size = new Size(90, 17);
      this.planetsRadarRange.TabIndex = 2;
      this.planetsRadarRange.Text = "Radar Range";
      this.planetsRadarRange.UseVisualStyleBackColor = true;
      this.planetsRadarRange.CheckedChanged += new EventHandler(this.planetsRadarRange_CheckedChanged);
      this.planetsSignature.AutoSize = true;
      this.planetsSignature.Checked = true;
      this.planetsSignature.CheckState = CheckState.Checked;
      this.planetsSignature.Location = new Point(124, 42);
      this.planetsSignature.Name = "planetsSignature";
      this.planetsSignature.Size = new Size(71, 17);
      this.planetsSignature.TabIndex = 1;
      this.planetsSignature.Text = "Signature";
      this.planetsSignature.UseVisualStyleBackColor = true;
      this.planetsSignature.CheckedChanged += new EventHandler(this.planetsSignature_CheckedChanged);
      this.planetsLayer.AutoSize = true;
      this.planetsLayer.Checked = true;
      this.planetsLayer.CheckState = CheckState.Checked;
      this.planetsLayer.Location = new Point(6, 19);
      this.planetsLayer.Name = "planetsLayer";
      this.planetsLayer.Size = new Size(52, 17);
      this.planetsLayer.TabIndex = 0;
      this.planetsLayer.Text = "Layer";
      this.planetsLayer.UseVisualStyleBackColor = true;
      this.planetsLayer.CheckedChanged += new EventHandler(this.planetsLayer_CheckedChanged);
      this.groupBox3.Controls.Add((Control) this.stargatesExpRange);
      this.groupBox3.Controls.Add((Control) this.stargatesAIR);
      this.groupBox3.Controls.Add((Control) this.stargatesNT2);
      this.groupBox3.Controls.Add((Control) this.stargatesNT1);
      this.groupBox3.Controls.Add((Control) this.stargatesNT0);
      this.groupBox3.Controls.Add((Control) this.label3);
      this.groupBox3.Controls.Add((Control) this.stargatesTextOverlay);
      this.groupBox3.Controls.Add((Control) this.stargatesRadarRange);
      this.groupBox3.Controls.Add((Control) this.stargatesSignature);
      this.groupBox3.Controls.Add((Control) this.stargatesLayer);
      this.groupBox3.Location = new Point(482, 3);
      this.groupBox3.Name = "groupBox3";
      this.groupBox3.Size = new Size(238, 110);
      this.groupBox3.TabIndex = 2;
      this.groupBox3.TabStop = false;
      this.groupBox3.Text = "Stargates";
      this.stargatesExpRange.AutoSize = true;
      this.stargatesExpRange.Checked = true;
      this.stargatesExpRange.CheckState = CheckState.Checked;
      this.stargatesExpRange.Location = new Point(6, 65);
      this.stargatesExpRange.Name = "stargatesExpRange";
      this.stargatesExpRange.Size = new Size(113, 17);
      this.stargatesExpRange.TabIndex = 9;
      this.stargatesExpRange.Text = "Exploration Range";
      this.stargatesExpRange.UseVisualStyleBackColor = true;
      this.stargatesExpRange.CheckedChanged += new EventHandler(this.stargatesExpRange_CheckedChanged);
      this.stargatesAIR.AutoSize = true;
      this.stargatesAIR.Checked = true;
      this.stargatesAIR.CheckState = CheckState.Checked;
      this.stargatesAIR.Location = new Point(125, 65);
      this.stargatesAIR.Name = "stargatesAIR";
      this.stargatesAIR.Size = new Size(109, 17);
      this.stargatesAIR.TabIndex = 8;
      this.stargatesAIR.Text = "Appears In Radar";
      this.stargatesAIR.UseVisualStyleBackColor = true;
      this.stargatesAIR.CheckedChanged += new EventHandler(this.stargatesAIR_CheckedChanged);
      this.stargatesNT2.AutoSize = true;
      this.stargatesNT2.Checked = true;
      this.stargatesNT2.CheckState = CheckState.Checked;
      this.stargatesNT2.Location = new Point(142, 91);
      this.stargatesNT2.Name = "stargatesNT2";
      this.stargatesNT2.Size = new Size(32, 17);
      this.stargatesNT2.TabIndex = 7;
      this.stargatesNT2.Text = "2";
      this.stargatesNT2.UseVisualStyleBackColor = true;
      this.stargatesNT2.CheckedChanged += new EventHandler(this.stargatesNT2_CheckedChanged);
      this.stargatesNT1.AutoSize = true;
      this.stargatesNT1.Checked = true;
      this.stargatesNT1.CheckState = CheckState.Checked;
      this.stargatesNT1.Location = new Point(104, 91);
      this.stargatesNT1.Name = "stargatesNT1";
      this.stargatesNT1.Size = new Size(32, 17);
      this.stargatesNT1.TabIndex = 6;
      this.stargatesNT1.Text = "1";
      this.stargatesNT1.UseVisualStyleBackColor = true;
      this.stargatesNT1.CheckedChanged += new EventHandler(this.stargatesNT1_CheckedChanged);
      this.stargatesNT0.AutoSize = true;
      this.stargatesNT0.Checked = true;
      this.stargatesNT0.CheckState = CheckState.Checked;
      this.stargatesNT0.Location = new Point(66, 91);
      this.stargatesNT0.Name = "stargatesNT0";
      this.stargatesNT0.Size = new Size(32, 17);
      this.stargatesNT0.TabIndex = 5;
      this.stargatesNT0.Text = "0";
      this.stargatesNT0.UseVisualStyleBackColor = true;
      this.stargatesNT0.CheckedChanged += new EventHandler(this.stargatesNT0_CheckedChanged);
      this.label3.AutoSize = true;
      this.label3.Location = new Point(6, 91);
      this.label3.Name = "label3";
      this.label3.Size = new Size(57, 13);
      this.label3.TabIndex = 4;
      this.label3.Text = "Nav Type:";
      this.stargatesTextOverlay.AutoSize = true;
      this.stargatesTextOverlay.Checked = true;
      this.stargatesTextOverlay.CheckState = CheckState.Checked;
      this.stargatesTextOverlay.Location = new Point(125, 19);
      this.stargatesTextOverlay.Name = "stargatesTextOverlay";
      this.stargatesTextOverlay.Size = new Size(86, 17);
      this.stargatesTextOverlay.TabIndex = 3;
      this.stargatesTextOverlay.Text = "Text Overlay";
      this.stargatesTextOverlay.UseVisualStyleBackColor = true;
      this.stargatesTextOverlay.CheckedChanged += new EventHandler(this.stargatesTextOverlay_CheckedChanged);
      this.stargatesRadarRange.AutoSize = true;
      this.stargatesRadarRange.Checked = true;
      this.stargatesRadarRange.CheckState = CheckState.Checked;
      this.stargatesRadarRange.Location = new Point(6, 42);
      this.stargatesRadarRange.Name = "stargatesRadarRange";
      this.stargatesRadarRange.Size = new Size(90, 17);
      this.stargatesRadarRange.TabIndex = 2;
      this.stargatesRadarRange.Text = "Radar Range";
      this.stargatesRadarRange.UseVisualStyleBackColor = true;
      this.stargatesRadarRange.CheckedChanged += new EventHandler(this.stargatesRadarRange_CheckedChanged);
      this.stargatesSignature.AutoSize = true;
      this.stargatesSignature.Checked = true;
      this.stargatesSignature.CheckState = CheckState.Checked;
      this.stargatesSignature.Location = new Point(125, 42);
      this.stargatesSignature.Name = "stargatesSignature";
      this.stargatesSignature.Size = new Size(71, 17);
      this.stargatesSignature.TabIndex = 1;
      this.stargatesSignature.Text = "Signature";
      this.stargatesSignature.UseVisualStyleBackColor = true;
      this.stargatesSignature.CheckedChanged += new EventHandler(this.stargatesSignature_CheckedChanged);
      this.stargatesLayer.AutoSize = true;
      this.stargatesLayer.Checked = true;
      this.stargatesLayer.CheckState = CheckState.Checked;
      this.stargatesLayer.Location = new Point(6, 19);
      this.stargatesLayer.Name = "stargatesLayer";
      this.stargatesLayer.Size = new Size(52, 17);
      this.stargatesLayer.TabIndex = 0;
      this.stargatesLayer.Text = "Layer";
      this.stargatesLayer.UseVisualStyleBackColor = true;
      this.stargatesLayer.CheckedChanged += new EventHandler(this.stargatesLayer_CheckedChanged);
      this.groupBox4.Controls.Add((Control) this.starbasesExpRange);
      this.groupBox4.Controls.Add((Control) this.starbasesAIR);
      this.groupBox4.Controls.Add((Control) this.starbasesNT2);
      this.groupBox4.Controls.Add((Control) this.starbasesNT1);
      this.groupBox4.Controls.Add((Control) this.starbasesNT0);
      this.groupBox4.Controls.Add((Control) this.label4);
      this.groupBox4.Controls.Add((Control) this.starbasesTextOverlay);
      this.groupBox4.Controls.Add((Control) this.starbasesRadarRange);
      this.groupBox4.Controls.Add((Control) this.starbasesSignature);
      this.groupBox4.Controls.Add((Control) this.starbasesLayer);
      this.groupBox4.Location = new Point(3, 119);
      this.groupBox4.Name = "groupBox4";
      this.groupBox4.Size = new Size(233, 110);
      this.groupBox4.TabIndex = 3;
      this.groupBox4.TabStop = false;
      this.groupBox4.Text = "Starbases";
      this.starbasesExpRange.AutoSize = true;
      this.starbasesExpRange.Checked = true;
      this.starbasesExpRange.CheckState = CheckState.Checked;
      this.starbasesExpRange.Location = new Point(6, 65);
      this.starbasesExpRange.Name = "starbasesExpRange";
      this.starbasesExpRange.Size = new Size(113, 17);
      this.starbasesExpRange.TabIndex = 9;
      this.starbasesExpRange.Text = "Exploration Range";
      this.starbasesExpRange.UseVisualStyleBackColor = true;
      this.starbasesExpRange.CheckedChanged += new EventHandler(this.starbasesExpRange_CheckedChanged);
      this.starbasesAIR.AutoSize = true;
      this.starbasesAIR.Checked = true;
      this.starbasesAIR.CheckState = CheckState.Checked;
      this.starbasesAIR.Location = new Point(122, 65);
      this.starbasesAIR.Name = "starbasesAIR";
      this.starbasesAIR.Size = new Size(109, 17);
      this.starbasesAIR.TabIndex = 8;
      this.starbasesAIR.Text = "Appears In Radar";
      this.starbasesAIR.UseVisualStyleBackColor = true;
      this.starbasesAIR.CheckedChanged += new EventHandler(this.starbasesAIR_CheckedChanged);
      this.starbasesNT2.AutoSize = true;
      this.starbasesNT2.Checked = true;
      this.starbasesNT2.CheckState = CheckState.Checked;
      this.starbasesNT2.Location = new Point(142, 91);
      this.starbasesNT2.Name = "starbasesNT2";
      this.starbasesNT2.Size = new Size(32, 17);
      this.starbasesNT2.TabIndex = 7;
      this.starbasesNT2.Text = "2";
      this.starbasesNT2.UseVisualStyleBackColor = true;
      this.starbasesNT2.CheckedChanged += new EventHandler(this.starbasesNT2_CheckedChanged);
      this.starbasesNT1.AutoSize = true;
      this.starbasesNT1.Checked = true;
      this.starbasesNT1.CheckState = CheckState.Checked;
      this.starbasesNT1.Location = new Point(104, 91);
      this.starbasesNT1.Name = "starbasesNT1";
      this.starbasesNT1.Size = new Size(32, 17);
      this.starbasesNT1.TabIndex = 6;
      this.starbasesNT1.Text = "1";
      this.starbasesNT1.UseVisualStyleBackColor = true;
      this.starbasesNT1.CheckedChanged += new EventHandler(this.starbasesNT1_CheckedChanged);
      this.starbasesNT0.AutoSize = true;
      this.starbasesNT0.Checked = true;
      this.starbasesNT0.CheckState = CheckState.Checked;
      this.starbasesNT0.Location = new Point(66, 91);
      this.starbasesNT0.Name = "starbasesNT0";
      this.starbasesNT0.Size = new Size(32, 17);
      this.starbasesNT0.TabIndex = 5;
      this.starbasesNT0.Text = "0";
      this.starbasesNT0.UseVisualStyleBackColor = true;
      this.starbasesNT0.CheckedChanged += new EventHandler(this.starbasesNT0_CheckedChanged);
      this.label4.AutoSize = true;
      this.label4.Location = new Point(6, 91);
      this.label4.Name = "label4";
      this.label4.Size = new Size(57, 13);
      this.label4.TabIndex = 4;
      this.label4.Text = "Nav Type:";
      this.starbasesTextOverlay.AutoSize = true;
      this.starbasesTextOverlay.Checked = true;
      this.starbasesTextOverlay.CheckState = CheckState.Checked;
      this.starbasesTextOverlay.Location = new Point(122, 19);
      this.starbasesTextOverlay.Name = "starbasesTextOverlay";
      this.starbasesTextOverlay.Size = new Size(86, 17);
      this.starbasesTextOverlay.TabIndex = 3;
      this.starbasesTextOverlay.Text = "Text Overlay";
      this.starbasesTextOverlay.UseVisualStyleBackColor = true;
      this.starbasesTextOverlay.CheckedChanged += new EventHandler(this.starbasesTextOverlay_CheckedChanged);
      this.starbasesRadarRange.AutoSize = true;
      this.starbasesRadarRange.Checked = true;
      this.starbasesRadarRange.CheckState = CheckState.Checked;
      this.starbasesRadarRange.Location = new Point(6, 42);
      this.starbasesRadarRange.Name = "starbasesRadarRange";
      this.starbasesRadarRange.Size = new Size(90, 17);
      this.starbasesRadarRange.TabIndex = 2;
      this.starbasesRadarRange.Text = "Radar Range";
      this.starbasesRadarRange.UseVisualStyleBackColor = true;
      this.starbasesRadarRange.CheckedChanged += new EventHandler(this.starbasesRadarRange_CheckedChanged);
      this.starbasesSignature.AutoSize = true;
      this.starbasesSignature.Checked = true;
      this.starbasesSignature.CheckState = CheckState.Checked;
      this.starbasesSignature.Location = new Point(122, 42);
      this.starbasesSignature.Name = "starbasesSignature";
      this.starbasesSignature.Size = new Size(71, 17);
      this.starbasesSignature.TabIndex = 1;
      this.starbasesSignature.Text = "Signature";
      this.starbasesSignature.UseVisualStyleBackColor = true;
      this.starbasesSignature.CheckedChanged += new EventHandler(this.starbasesSignature_CheckedChanged);
      this.starbasesLayer.AutoSize = true;
      this.starbasesLayer.Checked = true;
      this.starbasesLayer.CheckState = CheckState.Checked;
      this.starbasesLayer.Location = new Point(6, 19);
      this.starbasesLayer.Name = "starbasesLayer";
      this.starbasesLayer.Size = new Size(52, 17);
      this.starbasesLayer.TabIndex = 0;
      this.starbasesLayer.Text = "Layer";
      this.starbasesLayer.UseVisualStyleBackColor = true;
      this.starbasesLayer.CheckedChanged += new EventHandler(this.starbasesLayer_CheckedChanged);
      this.groupBox5.Controls.Add((Control) this.decoExpRange);
      this.groupBox5.Controls.Add((Control) this.decoAIR);
      this.groupBox5.Controls.Add((Control) this.decoNT2);
      this.groupBox5.Controls.Add((Control) this.decoNT1);
      this.groupBox5.Controls.Add((Control) this.decoNT0);
      this.groupBox5.Controls.Add((Control) this.label5);
      this.groupBox5.Controls.Add((Control) this.decorationsTextOverlay);
      this.groupBox5.Controls.Add((Control) this.decorationsRadarRange);
      this.groupBox5.Controls.Add((Control) this.decorationsSignature);
      this.groupBox5.Controls.Add((Control) this.decorationsLayer);
      this.groupBox5.Location = new Point(242, 119);
      this.groupBox5.Name = "groupBox5";
      this.groupBox5.Size = new Size(234, 110);
      this.groupBox5.TabIndex = 4;
      this.groupBox5.TabStop = false;
      this.groupBox5.Text = "Decorations";
      this.decoExpRange.AutoSize = true;
      this.decoExpRange.Checked = true;
      this.decoExpRange.CheckState = CheckState.Checked;
      this.decoExpRange.Location = new Point(6, 65);
      this.decoExpRange.Name = "decoExpRange";
      this.decoExpRange.Size = new Size(113, 17);
      this.decoExpRange.TabIndex = 9;
      this.decoExpRange.Text = "Exploration Range";
      this.decoExpRange.UseVisualStyleBackColor = true;
      this.decoExpRange.CheckedChanged += new EventHandler(this.decoExpRange_CheckedChanged);
      this.decoAIR.AutoSize = true;
      this.decoAIR.Checked = true;
      this.decoAIR.CheckState = CheckState.Checked;
      this.decoAIR.Location = new Point(124, 65);
      this.decoAIR.Name = "decoAIR";
      this.decoAIR.Size = new Size(109, 17);
      this.decoAIR.TabIndex = 8;
      this.decoAIR.Text = "Appears In Radar";
      this.decoAIR.UseVisualStyleBackColor = true;
      this.decoAIR.CheckedChanged += new EventHandler(this.decoAIR_CheckedChanged);
      this.decoNT2.AutoSize = true;
      this.decoNT2.Checked = true;
      this.decoNT2.CheckState = CheckState.Checked;
      this.decoNT2.Location = new Point(142, 91);
      this.decoNT2.Name = "decoNT2";
      this.decoNT2.Size = new Size(32, 17);
      this.decoNT2.TabIndex = 7;
      this.decoNT2.Text = "2";
      this.decoNT2.UseVisualStyleBackColor = true;
      this.decoNT2.CheckedChanged += new EventHandler(this.decoNT2_CheckedChanged);
      this.decoNT1.AutoSize = true;
      this.decoNT1.Checked = true;
      this.decoNT1.CheckState = CheckState.Checked;
      this.decoNT1.Location = new Point(104, 91);
      this.decoNT1.Name = "decoNT1";
      this.decoNT1.Size = new Size(32, 17);
      this.decoNT1.TabIndex = 6;
      this.decoNT1.Text = "1";
      this.decoNT1.UseVisualStyleBackColor = true;
      this.decoNT1.CheckedChanged += new EventHandler(this.decoNT1_CheckedChanged);
      this.decoNT0.AutoSize = true;
      this.decoNT0.Checked = true;
      this.decoNT0.CheckState = CheckState.Checked;
      this.decoNT0.Location = new Point(66, 91);
      this.decoNT0.Name = "decoNT0";
      this.decoNT0.Size = new Size(32, 17);
      this.decoNT0.TabIndex = 5;
      this.decoNT0.Text = "0";
      this.decoNT0.UseVisualStyleBackColor = true;
      this.decoNT0.CheckedChanged += new EventHandler(this.decoNT0_CheckedChanged);
      this.label5.AutoSize = true;
      this.label5.Location = new Point(6, 91);
      this.label5.Name = "label5";
      this.label5.Size = new Size(57, 13);
      this.label5.TabIndex = 4;
      this.label5.Text = "Nav Type:";
      this.decorationsTextOverlay.AutoSize = true;
      this.decorationsTextOverlay.Checked = true;
      this.decorationsTextOverlay.CheckState = CheckState.Checked;
      this.decorationsTextOverlay.Location = new Point(124, 19);
      this.decorationsTextOverlay.Name = "decorationsTextOverlay";
      this.decorationsTextOverlay.Size = new Size(86, 17);
      this.decorationsTextOverlay.TabIndex = 3;
      this.decorationsTextOverlay.Text = "Text Overlay";
      this.decorationsTextOverlay.UseVisualStyleBackColor = true;
      this.decorationsTextOverlay.CheckedChanged += new EventHandler(this.decorationsTextOverlay_CheckedChanged);
      this.decorationsRadarRange.AutoSize = true;
      this.decorationsRadarRange.Checked = true;
      this.decorationsRadarRange.CheckState = CheckState.Checked;
      this.decorationsRadarRange.Location = new Point(6, 42);
      this.decorationsRadarRange.Name = "decorationsRadarRange";
      this.decorationsRadarRange.Size = new Size(90, 17);
      this.decorationsRadarRange.TabIndex = 2;
      this.decorationsRadarRange.Text = "Radar Range";
      this.decorationsRadarRange.UseVisualStyleBackColor = true;
      this.decorationsRadarRange.CheckedChanged += new EventHandler(this.decorationsRadarRange_CheckedChanged);
      this.decorationsSignature.AutoSize = true;
      this.decorationsSignature.Checked = true;
      this.decorationsSignature.CheckState = CheckState.Checked;
      this.decorationsSignature.Location = new Point(124, 42);
      this.decorationsSignature.Name = "decorationsSignature";
      this.decorationsSignature.Size = new Size(71, 17);
      this.decorationsSignature.TabIndex = 1;
      this.decorationsSignature.Text = "Signature";
      this.decorationsSignature.UseVisualStyleBackColor = true;
      this.decorationsSignature.CheckedChanged += new EventHandler(this.decorationsSignature_CheckedChanged);
      this.decorationsLayer.AutoSize = true;
      this.decorationsLayer.Checked = true;
      this.decorationsLayer.CheckState = CheckState.Checked;
      this.decorationsLayer.Location = new Point(6, 19);
      this.decorationsLayer.Name = "decorationsLayer";
      this.decorationsLayer.Size = new Size(52, 17);
      this.decorationsLayer.TabIndex = 0;
      this.decorationsLayer.Text = "Layer";
      this.decorationsLayer.UseVisualStyleBackColor = true;
      this.decorationsLayer.CheckedChanged += new EventHandler(this.decorationsLayer_CheckedChanged);
      this.groupBox6.Controls.Add((Control) this.harvExpRange);
      this.groupBox6.Controls.Add((Control) this.harvAIR);
      this.groupBox6.Controls.Add((Control) this.harvNT2);
      this.groupBox6.Controls.Add((Control) this.harvNT1);
      this.groupBox6.Controls.Add((Control) this.harvNT0);
      this.groupBox6.Controls.Add((Control) this.label6);
      this.groupBox6.Controls.Add((Control) this.harvestablesTextOverlay);
      this.groupBox6.Controls.Add((Control) this.harvestablesRadarRange);
      this.groupBox6.Controls.Add((Control) this.harvestablesSignature);
      this.groupBox6.Controls.Add((Control) this.harvestablesLayer);
      this.groupBox6.Location = new Point(482, 119);
      this.groupBox6.Name = "groupBox6";
      this.groupBox6.Size = new Size(238, 110);
      this.groupBox6.TabIndex = 5;
      this.groupBox6.TabStop = false;
      this.groupBox6.Text = "Harvestables";
      this.harvExpRange.AutoSize = true;
      this.harvExpRange.Checked = true;
      this.harvExpRange.CheckState = CheckState.Checked;
      this.harvExpRange.Location = new Point(6, 65);
      this.harvExpRange.Name = "harvExpRange";
      this.harvExpRange.Size = new Size(113, 17);
      this.harvExpRange.TabIndex = 9;
      this.harvExpRange.Text = "Exploration Range";
      this.harvExpRange.UseVisualStyleBackColor = true;
      this.harvExpRange.CheckedChanged += new EventHandler(this.harvExpRange_CheckedChanged);
      this.harvAIR.AutoSize = true;
      this.harvAIR.Checked = true;
      this.harvAIR.CheckState = CheckState.Checked;
      this.harvAIR.Location = new Point(125, 65);
      this.harvAIR.Name = "harvAIR";
      this.harvAIR.Size = new Size(109, 17);
      this.harvAIR.TabIndex = 8;
      this.harvAIR.Text = "Appears In Radar";
      this.harvAIR.UseVisualStyleBackColor = true;
      this.harvAIR.CheckedChanged += new EventHandler(this.harvAIR_CheckedChanged);
      this.harvNT2.AutoSize = true;
      this.harvNT2.Checked = true;
      this.harvNT2.CheckState = CheckState.Checked;
      this.harvNT2.Location = new Point(142, 91);
      this.harvNT2.Name = "harvNT2";
      this.harvNT2.Size = new Size(32, 17);
      this.harvNT2.TabIndex = 7;
      this.harvNT2.Text = "2";
      this.harvNT2.UseVisualStyleBackColor = true;
      this.harvNT2.CheckedChanged += new EventHandler(this.harvNT2_CheckedChanged);
      this.harvNT1.AutoSize = true;
      this.harvNT1.Checked = true;
      this.harvNT1.CheckState = CheckState.Checked;
      this.harvNT1.Location = new Point(104, 91);
      this.harvNT1.Name = "harvNT1";
      this.harvNT1.Size = new Size(32, 17);
      this.harvNT1.TabIndex = 6;
      this.harvNT1.Text = "1";
      this.harvNT1.UseVisualStyleBackColor = true;
      this.harvNT1.CheckedChanged += new EventHandler(this.harvNT1_CheckedChanged);
      this.harvNT0.AutoSize = true;
      this.harvNT0.Checked = true;
      this.harvNT0.CheckState = CheckState.Checked;
      this.harvNT0.Location = new Point(66, 91);
      this.harvNT0.Name = "harvNT0";
      this.harvNT0.Size = new Size(32, 17);
      this.harvNT0.TabIndex = 5;
      this.harvNT0.Text = "0";
      this.harvNT0.UseVisualStyleBackColor = true;
      this.harvNT0.CheckedChanged += new EventHandler(this.harvNT0_CheckedChanged);
      this.label6.AutoSize = true;
      this.label6.Location = new Point(6, 91);
      this.label6.Name = "label6";
      this.label6.Size = new Size(57, 13);
      this.label6.TabIndex = 4;
      this.label6.Text = "Nav Type:";
      this.harvestablesTextOverlay.AutoSize = true;
      this.harvestablesTextOverlay.Checked = true;
      this.harvestablesTextOverlay.CheckState = CheckState.Checked;
      this.harvestablesTextOverlay.Location = new Point(125, 19);
      this.harvestablesTextOverlay.Name = "harvestablesTextOverlay";
      this.harvestablesTextOverlay.Size = new Size(86, 17);
      this.harvestablesTextOverlay.TabIndex = 3;
      this.harvestablesTextOverlay.Text = "Text Overlay";
      this.harvestablesTextOverlay.UseVisualStyleBackColor = true;
      this.harvestablesTextOverlay.CheckedChanged += new EventHandler(this.harvestablesTextOverlay_CheckedChanged);
      this.harvestablesRadarRange.AutoSize = true;
      this.harvestablesRadarRange.Checked = true;
      this.harvestablesRadarRange.CheckState = CheckState.Checked;
      this.harvestablesRadarRange.Location = new Point(6, 42);
      this.harvestablesRadarRange.Name = "harvestablesRadarRange";
      this.harvestablesRadarRange.Size = new Size(90, 17);
      this.harvestablesRadarRange.TabIndex = 2;
      this.harvestablesRadarRange.Text = "Radar Range";
      this.harvestablesRadarRange.UseVisualStyleBackColor = true;
      this.harvestablesRadarRange.CheckedChanged += new EventHandler(this.harvestablesRadarRange_CheckedChanged);
      this.harvestablesSignature.AutoSize = true;
      this.harvestablesSignature.Checked = true;
      this.harvestablesSignature.CheckState = CheckState.Checked;
      this.harvestablesSignature.Location = new Point(125, 42);
      this.harvestablesSignature.Name = "harvestablesSignature";
      this.harvestablesSignature.Size = new Size(71, 17);
      this.harvestablesSignature.TabIndex = 1;
      this.harvestablesSignature.Text = "Signature";
      this.harvestablesSignature.UseVisualStyleBackColor = true;
      this.harvestablesSignature.CheckedChanged += new EventHandler(this.harvestablesSignature_CheckedChanged);
      this.harvestablesLayer.AutoSize = true;
      this.harvestablesLayer.Checked = true;
      this.harvestablesLayer.CheckState = CheckState.Checked;
      this.harvestablesLayer.Location = new Point(6, 19);
      this.harvestablesLayer.Name = "harvestablesLayer";
      this.harvestablesLayer.Size = new Size(52, 17);
      this.harvestablesLayer.TabIndex = 0;
      this.harvestablesLayer.Text = "Layer";
      this.harvestablesLayer.UseVisualStyleBackColor = true;
      this.harvestablesLayer.CheckedChanged += new EventHandler(this.harvestablesLayer_CheckedChanged);
      this.flowLayoutPanel1.AutoScroll = true;
      this.flowLayoutPanel1.Controls.Add((Control) this.groupBox1);
      this.flowLayoutPanel1.Controls.Add((Control) this.groupBox2);
      this.flowLayoutPanel1.Controls.Add((Control) this.groupBox3);
      this.flowLayoutPanel1.Controls.Add((Control) this.groupBox4);
      this.flowLayoutPanel1.Controls.Add((Control) this.groupBox5);
      this.flowLayoutPanel1.Controls.Add((Control) this.groupBox6);
      this.flowLayoutPanel1.Dock = DockStyle.Fill;
      this.flowLayoutPanel1.Location = new Point(0, 0);
      this.flowLayoutPanel1.Name = "flowLayoutPanel1";
      this.flowLayoutPanel1.Size = new Size(748, 291);
      this.flowLayoutPanel1.TabIndex = 6;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.AutoScroll = true;
      this.Controls.Add((Control) this.flowLayoutPanel1);
      this.Name = nameof (OptionsGui);
      this.Size = new Size(748, 291);
      this.Load += new EventHandler(this.OptionsGui_Load);
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      this.groupBox3.ResumeLayout(false);
      this.groupBox3.PerformLayout();
      this.groupBox4.ResumeLayout(false);
      this.groupBox4.PerformLayout();
      this.groupBox5.ResumeLayout(false);
      this.groupBox5.PerformLayout();
      this.groupBox6.ResumeLayout(false);
      this.groupBox6.PerformLayout();
      this.flowLayoutPanel1.ResumeLayout(false);
      this.ResumeLayout(false);
    }
  }
}
