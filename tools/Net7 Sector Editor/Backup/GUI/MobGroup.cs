﻿// Decompiled with JetBrains decompiler
// Type: N7.GUI.MobGroup
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace N7.GUI
{
  public class MobGroup : Form
  {
    private IContainer components;
    private DataGridView dataGridView1;
    private Label label1;
    private DataGridView dataGridView2;
    private Label label2;
    private Button button1;
    private Button button2;
    private Button button3;
    private Button button4;
    private DataGridViewTextBoxColumn id;
    private DataGridViewTextBoxColumn level;
    private DataGridViewTextBoxColumn name;
    public IWindowsFormsEditorService _wfes;
    private DataTable mobsTable;
    private DataTable groupMobsTable;
    private int _id;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (MobGroup));
      this.dataGridView1 = new DataGridView();
      this.label1 = new Label();
      this.dataGridView2 = new DataGridView();
      this.label2 = new Label();
      this.button1 = new Button();
      this.button2 = new Button();
      this.button3 = new Button();
      this.button4 = new Button();
      this.id = new DataGridViewTextBoxColumn();
      this.level = new DataGridViewTextBoxColumn();
      this.name = new DataGridViewTextBoxColumn();
      ((ISupportInitialize) this.dataGridView1).BeginInit();
      ((ISupportInitialize) this.dataGridView2).BeginInit();
      this.SuspendLayout();
      this.dataGridView1.AllowUserToAddRows = false;
      this.dataGridView1.AllowUserToDeleteRows = false;
      this.dataGridView1.BackgroundColor = SystemColors.Window;
      this.dataGridView1.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dataGridView1.Location = new Point(12, 25);
      this.dataGridView1.Name = "dataGridView1";
      this.dataGridView1.ReadOnly = true;
      this.dataGridView1.Size = new Size(545, 150);
      this.dataGridView1.TabIndex = 0;
      this.label1.AutoSize = true;
      this.label1.Location = new Point(12, 9);
      this.label1.Name = "label1";
      this.label1.Size = new Size(96, 13);
      this.label1.TabIndex = 1;
      this.label1.Text = "Available Mob List:";
      this.dataGridView2.BackgroundColor = SystemColors.Window;
      this.dataGridView2.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.dataGridView2.Columns.AddRange((DataGridViewColumn) this.id, (DataGridViewColumn) this.level, (DataGridViewColumn) this.name);
      this.dataGridView2.Location = new Point(12, 210);
      this.dataGridView2.Name = "dataGridView2";
      this.dataGridView2.ReadOnly = true;
      this.dataGridView2.Size = new Size(463, 150);
      this.dataGridView2.TabIndex = 2;
      this.dataGridView2.CellContentClick += new DataGridViewCellEventHandler(this.dataGridView2_CellContentClick);
      this.label2.AutoSize = true;
      this.label2.Location = new Point(12, 194);
      this.label2.Name = "label2";
      this.label2.Size = new Size(134, 13);
      this.label2.TabIndex = 3;
      this.label2.Text = "Mobs in this Spawn Group:";
      this.button1.Location = new Point(481, 239);
      this.button1.Name = "button1";
      this.button1.Size = new Size(76, 23);
      this.button1.TabIndex = 5;
      this.button1.Text = "Remove ->>";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new EventHandler(this.button1_Click);
      this.button2.Location = new Point(482, 210);
      this.button2.Name = "button2";
      this.button2.Size = new Size(75, 23);
      this.button2.TabIndex = 6;
      this.button2.Text = "<<- Add";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new EventHandler(this.button2_Click);
      this.button3.Location = new Point(482, 366);
      this.button3.Name = "button3";
      this.button3.Size = new Size(75, 23);
      this.button3.TabIndex = 7;
      this.button3.Text = "Finish";
      this.button3.UseVisualStyleBackColor = true;
      this.button3.Click += new EventHandler(this.button3_Click);
      this.button4.Location = new Point(12, 366);
      this.button4.Name = "button4";
      this.button4.Size = new Size(75, 23);
      this.button4.TabIndex = 8;
      this.button4.Text = "Cancel";
      this.button4.UseVisualStyleBackColor = true;
      this.button4.Click += new EventHandler(this.button4_Click);
      this.id.FillWeight = 80f;
      this.id.HeaderText = "ID";
      this.id.Name = "id";
      this.id.ReadOnly = true;
      this.id.Width = 80;
      this.level.FillWeight = 40f;
      this.level.HeaderText = "Level";
      this.level.Name = "level";
      this.level.ReadOnly = true;
      this.level.Width = 40;
      this.name.HeaderText = "Name";
      this.name.Name = "name";
      this.name.ReadOnly = true;
      this.name.Width = 298;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(569, 394);
      this.Controls.Add((Control) this.button4);
      this.Controls.Add((Control) this.button3);
      this.Controls.Add((Control) this.button2);
      this.Controls.Add((Control) this.button1);
      this.Controls.Add((Control) this.label2);
      this.Controls.Add((Control) this.dataGridView2);
      this.Controls.Add((Control) this.label1);
      this.Controls.Add((Control) this.dataGridView1);
      this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = nameof (MobGroup);
      this.SizeGripStyle = SizeGripStyle.Hide;
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "Spawn Group";
      this.TopMost = true;
      this.Load += new EventHandler(this.MobGroup_Load);
      ((ISupportInitialize) this.dataGridView1).EndInit();
      ((ISupportInitialize) this.dataGridView2).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public MobGroup()
    {
      DataTable dataTable1 = Database.executeQuery(Database.DatabaseName.net7, "SELECT Auto_increment FROM information_schema.tables WHERE table_name='sector_objects' and table_schema='net7';");
      int num1 = 0;
      foreach (DataRow row in (InternalDataCollectionBase) dataTable1.Rows)
        num1 = int.Parse(row["Auto_increment"].ToString());
      DataTable dataTable2 = Database.executeQuery(Database.DatabaseName.net7, "SELECT sector_object_id FROM sector_objects where sector_object_id='" + (object) mainFrm.selectedObjectID + "';");
      int num2 = 0;
      foreach (DataRow row in (InternalDataCollectionBase) dataTable2.Rows)
      {
        num2 = int.Parse(row["sector_object_id"].ToString());
        Console.Out.WriteLine(num2);
      }
      this._id = num2 == 0 ? num1 : mainFrm.selectedObjectID;
      this.InitializeComponent();
    }

    private void MobGroup_Load(object sender, EventArgs e)
    {
      this.mobsTable = mainFrm.mobs.getMobTable();
      this.dataGridView1.DataSource = (object) this.mobsTable;
      this.dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
      this.groupMobsTable = Database.executeQuery(Database.DatabaseName.net7, "SELECT * FROM mob_spawn_group where spawn_group_id='" + (object) this._id + "';");
      DataTable dataTable = new DataTable();
      foreach (DataRow row1 in (InternalDataCollectionBase) this.groupMobsTable.Rows)
      {
        string str = row1["mob_id"].ToString();
        foreach (DataGridViewRow row2 in (IEnumerable) this.dataGridView1.Rows)
        {
          if (this.dataGridView1.Rows[row2.Index].Cells["mob_id"].Value.ToString() == str)
            this.dataGridView2.Rows.Add((object[]) new string[3]
            {
              this.dataGridView1.Rows[row2.Index].Cells["mob_id"].Value.ToString(),
              this.dataGridView1.Rows[row2.Index].Cells["level"].Value.ToString(),
              this.dataGridView1.Rows[row2.Index].Cells["name"].Value.ToString()
            });
        }
      }
      this.dataGridView2.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
    }

    private void button2_Click(object sender, EventArgs e)
    {
      foreach (DataGridViewRow selectedRow in (BaseCollection) this.dataGridView1.SelectedRows)
      {
        int num = 0;
        string[] strArray = new string[3]
        {
          this.dataGridView1.Rows[selectedRow.Index].Cells["mob_id"].Value.ToString(),
          this.dataGridView1.Rows[selectedRow.Index].Cells["level"].Value.ToString(),
          this.dataGridView1.Rows[selectedRow.Index].Cells["name"].Value.ToString()
        };
        string str1 = this.dataGridView1.Rows[selectedRow.Index].Cells["mob_id"].Value.ToString();
        for (int index = 0; index < this.dataGridView2.Rows.Count - 1; ++index)
        {
          string str2 = this.dataGridView2.Rows[index].Cells["id"].Value.ToString();
          if (str1 == str2)
            ++num;
        }
        try
        {
          Database.executeQuery(Database.DatabaseName.net7, "INSERT INTO mob_spawn_group SET spawn_group_id='" + (object) this._id + "', mob_id='" + strArray[0] + "', group_index='" + (object) num + "';");
          this.dataGridView2.Rows.Add((object[]) strArray);
          this.dataGridView2.Update();
        }
        catch (Exception ex)
        {
          throw;
        }
      }
    }

    private void button4_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void button3_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void button1_Click(object sender, EventArgs e)
    {
      int num = 0;
      foreach (DataGridViewRow selectedRow in (BaseCollection) this.dataGridView2.SelectedRows)
      {
        string str1 = this.dataGridView2.Rows[selectedRow.Index].Cells["id"].Value.ToString();
        for (int index = selectedRow.Index + 1; index < this.dataGridView2.Rows.Count - 1; ++index)
        {
          string str2 = this.dataGridView2.Rows[index].Cells["id"].Value.ToString();
          if (str1 == str2)
            ++num;
        }
        for (int index = 0; index < selectedRow.Index; ++index)
        {
          string str2 = this.dataGridView2.Rows[index].Cells["id"].Value.ToString();
          if (str1 == str2)
            ++num;
        }
        try
        {
          string query = "DELETE FROM mob_spawn_group where spawn_group_id='" + (object) this._id + "' and mob_id='" + str1 + "' and group_index='" + (object) num + "';";
          Console.Out.WriteLine(query);
          Database.executeQuery(Database.DatabaseName.net7, query);
          this.dataGridView2.Rows.Remove(selectedRow);
        }
        catch (Exception ex)
        {
          throw;
        }
      }
    }

    private void dataGridView2_CellContentClick(object sender, DataGridViewCellEventArgs e)
    {
    }
  }
}
