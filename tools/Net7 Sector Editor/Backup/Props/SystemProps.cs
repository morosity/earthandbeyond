﻿// Decompiled with JetBrains decompiler
// Type: N7.Props.SystemProps
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using System.ComponentModel;
using System.Drawing;

namespace N7.Props
{
  [DefaultProperty("Name")]
  internal class SystemProps
  {
    private string name;
    private float galaxy_x;
    private float galaxy_y;
    private float galaxy_z;
    private Color color;
    private string notes;

    [Category("Base Props")]
    [Description("Name of the system")]
    public string Name
    {
      get
      {
        return this.name;
      }
      set
      {
        this.name = value;
      }
    }

    [Description("X Coordinate of this system in the galaxy.")]
    [Category("Misc. Properties")]
    public float GalaxyX
    {
      get
      {
        return this.galaxy_x;
      }
      set
      {
        this.galaxy_x = value;
      }
    }

    [Category("Misc. Properties")]
    [Description("Y Coordinate of this system in the galaxy.")]
    public float GalaxyY
    {
      get
      {
        return this.galaxy_y;
      }
      set
      {
        this.galaxy_y = value;
      }
    }

    [Category("Misc. Properties")]
    [Description("Z Coordinate of this system in the galaxy.")]
    public float GalaxyZ
    {
      get
      {
        return this.galaxy_z;
      }
      set
      {
        this.galaxy_z = value;
      }
    }

    [Description("The Color of the System (For future tool purposes only).")]
    [Category("Misc. Properties")]
    public Color Color
    {
      get
      {
        return this.color;
      }
      set
      {
        this.color = value;
      }
    }

    [Category("Misc. Properties")]
    [Description("Any Developer Notes you may have.")]
    public string Notes
    {
      get
      {
        return this.notes;
      }
      set
      {
        this.notes = value;
      }
    }
  }
}
