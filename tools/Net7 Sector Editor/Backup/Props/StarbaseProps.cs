﻿// Decompiled with JetBrains decompiler
// Type: N7.Props.StarbaseProps
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using System.ComponentModel;

namespace N7.Props
{
  internal class StarbaseProps : BaseProps
  {
    private bool is_cap_ship;
    private bool is_dockable;

    public void fillBaseProps(BaseProps bp)
    {
      this.SectorID = bp.SectorID;
      this.NavType = bp.NavType;
      this.Signature = bp.Signature;
      this.IsHuge = bp.IsHuge;
      this.BaseXP = bp.BaseXP;
      this.ExplorationRange = bp.ExplorationRange;
      this.BaseAssetID = bp.BaseAssetID;
      this.Color = bp.Color;
      this.Type = bp.Type;
      this.Scale = bp.Scale;
      this.PositionX = bp.PositionX;
      this.PositionY = bp.PositionY;
      this.PositionZ = bp.PositionZ;
      this.Name = bp.Name;
      this.AppearsInRadar = bp.AppearsInRadar;
      this.RadarRange = bp.RadarRange;
      this.Destination = bp.Destination;
    }

    [Description("Is this starbase object a Capital Ship ?")]
    [Category("Starbase Object Props")]
    public bool IsCapShip
    {
      get
      {
        return this.is_cap_ship;
      }
      set
      {
        this.is_cap_ship = value;
      }
    }

    [Category("Starbase Object Props")]
    [Description("Is this starbase dockable by players?")]
    public bool IsDockable
    {
      get
      {
        return this.is_dockable;
      }
      set
      {
        this.is_dockable = value;
      }
    }
  }
}
