﻿// Decompiled with JetBrains decompiler
// Type: N7.Props.BaseProps
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using N7.Utilities;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Design;

namespace N7.Props
{
  [DefaultProperty("Name")]
  internal class BaseProps
  {
    private int sector_id;
    private string nav_type;
    private float signature;
    private bool is_huge;
    private int base_xp;
    private float exploration_range;
    private int base_asset_id;
    private Color color;
    private string _Type;
    private float scale;
    private float position_x;
    private float position_y;
    private float position_z;
    private double pitch;
    private double yaw;
    private double roll;
    private string name;
    private bool appears_in_rader;
    private float radar_range;
    private int gate_to;
    private int sound_effect;
    private float sound_effect_range;

    [Description("The Sector id in which this object belongs too.")]
    [Category("Nav Point Props")]
    [ReadOnly(true)]
    public int SectorID
    {
      get
      {
        return this.sector_id;
      }
      set
      {
        this.sector_id = value;
      }
    }

    [Description("The Nav Type of the Object (0,1,2)")]
    [Browsable(true)]
    [TypeConverter(typeof (NavTypeConverter))]
    [Category("Nav Point Props")]
    public string NavType
    {
      get
      {
        string str = "";
        if (this.nav_type != null)
          str = this.nav_type;
        else if (HE_GlobalVars._ListofNavTypes.Length > 0)
        {
          Array.Sort<string>(HE_GlobalVars._ListofNavTypes);
          str = HE_GlobalVars._ListofNavTypes[0];
        }
        return str;
      }
      set
      {
        this.nav_type = value;
      }
    }

    [Description("Base Signature of the Object")]
    [Category("Nav Point Props")]
    public float Signature
    {
      get
      {
        return this.signature;
      }
      set
      {
        this.signature = value;
      }
    }

    [Description("Is the Object Huge")]
    [Category("Nav Point Props")]
    public bool IsHuge
    {
      get
      {
        return this.is_huge;
      }
      set
      {
        this.is_huge = value;
      }
    }

    [Description("The Base Xp you get from exploring this Object")]
    [Category("Nav Point Props")]
    public int BaseXP
    {
      get
      {
        return this.base_xp;
      }
      set
      {
        this.base_xp = value;
      }
    }

    [Description("The range at which the players get xp when intially \n exploring an object.")]
    [Category("Nav Point Props")]
    public float ExplorationRange
    {
      get
      {
        return this.exploration_range;
      }
      set
      {
        this.exploration_range = value;
      }
    }

    [Category("Base Props")]
    [Editor(typeof (BaseAssetsEditor), typeof (UITypeEditor))]
    [Description("The ID of the graphical Asset for this object")]
    public int BaseAssetID
    {
      get
      {
        return this.base_asset_id;
      }
      set
      {
        this.base_asset_id = value;
      }
    }

    [Category("Base Props")]
    [Description("The Color shade of the object.")]
    public Color Color
    {
      get
      {
        return this.color;
      }
      set
      {
        this.color = value;
      }
    }

    [Category("Base Props")]
    [Description("The type of this Object (0,3,11,12,37,38)")]
    [Browsable(true)]
    [TypeConverter(typeof (TypesConverter))]
    [ReadOnly(true)]
    public string Type
    {
      get
      {
        string str = "";
        if (this._Type != null)
          str = this._Type;
        else if (HE_GlobalVars._ListofTypes.Length > 0)
        {
          Array.Sort<string>(HE_GlobalVars._ListofTypes);
          str = HE_GlobalVars._ListofTypes[0];
        }
        return str;
      }
      set
      {
        this._Type = value;
      }
    }

    [Description("The scale of the object")]
    [Category("Base Props")]
    public float Scale
    {
      get
      {
        return this.scale;
      }
      set
      {
        this.scale = value;
      }
    }

    [Description("The X Coordinate of the object")]
    [Category("Base Props")]
    public float PositionX
    {
      get
      {
        return this.position_x;
      }
      set
      {
        this.position_x = value;
      }
    }

    [Category("Base Props")]
    [Description("The Y Coordinate of the object")]
    public float PositionY
    {
      get
      {
        return this.position_y;
      }
      set
      {
        this.position_y = value;
      }
    }

    [Category("Base Props")]
    [Description("The Z Coordinate of the object")]
    public float PositionZ
    {
      get
      {
        return this.position_z;
      }
      set
      {
        this.position_z = value;
      }
    }

    [Category("Base Props")]
    [Description("The pitch(alttitude) in eular angles (0-360) of this object.")]
    public double Orientation_Pitch
    {
      get
      {
        return this.pitch;
      }
      set
      {
        this.pitch = value;
      }
    }

    [Category("Base Props")]
    [Description("The yaw(heading) in eular angles (0-360) of this object.")]
    public double Orientation_Yaw
    {
      get
      {
        return this.yaw;
      }
      set
      {
        this.yaw = value;
      }
    }

    [Category("Base Props")]
    [Description("The roll(bank) in eular angles (0-360) of this object.")]
    public double Orientation_Roll
    {
      get
      {
        return this.roll;
      }
      set
      {
        this.roll = value;
      }
    }

    [Category("Base Props")]
    [Description("The name of the Object")]
    public string Name
    {
      get
      {
        return this.name;
      }
      set
      {
        this.name = value;
      }
    }

    [Category("Base Props")]
    [Description("Will the object appear on the radar ?")]
    public bool AppearsInRadar
    {
      get
      {
        return this.appears_in_rader;
      }
      set
      {
        this.appears_in_rader = value;
      }
    }

    [Description("The radar range of the object")]
    [Category("Base Props")]
    public float RadarRange
    {
      get
      {
        return this.radar_range;
      }
      set
      {
        this.radar_range = value;
      }
    }

    [Description("The Destination of the object, Currently only used on planets, stargates and stations.")]
    [Category("Base Props")]
    [Editor(typeof (DestinationEditor), typeof (UITypeEditor))]
    public int Destination
    {
      get
      {
        return this.gate_to;
      }
      set
      {
        this.gate_to = value;
      }
    }

    [Description("The sound effect that is attached to this sector object.")]
    [Category("Base Props")]
    [Editor(typeof (SoundEffectEditor), typeof (UITypeEditor))]
    public int SoundEffect
    {
      get
      {
        return this.sound_effect;
      }
      set
      {
        this.sound_effect = value;
      }
    }

    [Category("Base Props")]
    [Description("The range (in km) at which players can here this objects sound.")]
    public float SoundEffectRange
    {
      get
      {
        return this.sound_effect_range;
      }
      set
      {
        this.sound_effect_range = value;
      }
    }
  }
}
