﻿// Decompiled with JetBrains decompiler
// Type: N7.Props.PlanetProps
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using System.ComponentModel;

namespace N7.Props
{
  internal class PlanetProps : BaseProps
  {
    private int orbit_id;
    private float orbit_dist;
    private float orbit_angle;
    private float orbit_rate;
    private float rotate_rate;
    private float rotate_angle;
    private float tilt_angle;
    private bool is_landable;

    public void fillBaseProps(BaseProps bp)
    {
      this.SectorID = bp.SectorID;
      this.NavType = bp.NavType;
      this.Signature = bp.Signature;
      this.IsHuge = bp.IsHuge;
      this.BaseXP = bp.BaseXP;
      this.ExplorationRange = bp.ExplorationRange;
      this.BaseAssetID = bp.BaseAssetID;
      this.Color = bp.Color;
      this.Type = bp.Type;
      this.Scale = bp.Scale;
      this.PositionX = bp.PositionX;
      this.PositionY = bp.PositionY;
      this.PositionZ = bp.PositionZ;
      this.Name = bp.Name;
      this.AppearsInRadar = bp.AppearsInRadar;
      this.RadarRange = bp.RadarRange;
      this.Destination = bp.Destination;
    }

    [Category("Planet Object Props")]
    [Description("The orbit ID of this object")]
    public int OrbitID
    {
      get
      {
        return this.orbit_id;
      }
      set
      {
        this.orbit_id = value;
      }
    }

    [Category("Planet Object Props")]
    [Description("The orbit distance")]
    public float OrbitDist
    {
      get
      {
        return this.orbit_dist;
      }
      set
      {
        this.orbit_dist = value;
      }
    }

    [Category("Planet Object Props")]
    [Description("The orbit angle")]
    public float OrbitAngle
    {
      get
      {
        return this.orbit_angle;
      }
      set
      {
        this.orbit_angle = value;
      }
    }

    [Description("The orbit rate")]
    [Category("Planet Object Props")]
    public float OrbitRate
    {
      get
      {
        return this.orbit_rate;
      }
      set
      {
        this.orbit_rate = value;
      }
    }

    [Description("The rotational rate")]
    [Category("Planet Object Props")]
    public float RotateRate
    {
      get
      {
        return this.rotate_rate;
      }
      set
      {
        this.rotate_rate = value;
      }
    }

    [Description("The rotation angle")]
    [Category("Planet Object Props")]
    public float RotateAngle
    {
      get
      {
        return this.rotate_angle;
      }
      set
      {
        this.rotate_angle = value;
      }
    }

    [Description("The angle of tilt")]
    [Category("Planet Object Props")]
    public float TiltAngle
    {
      get
      {
        return this.tilt_angle;
      }
      set
      {
        this.tilt_angle = value;
      }
    }

    [Category("Planet Object Props")]
    [Description("Is this planet landable ?")]
    public bool IsLandable
    {
      get
      {
        return this.is_landable;
      }
      set
      {
        this.is_landable = value;
      }
    }
  }
}
