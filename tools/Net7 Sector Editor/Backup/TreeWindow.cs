﻿// Decompiled with JetBrains decompiler
// Type: N7.TreeWindow
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using System.Data;
using System.Windows.Forms;

namespace N7
{
  internal class TreeWindow
  {
    private TreeNode[] parent;

    public TreeWindow(DataTable systems, DataTable sectors)
    {
      this.parent = new TreeNode[systems.Rows.Count];
      int index = 0;
      foreach (DataRow row1 in (InternalDataCollectionBase) systems.Rows)
      {
        string text1 = row1["name"].ToString();
        string str1 = row1["system_id"].ToString();
        TreeNode treeNode = new TreeNode(text1);
        foreach (DataRow row2 in (InternalDataCollectionBase) sectors.Rows)
        {
          string str2 = row2["system_id"].ToString();
          if (str1 == str2)
          {
            string text2 = row2["name"].ToString();
            treeNode.Nodes.Add(text2);
          }
        }
        this.parent[index] = treeNode;
        ++index;
      }
    }

    public TreeNode[] setupInitialTree()
    {
      return this.parent;
    }
  }
}
