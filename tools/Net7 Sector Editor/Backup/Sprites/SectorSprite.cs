﻿// Decompiled with JetBrains decompiler
// Type: N7.Sprites.SectorSprite
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using N7.Props;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using UMD.HCIL.Piccolo;
using UMD.HCIL.Piccolo.Event;
using UMD.HCIL.Piccolo.Nodes;

namespace N7.Sprites
{
  internal class SectorSprite
  {
    private PropertyGrid _pg;
    private SectorProps sp;
    private PText pname;
    private DataRow dr;

    public SectorSprite(PLayer layer, PropertyGrid pg, DataRow r, string name, float galaxy_x, float galaxy_y, float xmin, float xmax, float ymin, float ymax)
    {
      this._pg = pg;
      this.dr = r;
      this.setupData(r);
      float num1 = (float) (((double) xmax - (double) xmin) / 1000.0);
      double num2 = ((double) ymax - (double) ymin) / 1000.0;
      Pen pen = new Pen(Color.Honeydew, 2f);
      pen.DashStyle = DashStyle.Dash;
      PPath ellipse = PPath.CreateEllipse(galaxy_x, galaxy_y, num1, num1);
      ellipse.Pen = pen;
      ellipse.Brush = Brushes.Transparent;
      this.pname = new PText(name);
      this.pname.TextBrush = Brushes.White;
      this.pname.TextAlignment = StringAlignment.Center;
      this.pname.X = ellipse.X + (float) ((double) ellipse.Width / 2.0 - (double) this.pname.Width / 2.0);
      this.pname.Y = ellipse.Y + ellipse.Height / 2f;
      ellipse.Tag = (object) this;
      ellipse.AddChild((PNode) this.pname);
      layer.AddChild((PNode) ellipse);
      ellipse.ChildrenPickable = false;
      ellipse.MouseDown += new PInputEventHandler(this.Image_MouseDown);
    }

    private void setupData(DataRow r)
    {
      string str1 = r["name"].ToString();
      float num1 = float.Parse(r["x_min"].ToString());
      float num2 = float.Parse(r["x_max"].ToString());
      float num3 = float.Parse(r["y_min"].ToString());
      float num4 = float.Parse(r["y_max"].ToString());
      float num5 = float.Parse(r["z_min"].ToString());
      float num6 = float.Parse(r["z_max"].ToString());
      int num7 = int.Parse(r["grid_x"].ToString());
      int num8 = int.Parse(r["grid_y"].ToString());
      int num9 = int.Parse(r["grid_z"].ToString());
      float num10 = float.Parse(r["fog_near"].ToString());
      float num11 = float.Parse(r["fog_far"].ToString());
      int num12 = int.Parse(r["debris_mode"].ToString());
      bool flag1 = (bool) r["light_backdrop"];
      bool flag2 = (bool) r["fog_backdrop"];
      bool flag3 = (bool) r["swap_backdrop"];
      float num13 = float.Parse(r["backdrop_fog_near"].ToString());
      float num14 = float.Parse(r["backdrop_fog_far"].ToString());
      float num15 = float.Parse(r["max_tilt"].ToString());
      bool flag4 = (bool) r["auto_level"];
      float num16 = float.Parse(r["impulse_rate"].ToString());
      float num17 = float.Parse(r["decay_velocity"].ToString());
      float num18 = float.Parse(r["decay_spin"].ToString());
      int num19 = int.Parse(r["backdrop_asset"].ToString());
      string str2 = r["greetings"].ToString();
      string str3 = r["notes"].ToString();
      int num20 = int.Parse(r["system_id"].ToString());
      float num21 = float.Parse(r["galaxy_x"].ToString());
      float num22 = float.Parse(r["galaxy_y"].ToString());
      float num23 = float.Parse(r["galaxy_z"].ToString());
      int num24 = int.Parse(r["sector_type"].ToString());
      float num25 = num2 - num1;
      float num26 = num4 - num3;
      float num27 = num6 - num5;
      this.sp = new SectorProps();
      this.sp.Name = str1;
      this.sp.Width = num25;
      this.sp.Height = num26;
      this.sp.Depth = num27;
      this.sp.GridX = num7;
      this.sp.GridY = num8;
      this.sp.GridZ = num9;
      this.sp.FogNear = num10;
      this.sp.FogFar = num11;
      this.sp.DebrisMode = num12;
      this.sp.LightBackdrop = flag1;
      this.sp.FogBackdrop = flag2;
      this.sp.SwapBackdrop = flag3;
      this.sp.BackdropFogNear = num13;
      this.sp.BackdropFogFar = num14;
      this.sp.MaxTilt = num15;
      this.sp.AutoLevel = flag4;
      this.sp.ImpulseRate = num16;
      this.sp.DecayVelocity = num17;
      this.sp.DecaySpin = num18;
      this.sp.BackdropAsset = num19;
      this.sp.Greetings = str2;
      this.sp.Notes = str3;
      this.sp.SystemID = num20;
      this.sp.GalaxyX = num21;
      this.sp.GalaxyY = num22;
      this.sp.GalaxyZ = num23;
      string str4 = "";
      switch (num24)
      {
        case 0:
          str4 = "Space Sector";
          break;
        case 1:
          str4 = "Rocky Planet Surface";
          break;
        case 2:
          str4 = "Gas Giant Surface";
          break;
      }
      this.sp.SectorType = str4;
    }

    protected void Image_MouseDown(object sender, PInputEventArgs e)
    {
      this._pg.SelectedObject = (object) this.sp;
    }

    public PText getText()
    {
      return this.pname;
    }

    public void updateChangedInfo(string propertyName, string _changedValue)
    {
      string s = _changedValue.Replace("'", "''");
      if (propertyName == "Name")
        this.dr["name"] = (object) s;
      else if (propertyName == "Width" || propertyName == "Height" || propertyName == "Depth")
      {
        float num1 = 0.0f;
        float num2 = 0.0f;
        float num3 = 0.0f;
        float num4 = 0.0f;
        float num5 = 0.0f;
        float num6 = 0.0f;
        if ((double) this.sp.Width != 0.0)
        {
          num1 = (float) -((double) this.sp.Width / 2.0);
          num2 = this.sp.Width / 2f;
        }
        if ((double) this.sp.Height != 0.0)
        {
          num3 = (float) -((double) this.sp.Height / 2.0);
          num4 = this.sp.Height / 2f;
        }
        if ((double) this.sp.Depth != 0.0)
        {
          num5 = (float) -((double) this.sp.Depth / 2.0);
          num6 = this.sp.Depth / 2f;
        }
        this.dr["x_min"] = (object) num1;
        this.dr["x_max"] = (object) num2;
        this.dr["y_min"] = (object) num3;
        this.dr["y_max"] = (object) num4;
        this.dr["z_min"] = (object) num5;
        this.dr["z_max"] = (object) num6;
      }
      else if (propertyName == "GridX")
        this.dr["grix_x"] = (object) int.Parse(s);
      else if (propertyName == "GridY")
        this.dr["grix_y"] = (object) int.Parse(s);
      else if (propertyName == "GridZ")
        this.dr["grix_z"] = (object) int.Parse(s);
      else if (propertyName == "FogNear")
        this.dr["fog_near"] = (object) float.Parse(s);
      else if (propertyName == "FogFar")
        this.dr["fog_far"] = (object) float.Parse(s);
      else if (propertyName == "DebrisMode")
        this.dr["debris_mode"] = (object) int.Parse(s);
      else if (propertyName == "LightBackdrop")
        this.dr["light_backdrop"] = (object) bool.Parse(s);
      else if (propertyName == "FogBackdrop")
        this.dr["fog_backdrop"] = (object) bool.Parse(s);
      else if (propertyName == "SwapBackdrop")
        this.dr["swap_backdrop"] = (object) bool.Parse(s);
      else if (propertyName == "BackdropFogNear")
        this.dr["backdrop_fog_near"] = (object) float.Parse(s);
      else if (propertyName == "BackdropFogFar")
        this.dr["backdrop_fog_far"] = (object) float.Parse(s);
      else if (propertyName == "MaxTilt")
        this.dr["mex_tilt"] = (object) float.Parse(s);
      else if (propertyName == "AutoLevel")
        this.dr["auto_level"] = (object) bool.Parse(s);
      else if (propertyName == "ImpulseRate")
        this.dr["impulse_rate"] = (object) float.Parse(s);
      else if (propertyName == "DecayVelocity")
        this.dr["decay_velocity"] = (object) float.Parse(s);
      else if (propertyName == "DecaySpin")
        this.dr["decay_spin"] = (object) float.Parse(s);
      else if (propertyName == "BackdropAsset")
        this.dr["backdrop_asset"] = (object) int.Parse(s);
      else if (propertyName == "Greetings")
        this.dr["greetings"] = (object) s;
      else if (propertyName == "Notes")
        this.dr["notes"] = (object) s;
      else if (propertyName == "SystemID")
        this.dr["system_id"] = (object) int.Parse(s);
      else if (propertyName == "GalaxyX")
        this.dr["galaxy_x"] = (object) float.Parse(s);
      else if (propertyName == "GalaxyY")
        this.dr["galaxy_y"] = (object) float.Parse(s);
      else if (propertyName == "GalaxyZ")
        this.dr["galaxy_z"] = (object) float.Parse(s);
      else if (propertyName == "SectorType")
      {
        if (s == "Space Sector")
          this.dr["sector_type"] = (object) 0;
        else if (s == "Rocky Planet Surface")
          this.dr["sector_type"] = (object) 1;
        else if (s == "Gas Giant Surface")
          this.dr["sector_type"] = (object) 2;
      }
      if (this.dr.RowState == DataRowState.Modified)
        return;
      this.dr.SetModified();
    }
  }
}
