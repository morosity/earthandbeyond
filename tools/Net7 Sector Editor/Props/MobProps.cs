﻿// Decompiled with JetBrains decompiler
// Type: N7.Props.MobProps
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using N7.Utilities;
using System.ComponentModel;
using System.Drawing.Design;

namespace N7.Props
{
  internal class MobProps : BaseProps
  {
    private int count;
    private string spawn_group_id;
    private float spawn_radius;
    private float respawn_time;
    private bool delayed_spawn;

    public void fillBaseProps(BaseProps bp)
    {
      this.SectorID = bp.SectorID;
      this.NavType = bp.NavType;
      this.Signature = bp.Signature;
      this.IsHuge = bp.IsHuge;
      this.BaseXP = bp.BaseXP;
      this.ExplorationRange = bp.ExplorationRange;
      this.BaseAssetID = bp.BaseAssetID;
      this.Color = bp.Color;
      this.Type = bp.Type;
      this.Scale = bp.Scale;
      this.PositionX = bp.PositionX;
      this.PositionY = bp.PositionY;
      this.PositionZ = bp.PositionZ;
      this.Name = bp.Name;
      this.AppearsInRadar = bp.AppearsInRadar;
      this.RadarRange = bp.RadarRange;
      this.Destination = bp.Destination;
    }

    [Description("The number of maximum mobs spawned at a given time.")]
    [Category("Mob Object Props")]
    public int Count
    {
      get
      {
        return this.count;
      }
      set
      {
        this.count = value;
      }
    }

    [Editor(typeof (MobGroupEditor), typeof (UITypeEditor))]
    [Category("Mob Object Props")]
    [Description("The mobs that are contained within this spawn.")]
    public string SpawnGroup
    {
      get
      {
        return this.spawn_group_id;
      }
      set
      {
        this.spawn_group_id = value;
      }
    }

    [Description("The radius the mobs spead out to.")]
    [Category("Mob Object Props")]
    public float SpawnRadius
    {
      get
      {
        return this.spawn_radius;
      }
      set
      {
        this.spawn_radius = value;
      }
    }

    [Description("The time from when all mobs in the spawn were killed to the time when they start spawning again, value should be in seconds.")]
    [Category("Mob Object Props")]
    public float RespawnTime
    {
      get
      {
        return this.respawn_time;
      }
      set
      {
        this.respawn_time = value;
      }
    }

    [Category("Mob Object Props")]
    [Description("Is the mob spawn available at server startup or is there a delay ? Intial delay would be equal to the spawn timer.")]
    public bool DelayedSpawn
    {
      get
      {
        return this.delayed_spawn;
      }
      set
      {
        this.delayed_spawn = value;
      }
    }
  }
}
