﻿// Decompiled with JetBrains decompiler
// Type: N7.Props.SectorProps
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using N7.Utilities;
using System.ComponentModel;
using System.Drawing.Design;

namespace N7.Props
{
  [DefaultProperty("Name")]
  internal class SectorProps
  {
    private string name;
    private int sector_id;
    private float width;
    private float height;
    private float depth;
    private int grid_x;
    private int grid_y;
    private int grid_z;
    private float fog_near;
    private float fog_far;
    private int debris_mode;
    private bool light_backdrop;
    private bool fog_backdrop;
    private bool swap_backdrop;
    private float backdrop_fog_near;
    private float backdrop_fog_far;
    private float max_tilt;
    private bool auto_level;
    private float impulse_rate;
    private float decay_velocity;
    private float decay_spin;
    private int backdrop_asset;
    private string greetings;
    private string notes;
    private int system_id;
    private float galaxy_x;
    private float galaxy_y;
    private float galaxy_z;
    private string sector_type;

    [Category("Base Props")]
    [Description("Name of the sector")]
    public string Name
    {
      get
      {
        return this.name;
      }
      set
      {
        this.name = value;
      }
    }

    [Category("Base Props")]
    [Description("The Sector ID of this System.")]
    public int SectorID
    {
      get
      {
        return this.sector_id;
      }
      set
      {
        this.sector_id = value;
      }
    }

    [Description("The width of the Sector, centered on 0,0,0")]
    [Category("Base Props")]
    public float Width
    {
      get
      {
        return this.width;
      }
      set
      {
        this.width = value;
      }
    }

    [Description("The height of the Sector, centered on 0,0,0")]
    [Category("Base Props")]
    public float Height
    {
      get
      {
        return this.height;
      }
      set
      {
        this.height = value;
      }
    }

    [Category("Base Props")]
    [Description("The depth of the Sector, centered on 0,0,0")]
    public float Depth
    {
      get
      {
        return this.depth;
      }
      set
      {
        this.depth = value;
      }
    }

    [Description("Description and values currently unkown, leave be for the time being.")]
    [Category("Misc. Properties")]
    public int GridX
    {
      get
      {
        return this.grid_x;
      }
      set
      {
        this.grid_x = value;
      }
    }

    [Description("Description and values currently unkown, leave be for the time being.")]
    [Category("Misc. Properties")]
    public int GridY
    {
      get
      {
        return this.grid_y;
      }
      set
      {
        this.grid_y = value;
      }
    }

    [Category("Misc. Properties")]
    [Description("Description and values currently unkown, leave be for the time being.")]
    public int GridZ
    {
      get
      {
        return this.grid_z;
      }
      set
      {
        this.grid_z = value;
      }
    }

    [Category("Base Props")]
    [Description("Fog near visibility, distance at which you start to see the sector fog.")]
    public float FogNear
    {
      get
      {
        return this.fog_near;
      }
      set
      {
        this.fog_near = value;
      }
    }

    [Description("Fog far visibility, distance at which you cant see anything anymore.")]
    [Category("Base Props")]
    public float FogFar
    {
      get
      {
        return this.fog_far;
      }
      set
      {
        this.fog_far = value;
      }
    }

    [Category("Base Props")]
    [Description("Sets the background debris mode, values currently unknown.")]
    public int DebrisMode
    {
      get
      {
        return this.debris_mode;
      }
      set
      {
        this.debris_mode = value;
      }
    }

    [Description("Description and values currently unkown, leave be for the time being.")]
    [Category("Base Props")]
    public bool LightBackdrop
    {
      get
      {
        return this.light_backdrop;
      }
      set
      {
        this.light_backdrop = value;
      }
    }

    [Category("Base Props")]
    [Description("Description and values currently unkown, leave be for the time being.")]
    public bool FogBackdrop
    {
      get
      {
        return this.fog_backdrop;
      }
      set
      {
        this.fog_backdrop = value;
      }
    }

    [Description("Description and values currently unkown, leave be for the time being.")]
    [Category("Base Props")]
    public bool SwapBackdrop
    {
      get
      {
        return this.swap_backdrop;
      }
      set
      {
        this.swap_backdrop = value;
      }
    }

    [Description("Background Fog near visibility, same as sector fog but for the backdround.")]
    [Category("Base Props")]
    public float BackdropFogNear
    {
      get
      {
        return this.backdrop_fog_near;
      }
      set
      {
        this.backdrop_fog_near = value;
      }
    }

    [Description("Background Fog far visibility, same as sector fog but for the backdround.")]
    [Category("Base Props")]
    public float BackdropFogFar
    {
      get
      {
        return this.backdrop_fog_far;
      }
      set
      {
        this.backdrop_fog_far = value;
      }
    }

    [Description("The maximum angle at which players ships tilt when moving about the sector.")]
    [Category("Base Props")]
    public float MaxTilt
    {
      get
      {
        return this.max_tilt;
      }
      set
      {
        this.max_tilt = value;
      }
    }

    [Description("Determines whether or not pilots ships can use the autolevel feature in this sector.")]
    [Category("Base Props")]
    public bool AutoLevel
    {
      get
      {
        return this.auto_level;
      }
      set
      {
        this.auto_level = value;
      }
    }

    [Description("The Maximum impulse rate of players in this sector")]
    [Category("Base Props")]
    public float ImpulseRate
    {
      get
      {
        return this.impulse_rate;
      }
      set
      {
        this.impulse_rate = value;
      }
    }

    [Description("Description and values currently unkown, leave be for the time being.")]
    [Category("Base Props")]
    public float DecayVelocity
    {
      get
      {
        return this.decay_velocity;
      }
      set
      {
        this.decay_velocity = value;
      }
    }

    [Category("Base Props")]
    [Description("Description and values currently unkown, leave be for the time being.")]
    public float DecaySpin
    {
      get
      {
        return this.decay_spin;
      }
      set
      {
        this.decay_spin = value;
      }
    }

    [Editor(typeof (BaseAssetsEditor), typeof (UITypeEditor))]
    [Description("The id of the 3d base asset that represents this sectors background.")]
    [Category("Base Props")]
    public int BackdropAsset
    {
      get
      {
        return this.backdrop_asset;
      }
      set
      {
        this.backdrop_asset = value;
      }
    }

    [Category("Base Props")]
    [Description("Greetings you see upon entering the sector")]
    public string Greetings
    {
      get
      {
        return this.greetings;
      }
      set
      {
        this.greetings = value;
      }
    }

    [Description("Any Dev Notes")]
    [Category("Misc. Properties")]
    public string Notes
    {
      get
      {
        return this.notes;
      }
      set
      {
        this.notes = value;
      }
    }

    [Category("Base Props")]
    [ReadOnly(true)]
    [Description("The System Id that this sector is part of.")]
    public int SystemID
    {
      get
      {
        return this.system_id;
      }
      set
      {
        this.system_id = value;
      }
    }

    [Description("The X Coordinate of this Sector in the Galaxy")]
    [Category("Misc. Properties")]
    public float GalaxyX
    {
      get
      {
        return this.galaxy_x;
      }
      set
      {
        this.galaxy_x = value;
      }
    }

    [Category("Misc. Properties")]
    [Description("The Y Coordinate of this Sector in the Galaxy")]
    public float GalaxyY
    {
      get
      {
        return this.galaxy_y;
      }
      set
      {
        this.galaxy_y = value;
      }
    }

    [Description("The Z Coordinate of this Sector in the Galaxy")]
    [Category("Misc. Properties")]
    public float GalaxyZ
    {
      get
      {
        return this.galaxy_z;
      }
      set
      {
        this.galaxy_z = value;
      }
    }

    [Description("The Type of Sector.")]
    [Browsable(true)]
    [TypeConverter(typeof (SectorTypeConvertor))]
    [Category("Base Props")]
    public string SectorType
    {
      get
      {
        string str = "";
        if (this.sector_type != null)
          str = this.sector_type;
        else if (HE_GlobalVars._listofSectorTypes.Length > 0)
          str = HE_GlobalVars._listofSectorTypes[0];
        return str;
      }
      set
      {
        this.sector_type = value;
      }
    }
  }
}
