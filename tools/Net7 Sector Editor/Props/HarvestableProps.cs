﻿// Decompiled with JetBrains decompiler
// Type: N7.Props.HarvestableProps
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using N7.Utilities;
using System;
using System.ComponentModel;
using System.Drawing.Design;

namespace N7.Props
{
  internal class HarvestableProps : BaseProps
  {
    private string level;
    private string field;
    private string res_type;
    private int res_count;
    private float spawn_radius;
    private int pop_rock_chance;
    private string spawn_group_id;
    private float max_field_radius;
    private int respawn_timer;

    public void fillBaseProps(BaseProps bp)
    {
      this.SectorID = bp.SectorID;
      this.NavType = bp.NavType;
      this.Signature = bp.Signature;
      this.IsHuge = bp.IsHuge;
      this.BaseXP = bp.BaseXP;
      this.ExplorationRange = bp.ExplorationRange;
      this.BaseAssetID = bp.BaseAssetID;
      this.Color = bp.Color;
      this.Type = bp.Type;
      this.Scale = bp.Scale;
      this.PositionX = bp.PositionX;
      this.PositionY = bp.PositionY;
      this.PositionZ = bp.PositionZ;
      this.Name = bp.Name;
      this.AppearsInRadar = bp.AppearsInRadar;
      this.RadarRange = bp.RadarRange;
      this.Destination = bp.Destination;
    }

    [Description("The average level of the resource field")]
    [Browsable(true)]
    [TypeConverter(typeof (LevelConverter))]
    [Category("Harvestable Object Props")]
    public string Level
    {
      get
      {
        string str = "";
        if (this.level != null)
          str = this.level;
        else if (HE_GlobalVars._ListofLevels.Length > 0)
        {
          Array.Sort<string>(HE_GlobalVars._ListofLevels);
          str = HE_GlobalVars._ListofLevels[0];
        }
        return str;
      }
      set
      {
        this.level = value;
      }
    }

    [TypeConverter(typeof (FieldTypeConverter))]
    [Browsable(true)]
    [Category("Harvestable Object Props")]
    [Description("The type of resource field")]
    public string Field
    {
      get
      {
        string str = "";
        if (this.field != null)
          str = this.field;
        else if (HE_GlobalVars._ListofFieldTypes.Length > 0)
          str = HE_GlobalVars._ListofFieldTypes[0];
        return str;
      }
      set
      {
        this.field = value;
      }
    }

    [Category("Harvestable Object Props")]
    [Description("The number of harvestable(resource) objects in this field")]
    public int ResCount
    {
      get
      {
        return this.res_count;
      }
      set
      {
        this.res_count = value;
      }
    }

    [Editor(typeof (ResTypeEditor), typeof (UITypeEditor))]
    [Category("Harvestable Object Props")]
    [Description("The types of harvestables(resources) in this field, collection object")]
    public string ResType
    {
      get
      {
        return this.res_type;
      }
      set
      {
        this.res_type = value;
      }
    }

    [Description("The maximum radius at which the field spawns resources.")]
    [Category("Harvestable Object Props")]
    public float MaxFieldRadius
    {
      get
      {
        return this.max_field_radius;
      }
      set
      {
        this.max_field_radius = value;
      }
    }

    [Category("Harvestable Object Props")]
    [Description("The time in minutes which the field respawns at once any roid is touched, after the last touch.")]
    public int RespawnTimer
    {
      get
      {
        return this.respawn_timer;
      }
      set
      {
        this.respawn_timer = value;
      }
    }

    [Category("Harvestable Object Props")]
    [Description("If the field contains mob guardians, the radius at which they will spread out too. Radius should be no larger then the Maximum field size.")]
    public float MobSpawnRadius
    {
      get
      {
        return this.spawn_radius;
      }
      set
      {
        this.spawn_radius = value;
      }
    }

    [Editor(typeof (ContrastEditor), typeof (UITypeEditor))]
    [Category("Harvestable Object Props")]
    [Description("The percentage chance that the field will contain pop-rocks. (1-100)")]
    public int PopRockChance
    {
      get
      {
        return this.pop_rock_chance;
      }
      set
      {
        this.pop_rock_chance = value;
      }
    }

    [Category("Harvestable Object Props")]
    [Description("The mob spawn group that guard this field, collection object")]
    [Editor(typeof (MobGroupEditor), typeof (UITypeEditor))]
    public string SpawnGroup
    {
      get
      {
        return this.spawn_group_id;
      }
      set
      {
        this.spawn_group_id = value;
      }
    }
  }
}
