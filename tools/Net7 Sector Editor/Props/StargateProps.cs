﻿// Decompiled with JetBrains decompiler
// Type: N7.Props.StargateProps
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using N7.Utilities;
using System;
using System.ComponentModel;

namespace N7.Props
{
  internal class StargateProps : BaseProps
  {
    private bool is_class_specific;
    private string faction_id;
    private string min_security_level;

    public void fillBaseProps(BaseProps bp)
    {
      this.SectorID = bp.SectorID;
      this.NavType = bp.NavType;
      this.Signature = bp.Signature;
      this.IsHuge = bp.IsHuge;
      this.BaseXP = bp.BaseXP;
      this.ExplorationRange = bp.ExplorationRange;
      this.BaseAssetID = bp.BaseAssetID;
      this.Color = bp.Color;
      this.Type = bp.Type;
      this.Scale = bp.Scale;
      this.PositionX = bp.PositionX;
      this.PositionY = bp.PositionY;
      this.PositionZ = bp.PositionZ;
      this.Name = bp.Name;
      this.AppearsInRadar = bp.AppearsInRadar;
      this.RadarRange = bp.RadarRange;
      this.Destination = bp.Destination;
    }

    [Category("Min Security Level")]
    [TypeConverter(typeof (SecLevelConverter))]
    [Description("Securty level alowed to gate")]
    [Browsable(true)]
    public string MinSecurityLevel
    {
      get
      {
        string str = "";
        if (this.min_security_level != null)
          str = this.min_security_level;
        else if (HE_GlobalVars._ListofSecuritys.Length > 0)
          str = HE_GlobalVars._ListofSecuritys[0];
        return str;
      }
      set
      {
        this.min_security_level = value;
      }
    }

    [Description("Is this stargate limited to a certain class ?")]
    [Category("Stargate Object Props")]
    public bool IsClassSpecific
    {
      get
      {
        return this.is_class_specific;
      }
      set
      {
        this.is_class_specific = value;
      }
    }

    [Description("The faction ID of the faction that controls this stargate")]
    [Category("Stargate Object Props")]
    [Browsable(true)]
    [TypeConverter(typeof (FactionConverter))]
    public string FactionID
    {
      get
      {
        string str = "";
        if (this.faction_id != null)
          str = this.faction_id;
        else if (HE_GlobalVars._ListofFactions.Length > 0)
        {
          Array.Sort<string>(HE_GlobalVars._ListofFactions);
          str = HE_GlobalVars._ListofFactions[0];
        }
        return str;
      }
      set
      {
        this.faction_id = value;
      }
    }
  }
}
