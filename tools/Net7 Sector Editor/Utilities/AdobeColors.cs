﻿// Decompiled with JetBrains decompiler
// Type: N7.Utilities.AdobeColors
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using System.Drawing;

namespace N7.Utilities
{
  public class AdobeColors
  {
    public static Color SetBrightness(Color c, double brightness)
    {
      AdobeColors.HSL hsl = AdobeColors.RGB_to_HSL(c);
      hsl.L = brightness;
      return AdobeColors.HSL_to_RGB(hsl);
    }

    public static Color ModifyBrightness(Color c, double brightness)
    {
      AdobeColors.HSL hsl = AdobeColors.RGB_to_HSL(c);
      hsl.L *= brightness;
      return AdobeColors.HSL_to_RGB(hsl);
    }

    public static Color SetSaturation(Color c, double Saturation)
    {
      AdobeColors.HSL hsl = AdobeColors.RGB_to_HSL(c);
      hsl.S = Saturation;
      return AdobeColors.HSL_to_RGB(hsl);
    }

    public static Color ModifySaturation(Color c, double Saturation)
    {
      AdobeColors.HSL hsl = AdobeColors.RGB_to_HSL(c);
      hsl.S *= Saturation;
      return AdobeColors.HSL_to_RGB(hsl);
    }

    public static Color SetHue(Color c, double Hue)
    {
      AdobeColors.HSL hsl = AdobeColors.RGB_to_HSL(c);
      hsl.H = Hue;
      return AdobeColors.HSL_to_RGB(hsl);
    }

    public static Color ModifyHue(Color c, double Hue)
    {
      AdobeColors.HSL hsl = AdobeColors.RGB_to_HSL(c);
      hsl.H *= Hue;
      return AdobeColors.HSL_to_RGB(hsl);
    }

    public static Color HSL_to_RGB(AdobeColors.HSL hsl)
    {
      int num1 = AdobeColors.Round(hsl.L * (double) byte.MaxValue);
      int num2 = AdobeColors.Round((1.0 - hsl.S) * (hsl.L / 1.0) * (double) byte.MaxValue);
      double num3 = (double) (num1 - num2) / (double) byte.MaxValue;
      if (hsl.H >= 0.0 && hsl.H <= 1.0 / 6.0)
      {
        int green = AdobeColors.Round((hsl.H - 0.0) * num3 * 1530.0 + (double) num2);
        return Color.FromArgb(num1, green, num2);
      }
      if (hsl.H <= 1.0 / 3.0)
        return Color.FromArgb(AdobeColors.Round(-((hsl.H - 1.0 / 6.0) * num3) * 1530.0 + (double) num1), num1, num2);
      if (hsl.H <= 0.5)
      {
        int blue = AdobeColors.Round((hsl.H - 1.0 / 3.0) * num3 * 1530.0 + (double) num2);
        return Color.FromArgb(num2, num1, blue);
      }
      if (hsl.H <= 2.0 / 3.0)
      {
        int green = AdobeColors.Round(-((hsl.H - 0.5) * num3) * 1530.0 + (double) num1);
        return Color.FromArgb(num2, green, num1);
      }
      if (hsl.H <= 5.0 / 6.0)
        return Color.FromArgb(AdobeColors.Round((hsl.H - 2.0 / 3.0) * num3 * 1530.0 + (double) num2), num2, num1);
      if (hsl.H > 1.0)
        return Color.FromArgb(0, 0, 0);
      int blue1 = AdobeColors.Round(-((hsl.H - 5.0 / 6.0) * num3) * 1530.0 + (double) num1);
      return Color.FromArgb(num1, num2, blue1);
    }

    public static AdobeColors.HSL RGB_to_HSL(Color c)
    {
      AdobeColors.HSL hsl = new AdobeColors.HSL();
      int num1;
      int num2;
      if ((int) c.R > (int) c.G)
      {
        num1 = (int) c.R;
        num2 = (int) c.G;
      }
      else
      {
        num1 = (int) c.G;
        num2 = (int) c.R;
      }
      if ((int) c.B > num1)
        num1 = (int) c.B;
      else if ((int) c.B < num2)
        num2 = (int) c.B;
      int num3 = num1 - num2;
      hsl.L = (double) num1 / (double) byte.MaxValue;
      hsl.S = num1 != 0 ? (double) num3 / (double) num1 : 0.0;
      double num4 = num3 != 0 ? 60.0 / (double) num3 : 0.0;
      hsl.H = num1 != (int) c.R ? (num1 != (int) c.G ? (num1 != (int) c.B ? 0.0 : (240.0 + num4 * (double) ((int) c.R - (int) c.G)) / 360.0) : (120.0 + num4 * (double) ((int) c.B - (int) c.R)) / 360.0) : ((int) c.G >= (int) c.B ? num4 * (double) ((int) c.G - (int) c.B) / 360.0 : (360.0 + num4 * (double) ((int) c.G - (int) c.B)) / 360.0);
      return hsl;
    }

    public static AdobeColors.CMYK RGB_to_CMYK(Color c)
    {
      AdobeColors.CMYK cmyk = new AdobeColors.CMYK();
      double num = 1.0;
      cmyk.C = (double) ((int) byte.MaxValue - (int) c.R) / (double) byte.MaxValue;
      if (num > cmyk.C)
        num = cmyk.C;
      cmyk.M = (double) ((int) byte.MaxValue - (int) c.G) / (double) byte.MaxValue;
      if (num > cmyk.M)
        num = cmyk.M;
      cmyk.Y = (double) ((int) byte.MaxValue - (int) c.B) / (double) byte.MaxValue;
      if (num > cmyk.Y)
        num = cmyk.Y;
      if (num > 0.0)
        cmyk.K = num;
      return cmyk;
    }

    public static Color CMYK_to_RGB(AdobeColors.CMYK _cmyk)
    {
      return Color.FromArgb(AdobeColors.Round((double) byte.MaxValue - (double) byte.MaxValue * _cmyk.C), AdobeColors.Round((double) byte.MaxValue - (double) byte.MaxValue * _cmyk.M), AdobeColors.Round((double) byte.MaxValue - (double) byte.MaxValue * _cmyk.Y));
    }

    private static int Round(double val)
    {
      int num = (int) val;
      if ((int) (val * 100.0) % 100 >= 50)
        ++num;
      return num;
    }

    public class HSL
    {
      private double _h;
      private double _s;
      private double _l;

      public HSL()
      {
        this._h = 0.0;
        this._s = 0.0;
        this._l = 0.0;
      }

      public double H
      {
        get
        {
          return this._h;
        }
        set
        {
          this._h = value;
          this._h = this._h > 1.0 ? 1.0 : (this._h < 0.0 ? 0.0 : this._h);
        }
      }

      public double S
      {
        get
        {
          return this._s;
        }
        set
        {
          this._s = value;
          this._s = this._s > 1.0 ? 1.0 : (this._s < 0.0 ? 0.0 : this._s);
        }
      }

      public double L
      {
        get
        {
          return this._l;
        }
        set
        {
          this._l = value;
          this._l = this._l > 1.0 ? 1.0 : (this._l < 0.0 ? 0.0 : this._l);
        }
      }
    }

    public class CMYK
    {
      private double _c;
      private double _m;
      private double _y;
      private double _k;

      public CMYK()
      {
        this._c = 0.0;
        this._m = 0.0;
        this._y = 0.0;
        this._k = 0.0;
      }

      public double C
      {
        get
        {
          return this._c;
        }
        set
        {
          this._c = value;
          this._c = this._c > 1.0 ? 1.0 : (this._c < 0.0 ? 0.0 : this._c);
        }
      }

      public double M
      {
        get
        {
          return this._m;
        }
        set
        {
          this._m = value;
          this._m = this._m > 1.0 ? 1.0 : (this._m < 0.0 ? 0.0 : this._m);
        }
      }

      public double Y
      {
        get
        {
          return this._y;
        }
        set
        {
          this._y = value;
          this._y = this._y > 1.0 ? 1.0 : (this._y < 0.0 ? 0.0 : this._y);
        }
      }

      public double K
      {
        get
        {
          return this._k;
        }
        set
        {
          this._k = value;
          this._k = this._k > 1.0 ? 1.0 : (this._k < 0.0 ? 0.0 : this._k);
        }
      }
    }
  }
}
