﻿// Decompiled with JetBrains decompiler
// Type: N7.Utilities.QuaternionCalc
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using System;

namespace N7.Utilities
{
  internal class QuaternionCalc
  {
    public double[] AngleToQuat(double headingDeg, double attitudeDeg, double bankDeg)
    {
      double[] numArray = new double[4];
      double num1 = Math.PI / 180.0 * headingDeg;
      double num2 = Math.PI / 180.0 * attitudeDeg;
      double num3 = Math.PI / 180.0 * bankDeg;
      double num4 = Math.Cos(num1 / 2.0);
      double num5 = Math.Sin(num1 / 2.0);
      double num6 = Math.Cos(num2 / 2.0);
      double num7 = Math.Sin(num2 / 2.0);
      double num8 = Math.Cos(num3 / 2.0);
      double num9 = Math.Sin(num3 / 2.0);
      double num10 = num4 * num6;
      double num11 = num5 * num7;
      numArray[0] = num10 * num8 - num11 * num9;
      numArray[1] = num10 * num9 + num11 * num8;
      numArray[2] = num5 * num6 * num8 + num4 * num7 * num9;
      numArray[3] = num4 * num7 * num8 - num5 * num6 * num9;
      return numArray;
    }

    public double[] QuatToAngle(double[] q1)
    {
      double[] numArray = new double[3];
      double num1 = q1[0] * q1[0];
      double num2 = q1[1] * q1[1];
      double num3 = q1[2] * q1[2];
      double num4 = q1[3] * q1[3];
      double num5 = num2 + num3 + num4 + num1;
      double num6 = q1[1] * q1[2] + q1[3] * q1[0];
      if (num6 > 0.499 * num5)
      {
        numArray[0] = 2.0 * Math.Atan2(q1[1], q1[0]);
        numArray[1] = Math.PI / 2.0;
        numArray[2] = 0.0;
        return numArray;
      }
      if (num6 < -0.499 * num5)
      {
        numArray[0] = -2.0 * Math.Atan2(q1[1], q1[0]);
        numArray[1] = -1.0 * Math.PI / 2.0;
        numArray[2] = 0.0;
        return numArray;
      }
      double num7 = Math.Atan2(2.0 * q1[2] * q1[0] - 2.0 * q1[1] * q1[3], num2 - num3 - num4 + num1);
      double num8 = Math.Asin(2.0 * num6 / num5);
      double num9 = Math.Atan2(2.0 * q1[1] * q1[0] - 2.0 * q1[2] * q1[3], -num2 + num3 - num4 + num1);
      double num10 = 180.0 / Math.PI * num7;
      double num11 = 180.0 / Math.PI * num8;
      double num12 = 180.0 / Math.PI * num9;
      numArray[0] = num10;
      numArray[1] = num11;
      numArray[2] = num12;
      return numArray;
    }
  }
}
