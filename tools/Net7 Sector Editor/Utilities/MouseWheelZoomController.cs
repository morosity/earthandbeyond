﻿// Decompiled with JetBrains decompiler
// Type: N7.Utilities.MouseWheelZoomController
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using System.Drawing;
using UMD.HCIL.Piccolo;
using UMD.HCIL.Piccolo.Event;

namespace N7.Utilities
{
  public class MouseWheelZoomController
  {
    public static float MIN_SCALE = 0.0001f;
    public static float MAX_SCALE = 2500f;
    private PCamera camera;

    public MouseWheelZoomController(PCamera camera)
    {
      this.camera = camera;
      camera.Canvas.ZoomEventHandler = (PZoomEventHandler) null;
      camera.MouseWheel += new PInputEventHandler(this.OnMouseWheel);
    }

    public void OnMouseWheel(object o, PInputEventArgs ea)
    {
      float viewScale = this.camera.ViewScale;
      float scale = (float) (1.0 + 1.0 / 1000.0 * (double) ea.WheelDelta);
      float num = viewScale * scale;
      if ((double) num < (double) MouseWheelZoomController.MIN_SCALE)
        this.camera.ViewScale = MouseWheelZoomController.MIN_SCALE;
      else if ((double) MouseWheelZoomController.MAX_SCALE > 0.0 && (double) num > (double) MouseWheelZoomController.MAX_SCALE)
      {
        this.camera.ViewScale = MouseWheelZoomController.MAX_SCALE;
      }
      else
      {
        PointF position = ea.Position;
        this.camera.ScaleViewBy(scale, position.X, position.Y);
      }
    }
  }
}
