﻿// Decompiled with JetBrains decompiler
// Type: N7.Utilities.FactionConverter
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using System.Collections;
using System.ComponentModel;

namespace N7.Utilities
{
  public class FactionConverter : StringConverter
  {
    public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
    {
      return true;
    }

    public override bool GetStandardValuesExclusive(ITypeDescriptorContext context)
    {
      return true;
    }

    public override TypeConverter.StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
    {
      return new TypeConverter.StandardValuesCollection((ICollection) HE_GlobalVars._ListofFactions);
    }
  }
}
