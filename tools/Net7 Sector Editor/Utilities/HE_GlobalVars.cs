﻿// Decompiled with JetBrains decompiler
// Type: N7.Utilities.HE_GlobalVars
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

namespace N7.Utilities
{
  internal class HE_GlobalVars
  {
    internal static string[] _ListofTypes = new string[8]
    {
      "Mobs",
      "Planets",
      "Stargates",
      "Starbases",
      "Decorations",
      "Harvestables",
      "Radation",
      "GWell"
    };
    internal static string[] _ListofSecuritys = new string[11]
    {
      "USER",
      "",
      "HELPER",
      "BETA",
      "BETA_PLUS",
      "GM",
      "DGM",
      "HGM",
      "DEV",
      "SDEV",
      "ADMIN"
    };
    internal static string[] _ListofFieldTypes = new string[6]
    {
      "Random",
      "Ring",
      "Donut",
      "Cylinder",
      "Sphere",
      "Gas Cloud Clump"
    };
    internal static string[] _ListofNavTypes = new string[3]
    {
      "0",
      "1",
      "2"
    };
    internal static string[] _ListofLevels = new string[9]
    {
      "1",
      "2",
      "3",
      "4",
      "5",
      "6",
      "7",
      "8",
      "9"
    };
    internal static string[] _listofSectorTypes = new string[3]
    {
      "Space Sector",
      "Rocky Planet Surface",
      "Gas Giant Surface"
    };
    internal static string[] _ListofFactions;
  }
}
