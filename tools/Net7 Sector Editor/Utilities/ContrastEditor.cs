﻿// Decompiled with JetBrains decompiler
// Type: N7.Utilities.ContrastEditor
// Assembly: Net7 Sector Editor, Version=1.2.2.0, Culture=neutral, PublicKeyToken=null
// MVID: 13AC8D32-FAF4-42C7-9421-35D01016951D
// Assembly location: D:\Server\eab\Tools\Net7 Sector Editor.exe

using N7.GUI;
using System;
using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace N7.Utilities
{
  public class ContrastEditor : UITypeEditor
  {
    public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
    {
      return UITypeEditorEditStyle.DropDown;
    }

    public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
    {
      IWindowsFormsEditorService service = provider.GetService(typeof (IWindowsFormsEditorService)) as IWindowsFormsEditorService;
      if (service != null)
      {
        frmContrast frmContrast = new frmContrast();
        frmContrast.trackBar1.Value = (int) value;
        frmContrast.BarValue = frmContrast.trackBar1.Value;
        frmContrast._wfes = service;
        service.DropDownControl((Control) frmContrast);
        value = (object) frmContrast.BarValue;
      }
      return value;
    }
  }
}
