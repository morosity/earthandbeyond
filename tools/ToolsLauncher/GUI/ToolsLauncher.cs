﻿// Decompiled with JetBrains decompiler
// Type: ToolsLauncher.ToolsLauncher
// Assembly: ToolsLauncher, Version=1.1.0.1, Culture=neutral, PublicKeyToken=null
// MVID: B3444A73-323E-44BC-BC2D-02089BA9FF5C
// Assembly location: D:\Server\eab\Tools\ToolsLauncher.exe

using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using ToolsLauncher.GUI;
using ToolsLauncher.Properties;
using ToolsLauncher.Updateing;

namespace ToolsLauncher
{
  public class ToolsLauncher : Form
  {
    private List<FileLinkList> m_LinkList = new List<FileLinkList>();
    private List<FtpWindow> m_FtpWindows = new List<FtpWindow>();
    private List<FtpAddy> m_FtpLocation = new List<FtpAddy>();
    private List<ToolStripMenuItem> FtpUrlList = new List<ToolStripMenuItem>();
    private bool m_Close = false;
    private IContainer components = (IContainer) null;
    private IRCMessenger IrcMsg;
    private BackgroundWorker m_Worker;
    private Button MobEdit;
    private Button StationEdit;
    private Button MissionEdit;
    private Button ItemEdit;
    private Button SectorEdit;
    private Button EffectEdit;
    private MenuStrip menuStrip1;
    private ToolStripMenuItem fTPToolStripMenuItem;
    private ToolStripMenuItem loadFTPToolStripMenuItem;
    private ToolStripMenuItem messengerToolStripMenuItem;
    private ToolStripMenuItem settingsToolStripMenuItem;
    private Button LaunchN7;
    private Button ToolsPatch;
    private ContextMenuStrip contextMenuStrip1;
    private ToolStripMenuItem Launch;
    private ToolStripMenuItem EffectEditorMenu;
    private ToolStripMenuItem ItemEditorMenu;
    private ToolStripMenuItem MissionEditorMenu;
    private ToolStripMenuItem MobEditorMenu;
    private ToolStripMenuItem SectorEditorMenu;
    private ToolStripMenuItem StationEditorMenu;
    private ToolStripMenuItem Update;
    private ToolStripMenuItem Maximize;
    private ToolStripMenuItem Quit;
    private NotifyIcon notifyIcon1;
    private ToolStripSeparator toolStripSeparator1;
    private ToolStripMenuItem LaunchN7Menu;
    private ToolStripSeparator toolStripSeparator2;
    private ToolStripMenuItem Minimize;
    private ToolStripSeparator toolStripSeparator3;
    private ToolStripMenuItem helpToolStripMenuItem;
    private ToolStripMenuItem aboutToolStripMenuItem;
    private Button FactionEditor;
    private ToolStripMenuItem FactionEditorMenu;

    public ToolsLauncher()
    {
      this.InitializeComponent();
      this.m_LinkList.Add(new FileLinkList("N7 Mob Editor.exe", this.MobEdit));
      this.m_LinkList.Add(new FileLinkList("Station Tools.exe", this.StationEdit));
      this.m_LinkList.Add(new FileLinkList("net7 Sector Editor.exe", this.SectorEdit));
      this.m_LinkList.Add(new FileLinkList("net7 Item Editor.exe", this.ItemEdit));
      this.m_LinkList.Add(new FileLinkList("Effect Editor.exe", this.EffectEdit));
      this.m_LinkList.Add(new FileLinkList("MissionEditor.exe", this.MissionEdit));
      this.m_LinkList.Add(new FileLinkList("FactionEditor.exe", this.FactionEditor));
      this.m_LinkList.Add(new FileLinkList("ToolsPatcher", this.ToolsPatch));
      this.m_LinkList.Add(new FileLinkList("N7 Mob Editor.exe", this.MobEditorMenu));
      this.m_LinkList.Add(new FileLinkList("Station Tools.exe", this.StationEditorMenu));
      this.m_LinkList.Add(new FileLinkList("net7 Sector Editor.exe", this.SectorEditorMenu));
      this.m_LinkList.Add(new FileLinkList("net7 Item Editor.exe", this.ItemEditorMenu));
      this.m_LinkList.Add(new FileLinkList("Effect Editor.exe", this.EffectEditorMenu));
      this.m_LinkList.Add(new FileLinkList("MissionEditor.exe", this.MissionEditorMenu));
      this.m_LinkList.Add(new FileLinkList("FactionEditor.exe", this.FactionEditorMenu));
      this.m_LinkList.Add(new FileLinkList("ToolsPatcher", this.Update));
      MySqlConnection connection = new MySqlConnection(SQLData.ConnStr("Net7_toolsinfo"));
      DataTable dataTable = new DataTable();
      MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter("SELECT ftp_list.FTP_ID,ftp_list.FTP_Name,ftp_list.FTP_Path,ftp_list.Username,ftp_list.`Password`,ftp_list.Usergroup FROM ftp_list", connection);
      if (dataTable.Rows.Count > 0)
      {
        for (int index = 0; index < dataTable.Rows.Count; ++index)
        {
          DataRow row = dataTable.Rows[index];
          this.m_FtpLocation.Add(new FtpAddy(row["FTP_Path"].ToString(), row["FTP_Name"].ToString()));
        }
      }
      this.CreateFtpLinks();
    }

    private string FindExeFile(object Object)
    {
      if (Object.GetType() == typeof (Button))
      {
        foreach (FileLinkList link in this.m_LinkList)
        {
          if (link.BObject == Object)
            return link.ExeFile;
        }
      }
      else if (Object.GetType() == typeof (ToolStripMenuItem))
      {
        foreach (FileLinkList link in this.m_LinkList)
        {
          if (link.MObject == Object)
            return link.ExeFile;
        }
      }
      return "";
    }

    private void ClickIcon(object sender, EventArgs e)
    {
      object Object = (object) null;
      if (sender.GetType() == typeof (Button))
        Object = (object) (Button) sender;
      else if (sender.GetType() == typeof (ToolStripMenuItem))
        Object = (object) (ToolStripMenuItem) sender;
      ProcessStartInfo startInfo = new ProcessStartInfo();
      string path2 = this.FindExeFile(Object);
      if (path2 == "ToolsPatcher")
      {
        this.DoCheckForUpdates();
        path2 = "";
      }
      if (!(path2 != ""))
        return;
      startInfo.WorkingDirectory = Directory.GetCurrentDirectory();
      startInfo.FileName = Path.Combine(startInfo.WorkingDirectory, path2);
      if (File.Exists(startInfo.FileName))
        Process.Start(startInfo);
    }

    private void CreateFtpLinks()
    {
      foreach (FtpAddy ftpAddy in this.m_FtpLocation)
      {
        ToolStripMenuItem toolStripMenuItem = new ToolStripMenuItem(ftpAddy.Name);
        toolStripMenuItem.Size = new Size(152, 22);
        toolStripMenuItem.Click += new EventHandler(this.loadFTPToolStripMenuItem_Click);
        this.FtpUrlList.Add(toolStripMenuItem);
      }
      this.loadFTPToolStripMenuItem.DropDownItems.AddRange((ToolStripItem[]) this.FtpUrlList.ToArray());
    }

    private void loadFTPToolStripMenuItem_Click(object sender, EventArgs e)
    {
      ToolStripMenuItem toolStripMenuItem = (ToolStripMenuItem) sender;
      this.FTPWindowClose();
      foreach (FtpAddy ftpAddy in this.m_FtpLocation)
      {
        if (toolStripMenuItem.Text == ftpAddy.Name)
          this.m_FtpWindows.Add(new FtpWindow(ftpAddy.Location, ftpAddy.Name));
      }
    }

    public void FTPWindowClose()
    {
      List<FtpWindow> ftpWindowList = new List<FtpWindow>();
      foreach (FtpWindow ftpWindow in this.m_FtpWindows)
      {
        if (ftpWindow.m_WindowDead)
        {
          ftpWindowList.Add(ftpWindow);
          break;
        }
      }
      foreach (FtpWindow ftpWindow in ftpWindowList)
        this.m_FtpWindows.Remove(ftpWindow);
    }

    private void messengerToolStripMenuItem_Click(object sender, EventArgs e)
    {
      if (this.IrcMsg == null || this.IrcMsg.IsDisposed)
        this.IrcMsg = new IRCMessenger();
      this.IrcMsg.Show();
    }

    private void ToolsLauncher_FormClosing(object sender, FormClosingEventArgs e)
    {
      if (!this.m_Close)
        e.Cancel = true;
      else
        this.notifyIcon1.Visible = false;
      this.Hide();
    }

    private void ToolsLauncher_Resize(object sender, EventArgs e)
    {
      if (FormWindowState.Minimized != this.WindowState)
        return;
      this.ShowInTaskbar = false;
      this.Hide();
      this.Maximize.Enabled = true;
      this.Minimize.Enabled = false;
      this.WindowState = FormWindowState.Normal;
    }

    private void notifyIcon1_DoubleClick(object sender, EventArgs e)
    {
      this.Show();
      this.ShowInTaskbar = true;
      this.WindowState = FormWindowState.Normal;
    }

    private void Quit_Click(object sender, EventArgs e)
    {
      this.m_Close = true;
      if (this.IrcMsg != null && !this.IrcMsg.IsDisposed)
        this.IrcMsg.CloseWindow();
      foreach (Form ftpWindow in this.m_FtpWindows)
        ftpWindow.Close();
      Application.Exit();
    }

    private void Maximize_Click(object sender, EventArgs e)
    {
      this.Show();
      this.ShowInTaskbar = true;
      this.WindowState = FormWindowState.Normal;
      this.Maximize.Enabled = false;
      this.Minimize.Enabled = true;
    }

    private void Minimize_Click(object sender, EventArgs e)
    {
      if (FormWindowState.Minimized == this.WindowState)
        this.ShowInTaskbar = false;
      this.Hide();
      this.Maximize.Enabled = true;
      this.Minimize.Enabled = false;
    }

    private void ToolsLauncher_Load(object sender, EventArgs e)
    {
      this.Location = new Point(Screen.PrimaryScreen.WorkingArea.Width - this.Size.Width - 20, Screen.PrimaryScreen.WorkingArea.Height - this.Size.Height - 20);
      this.DoCheckForUpdates();
    }

    private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
    {
      new SettingsFrm().Show((IWin32Window) this);
    }

    private void LaunchN7_Click(object sender, EventArgs e)
    {
      string launchNet7Path = Settings.Default.LaunchNet7Path;
      ProcessStartInfo startInfo = new ProcessStartInfo();
      startInfo.WorkingDirectory = launchNet7Path + "/";
      startInfo.FileName = Path.Combine(launchNet7Path, "LaunchNet7.exe");
      Console.Out.WriteLine(startInfo.WorkingDirectory);
      if (File.Exists(startInfo.FileName))
      {
        Process.Start(startInfo);
      }
      else
      {
        int num = (int) MessageBox.Show("Sorry we could not find LaunchNet7.exe please set your path in the settings.");
      }
    }

    private void DoCheckForUpdates()
    {
      BackgroundWorker backgroundWorker1 = new BackgroundWorker();
      backgroundWorker1.WorkerReportsProgress = true;
      backgroundWorker1.WorkerSupportsCancellation = true;
      backgroundWorker1.DoWork += (DoWorkEventHandler) ((sender, e) =>
      {
        BackgroundWorker backgroundWorker2 = (BackgroundWorker) sender;
        backgroundWorker2.ReportProgress(0, (object) "Checking Configuration");
        List<Updater> updaterList = new List<Updater>();
        updaterList.Add(new Updater()
        {
          BaseUrl = "http://toolspatch.net-7.org/",
          VersionFileName = "Version.txt",
          FileListFileName = "Files.txt",
          VersionCompareMode = VersionCompareMode.SameText,
          RestartOnUpdate = true,
          ChangelogFileName = ""
        });
        Progress progress = new Progress(0, updaterList.Count * 3);
        foreach (Updater updater in updaterList)
        {
          if (backgroundWorker2.CancellationPending)
          {
            e.Cancel = true;
            return;
          }
          backgroundWorker2.ReportProgress(progress.ToProgressPercentage());
          backgroundWorker2.ReportProgress(0, (object) ("Checking Updates For " + updater.Name));
          switch (updater.CheckForUpdates())
          {
            case UpdateCheckResult.UpdateAvailable:
            case UpdateCheckResult.DownloadRequired:
              progress.Increment();
              backgroundWorker2.ReportProgress(progress.ToProgressPercentage());
              backgroundWorker2.ReportProgress(0, (object) string.Format("Downloading Changelog for '{0}'", (object) updater.Name));
              updater.DownloadPatchlog();
              progress.Increment();
              backgroundWorker2.ReportProgress(progress.ToProgressPercentage());
              backgroundWorker2.ReportProgress(0, (object) ("Updating " + updater.Name));
              if (((DialogResult?) this.Invoke((Delegate) new ToolsLauncher.ToolsLauncher.DoDisplayUpdateCallback(this.DoDisplayUpdate), (object) updater)).GetValueOrDefault() == DialogResult.Cancel)
              {
                e.Cancel = true;
                return;
              }
              progress.Increment();
              backgroundWorker2.ReportProgress(progress.ToProgressPercentage());
              if (updater.SelfUpdateDetected && updater.SelfUpdateItem.CheckStatus != UpdateCheckStatus.Ok)
              {
                this.DoRunSelfUpdate(updater.SelfUpdateItem.TargetFileName);
                e.Cancel = true;
                return;
              }
              break;
            default:
              progress.Increment();
              progress.Increment();
              progress.Increment();
              backgroundWorker2.ReportProgress(progress.ToProgressPercentage());
              break;
          }
        }
        e.Result = (object) updaterList;
      });
      backgroundWorker1.ProgressChanged += (ProgressChangedEventHandler) ((sender, e) =>
      {
        if (e.UserState != null)
          ;
      });
      backgroundWorker1.RunWorkerCompleted += (RunWorkerCompletedEventHandler) ((sender, e) =>
      {
        if (e.Error != null)
        {
          this.DisplayErrorMessage("Error on client validation.", e.Error);
          this.m_Close = true;
          this.Close();
        }
        else if (e.Cancelled)
        {
          this.m_Close = true;
          this.Close();
        }
        else
        {
          List<Updater> result = e.Result as List<Updater>;
          if (result != null)
          {
            int num = 0;
            StringBuilder stringBuilder = new StringBuilder();
            foreach (Updater updater in result)
            {
              if (string.Equals(updater.Name, "net7", StringComparison.InvariantCultureIgnoreCase))
              {
                string str1 = string.Empty;
                if (!string.IsNullOrEmpty(updater.CurrentVersionText))
                  str1 = str1 + "Current Version: " + updater.CurrentVersionText;
                if (!string.Equals(updater.CurrentVersionText, updater.NewVersionText, StringComparison.InvariantCultureIgnoreCase) && !string.IsNullOrEmpty(updater.NewVersionText))
                {
                  if (str1.Length > 0)
                    str1 += ", ";
                  string str2 = str1 + "Server Version: " + updater.NewVersionText;
                }
              }
              if (updater.CheckResult == UpdateCheckResult.Timeout || updater.CheckResult == UpdateCheckResult.Error)
              {
                ++num;
                if (stringBuilder.Length > 0)
                  stringBuilder.Append("\n\n");
                if (updater.CheckResult == UpdateCheckResult.Timeout)
                  stringBuilder.Append("Timeout while checking updates for '" + updater.Name + "'");
                else if (updater.CheckResult == UpdateCheckResult.Error)
                  stringBuilder.Append("Error while checking updates for '" + updater.Name + "'");
                if (updater.Error != null)
                  stringBuilder.Append("\nDetails: " + updater.Error.Message);
              }
            }
          }
        }
      });
      this.m_Worker = backgroundWorker1;
      backgroundWorker1.RunWorkerAsync();
    }

    private DialogResult DoDisplayUpdate(Updater updater)
    {
      if (updater == null)
        return DialogResult.None;
      using (FormUpdate formUpdate = new FormUpdate(updater))
        return formUpdate.ShowDialog((IWin32Window) this);
    }

    private void DoRunSelfUpdate(string replaceFileName)
    {
      string tempFileName = Path.GetTempFileName();
      string str = Path.ChangeExtension(tempFileName, ".exe");
      if (File.Exists(str))
        File.Delete(str);
      File.Move(tempFileName, str);
      File.WriteAllBytes(str, Resources.ExeUpdater);
      Process.Start(str, string.Format("-exeFileName \"{0}\" -replaceFileName \"{1}\" -waitForPid {2}", (object) Path.GetFullPath(Application.ExecutablePath), (object) Path.GetFullPath(replaceFileName), (object) Process.GetCurrentProcess().Id));
    }

    private void DisplayErrorMessage(string message, Exception e)
    {
      int num = (int) MessageBox.Show((IWin32Window) this, message + "\nDetails: " + e.Message, Program.Title + " - Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
    }

    private void DisplayWarningMessage(string message)
    {
      int num = (int) MessageBox.Show((IWin32Window) this, message, Program.Title + " - Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
    }

    private void DisplayInformalMessage(string message)
    {
      int num = (int) MessageBox.Show((IWin32Window) this, message, Program.Title + " - Information", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
    }

    private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
    {
    }

    private void notifyIcon1_DoubleClick_1(object sender, EventArgs e)
    {
      this.Show();
    }

    private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
    {
      using (About about = new About())
      {
        int num = (int) about.ShowDialog((IWin32Window) this);
      }
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
      {
        this.notifyIcon1.Dispose();
        this.components.Dispose();
      }
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (ToolsLauncher.ToolsLauncher));
      this.MobEdit = new Button();
      this.StationEdit = new Button();
      this.MissionEdit = new Button();
      this.ItemEdit = new Button();
      this.SectorEdit = new Button();
      this.EffectEdit = new Button();
      this.menuStrip1 = new MenuStrip();
      this.fTPToolStripMenuItem = new ToolStripMenuItem();
      this.loadFTPToolStripMenuItem = new ToolStripMenuItem();
      this.messengerToolStripMenuItem = new ToolStripMenuItem();
      this.settingsToolStripMenuItem = new ToolStripMenuItem();
      this.helpToolStripMenuItem = new ToolStripMenuItem();
      this.aboutToolStripMenuItem = new ToolStripMenuItem();
      this.LaunchN7 = new Button();
      this.ToolsPatch = new Button();
      this.contextMenuStrip1 = new ContextMenuStrip(this.components);
      this.Launch = new ToolStripMenuItem();
      this.EffectEditorMenu = new ToolStripMenuItem();
      this.ItemEditorMenu = new ToolStripMenuItem();
      this.MissionEditorMenu = new ToolStripMenuItem();
      this.MobEditorMenu = new ToolStripMenuItem();
      this.SectorEditorMenu = new ToolStripMenuItem();
      this.StationEditorMenu = new ToolStripMenuItem();
      this.FactionEditorMenu = new ToolStripMenuItem();
      this.toolStripSeparator1 = new ToolStripSeparator();
      this.LaunchN7Menu = new ToolStripMenuItem();
      this.Update = new ToolStripMenuItem();
      this.toolStripSeparator3 = new ToolStripSeparator();
      this.Minimize = new ToolStripMenuItem();
      this.Maximize = new ToolStripMenuItem();
      this.toolStripSeparator2 = new ToolStripSeparator();
      this.Quit = new ToolStripMenuItem();
      this.notifyIcon1 = new NotifyIcon(this.components);
      this.FactionEditor = new Button();
      this.menuStrip1.SuspendLayout();
      this.contextMenuStrip1.SuspendLayout();
      this.SuspendLayout();
      this.MobEdit.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, (byte) 0);
      this.MobEdit.ForeColor = Color.FromArgb(192, 64, 0);
      this.MobEdit.Image = (Image) componentResourceManager.GetObject("MobEdit.Image");
      this.MobEdit.ImageAlign = ContentAlignment.MiddleLeft;
      this.MobEdit.Location = new Point(12, 170);
      this.MobEdit.Name = "MobEdit";
      this.MobEdit.Size = new Size(227, 40);
      this.MobEdit.TabIndex = 3;
      this.MobEdit.Text = "Mob Editor";
      this.MobEdit.TextAlign = ContentAlignment.MiddleRight;
      this.MobEdit.UseVisualStyleBackColor = true;
      this.MobEdit.Click += new EventHandler(this.ClickIcon);
      this.StationEdit.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, (byte) 0);
      this.StationEdit.ForeColor = Color.FromArgb(192, 64, 0);
      this.StationEdit.Image = (Image) componentResourceManager.GetObject("StationEdit.Image");
      this.StationEdit.ImageAlign = ContentAlignment.MiddleLeft;
      this.StationEdit.Location = new Point(12, 262);
      this.StationEdit.Name = "StationEdit";
      this.StationEdit.Size = new Size(227, 40);
      this.StationEdit.TabIndex = 5;
      this.StationEdit.Text = "Station Editor";
      this.StationEdit.TextAlign = ContentAlignment.MiddleRight;
      this.StationEdit.UseVisualStyleBackColor = true;
      this.StationEdit.Click += new EventHandler(this.ClickIcon);
      this.MissionEdit.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, (byte) 0);
      this.MissionEdit.ForeColor = Color.FromArgb(192, 64, 0);
      this.MissionEdit.Image = (Image) componentResourceManager.GetObject("MissionEdit.Image");
      this.MissionEdit.ImageAlign = ContentAlignment.MiddleLeft;
      this.MissionEdit.Location = new Point(12, 124);
      this.MissionEdit.Name = "MissionEdit";
      this.MissionEdit.Size = new Size(227, 40);
      this.MissionEdit.TabIndex = 2;
      this.MissionEdit.Text = "Mission Editor";
      this.MissionEdit.TextAlign = ContentAlignment.MiddleRight;
      this.MissionEdit.UseVisualStyleBackColor = true;
      this.MissionEdit.Click += new EventHandler(this.ClickIcon);
      this.ItemEdit.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, (byte) 0);
      this.ItemEdit.ForeColor = Color.FromArgb(192, 64, 0);
      this.ItemEdit.Image = (Image) componentResourceManager.GetObject("ItemEdit.Image");
      this.ItemEdit.ImageAlign = ContentAlignment.MiddleLeft;
      this.ItemEdit.Location = new Point(12, 78);
      this.ItemEdit.Name = "ItemEdit";
      this.ItemEdit.Size = new Size(227, 40);
      this.ItemEdit.TabIndex = 1;
      this.ItemEdit.Text = "Item Editor";
      this.ItemEdit.TextAlign = ContentAlignment.MiddleRight;
      this.ItemEdit.UseVisualStyleBackColor = true;
      this.ItemEdit.Click += new EventHandler(this.ClickIcon);
      this.SectorEdit.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, (byte) 0);
      this.SectorEdit.ForeColor = Color.FromArgb(192, 64, 0);
      this.SectorEdit.Image = (Image) componentResourceManager.GetObject("SectorEdit.Image");
      this.SectorEdit.ImageAlign = ContentAlignment.MiddleLeft;
      this.SectorEdit.Location = new Point(12, 216);
      this.SectorEdit.Name = "SectorEdit";
      this.SectorEdit.Size = new Size(227, 40);
      this.SectorEdit.TabIndex = 4;
      this.SectorEdit.Text = "Sector Editor";
      this.SectorEdit.TextAlign = ContentAlignment.MiddleRight;
      this.SectorEdit.UseVisualStyleBackColor = true;
      this.SectorEdit.Click += new EventHandler(this.ClickIcon);
      this.EffectEdit.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold | FontStyle.Italic, GraphicsUnit.Point, (byte) 0);
      this.EffectEdit.ForeColor = Color.FromArgb(192, 64, 0);
      this.EffectEdit.Image = (Image) componentResourceManager.GetObject("EffectEdit.Image");
      this.EffectEdit.ImageAlign = ContentAlignment.MiddleLeft;
      this.EffectEdit.Location = new Point(12, 32);
      this.EffectEdit.Name = "EffectEdit";
      this.EffectEdit.Size = new Size(227, 40);
      this.EffectEdit.TabIndex = 0;
      this.EffectEdit.Text = "Effect Editor";
      this.EffectEdit.TextAlign = ContentAlignment.MiddleRight;
      this.EffectEdit.UseVisualStyleBackColor = true;
      this.EffectEdit.Click += new EventHandler(this.ClickIcon);
      this.menuStrip1.BackColor = SystemColors.ButtonFace;
      this.menuStrip1.Items.AddRange(new ToolStripItem[4]
      {
        (ToolStripItem) this.fTPToolStripMenuItem,
        (ToolStripItem) this.messengerToolStripMenuItem,
        (ToolStripItem) this.settingsToolStripMenuItem,
        (ToolStripItem) this.helpToolStripMenuItem
      });
      this.menuStrip1.Location = new Point(0, 0);
      this.menuStrip1.Name = "menuStrip1";
      this.menuStrip1.Size = new Size(250, 24);
      this.menuStrip1.TabIndex = 7;
      this.menuStrip1.Text = "menuStrip1";
      this.fTPToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[1]
      {
        (ToolStripItem) this.loadFTPToolStripMenuItem
      });
      this.fTPToolStripMenuItem.Name = "fTPToolStripMenuItem";
      this.fTPToolStripMenuItem.Size = new Size(39, 20);
      this.fTPToolStripMenuItem.Text = "FTP";
      this.loadFTPToolStripMenuItem.Name = "loadFTPToolStripMenuItem";
      this.loadFTPToolStripMenuItem.Size = new Size(123, 22);
      this.loadFTPToolStripMenuItem.Text = "Load FTP";
      this.loadFTPToolStripMenuItem.Click += new EventHandler(this.loadFTPToolStripMenuItem_Click);
      this.messengerToolStripMenuItem.Name = "messengerToolStripMenuItem";
      this.messengerToolStripMenuItem.Size = new Size(76, 20);
      this.messengerToolStripMenuItem.Text = "Messenger";
      this.messengerToolStripMenuItem.Click += new EventHandler(this.messengerToolStripMenuItem_Click);
      this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
      this.settingsToolStripMenuItem.Size = new Size(61, 20);
      this.settingsToolStripMenuItem.Text = "Settings";
      this.settingsToolStripMenuItem.Click += new EventHandler(this.settingsToolStripMenuItem_Click);
      this.helpToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[1]
      {
        (ToolStripItem) this.aboutToolStripMenuItem
      });
      this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
      this.helpToolStripMenuItem.Size = new Size(44, 20);
      this.helpToolStripMenuItem.Text = "&Help";
      this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
      this.aboutToolStripMenuItem.Size = new Size(116, 22);
      this.aboutToolStripMenuItem.Text = "&About...";
      this.aboutToolStripMenuItem.Click += new EventHandler(this.aboutToolStripMenuItem_Click);
      this.LaunchN7.Image = (Image) componentResourceManager.GetObject("LaunchN7.Image");
      this.LaunchN7.ImageAlign = ContentAlignment.MiddleLeft;
      this.LaunchN7.Location = new Point(12, 354);
      this.LaunchN7.Name = "LaunchN7";
      this.LaunchN7.Size = new Size(94, 30);
      this.LaunchN7.TabIndex = 6;
      this.LaunchN7.Text = "Launch net7";
      this.LaunchN7.TextAlign = ContentAlignment.MiddleRight;
      this.LaunchN7.UseVisualStyleBackColor = true;
      this.LaunchN7.Click += new EventHandler(this.LaunchN7_Click);
      this.ToolsPatch.Image = (Image) componentResourceManager.GetObject("ToolsPatch.Image");
      this.ToolsPatch.ImageAlign = ContentAlignment.MiddleLeft;
      this.ToolsPatch.Location = new Point(112, 354);
      this.ToolsPatch.Name = "ToolsPatch";
      this.ToolsPatch.Size = new Size((int) sbyte.MaxValue, 30);
      this.ToolsPatch.TabIndex = 7;
      this.ToolsPatch.Text = "Check For Updates";
      this.ToolsPatch.TextAlign = ContentAlignment.MiddleRight;
      this.ToolsPatch.UseVisualStyleBackColor = true;
      this.ToolsPatch.Click += new EventHandler(this.ClickIcon);
      this.contextMenuStrip1.Items.AddRange(new ToolStripItem[7]
      {
        (ToolStripItem) this.Launch,
        (ToolStripItem) this.Update,
        (ToolStripItem) this.toolStripSeparator3,
        (ToolStripItem) this.Minimize,
        (ToolStripItem) this.Maximize,
        (ToolStripItem) this.toolStripSeparator2,
        (ToolStripItem) this.Quit
      });
      this.contextMenuStrip1.Name = "contextMenuStrip1";
      this.contextMenuStrip1.ShowImageMargin = false;
      this.contextMenuStrip1.Size = new Size(128, 148);
      this.contextMenuStrip1.Opening += new CancelEventHandler(this.contextMenuStrip1_Opening);
      this.Launch.DropDownItems.AddRange(new ToolStripItem[9]
      {
        (ToolStripItem) this.EffectEditorMenu,
        (ToolStripItem) this.ItemEditorMenu,
        (ToolStripItem) this.MissionEditorMenu,
        (ToolStripItem) this.MobEditorMenu,
        (ToolStripItem) this.SectorEditorMenu,
        (ToolStripItem) this.StationEditorMenu,
        (ToolStripItem) this.FactionEditorMenu,
        (ToolStripItem) this.toolStripSeparator1,
        (ToolStripItem) this.LaunchN7Menu
      });
      this.Launch.Name = "Launch";
      this.Launch.Size = new Size((int) sbyte.MaxValue, 22);
      this.Launch.Text = "Launch";
      this.EffectEditorMenu.Image = (Image) componentResourceManager.GetObject("EffectEditorMenu.Image");
      this.EffectEditorMenu.Name = "EffectEditorMenu";
      this.EffectEditorMenu.Size = new Size(152, 22);
      this.EffectEditorMenu.Text = "Effect Editor";
      this.EffectEditorMenu.Click += new EventHandler(this.ClickIcon);
      this.ItemEditorMenu.Image = (Image) componentResourceManager.GetObject("ItemEditorMenu.Image");
      this.ItemEditorMenu.Name = "ItemEditorMenu";
      this.ItemEditorMenu.Size = new Size(152, 22);
      this.ItemEditorMenu.Text = "Item Editor";
      this.ItemEditorMenu.Click += new EventHandler(this.ClickIcon);
      this.MissionEditorMenu.Image = (Image) componentResourceManager.GetObject("MissionEditorMenu.Image");
      this.MissionEditorMenu.Name = "MissionEditorMenu";
      this.MissionEditorMenu.Size = new Size(152, 22);
      this.MissionEditorMenu.Text = "Mission Editor";
      this.MissionEditorMenu.Click += new EventHandler(this.ClickIcon);
      this.MobEditorMenu.Image = (Image) componentResourceManager.GetObject("MobEditorMenu.Image");
      this.MobEditorMenu.Name = "MobEditorMenu";
      this.MobEditorMenu.Size = new Size(152, 22);
      this.MobEditorMenu.Text = "Mob Editor";
      this.MobEditorMenu.Click += new EventHandler(this.ClickIcon);
      this.SectorEditorMenu.Image = (Image) componentResourceManager.GetObject("SectorEditorMenu.Image");
      this.SectorEditorMenu.Name = "SectorEditorMenu";
      this.SectorEditorMenu.Size = new Size(152, 22);
      this.SectorEditorMenu.Text = "Sector Editor";
      this.SectorEditorMenu.Click += new EventHandler(this.ClickIcon);
      this.StationEditorMenu.Image = (Image) componentResourceManager.GetObject("StationEditorMenu.Image");
      this.StationEditorMenu.Name = "StationEditorMenu";
      this.StationEditorMenu.Size = new Size(152, 22);
      this.StationEditorMenu.Text = "Station Editor";
      this.StationEditorMenu.Click += new EventHandler(this.ClickIcon);
      this.FactionEditorMenu.Name = "FactionEditorMenu";
      this.FactionEditorMenu.Size = new Size(152, 22);
      this.FactionEditorMenu.Text = "Faction Editor";
      this.FactionEditorMenu.Click += new EventHandler(this.ClickIcon);
      this.toolStripSeparator1.Name = "toolStripSeparator1";
      this.toolStripSeparator1.Size = new Size(149, 6);
      this.LaunchN7Menu.Image = (Image) componentResourceManager.GetObject("LaunchN7Menu.Image");
      this.LaunchN7Menu.Name = "LaunchN7Menu";
      this.LaunchN7Menu.Size = new Size(152, 22);
      this.LaunchN7Menu.Text = "Launch net7";
      this.LaunchN7Menu.Click += new EventHandler(this.LaunchN7_Click);
      this.Update.Name = "Update";
      this.Update.Size = new Size((int) sbyte.MaxValue, 22);
      this.Update.Text = "Update";
      this.Update.Click += new EventHandler(this.ClickIcon);
      this.toolStripSeparator3.Name = "toolStripSeparator3";
      this.toolStripSeparator3.Size = new Size(124, 6);
      this.Minimize.Name = "Minimize";
      this.Minimize.Size = new Size((int) sbyte.MaxValue, 22);
      this.Minimize.Text = "Minimize";
      this.Minimize.Click += new EventHandler(this.Minimize_Click);
      this.Maximize.Name = "Maximize";
      this.Maximize.Size = new Size((int) sbyte.MaxValue, 22);
      this.Maximize.Text = "Maximize";
      this.Maximize.Click += new EventHandler(this.Maximize_Click);
      this.toolStripSeparator2.Name = "toolStripSeparator2";
      this.toolStripSeparator2.Size = new Size(124, 6);
      this.Quit.Name = "Quit";
      this.Quit.Size = new Size((int) sbyte.MaxValue, 22);
      this.Quit.Text = "Quit";
      this.Quit.Click += new EventHandler(this.Quit_Click);
      this.notifyIcon1.ContextMenuStrip = this.contextMenuStrip1;
      this.notifyIcon1.Icon = (Icon) componentResourceManager.GetObject("notifyIcon1.Icon");
      this.notifyIcon1.Text = "net7 Tools Launch Pad";
      this.notifyIcon1.Visible = true;
      this.notifyIcon1.DoubleClick += new EventHandler(this.notifyIcon1_DoubleClick_1);
      this.FactionEditor.Font = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Bold | FontStyle.Italic);
      this.FactionEditor.ForeColor = Color.FromArgb(192, 64, 0);
      this.FactionEditor.Image = (Image) componentResourceManager.GetObject("FactionEditor.Image");
      this.FactionEditor.ImageAlign = ContentAlignment.MiddleLeft;
      this.FactionEditor.Location = new Point(12, 308);
      this.FactionEditor.Name = "FactionEditor";
      this.FactionEditor.Size = new Size(227, 40);
      this.FactionEditor.TabIndex = 8;
      this.FactionEditor.Text = "Faction Editor";
      this.FactionEditor.TextAlign = ContentAlignment.MiddleRight;
      this.FactionEditor.UseVisualStyleBackColor = true;
      this.FactionEditor.Click += new EventHandler(this.ClickIcon);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.BackColor = Color.DarkGray;
      this.ClientSize = new Size(250, 388);
      this.Controls.Add((Control) this.FactionEditor);
      this.Controls.Add((Control) this.EffectEdit);
      this.Controls.Add((Control) this.SectorEdit);
      this.Controls.Add((Control) this.ItemEdit);
      this.Controls.Add((Control) this.ToolsPatch);
      this.Controls.Add((Control) this.LaunchN7);
      this.Controls.Add((Control) this.MissionEdit);
      this.Controls.Add((Control) this.MobEdit);
      this.Controls.Add((Control) this.menuStrip1);
      this.Controls.Add((Control) this.StationEdit);
      this.FormBorderStyle = FormBorderStyle.FixedDialog;
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.MaximizeBox = false;
      this.Name = nameof (ToolsLauncher);
      this.SizeGripStyle = SizeGripStyle.Hide;
      this.StartPosition = FormStartPosition.Manual;
      this.Text = "net7 Tools Launch Pad";
      this.Load += new EventHandler(this.ToolsLauncher_Load);
      this.FormClosing += new FormClosingEventHandler(this.ToolsLauncher_FormClosing);
      this.Resize += new EventHandler(this.ToolsLauncher_Resize);
      this.menuStrip1.ResumeLayout(false);
      this.menuStrip1.PerformLayout();
      this.contextMenuStrip1.ResumeLayout(false);
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    private delegate DialogResult DoDisplayUpdateCallback(Updater updater);
  }
}
