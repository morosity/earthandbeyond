﻿// Decompiled with JetBrains decompiler
// Type: ToolsLauncher.IRCMessenger
// Assembly: ToolsLauncher, Version=1.1.0.1, Culture=neutral, PublicKeyToken=null
// MVID: B3444A73-323E-44BC-BC2D-02089BA9FF5C
// Assembly location: D:\Server\eab\Tools\ToolsLauncher.exe

using Meebey.SmartIrc4net;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace ToolsLauncher
{
  public class IRCMessenger : Form
  {
    private string m_IRCChannel = "#test";
    private string m_IRCServer = "eservices.dyndns.org";
    private int m_IRCPort = 6667;
    private bool m_Close = false;
    private TreeNode m_UserTree = new TreeNode();
    private List<PrivateMessage> m_PrivateWindows = new List<PrivateMessage>();
    private List<string> m_OnlineUsers = new List<string>();
    private IContainer components = (IContainer) null;
    private string m_Nick;
    private IrcClient m_IRCClient;
    private Thread m_IrcThread;
    private TreeView UserList;
    private NotifyIcon notifyIcon1;
    private ContextMenuStrip NotifyMenu;
    private ToolStripMenuItem quitToolStripMenuItem;

    public IRCMessenger()
    {
      this.InitializeComponent();
      this.m_Nick = SQLData.User;
    }

    public void ListenThread()
    {
      this.m_IRCClient.Listen();
    }

    private void IRCMessenger_Load(object sender, EventArgs e)
    {
      this.m_IRCClient = new IrcClient();
      this.m_IRCClient.Encoding = Encoding.UTF8;
      this.m_IRCClient.SendDelay = 200;
      this.m_IRCClient.ActiveChannelSyncing = true;
      this.m_IRCClient.AutoRelogin = true;
      this.m_IRCClient.AutoRejoin = true;
      this.m_IRCClient.AutoReconnect = true;
      this.m_IRCClient.AutoRejoinOnKick = true;
      this.m_IRCClient.AutoRetry = true;
      this.m_IRCClient.OnConnected += new EventHandler(this.ircClient_OnConnect);
      this.m_IRCClient.OnJoin += new JoinEventHandler(this.ircClient_OnJoin);
      this.m_IRCClient.OnNames += new NamesEventHandler(this.ircClient_OnNames);
      this.m_IRCClient.OnPart += new PartEventHandler(this.ircClient_OnPart);
      this.m_IRCClient.OnQuit += new QuitEventHandler(this.ircClient_OnQuit);
      this.m_IRCClient.OnKick += new KickEventHandler(this.ircClient_OnKick);
      this.m_IRCClient.OnQueryMessage += new IrcEventHandler(this.ircClient_OnPrivMsg);
      this.m_IRCClient.OnErrorMessage += new IrcEventHandler(this.ircClient_OnError);
      this.m_IRCClient.Connect(this.m_IRCServer, this.m_IRCPort);
      this.m_IRCClient.Login(this.m_Nick, "SmartIrc4net Test Bot");
      this.m_IrcThread = new Thread(new ThreadStart(this.ListenThread));
      this.m_IrcThread.Start();
    }

    private PrivateMessage FindWindow(string NickName)
    {
      foreach (PrivateMessage privateWindow in this.m_PrivateWindows)
      {
        if (privateWindow.m_PrivateNick == NickName)
          return privateWindow;
      }
      return (PrivateMessage) null;
    }

    public void SendMsg(string PrivateNick, string Text)
    {
      string str = Text;
      char[] chArray = new char[1]{ '\n' };
      foreach (string message in str.Split(chArray))
        this.m_IRCClient.RfcPrivmsg(PrivateNick, message);
    }

    private void SendWindowMsg(IrcEventArgs e)
    {
      PrivateMessage privateMessage = this.FindWindow(e.Data.Nick);
      if (privateMessage != null && privateMessage.m_Closing)
      {
        privateMessage.Close();
        this.m_PrivateWindows.Remove(privateMessage);
        privateMessage = (PrivateMessage) null;
      }
      if (privateMessage == null)
      {
        privateMessage = new PrivateMessage();
        privateMessage.Show();
        privateMessage.SetParent(this);
        privateMessage.m_PrivateNick = e.Data.Nick;
        privateMessage.m_MyNick = this.m_Nick;
        privateMessage.Text = e.Data.Nick + " - EnB Messager";
        this.m_PrivateWindows.Add(privateMessage);
      }
      if (privateMessage == null || e.Data.Message == null)
        return;
      privateMessage.ReciveMessage(e.Data.Message);
    }

    private void ircClient_OnError(object sender, IrcEventArgs e)
    {
      if (!(e.Data.Message == "11"))
        return;
      int num = (int) MessageBox.Show("Player is not online anymore", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
    }

    private void ircClient_OnPrivMsg(object sender, IrcEventArgs e)
    {
      this.BeginInvoke((Delegate) new IRCMessenger.ActionDelegateIrcEvent(this.SendWindowMsg), (object) e);
    }

    private void ircClient_OnNames(object sender, NamesEventArgs e)
    {
      Channel channel = this.m_IRCClient.GetChannel(this.m_IRCChannel);
      this.m_UserTree.Nodes.Clear();
      foreach (DictionaryEntry user in channel.Users)
      {
        ChannelUser channelUser = (ChannelUser) user.Value;
        if (channelUser.Nick != this.m_Nick)
          this.m_UserTree.Nodes.Add(new TreeNode()
          {
            Text = channelUser.Nick
          });
      }
      this.BeginInvoke((Delegate) new IRCMessenger.ActionDelegateNoParams(this.UpdateUserList), (object[]) null);
    }

    private void ircClient_OnConnect(object sender, EventArgs e)
    {
      this.m_IRCClient.RfcJoin(this.m_IRCChannel);
    }

    private void ircClient_OnJoin(object sender, JoinEventArgs e)
    {
      if (!(e.Data.Nick != this.m_Nick))
        return;
      this.m_IRCClient.RfcNames(e.Data.Channel);
      Console.WriteLine(e.Data.Nick + " Came Online");
    }

    private void ircClient_OnPart(object sender, PartEventArgs e)
    {
      if (!(e.Data.Nick != this.m_Nick))
        return;
      this.m_IRCClient.RfcNames(e.Data.Channel);
      Console.WriteLine(e.Data.Nick + " went Offline");
    }

    private void ircClient_OnQuit(object sender, QuitEventArgs e)
    {
      if (!(e.Data.Nick != this.m_Nick))
        return;
      this.m_IRCClient.RfcNames(this.m_IRCChannel);
      Console.WriteLine(e.Data.Nick + " went Offline");
    }

    private void ircClient_OnKick(object sender, KickEventArgs e)
    {
      if (!(e.Data.Nick != this.m_Nick))
        return;
      this.m_IRCClient.RfcNames(e.Data.Channel);
      Console.WriteLine(e.Data.Nick + " went Offline");
    }

    private void OnRawMessage(object sender, IrcEventArgs e)
    {
      Console.WriteLine("Received: " + e.Data.RawMessage);
    }

    private void UpdateUserList()
    {
      TreeNode[] treeNodeArray = this.UserList.Nodes.Find("Online", false);
      treeNodeArray[0].Nodes.Clear();
      foreach (TreeNode node in this.m_UserTree.Nodes)
        treeNodeArray[0].Nodes.Add((TreeNode) node.Clone());
      treeNodeArray[0].Expand();
    }

    private void UserList_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
    {
      IrcEventArgs e1 = new IrcEventArgs(new IrcMessageData((IrcClient) null, (string) null, e.Node.Text, (string) null, (string) null, (string) null, (string) null, " ", ReceiveType.Info, ReplyCode.Null));
      if (!(e.Node.Name != "Online"))
        return;
      this.SendWindowMsg(e1);
    }

    private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
    {
      this.Show();
    }

    private void IRCMessenger_FormClosing(object sender, FormClosingEventArgs e)
    {
      if (this.m_Close)
      {
        this.m_IRCClient.RfcQuit("Sign Off");
        this.m_IRCClient.Disconnect();
        this.m_IrcThread.Abort();
        foreach (Form privateWindow in this.m_PrivateWindows)
          privateWindow.Close();
        this.notifyIcon1.Visible = false;
      }
      else
      {
        e.Cancel = true;
        this.Hide();
      }
    }

    public void CloseWindow()
    {
      this.m_Close = true;
      this.Close();
    }

    private void quitToolStripMenuItem_Click(object sender, EventArgs e)
    {
      this.CloseWindow();
    }

    private void notifyIcon1_DoubleClick(object sender, EventArgs e)
    {
      this.Show();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.components = (IContainer) new Container();
      TreeNode treeNode = new TreeNode("Online");
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (IRCMessenger));
      this.UserList = new TreeView();
      this.notifyIcon1 = new NotifyIcon(this.components);
      this.NotifyMenu = new ContextMenuStrip(this.components);
      this.quitToolStripMenuItem = new ToolStripMenuItem();
      this.NotifyMenu.SuspendLayout();
      this.SuspendLayout();
      this.UserList.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.UserList.Location = new Point(-1, 0);
      this.UserList.Name = "UserList";
      treeNode.Name = "Online";
      treeNode.Text = "Online";
      this.UserList.Nodes.AddRange(new TreeNode[1]
      {
        treeNode
      });
      this.UserList.ShowLines = false;
      this.UserList.Size = new Size(203, 346);
      this.UserList.TabIndex = 0;
      this.UserList.NodeMouseDoubleClick += new TreeNodeMouseClickEventHandler(this.UserList_NodeMouseDoubleClick);
      this.notifyIcon1.BalloonTipIcon = ToolTipIcon.Info;
      this.notifyIcon1.BalloonTipText = nameof (IRCMessenger);
      this.notifyIcon1.BalloonTipTitle = "Tools Launcher";
      this.notifyIcon1.ContextMenuStrip = this.NotifyMenu;
      this.notifyIcon1.Icon = (Icon) componentResourceManager.GetObject("notifyIcon1.Icon");
      this.notifyIcon1.Text = "Messenger";
      this.notifyIcon1.Visible = true;
      this.notifyIcon1.DoubleClick += new EventHandler(this.notifyIcon1_DoubleClick);
      this.notifyIcon1.MouseDoubleClick += new MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
      this.NotifyMenu.Items.AddRange(new ToolStripItem[1]
      {
        (ToolStripItem) this.quitToolStripMenuItem
      });
      this.NotifyMenu.Name = "NotifyMenu";
      this.NotifyMenu.Size = new Size(98, 26);
      this.quitToolStripMenuItem.Name = "quitToolStripMenuItem";
      this.quitToolStripMenuItem.Size = new Size(97, 22);
      this.quitToolStripMenuItem.Text = "&Quit";
      this.quitToolStripMenuItem.Click += new EventHandler(this.quitToolStripMenuItem_Click);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(202, 345);
      this.Controls.Add((Control) this.UserList);
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.Name = nameof (IRCMessenger);
      this.Text = nameof (IRCMessenger);
      this.Load += new EventHandler(this.IRCMessenger_Load);
      this.FormClosing += new FormClosingEventHandler(this.IRCMessenger_FormClosing);
      this.NotifyMenu.ResumeLayout(false);
      this.ResumeLayout(false);
    }

    public delegate void ActionDelegateNoParams();

    public delegate void ActionDelegateIrcEvent(IrcEventArgs e);
  }
}
