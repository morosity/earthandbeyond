﻿// Decompiled with JetBrains decompiler
// Type: ToolsLauncher.FtpWindow
// Assembly: ToolsLauncher, Version=1.1.0.1, Culture=neutral, PublicKeyToken=null
// MVID: B3444A73-323E-44BC-BC2D-02089BA9FF5C
// Assembly location: D:\Server\eab\Tools\ToolsLauncher.exe

using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace ToolsLauncher
{
  public class FtpWindow : Form
  {
    private IContainer components = (IContainer) null;
    public bool m_WindowDead = false;
    private WebBrowser FtpBrowser;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.FtpBrowser = new WebBrowser();
      this.SuspendLayout();
      this.FtpBrowser.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.FtpBrowser.Location = new Point(2, 1);
      this.FtpBrowser.MinimumSize = new Size(20, 20);
      this.FtpBrowser.Name = "FtpBrowser";
      this.FtpBrowser.Size = new Size(542, 260);
      this.FtpBrowser.TabIndex = 0;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(544, 260);
      this.Controls.Add((Control) this.FtpBrowser);
      this.Name = nameof (FtpWindow);
      this.Text = nameof (FtpWindow);
      this.FormClosed += new FormClosedEventHandler(this.FtpWindow_FormClosed);
      this.ResumeLayout(false);
    }

    public FtpWindow(string FTPUrl, string Name)
    {
      this.InitializeComponent();
      this.Text = Name;
      this.FtpBrowser.Navigate(FTPUrl);
      this.Show();
    }

    private void FtpWindow_FormClosed(object sender, FormClosedEventArgs e)
    {
      this.m_WindowDead = true;
    }
  }
}
