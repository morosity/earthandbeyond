﻿// Decompiled with JetBrains decompiler
// Type: ToolsLauncher.SettingsFrm
// Assembly: ToolsLauncher, Version=1.1.0.1, Culture=neutral, PublicKeyToken=null
// MVID: B3444A73-323E-44BC-BC2D-02089BA9FF5C
// Assembly location: D:\Server\eab\Tools\ToolsLauncher.exe

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using ToolsLauncher.Properties;

namespace ToolsLauncher
{
  public class SettingsFrm : Form
  {
    private IContainer components = (IContainer) null;
    private TableLayoutPanel tableLayoutPanel1;
    private Button button1;
    private GroupBox groupBox1;
    private Label label1;
    private Button button2;
    private TextBox textBox1;
    private FolderBrowserDialog folderBrowserDialog1;

    public SettingsFrm()
    {
      this.InitializeComponent();
    }

    private void Settings_Load(object sender, EventArgs e)
    {
      this.textBox1.Text = Settings.Default.LaunchNet7Path;
    }

    private void button1_Click(object sender, EventArgs e)
    {
      Settings.Default.Save();
      this.Close();
    }

    private void button2_Click(object sender, EventArgs e)
    {
      if (this.folderBrowserDialog1.ShowDialog() != DialogResult.OK)
        return;
      this.textBox1.Text = this.folderBrowserDialog1.SelectedPath;
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (SettingsFrm));
      this.tableLayoutPanel1 = new TableLayoutPanel();
      this.button1 = new Button();
      this.groupBox1 = new GroupBox();
      this.button2 = new Button();
      this.textBox1 = new TextBox();
      this.label1 = new Label();
      this.folderBrowserDialog1 = new FolderBrowserDialog();
      this.tableLayoutPanel1.SuspendLayout();
      this.groupBox1.SuspendLayout();
      this.SuspendLayout();
      this.tableLayoutPanel1.ColumnCount = 3;
      this.tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100f));
      this.tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle());
      this.tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle());
      this.tableLayoutPanel1.Controls.Add((Control) this.button1, 2, 1);
      this.tableLayoutPanel1.Controls.Add((Control) this.groupBox1, 0, 0);
      this.tableLayoutPanel1.Dock = DockStyle.Fill;
      this.tableLayoutPanel1.Location = new Point(0, 0);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 2;
      this.tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 100f));
      this.tableLayoutPanel1.RowStyles.Add(new RowStyle());
      this.tableLayoutPanel1.Size = new Size(437, 102);
      this.tableLayoutPanel1.TabIndex = 0;
      this.button1.Location = new Point(359, 76);
      this.button1.Name = "button1";
      this.button1.Size = new Size(75, 23);
      this.button1.TabIndex = 0;
      this.button1.Text = "Finish";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new EventHandler(this.button1_Click);
      this.tableLayoutPanel1.SetColumnSpan((Control) this.groupBox1, 3);
      this.groupBox1.Controls.Add((Control) this.button2);
      this.groupBox1.Controls.Add((Control) this.textBox1);
      this.groupBox1.Controls.Add((Control) this.label1);
      this.groupBox1.Dock = DockStyle.Fill;
      this.groupBox1.Location = new Point(3, 3);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new Size(431, 67);
      this.groupBox1.TabIndex = 2;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "General Settings";
      this.button2.Location = new Point(352, 23);
      this.button2.Name = "button2";
      this.button2.Size = new Size(72, 23);
      this.button2.TabIndex = 2;
      this.button2.Text = "Browse";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new EventHandler(this.button2_Click);
      this.textBox1.Location = new Point(112, 25);
      this.textBox1.Name = "textBox1";
      this.textBox1.Size = new Size(235, 20);
      this.textBox1.TabIndex = 1;
      this.label1.AutoSize = true;
      this.label1.Location = new Point(9, 28);
      this.label1.Name = "label1";
      this.label1.Size = new Size(97, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Launch net7 Path:";
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(437, 102);
      this.Controls.Add((Control) this.tableLayoutPanel1);
      this.FormBorderStyle = FormBorderStyle.FixedToolWindow;
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.Name = nameof (SettingsFrm);
      this.StartPosition = FormStartPosition.CenterScreen;
      this.Text = "Settings";
      this.Load += new EventHandler(this.Settings_Load);
      this.tableLayoutPanel1.ResumeLayout(false);
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.ResumeLayout(false);
    }
  }
}
