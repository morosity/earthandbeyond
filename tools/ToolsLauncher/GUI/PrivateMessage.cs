﻿// Decompiled with JetBrains decompiler
// Type: ToolsLauncher.PrivateMessage
// Assembly: ToolsLauncher, Version=1.1.0.1, Culture=neutral, PublicKeyToken=null
// MVID: B3444A73-323E-44BC-BC2D-02089BA9FF5C
// Assembly location: D:\Server\eab\Tools\ToolsLauncher.exe

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Windows.Forms;

namespace ToolsLauncher
{
  public class PrivateMessage : Form
  {
    private IContainer components = (IContainer) null;
    public Process p = new Process();
    public bool m_Closing = false;
    private IRCMessenger m_ParentMsg = (IRCMessenger) null;
    private const int WM_SCROLL = 276;
    private const int WM_VSCROLL = 277;
    private const int SB_LINEUP = 0;
    private const int SB_LINELEFT = 0;
    private const int SB_LINEDOWN = 1;
    private const int SB_LINERIGHT = 1;
    private const int SB_PAGEUP = 2;
    private const int SB_PAGELEFT = 2;
    private const int SB_PAGEDOWN = 3;
    private const int SB_PAGERIGTH = 3;
    private const int SB_PAGETOP = 6;
    private const int SB_LEFT = 6;
    private const int SB_PAGEBOTTOM = 7;
    private const int SB_RIGHT = 7;
    private const int SB_ENDSCROLL = 8;
    private RichTextBox ReciveBox;
    private RichTextBox SendBox;
    private SplitContainer splitContainer1;
    private Button button1;
    public string m_PrivateNick;
    public string m_MyNick;
    public string m_RtfData;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (PrivateMessage));
      this.ReciveBox = new RichTextBox();
      this.SendBox = new RichTextBox();
      this.splitContainer1 = new SplitContainer();
      this.button1 = new Button();
      this.splitContainer1.Panel1.SuspendLayout();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      this.SuspendLayout();
      this.ReciveBox.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.ReciveBox.BackColor = SystemColors.ControlLightLight;
      this.ReciveBox.Location = new Point(0, 0);
      this.ReciveBox.Name = "ReciveBox";
      this.ReciveBox.ReadOnly = true;
      this.ReciveBox.Size = new Size(369, 180);
      this.ReciveBox.TabIndex = 2;
      this.ReciveBox.Text = "";
      this.ReciveBox.LinkClicked += new LinkClickedEventHandler(this.ReciveBox_LinkClicked);
      this.SendBox.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.SendBox.Location = new Point(0, 3);
      this.SendBox.Name = "SendBox";
      this.SendBox.Size = new Size(369, 109);
      this.SendBox.TabIndex = 1;
      this.SendBox.Text = "";
      this.SendBox.KeyPress += new KeyPressEventHandler(this.SendBox_KeyPress);
      this.splitContainer1.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.splitContainer1.Location = new Point(2, 3);
      this.splitContainer1.Name = "splitContainer1";
      this.splitContainer1.Orientation = Orientation.Horizontal;
      this.splitContainer1.Panel1.Controls.Add((Control) this.ReciveBox);
      this.splitContainer1.Panel2.Controls.Add((Control) this.SendBox);
      this.splitContainer1.Size = new Size(372, 298);
      this.splitContainer1.SplitterDistance = 179;
      this.splitContainer1.TabIndex = 2;
      this.button1.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.button1.Location = new Point(372, 225);
      this.button1.Name = "button1";
      this.button1.Size = new Size(77, 76);
      this.button1.TabIndex = 3;
      this.button1.Text = "Send";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new EventHandler(this.button1_Click);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(450, 302);
      this.Controls.Add((Control) this.button1);
      this.Controls.Add((Control) this.splitContainer1);
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.Name = nameof (PrivateMessage);
      this.Text = nameof (PrivateMessage);
      this.Load += new EventHandler(this.PrivateMessage_Load);
      this.Activated += new EventHandler(this.PrivateMessage_Activated);
      this.FormClosing += new FormClosingEventHandler(this.PrivateMessage_FormClosing);
      this.splitContainer1.Panel1.ResumeLayout(false);
      this.splitContainer1.Panel2.ResumeLayout(false);
      this.splitContainer1.ResumeLayout(false);
      this.ResumeLayout(false);
    }

    public PrivateMessage()
    {
      this.InitializeComponent();
    }

    private void SendMessage()
    {
      this.m_RtfData = this.m_RtfData.Insert(this.m_RtfData.Length - 1, "\\cf3\\b " + this.m_MyNick + ":\\b0\\cf1  " + this.SendBox.Text + "\\par");
      this.ReciveBox.Rtf = this.m_RtfData;
      this.m_ParentMsg.SendMsg(this.m_PrivateNick, this.SendBox.Text);
      this.SendBox.Clear();
      this.ReciveBox.Focus();
      this.ReciveBox.Select();
      this.ReciveBox.SelectionStart = this.ReciveBox.Rtf.Length;
      this.SendBox.Focus();
    }

    public void SetParent(IRCMessenger Parnt)
    {
      this.m_ParentMsg = Parnt;
    }

    public void ReciveMessage(string msg)
    {
      this.m_RtfData = this.m_RtfData.Insert(this.m_RtfData.Length - 1, "\\cf2\\b " + this.m_PrivateNick + ":\\b0\\cf1  " + msg + "\\par");
      this.ReciveBox.Rtf = this.m_RtfData;
      this.ReciveBox.Select();
      this.ReciveBox.SelectionStart = this.ReciveBox.Rtf.Length;
      FlashWindow.SendMessage(this.ReciveBox.Handle, 277, (IntPtr) 3, IntPtr.Zero);
      if (FlashWindow.GetForegroundWindow() != this.Handle)
        FlashWindow.Flash((Form) this, 3U);
      else
        this.SendBox.Focus();
    }

    private void button1_Click(object sender, EventArgs e)
    {
      if (this.SendBox.Text != "\n")
        this.SendMessage();
      else
        this.SendBox.Clear();
    }

    private void PrivateMessage_FormClosing(object sender, FormClosingEventArgs e)
    {
      this.m_Closing = true;
    }

    private void SendBox_KeyPress(object sender, KeyPressEventArgs e)
    {
      if (this.SendBox.Text != "\n")
      {
        if (e.KeyChar != '\r')
          return;
        this.SendBox.Text = this.SendBox.Text.TrimEnd('\n');
        this.SendMessage();
      }
      else
        this.SendBox.Clear();
    }

    private void PrivateMessage_Load(object sender, EventArgs e)
    {
      this.m_RtfData = "{\\rtf1\\ansi\\ansicpg1252\\deff0\\deflang1033{\\fonttbl{\\f0\\fswiss\\fcharset0 Arial;}}{\\colortbl;\\red0\\green0\\blue0;\\red255\\green0\\blue0;\\red0\\green0\\blue255;}\\fs18}";
      this.ReciveBox.Rtf = this.m_RtfData;
    }

    private void ReciveBox_LinkClicked(object sender, LinkClickedEventArgs e)
    {
      this.p = Process.Start(e.LinkText);
    }

    private void PrivateMessage_Activated(object sender, EventArgs e)
    {
      this.SendBox.Focus();
    }
  }
}
