﻿// Decompiled with JetBrains decompiler
// Type: ToolsLauncher.Program
// Assembly: ToolsLauncher, Version=1.1.0.1, Culture=neutral, PublicKeyToken=null
// MVID: B3444A73-323E-44BC-BC2D-02089BA9FF5C
// Assembly location: D:\Server\eab\Tools\ToolsLauncher.exe

using System;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace ToolsLauncher
{
  internal static class Program
  {
    [STAThread]
    private static void Main()
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      try
      {
        DataSet dataSet = new DataSet();
        int num = (int) dataSet.ReadXml(Application.StartupPath + "\\Config.xml");
        DataRow row = dataSet.Tables[0].Rows[0];
        SQLData.Host = row.ItemArray[0].ToString();
        SQLData.Port = int.Parse(row.ItemArray[1].ToString());
        SQLData.User = row.ItemArray[2].ToString();
        SQLData.Pass = row.ItemArray[3].ToString();
      }
      catch
      {
        SQLData.Host = "";
        SQLData.Port = 0;
        SQLData.User = "";
        SQLData.Pass = "";
      }
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      Login login = new Login();
      login.LoginUsername.Text = SQLData.User;
      login.LoginPassword.Text = SQLData.Pass;
      login.SQLServer.Text = SQLData.Host;
      login.SQLPort.Text = SQLData.Port.ToString();
      int num1 = (int) login.ShowDialog();
      if (!login.m_Cancel && login.m_HasChanged)
      {
        SQLData.Host = login.SQLServer.Text;
        SQLData.Port = Convert.ToInt32(login.SQLPort.Text, 10);
        XmlTextWriter xmlTextWriter = new XmlTextWriter(Application.StartupPath + "\\Config.xml", (Encoding) null);
        xmlTextWriter.Formatting = Formatting.Indented;
        xmlTextWriter.WriteStartDocument();
        xmlTextWriter.WriteStartElement("Confg");
        xmlTextWriter.WriteStartElement("Host");
        xmlTextWriter.WriteString(SQLData.Host.ToString());
        xmlTextWriter.WriteEndElement();
        xmlTextWriter.WriteStartElement("Port");
        xmlTextWriter.WriteString(SQLData.Port.ToString());
        xmlTextWriter.WriteEndElement();
        xmlTextWriter.WriteStartElement("User");
        xmlTextWriter.WriteString(login.LoginUsername.Text);
        xmlTextWriter.WriteEndElement();
        xmlTextWriter.WriteStartElement("Pass");
        xmlTextWriter.WriteString(login.LoginPassword.Text);
        xmlTextWriter.WriteEndElement();
        xmlTextWriter.WriteEndElement();
        xmlTextWriter.WriteEndDocument();
        xmlTextWriter.Close();
      }
      login.Close();
      if (login.m_Cancel)
        return;
      Application.Run((Form) new ToolsLauncher.ToolsLauncher());
    }

    public static string Title
    {
      get
      {
        return "LaunchNet7";
      }
    }

    public static string TitleForErrorMessages
    {
      get
      {
        return Program.Title + " - Error";
      }
    }

    public static string TitleForWarningMessages
    {
      get
      {
        return Program.Title + " - Warning";
      }
    }

    public static string TitleForInformationMessages
    {
      get
      {
        return Program.Title + " - Information";
      }
    }

    internal static void LogException(Exception e)
    {
    }
  }
}
