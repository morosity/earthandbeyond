﻿// Decompiled with JetBrains decompiler
// Type: ToolsLauncher.Utility
// Assembly: ToolsLauncher, Version=1.1.0.1, Culture=neutral, PublicKeyToken=null
// MVID: B3444A73-323E-44BC-BC2D-02089BA9FF5C
// Assembly location: D:\Server\eab\Tools\ToolsLauncher.exe

namespace ToolsLauncher
{
  internal static class Utility
  {
    public static string FormDescription(string Src)
    {
      return Src.Replace("\r", "").Replace("\n", "\\n");
    }

    public static string ParseDescription(string Src)
    {
      return Src.Replace("\\n", "\r\n");
    }

    public static string FormParen(int Data, int Width)
    {
      return Utility.FormParen(Data.ToString(), Width);
    }

    public static string FormParen(string Data, int Width)
    {
      if (Data.Length > Width)
        return "Invalid";
      string str = "(";
      for (int index = 0; index < Width - Data.ToString().Length; ++index)
        str += "0";
      return str + Data + ")";
    }

    public static int ParseParen(string Src)
    {
      int startIndex = Src.IndexOf('(');
      if (startIndex == -1)
        return -1;
      int num = Src.IndexOf(')', startIndex);
      if (num == -1)
        return -1;
      string s = Src.Substring(startIndex + 1, num - startIndex - 1);
      try
      {
        return int.Parse(s);
      }
      catch
      {
        return -1;
      }
    }
  }
}
