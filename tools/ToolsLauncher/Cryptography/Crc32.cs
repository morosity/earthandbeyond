﻿// Decompiled with JetBrains decompiler
// Type: ToolsLauncher.Cryptography.Crc32
// Assembly: ToolsLauncher, Version=1.1.0.1, Culture=neutral, PublicKeyToken=null
// MVID: B3444A73-323E-44BC-BC2D-02089BA9FF5C
// Assembly location: D:\Server\eab\Tools\ToolsLauncher.exe

using System;
using System.IO;

namespace ToolsLauncher.Cryptography
{
  public static class Crc32
  {
    public static uint[] CreateTable(uint polynomial)
    {
      uint[] numArray = new uint[256];
      for (int index1 = 0; index1 < 256; ++index1)
      {
        uint num = (uint) index1;
        for (int index2 = 0; index2 < 8; ++index2)
        {
          if (((int) num & 1) == 1)
            num = num >> 1 ^ polynomial;
          else
            num >>= 1;
        }
        numArray[index1] = num;
      }
      return numArray;
    }

    public static uint Process(byte[] bytes)
    {
      Crc32Processor crc32Processor = new Crc32Processor();
      crc32Processor.Process(bytes);
      return crc32Processor.Current;
    }

    public static uint Process(byte[] buffer, int startIndex, int length)
    {
      Crc32Processor crc32Processor = new Crc32Processor();
      crc32Processor.Process(buffer, startIndex, length);
      return crc32Processor.Current;
    }

    public static uint ProcessStream(Stream stream)
    {
      if (stream == null)
        throw new ArgumentOutOfRangeException(nameof (stream));
      return Crc32.InternalProcessStream(stream, new long?(), new int?());
    }

    public static uint ProcessStream(Stream stream, int bufferSize)
    {
      if (stream == null)
        throw new ArgumentOutOfRangeException(nameof (stream));
      if (bufferSize <= 0)
        throw new ArgumentOutOfRangeException(nameof (bufferSize));
      return Crc32.InternalProcessStream(stream, new long?(), new int?(bufferSize));
    }

    public static uint ProcessStream(Stream stream, long maxLength)
    {
      if (stream == null)
        throw new ArgumentOutOfRangeException(nameof (stream));
      if (maxLength < 0L)
        throw new ArgumentOutOfRangeException(nameof (maxLength));
      return Crc32.InternalProcessStream(stream, new long?(maxLength), new int?());
    }

    public static uint ProcessStream(Stream stream, long maxLength, int bufferSize)
    {
      if (stream == null)
        throw new ArgumentOutOfRangeException(nameof (stream));
      if (bufferSize <= 0)
        throw new ArgumentOutOfRangeException(nameof (bufferSize));
      if (maxLength < 0L)
        throw new ArgumentOutOfRangeException(nameof (maxLength));
      return Crc32.InternalProcessStream(stream, new long?(maxLength), new int?(bufferSize));
    }

    private static uint InternalProcessStream(Stream stream, long? maxLength, int? bufferSize)
    {
      Crc32Processor crc32Processor = new Crc32Processor();
      byte[] buffer = new byte[bufferSize.GetValueOrDefault(4096)];
      long num = 0;
      while (true)
      {
        int length = stream.Read(buffer, 0, maxLength.HasValue ? (int) Math.Min((long) buffer.Length, maxLength.Value - num) : buffer.Length);
        crc32Processor.Process(buffer, 0, length);
        if (maxLength.HasValue)
        {
          num += (long) length;
          if (num >= maxLength.Value)
            break;
        }
        else if (length == 0)
          break;
      }
      return crc32Processor.Current;
    }
  }
}
