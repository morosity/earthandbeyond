﻿// Decompiled with JetBrains decompiler
// Type: ToolsLauncher.FlashWindow
// Assembly: ToolsLauncher, Version=1.1.0.1, Culture=neutral, PublicKeyToken=null
// MVID: B3444A73-323E-44BC-BC2D-02089BA9FF5C
// Assembly location: D:\Server\eab\Tools\ToolsLauncher.exe

using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace ToolsLauncher
{
  public static class FlashWindow
  {
    public const uint FLASHW_STOP = 0;
    public const uint FLASHW_CAPTION = 1;
    public const uint FLASHW_TRAY = 2;
    public const uint FLASHW_ALL = 3;
    public const uint FLASHW_TIMER = 4;
    public const uint FLASHW_TIMERNOFG = 12;

    [DllImport("user32.dll", CharSet = CharSet.Auto)]
    public static extern IntPtr GetForegroundWindow();

    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    private static extern bool FlashWindowEx(ref FlashWindow.FLASHWINFO pwfi);

    [DllImport("user32.dll", CharSet = CharSet.Auto)]
    public static extern int SendMessage(IntPtr hWnd, int wMsg, IntPtr wParam, IntPtr lParam);

    public static bool Flash(Form form)
    {
      if (!FlashWindow.Win2000OrLater)
        return false;
      FlashWindow.FLASHWINFO flashwinfo = FlashWindow.Create_FLASHWINFO(form.Handle, 15U, uint.MaxValue, 0U);
      return FlashWindow.FlashWindowEx(ref flashwinfo);
    }

    private static FlashWindow.FLASHWINFO Create_FLASHWINFO(IntPtr handle, uint flags, uint count, uint timeout)
    {
      FlashWindow.FLASHWINFO flashwinfo = new FlashWindow.FLASHWINFO();
      flashwinfo.cbSize = Convert.ToUInt32(Marshal.SizeOf((object) flashwinfo));
      flashwinfo.hwnd = handle;
      flashwinfo.dwFlags = flags;
      flashwinfo.uCount = count;
      flashwinfo.dwTimeout = timeout;
      return flashwinfo;
    }

    public static bool Flash(Form form, uint count)
    {
      if (!FlashWindow.Win2000OrLater)
        return false;
      FlashWindow.FLASHWINFO flashwinfo = FlashWindow.Create_FLASHWINFO(form.Handle, 3U, count, 0U);
      return FlashWindow.FlashWindowEx(ref flashwinfo);
    }

    public static bool Start(Form form)
    {
      if (!FlashWindow.Win2000OrLater)
        return false;
      FlashWindow.FLASHWINFO flashwinfo = FlashWindow.Create_FLASHWINFO(form.Handle, 3U, uint.MaxValue, 0U);
      return FlashWindow.FlashWindowEx(ref flashwinfo);
    }

    public static bool Stop(Form form)
    {
      if (!FlashWindow.Win2000OrLater)
        return false;
      FlashWindow.FLASHWINFO flashwinfo = FlashWindow.Create_FLASHWINFO(form.Handle, 0U, uint.MaxValue, 0U);
      return FlashWindow.FlashWindowEx(ref flashwinfo);
    }

    private static bool Win2000OrLater
    {
      get
      {
        return Environment.OSVersion.Version.Major >= 5;
      }
    }

    private struct FLASHWINFO
    {
      public uint cbSize;
      public IntPtr hwnd;
      public uint dwFlags;
      public uint uCount;
      public uint dwTimeout;
    }
  }
}
