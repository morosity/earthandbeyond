﻿// Decompiled with JetBrains decompiler
// Type: ToolsLauncher.Progress
// Assembly: ToolsLauncher, Version=1.1.0.1, Culture=neutral, PublicKeyToken=null
// MVID: B3444A73-323E-44BC-BC2D-02089BA9FF5C
// Assembly location: D:\Server\eab\Tools\ToolsLauncher.exe

namespace ToolsLauncher
{
  public class Progress
  {
    private int m_MinimumValue;
    private int m_MaximumValue;
    private int m_CurrentValue;

    public Progress()
      : this(0, 100)
    {
    }

    public Progress(int minimumValue, int maximumValue)
    {
      this.Reset(minimumValue, maximumValue);
    }

    public int MinimumValue
    {
      get
      {
        return this.m_MinimumValue;
      }
      set
      {
        this.m_MinimumValue = value;
      }
    }

    public int MaximumValue
    {
      get
      {
        return this.m_MaximumValue;
      }
      set
      {
        this.m_MaximumValue = value;
      }
    }

    public int CurrentValue
    {
      get
      {
        return this.m_CurrentValue;
      }
      set
      {
        this.m_CurrentValue = value;
      }
    }

    public void Increment()
    {
      ++this.m_CurrentValue;
    }

    public void Reset()
    {
      this.m_CurrentValue = this.m_MinimumValue;
    }

    public void Reset(int maximumValue)
    {
      this.m_MaximumValue = maximumValue;
      this.m_CurrentValue = this.m_MinimumValue;
    }

    public void Reset(int minimumValue, int maximumValue)
    {
      this.m_MinimumValue = minimumValue;
      this.m_MaximumValue = maximumValue;
      this.m_CurrentValue = this.m_MinimumValue;
    }

    public int ToProgressPercentage()
    {
      if (this.m_CurrentValue == 0 || this.m_CurrentValue < this.m_MinimumValue)
        return 0;
      if (this.m_CurrentValue > this.m_MaximumValue)
        return 100;
      float num = (float) (this.m_MaximumValue - this.m_MinimumValue);
      if ((double) num == 0.0)
        return 0;
      return (int) ((double) this.m_CurrentValue / (double) num * 100.0);
    }
  }
}
