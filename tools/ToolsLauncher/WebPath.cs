﻿// Decompiled with JetBrains decompiler
// Type: ToolsLauncher.WebPath
// Assembly: ToolsLauncher, Version=1.1.0.1, Culture=neutral, PublicKeyToken=null
// MVID: B3444A73-323E-44BC-BC2D-02089BA9FF5C
// Assembly location: D:\Server\eab\Tools\ToolsLauncher.exe

namespace ToolsLauncher
{
  public static class WebPath
  {
    public static string Combine(string url1, string url2)
    {
      if (!url1.EndsWith("/"))
        url1 += (string) (object) '/';
      return url1 + url2;
    }
  }
}
