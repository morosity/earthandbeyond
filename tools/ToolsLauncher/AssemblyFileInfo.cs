﻿// Decompiled with JetBrains decompiler
// Type: ToolsLauncher.AssemblyFileInfo
// Assembly: ToolsLauncher, Version=1.1.0.1, Culture=neutral, PublicKeyToken=null
// MVID: B3444A73-323E-44BC-BC2D-02089BA9FF5C
// Assembly location: D:\Server\eab\Tools\ToolsLauncher.exe

using System;
using System.Reflection;
using System.Runtime.InteropServices;

namespace ToolsLauncher
{
  public class AssemblyFileInfo
  {
    private static AssemblyFileInfo m_Current;
    private Version m_FileVersion;
    private Version m_AssemblyVersion;
    private Version m_InformalVersion;
    private string m_Title;
    private string m_Company;
    private string m_Description;
    private string m_Configuration;
    private string m_Culture;
    private string m_Product;
    private string m_Trademark;
    private string m_Copyright;
    private Guid m_Guid;
    private bool m_IsComVisible;

    public static AssemblyFileInfo FromFile(string assemblyFileName)
    {
      return AssemblyFileInfo.FromAssembly(Assembly.LoadFile(assemblyFileName));
    }

    public static AssemblyFileInfo FromAssembly(Assembly assembly)
    {
      AssemblyFileInfo assemblyFileInfo = new AssemblyFileInfo();
      assemblyFileInfo.m_AssemblyVersion = assembly.GetName().Version;
      foreach (object customAttribute in assembly.GetCustomAttributes(true))
      {
        if (customAttribute is AssemblyFileVersionAttribute)
          assemblyFileInfo.m_FileVersion = new Version(((AssemblyFileVersionAttribute) customAttribute).Version);
        else if (customAttribute is AssemblyTitleAttribute)
          assemblyFileInfo.m_Title = ((AssemblyTitleAttribute) customAttribute).Title;
        else if (customAttribute is AssemblyCompanyAttribute)
          assemblyFileInfo.m_Company = ((AssemblyCompanyAttribute) customAttribute).Company;
        else if (customAttribute is AssemblyDescriptionAttribute)
          assemblyFileInfo.m_Description = ((AssemblyDescriptionAttribute) customAttribute).Description;
        else if (customAttribute is AssemblyConfigurationAttribute)
          assemblyFileInfo.m_Configuration = ((AssemblyConfigurationAttribute) customAttribute).Configuration;
        else if (customAttribute is AssemblyCopyrightAttribute)
          assemblyFileInfo.m_Copyright = ((AssemblyCopyrightAttribute) customAttribute).Copyright;
        else if (customAttribute is AssemblyCultureAttribute)
          assemblyFileInfo.m_Culture = ((AssemblyCultureAttribute) customAttribute).Culture;
        else if (customAttribute is GuidAttribute)
          assemblyFileInfo.m_Guid = new Guid(((GuidAttribute) customAttribute).Value);
        else if (customAttribute is ComVisibleAttribute)
          assemblyFileInfo.m_IsComVisible = ((ComVisibleAttribute) customAttribute).Value;
        else if (customAttribute is AssemblyProductAttribute)
          assemblyFileInfo.m_Product = ((AssemblyProductAttribute) customAttribute).Product;
        else if (customAttribute is AssemblyTrademarkAttribute)
          assemblyFileInfo.m_Trademark = ((AssemblyTrademarkAttribute) customAttribute).Trademark;
        else if (customAttribute is AssemblyInformationalVersionAttribute)
          assemblyFileInfo.m_InformalVersion = new Version(((AssemblyInformationalVersionAttribute) customAttribute).InformationalVersion);
      }
      return assemblyFileInfo;
    }

    public static AssemblyFileInfo Current
    {
      get
      {
        if (AssemblyFileInfo.m_Current == null)
          AssemblyFileInfo.m_Current = AssemblyFileInfo.FromAssembly(Assembly.GetExecutingAssembly());
        return AssemblyFileInfo.m_Current;
      }
    }

    public Version FileVersion
    {
      get
      {
        return this.m_FileVersion;
      }
    }

    public Version AssemblyVersion
    {
      get
      {
        return this.m_AssemblyVersion;
      }
    }

    public Version InformalVersion
    {
      get
      {
        return this.m_InformalVersion;
      }
    }

    public string Title
    {
      get
      {
        return this.m_Title;
      }
    }

    public string Company
    {
      get
      {
        return this.m_Company;
      }
    }

    public string Description
    {
      get
      {
        return this.m_Description;
      }
    }

    public string Configuration
    {
      get
      {
        return this.m_Configuration;
      }
      set
      {
        this.m_Configuration = value;
      }
    }

    public string Culture
    {
      get
      {
        return this.m_Culture;
      }
    }

    public string Product
    {
      get
      {
        return this.m_Product;
      }
    }

    public string Trademark
    {
      get
      {
        return this.m_Trademark;
      }
    }

    public string Copyright
    {
      get
      {
        return this.m_Copyright;
      }
    }

    public Guid Guid
    {
      get
      {
        return this.m_Guid;
      }
      set
      {
        this.m_Guid = value;
      }
    }

    public bool IsComVisible
    {
      get
      {
        return this.m_IsComVisible;
      }
    }
  }
}
