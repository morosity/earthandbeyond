﻿// Decompiled with JetBrains decompiler
// Type: ToolsLauncher.LauncherUtility
// Assembly: ToolsLauncher, Version=1.1.0.1, Culture=neutral, PublicKeyToken=null
// MVID: B3444A73-323E-44BC-BC2D-02089BA9FF5C
// Assembly location: D:\Server\eab\Tools\ToolsLauncher.exe

using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;

namespace ToolsLauncher
{
  public static class LauncherUtility
  {
    [DllImport("kernel32.dll")]
    private static extern int GetShortPathName(string longPath, StringBuilder buffer, int bufferSize);

    public static string GetShortPathName(string path)
    {
      StringBuilder buffer = new StringBuilder(256);
      LauncherUtility.GetShortPathName(path, buffer, buffer.Capacity);
      return buffer.ToString();
    }

    public static string DownloadString(string url)
    {
      return LauncherUtility.DownloadString(url, 1000);
    }

    public static string DownloadString(string url, int timeoutInMilliseconds)
    {
      HttpWebRequest httpWebRequest = (HttpWebRequest) WebRequest.Create(url);
      httpWebRequest.Timeout = timeoutInMilliseconds;
      using (HttpWebResponse response = (HttpWebResponse) httpWebRequest.GetResponse())
        return new StreamReader(response.GetResponseStream()).ReadToEnd();
    }
  }
}
