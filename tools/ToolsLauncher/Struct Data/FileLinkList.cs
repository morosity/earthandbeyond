﻿// Decompiled with JetBrains decompiler
// Type: ToolsLauncher.FileLinkList
// Assembly: ToolsLauncher, Version=1.1.0.1, Culture=neutral, PublicKeyToken=null
// MVID: B3444A73-323E-44BC-BC2D-02089BA9FF5C
// Assembly location: D:\Server\eab\Tools\ToolsLauncher.exe

using System.Windows.Forms;

namespace ToolsLauncher
{
  internal class FileLinkList
  {
    public string ExeFile;
    public Button BObject;
    public ToolStripMenuItem MObject;

    public FileLinkList(string ExeF, Button Object)
    {
      this.ExeFile = ExeF;
      this.BObject = Object;
    }

    public FileLinkList(string ExeF, ToolStripMenuItem Object)
    {
      this.ExeFile = ExeF;
      this.MObject = Object;
    }
  }
}
