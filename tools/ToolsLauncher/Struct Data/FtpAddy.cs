﻿// Decompiled with JetBrains decompiler
// Type: ToolsLauncher.FtpAddy
// Assembly: ToolsLauncher, Version=1.1.0.1, Culture=neutral, PublicKeyToken=null
// MVID: B3444A73-323E-44BC-BC2D-02089BA9FF5C
// Assembly location: D:\Server\eab\Tools\ToolsLauncher.exe

namespace ToolsLauncher
{
  internal class FtpAddy
  {
    public string Location;
    public string Name;

    public FtpAddy(string Loc, string N)
    {
      this.Location = Loc;
      this.Name = N;
    }
  }
}
