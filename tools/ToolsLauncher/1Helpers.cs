﻿// Decompiled with JetBrains decompiler
// Type: ToolsLauncher.SQLData
// Assembly: ToolsLauncher, Version=1.1.0.1, Culture=neutral, PublicKeyToken=null
// MVID: B3444A73-323E-44BC-BC2D-02089BA9FF5C
// Assembly location: D:\Server\eab\Tools\ToolsLauncher.exe

namespace ToolsLauncher
{
  internal static class SQLData
  {
    private static string m_Host;
    private static int m_Port;
    private static string m_User;
    private static string m_Pass;

    public static string Host
    {
      get
      {
        return SQLData.m_Host;
      }
      set
      {
        SQLData.m_Host = value;
      }
    }

    public static int Port
    {
      get
      {
        return SQLData.m_Port;
      }
      set
      {
        SQLData.m_Port = value;
      }
    }

    public static string User
    {
      get
      {
        return SQLData.m_User;
      }
      set
      {
        SQLData.m_User = value;
      }
    }

    public static string Pass
    {
      get
      {
        return SQLData.m_Pass;
      }
      set
      {
        SQLData.m_Pass = value;
      }
    }

    public static string ConnStr(string DB)
    {
      return "Connect Timeout=30;Persist Security Info=False;Database=" + DB + ";Host=" + SQLData.m_Host + ";Port=" + SQLData.m_Port.ToString() + ";Username=" + SQLData.m_User + ";Password=" + SQLData.m_Pass;
    }
  }
}
