﻿// Decompiled with JetBrains decompiler
// Type: ToolsLauncher.Resources
// Assembly: ToolsLauncher, Version=1.1.0.1, Culture=neutral, PublicKeyToken=null
// MVID: B3444A73-323E-44BC-BC2D-02089BA9FF5C
// Assembly location: D:\Server\eab\Tools\ToolsLauncher.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace ToolsLauncher
{
  [CompilerGenerated]
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "2.0.0.0")]
  [DebuggerNonUserCode]
  internal class Resources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal Resources()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (object.ReferenceEquals((object) ToolsLauncher.Resources.resourceMan, (object) null))
          ToolsLauncher.Resources.resourceMan = new ResourceManager("ToolsLauncher.Resources", typeof (ToolsLauncher.Resources).Assembly);
        return ToolsLauncher.Resources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return ToolsLauncher.Resources.resourceCulture;
      }
      set
      {
        ToolsLauncher.Resources.resourceCulture = value;
      }
    }

    internal static Bitmap Error_16x16x32
    {
      get
      {
        return (Bitmap) ToolsLauncher.Resources.ResourceManager.GetObject(nameof (Error_16x16x32), ToolsLauncher.Resources.resourceCulture);
      }
    }

    internal static Bitmap Error_48x48x32
    {
      get
      {
        return (Bitmap) ToolsLauncher.Resources.ResourceManager.GetObject(nameof (Error_48x48x32), ToolsLauncher.Resources.resourceCulture);
      }
    }

    internal static byte[] ExeUpdater
    {
      get
      {
        return (byte[]) ToolsLauncher.Resources.ResourceManager.GetObject(nameof (ExeUpdater), ToolsLauncher.Resources.resourceCulture);
      }
    }

    internal static Bitmap LaunchNet7
    {
      get
      {
        return (Bitmap) ToolsLauncher.Resources.ResourceManager.GetObject(nameof (LaunchNet7), ToolsLauncher.Resources.resourceCulture);
      }
    }

    internal static Bitmap NavBack
    {
      get
      {
        return (Bitmap) ToolsLauncher.Resources.ResourceManager.GetObject(nameof (NavBack), ToolsLauncher.Resources.resourceCulture);
      }
    }

    internal static Bitmap NavForward
    {
      get
      {
        return (Bitmap) ToolsLauncher.Resources.ResourceManager.GetObject(nameof (NavForward), ToolsLauncher.Resources.resourceCulture);
      }
    }

    internal static Bitmap Setup_48x48x32
    {
      get
      {
        return (Bitmap) ToolsLauncher.Resources.ResourceManager.GetObject(nameof (Setup_48x48x32), ToolsLauncher.Resources.resourceCulture);
      }
    }

    internal static Bitmap Warning_16x16x32
    {
      get
      {
        return (Bitmap) ToolsLauncher.Resources.ResourceManager.GetObject(nameof (Warning_16x16x32), ToolsLauncher.Resources.resourceCulture);
      }
    }
  }
}
