﻿// Decompiled with JetBrains decompiler
// Type: ToolsLauncher.Updateing.UpdateItem
// Assembly: ToolsLauncher, Version=1.1.0.1, Culture=neutral, PublicKeyToken=null
// MVID: B3444A73-323E-44BC-BC2D-02089BA9FF5C
// Assembly location: D:\Server\eab\Tools\ToolsLauncher.exe

using System.IO;

namespace ToolsLauncher.Updateing
{
  public class UpdateItem
  {
    private string m_RootPath;
    private string m_FileName;
    private string m_TargetFileName;
    private string m_FullName;
    private string m_Hash;
    private bool m_IsUpdateRequired;
    private UpdateCheckStatus m_CheckStatus;

    public UpdateItem(string RootPath)
    {
      this.m_RootPath = RootPath;
    }

    public UpdateItem()
    {
      this.m_RootPath = "";
    }

    public string FileName
    {
      get
      {
        return this.m_FileName;
      }
      set
      {
        this.m_FileName = value;
        this.m_FullName = Path.GetFullPath(this.m_RootPath + value);
      }
    }

    public string TargetFileName
    {
      get
      {
        if (this.m_TargetFileName == null)
          return this.m_FileName;
        return this.m_TargetFileName;
      }
      set
      {
        this.m_TargetFileName = value;
      }
    }

    public string FullName
    {
      get
      {
        return this.m_FullName;
      }
    }

    public string Hash
    {
      get
      {
        return this.m_Hash;
      }
      set
      {
        this.m_Hash = value;
      }
    }

    public bool IsUpdateRequired
    {
      get
      {
        return this.m_IsUpdateRequired;
      }
      set
      {
        this.m_IsUpdateRequired = value;
      }
    }

    public UpdateCheckStatus CheckStatus
    {
      get
      {
        return this.m_CheckStatus;
      }
      set
      {
        this.m_CheckStatus = value;
      }
    }
  }
}
