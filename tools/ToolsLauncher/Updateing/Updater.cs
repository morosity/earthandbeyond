﻿// Decompiled with JetBrains decompiler
// Type: ToolsLauncher.Updateing.Updater
// Assembly: ToolsLauncher, Version=1.1.0.1, Culture=neutral, PublicKeyToken=null
// MVID: B3444A73-323E-44BC-BC2D-02089BA9FF5C
// Assembly location: D:\Server\eab\Tools\ToolsLauncher.exe

using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Text;
using System.Windows.Forms;
using ToolsLauncher.Cryptography;

namespace ToolsLauncher.Updateing
{
  public class Updater
  {
    private UpdateItemCollection m_Items = new UpdateItemCollection();
    public const int DisplayProgressText1 = -1;
    public const int DisplayProgressText2 = -2;
    public const int DisplayProgress1 = 1;
    public const int DisplayProgress2 = 2;
    private string m_ServerVersionRawText;
    private string m_RootPath;
    private string m_Name;
    private string m_VersionFileName;
    private string m_FileListFileName;
    private string m_ChangelogFileName;
    private string m_CurrentVersionText;
    private string m_NewVersionText;
    private bool m_RestartOnUpdate;
    private UpdateCheckResult m_CheckResult;
    private Exception m_Error;
    private VersionCompareMode m_VersionCompareMode;
    private bool m_ForceUpdate;
    private string m_BaseUrl;
    private string m_CurrentChangelog;
    private bool m_SuccessfulUpdated;
    private bool m_SelfUpdateDetected;
    private UpdateItem m_SelfUpdateItem;

    public string RootPath
    {
      get
      {
        return this.m_RootPath;
      }
      set
      {
        this.m_RootPath = value;
      }
    }

    public string Name
    {
      get
      {
        return this.m_Name;
      }
      set
      {
        this.m_Name = value;
      }
    }

    public string VersionFileName
    {
      get
      {
        return this.m_VersionFileName;
      }
      set
      {
        this.m_VersionFileName = value;
      }
    }

    public string FileListFileName
    {
      get
      {
        return this.m_FileListFileName;
      }
      set
      {
        this.m_FileListFileName = value;
      }
    }

    public string ChangelogFileName
    {
      get
      {
        return this.m_ChangelogFileName;
      }
      set
      {
        this.m_ChangelogFileName = value;
      }
    }

    public string ChangelogUrl
    {
      get
      {
        return WebPath.Combine(this.BaseUrl, this.ChangelogFileName);
      }
    }

    public string CurrentVersionText
    {
      get
      {
        return this.m_CurrentVersionText;
      }
      set
      {
        this.m_CurrentVersionText = value;
      }
    }

    public string NewVersionText
    {
      get
      {
        return this.m_NewVersionText;
      }
      set
      {
        this.m_NewVersionText = value;
      }
    }

    public bool RestartOnUpdate
    {
      get
      {
        return this.m_RestartOnUpdate;
      }
      set
      {
        this.m_RestartOnUpdate = value;
      }
    }

    public UpdateCheckResult CheckResult
    {
      get
      {
        return this.m_CheckResult;
      }
      set
      {
        this.m_CheckResult = value;
      }
    }

    public Exception Error
    {
      get
      {
        return this.m_Error;
      }
      set
      {
        this.m_Error = value;
      }
    }

    public VersionCompareMode VersionCompareMode
    {
      get
      {
        return this.m_VersionCompareMode;
      }
      set
      {
        this.m_VersionCompareMode = value;
      }
    }

    public bool ForceUpdate
    {
      get
      {
        return this.m_ForceUpdate;
      }
      set
      {
        this.m_ForceUpdate = value;
      }
    }

    public string BaseUrl
    {
      get
      {
        return this.m_BaseUrl;
      }
      set
      {
        this.m_BaseUrl = value;
      }
    }

    public string VersionFileUrl
    {
      get
      {
        return WebPath.Combine(this.BaseUrl, this.VersionFileName);
      }
    }

    public string FileListUrl
    {
      get
      {
        return WebPath.Combine(this.BaseUrl, this.FileListFileName);
      }
    }

    public string CurrentChangelog
    {
      get
      {
        return this.m_CurrentChangelog;
      }
      set
      {
        this.m_CurrentChangelog = value;
      }
    }

    public virtual UpdateCheckResult CheckForUpdates()
    {
      if (string.IsNullOrEmpty(this.VersionFileName))
        throw new InvalidOperationException("VersionFileName is not set to a value.");
      if (string.IsNullOrEmpty(this.BaseUrl))
        throw new InvalidOperationException("BaseUrl is not set to a value.");
      if (this.ForceUpdate)
      {
        this.ForceUpdate = true;
        this.CurrentVersionText = "Update required.";
      }
      Version version1 = (Version) null;
      int num1 = 0;
      string str1 = (string) null;
      if (!File.Exists(this.VersionFileName))
      {
        this.ForceUpdate = true;
        this.CurrentVersionText = "Version cannot be determined.";
      }
      else
      {
        str1 = File.ReadAllText(this.VersionFileName);
        switch (this.VersionCompareMode)
        {
          case VersionCompareMode.SameText:
            this.CurrentVersionText = str1;
            break;
          case VersionCompareMode.Number:
            num1 = int.Parse(str1);
            this.CurrentVersionText = num1.ToString();
            break;
          case VersionCompareMode.Version:
            version1 = new Version(str1);
            this.CurrentVersionText = version1.ToString();
            break;
        }
      }
      string str2;
      try
      {
        str2 = LauncherUtility.DownloadString(this.VersionFileUrl);
        this.m_ServerVersionRawText = str2;
      }
      catch (WebException ex)
      {
        this.Error = (Exception) ex;
        Program.LogException((Exception) ex);
        this.CheckResult = ex.Status != WebExceptionStatus.Timeout ? UpdateCheckResult.Error : UpdateCheckResult.Timeout;
        return this.CheckResult;
      }
      catch (Exception ex)
      {
        this.Error = ex;
        Program.LogException(ex);
        this.CheckResult = UpdateCheckResult.Error;
        return this.CheckResult;
      }
      Version version2 = (Version) null;
      int num2 = 0;
      switch (this.VersionCompareMode)
      {
        case VersionCompareMode.SameText:
          this.NewVersionText = str2;
          break;
        case VersionCompareMode.Number:
          num2 = int.Parse(str2);
          this.NewVersionText = num2.ToString();
          break;
        case VersionCompareMode.Version:
          version2 = new Version(str2);
          this.NewVersionText = version2.ToString();
          break;
      }
      if (this.ForceUpdate)
      {
        this.CheckResult = UpdateCheckResult.DownloadRequired;
        return this.CheckResult;
      }
      switch (this.VersionCompareMode)
      {
        case VersionCompareMode.SameText:
          this.CheckResult = !string.Equals(str2, str1, StringComparison.InvariantCultureIgnoreCase) ? UpdateCheckResult.UpdateAvailable : UpdateCheckResult.NoUpdatesAvailable;
          break;
        case VersionCompareMode.Number:
          this.CheckResult = num2 > num1 ? UpdateCheckResult.UpdateAvailable : UpdateCheckResult.NoUpdatesAvailable;
          break;
        case VersionCompareMode.Version:
          this.CheckResult = version2 > version1 ? UpdateCheckResult.UpdateAvailable : UpdateCheckResult.NoUpdatesAvailable;
          break;
        default:
          this.CheckResult = UpdateCheckResult.UpdateAvailable;
          break;
      }
      return this.CheckResult;
    }

    public void UpdateWithWorker(BackgroundWorker worker)
    {
      Progress progress1 = new Progress(0, 3);
      Progress progress2 = new Progress();
      byte[] buffer = new byte[4096];
      worker.ReportProgress(1, (object) progress1);
      using (WebClient webClient = new WebClient())
      {
        worker.ReportProgress(-1, (object) "Downloading File List ...");
        worker.ReportProgress(-2, (object) string.Empty);
        string s;
        try
        {
          s = webClient.DownloadString(this.FileListUrl);
        }
        catch (Exception ex)
        {
          throw new ApplicationException("Could not download file-list.\nDetails: " + ex.Message, ex);
        }
        progress1.Increment();
        worker.ReportProgress(1, (object) progress1);
        worker.ReportProgress(-1, (object) "Validating File List ...");
        worker.ReportProgress(-2, (object) string.Empty);
        this.Items.Clear();
        StringReader stringReader = new StringReader(s);
        string str = stringReader.ReadLine();
        while (str != null)
        {
          string[] strArray = str.Split('\t');
          UpdateItem updateItem = new UpdateItem(this.m_RootPath);
          if (strArray.Length >= 1)
          {
            updateItem.FileName = strArray[0];
            if (string.Equals(Path.GetFullPath(updateItem.FileName), Path.GetFullPath(Application.ExecutablePath), StringComparison.InvariantCultureIgnoreCase))
            {
              updateItem.TargetFileName = updateItem.FileName + ".tmp";
              this.SelfUpdateDetected = true;
              this.SelfUpdateItem = updateItem;
            }
            if (!string.IsNullOrEmpty(updateItem.FileName))
              this.m_Items.Add(updateItem);
            else
              continue;
          }
          if (strArray.Length >= 2)
            updateItem.Hash = strArray[1];
          str = stringReader.ReadLine();
        }
        progress1.Increment();
        worker.ReportProgress(1, (object) progress1);
        if (worker.CancellationPending)
          return;
        progress2.Reset(0, this.Items.Count);
        worker.ReportProgress(2, (object) progress2);
        worker.ReportProgress(-1, (object) "Validating Files ...");
        Crc32Processor crc32Processor = new Crc32Processor();
        bool flag;
        foreach (UpdateItem updateItem in (Collection<UpdateItem>) this.Items)
        {
          if (worker.CancellationPending)
            return;
          worker.ReportProgress(-2, (object) updateItem.FullName);
          if (!File.Exists(updateItem.FullName))
          {
            updateItem.CheckStatus = UpdateCheckStatus.NotExisiting;
            updateItem.IsUpdateRequired = true;
          }
          else
          {
            using (FileStream fileStream = File.OpenRead(updateItem.FullName))
            {
              int length;
              do
              {
                flag = true;
                length = fileStream.Read(buffer, 0, buffer.Length);
                crc32Processor.Process(buffer, 0, length);
              }
              while (length == buffer.Length);
            }
            if (updateItem.Hash != crc32Processor.Current.ToString("X8"))
            {
              updateItem.CheckStatus = UpdateCheckStatus.HashMismatch;
              updateItem.IsUpdateRequired = true;
            }
            else
              updateItem.CheckStatus = UpdateCheckStatus.Ok;
            crc32Processor.Reset();
          }
          progress2.Increment();
          worker.ReportProgress(2, (object) progress2);
        }
        progress1.Increment();
        worker.ReportProgress(1, (object) progress1);
        worker.ReportProgress(-1, (object) "Downloading Files ...");
        foreach (UpdateItem updateItem in (Collection<UpdateItem>) this.Items)
        {
          if (worker.CancellationPending)
            return;
          if (updateItem.IsUpdateRequired)
          {
            string url2 = updateItem.FileName;
            if (url2.StartsWith("./"))
              url2 = url2.Substring(2);
            using (Stream stream = webClient.OpenRead(WebPath.Combine(this.BaseUrl, url2)))
            {
              string directoryName = Path.GetDirectoryName(updateItem.FullName);
              if (!Directory.Exists(directoryName))
                Directory.CreateDirectory(directoryName);
              using (FileStream fileStream = File.Open(this.m_RootPath + updateItem.TargetFileName, File.Exists(this.m_RootPath + updateItem.TargetFileName) ? FileMode.Truncate : FileMode.Create, FileAccess.Write, FileShare.None))
              {
                int maximumValue = int.Parse(webClient.ResponseHeaders[HttpResponseHeader.ContentLength]);
                progress2.Reset(0, maximumValue);
                worker.ReportProgress(2, (object) progress2);
                int num = 0;
                do
                {
                  flag = true;
                  worker.ReportProgress(-2, (object) string.Format("Downloading: {0} - {1:N0}/{2:N0}", (object) url2, (object) num, (object) maximumValue));
                  int count = stream.Read(buffer, 0, buffer.Length);
                  fileStream.Write(buffer, 0, count);
                  num += count;
                  progress2.CurrentValue = num;
                  worker.ReportProgress(2, (object) progress2);
                }
                while (num < maximumValue);
              }
            }
            if (!string.IsNullOrEmpty(updateItem.Hash))
            {
              progress2.Reset();
              worker.ReportProgress(-2, (object) string.Format("Validating: {0}", (object) url2));
              using (FileStream fileStream = File.OpenRead(this.m_RootPath + updateItem.TargetFileName))
              {
                int length;
                do
                {
                  flag = true;
                  length = fileStream.Read(buffer, 0, buffer.Length);
                  crc32Processor.Process(buffer, 0, length);
                }
                while (length != 0);
              }
              if (updateItem.Hash != crc32Processor.Current.ToString("X8"))
                throw new ApplicationException("Downloaded file is invalid. Please restart launcher and repatch files.\nFile: " + this.m_RootPath + updateItem.TargetFileName);
              crc32Processor.Reset();
            }
          }
        }
        worker.ReportProgress(-1, (object) "Writing Version File ...");
        worker.ReportProgress(-2, (object) string.Empty);
        File.WriteAllText(this.VersionFileName, this.m_ServerVersionRawText, Encoding.Default);
        this.m_SuccessfulUpdated = true;
        this.m_CurrentVersionText = this.m_NewVersionText;
      }
    }

    public bool SuccessfulUpdated
    {
      get
      {
        return this.m_SuccessfulUpdated;
      }
    }

    public UpdateItemCollection Items
    {
      get
      {
        return this.m_Items;
      }
    }

    public bool SelfUpdateDetected
    {
      get
      {
        return this.m_SelfUpdateDetected;
      }
      set
      {
        this.m_SelfUpdateDetected = value;
      }
    }

    public UpdateItem SelfUpdateItem
    {
      get
      {
        return this.m_SelfUpdateItem;
      }
      set
      {
        this.m_SelfUpdateItem = value;
      }
    }

    public void DownloadPatchlog()
    {
      if (string.IsNullOrEmpty(this.ChangelogFileName))
        return;
      try
      {
        this.CurrentChangelog = LauncherUtility.DownloadString(this.ChangelogUrl);
      }
      catch (Exception ex)
      {
        Program.LogException(ex);
      }
    }
  }
}
