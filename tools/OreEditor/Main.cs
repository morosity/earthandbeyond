﻿// Decompiled with JetBrains decompiler
// Type: OreEditor.Main
// Assembly: OreEditor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C0E9A1B-E579-45C0-B2C2-63BCDF738A55
// Assembly location: D:\Server\eab\Tools\OreEditor\OreEditor\OreEditor.exe

using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using OreEditor.GUI.Pages;
using OreEditor.MySQL;
using OreEditor.Properties;
using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace OreEditor
{
    public class Main : XtraForm
    {
        public DataTable m_SectorList;
        private DataTable m_FieldList;
        private string m_OldPage;
        private FieldOreEdit pnFieldOreEdit;
        private SectorOreEdit pnSectorOreEdit;
        private IContainer components;
        private RibbonControl rRibbon = new RibbonControl();
        private RibbonPage pSector = new RibbonPage();
        private RibbonPageGroup ribbonPageGroup1 = new RibbonPageGroup();
        private RibbonPage pField = new RibbonPage();
        private PanelControl pPannel = new PanelControl();
        private RepositoryItemLookUpEdit rItemSectorEdit = new RepositoryItemLookUpEdit();
        public BarEditItem eBarSector = new BarEditItem();
        private BarEditItem eBarField = new BarEditItem();
        private RepositoryItemLookUpEdit repositoryItemLookUpEdit1 = new RepositoryItemLookUpEdit();
        private RibbonPageGroup ribbonPageGroup2 = new RibbonPageGroup();
        private BarButtonItem bSave = new BarButtonItem();
        private BarButtonItem bBarReload = new BarButtonItem();
        private RibbonPageGroup ribbonPageGroup3 = new RibbonPageGroup();
        private RibbonPageGroup ribbonPageGroup4 = new RibbonPageGroup();

        public Main()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            m_SectorList.Merge(SQL.ExecuteQuery("SELECT sectors.sector_id as sector_id,sectors.name as name FROM sectors", "net7"));
            ((RepositoryItemLookUpEditBase)rItemSectorEdit).DataSource = m_SectorList;
            ((RepositoryItemLookUpEditBase)rItemSectorEdit).ValueMember = "sector_id";
            ((RepositoryItemLookUpEditBase)rItemSectorEdit).DisplayMember = "name";
            Controls.Add((Control)pnFieldOreEdit);
            Controls.Add((Control)pnSectorOreEdit);
            ((Control)pnFieldOreEdit).Hide();
            ((Control)pnSectorOreEdit).Show();
            rRibbon.SelectedPage = rRibbon.Pages[0];
            m_OldPage = rRibbon.SelectedPage.Name;
            pPannel_Resize((object)null, (EventArgs)null);
        }

        private void rRibbon_SelectedPageChanged(object sender, EventArgs e)
        {
            if (rRibbon.SelectedPage.Name == m_OldPage)
                return;
            switch (m_OldPage)
            {
                case "pSector":
                    ((Control)pnSectorOreEdit).Hide();
                    break;
                case "pField":
                    ((Control)pnFieldOreEdit).Hide();
                    break;
            }
            switch (rRibbon.SelectedPage.Name)
            {
                case "pSector":
                    ((Control)pnSectorOreEdit).Show();
                    break;
                case "pField":
                    ((Control)pnFieldOreEdit).Show();
                    break;
            }
            m_OldPage = rRibbon.SelectedPage.Name;
        }

        private void pPannel_Resize(object sender, EventArgs e)
        {
            if (Controls.Count <= 0)
                return;
            for (int index = 0; index < Controls.Count; ++index)
            {
                Controls[index].Height = Size.Height;
                Controls[index].Width = Size.Width;
            }
        }

        private void UpdateFields(int SectorID)
        {
            string Query = "SELECT sector_objects.name, sector_objects.sector_id, sector_objects_harvestable.resource_id FROM sector_objects_harvestable Inner Join sector_objects ON sector_objects_harvestable.resource_id = sector_objects.sector_object_id WHERE sector_objects.sector_id =  '" + SectorID.ToString() + "';";
            m_FieldList.Clear();
            m_FieldList.Merge(SQL.ExecuteQuery(Query, "net7"));
            ((RepositoryItemLookUpEditBase)repositoryItemLookUpEdit1).DataSource = m_FieldList;
            ((RepositoryItemLookUpEditBase)repositoryItemLookUpEdit1).DisplayMember = "name";
            ((RepositoryItemLookUpEditBase)repositoryItemLookUpEdit1).ValueMember = "resource_id";
        }

        private void eBarSector_EditValueChanged(object sender, EventArgs e)
        {
            pnSectorOreEdit.ChangeSector(int.Parse(eBarSector.EditValue.ToString()));
            UpdateFields(int.Parse(eBarSector.EditValue.ToString()));
        }

        private void bBarField_EditValueChanged(object sender, EventArgs e)
        {
            pnFieldOreEdit.ChangeResourceID(int.Parse(eBarField.EditValue.ToString()));
        }

        private void bSave_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to save the data?", "Saveing", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) != DialogResult.Yes)
                return;
            switch (rRibbon.SelectedPage.Name)
            {
                case "pSector":
                    pnSectorOreEdit.SaveData();
                    break;
                case "pField":
                    pnFieldOreEdit.SaveData();
                    break;
            }
        }

        private void bBarReload_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to reload the data? You could loose unsaved work.", "Reloading", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) != DialogResult.Yes)
                return;
            switch (rRibbon.SelectedPage.Name)
            {
                case "pSector":
                    if (eBarSector.EditValue == null)
                        break;
                    UpdateFields(int.Parse(eBarSector.EditValue.ToString()));
                    pnSectorOreEdit.ReloadData();
                    break;
                case "pField":
                    pnFieldOreEdit.ReloadData();
                    break;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
                components.Dispose();
            Dispose(disposing);
        }

        private void InitializeComponent()
        {
            ((ISupportInitialize)rRibbon).BeginInit();
            ((ISupportInitialize)rItemSectorEdit).BeginInit();
            ((ISupportInitialize)repositoryItemLookUpEdit1).BeginInit();
            ((ISupportInitialize)pPannel).BeginInit();
            ((XtraForm)this).SuspendLayout();
            rRibbon.ApplicationIcon = ((Bitmap)null);
            ((BarItems)rRibbon.Items).AddRange(new BarItem[4]
            {
        (BarItem) eBarSector,
        (BarItem) eBarField,
        (BarItem) bSave,
        (BarItem) bBarReload
            });
            ((Control)rRibbon).Location = new Point(0, 0);
            rRibbon.MaxItemId = 5;
            ((Control)rRibbon).Name = "rRibbon";
            rRibbon.Pages.AddRange(new RibbonPage[2]
            {
        pSector,
        pField
            });
            rRibbon.RepositoryItems.AddRange(new RepositoryItem[2]
            {
        (RepositoryItem) rItemSectorEdit,
        (RepositoryItem) repositoryItemLookUpEdit1
            });
            rRibbon.SelectedPage = pField;
            ((Control)rRibbon).Size = new Size(553, 143);
            rRibbon.SelectedPageChanged += new EventHandler(rRibbon_SelectedPageChanged);
            ((BarItem)eBarSector).Caption = "Sector";
            eBarSector.Edit = ((RepositoryItem)rItemSectorEdit);
            ((BarItem)eBarSector).Id = 1;
            ((BarItem)eBarSector).Name = "eBarSector";
            ((BarItem)eBarSector).Width = 250;
            eBarSector.EditValueChanged += new EventHandler(eBarSector_EditValueChanged);
            ((RepositoryItem)rItemSectorEdit).AutoHeight = false;
            ((RepositoryItemButtonEdit)rItemSectorEdit).Buttons.AddRange(new EditorButton[1]
            {
        new EditorButton(ButtonPredefines.Combo)
            });
            ((RepositoryItem)rItemSectorEdit).Name = "rItemSectorEdit";
            ((BarItem)eBarField).Caption = "Field   ";
            eBarField.Edit = ((RepositoryItem)repositoryItemLookUpEdit1);
            ((BarItem)eBarField).Id = 2;
            ((BarItem)eBarField).Name = "eBarField";
            ((BarItem)eBarField).Width = 250;
            eBarField.EditValueChanged += new EventHandler(bBarField_EditValueChanged);
            ((RepositoryItem)repositoryItemLookUpEdit1).AutoHeight = false;
            ((RepositoryItemButtonEdit)repositoryItemLookUpEdit1).Buttons.AddRange(new EditorButton[1]
            {
        new EditorButton(ButtonPredefines.Combo)
            });
            ((RepositoryItem)repositoryItemLookUpEdit1).Name = "repositoryItemLookUpEdit1";
            ((BarItem)bSave).Caption = "Save";
            ((BarItem)bSave).Glyph = ((Image)Resources.fileexport);
            ((BarItem)bSave).Id = 3;
            ((BarItem)bSave).Name = "bSave";
            ((BarItem)bSave).RibbonStyle = RibbonItemStyles.Large;
            // ISSUE: method pointer
            ((BarItem)bSave).ItemClick += new ItemClickEventHandler(bSave_ItemClick);
            ((BarItem)bBarReload).Caption = "Reload";
            ((BarItem)bBarReload).Glyph = ((Image)Resources.reload_page);
            ((BarItem)bBarReload).Id = 4;
            ((BarItem)bBarReload).Name = "bBarReload";
            ((BarItem)bBarReload).RibbonStyle = RibbonItemStyles.Large;
            // ISSUE: method pointer
            ((BarItem)bBarReload).ItemClick += new ItemClickEventHandler(bBarReload_ItemClick);
            pSector.Groups.AddRange(new RibbonPageGroup[2]
            {
        ribbonPageGroup1,
        ribbonPageGroup3
            });
            pSector.Name = "pSector";
            pSector.Text = "Sector";
            ((BarItemLinkCollection)ribbonPageGroup1.ItemLinks).Add((BarItem)eBarSector);
            ribbonPageGroup1.Name = "ribbonPageGroup1";
            ribbonPageGroup1.Text = "Select Sector";
            ((BarItemLinkCollection)ribbonPageGroup3.ItemLinks).Add((BarItem)bSave);
            ((BarItemLinkCollection)ribbonPageGroup3.ItemLinks).Add((BarItem)bBarReload, true);
            ribbonPageGroup3.Name = "ribbonPageGroup3";
            ribbonPageGroup3.Text = "Save";
            pField.Groups.AddRange(new RibbonPageGroup[2]
            {
        ribbonPageGroup2,
        ribbonPageGroup4
            });
            pField.Name = "pField";
            pField.Text = "Field";
            ((BarItemLinkCollection)ribbonPageGroup2.ItemLinks).Add((BarItem)eBarSector);
            ((BarItemLinkCollection)ribbonPageGroup2.ItemLinks).Add((BarItem)eBarField);
            ribbonPageGroup2.Name = "ribbonPageGroup2";
            ribbonPageGroup2.Text = "Select Sector and Field";
            ((BarItemLinkCollection)ribbonPageGroup4.ItemLinks).Add((BarItem)bSave);
            ((BarItemLinkCollection)ribbonPageGroup4.ItemLinks).Add((BarItem)bBarReload, true);
            ribbonPageGroup4.Name = "ribbonPageGroup4";
            ribbonPageGroup4.Text = "Save";
            Dock = DockStyle.Fill;
            Location = new Point(0, 143);
            Name = "pPannel";
            Size = new Size(553, 391);
            TabIndex = 3;
            Resize += new EventHandler(pPannel_Resize);
            AutoScaleDimensions = new SizeF(6f, 13f);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(553, 534);
            Controls.Add((Control)pPannel);
            Controls.Add((Control)rRibbon);
            MaximumSize = new Size(561, 538);
            Name = nameof(Main);
            //rRibbon = rRibbon;
            Text = "Ore Editor";
            Load += new EventHandler(Form1_Load);
            ((ISupportInitialize)rRibbon).EndInit();
            ((ISupportInitialize)rItemSectorEdit).EndInit();
            ((ISupportInitialize)repositoryItemLookUpEdit1).EndInit();
            ((ISupportInitialize)pPannel).EndInit();
            ((XtraForm)this).ResumeLayout(false);
        }
    }
}
