﻿// Decompiled with JetBrains decompiler
// Type: OreEditor.MySQL.SQL
// Assembly: OreEditor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C0E9A1B-E579-45C0-B2C2-63BCDF738A55
// Assembly location: D:\Server\eab\Tools\OreEditor\OreEditor\OreEditor.exe

using MySql.Data.MySqlClient;
using System;
using System.Data;
using System.Data.Common;
using System.Windows.Forms;

namespace OreEditor.MySQL
{
  internal static class SQL
  {
    private static MySqlConnection m_mySqlConnection;
    private static Timer m_SqlPingTimer;
    private static string m_LoginUser;
    private static string m_Host;
    private static string m_User;
    private static string m_Pass;
    private static int m_Port;
    private static bool m_Connected;

    public static bool Connected
    {
      get
      {
        return SQL.m_Connected;
      }
      set
      {
        SQL.m_Connected = value;
      }
    }

    public static string LoginUser
    {
      get
      {
        return SQL.m_LoginUser;
      }
      set
      {
        SQL.m_LoginUser = value;
      }
    }

    public static string Host
    {
      get
      {
        return SQL.m_Host;
      }
      set
      {
        SQL.m_Host = value;
      }
    }

    public static string User
    {
      get
      {
        return SQL.m_User;
      }
      set
      {
        SQL.m_User = value;
      }
    }

    public static string Pass
    {
      get
      {
        return SQL.m_Pass;
      }
      set
      {
        SQL.m_Pass = value;
      }
    }

    public static int Port
    {
      get
      {
        return SQL.m_Port;
      }
      set
      {
        SQL.m_Port = value;
      }
    }

    public static string ConnStr(string DB)
    {
      return "Connect Timeout=30;Persist Security Info=False;Database=" + DB + ";Host=" + SQL.m_Host + ";Port=" + SQL.m_Port.ToString() + ";Username=" + SQL.m_User + ";Password=" + SQL.m_Pass + ";Allow Zero Datetime=true";
    }

    public static void Connect()
    {
      SQL.m_mySqlConnection = new MySqlConnection(SQL.ConnStr("net7"));
      ((DbConnection) SQL.m_mySqlConnection).Open();
      SQL.m_Connected = true;
      SQL.m_SqlPingTimer = new Timer();
      SQL.m_SqlPingTimer.Interval = 120000;
      SQL.m_SqlPingTimer.Enabled = true;
      SQL.m_SqlPingTimer.Tick += new EventHandler(SQL.m_SqlPingTimer_Tick);
      SQL.m_SqlPingTimer.Start();
    }

    private static void m_SqlPingTimer_Tick(object sender, EventArgs e)
    {
      SQL.PingSQL();
    }

    public static void PingSQL()
    {
      if (!SQL.m_Connected || SQL.m_mySqlConnection.Ping())
        return;
      SQL.Connect();
    }

    public static void CommitProposedChanges(DataTable ds)
    {
      if (ds == null)
        return;
      for (int index = 0; index < ds.Rows.Count; ++index)
      {
        if (ds.Rows[index].HasVersion(DataRowVersion.Proposed))
          ds.Rows[index].EndEdit();
      }
    }

    public static void CommitProposedChanges(DataSet ds)
    {
      if (ds == null)
        return;
      for (int index1 = 0; index1 < ds.Tables.Count; ++index1)
      {
        for (int index2 = 0; index2 < ds.Tables[index1].Rows.Count; ++index2)
        {
          if (ds.Tables[index1].Rows[index2].HasVersion(DataRowVersion.Proposed))
            ds.Tables[index1].Rows[index2].EndEdit();
        }
      }
    }

    public static void UpdateDT(DataTable dt, string Query)
    {
      if (((DbConnection) SQL.m_mySqlConnection).Database != "net7")
        ((DbConnection) SQL.m_mySqlConnection).ChangeDatabase("net7");
      MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter(Query, SQL.m_mySqlConnection);
      ((DbCommandBuilder) new MySqlCommandBuilder(mySqlDataAdapter)).ConflictOption = ConflictOption.OverwriteChanges;
      ((DbDataAdapter) mySqlDataAdapter).Update(dt);
    }

    public static void UpdateDT(DataTable dt, string Schema, string Query)
    {
      if (((DbConnection) SQL.m_mySqlConnection).Database != Schema)
        ((DbConnection) SQL.m_mySqlConnection).ChangeDatabase(Schema);
      MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter(Query, SQL.m_mySqlConnection);
      ((DbCommandBuilder) new MySqlCommandBuilder(mySqlDataAdapter)).ConflictOption = ConflictOption.OverwriteChanges;
      ((DbDataAdapter) mySqlDataAdapter).Update(dt);
    }

    public static DataSet GetSchemaDS(string Query, string TableName)
    {
      DataSet dataSet = new DataSet();
      if (TableName == "")
        TableName = "noname";
      if (((DbConnection) SQL.m_mySqlConnection).Database != "net7")
        ((DbConnection) SQL.m_mySqlConnection).ChangeDatabase("net7");
      MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter(Query, SQL.m_mySqlConnection);
      MySqlCommandBuilder sqlCommandBuilder = new MySqlCommandBuilder(mySqlDataAdapter);
      ((DbDataAdapter) mySqlDataAdapter).FillSchema(dataSet, SchemaType.Source, TableName);
      return dataSet;
    }

    public static DataSet GetSchemaDS(string Query, string Schema, string TableName)
    {
      DataSet dataSet = new DataSet();
      if (TableName == "")
        TableName = "noname";
      if (((DbConnection) SQL.m_mySqlConnection).Database != Schema)
        ((DbConnection) SQL.m_mySqlConnection).ChangeDatabase(Schema);
      MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter(Query, SQL.m_mySqlConnection);
      MySqlCommandBuilder sqlCommandBuilder = new MySqlCommandBuilder(mySqlDataAdapter);
      ((DbDataAdapter) mySqlDataAdapter).FillSchema(dataSet, SchemaType.Source, TableName);
      return dataSet;
    }

    public static DataTable GetSchema(string Query)
    {
      DataTable dataTable = new DataTable();
      if (((DbConnection) SQL.m_mySqlConnection).Database != "net7")
        ((DbConnection) SQL.m_mySqlConnection).ChangeDatabase("net7");
      ((DbDataAdapter) new MySqlDataAdapter(Query, SQL.m_mySqlConnection)).FillSchema(dataTable, SchemaType.Source);
      return dataTable;
    }

    public static DataTable GetSchema(string Query, string Schema)
    {
      DataTable dataTable = new DataTable();
      if (((DbConnection) SQL.m_mySqlConnection).Database != Schema)
        ((DbConnection) SQL.m_mySqlConnection).ChangeDatabase(Schema);
      ((DbDataAdapter) new MySqlDataAdapter(Query, SQL.m_mySqlConnection)).FillSchema(dataTable, SchemaType.Source);
      return dataTable;
    }

    public static DataRow FillDataRow(DataRow dt)
    {
      foreach (DataColumn column in (InternalDataCollectionBase) dt.Table.Columns)
      {
        switch (column.DataType.Name)
        {
          case "Int32":
          case "Int64":
          case "Int16":
          case "UInt64":
          case "UInt32":
          case "UInt16":
            dt[column.ColumnName] = (object) 0;
            continue;
          case "Byte":
            dt[column.ColumnName] = (object) (byte) 0;
            continue;
          case "SByte":
            dt[column.ColumnName] = (object) (sbyte) 0;
            continue;
          case "Double":
          case "Single":
            dt[column.ColumnName] = (object) 0.0f;
            continue;
          case "String":
            dt[column.ColumnName] = (object) "changeme";
            continue;
          default:
            continue;
        }
      }
      return dt;
    }

    public static DataSet ExecuteQueryDs(string Query, string TableName)
    {
      DataSet dataSet = new DataSet();
      if (TableName == "")
        TableName = "noname";
      if (((DbConnection) SQL.m_mySqlConnection).Database != "net7")
        ((DbConnection) SQL.m_mySqlConnection).ChangeDatabase("net7");
      MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter(Query, SQL.m_mySqlConnection);
      MySqlCommandBuilder sqlCommandBuilder = new MySqlCommandBuilder(mySqlDataAdapter);
      ((DbDataAdapter) mySqlDataAdapter).FillSchema(dataSet, SchemaType.Source, TableName);
      ((DbDataAdapter) mySqlDataAdapter).Fill(dataSet, TableName);
      return dataSet;
    }

    public static DataSet ExecuteQueryDs(string Query, string Schema, string TableName)
    {
      DataSet dataSet = new DataSet();
      if (TableName == "")
        TableName = "noname";
      if (((DbConnection) SQL.m_mySqlConnection).Database != Schema)
        ((DbConnection) SQL.m_mySqlConnection).ChangeDatabase(Schema);
      MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter(Query, SQL.m_mySqlConnection);
      MySqlCommandBuilder sqlCommandBuilder = new MySqlCommandBuilder(mySqlDataAdapter);
      ((DbDataAdapter) mySqlDataAdapter).FillSchema(dataSet, SchemaType.Source, TableName);
      ((DbDataAdapter) mySqlDataAdapter).Fill(dataSet, TableName);
      return dataSet;
    }

    public static DataTable ExecuteQuery(string Query)
    {
      DataTable dataTable = new DataTable();
      if (((DbConnection) SQL.m_mySqlConnection).Database != "net7")
        ((DbConnection) SQL.m_mySqlConnection).ChangeDatabase("net7");
      ((DbDataAdapter) new MySqlDataAdapter(Query, SQL.m_mySqlConnection)).Fill(dataTable);
      return dataTable;
    }

    public static DataTable ExecuteQuery(string Query, string Schema)
    {
      DataTable dataTable = new DataTable();
      if (((DbConnection) SQL.m_mySqlConnection).Database != Schema)
        ((DbConnection) SQL.m_mySqlConnection).ChangeDatabase(Schema);
      ((DbDataAdapter) new MySqlDataAdapter(Query, SQL.m_mySqlConnection)).Fill(dataTable);
      return dataTable;
    }

    public static int ExecuteNonQuery(string Query)
    {
      if (((DbConnection) SQL.m_mySqlConnection).Database != "net7")
        ((DbConnection) SQL.m_mySqlConnection).ChangeDatabase("net7");
      return ((DbCommand) new MySqlCommand(Query, SQL.m_mySqlConnection)).ExecuteNonQuery();
    }

    public static int ExecuteNonQuery(string Query, string Schema)
    {
      if (((DbConnection) SQL.m_mySqlConnection).Database != Schema)
        ((DbConnection) SQL.m_mySqlConnection).ChangeDatabase(Schema);
      return ((DbCommand) new MySqlCommand(Query, SQL.m_mySqlConnection)).ExecuteNonQuery();
    }

    public static string EscapeString(string UnsafeString)
    {
      return UnsafeString.Replace("\\", "\\\\").Replace("'", "\\'").Replace("\"", "\\\"");
    }

    public static string[] EscapeString(string[] UnsafeStringArray)
    {
      string[] array = new string[1]{ "" };
      foreach (string unsafeString in UnsafeStringArray)
      {
        string str = unsafeString.Replace("\\", "\\\\").Replace("'", "\\'").Replace("\"", "\\\"");
        array[array.Length - 1] = str;
        Array.Resize<string>(ref array, array.Length + 1);
      }
      return array;
    }
  }
}
