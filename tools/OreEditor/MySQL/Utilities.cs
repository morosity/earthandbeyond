﻿// Decompiled with JetBrains decompiler
// Type: OreEditor.MySQL.Utilities
// Assembly: OreEditor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C0E9A1B-E579-45C0-B2C2-63BCDF738A55
// Assembly location: D:\Server\eab\Tools\OreEditor\OreEditor\OreEditor.exe

using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace OreEditor.MySQL
{
  internal static class Utilities
  {
    public static void WriteToErrorLog(string Message)
    {
      StreamWriter streamWriter = new StreamWriter("Error.log", true);
      streamWriter.WriteLine(DateTime.Now.ToString("MM/dd/yy :: HH:mm:ss | ") + Message);
      streamWriter.Close();
    }

    public static string GetSha1(string value)
    {
      byte[] hash = new SHA1Managed().ComputeHash(Encoding.ASCII.GetBytes(value));
      string empty = string.Empty;
      foreach (byte num in hash)
        empty += num.ToString("X2");
      return empty;
    }

    public static string IdentifyRace(string Race_ID)
    {
      switch (int.Parse(Race_ID))
      {
        case 0:
          return "Terran";
        case 1:
          return "Jenquai";
        case 2:
          return "Progen";
        default:
          return "Uknown";
      }
    }

    public static string IdentifyProfession(string Prof_ID)
    {
      switch (int.Parse(Prof_ID))
      {
        case 0:
          return "Warrior";
        case 1:
          return "Trader";
        case 2:
          return "Explorer";
        default:
          return "Unknown";
      }
    }
  }
}
