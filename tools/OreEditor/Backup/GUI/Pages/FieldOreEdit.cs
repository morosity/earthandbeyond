﻿// Decompiled with JetBrains decompiler
// Type: OreEditor.GUI.Pages.FieldOreEdit
// Assembly: OreEditor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C0E9A1B-E579-45C0-B2C2-63BCDF738A55
// Assembly location: D:\Server\eab\Tools\OreEditor\OreEditor\OreEditor.exe

using DevExpress.Data;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using OreEditor.MySQL;
using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace OreEditor.GUI.Pages
{
  public class FieldOreEdit : XtraUserControl
  {
    private int m_ResourceID;
    private DataTable m_OreList;
    private DataTable m_CurrentOres;
    private DataTable m_AddOres;
    private IContainer components;
    private GridView gridView4;
    private GridControl gcCurrentOre;
    private GridView gvCurrentOre;
    private GridColumn gridColumn1;
    private GridColumn gridColumn3;
    private SimpleButton bAddRow;
    private GridControl gcAdd;
    private GridView gvAdd;
    private GridColumn gridColumn4;
    private GridColumn gridColumn5;
    private GridColumn gridColumn6;
    private GridView gridView2;
    private SimpleButton bDelAll;
    private SimpleButton bAddAll;
    private SimpleButton bDelRow;
    private GroupControl groupControl2;
    private GroupControl groupControl1;
    private GridColumn gridColumn2;
    private GridColumn gridColumn7;

    public FieldOreEdit()
    {
      base.\u002Ector();
      this.InitializeComponent();
    }

    public void ChangeResourceID(int ResourceID)
    {
      this.m_ResourceID = ResourceID;
      string Query = "SELECT sector_objects_harvestable_oretypes.resource_id, sector_objects_harvestable_oretypes.additional_ore_item_id, sector_objects_harvestable_oretypes.frequency, 1 as level, 'none' as name FROM sector_objects_harvestable_oretypes WHERE sector_objects_harvestable_oretypes.resource_id =  '" + ResourceID.ToString() + "';";
      this.m_CurrentOres.Clear();
      this.m_CurrentOres.Merge(SQL.ExecuteQuery(Query, "net7"));
      this.gcCurrentOre.set_DataSource((object) this.m_CurrentOres);
      this.m_AddOres.Clear();
      this.m_AddOres.Merge(this.m_OreList);
      foreach (DataRow row in (InternalDataCollectionBase) this.m_CurrentOres.Rows)
      {
        foreach (DataRow dataRow in this.m_AddOres.Select("[id]='" + row["additional_ore_item_id"].ToString() + "'"))
        {
          row["level"] = dataRow["level"];
          row["name"] = dataRow["name"];
          dataRow.Delete();
        }
      }
      this.gcAdd.set_DataSource((object) this.m_AddOres);
    }

    private void FieldOreEdit_Load(object sender, EventArgs e)
    {
      this.m_OreList.Merge(SQL.ExecuteQuery("SELECT item_base.name,item_base.id,item_base.level FROM item_base WHERE item_base.category =  '81'", "net7"));
    }

    private void bAddRow_Click(object sender, EventArgs e)
    {
      GridView gridView = this.gcAdd.get_Views().get_Item(0) as GridView;
      int focusedRowHandle = ((ColumnView) gridView).get_FocusedRowHandle();
      if (focusedRowHandle < 0)
        return;
      int num1 = int.Parse(((ColumnView) gridView).GetRowCellValue(focusedRowHandle, ((ColumnView) this.gvAdd).get_Columns().get_Item("id")).ToString());
      int num2 = int.Parse(((ColumnView) gridView).GetRowCellValue(focusedRowHandle, ((ColumnView) this.gvAdd).get_Columns().get_Item("level")).ToString());
      string str = ((ColumnView) gridView).GetRowCellValue(focusedRowHandle, ((ColumnView) this.gvAdd).get_Columns().get_Item("name")).ToString();
      DataRow row = this.m_CurrentOres.NewRow();
      row["resource_id"] = (object) this.m_ResourceID;
      row["additional_ore_item_id"] = (object) num1;
      row["frequency"] = (object) 0;
      row["level"] = (object) num2;
      row["name"] = (object) str;
      this.m_CurrentOres.Rows.Add(row);
      foreach (DataRow dataRow in this.m_AddOres.Select("[id]='" + num1.ToString() + "'"))
        dataRow.Delete();
    }

    private void bDelRow_Click(object sender, EventArgs e)
    {
      GridView gridView = this.gcCurrentOre.get_Views().get_Item(0) as GridView;
      int focusedRowHandle = ((ColumnView) gridView).get_FocusedRowHandle();
      if (focusedRowHandle < 0)
        return;
      int num = int.Parse(((ColumnView) gridView).GetRowCellValue(focusedRowHandle, ((ColumnView) this.gvCurrentOre).get_Columns().get_Item("additional_ore_item_id")).ToString());
      DataRow[] dataRowArray = this.m_OreList.Select("[id]='" + num.ToString() + "'");
      if (dataRowArray.Length == 1)
      {
        DataRow dataRow1 = dataRowArray[0];
        DataRow row = this.m_AddOres.NewRow();
        row["name"] = dataRow1["name"];
        row["level"] = dataRow1["level"];
        row["id"] = dataRow1["id"];
        this.m_AddOres.Rows.Add(row);
        foreach (DataRow dataRow2 in this.m_CurrentOres.Select("[additional_ore_item_id]='" + num.ToString() + "'"))
          dataRow2.Delete();
      }
      else
      {
        foreach (DataRow dataRow in this.m_CurrentOres.Select("[additional_ore_item_id]='" + num.ToString() + "'"))
          dataRow.Delete();
      }
    }

    private void bAddAll_Click(object sender, EventArgs e)
    {
      foreach (DataRow row1 in (InternalDataCollectionBase) this.m_AddOres.Rows)
      {
        if (row1.RowState != DataRowState.Deleted)
        {
          DataRow row2 = this.m_CurrentOres.NewRow();
          row2["resource_id"] = (object) this.m_ResourceID;
          row2["additional_ore_item_id"] = row1["id"];
          row2["name"] = row1["name"];
          row2["level"] = row1["level"];
          row2["frequency"] = (object) 0;
          this.m_CurrentOres.Rows.Add(row2);
          row1.Delete();
        }
      }
    }

    private void bDelAll_Click(object sender, EventArgs e)
    {
      foreach (DataRow row1 in (InternalDataCollectionBase) this.m_CurrentOres.Rows)
      {
        DataRow[] dataRowArray = this.m_OreList.Select("[id]='" + int.Parse(row1["additional_ore_item_id"].ToString()).ToString() + "'");
        if (dataRowArray.Length == 1)
        {
          DataRow dataRow = dataRowArray[0];
          DataRow row2 = this.m_AddOres.NewRow();
          row2["name"] = dataRow["name"];
          row2["level"] = dataRow["level"];
          row2["id"] = dataRow["id"];
          this.m_AddOres.Rows.Add(row2);
          row1.Delete();
        }
        else
          row1.Delete();
      }
    }

    public void ReloadData()
    {
      if (this.m_ResourceID == -1)
        return;
      this.ChangeResourceID(this.m_ResourceID);
    }

    public void SaveData()
    {
      SQL.UpdateDT(this.m_CurrentOres, "SELECT sector_objects_harvestable_oretypes.resource_id, sector_objects_harvestable_oretypes.additional_ore_item_id, sector_objects_harvestable_oretypes.frequency FROM sector_objects_harvestable_oretypes WHERE sector_objects_harvestable_oretypes.resource_id =  '" + this.m_ResourceID.ToString() + "';");
      int num = (int) MessageBox.Show("Saved Data");
    }

    protected virtual void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.gridView4 = new GridView();
      this.gcCurrentOre = new GridControl();
      this.gvCurrentOre = new GridView();
      this.gridColumn1 = new GridColumn();
      this.gridColumn3 = new GridColumn();
      this.gridColumn2 = new GridColumn();
      this.gridColumn7 = new GridColumn();
      this.bAddRow = new SimpleButton();
      this.gcAdd = new GridControl();
      this.gvAdd = new GridView();
      this.gridColumn4 = new GridColumn();
      this.gridColumn5 = new GridColumn();
      this.gridColumn6 = new GridColumn();
      this.gridView2 = new GridView();
      this.bDelAll = new SimpleButton();
      this.bAddAll = new SimpleButton();
      this.bDelRow = new SimpleButton();
      this.groupControl2 = new GroupControl();
      this.groupControl1 = new GroupControl();
      ((ISupportInitialize) this.gridView4).BeginInit();
      ((ISupportInitialize) this.gcCurrentOre).BeginInit();
      ((ISupportInitialize) this.gvCurrentOre).BeginInit();
      ((ISupportInitialize) this.gcAdd).BeginInit();
      ((ISupportInitialize) this.gvAdd).BeginInit();
      ((ISupportInitialize) this.gridView2).BeginInit();
      ((ISupportInitialize) this.groupControl2).BeginInit();
      ((Control) this.groupControl2).SuspendLayout();
      ((ISupportInitialize) this.groupControl1).BeginInit();
      ((Control) this.groupControl1).SuspendLayout();
      ((Control) this).SuspendLayout();
      ((BaseView) this.gridView4).set_GridControl(this.gcCurrentOre);
      ((BaseView) this.gridView4).set_Name("gridView4");
      ((Control) this.gcCurrentOre).Dock = DockStyle.Fill;
      ((Control) this.gcCurrentOre).Location = new Point(2, 22);
      this.gcCurrentOre.set_MainView((BaseView) this.gvCurrentOre);
      ((Control) this.gcCurrentOre).Name = "gcCurrentOre";
      ((Control) this.gcCurrentOre).Size = new Size(248, 345);
      ((Control) this.gcCurrentOre).TabIndex = 1;
      this.gcCurrentOre.get_ViewCollection().AddRange(new BaseView[2]
      {
        (BaseView) this.gvCurrentOre,
        (BaseView) this.gridView4
      });
      ((ColumnView) this.gvCurrentOre).get_Columns().AddRange(new GridColumn[4]
      {
        this.gridColumn1,
        this.gridColumn3,
        this.gridColumn2,
        this.gridColumn7
      });
      ((BaseView) this.gvCurrentOre).set_GridControl(this.gcCurrentOre);
      ((ColumnView) this.gvCurrentOre).set_GroupCount(1);
      ((BaseView) this.gvCurrentOre).set_Name("gvCurrentOre");
      this.gvCurrentOre.get_OptionsView().set_ColumnAutoWidth(false);
      ((ColumnView) this.gvCurrentOre).get_SortInfo().AddRange(new GridColumnSortInfo[1]
      {
        new GridColumnSortInfo(this.gridColumn2, (ColumnSortOrder) 1)
      });
      this.gridColumn1.set_Caption("ItemID");
      this.gridColumn1.set_FieldName("additional_ore_item_id");
      this.gridColumn1.set_Name("gridColumn1");
      this.gridColumn1.get_OptionsColumn().set_AllowEdit(false);
      this.gridColumn1.set_Visible(true);
      this.gridColumn1.set_VisibleIndex(0);
      this.gridColumn1.set_Width(61);
      this.gridColumn3.set_Caption("Frequency");
      this.gridColumn3.set_FieldName("frequency");
      this.gridColumn3.set_Name("gridColumn3");
      this.gridColumn3.set_Visible(true);
      this.gridColumn3.set_VisibleIndex(2);
      this.gridColumn3.set_Width(94);
      this.gridColumn2.set_Caption("Level");
      this.gridColumn2.set_FieldName("level");
      this.gridColumn2.set_Name("gridColumn2");
      this.gridColumn2.get_OptionsColumn().set_AllowEdit(false);
      this.gridColumn7.set_Caption("Name");
      this.gridColumn7.set_FieldName("name");
      this.gridColumn7.set_Name("gridColumn7");
      this.gridColumn7.get_OptionsColumn().set_AllowEdit(false);
      this.gridColumn7.set_Visible(true);
      this.gridColumn7.set_VisibleIndex(1);
      this.gridColumn7.set_Width(147);
      ((Control) this.bAddRow).Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      ((Control) this.bAddRow).Location = new Point(258, 54);
      ((Control) this.bAddRow).Name = "bAddRow";
      ((Control) this.bAddRow).Size = new Size(45, 23);
      ((Control) this.bAddRow).TabIndex = 10;
      ((Control) this.bAddRow).Text = "<";
      ((Control) this.bAddRow).Click += new EventHandler(this.bAddRow_Click);
      ((Control) this.gcAdd).Dock = DockStyle.Fill;
      ((Control) this.gcAdd).Location = new Point(2, 22);
      this.gcAdd.set_MainView((BaseView) this.gvAdd);
      ((Control) this.gcAdd).Name = "gcAdd";
      ((Control) this.gcAdd).Size = new Size(248, 345);
      ((Control) this.gcAdd).TabIndex = 0;
      this.gcAdd.get_ViewCollection().AddRange(new BaseView[2]
      {
        (BaseView) this.gvAdd,
        (BaseView) this.gridView2
      });
      ((ColumnView) this.gvAdd).get_Columns().AddRange(new GridColumn[3]
      {
        this.gridColumn4,
        this.gridColumn5,
        this.gridColumn6
      });
      ((BaseView) this.gvAdd).set_GridControl(this.gcAdd);
      ((ColumnView) this.gvAdd).set_GroupCount(1);
      ((BaseView) this.gvAdd).set_Name("gvAdd");
      this.gvAdd.get_OptionsView().set_ColumnAutoWidth(false);
      ((ColumnView) this.gvAdd).get_SortInfo().AddRange(new GridColumnSortInfo[1]
      {
        new GridColumnSortInfo(this.gridColumn6, (ColumnSortOrder) 1)
      });
      this.gridColumn4.set_Caption("ItemID");
      this.gridColumn4.set_FieldName("id");
      this.gridColumn4.set_Name("gridColumn4");
      this.gridColumn4.get_OptionsColumn().set_AllowEdit(false);
      this.gridColumn4.set_Visible(true);
      this.gridColumn4.set_VisibleIndex(0);
      this.gridColumn4.set_Width(63);
      this.gridColumn5.set_Caption("Name");
      this.gridColumn5.set_FieldName("name");
      this.gridColumn5.set_Name("gridColumn5");
      this.gridColumn5.get_OptionsColumn().set_AllowEdit(false);
      this.gridColumn5.set_Visible(true);
      this.gridColumn5.set_VisibleIndex(1);
      this.gridColumn5.set_Width(147);
      this.gridColumn6.set_Caption("Level");
      this.gridColumn6.set_FieldName("level");
      this.gridColumn6.set_Name("gridColumn6");
      this.gridColumn6.get_OptionsColumn().set_AllowEdit(false);
      this.gridColumn6.set_Visible(true);
      this.gridColumn6.set_VisibleIndex(2);
      this.gridColumn6.set_Width(42);
      ((BaseView) this.gridView2).set_GridControl(this.gcAdd);
      ((BaseView) this.gridView2).set_Name("gridView2");
      ((Control) this.bDelAll).Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      ((Control) this.bDelAll).Location = new Point(258, 141);
      ((Control) this.bDelAll).Name = "bDelAll";
      ((Control) this.bDelAll).Size = new Size(45, 23);
      ((Control) this.bDelAll).TabIndex = 13;
      ((Control) this.bDelAll).Text = ">>";
      ((Control) this.bDelAll).Click += new EventHandler(this.bDelAll_Click);
      ((Control) this.bAddAll).Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      ((Control) this.bAddAll).Location = new Point(258, 112);
      ((Control) this.bAddAll).Name = "bAddAll";
      ((Control) this.bAddAll).Size = new Size(45, 23);
      ((Control) this.bAddAll).TabIndex = 12;
      ((Control) this.bAddAll).Text = "<<";
      ((Control) this.bAddAll).Click += new EventHandler(this.bAddAll_Click);
      ((Control) this.bDelRow).Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      ((Control) this.bDelRow).Location = new Point(258, 83);
      ((Control) this.bDelRow).Name = "bDelRow";
      ((Control) this.bDelRow).Size = new Size(45, 23);
      ((Control) this.bDelRow).TabIndex = 11;
      ((Control) this.bDelRow).Text = ">";
      ((Control) this.bDelRow).Click += new EventHandler(this.bDelRow_Click);
      ((Control) this.groupControl2).Controls.Add((Control) this.gcAdd);
      ((Control) this.groupControl2).Dock = DockStyle.Right;
      ((Control) this.groupControl2).Location = new Point(309, 0);
      ((Control) this.groupControl2).Name = "groupControl2";
      ((Control) this.groupControl2).Size = new Size(252, 369);
      ((Control) this.groupControl2).TabIndex = 8;
      ((Control) this.groupControl2).Text = "Avalable";
      ((Control) this.groupControl1).Controls.Add((Control) this.gcCurrentOre);
      ((Control) this.groupControl1).Dock = DockStyle.Left;
      ((Control) this.groupControl1).Location = new Point(0, 0);
      ((Control) this.groupControl1).Name = "groupControl1";
      ((Control) this.groupControl1).Size = new Size(252, 369);
      ((Control) this.groupControl1).TabIndex = 7;
      ((Control) this.groupControl1).Text = "Current";
      ((ContainerControl) this).AutoScaleDimensions = new SizeF(6f, 13f);
      ((ContainerControl) this).AutoScaleMode = AutoScaleMode.Font;
      ((Control) this).Controls.Add((Control) this.bAddRow);
      ((Control) this).Controls.Add((Control) this.bDelAll);
      ((Control) this).Controls.Add((Control) this.bAddAll);
      ((Control) this).Controls.Add((Control) this.bDelRow);
      ((Control) this).Controls.Add((Control) this.groupControl2);
      ((Control) this).Controls.Add((Control) this.groupControl1);
      ((Control) this).Name = nameof (FieldOreEdit);
      ((Control) this).Size = new Size(561, 369);
      ((UserControl) this).Load += new EventHandler(this.FieldOreEdit_Load);
      ((ISupportInitialize) this.gridView4).EndInit();
      ((ISupportInitialize) this.gcCurrentOre).EndInit();
      ((ISupportInitialize) this.gvCurrentOre).EndInit();
      ((ISupportInitialize) this.gcAdd).EndInit();
      ((ISupportInitialize) this.gvAdd).EndInit();
      ((ISupportInitialize) this.gridView2).EndInit();
      ((ISupportInitialize) this.groupControl2).EndInit();
      ((Control) this.groupControl2).ResumeLayout(false);
      ((ISupportInitialize) this.groupControl1).EndInit();
      ((Control) this.groupControl1).ResumeLayout(false);
      ((Control) this).ResumeLayout(false);
    }
  }
}
