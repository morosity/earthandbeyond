﻿// Decompiled with JetBrains decompiler
// Type: OreEditor.Main
// Assembly: OreEditor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C0E9A1B-E579-45C0-B2C2-63BCDF738A55
// Assembly location: D:\Server\eab\Tools\OreEditor\OreEditor\OreEditor.exe

using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using OreEditor.GUI.Pages;
using OreEditor.MySQL;
using OreEditor.Properties;
using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace OreEditor
{
  public class Main : RibbonForm
  {
    public DataTable m_SectorList;
    private DataTable m_FieldList;
    private string m_OldPage;
    private FieldOreEdit pnFieldOreEdit;
    private SectorOreEdit pnSectorOreEdit;
    private IContainer components;
    private RibbonControl rRibbon;
    private RibbonPage pSector;
    private RibbonPageGroup ribbonPageGroup1;
    private RibbonPage pField;
    private PanelControl pPannel;
    private RepositoryItemLookUpEdit rItemSectorEdit;
    public BarEditItem eBarSector;
    private BarEditItem eBarField;
    private RepositoryItemLookUpEdit repositoryItemLookUpEdit1;
    private RibbonPageGroup ribbonPageGroup2;
    private BarButtonItem bSave;
    private BarButtonItem bBarReload;
    private RibbonPageGroup ribbonPageGroup3;
    private RibbonPageGroup ribbonPageGroup4;

    public Main()
    {
      base.\u002Ector();
      this.InitializeComponent();
    }

    private void Form1_Load(object sender, EventArgs e)
    {
      this.m_SectorList.Merge(SQL.ExecuteQuery("SELECT sectors.sector_id as sector_id,sectors.name as name FROM sectors", "net7"));
      ((RepositoryItemLookUpEditBase) this.rItemSectorEdit).set_DataSource((object) this.m_SectorList);
      ((RepositoryItemLookUpEditBase) this.rItemSectorEdit).set_ValueMember("sector_id");
      ((RepositoryItemLookUpEditBase) this.rItemSectorEdit).set_DisplayMember("name");
      ((Control) this.pPannel).Controls.Add((Control) this.pnFieldOreEdit);
      ((Control) this.pPannel).Controls.Add((Control) this.pnSectorOreEdit);
      ((Control) this.pnFieldOreEdit).Hide();
      ((Control) this.pnSectorOreEdit).Show();
      this.rRibbon.set_SelectedPage(this.rRibbon.get_Pages().get_Item(0));
      this.m_OldPage = this.rRibbon.get_SelectedPage().get_Name();
      this.pPannel_Resize((object) null, (EventArgs) null);
    }

    private void rRibbon_SelectedPageChanged(object sender, EventArgs e)
    {
      if (this.rRibbon.get_SelectedPage().get_Name() == this.m_OldPage)
        return;
      switch (this.m_OldPage)
      {
        case "pSector":
          ((Control) this.pnSectorOreEdit).Hide();
          break;
        case "pField":
          ((Control) this.pnFieldOreEdit).Hide();
          break;
      }
      switch (this.rRibbon.get_SelectedPage().get_Name())
      {
        case "pSector":
          ((Control) this.pnSectorOreEdit).Show();
          break;
        case "pField":
          ((Control) this.pnFieldOreEdit).Show();
          break;
      }
      this.m_OldPage = this.rRibbon.get_SelectedPage().get_Name();
    }

    private void pPannel_Resize(object sender, EventArgs e)
    {
      if (((Control) this.pPannel).Controls.Count <= 0)
        return;
      for (int index = 0; index < ((Control) this.pPannel).Controls.Count; ++index)
      {
        ((Control) this.pPannel).Controls[index].Height = ((Control) this.pPannel).Size.Height;
        ((Control) this.pPannel).Controls[index].Width = ((Control) this.pPannel).Size.Width;
      }
    }

    private void UpdateFields(int SectorID)
    {
      string Query = "SELECT sector_objects.name, sector_objects.sector_id, sector_objects_harvestable.resource_id FROM sector_objects_harvestable Inner Join sector_objects ON sector_objects_harvestable.resource_id = sector_objects.sector_object_id WHERE sector_objects.sector_id =  '" + SectorID.ToString() + "';";
      this.m_FieldList.Clear();
      this.m_FieldList.Merge(SQL.ExecuteQuery(Query, "net7"));
      ((RepositoryItemLookUpEditBase) this.repositoryItemLookUpEdit1).set_DataSource((object) this.m_FieldList);
      ((RepositoryItemLookUpEditBase) this.repositoryItemLookUpEdit1).set_DisplayMember("name");
      ((RepositoryItemLookUpEditBase) this.repositoryItemLookUpEdit1).set_ValueMember("resource_id");
    }

    private void eBarSector_EditValueChanged(object sender, EventArgs e)
    {
      this.pnSectorOreEdit.ChangeSector(int.Parse(this.eBarSector.get_EditValue().ToString()));
      this.UpdateFields(int.Parse(this.eBarSector.get_EditValue().ToString()));
    }

    private void bBarField_EditValueChanged(object sender, EventArgs e)
    {
      this.pnFieldOreEdit.ChangeResourceID(int.Parse(this.eBarField.get_EditValue().ToString()));
    }

    private void bSave_ItemClick(object sender, ItemClickEventArgs e)
    {
      if (MessageBox.Show("Are you sure you want to save the data?", "Saveing", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) != DialogResult.Yes)
        return;
      switch (this.rRibbon.get_SelectedPage().get_Name())
      {
        case "pSector":
          this.pnSectorOreEdit.SaveData();
          break;
        case "pField":
          this.pnFieldOreEdit.SaveData();
          break;
      }
    }

    private void bBarReload_ItemClick(object sender, ItemClickEventArgs e)
    {
      if (MessageBox.Show("Are you sure you want to reload the data? You could loose unsaved work.", "Reloading", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) != DialogResult.Yes)
        return;
      switch (this.rRibbon.get_SelectedPage().get_Name())
      {
        case "pSector":
          if (this.eBarSector.get_EditValue() == null)
            break;
          this.UpdateFields(int.Parse(this.eBarSector.get_EditValue().ToString()));
          this.pnSectorOreEdit.ReloadData();
          break;
        case "pField":
          this.pnFieldOreEdit.ReloadData();
          break;
      }
    }

    protected virtual void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      ((XtraForm) this).Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.rRibbon = new RibbonControl();
      this.eBarSector = new BarEditItem();
      this.rItemSectorEdit = new RepositoryItemLookUpEdit();
      this.eBarField = new BarEditItem();
      this.repositoryItemLookUpEdit1 = new RepositoryItemLookUpEdit();
      this.bSave = new BarButtonItem();
      this.bBarReload = new BarButtonItem();
      this.pSector = new RibbonPage();
      this.ribbonPageGroup1 = new RibbonPageGroup();
      this.ribbonPageGroup3 = new RibbonPageGroup();
      this.pField = new RibbonPage();
      this.ribbonPageGroup2 = new RibbonPageGroup();
      this.ribbonPageGroup4 = new RibbonPageGroup();
      this.pPannel = new PanelControl();
      ((ISupportInitialize) this.rRibbon).BeginInit();
      ((ISupportInitialize) this.rItemSectorEdit).BeginInit();
      ((ISupportInitialize) this.repositoryItemLookUpEdit1).BeginInit();
      ((ISupportInitialize) this.pPannel).BeginInit();
      ((XtraForm) this).SuspendLayout();
      this.rRibbon.set_ApplicationIcon((Bitmap) null);
      ((BarItems) this.rRibbon.get_Items()).AddRange(new BarItem[4]
      {
        (BarItem) this.eBarSector,
        (BarItem) this.eBarField,
        (BarItem) this.bSave,
        (BarItem) this.bBarReload
      });
      ((Control) this.rRibbon).Location = new Point(0, 0);
      this.rRibbon.set_MaxItemId(5);
      ((Control) this.rRibbon).Name = "rRibbon";
      this.rRibbon.get_Pages().AddRange(new RibbonPage[2]
      {
        this.pSector,
        this.pField
      });
      this.rRibbon.get_RepositoryItems().AddRange(new RepositoryItem[2]
      {
        (RepositoryItem) this.rItemSectorEdit,
        (RepositoryItem) this.repositoryItemLookUpEdit1
      });
      this.rRibbon.set_SelectedPage(this.pField);
      ((Control) this.rRibbon).Size = new Size(553, 143);
      this.rRibbon.add_SelectedPageChanged(new EventHandler(this.rRibbon_SelectedPageChanged));
      ((BarItem) this.eBarSector).set_Caption("Sector");
      this.eBarSector.set_Edit((RepositoryItem) this.rItemSectorEdit);
      ((BarItem) this.eBarSector).set_Id(1);
      ((BarItem) this.eBarSector).set_Name("eBarSector");
      ((BarItem) this.eBarSector).set_Width(250);
      this.eBarSector.add_EditValueChanged(new EventHandler(this.eBarSector_EditValueChanged));
      ((RepositoryItem) this.rItemSectorEdit).set_AutoHeight(false);
      ((RepositoryItemButtonEdit) this.rItemSectorEdit).get_Buttons().AddRange(new EditorButton[1]
      {
        new EditorButton((ButtonPredefines) -5)
      });
      ((RepositoryItem) this.rItemSectorEdit).set_Name("rItemSectorEdit");
      ((BarItem) this.eBarField).set_Caption("Field   ");
      this.eBarField.set_Edit((RepositoryItem) this.repositoryItemLookUpEdit1);
      ((BarItem) this.eBarField).set_Id(2);
      ((BarItem) this.eBarField).set_Name("eBarField");
      ((BarItem) this.eBarField).set_Width(250);
      this.eBarField.add_EditValueChanged(new EventHandler(this.bBarField_EditValueChanged));
      ((RepositoryItem) this.repositoryItemLookUpEdit1).set_AutoHeight(false);
      ((RepositoryItemButtonEdit) this.repositoryItemLookUpEdit1).get_Buttons().AddRange(new EditorButton[1]
      {
        new EditorButton((ButtonPredefines) -5)
      });
      ((RepositoryItem) this.repositoryItemLookUpEdit1).set_Name("repositoryItemLookUpEdit1");
      ((BarItem) this.bSave).set_Caption("Save");
      ((BarItem) this.bSave).set_Glyph((Image) Resources.fileexport);
      ((BarItem) this.bSave).set_Id(3);
      ((BarItem) this.bSave).set_Name("bSave");
      ((BarItem) this.bSave).set_RibbonStyle((RibbonItemStyles) 1);
      // ISSUE: method pointer
      ((BarItem) this.bSave).add_ItemClick(new ItemClickEventHandler((object) this, __methodptr(bSave_ItemClick)));
      ((BarItem) this.bBarReload).set_Caption("Reload");
      ((BarItem) this.bBarReload).set_Glyph((Image) Resources.reload_page);
      ((BarItem) this.bBarReload).set_Id(4);
      ((BarItem) this.bBarReload).set_Name("bBarReload");
      ((BarItem) this.bBarReload).set_RibbonStyle((RibbonItemStyles) 1);
      // ISSUE: method pointer
      ((BarItem) this.bBarReload).add_ItemClick(new ItemClickEventHandler((object) this, __methodptr(bBarReload_ItemClick)));
      this.pSector.get_Groups().AddRange(new RibbonPageGroup[2]
      {
        this.ribbonPageGroup1,
        this.ribbonPageGroup3
      });
      this.pSector.set_Name("pSector");
      this.pSector.set_Text("Sector");
      ((BarItemLinkCollection) this.ribbonPageGroup1.get_ItemLinks()).Add((BarItem) this.eBarSector);
      this.ribbonPageGroup1.set_Name("ribbonPageGroup1");
      this.ribbonPageGroup1.set_Text("Select Sector");
      ((BarItemLinkCollection) this.ribbonPageGroup3.get_ItemLinks()).Add((BarItem) this.bSave);
      ((BarItemLinkCollection) this.ribbonPageGroup3.get_ItemLinks()).Add((BarItem) this.bBarReload, true);
      this.ribbonPageGroup3.set_Name("ribbonPageGroup3");
      this.ribbonPageGroup3.set_Text("Save");
      this.pField.get_Groups().AddRange(new RibbonPageGroup[2]
      {
        this.ribbonPageGroup2,
        this.ribbonPageGroup4
      });
      this.pField.set_Name("pField");
      this.pField.set_Text("Field");
      ((BarItemLinkCollection) this.ribbonPageGroup2.get_ItemLinks()).Add((BarItem) this.eBarSector);
      ((BarItemLinkCollection) this.ribbonPageGroup2.get_ItemLinks()).Add((BarItem) this.eBarField);
      this.ribbonPageGroup2.set_Name("ribbonPageGroup2");
      this.ribbonPageGroup2.set_Text("Select Sector and Field");
      ((BarItemLinkCollection) this.ribbonPageGroup4.get_ItemLinks()).Add((BarItem) this.bSave);
      ((BarItemLinkCollection) this.ribbonPageGroup4.get_ItemLinks()).Add((BarItem) this.bBarReload, true);
      this.ribbonPageGroup4.set_Name("ribbonPageGroup4");
      this.ribbonPageGroup4.set_Text("Save");
      ((Control) this.pPannel).Dock = DockStyle.Fill;
      ((Control) this.pPannel).Location = new Point(0, 143);
      ((Control) this.pPannel).Name = "pPannel";
      ((Control) this.pPannel).Size = new Size(553, 391);
      ((Control) this.pPannel).TabIndex = 3;
      ((Control) this.pPannel).Resize += new EventHandler(this.pPannel_Resize);
      ((ContainerControl) this).AutoScaleDimensions = new SizeF(6f, 13f);
      ((ContainerControl) this).AutoScaleMode = AutoScaleMode.Font;
      ((Form) this).ClientSize = new Size(553, 534);
      ((Control) this).Controls.Add((Control) this.pPannel);
      ((Control) this).Controls.Add((Control) this.rRibbon);
      ((Control) this).MaximumSize = new Size(561, 538);
      ((Control) this).Name = nameof (Main);
      this.set_Ribbon(this.rRibbon);
      ((Control) this).Text = "Ore Editor";
      ((Form) this).Load += new EventHandler(this.Form1_Load);
      ((ISupportInitialize) this.rRibbon).EndInit();
      ((ISupportInitialize) this.rItemSectorEdit).EndInit();
      ((ISupportInitialize) this.repositoryItemLookUpEdit1).EndInit();
      ((ISupportInitialize) this.pPannel).EndInit();
      ((XtraForm) this).ResumeLayout(false);
    }
  }
}
