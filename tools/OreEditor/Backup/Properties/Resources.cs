﻿// Decompiled with JetBrains decompiler
// Type: OreEditor.Properties.Resources
// Assembly: OreEditor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C0E9A1B-E579-45C0-B2C2-63BCDF738A55
// Assembly location: D:\Server\eab\Tools\OreEditor\OreEditor\OreEditor.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace OreEditor.Properties
{
  [CompilerGenerated]
  [DebuggerNonUserCode]
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "2.0.0.0")]
  internal class Resources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal Resources()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (object.ReferenceEquals((object) OreEditor.Properties.Resources.resourceMan, (object) null))
          OreEditor.Properties.Resources.resourceMan = new ResourceManager("OreEditor.Properties.Resources", typeof (OreEditor.Properties.Resources).Assembly);
        return OreEditor.Properties.Resources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return OreEditor.Properties.Resources.resourceCulture;
      }
      set
      {
        OreEditor.Properties.Resources.resourceCulture = value;
      }
    }

    internal static Bitmap fileexport
    {
      get
      {
        return (Bitmap) OreEditor.Properties.Resources.ResourceManager.GetObject(nameof (fileexport), OreEditor.Properties.Resources.resourceCulture);
      }
    }

    internal static byte[] MySql_Data
    {
      get
      {
        return (byte[]) OreEditor.Properties.Resources.ResourceManager.GetObject(nameof (MySql_Data), OreEditor.Properties.Resources.resourceCulture);
      }
    }

    internal static Bitmap reload_page
    {
      get
      {
        return (Bitmap) OreEditor.Properties.Resources.ResourceManager.GetObject(nameof (reload_page), OreEditor.Properties.Resources.resourceCulture);
      }
    }

    internal static byte[] ToolAuthDLL
    {
      get
      {
        return (byte[]) OreEditor.Properties.Resources.ResourceManager.GetObject(nameof (ToolAuthDLL), OreEditor.Properties.Resources.resourceCulture);
      }
    }
  }
}
