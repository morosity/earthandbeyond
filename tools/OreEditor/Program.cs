﻿// Decompiled with JetBrains decompiler
// Type: OreEditor.Program
// Assembly: OreEditor, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 9C0E9A1B-E579-45C0-B2C2-63BCDF738A55
// Assembly location: D:\Server\eab\Tools\OreEditor\OreEditor\OreEditor.exe

using OreEditor.GUI;
using OreEditor.MySQL;
using System;
using System.Data;
using System.Windows.Forms;

namespace OreEditor
{
  internal static class Program
  {
    public static Main mProgram;

    [STAThread]
    private static void Main()
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      try
      {
        DataSet dataSet = new DataSet();
        int num = (int) dataSet.ReadXml(Application.StartupPath + "\\Config.xml");
        DataRow row = dataSet.Tables[0].Rows[0];
        SQL.Host = row.ItemArray[0].ToString();
        SQL.Port = int.Parse(row.ItemArray[1].ToString());
        SQL.User = row.ItemArray[2].ToString();
        SQL.Pass = row.ItemArray[3].ToString();
      }
      catch (Exception ex)
      {
        SQL.Host = "dev.net-7.org";
        SQL.Port = 3307;
        SQL.User = "";
        SQL.Pass = "";
      }
      Login login = new Login();
      login.LoginUsername.Text = SQL.User;
      login.LoginPassword.Text = SQL.Pass;
      login.SQLServer.Text = SQL.Host;
      login.SQLPort.Text = SQL.Port.ToString();
      int num1 = (int) login.ShowDialog();
      if (login.m_Cancel)
        return;
      SQL.Connect();
      Program.mProgram = new Main();
      Application.Run(Program.mProgram);
    }
  }
}
