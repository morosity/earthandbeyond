﻿// Decompiled with JetBrains decompiler
// Type: MissionEditor.Database.Database
// Assembly: MissionEditor, Version=1.6.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 00D14066-6848-4104-A662-B9B4BF67FB6E
// Assembly location: D:\Server\eab\Tools\MissionEditor.exe

using CommonTools;
using CommonTools.Database;
using Singleton;
using System;
using System.Data;
using System.IO;
using System.Text;
using System.Xml;

namespace MissionEditor.Database
{
  public class Database
  {
    public const string LOG_FILE = "missions.xml";

    public static string getFirstMissionId()
    {
      DataTable dataTable = Get<DB>.Instance.executeQuery("SELECT " + ColumnData.GetName((Enum) Net7.Table_missions._mission_id) + " FROM " + Net7.Tables.missions.ToString() + " LIMIT 1", (string[]) null, (string[]) null);
      if (dataTable.Rows.Count != 0)
        return ColumnData.GetString(dataTable.Rows[0], (Enum) Net7.Table_missions._mission_id);
      return (string) null;
    }

    public static Mission getMission(string id)
    {
      Mission mission = (Mission) null;
      string str = nameof (id);
      DataTable dataTable = Get<DB>.Instance.executeQuery("SELECT * FROM " + (object) Net7.Tables.missions + " WHERE " + ColumnData.GetName((Enum) Net7.Table_missions._mission_id) + " = ?" + str, new string[1]
      {
        str
      }, new string[1]{ id });
      if (dataTable.Rows.Count == 1)
      {
        mission = new Mission();
        mission.setId(id);
        SecLevels enumValue1;
        Enumeration.TryParse<SecLevels>(ColumnData.GetString(dataTable.Rows[0], (Enum) Net7.Table_missions._mission_MinSecLvl), out enumValue1);
        mission.setMinSecLvl(enumValue1);
        mission.setXml(ColumnData.GetString(dataTable.Rows[0], (Enum) Net7.Table_missions._mission_XML));
        mission.setName(ColumnData.GetString(dataTable.Rows[0], (Enum) Net7.Table_missions._mission_name));
        MissionType enumValue2;
        Enumeration.TryParse<MissionType>(ColumnData.GetString(dataTable.Rows[0], (Enum) Net7.Table_missions._mission_type), out enumValue2);
        mission.setType(enumValue2);
        mission.setKey(ColumnData.GetString(dataTable.Rows[0], (Enum) Net7.Table_missions._mission_key));
      }
      return mission;
    }

    public static string getNextMissionId()
    {
      DataTable dataTable = Get<DB>.Instance.executeQuery("SELECT MAX(" + ColumnData.GetName((Enum) Net7.Table_missions._mission_id) + ") FROM " + (object) Net7.Tables.missions, (string[]) null, (string[]) null);
      int result = 1;
      if (dataTable.Rows.Count == 1 && dataTable.Rows[0] != null)
      {
        int.TryParse(dataTable.Rows[0][0].ToString(), out result);
        ++result;
      }
      return result.ToString();
    }

    public static void setMission(Mission mission, bool newMission)
    {
      string[] parameter = new string[6]
      {
        "id",
        "xml",
        "name",
        "key",
        "type",
        "MinSecLvl"
      };
      string[] strArray = new string[6]
      {
        mission.getId(),
        mission.getXML(),
        mission.getName(),
        mission.getKey(),
        ((int) mission.getType()).ToString(),
        ((int) mission.getMinSecLvl()).ToString()
      };
      string query;
      if (newMission)
      {
        MissionEditor.Database.Database.writeXmlLog(mission, MissionEditor.Database.Database.LogAction.Add);
        DataTable dataTable = Get<DB>.Instance.executeQuery("INSERT INTO " + (object) Net7.Tables.missions + " ( " + ColumnData.GetName((Enum) Net7.Table_missions._mission_XML) + "," + ColumnData.GetName((Enum) Net7.Table_missions._mission_name) + "," + ColumnData.GetName((Enum) Net7.Table_missions._mission_key) + "," + ColumnData.GetName((Enum) Net7.Table_missions._mission_type) + "," + ColumnData.GetName((Enum) Net7.Table_missions._mission_MinSecLvl) + " ) VALUES (?" + parameter[1] + ",?" + parameter[2] + ",?" + parameter[3] + ",?" + parameter[4] + ",?" + parameter[5] + "); SELECT " + ColumnData.GetName((Enum) Net7.Table_missions._mission_id) + " FROM " + (object) Net7.Tables.missions + " WHERE " + ColumnData.GetName((Enum) Net7.Table_missions._mission_id) + " IS NULL;", parameter, strArray);
        mission.setId(ColumnData.GetString(dataTable.Rows[0], (Enum) Net7.Table_missions._mission_id));
        strArray[0] = mission.getId();
        strArray[1] = mission.getXML();
        query = "UPDATE " + (object) Net7.Tables.missions + " SET " + ColumnData.GetName((Enum) Net7.Table_missions._mission_XML) + " = ?" + parameter[1] + " WHERE " + ColumnData.GetName((Enum) Net7.Table_missions._mission_id) + " = ?" + parameter[0];
      }
      else
      {
        MissionEditor.Database.Database.writeXmlLog(mission, MissionEditor.Database.Database.LogAction.Edit);
        query = "UPDATE " + (object) Net7.Tables.missions + " SET " + ColumnData.GetName((Enum) Net7.Table_missions._mission_XML) + " = ?" + parameter[1] + "," + ColumnData.GetName((Enum) Net7.Table_missions._mission_name) + " = ?" + parameter[2] + "," + ColumnData.GetName((Enum) Net7.Table_missions._mission_key) + " = ?" + parameter[3] + "," + ColumnData.GetName((Enum) Net7.Table_missions._mission_type) + " = ?" + parameter[4] + "," + ColumnData.GetName((Enum) Net7.Table_missions._mission_MinSecLvl) + " = ?" + parameter[5] + " WHERE " + ColumnData.GetName((Enum) Net7.Table_missions._mission_id) + " = ?" + parameter[0];
      }
      Get<DB>.Instance.executeQuery(query, parameter, strArray);
    }

    public static void deleteMission(Mission mission)
    {
      MissionEditor.Database.Database.writeXmlLog(mission, MissionEditor.Database.Database.LogAction.Delete);
      string str = "id";
      Get<DB>.Instance.executeQuery("DELETE FROM " + (object) Net7.Tables.missions + " WHERE " + ColumnData.GetName((Enum) Net7.Table_missions._mission_id) + " = ?" + str, new string[1]
      {
        str
      }, new string[1]{ mission.getId() });
    }

    private static void writeXmlLog(Mission mission, MissionEditor.Database.Database.LogAction logAction)
    {
      if (!File.Exists("missions.xml"))
      {
        XmlTextWriter xmlTextWriter = new XmlTextWriter("missions.xml", Encoding.Unicode);
        xmlTextWriter.WriteStartElement(MissionEditor.Database.Database.LogXmlTag.Missions.ToString());
        xmlTextWriter.WriteEndElement();
        xmlTextWriter.Close();
      }
      DateTime now = DateTime.Now;
      XmlDocument xmlDocument = new XmlDocument();
      xmlDocument.Load("missions.xml");
      XmlElement element = xmlDocument.CreateElement(MissionEditor.Database.Database.LogXmlTag.Mission.ToString());
      element.SetAttribute(MissionEditor.Database.Database.LogXmlTag.Date.ToString(), now.ToString("yyyy/MM/dd"));
      element.SetAttribute(MissionEditor.Database.Database.LogXmlTag.Time.ToString(), now.ToString("HH:mm:ss"));
      element.SetAttribute(MissionEditor.Database.Database.LogXmlTag.Action.ToString(), logAction.ToString());
      element.SetAttribute(MissionEditor.Database.Database.LogXmlTag.Id.ToString(), mission.getId());
      element.SetAttribute(MissionEditor.Database.Database.LogXmlTag.Name.ToString(), mission.getName());
      element.SetAttribute(MissionEditor.Database.Database.LogXmlTag.Type.ToString(), mission.getType().ToString());
      element.SetAttribute(MissionEditor.Database.Database.LogXmlTag.Key.ToString(), mission.getKey());
      XmlText textNode = xmlDocument.CreateTextNode(mission.getXML());
      element.AppendChild((XmlNode) textNode);
      xmlDocument.DocumentElement.AppendChild((XmlNode) element);
      xmlDocument.Save("missions.xml");
    }

    private static void readXmlLog()
    {
      using (FileStream fileStream = File.OpenRead("log.xml"))
      {
        XmlParserContext context = new XmlParserContext((XmlNameTable) new NameTable(), (XmlNamespaceManager) null, (string) null, XmlSpace.Default);
        XmlTextReader xmlTextReader = new XmlTextReader((Stream) fileStream, XmlNodeType.Element, context);
        while (xmlTextReader.Read())
        {
          if (xmlTextReader.NodeType == XmlNodeType.Element)
            Console.WriteLine("Element: {0}, Value: {1}", (object) xmlTextReader.Name, (object) xmlTextReader.ReadElementString());
        }
      }
    }

    private enum LogAction
    {
      Add,
      Delete,
      Edit,
    }

    private enum LogXmlTag
    {
      Missions,
      Date,
      Time,
      Action,
      Mission,
      Id,
      Xml,
      Name,
      Type,
      Key,
    }
  }
}
