﻿// Decompiled with JetBrains decompiler
// Type: MissionEditor.Database.DataConfiguration
// Assembly: MissionEditor, Version=1.6.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 00D14066-6848-4104-A662-B9B4BF67FB6E
// Assembly location: D:\Server\eab\Tools\MissionEditor.exe

using CommonTools.Database;
using CommonTools.Gui;
using Singleton;
using System;
using System.Collections.Generic;
using System.Data;

namespace MissionEditor.Database
{
  public class DataConfiguration
  {
    private static DlgSearch m_dlgSearch = new DlgSearch();
    private static List<DataConfiguration.DataValidation> listDataValidations = new List<DataConfiguration.DataValidation>();
    private Net7.Tables m_table;
    private Enum m_id;
    private Enum[] m_description;
    private static Dictionary<DataConfiguration.DataType, DataConfiguration> m_DataConfigurations;

    public static void init()
    {
      DataConfiguration.m_DataConfigurations = new Dictionary<DataConfiguration.DataType, DataConfiguration>();
      DataConfiguration.m_DataConfigurations.Add(DataConfiguration.DataType.faction, new DataConfiguration(Net7.Tables.factions, (Enum) Net7.Table_factions._faction_id, new Enum[1]
      {
        (Enum) Net7.Table_factions._name
      }));
      DataConfiguration.m_DataConfigurations.Add(DataConfiguration.DataType.item, new DataConfiguration(Net7.Tables.item_base, (Enum) Net7.Table_item_base._id, new Enum[1]
      {
        (Enum) Net7.Table_item_base._name
      }));
      DataConfiguration.m_DataConfigurations.Add(DataConfiguration.DataType.mob, new DataConfiguration(Net7.Tables.mob_base, (Enum) Net7.Table_mob_base._mob_id, new Enum[1]
      {
        (Enum) Net7.Table_mob_base._name
      }));
      DataConfiguration.m_DataConfigurations.Add(DataConfiguration.DataType.sector_object, new DataConfiguration(Net7.Tables.sector_objects, (Enum) Net7.Table_sector_objects._sector_object_id, new Enum[1]
      {
        (Enum) Net7.Table_sector_objects._name
      }));
      DataConfiguration.m_DataConfigurations.Add(DataConfiguration.DataType.npc, new DataConfiguration(Net7.Tables.starbase_npcs, (Enum) Net7.Table_starbase_npcs._npc_Id, new Enum[2]
      {
        (Enum) Net7.Table_starbase_npcs._first_name,
        (Enum) Net7.Table_starbase_npcs._last_name
      }));
      DataConfiguration.m_DataConfigurations.Add(DataConfiguration.DataType.sector, new DataConfiguration(Net7.Tables.sectors, (Enum) Net7.Table_sectors._sector_id, new Enum[1]
      {
        (Enum) Net7.Table_sectors._name
      }));
      DataConfiguration.m_DataConfigurations.Add(DataConfiguration.DataType.skill, new DataConfiguration(Net7.Tables.skills, (Enum) Net7.Table_skills._skill_id, new Enum[1]
      {
        (Enum) Net7.Table_skills._name
      }));
      DataConfiguration.m_DataConfigurations.Add(DataConfiguration.DataType.mission, new DataConfiguration(Net7.Tables.missions, (Enum) Net7.Table_missions._mission_id, new Enum[1]
      {
        (Enum) Net7.Table_missions._mission_name
      }));
    }

    public static bool isValid(DataConfiguration.DataType dataType, string code)
    {
      DataConfiguration dataConfiguration;
      if (DataConfiguration.m_DataConfigurations.TryGetValue(dataType, out dataConfiguration))
        return dataConfiguration.isValid(code);
      throw new Exception("Unable to convert '" + dataType.ToString() + "' into a DataConfiguration.DataType");
    }

    public static void addValidation(DataConfiguration.DataType dataType, string code)
    {
      DataConfiguration dataConfiguration;
      if (!DataConfiguration.m_DataConfigurations.TryGetValue(dataType, out dataConfiguration))
        throw new Exception("Unable to convert '" + dataType.ToString() + "' into a DataConfiguration.DataType");
      string errorMessage = "The " + dataType.ToString() + " code '" + code + "' does not exist";
      DataConfiguration.listDataValidations.Add(new DataConfiguration.DataValidation(dataConfiguration, code, errorMessage));
    }

    public static void addValidation(string errorMessage)
    {
      DataConfiguration.listDataValidations.Add(new DataConfiguration.DataValidation((DataConfiguration) null, (string) null, errorMessage));
    }

    public static bool validate(out string errorMessage)
    {
      errorMessage = "";
      string query = "";
      int num = 0;
      List<string> stringList1 = new List<string>();
      List<string> stringList2 = new List<string>();
      List<DataConfiguration.DataValidation> dataValidationList = new List<DataConfiguration.DataValidation>();
      foreach (DataConfiguration.DataValidation listDataValidation in DataConfiguration.listDataValidations)
      {
        if (listDataValidation.dataConfiguration == null && listDataValidation.code == null)
        {
          errorMessage = listDataValidation.errorMessage;
          break;
        }
        string str = listDataValidation.dataConfiguration.m_id.ToString() + num.ToString();
        if (query.Length != 0)
          query += " UNION ";
        query = query + "SELECT " + num.ToString() + "," + ColumnData.GetName(listDataValidation.dataConfiguration.m_id) + " FROM " + (object) listDataValidation.dataConfiguration.m_table + " WHERE " + ColumnData.GetName(listDataValidation.dataConfiguration.m_id) + " = ?" + str;
        stringList1.Add(str);
        stringList2.Add(listDataValidation.code);
        dataValidationList.Add(listDataValidation);
        ++num;
      }
      if (errorMessage.Length == 0 && query.Length != 0)
      {
        DataTable dataTable = Get<DB>.Instance.executeQuery(query, stringList1.ToArray(), stringList2.ToArray());
        for (int index = 0; index < dataValidationList.Count; ++index)
        {
          if (dataTable.Rows.Count <= index || !dataTable.Rows[index][0].ToString().Equals(index.ToString()))
          {
            errorMessage = dataValidationList[index].errorMessage;
            break;
          }
        }
      }
      DataConfiguration.listDataValidations.Clear();
      return errorMessage.Length == 0;
    }

    public static string search(DataConfiguration.DataType dataType)
    {
      DataConfiguration dataConfiguration;
      if (!DataConfiguration.m_DataConfigurations.TryGetValue(dataType, out dataConfiguration))
        throw new Exception("Unable to convert '" + dataType.ToString() + "' into a DataConfiguration.DataType");
      DataConfiguration.m_dlgSearch.configure(dataConfiguration.m_table);
      int num = (int) DataConfiguration.m_dlgSearch.ShowDialog();
      return DataConfiguration.m_dlgSearch.getSelectedId();
    }

    public static string getDescription(DataConfiguration.DataType dataType, string id)
    {
      DataConfiguration dataConfiguration;
      if (!DataConfiguration.m_DataConfigurations.TryGetValue(dataType, out dataConfiguration))
        throw new Exception("Unable to convert '" + dataType.ToString() + "' into a DataConfiguration.DataType");
      string str1 = "";
      foreach (Enum enumValue in dataConfiguration.m_description)
        str1 = (str1.Length != 0 ? str1 + "," : str1 + "SELECT ") + ColumnData.GetName(enumValue);
      string str2 = nameof (id);
      DataTable dataTable = Get<DB>.Instance.executeQuery(str1 + " FROM " + dataConfiguration.m_table.ToString() + " WHERE " + ColumnData.GetName(dataConfiguration.m_id) + " = ?" + str2, new string[1]
      {
        str2
      }, new string[1]{ id });
      string str3 = "";
      if (dataTable.Rows.Count == 1)
      {
        foreach (DataColumn column in (InternalDataCollectionBase) dataTable.Columns)
        {
          if (str3.Length != 0)
            str3 += " ";
          str3 += dataTable.Rows[0][column].ToString();
        }
      }
      return str3 + " (" + id + ")";
    }

    public DataConfiguration(Net7.Tables table, Enum id, Enum[] description)
    {
      this.m_table = table;
      this.m_id = id;
      this.m_description = description;
    }

    public bool isValid(string code)
    {
      return Get<DB>.Instance.select(new Enum[1]
      {
        this.m_id
      }, this.m_table, this.m_id, code).Rows.Count != 0;
    }

    public enum DataType
    {
      faction,
      item,
      mob,
      sector_object,
      npc,
      sector,
      skill,
      mission,
    }

    private class DataValidation
    {
      public DataConfiguration dataConfiguration;
      public string code;
      public string errorMessage;

      public DataValidation(DataConfiguration dataConfiguration, string code, string errorMessage)
      {
        this.dataConfiguration = dataConfiguration;
        this.code = code;
        this.errorMessage = errorMessage;
      }
    }
  }
}
