﻿// Decompiled with JetBrains decompiler
// Type: MissionEditor.Database.CodeDescSearch
// Assembly: MissionEditor, Version=1.6.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 00D14066-6848-4104-A662-B9B4BF67FB6E
// Assembly location: D:\Server\eab\Tools\MissionEditor.exe

using System;
using System.Windows.Forms;

namespace MissionEditor.Database
{
  internal class CodeDescSearch
  {
    private DataConfiguration.DataType m_dataType;
    private TextBox m_code;
    private TextBox m_description;
    private Button m_search;

    public CodeDescSearch(TextBox code, TextBox description, Button search)
    {
      this.m_code = code;
      this.m_code.Leave += new EventHandler(this.onLeaveId);
      this.m_description = description;
      this.m_search = search;
      this.m_search.Click += new EventHandler(this.onSearch);
    }

    public void setDataType(DataConfiguration.DataType dataType)
    {
      this.m_dataType = dataType;
    }

    private void onLeaveId(object sender, EventArgs e)
    {
      this.setDescription();
    }

    private void onSearch(object sender, EventArgs e)
    {
      string str = DataConfiguration.search(this.m_dataType);
      if (str.Length == 0)
        return;
      this.m_code.Text = str;
      this.setDescription();
    }

    private void setDescription()
    {
      this.m_description.Text = DataConfiguration.getDescription(this.m_dataType, this.m_code.Text);
    }
  }
}
