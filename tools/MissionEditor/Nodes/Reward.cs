﻿// Decompiled with JetBrains decompiler
// Type: MissionEditor.Nodes.Reward
// Assembly: MissionEditor, Version=1.6.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 00D14066-6848-4104-A662-B9B4BF67FB6E
// Assembly location: D:\Server\eab\Tools\MissionEditor.exe

using CommonTools;
using MissionEditor.Database;
using System;
using System.IO;
using System.Xml;

namespace MissionEditor.Nodes
{
  public class Reward
  {
    private RewardType m_rewardType;
    private string m_value;
    private string m_flag;
    private string m_char_data;

    public Reward()
    {
      this.clear();
    }

    public void clear()
    {
      this.m_rewardType = RewardType.Credits;
      this.m_value = "";
      this.m_flag = "";
      this.m_char_data = "";
    }

    public void setRewardType(RewardType rewardType)
    {
      this.m_rewardType = rewardType;
    }

    public RewardType getRewardType()
    {
      return this.m_rewardType;
    }

    public void setValue(string value)
    {
      this.m_value = value;
    }

    public string getValue()
    {
      return this.m_value;
    }

    public string getCharData()
    {
      return this.m_char_data;
    }

    public void setCharData(string value)
    {
      this.m_char_data = value;
    }

    public string getFormattedValue()
    {
      switch (this.m_rewardType)
      {
        case RewardType.Faction:
          return DataConfiguration.getDescription(DataConfiguration.DataType.faction, this.getFlag());
        case RewardType.Item_ID:
          return DataConfiguration.getDescription(DataConfiguration.DataType.item, this.getValue());
        case RewardType.Award_Skill:
          return DataConfiguration.getDescription(DataConfiguration.DataType.skill, this.getValue());
        default:
          return this.m_value;
      }
    }

    public void setFlag(string flag)
    {
      this.m_flag = flag;
    }

    public bool hasFlag()
    {
      if (this.m_flag != null)
        return this.m_flag.Length != 0;
      return false;
    }

    public string getFlag()
    {
      return this.m_flag;
    }

    public void fromXml(XmlNode xmlNode)
    {
      string flag;
      CommonTools.Xml.getAttribute(xmlNode, "ID", true, out flag);
      RewardType enumValue;
      if (!Enumeration.TryParse<RewardType>(flag, out enumValue))
        throw new Exception("Unable to convert '" + flag + "' into a RewardType\n\n" + xmlNode.InnerXml);
      this.setRewardType(enumValue);
      switch (enumValue)
      {
        case RewardType.Credits:
        case RewardType.Explore_XP:
        case RewardType.Combat_XP:
        case RewardType.Trade_XP:
        case RewardType.Faction:
          int num1;
          CommonTools.Xml.getValue(xmlNode, true, out num1);
          this.setValue(num1.ToString());
          break;
        case RewardType.Item_ID:
        case RewardType.Award_Skill:
        case RewardType.Advance_Mission:
        case RewardType.Item_Blueprint:
          CommonTools.Xml.getValue(xmlNode, true, out flag);
          this.setValue(flag);
          break;
        case RewardType.Hull_Upgrade:
          int num2;
          CommonTools.Xml.getValue(xmlNode, true, out num2);
          this.setValue(num2.ToString());
          break;
        case RewardType.Run_Script:
          CommonTools.Xml.getValue(xmlNode, true, out flag);
          this.setValue(flag);
          break;
        case RewardType.Play_Sound_Reward:
          CommonTools.Xml.getValue(xmlNode, true, out flag);
          this.setValue(flag);
          break;
      }
      if (CommonTools.Xml.getAttribute(xmlNode, "Flags", false, out flag))
        this.setFlag(flag);
      this.addValidations();
    }

    public void getXML(StringWriter stringWriter)
    {
      stringWriter.WriteLine("<Reward" + CommonTools.Xml.attribute("ID", ((int) this.m_rewardType).ToString()) + (this.hasFlag() ? CommonTools.Xml.attribute("Flags", this.getFlag()) : "") + CommonTools.Xml.tagEnd() + this.m_value.ToString() + CommonTools.Xml.tagEnd(nameof (Reward)));
    }

    public void addValidations()
    {
      if (this.m_value == null || this.m_value.Length == 0)
      {
        DataConfiguration.addValidation("The reward value is mandatory");
      }
      else
      {
        switch (this.getRewardType())
        {
          case RewardType.Credits:
          case RewardType.Explore_XP:
          case RewardType.Combat_XP:
          case RewardType.Trade_XP:
            int result1;
            if (int.TryParse(this.getValue(), out result1) && result1 >= 0 && result1 <= 500000)
              break;
            DataConfiguration.addValidation("The reward quantity '" + this.getValue() + "' must be between 1 and 500,000");
            break;
          case RewardType.Faction:
            int result2;
            if (!int.TryParse(this.getValue(), out result2) || result2 < 0 || result2 > 500000)
              DataConfiguration.addValidation("The reward quantity '" + this.getValue() + "' must be between 1 and 500,000");
            DataConfiguration.addValidation(DataConfiguration.DataType.faction, this.getFlag());
            break;
          case RewardType.Item_ID:
            DataConfiguration.addValidation(DataConfiguration.DataType.item, this.getValue());
            break;
          case RewardType.Hull_Upgrade:
            int result3;
            if (int.TryParse(this.getValue(), out result3))
            {
              switch (result3)
              {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                  return;
              }
            }
            DataConfiguration.addValidation("The quantity '" + this.getValue() + "' must be between 1 and 6");
            break;
          case RewardType.Advance_Mission:
            DataConfiguration.addValidation(DataConfiguration.DataType.mission, this.getValue());
            break;
        }
      }
    }

    internal void getReport(StringWriter stringWriter)
    {
      stringWriter.WriteLine("<tr><td>" + this.m_rewardType.ToString() + "</td>");
      stringWriter.WriteLine("<td>" + this.getFormattedValue() + "</td></tr>");
    }
  }
}
