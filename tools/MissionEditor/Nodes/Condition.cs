﻿// Decompiled with JetBrains decompiler
// Type: MissionEditor.Nodes.Condition
// Assembly: MissionEditor, Version=1.6.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 00D14066-6848-4104-A662-B9B4BF67FB6E
// Assembly location: D:\Server\eab\Tools\MissionEditor.exe

using CommonTools;
using MissionEditor.Database;
using System;
using System.IO;
using System.Xml;

namespace MissionEditor.Nodes
{
  public class Condition
  {
    private ConditionType m_conditionType;
    private string m_value;
    private string m_flag;

    public Condition()
    {
      this.clear();
    }

    public void clear()
    {
      this.m_value = "";
      this.m_flag = "";
    }

    public void setConditionType(ConditionType restrictionType)
    {
      this.m_conditionType = restrictionType;
    }

    public ConditionType getConditionType()
    {
      return this.m_conditionType;
    }

    public void setFlag(string flag)
    {
      this.m_flag = flag;
    }

    public string getFlag()
    {
      return this.m_flag;
    }

    public void setValue(string value)
    {
      this.m_value = value;
    }

    public string getValue()
    {
      return this.m_value;
    }

    public string getFormattedValue()
    {
      switch (this.m_conditionType)
      {
        case ConditionType.Race:
          Races enumValue1;
          if (Enumeration.TryParse<Races>(this.m_value, out enumValue1))
            return enumValue1.ToString();
          break;
        case ConditionType.Profession:
          Professions enumValue2;
          if (Enumeration.TryParse<Professions>(this.m_value, out enumValue2))
            return enumValue2.ToString();
          break;
        case ConditionType.Faction_Required:
          return this.getValue() + " x " + DataConfiguration.getDescription(DataConfiguration.DataType.faction, this.getFlag());
        case ConditionType.Item_Required:
          return this.getValue() + " x " + DataConfiguration.getDescription(DataConfiguration.DataType.item, this.getFlag());
        case ConditionType.Mission_Required:
          return DataConfiguration.getDescription(DataConfiguration.DataType.mission, this.getValue());
      }
      return this.m_value;
    }

    public void fromXml(XmlNode xmlNode)
    {
      string flag;
      CommonTools.Xml.getAttribute(xmlNode, "ID", true, out flag);
      ConditionType enumValue;
      if (!Enumeration.TryParse<ConditionType>(flag, out enumValue))
        throw new Exception("Unable to convert '" + flag + "' into a ConditionType\n\n" + xmlNode.InnerXml);
      this.setConditionType(enumValue);
      CommonTools.Xml.getValue(xmlNode, true, out flag);
      this.setValue(flag);
      if (CommonTools.Xml.getAttribute(xmlNode, "Flags", false, out flag))
        this.setFlag(flag);
      this.addValidations();
    }

    public void getXML(StringWriter stringWriter)
    {
      stringWriter.WriteLine("<Condition" + CommonTools.Xml.attribute("ID", (int) this.m_conditionType) + (this.m_flag.Length != 0 ? CommonTools.Xml.attribute("Flags", this.m_flag) : "") + ">" + this.m_value + CommonTools.Xml.tagEnd(nameof (Condition)));
    }

    public void getReport(StringWriter stringWriter)
    {
      stringWriter.WriteLine("<TR><TD>" + this.m_conditionType.ToString() + "</TD>");
      stringWriter.WriteLine("<TD>" + this.getFormattedValue() + "</TD></TR>");
    }

    public void addValidations()
    {
      switch (this.m_conditionType)
      {
        case ConditionType.Overall_Level:
          int result1;
          if (int.TryParse(this.getValue(), out result1) && result1 >= 1 && result1 <= 150)
            break;
          DataConfiguration.addValidation("The " + (object) this.m_conditionType + " should be within 1 and 150.");
          break;
        case ConditionType.Combat_Level:
        case ConditionType.Explore_Level:
        case ConditionType.Trade_Level:
          int result2;
          if (int.TryParse(this.getValue(), out result2) && result2 >= 1 && result2 <= 50)
            break;
          DataConfiguration.addValidation("The " + (object) this.m_conditionType + " should be within 1 and 50.");
          break;
        case ConditionType.Race:
          if ("012".Contains(this.getValue()))
            break;
          DataConfiguration.addValidation("Race type is invalid.");
          break;
        case ConditionType.Profession:
          if ("012".Contains(this.getValue()))
            break;
          DataConfiguration.addValidation("Profession type is invalid.");
          break;
        case ConditionType.Hull_Level:
          int result3;
          if (int.TryParse(this.getValue(), out result3) && result3 >= 1 && result3 <= 6)
            break;
          DataConfiguration.addValidation("The " + (object) this.m_conditionType + " should be within 1 and 6.");
          break;
        case ConditionType.Faction_Required:
          int result4;
          if (!int.TryParse(this.getValue(), out result4) || result4 < 1 || result4 > 999999)
            DataConfiguration.addValidation("The " + (object) this.m_conditionType + " should be within 1 and 999,999.");
          DataConfiguration.addValidation(DataConfiguration.DataType.faction, this.getFlag());
          break;
        case ConditionType.Item_Required:
          int result5;
          if (!int.TryParse(this.getValue(), out result5) || result5 < 1 || result5 > 5000)
            DataConfiguration.addValidation("The " + (object) this.m_conditionType + " should be within 1 and 5,000.");
          DataConfiguration.addValidation(DataConfiguration.DataType.item, this.getFlag());
          break;
        case ConditionType.Mission_Required:
          DataConfiguration.addValidation(DataConfiguration.DataType.mission, this.getValue());
          break;
      }
    }
  }
}
