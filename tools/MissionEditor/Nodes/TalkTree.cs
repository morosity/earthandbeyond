﻿// Decompiled with JetBrains decompiler
// Type: MissionEditor.Nodes.TalkTree
// Assembly: MissionEditor, Version=1.6.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 00D14066-6848-4104-A662-B9B4BF67FB6E
// Assembly location: D:\Server\eab\Tools\MissionEditor.exe

using CommonTools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace MissionEditor.Nodes
{
  public class TalkTree
  {
    private string m_nodeId;
    private string m_npcText;
    private string m_sound;
    private List<TalkTree.Branch> m_pcReplies;
    private TalkTree.Flag m_flag;

    public TalkTree()
    {
      this.m_nodeId = "";
      this.m_sound = "";
      this.m_npcText = "";
      this.m_pcReplies = (List<TalkTree.Branch>) null;
      this.m_flag = (TalkTree.Flag) null;
    }

    public void setNodeId(string nodeId)
    {
      this.m_nodeId = nodeId;
    }

    public string getNodeId()
    {
      return this.m_nodeId;
    }

    public string getSound()
    {
      return this.m_sound;
    }

    public void setSound(string sound)
    {
      this.m_sound = sound;
    }

    public void setNpcText(string npcText)
    {
      this.m_npcText = npcText;
    }

    public string getNpcText()
    {
      return this.m_npcText;
    }

    public void addPcReply(TalkTree.Branch branch)
    {
      if (this.m_pcReplies == null)
        this.m_pcReplies = new List<TalkTree.Branch>();
      this.m_pcReplies.Add(branch);
    }

    public bool hasReplies()
    {
      if (this.m_pcReplies != null)
        return this.m_pcReplies.Count != 0;
      return false;
    }

    public List<TalkTree.Branch> getPcReplies()
    {
      return this.m_pcReplies;
    }

    public void setFlag(TalkTree.Flag flag)
    {
      this.m_flag = flag;
    }

    public bool hasFlag()
    {
      return this.m_flag != null;
    }

    public TalkTree.Flag getFlag()
    {
      return this.m_flag;
    }

    public void getXML(StringWriter stringWriter)
    {
      if (this.m_nodeId.Length == 0)
        return;
      stringWriter.Write(CommonTools.Xml.tagStart() + "Tree" + CommonTools.Xml.attribute("Node", this.m_nodeId));
      if (this.m_sound != "")
        stringWriter.Write(CommonTools.Xml.attribute("Sound", this.m_sound));
      stringWriter.WriteLine(CommonTools.Xml.tagEnd());
      if (this.m_npcText != null && this.m_npcText.Length != 0)
        stringWriter.WriteLine(CommonTools.Xml.tag("Text", this.m_npcText));
      if (this.hasReplies())
      {
        foreach (TalkTree.Branch pcReply in this.m_pcReplies)
          pcReply.getXML(stringWriter);
      }
      if (this.hasFlag())
        this.m_flag.getXML(stringWriter);
      stringWriter.WriteLine(CommonTools.Xml.tagEnd("Tree"));
    }

    public void fromXML(XmlNode xmlNode)
    {
      this.setNodeId(xmlNode.Attributes["Node"].Value);
      if (xmlNode.Attributes.Count > 1)
        this.setSound(xmlNode.Attributes["Sound"].Value);
      foreach (XmlNode childNode in xmlNode.ChildNodes)
      {
        if (childNode.Name.Equals("Text"))
          this.setNpcText(childNode.FirstChild.Value);
        else if (childNode.Name.Equals("Branch"))
          this.addPcReply(new TalkTree.Branch()
          {
            gotoNode = childNode.Attributes["Node"].Value,
            text = childNode.FirstChild.Value
          });
        else if (childNode.Name.Equals("Flags"))
        {
          TalkTree.Flag flag = new TalkTree.Flag();
          foreach (XmlAttribute attribute in (XmlNamedNodeMap) childNode.Attributes)
          {
            if (attribute.Name.Equals("Data"))
              flag.data = attribute.Value;
          }
          flag.value = (TalkTreeFlag) Enum.Parse(typeof (TalkTreeFlag), childNode.FirstChild.Value);
          this.setFlag(flag);
        }
      }
    }

    public void addValidations()
    {
    }

    public bool valid(out string error)
    {
      error = "";
      return error.Length == 0;
    }

    internal void getReport(StringWriter stringWriter)
    {
      if (this.m_nodeId.Length == 0)
        return;
      stringWriter.WriteLine("<tr><td></td><td><TABLE>");
      stringWriter.WriteLine("<TR><th colspan=\"3\">" + this.m_nodeId + ". " + this.m_npcText + "|Sound='" + this.m_sound + "'</th></tr>");
      if (this.hasReplies())
      {
        foreach (TalkTree.Branch pcReply in this.m_pcReplies)
        {
          stringWriter.Write("<TR><td>&nbsp;</td>");
          stringWriter.Write("<td>" + pcReply.gotoNode + "</td>");
          stringWriter.WriteLine("<td>" + pcReply.text + "</td></tr>");
        }
      }
      if (this.hasFlag())
      {
        stringWriter.Write("<TR><td>&nbsp;</td>");
        stringWriter.Write("<td>" + this.m_flag.value.ToString() + "</td>");
        stringWriter.WriteLine("<td>" + (this.m_flag.data == null || this.m_flag.data.Length == 0 ? "&nbsp;" : this.m_flag.data) + "</td></tr>");
      }
      stringWriter.WriteLine("</TABLE></td></tr>");
    }

    public class Branch
    {
      public string gotoNode;
      public string text;

      public Branch()
      {
        this.gotoNode = "";
        this.text = "";
      }

      public void getXML(StringWriter stringWriter)
      {
        stringWriter.WriteLine(CommonTools.Xml.tagStart() + nameof (Branch) + CommonTools.Xml.attribute("Node", this.gotoNode) + CommonTools.Xml.tagEnd() + this.text + CommonTools.Xml.tagEnd(nameof (Branch)));
      }
    }

    public class Flag
    {
      public string data;
      public TalkTreeFlag value;

      public Flag()
      {
        this.data = "";
      }

      public void getXML(StringWriter stringWriter)
      {
        stringWriter.Write(CommonTools.Xml.tagStart() + "Flags");
        if (this.data.Length != 0)
          stringWriter.Write(CommonTools.Xml.attribute("Data", this.data));
        stringWriter.WriteLine(CommonTools.Xml.tagEnd() + ((int) this.value).ToString() + CommonTools.Xml.tagEnd("Flags"));
      }
    }
  }
}
