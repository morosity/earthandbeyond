﻿// Decompiled with JetBrains decompiler
// Type: MissionEditor.Nodes.Completion
// Assembly: MissionEditor, Version=1.6.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 00D14066-6848-4104-A662-B9B4BF67FB6E
// Assembly location: D:\Server\eab\Tools\MissionEditor.exe

using CommonTools;
using MissionEditor.Database;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace MissionEditor.Nodes
{
  public class Completion
  {
    private CompletionType m_completionType;
    private string m_value;
    private int m_count;
    private string m_data;
    private string m_char_data;

    public Completion()
    {
      this.clear();
    }

    public void clear()
    {
      this.m_value = "";
      this.m_count = -1;
      this.m_data = "";
      this.m_char_data = "";
    }

    public void setCompletionType(CompletionType id)
    {
      this.m_completionType = id;
    }

    public CompletionType getCompletionType()
    {
      return this.m_completionType;
    }

    public void setValue(string value)
    {
      this.m_value = value;
    }

    public string getValue()
    {
      return this.m_value;
    }

    public string getFormattedValue()
    {
      switch (this.m_completionType)
      {
        case CompletionType.Talk_To_Npc:
          return DataConfiguration.getDescription(DataConfiguration.DataType.npc, this.getValue());
        case CompletionType.Give_Item:
          return DataConfiguration.getDescription(DataConfiguration.DataType.item, this.getValue());
        case CompletionType.Fight_Mob:
          return this.getCount().ToString() + " x " + DataConfiguration.getDescription(DataConfiguration.DataType.mob, this.getValue());
        case CompletionType.Current_Sector:
          return DataConfiguration.getDescription(DataConfiguration.DataType.sector, this.getValue());
        case CompletionType.Nearest_Nav:
          return DataConfiguration.getDescription(DataConfiguration.DataType.sector_object, this.getValue());
        case CompletionType.Obtain_Items:
        case CompletionType.Possess_Item:
        case CompletionType.Obtain_Blueprint:
          return DataConfiguration.getDescription(DataConfiguration.DataType.item, this.getValue());
        case CompletionType.Use_Skill_On_Mob_Type:
          return DataConfiguration.getDescription(DataConfiguration.DataType.skill, this.getData()) + " > " + DataConfiguration.getDescription(DataConfiguration.DataType.mob, this.getValue());
        case CompletionType.Use_Skill_On_Object:
          return DataConfiguration.getDescription(DataConfiguration.DataType.skill, this.getData()) + " > " + DataConfiguration.getDescription(DataConfiguration.DataType.sector_object, this.getValue());
        case CompletionType.Scan_Object:
          return DataConfiguration.getDescription(DataConfiguration.DataType.sector_object, this.getValue());
        case CompletionType.Arrive_At:
          return DataConfiguration.getDescription(DataConfiguration.DataType.sector_object, this.getValue());
        case CompletionType.Nav_Message:
          return DataConfiguration.getDescription(DataConfiguration.DataType.sector_object, this.getValue());
        case CompletionType.Proximity_To_Space_Npc:
          return DataConfiguration.getDescription(DataConfiguration.DataType.sector_object, this.getValue());
        case CompletionType.Talk_Space_Npc:
          return DataConfiguration.getDescription(DataConfiguration.DataType.sector_object, this.getValue());
        case CompletionType.Play_Sound:
          return this.getCharData().ToString();
        default:
          return this.m_value;
      }
    }

    public void setCount(int count)
    {
      this.m_count = count;
    }

    public int getCount()
    {
      return this.m_count;
    }

    public void setData(string data)
    {
      this.m_data = data;
    }

    public void setCharData(string data)
    {
      this.m_char_data = data;
    }

    public string getData()
    {
      return this.m_data;
    }

    public string getCharData()
    {
      return this.m_char_data;
    }

    public bool contains(List<Completion> completionList, CompletionType completionType)
    {
      if (completionList != null)
      {
        foreach (Completion completion in completionList)
        {
          if (completion.getCompletionType().Equals((object) completionType))
            return true;
        }
      }
      return false;
    }

    public void fromXml(XmlNode xmlNode)
    {
      string data;
      CommonTools.Xml.getAttribute(xmlNode, "ID", true, out data);
      CompletionType enumValue;
      if (!Enumeration.TryParse<CompletionType>(data, out enumValue))
        throw new Exception("Unable to convert '" + data + "' into a CompletionType\n\n" + xmlNode.InnerXml);
      this.setCompletionType(enumValue);
      int count;
      if (CommonTools.Xml.getAttribute(xmlNode, "Count", false, out count))
        this.setCount(count);
      if (CommonTools.Xml.getAttribute(xmlNode, "Data", false, out data))
        this.setData(data);
      if (CommonTools.Xml.getAttribute(xmlNode, "Text", false, out data))
        this.setCharData(data);
      CommonTools.Xml.getValue(xmlNode, true, out data);
      this.setValue(data);
      this.addValidations();
    }

    public void getXML(StringWriter stringWriter)
    {
      stringWriter.Write(CommonTools.Xml.tagStart() + nameof (Completion) + CommonTools.Xml.attribute("ID", ((int) this.m_completionType).ToString()));
      switch (this.getCompletionType())
      {
        case CompletionType.Give_Item:
          if (this.getCount() != -1)
          {
            stringWriter.Write(CommonTools.Xml.attribute("Count", this.getCount()));
            break;
          }
          break;
        case CompletionType.Fight_Mob:
          stringWriter.Write(CommonTools.Xml.attribute("Count", this.getCount()));
          break;
        case CompletionType.Obtain_Items:
          if (this.getCount() != -1)
          {
            stringWriter.Write(CommonTools.Xml.attribute("Count", this.getCount()));
            break;
          }
          break;
        case CompletionType.Use_Skill_On_Mob_Type:
          stringWriter.Write(CommonTools.Xml.attribute("Data", this.getData()));
          break;
        case CompletionType.Use_Skill_On_Object:
          stringWriter.Write(CommonTools.Xml.attribute("Data", this.getData()));
          break;
        case CompletionType.Possess_Item:
          stringWriter.Write(CommonTools.Xml.attribute("Count", this.getCount()));
          break;
        case CompletionType.Nav_Message:
          stringWriter.Write(CommonTools.Xml.attribute("Count", this.getCount()));
          break;
        case CompletionType.Play_Sound:
          stringWriter.Write(CommonTools.Xml.attribute("Text", this.getCharData()));
          break;
      }
      stringWriter.WriteLine(">" + this.getValue() + CommonTools.Xml.tagEnd(nameof (Completion)));
    }

    public void addValidations()
    {
      if (this.m_value == null || this.m_value.Length == 0)
      {
        DataConfiguration.addValidation("The completion value is mandatory");
      }
      else
      {
        switch (this.getCompletionType())
        {
          case CompletionType.Talk_To_Npc:
            DataConfiguration.addValidation(DataConfiguration.DataType.npc, this.getValue());
            break;
          case CompletionType.Give_Item:
            if (this.getCount() == -1)
              this.setCount(1);
            if (this.getCount() < 0 || this.getCount() > 500)
              DataConfiguration.addValidation("The quantity of items to give must be between 1 and 500");
            DataConfiguration.addValidation(DataConfiguration.DataType.item, this.getValue());
            break;
          case CompletionType.Receive_Item:
            int result1;
            if (int.TryParse(this.getValue(), out result1))
            {
              switch (result1)
              {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                case 10:
                  return;
              }
            }
            DataConfiguration.addValidation("The quantity must be between 1 and 10");
            break;
          case CompletionType.Fight_Mob:
            if (this.getCount() == -1)
              this.setCount(1);
            if (this.getCount() < 0 || this.getCount() > 5000)
              DataConfiguration.addValidation("The number of mobs to fight must be between 1 and 5,000");
            DataConfiguration.addValidation(DataConfiguration.DataType.mob, this.getValue());
            break;
          case CompletionType.Current_Sector:
            DataConfiguration.addValidation(DataConfiguration.DataType.sector, this.getValue());
            break;
          case CompletionType.Nearest_Nav:
            DataConfiguration.addValidation(DataConfiguration.DataType.sector_object, this.getValue());
            break;
          case CompletionType.Obtain_Items:
          case CompletionType.Obtain_Blueprint:
            DataConfiguration.addValidation(DataConfiguration.DataType.item, this.getValue());
            break;
          case CompletionType.Use_Skill_On_Mob_Type:
            DataConfiguration.addValidation(DataConfiguration.DataType.mob, this.getValue());
            DataConfiguration.addValidation(DataConfiguration.DataType.skill, this.getData());
            break;
          case CompletionType.Use_Skill_On_Object:
            DataConfiguration.addValidation(DataConfiguration.DataType.sector_object, this.getValue());
            DataConfiguration.addValidation(DataConfiguration.DataType.skill, this.getData());
            break;
          case CompletionType.Scan_Object:
            DataConfiguration.addValidation(DataConfiguration.DataType.sector_object, this.getValue());
            break;
          case CompletionType.Possess_Item:
            if (this.getCount() == -1)
              this.setCount(1);
            if (this.getCount() < 0 || this.getCount() > 5000)
              DataConfiguration.addValidation("The quantity must be between 1 and 5,000");
            DataConfiguration.addValidation(DataConfiguration.DataType.item, this.getValue());
            break;
          case CompletionType.Give_Credits:
            int result2;
            if (int.TryParse(this.getValue(), out result2) && result2 >= 0 && result2 <= 2500000)
              break;
            DataConfiguration.addValidation("The amount of credits to give must be between 1 and 2,500,000");
            break;
          case CompletionType.Arrive_At:
          case CompletionType.Proximity_To_Space_Npc:
          case CompletionType.Talk_Space_Npc:
            DataConfiguration.addValidation(DataConfiguration.DataType.sector_object, this.getValue());
            break;
          case CompletionType.Nav_Message:
            if (this.getCount() == -1)
              this.setCount(5000);
            if (this.getCount() != 5000 && this.getCount() != 10000 && this.getCount() != 30000)
              DataConfiguration.addValidation("The 3 Nav Message ranges are 5000, 10000 and 30000. You must use one of these ranges to trigger your message.");
            DataConfiguration.addValidation(DataConfiguration.DataType.sector_object, this.getValue());
            break;
        }
      }
    }

    internal void getReport(StringWriter stringWriter)
    {
      stringWriter.WriteLine("<tr><td>" + this.m_completionType.ToString() + "</td>");
      stringWriter.WriteLine("<td>" + this.getFormattedValue() + "</td></tr>");
    }
  }
}
