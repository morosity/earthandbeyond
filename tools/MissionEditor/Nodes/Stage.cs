﻿// Decompiled with JetBrains decompiler
// Type: MissionEditor.Nodes.Stage
// Assembly: MissionEditor, Version=1.6.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 00D14066-6848-4104-A662-B9B4BF67FB6E
// Assembly location: D:\Server\eab\Tools\MissionEditor.exe

using CommonTools;
using MissionEditor.Database;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Xml;

namespace MissionEditor.Nodes
{
  public class Stage
  {
    public const int m_maxStages = 20;
    private string m_id;
    private string m_description;
    private List<Completion> m_completions;
    private List<Reward> m_rewards;
    private List<TalkTree> m_talkTrees;

    public Stage()
    {
      this.clear();
    }

    public void clear()
    {
      this.m_id = "";
      this.m_description = "";
      if (this.m_completions != null && this.m_completions.Count != 0)
        this.m_completions.Clear();
      this.m_completions = (List<Completion>) null;
      if (this.m_rewards != null && this.m_rewards.Count != 0)
        this.m_rewards.Clear();
      this.m_rewards = (List<Reward>) null;
      if (this.m_talkTrees != null && this.m_talkTrees.Count != 0)
        this.m_talkTrees.Clear();
      this.m_talkTrees = (List<TalkTree>) null;
    }

    public override string ToString()
    {
      return this.getId() + "." + this.getDescription();
    }

    public void setId(string id)
    {
      this.m_id = id;
    }

    public string getId()
    {
      return this.m_id;
    }

    public void setDescription(string description)
    {
      this.m_description = description;
    }

    public string getDescription()
    {
      return this.m_description;
    }

    public void addCompletion(Completion completion)
    {
      if (this.m_completions == null)
        this.m_completions = new List<Completion>();
      if (!this.CheckBadCompletionDuplicates(completion))
        return;
      this.m_completions.Add(completion);
    }

    public bool CheckBadCompletionDuplicates(Completion completion)
    {
      if (completion.getCompletionType() != CompletionType.Nearest_Nav || !completion.contains(this.m_completions, CompletionType.Nearest_Nav))
        return true;
      DataConfiguration.addValidation("You can only have one Nearest_Nav per stage completion.");
      return false;
    }

    public bool hasCompletions()
    {
      if (this.m_completions != null)
        return this.m_completions.Count != 0;
      return false;
    }

    public List<Completion> getCompletions()
    {
      return this.m_completions;
    }

    public void removeCompletion(Completion completion)
    {
      if (this.m_completions == null)
        return;
      this.m_completions.Remove(completion);
    }

    public void clearCompletions()
    {
      if (this.m_completions == null)
        return;
      this.m_completions.Clear();
    }

    public void addReward(Reward reward)
    {
      if (this.m_rewards == null)
        this.m_rewards = new List<Reward>();
      this.m_rewards.Add(reward);
    }

    public bool hasRewards()
    {
      if (this.m_rewards != null)
        return this.m_rewards.Count != 0;
      return false;
    }

    public List<Reward> getRewards()
    {
      return this.m_rewards;
    }

    public void removeReward(Reward reward)
    {
      if (this.m_rewards == null)
        return;
      this.m_rewards.Remove(reward);
    }

    public void clearRewards()
    {
      if (this.m_rewards == null)
        return;
      this.m_rewards.Clear();
    }

    public void addTalkTree(TalkTree talkTree)
    {
      if (this.m_talkTrees == null)
        this.m_talkTrees = new List<TalkTree>();
      this.m_talkTrees.Add(talkTree);
    }

    public void clearTalkTrees()
    {
      if (this.m_talkTrees == null)
        return;
      this.m_talkTrees.Clear();
    }

    public bool hasTalkTrees()
    {
      if (this.m_talkTrees != null)
        return this.m_talkTrees.Count != 0;
      return false;
    }

    public List<TalkTree> getTalkTrees()
    {
      return this.m_talkTrees;
    }

    public void getTalkTreesXML(StringWriter stringWriter)
    {
      if (!this.hasTalkTrees())
        return;
      foreach (TalkTree talkTree in this.m_talkTrees)
        talkTree.getXML(stringWriter);
    }

    public void fromXml(XmlNode xmlNode)
    {
      string str;
      CommonTools.Xml.getAttribute(xmlNode, "ID", true, out str);
      this.setId(str);
      foreach (XmlNode childNode in xmlNode.ChildNodes)
      {
        if (childNode.Name.Equals("Description"))
        {
          CommonTools.Xml.getValue(childNode, !this.getId().Equals("0"), out str);
          this.setDescription(str);
        }
        else if (childNode.Name.Equals("Completion"))
        {
          Completion completion = new Completion();
          completion.fromXml(childNode);
          this.addCompletion(completion);
        }
        else if (childNode.Name.Equals("Reward"))
        {
          Reward reward = new Reward();
          reward.fromXml(childNode);
          this.addReward(reward);
        }
        else if (childNode.Name.Equals("Tree"))
        {
          TalkTree talkTree = new TalkTree();
          talkTree.fromXML(childNode);
          this.addTalkTree(talkTree);
        }
        else
        {
          int num = (int) MessageBox.Show("Unexpected node: " + childNode.Name + " in theStage tag");
        }
      }
      this.addValidations(Stage.ValidationType.Complete);
    }

    public void getXML(StringWriter stringWriter)
    {
      stringWriter.WriteLine("<Stage ID=\"" + this.m_id + "\">");
      if (this.m_description != null && this.m_description.Length != 0)
        stringWriter.WriteLine("<Description>" + this.m_description + "</Description>");
      if (this.hasCompletions())
      {
        foreach (Completion completion in this.getCompletions())
          completion.getXML(stringWriter);
      }
      if (this.hasTalkTrees())
      {
        foreach (TalkTree talkTree in this.getTalkTrees())
          talkTree.getXML(stringWriter);
      }
      if (this.hasRewards())
      {
        foreach (Reward reward in this.getRewards())
          reward.getXML(stringWriter);
      }
      stringWriter.WriteLine("</Stage>");
    }

    public void getReport(StringWriter stringWriter)
    {
      if (this.hasCompletions())
      {
        stringWriter.WriteLine("<tr><td colspan=\"2\"><BR><B>Completions<B></td></tr>");
        foreach (Completion completion in this.getCompletions())
          completion.getReport(stringWriter);
      }
      if (this.hasRewards())
      {
        stringWriter.WriteLine("<tr><td colspan=\"2\"><BR><B>Rewards<B></td></tr>");
        foreach (Reward reward in this.getRewards())
          reward.getReport(stringWriter);
      }
      if (!this.hasTalkTrees())
        return;
      stringWriter.WriteLine("<tr><td colspan=\"2\"><BR><B>Talk Tree<B></td></tr>");
      foreach (TalkTree talkTree in this.getTalkTrees())
        talkTree.getReport(stringWriter);
    }

    public void addValidations(Stage.ValidationType validationType)
    {
      int result;
      if (!int.TryParse(this.getId(), out result) || result < 0 || result >= 20)
        DataConfiguration.addValidation("The stage ID must be within 0 and " + 19.ToString());
      else if (result != 0 && this.getDescription().Length == 0)
        DataConfiguration.addValidation("The stage description must be specified (Stage: " + this.getId() + ")");
      if (!validationType.Equals((object) Stage.ValidationType.Complete))
        return;
      if (!this.hasCompletions())
      {
        DataConfiguration.addValidation("Stage completions are mandatory (Stage: " + this.getId() + ")");
      }
      else
      {
        int num1 = 0;
        foreach (Completion completion in this.getCompletions())
        {
          switch (completion.getCompletionType())
          {
            case CompletionType.Fight_Mob:
            case CompletionType.Use_Skill_On_Mob_Type:
            case CompletionType.Use_Skill_On_Object:
              int num2 = completion.getCount();
              if (num2 == -1)
                num2 = 1;
              num1 += num2;
              continue;
            default:
              continue;
          }
        }
        if (num1 < 32)
          return;
        DataConfiguration.addValidation("There cannot be more than 32 actions in a stage (Stage: " + this.getId() + ")");
      }
    }

    public enum ValidationType
    {
      FromDialog,
      Complete,
    }
  }
}
