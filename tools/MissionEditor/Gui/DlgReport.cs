﻿// Decompiled with JetBrains decompiler
// Type: MissionEditor.Gui.DlgReport
// Assembly: MissionEditor, Version=1.6.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 00D14066-6848-4104-A662-B9B4BF67FB6E
// Assembly location: D:\Server\eab\Tools\MissionEditor.exe

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace MissionEditor.Gui
{
  public class DlgReport : Form
  {
    private IContainer components;
    private WebBrowser guiWebBrowser;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.guiWebBrowser = new WebBrowser();
      this.SuspendLayout();
      this.guiWebBrowser.AllowNavigation = false;
      this.guiWebBrowser.AllowWebBrowserDrop = false;
      this.guiWebBrowser.Dock = DockStyle.Fill;
      this.guiWebBrowser.Location = new Point(0, 0);
      this.guiWebBrowser.MinimumSize = new Size(20, 20);
      this.guiWebBrowser.Name = "guiWebBrowser";
      this.guiWebBrowser.Size = new Size(541, 492);
      this.guiWebBrowser.TabIndex = 0;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(541, 492);
      this.Controls.Add((Control) this.guiWebBrowser);
      this.Name = nameof (DlgReport);
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = nameof (DlgReport);
      this.ResumeLayout(false);
    }

    public DlgReport()
    {
      this.InitializeComponent();
    }

    public void set(string html)
    {
      if (this.guiWebBrowser.Document != (HtmlDocument) null)
      {
        this.guiWebBrowser.Document.OpenNew(true);
        this.guiWebBrowser.Document.Write(html);
      }
      else
        this.guiWebBrowser.DocumentText = html;
    }

    protected override void OnShown(EventArgs e)
    {
      base.OnShown(e);
    }
  }
}
