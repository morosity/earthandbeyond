﻿// Decompiled with JetBrains decompiler
// Type: MissionEditor.Gui.TabMission
// Assembly: MissionEditor, Version=1.6.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 00D14066-6848-4104-A662-B9B4BF67FB6E
// Assembly location: D:\Server\eab\Tools\MissionEditor.exe

using CommonTools;
using CommonTools.Gui;
using MissionEditor.Database;
using MissionEditor.Nodes;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace MissionEditor.Gui
{
  public class TabMission : UserControl
  {
    private IContainer components;
    private TextBox guiSummaryTxt;
    private Label guiSummaryLbl;
    private TextBox guiNameTxt;
    private Label guiNameLbl;
    private CheckBox guiForfeitableChk;
    private TextBox guiTimeTxt;
    private Label guiTimeLbl;
    private TextBox guiIdTxt;
    private Label guiIdLbl;
    private ListView guiConditionsTbl;
    private ColumnHeader guiColumnHeaderType;
    private ColumnHeader guiColumnHeaderValue;
    private Label guiConditionsLbl;
    private Button guiConditionsAddBtn;
    private Button guiConditionsRemoveBtn;
    private Button guiStagesRemoveBtn;
    private Button guiStagesAddBtn;
    private Label guiStagesLbl;
    private ListView guiStagesTbl;
    private ColumnHeader guiColumnStageId;
    private ColumnHeader guiColumnStageDescription;
    private Button guiConditionsEditBtn;
    private ComboBox guiTypeCbo;
    private Label guiTypeLbl;
    private Label guiKeyLbl;
    private TextBox guiKeyTxt;
    private Button guiKeyBtn;
    private Button guiStageDownBtn;
    private Button guiStageUpBtn;
    private Button guiConditionUpBtn;
    private Button guiConditionDownBtn;
    private Label label1;
    private ComboBox minSecuirtLevel;
    private FrmMission m_parent;
    private Mission m_mission;
    private DlgConditions m_dlgConditions;
    private DlgStages m_dlgStages;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (TabMission));
      this.guiSummaryTxt = new TextBox();
      this.guiSummaryLbl = new Label();
      this.guiNameTxt = new TextBox();
      this.guiNameLbl = new Label();
      this.guiForfeitableChk = new CheckBox();
      this.guiTimeTxt = new TextBox();
      this.guiTimeLbl = new Label();
      this.guiIdTxt = new TextBox();
      this.guiIdLbl = new Label();
      this.guiConditionsTbl = new ListView();
      this.guiColumnHeaderType = new ColumnHeader();
      this.guiColumnHeaderValue = new ColumnHeader();
      this.guiConditionsLbl = new Label();
      this.guiConditionsAddBtn = new Button();
      this.guiConditionsRemoveBtn = new Button();
      this.guiStagesRemoveBtn = new Button();
      this.guiStagesAddBtn = new Button();
      this.guiStagesLbl = new Label();
      this.guiStagesTbl = new ListView();
      this.guiColumnStageId = new ColumnHeader();
      this.guiColumnStageDescription = new ColumnHeader();
      this.guiConditionsEditBtn = new Button();
      this.guiTypeCbo = new ComboBox();
      this.guiTypeLbl = new Label();
      this.guiKeyLbl = new Label();
      this.guiKeyTxt = new TextBox();
      this.guiKeyBtn = new Button();
      this.guiStageDownBtn = new Button();
      this.guiStageUpBtn = new Button();
      this.guiConditionUpBtn = new Button();
      this.guiConditionDownBtn = new Button();
      this.label1 = new Label();
      this.minSecuirtLevel = new ComboBox();
      this.SuspendLayout();
      this.guiSummaryTxt.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.guiSummaryTxt.Location = new Point(74, 66);
      this.guiSummaryTxt.Multiline = true;
      this.guiSummaryTxt.Name = "guiSummaryTxt";
      this.guiSummaryTxt.Size = new Size(461, 45);
      this.guiSummaryTxt.TabIndex = 10;
      this.guiSummaryTxt.TextChanged += new EventHandler(this.onChanged);
      this.guiSummaryLbl.AutoSize = true;
      this.guiSummaryLbl.Location = new Point(15, 69);
      this.guiSummaryLbl.Name = "guiSummaryLbl";
      this.guiSummaryLbl.Size = new Size(53, 13);
      this.guiSummaryLbl.TabIndex = 9;
      this.guiSummaryLbl.Text = "Summary:";
      this.guiNameTxt.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.guiNameTxt.Location = new Point(74, 40);
      this.guiNameTxt.Name = "guiNameTxt";
      this.guiNameTxt.Size = new Size(356, 20);
      this.guiNameTxt.TabIndex = 8;
      this.guiNameTxt.TextChanged += new EventHandler(this.onChanged);
      this.guiNameLbl.AutoSize = true;
      this.guiNameLbl.Location = new Point(30, 43);
      this.guiNameLbl.Name = "guiNameLbl";
      this.guiNameLbl.Size = new Size(38, 13);
      this.guiNameLbl.TabIndex = 7;
      this.guiNameLbl.Text = "Name:";
      this.guiForfeitableChk.AutoSize = true;
      this.guiForfeitableChk.Location = new Point(74, 142);
      this.guiForfeitableChk.Name = "guiForfeitableChk";
      this.guiForfeitableChk.Size = new Size(75, 17);
      this.guiForfeitableChk.TabIndex = 13;
      this.guiForfeitableChk.Text = "Forfeitable";
      this.guiForfeitableChk.UseVisualStyleBackColor = true;
      this.guiForfeitableChk.CheckedChanged += new EventHandler(this.onChanged);
      this.guiTimeTxt.Location = new Point(74, 117);
      this.guiTimeTxt.Name = "guiTimeTxt";
      this.guiTimeTxt.Size = new Size(100, 20);
      this.guiTimeTxt.TabIndex = 12;
      this.guiTimeTxt.TextChanged += new EventHandler(this.onChanged);
      this.guiTimeTxt.KeyPress += new KeyPressEventHandler(this.onTimeKeyPress);
      this.guiTimeLbl.AutoSize = true;
      this.guiTimeLbl.Location = new Point(35, 120);
      this.guiTimeLbl.Name = "guiTimeLbl";
      this.guiTimeLbl.Size = new Size(33, 13);
      this.guiTimeLbl.TabIndex = 11;
      this.guiTimeLbl.Text = "Time:";
      this.guiIdTxt.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.guiIdTxt.Enabled = false;
      this.guiIdTxt.Location = new Point(74, 13);
      this.guiIdTxt.Name = "guiIdTxt";
      this.guiIdTxt.Size = new Size(55, 20);
      this.guiIdTxt.TabIndex = 1;
      this.guiIdLbl.AutoSize = true;
      this.guiIdLbl.Location = new Point(47, 16);
      this.guiIdLbl.Name = "guiIdLbl";
      this.guiIdLbl.Size = new Size(21, 13);
      this.guiIdLbl.TabIndex = 0;
      this.guiIdLbl.Text = "ID:";
      this.guiConditionsTbl.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.guiConditionsTbl.Columns.AddRange(new ColumnHeader[2]
      {
        this.guiColumnHeaderType,
        this.guiColumnHeaderValue
      });
      this.guiConditionsTbl.FullRowSelect = true;
      this.guiConditionsTbl.HideSelection = false;
      this.guiConditionsTbl.Location = new Point(74, 165);
      this.guiConditionsTbl.Name = "guiConditionsTbl";
      this.guiConditionsTbl.Size = new Size(380, 113);
      this.guiConditionsTbl.TabIndex = 15;
      this.guiConditionsTbl.UseCompatibleStateImageBehavior = false;
      this.guiConditionsTbl.View = View.Details;
      this.guiColumnHeaderType.Text = "Type";
      this.guiColumnHeaderType.Width = 100;
      this.guiColumnHeaderValue.Text = "Value";
      this.guiColumnHeaderValue.Width = 250;
      this.guiConditionsLbl.AutoSize = true;
      this.guiConditionsLbl.Location = new Point(9, 165);
      this.guiConditionsLbl.Name = "guiConditionsLbl";
      this.guiConditionsLbl.Size = new Size(59, 13);
      this.guiConditionsLbl.TabIndex = 14;
      this.guiConditionsLbl.Text = "Conditions:";
      this.guiConditionsAddBtn.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.guiConditionsAddBtn.Location = new Point(460, 165);
      this.guiConditionsAddBtn.Name = "guiConditionsAddBtn";
      this.guiConditionsAddBtn.Size = new Size(75, 23);
      this.guiConditionsAddBtn.TabIndex = 16;
      this.guiConditionsAddBtn.Text = "Add";
      this.guiConditionsAddBtn.UseVisualStyleBackColor = true;
      this.guiConditionsAddBtn.Click += new EventHandler(this.onConditionAdd);
      this.guiConditionsRemoveBtn.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.guiConditionsRemoveBtn.Enabled = false;
      this.guiConditionsRemoveBtn.Location = new Point(460, 223);
      this.guiConditionsRemoveBtn.Name = "guiConditionsRemoveBtn";
      this.guiConditionsRemoveBtn.Size = new Size(75, 23);
      this.guiConditionsRemoveBtn.TabIndex = 18;
      this.guiConditionsRemoveBtn.Text = "Remove";
      this.guiConditionsRemoveBtn.UseVisualStyleBackColor = true;
      this.guiConditionsRemoveBtn.Click += new EventHandler(this.onConditionRemove);
      this.guiStagesRemoveBtn.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.guiStagesRemoveBtn.Enabled = false;
      this.guiStagesRemoveBtn.Location = new Point(458, 330);
      this.guiStagesRemoveBtn.Name = "guiStagesRemoveBtn";
      this.guiStagesRemoveBtn.Size = new Size(75, 23);
      this.guiStagesRemoveBtn.TabIndex = 23;
      this.guiStagesRemoveBtn.Text = "Remove";
      this.guiStagesRemoveBtn.UseVisualStyleBackColor = true;
      this.guiStagesRemoveBtn.Click += new EventHandler(this.onStageRemove);
      this.guiStagesAddBtn.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.guiStagesAddBtn.Location = new Point(458, 301);
      this.guiStagesAddBtn.Name = "guiStagesAddBtn";
      this.guiStagesAddBtn.Size = new Size(75, 23);
      this.guiStagesAddBtn.TabIndex = 22;
      this.guiStagesAddBtn.Text = "Add";
      this.guiStagesAddBtn.UseVisualStyleBackColor = true;
      this.guiStagesAddBtn.Click += new EventHandler(this.onStageAdd);
      this.guiStagesLbl.AutoSize = true;
      this.guiStagesLbl.Location = new Point(25, 306);
      this.guiStagesLbl.Name = "guiStagesLbl";
      this.guiStagesLbl.Size = new Size(43, 13);
      this.guiStagesLbl.TabIndex = 21;
      this.guiStagesLbl.Text = "Stages:";
      this.guiStagesTbl.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.guiStagesTbl.Columns.AddRange(new ColumnHeader[2]
      {
        this.guiColumnStageId,
        this.guiColumnStageDescription
      });
      this.guiStagesTbl.FullRowSelect = true;
      this.guiStagesTbl.HideSelection = false;
      this.guiStagesTbl.Location = new Point(74, 301);
      this.guiStagesTbl.MultiSelect = false;
      this.guiStagesTbl.Name = "guiStagesTbl";
      this.guiStagesTbl.Size = new Size(380, 209);
      this.guiStagesTbl.TabIndex = 20;
      this.guiStagesTbl.UseCompatibleStateImageBehavior = false;
      this.guiStagesTbl.View = View.Details;
      this.guiStagesTbl.SelectedIndexChanged += new EventHandler(this.onStageSelected);
      this.guiColumnStageId.Text = "Id";
      this.guiColumnStageId.Width = 30;
      this.guiColumnStageDescription.Text = "Value";
      this.guiColumnStageDescription.Width = 320;
      this.guiConditionsEditBtn.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.guiConditionsEditBtn.Enabled = false;
      this.guiConditionsEditBtn.Location = new Point(460, 194);
      this.guiConditionsEditBtn.Name = "guiConditionsEditBtn";
      this.guiConditionsEditBtn.Size = new Size(75, 23);
      this.guiConditionsEditBtn.TabIndex = 17;
      this.guiConditionsEditBtn.Text = "Edit";
      this.guiConditionsEditBtn.UseVisualStyleBackColor = true;
      this.guiConditionsEditBtn.Click += new EventHandler(this.onConditionEdit);
      this.guiTypeCbo.DropDownStyle = ComboBoxStyle.DropDownList;
      this.guiTypeCbo.FormattingEnabled = true;
      this.guiTypeCbo.Location = new Point(193, 13);
      this.guiTypeCbo.Name = "guiTypeCbo";
      this.guiTypeCbo.Size = new Size(121, 21);
      this.guiTypeCbo.TabIndex = 3;
      this.guiTypeCbo.SelectedIndexChanged += new EventHandler(this.onChanged);
      this.guiTypeLbl.AutoSize = true;
      this.guiTypeLbl.Location = new Point(153, 16);
      this.guiTypeLbl.Name = "guiTypeLbl";
      this.guiTypeLbl.Size = new Size(34, 13);
      this.guiTypeLbl.TabIndex = 2;
      this.guiTypeLbl.Text = "Type:";
      this.guiKeyLbl.AutoSize = true;
      this.guiKeyLbl.Location = new Point(320, 16);
      this.guiKeyLbl.Name = "guiKeyLbl";
      this.guiKeyLbl.Size = new Size(28, 13);
      this.guiKeyLbl.TabIndex = 4;
      this.guiKeyLbl.Text = "Key:";
      this.guiKeyTxt.Location = new Point(354, 13);
      this.guiKeyTxt.Name = "guiKeyTxt";
      this.guiKeyTxt.Size = new Size(100, 20);
      this.guiKeyTxt.TabIndex = 5;
      this.guiKeyTxt.TextChanged += new EventHandler(this.onChanged);
      this.guiKeyBtn.Location = new Point(460, 11);
      this.guiKeyBtn.Name = "guiKeyBtn";
      this.guiKeyBtn.Size = new Size(75, 23);
      this.guiKeyBtn.TabIndex = 6;
      this.guiKeyBtn.Text = "Search";
      this.guiKeyBtn.UseVisualStyleBackColor = true;
      this.guiKeyBtn.Click += new EventHandler(this.onKeySearch);
      this.guiStageDownBtn.Image = (Image) componentResourceManager.GetObject("guiStageDownBtn.Image");
      this.guiStageDownBtn.Location = new Point(494, 359);
      this.guiStageDownBtn.Margin = new Padding(1);
      this.guiStageDownBtn.Name = "guiStageDownBtn";
      this.guiStageDownBtn.Size = new Size(39, 35);
      this.guiStageDownBtn.TabIndex = 25;
      this.guiStageDownBtn.UseVisualStyleBackColor = true;
      this.guiStageUpBtn.Image = (Image) componentResourceManager.GetObject("guiStageUpBtn.Image");
      this.guiStageUpBtn.Location = new Point(456, 359);
      this.guiStageUpBtn.Margin = new Padding(1);
      this.guiStageUpBtn.Name = "guiStageUpBtn";
      this.guiStageUpBtn.Size = new Size(39, 35);
      this.guiStageUpBtn.TabIndex = 24;
      this.guiStageUpBtn.UseVisualStyleBackColor = true;
      this.guiConditionUpBtn.Image = (Image) componentResourceManager.GetObject("guiConditionUpBtn.Image");
      this.guiConditionUpBtn.Location = new Point(458, 250);
      this.guiConditionUpBtn.Margin = new Padding(1);
      this.guiConditionUpBtn.Name = "guiConditionUpBtn";
      this.guiConditionUpBtn.Size = new Size(39, 35);
      this.guiConditionUpBtn.TabIndex = 19;
      this.guiConditionUpBtn.UseVisualStyleBackColor = true;
      this.guiConditionUpBtn.Click += new EventHandler(this.onSimpleChanged);
      this.guiConditionDownBtn.Image = (Image) componentResourceManager.GetObject("guiConditionDownBtn.Image");
      this.guiConditionDownBtn.Location = new Point(496, 250);
      this.guiConditionDownBtn.Margin = new Padding(1);
      this.guiConditionDownBtn.Name = "guiConditionDownBtn";
      this.guiConditionDownBtn.Size = new Size(39, 35);
      this.guiConditionDownBtn.TabIndex = 20;
      this.guiConditionDownBtn.UseVisualStyleBackColor = true;
      this.guiConditionDownBtn.Click += new EventHandler(this.onSimpleChanged);
      this.label1.AutoSize = true;
      this.label1.Location = new Point(190, 119);
      this.label1.Name = "label1";
      this.label1.Size = new Size(97, 13);
      this.label1.TabIndex = 27;
      this.label1.Text = "Min Security Level:";
      this.minSecuirtLevel.AutoCompleteMode = AutoCompleteMode.Suggest;
      this.minSecuirtLevel.AutoCompleteSource = AutoCompleteSource.ListItems;
      this.minSecuirtLevel.FormattingEnabled = true;
      this.minSecuirtLevel.Location = new Point(293, 116);
      this.minSecuirtLevel.Name = "minSecuirtLevel";
      this.minSecuirtLevel.Size = new Size(161, 21);
      this.minSecuirtLevel.TabIndex = 28;
      this.minSecuirtLevel.SelectedIndexChanged += new EventHandler(this.onChanged);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.AutoSize = true;
      this.BackColor = Color.AntiqueWhite;
      this.Controls.Add((Control) this.minSecuirtLevel);
      this.Controls.Add((Control) this.label1);
      this.Controls.Add((Control) this.guiConditionUpBtn);
      this.Controls.Add((Control) this.guiConditionDownBtn);
      this.Controls.Add((Control) this.guiStageUpBtn);
      this.Controls.Add((Control) this.guiStageDownBtn);
      this.Controls.Add((Control) this.guiKeyBtn);
      this.Controls.Add((Control) this.guiKeyTxt);
      this.Controls.Add((Control) this.guiKeyLbl);
      this.Controls.Add((Control) this.guiTypeLbl);
      this.Controls.Add((Control) this.guiTypeCbo);
      this.Controls.Add((Control) this.guiConditionsEditBtn);
      this.Controls.Add((Control) this.guiStagesRemoveBtn);
      this.Controls.Add((Control) this.guiStagesAddBtn);
      this.Controls.Add((Control) this.guiStagesLbl);
      this.Controls.Add((Control) this.guiStagesTbl);
      this.Controls.Add((Control) this.guiConditionsRemoveBtn);
      this.Controls.Add((Control) this.guiConditionsAddBtn);
      this.Controls.Add((Control) this.guiConditionsLbl);
      this.Controls.Add((Control) this.guiConditionsTbl);
      this.Controls.Add((Control) this.guiSummaryTxt);
      this.Controls.Add((Control) this.guiSummaryLbl);
      this.Controls.Add((Control) this.guiNameTxt);
      this.Controls.Add((Control) this.guiNameLbl);
      this.Controls.Add((Control) this.guiForfeitableChk);
      this.Controls.Add((Control) this.guiTimeTxt);
      this.Controls.Add((Control) this.guiTimeLbl);
      this.Controls.Add((Control) this.guiIdTxt);
      this.Controls.Add((Control) this.guiIdLbl);
      this.Name = nameof (TabMission);
      this.Size = new Size(550, 525);
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public TabMission(FrmMission parent)
    {
      this.InitializeComponent();
      this.m_parent = parent;
      this.m_mission = (Mission) null;
      this.m_dlgConditions = new DlgConditions();
      this.m_dlgStages = new DlgStages();
      Enumeration.AddSortedByName<MissionType>(this.guiTypeCbo);
      Enumeration.AddSortedByName<SecLevels>(this.minSecuirtLevel);
      TableButtonHandler tableButtonHandler1 = new TableButtonHandler(this.guiConditionsTbl, this.guiConditionsAddBtn, this.guiConditionsRemoveBtn, this.guiConditionsEditBtn, this.guiConditionUpBtn, this.guiConditionDownBtn);
      this.guiConditionUpBtn.Click += new EventHandler(this.onConditionReordered);
      this.guiConditionDownBtn.Click += new EventHandler(this.onConditionReordered);
      TableButtonHandler tableButtonHandler2 = new TableButtonHandler(this.guiStagesTbl, this.guiStagesAddBtn, this.guiStagesRemoveBtn, (Button) null, this.guiStageUpBtn, this.guiStageDownBtn);
      this.guiStageUpBtn.Click += new EventHandler(this.onStageReordered);
      this.guiStageDownBtn.Click += new EventHandler(this.onStageReordered);
    }

    public void setMission(Mission mission)
    {
      this.m_mission = mission;
      this.guiIdTxt.Text = this.m_mission.getId();
      this.minSecuirtLevel.SelectedItem = (object) this.m_mission.getMinSecLvl();
      this.guiTypeCbo.SelectedItem = (object) this.m_mission.getType();
      this.guiKeyTxt.Text = this.m_mission.getKey();
      this.guiNameTxt.Text = this.m_mission.getName();
      this.guiSummaryTxt.Text = this.m_mission.getSummary();
      this.guiTimeTxt.Text = this.m_mission.getAllowedTime().ToString();
      this.guiForfeitableChk.Checked = this.m_mission.isForfeitable();
      this.guiConditionsTbl.Items.Clear();
      if (this.m_mission.hasConditions())
      {
        foreach (Condition condition in this.m_mission.getConditions())
        {
          ListViewItem listViewItem = new ListViewItem(new string[2]
          {
            condition.getConditionType().ToString(),
            condition.getFormattedValue()
          });
          this.guiConditionsTbl.Items.Add(listViewItem);
          listViewItem.Tag = (object) condition;
        }
        this.guiConditionsTbl.Items[0].Selected = true;
      }
      this.guiStagesTbl.Items.Clear();
      if (!this.m_mission.hasStages())
        return;
      foreach (Stage stage in this.m_mission.getStages())
      {
        ListViewItem listViewItem = new ListViewItem(new string[2]
        {
          stage.getId(),
          stage.getDescription()
        });
        this.guiStagesTbl.Items.Add(listViewItem);
        listViewItem.Tag = (object) stage;
      }
      this.guiStagesTbl.Items[0].Selected = true;
    }

    public void setMissionType(MissionType missionType, string missionKey)
    {
      this.m_mission.setType(missionType);
      this.m_mission.setKey(missionKey);
      this.guiTypeCbo.SelectedItem = (object) missionType;
      this.guiKeyTxt.Text = missionKey;
    }

    public ListView getStageTable()
    {
      return this.guiStagesTbl;
    }

    private void onSimpleChanged(object sender, EventArgs e)
    {
      this.m_parent.onChanged();
    }

    private void onChanged(object sender, EventArgs e)
    {
      string name = ((Control) sender).Name;
      if (name.Equals(this.guiKeyTxt.Name))
        this.m_mission.setKey(this.guiKeyTxt.Text);
      else if (name.Equals(this.guiNameTxt.Name))
        this.m_mission.setName(this.guiNameTxt.Text);
      else if (name.Equals(this.minSecuirtLevel.Name))
        this.m_mission.setMinSecLvl((SecLevels) this.minSecuirtLevel.SelectedItem);
      else if (name.Equals(this.guiTypeCbo.Name))
        this.m_mission.setType((MissionType) this.guiTypeCbo.SelectedItem);
      else if (name.Equals(this.guiKeyTxt.Name))
        this.m_mission.setKey(this.guiKeyTxt.Text);
      else if (name.Equals(this.guiSummaryTxt.Name))
        this.m_mission.setSummary(this.guiSummaryTxt.Text);
      else if (name.Equals(this.guiTimeTxt.Name))
      {
        int result;
        if (!int.TryParse(this.guiTimeTxt.Text, out result))
          result = 0;
        this.m_mission.setAllowedTime(result);
      }
      else if (name.Equals(this.guiForfeitableChk.Name))
        this.m_mission.setForfeitable(this.guiForfeitableChk.Checked);
      this.m_parent.onChanged();
    }

    private void onTimeKeyPress(object sender, KeyPressEventArgs e)
    {
      if (e.KeyChar >= '0' && e.KeyChar <= '9')
        return;
      e.Handled = true;
    }

    private void onConditionSelected(object sender, EventArgs e)
    {
      int num = (int) MessageBox.Show(nameof (onConditionSelected));
      this.guiConditionsRemoveBtn.Enabled = ((ListView) sender).SelectedItems.Count != 0;
    }

    private void onConditionAdd(object sender, EventArgs e)
    {
      int num = (int) this.m_dlgConditions.ShowDialog();
      Condition condition;
      if (!this.m_dlgConditions.getValues(out condition))
        return;
      this.m_parent.onChanged();
      this.m_mission.addCondition(condition);
      this.guiConditionsTbl.Items.Add(new ListViewItem(new string[2]
      {
        condition.getConditionType().ToString(),
        condition.getFormattedValue()
      })
      {
        Tag = (object) condition
      });
    }

    private void onConditionEdit(object sender, EventArgs e)
    {
      this.m_dlgConditions.editCondition((Condition) this.guiConditionsTbl.SelectedItems[0].Tag);
      int num = (int) this.m_dlgConditions.ShowDialog();
      Condition condition;
      if (!this.m_dlgConditions.getValues(out condition))
        return;
      this.m_parent.onChanged();
      this.guiConditionsTbl.SelectedItems[0].Tag = (object) condition;
    }

    private void onConditionRemove(object sender, EventArgs e)
    {
      this.m_parent.onChanged();
      foreach (ListViewItem selectedItem in this.guiConditionsTbl.SelectedItems)
      {
        this.m_mission.removeCondition((Condition) selectedItem.Tag);
        this.guiConditionsTbl.Items.Remove(selectedItem);
      }
    }

    private void onConditionReordered(object sender, EventArgs e)
    {
      this.m_mission.clearConditions();
      int index1 = this.guiConditionsTbl.SelectedItems[0].Index;
      for (int index2 = 0; index2 < this.guiConditionsTbl.Items.Count; ++index2)
      {
        Condition tag = (Condition) this.guiConditionsTbl.Items[index2].Tag;
        this.guiConditionsTbl.Items.RemoveAt(index2);
        ListViewItem listViewItem = new ListViewItem(new string[2]
        {
          tag.getConditionType().ToString(),
          tag.getFormattedValue()
        });
        listViewItem.Tag = (object) tag;
        this.guiConditionsTbl.Items.Insert(index2, listViewItem);
        if (index2 == index1)
          listViewItem.Selected = true;
        this.m_mission.addCondition(tag);
      }
      this.m_parent.onChanged();
    }

    private void onStageSelected(object sender, EventArgs e)
    {
      if (this.guiStagesTbl.SelectedIndices.Count != 1)
        return;
      this.m_parent.onStageSelected((Stage) this.guiStagesTbl.SelectedItems[0].Tag);
    }

    public void addStage(bool showDialog)
    {
      int id = 0;
      if (this.guiStagesTbl.Items.Count != 0)
        id = int.Parse(((Stage) this.guiStagesTbl.Items[this.guiStagesTbl.Items.Count - 1].Tag).getId()) + 1;
      Stage stage = (Stage) null;
      bool flag = true;
      if (showDialog)
      {
        this.m_dlgStages.setId(id);
        int num = (int) this.m_dlgStages.ShowDialog();
        flag = this.m_dlgStages.getValues(out stage);
      }
      else
      {
        stage = new Stage();
        stage.setId(id.ToString());
      }
      if (!flag)
        return;
      this.m_parent.onChanged();
      this.m_mission.addStage(stage);
      ListViewItem listViewItem = new ListViewItem(new string[2]
      {
        stage.getId(),
        stage.getDescription()
      });
      listViewItem.Tag = (object) stage;
      this.guiStagesTbl.Items.Add(listViewItem);
      this.m_parent.onChangedStages();
      listViewItem.Selected = true;
    }

    private void onStageAdd(object sender, EventArgs e)
    {
      this.addStage(this.guiStagesTbl.Items.Count != 0);
    }

    private void onStageEdit(object sender, EventArgs e)
    {
      this.m_dlgStages.editStage((Stage) this.guiStagesTbl.SelectedItems[0].Tag);
      this.m_parent.onChanged();
    }

    private void onStageRemove(object sender, EventArgs e)
    {
      foreach (ListViewItem selectedItem in this.guiStagesTbl.SelectedItems)
      {
        int index = selectedItem.Index;
        this.m_mission.removeStage((Stage) selectedItem.Tag);
        this.guiStagesTbl.Items.Remove(selectedItem);
        if (index >= this.guiStagesTbl.Items.Count)
          --index;
        if (index != -1)
        {
          this.guiStagesTbl.Items[index].Selected = true;
          this.onStageReordered((object) null, (EventArgs) null);
        }
        else
        {
          this.m_parent.onChanged();
          this.m_parent.onChangedStages();
        }
      }
    }

    private void onStageReordered(object sender, EventArgs e)
    {
      this.m_mission.clearStages();
      int index1 = this.guiStagesTbl.SelectedItems[0].Index;
      for (int index2 = 0; index2 < this.guiStagesTbl.Items.Count; ++index2)
      {
        Stage tag = (Stage) this.guiStagesTbl.Items[index2].Tag;
        tag.setId(index2.ToString());
        this.guiStagesTbl.Items.RemoveAt(index2);
        ListViewItem listViewItem = new ListViewItem(new string[2]
        {
          tag.getId(),
          tag.getDescription()
        });
        listViewItem.Tag = (object) tag;
        this.guiStagesTbl.Items.Insert(index2, listViewItem);
        if (index2 == index1)
          listViewItem.Selected = true;
        this.m_mission.addStage(tag);
      }
      this.m_parent.onChanged();
      this.m_parent.onChangedStages();
    }

    private void onKeySearch(object sender, EventArgs e)
    {
      string str = "";
      switch ((MissionType) this.guiTypeCbo.SelectedItem)
      {
        case MissionType.Npc:
          str = DataConfiguration.search(DataConfiguration.DataType.npc);
          break;
        case MissionType.Sector:
          str = DataConfiguration.search(DataConfiguration.DataType.sector);
          break;
      }
      if (str.Length == 0)
        return;
      this.guiKeyTxt.Text = str;
    }
  }
}
