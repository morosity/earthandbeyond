﻿// Decompiled with JetBrains decompiler
// Type: MissionEditor.Gui.DlgConditions
// Assembly: MissionEditor, Version=1.6.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 00D14066-6848-4104-A662-B9B4BF67FB6E
// Assembly location: D:\Server\eab\Tools\MissionEditor.exe

using CommonTools;
using MissionEditor.Database;
using MissionEditor.Nodes;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace MissionEditor.Gui
{
  public class DlgConditions : Form
  {
    private bool m_madeSelection;
    private Condition m_condition;
    private CodeDescSearch m_codeSearch;
    private IContainer components;
    private Label guiTypeLbl;
    private ComboBox guiTypeCbo;
    private ComboBox guiValueCbo;
    private Label guiValueLbl;
    private Button guiOkBtn;
    private Button guiCancelBtn;
    private TextBox guiCodeTxt;
    private Button guiCodeSearchBtn;
    private TextBox guiCodeDescriptionTxt;
    private Label guiCodeLbl;
    private Label guiAmountLbl;
    private TextBox guiAmountTxt;

    public DlgConditions()
    {
      this.InitializeComponent();
      this.m_condition = (Condition) null;
      this.m_codeSearch = new CodeDescSearch(this.guiCodeTxt, this.guiCodeDescriptionTxt, this.guiCodeSearchBtn);
      this.guiTypeCbo.Items.Clear();
      Enumeration.AddSortedByName<ConditionType>(this.guiTypeCbo);
      this.guiTypeCbo.SelectedIndex = 0;
    }

    protected override void OnShown(EventArgs e)
    {
      base.OnShown(e);
      this.m_madeSelection = false;
      this.guiTypeCbo.Focus();
      this.guiTypeCbo.SelectedIndex = 0;
    }

    public void editCondition(Condition condition)
    {
      this.m_condition = condition;
      this.guiTypeCbo.SelectedItem = (object) this.m_condition.getConditionType();
      switch (this.m_condition.getConditionType())
      {
        case ConditionType.Overall_Level:
        case ConditionType.Combat_Level:
        case ConditionType.Explore_Level:
        case ConditionType.Trade_Level:
        case ConditionType.Hull_Level:
          this.guiAmountTxt.Text = this.m_condition.getValue();
          break;
        case ConditionType.Race:
          this.guiValueCbo.SelectedItem = (object) (Races) int.Parse(this.m_condition.getValue());
          break;
        case ConditionType.Profession:
          this.guiValueCbo.SelectedItem = (object) (Professions) int.Parse(this.m_condition.getValue());
          break;
        case ConditionType.Faction_Required:
          this.guiCodeTxt.Text = this.m_condition.getValue();
          this.guiAmountTxt.Text = this.m_condition.getFlag();
          this.m_codeSearch.setDataType(DataConfiguration.DataType.faction);
          break;
        case ConditionType.Item_Required:
          this.guiCodeTxt.Text = this.m_condition.getValue();
          this.guiAmountTxt.Text = this.m_condition.getFlag();
          this.m_codeSearch.setDataType(DataConfiguration.DataType.item);
          break;
      }
    }

    private void onTypeSelected(object sender, EventArgs e)
    {
      this.guiValueCbo.Items.Clear();
      this.guiValueCbo.Enabled = false;
      this.guiCodeTxt.Enabled = false;
      this.guiCodeSearchBtn.Enabled = false;
      this.guiAmountTxt.Enabled = false;
      switch ((ConditionType) this.guiTypeCbo.SelectedItem)
      {
        case ConditionType.Overall_Level:
        case ConditionType.Combat_Level:
        case ConditionType.Explore_Level:
        case ConditionType.Trade_Level:
        case ConditionType.Hull_Level:
          this.guiAmountTxt.Enabled = true;
          break;
        case ConditionType.Race:
          this.guiValueCbo.Enabled = true;
          this.guiValueCbo.Items.Clear();
          Enumeration.AddSortedByName<Races>(this.guiValueCbo);
          this.guiValueCbo.SelectedIndex = 0;
          break;
        case ConditionType.Profession:
          this.guiValueCbo.Enabled = true;
          this.guiValueCbo.Items.Clear();
          Enumeration.AddSortedByName<Professions>(this.guiValueCbo);
          this.guiValueCbo.SelectedIndex = 0;
          break;
        case ConditionType.Faction_Required:
          this.guiCodeTxt.Enabled = true;
          this.guiCodeSearchBtn.Enabled = true;
          this.guiAmountTxt.Enabled = true;
          this.m_codeSearch.setDataType(DataConfiguration.DataType.faction);
          break;
        case ConditionType.Item_Required:
          this.guiCodeTxt.Enabled = true;
          this.guiCodeSearchBtn.Enabled = true;
          this.guiAmountTxt.Enabled = true;
          this.m_codeSearch.setDataType(DataConfiguration.DataType.item);
          break;
        case ConditionType.Mission_Required:
          this.guiCodeTxt.Enabled = true;
          this.guiCodeSearchBtn.Enabled = true;
          this.m_codeSearch.setDataType(DataConfiguration.DataType.mission);
          break;
      }
    }

    private void onOk(object sender, EventArgs e)
    {
      this.m_condition = new Condition();
      ConditionType selectedItem = (ConditionType) this.guiTypeCbo.SelectedItem;
      this.m_condition.setConditionType(selectedItem);
      switch (selectedItem)
      {
        case ConditionType.Overall_Level:
        case ConditionType.Combat_Level:
        case ConditionType.Explore_Level:
        case ConditionType.Trade_Level:
        case ConditionType.Hull_Level:
          this.m_condition.setValue(this.guiAmountTxt.Text);
          break;
        case ConditionType.Race:
          this.m_condition.setValue(((int) this.guiValueCbo.SelectedItem).ToString());
          break;
        case ConditionType.Profession:
          this.m_condition.setValue(((int) this.guiValueCbo.SelectedItem).ToString());
          break;
        case ConditionType.Faction_Required:
        case ConditionType.Item_Required:
          this.m_condition.setValue(this.guiAmountTxt.Text);
          this.m_condition.setFlag(this.guiCodeTxt.Text);
          break;
        case ConditionType.Mission_Required:
          this.m_condition.setValue(this.guiCodeTxt.Text);
          break;
      }
      this.m_condition.addValidations();
      string errorMessage;
      if (DataConfiguration.validate(out errorMessage))
      {
        this.m_madeSelection = true;
        this.Close();
      }
      else
      {
        int num = (int) MessageBox.Show(errorMessage);
      }
    }

    private void onCancel(object sender, EventArgs e)
    {
      this.Close();
    }

    public bool getValues(out Condition condition)
    {
      condition = this.m_condition;
      return this.m_madeSelection;
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.guiTypeLbl = new Label();
      this.guiTypeCbo = new ComboBox();
      this.guiValueCbo = new ComboBox();
      this.guiValueLbl = new Label();
      this.guiOkBtn = new Button();
      this.guiCancelBtn = new Button();
      this.guiCodeTxt = new TextBox();
      this.guiCodeSearchBtn = new Button();
      this.guiCodeDescriptionTxt = new TextBox();
      this.guiCodeLbl = new Label();
      this.guiAmountLbl = new Label();
      this.guiAmountTxt = new TextBox();
      this.SuspendLayout();
      this.guiTypeLbl.AutoSize = true;
      this.guiTypeLbl.Location = new Point(23, 13);
      this.guiTypeLbl.Name = "guiTypeLbl";
      this.guiTypeLbl.Size = new Size(34, 13);
      this.guiTypeLbl.TabIndex = 0;
      this.guiTypeLbl.Text = "Type:";
      this.guiTypeCbo.DropDownStyle = ComboBoxStyle.DropDownList;
      this.guiTypeCbo.FormattingEnabled = true;
      this.guiTypeCbo.Location = new Point(63, 10);
      this.guiTypeCbo.Name = "guiTypeCbo";
      this.guiTypeCbo.Size = new Size(338, 21);
      this.guiTypeCbo.TabIndex = 1;
      this.guiTypeCbo.SelectedIndexChanged += new EventHandler(this.onTypeSelected);
      this.guiValueCbo.DropDownStyle = ComboBoxStyle.DropDownList;
      this.guiValueCbo.FormattingEnabled = true;
      this.guiValueCbo.Location = new Point(63, 37);
      this.guiValueCbo.Name = "guiValueCbo";
      this.guiValueCbo.Size = new Size(338, 21);
      this.guiValueCbo.TabIndex = 3;
      this.guiValueLbl.AutoSize = true;
      this.guiValueLbl.Location = new Point(20, 40);
      this.guiValueLbl.Name = "guiValueLbl";
      this.guiValueLbl.Size = new Size(37, 13);
      this.guiValueLbl.TabIndex = 2;
      this.guiValueLbl.Text = "Value:";
      this.guiOkBtn.Location = new Point(245, 116);
      this.guiOkBtn.Name = "guiOkBtn";
      this.guiOkBtn.Size = new Size(75, 23);
      this.guiOkBtn.TabIndex = 10;
      this.guiOkBtn.Text = "Ok";
      this.guiOkBtn.UseVisualStyleBackColor = true;
      this.guiOkBtn.Click += new EventHandler(this.onOk);
      this.guiCancelBtn.Location = new Point(326, 116);
      this.guiCancelBtn.Name = "guiCancelBtn";
      this.guiCancelBtn.Size = new Size(75, 23);
      this.guiCancelBtn.TabIndex = 11;
      this.guiCancelBtn.Text = "Cancel";
      this.guiCancelBtn.UseVisualStyleBackColor = true;
      this.guiCancelBtn.Click += new EventHandler(this.onCancel);
      this.guiCodeTxt.Location = new Point(63, 64);
      this.guiCodeTxt.Name = "guiCodeTxt";
      this.guiCodeTxt.Size = new Size(43, 20);
      this.guiCodeTxt.TabIndex = 5;
      this.guiCodeSearchBtn.Location = new Point(349, 62);
      this.guiCodeSearchBtn.Name = "guiCodeSearchBtn";
      this.guiCodeSearchBtn.Size = new Size(52, 23);
      this.guiCodeSearchBtn.TabIndex = 7;
      this.guiCodeSearchBtn.Text = "Search";
      this.guiCodeSearchBtn.UseVisualStyleBackColor = true;
      this.guiCodeDescriptionTxt.Location = new Point(112, 64);
      this.guiCodeDescriptionTxt.Name = "guiCodeDescriptionTxt";
      this.guiCodeDescriptionTxt.ReadOnly = true;
      this.guiCodeDescriptionTxt.Size = new Size(231, 20);
      this.guiCodeDescriptionTxt.TabIndex = 6;
      this.guiCodeDescriptionTxt.TabStop = false;
      this.guiCodeLbl.AutoSize = true;
      this.guiCodeLbl.Location = new Point(19, 67);
      this.guiCodeLbl.Name = "guiCodeLbl";
      this.guiCodeLbl.Size = new Size(35, 13);
      this.guiCodeLbl.TabIndex = 4;
      this.guiCodeLbl.Text = "Code:";
      this.guiAmountLbl.AutoSize = true;
      this.guiAmountLbl.Location = new Point(11, 93);
      this.guiAmountLbl.Name = "guiAmountLbl";
      this.guiAmountLbl.Size = new Size(46, 13);
      this.guiAmountLbl.TabIndex = 8;
      this.guiAmountLbl.Text = "Amount:";
      this.guiAmountTxt.Location = new Point(63, 90);
      this.guiAmountTxt.Name = "guiAmountTxt";
      this.guiAmountTxt.Size = new Size(88, 20);
      this.guiAmountTxt.TabIndex = 9;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(413, 150);
      this.Controls.Add((Control) this.guiAmountLbl);
      this.Controls.Add((Control) this.guiAmountTxt);
      this.Controls.Add((Control) this.guiCodeLbl);
      this.Controls.Add((Control) this.guiCodeDescriptionTxt);
      this.Controls.Add((Control) this.guiCodeSearchBtn);
      this.Controls.Add((Control) this.guiCodeTxt);
      this.Controls.Add((Control) this.guiCancelBtn);
      this.Controls.Add((Control) this.guiOkBtn);
      this.Controls.Add((Control) this.guiValueCbo);
      this.Controls.Add((Control) this.guiValueLbl);
      this.Controls.Add((Control) this.guiTypeCbo);
      this.Controls.Add((Control) this.guiTypeLbl);
      this.Name = nameof (DlgConditions);
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "Conditions";
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
