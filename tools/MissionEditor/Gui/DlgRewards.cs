﻿// Decompiled with JetBrains decompiler
// Type: MissionEditor.Gui.DlgRewards
// Assembly: MissionEditor, Version=1.6.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 00D14066-6848-4104-A662-B9B4BF67FB6E
// Assembly location: D:\Server\eab\Tools\MissionEditor.exe

using CommonTools;
using MissionEditor.Database;
using MissionEditor.Nodes;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace MissionEditor.Gui
{
  public class DlgRewards : Form
  {
    private bool m_madeSelection;
    private Reward m_reward;
    private CodeDescSearch m_codeSearch;
    private IContainer components;
    private Label guiTypeLbl;
    private ComboBox guiTypeCbo;
    private Button guiOkBtn;
    private Button guiCancelBtn;
    private TextBox guiAmountTxt;
    private Label guiCodeLbl;
    private TextBox guiCodeDescriptionTxt;
    private Button guiCodeSearchBtn;
    private TextBox guiCodeTxt;
    private Label guiAmountLbl;

    public DlgRewards()
    {
      this.InitializeComponent();
      this.m_reward = (Reward) null;
      this.m_codeSearch = new CodeDescSearch(this.guiCodeTxt, this.guiCodeDescriptionTxt, this.guiCodeSearchBtn);
      this.guiTypeCbo.Items.Clear();
      Enumeration.AddSortedByName<RewardType>(this.guiTypeCbo);
      this.guiTypeCbo.SelectedIndex = 0;
    }

    protected override void OnShown(EventArgs e)
    {
      base.OnShown(e);
      this.m_madeSelection = false;
      this.guiTypeCbo.Focus();
    }

    public void editReward(Reward reward)
    {
      this.m_reward = reward;
    }

    private void onTypeSelected(object sender, EventArgs e)
    {
      this.guiAmountTxt.Enabled = false;
      this.guiCodeTxt.Enabled = false;
      this.guiCodeSearchBtn.Enabled = false;
      switch ((RewardType) this.guiTypeCbo.SelectedItem)
      {
        case RewardType.Credits:
        case RewardType.Explore_XP:
        case RewardType.Combat_XP:
        case RewardType.Trade_XP:
          this.guiAmountTxt.Enabled = true;
          break;
        case RewardType.Faction:
          this.guiCodeTxt.Enabled = true;
          this.guiCodeSearchBtn.Enabled = true;
          this.guiAmountTxt.Enabled = true;
          this.m_codeSearch.setDataType(DataConfiguration.DataType.faction);
          break;
        case RewardType.Item_ID:
          this.guiCodeTxt.Enabled = true;
          this.guiCodeSearchBtn.Enabled = true;
          this.m_codeSearch.setDataType(DataConfiguration.DataType.item);
          break;
        case RewardType.Hull_Upgrade:
          this.guiAmountTxt.Enabled = true;
          break;
        case RewardType.Run_Script:
          this.guiAmountTxt.Enabled = true;
          break;
        case RewardType.Award_Skill:
          this.guiCodeTxt.Enabled = true;
          this.guiCodeSearchBtn.Enabled = true;
          this.m_codeSearch.setDataType(DataConfiguration.DataType.skill);
          break;
        case RewardType.Advance_Mission:
          this.guiCodeTxt.Enabled = true;
          this.guiCodeSearchBtn.Enabled = true;
          this.m_codeSearch.setDataType(DataConfiguration.DataType.mission);
          break;
        case RewardType.Item_Blueprint:
          this.guiCodeTxt.Enabled = true;
          this.guiCodeSearchBtn.Enabled = true;
          this.m_codeSearch.setDataType(DataConfiguration.DataType.item);
          break;
        case RewardType.Play_Sound_Reward:
          this.guiCodeTxt.Enabled = true;
          this.guiCodeSearchBtn.Enabled = false;
          break;
      }
    }

    private void onOk(object sender, EventArgs e)
    {
      this.m_reward = new Reward();
      RewardType selectedItem = (RewardType) this.guiTypeCbo.SelectedItem;
      this.m_reward.setRewardType(selectedItem);
      switch (selectedItem)
      {
        case RewardType.Credits:
        case RewardType.Explore_XP:
        case RewardType.Combat_XP:
        case RewardType.Trade_XP:
          this.m_reward.setValue(this.guiAmountTxt.Text);
          break;
        case RewardType.Faction:
          this.m_reward.setValue(this.guiAmountTxt.Text);
          this.m_reward.setFlag(this.guiCodeTxt.Text);
          break;
        case RewardType.Item_ID:
        case RewardType.Item_Blueprint:
          this.m_reward.setValue(this.guiCodeTxt.Text);
          break;
        case RewardType.Hull_Upgrade:
        case RewardType.Run_Script:
          this.m_reward.setValue(this.guiAmountTxt.Text);
          break;
        case RewardType.Award_Skill:
          this.m_reward.setValue(this.guiCodeTxt.Text);
          break;
        case RewardType.Advance_Mission:
          this.m_reward.setValue(this.guiCodeTxt.Text);
          break;
        case RewardType.Play_Sound_Reward:
          this.m_reward.setCharData(this.guiCodeTxt.Text);
          this.m_reward.setValue(this.guiCodeTxt.Text);
          break;
      }
      this.m_reward.addValidations();
      string errorMessage;
      if (DataConfiguration.validate(out errorMessage))
      {
        this.m_madeSelection = true;
        this.Close();
      }
      else
      {
        int num = (int) MessageBox.Show(errorMessage);
      }
    }

    private void onCancel(object sender, EventArgs e)
    {
      this.Close();
    }

    public bool getValues(out Reward reward)
    {
      reward = this.m_reward;
      return this.m_madeSelection;
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.guiTypeLbl = new Label();
      this.guiTypeCbo = new ComboBox();
      this.guiOkBtn = new Button();
      this.guiCancelBtn = new Button();
      this.guiAmountTxt = new TextBox();
      this.guiCodeLbl = new Label();
      this.guiCodeDescriptionTxt = new TextBox();
      this.guiCodeSearchBtn = new Button();
      this.guiCodeTxt = new TextBox();
      this.guiAmountLbl = new Label();
      this.SuspendLayout();
      this.guiTypeLbl.AutoSize = true;
      this.guiTypeLbl.Location = new Point(20, 13);
      this.guiTypeLbl.Name = "guiTypeLbl";
      this.guiTypeLbl.Size = new Size(34, 13);
      this.guiTypeLbl.TabIndex = 0;
      this.guiTypeLbl.Text = "Type:";
      this.guiTypeCbo.DropDownStyle = ComboBoxStyle.DropDownList;
      this.guiTypeCbo.FormattingEnabled = true;
      this.guiTypeCbo.Location = new Point(60, 10);
      this.guiTypeCbo.Name = "guiTypeCbo";
      this.guiTypeCbo.Size = new Size(341, 21);
      this.guiTypeCbo.TabIndex = 1;
      this.guiTypeCbo.SelectedIndexChanged += new EventHandler(this.onTypeSelected);
      this.guiOkBtn.Location = new Point(245, 115);
      this.guiOkBtn.Name = "guiOkBtn";
      this.guiOkBtn.Size = new Size(75, 23);
      this.guiOkBtn.TabIndex = 8;
      this.guiOkBtn.Text = "Ok";
      this.guiOkBtn.UseVisualStyleBackColor = true;
      this.guiOkBtn.Click += new EventHandler(this.onOk);
      this.guiCancelBtn.Location = new Point(326, 115);
      this.guiCancelBtn.Name = "guiCancelBtn";
      this.guiCancelBtn.Size = new Size(75, 23);
      this.guiCancelBtn.TabIndex = 9;
      this.guiCancelBtn.Text = "Cancel";
      this.guiCancelBtn.UseVisualStyleBackColor = true;
      this.guiCancelBtn.Click += new EventHandler(this.onCancel);
      this.guiAmountTxt.Location = new Point(60, 37);
      this.guiAmountTxt.Name = "guiAmountTxt";
      this.guiAmountTxt.Size = new Size(71, 20);
      this.guiAmountTxt.TabIndex = 3;
      this.guiCodeLbl.AutoSize = true;
      this.guiCodeLbl.Location = new Point(19, 68);
      this.guiCodeLbl.Name = "guiCodeLbl";
      this.guiCodeLbl.Size = new Size(35, 13);
      this.guiCodeLbl.TabIndex = 4;
      this.guiCodeLbl.Text = "Code:";
      this.guiCodeDescriptionTxt.Location = new Point(108, 65);
      this.guiCodeDescriptionTxt.Name = "guiCodeDescriptionTxt";
      this.guiCodeDescriptionTxt.ReadOnly = true;
      this.guiCodeDescriptionTxt.Size = new Size(235, 20);
      this.guiCodeDescriptionTxt.TabIndex = 6;
      this.guiCodeDescriptionTxt.TabStop = false;
      this.guiCodeSearchBtn.Location = new Point(349, 63);
      this.guiCodeSearchBtn.Name = "guiCodeSearchBtn";
      this.guiCodeSearchBtn.Size = new Size(52, 23);
      this.guiCodeSearchBtn.TabIndex = 7;
      this.guiCodeSearchBtn.Text = "Search";
      this.guiCodeSearchBtn.UseVisualStyleBackColor = true;
      this.guiCodeTxt.Location = new Point(60, 65);
      this.guiCodeTxt.Name = "guiCodeTxt";
      this.guiCodeTxt.Size = new Size(43, 20);
      this.guiCodeTxt.TabIndex = 5;
      this.guiAmountLbl.AutoSize = true;
      this.guiAmountLbl.Location = new Point(12, 40);
      this.guiAmountLbl.Name = "guiAmountLbl";
      this.guiAmountLbl.Size = new Size(46, 13);
      this.guiAmountLbl.TabIndex = 2;
      this.guiAmountLbl.Text = "Amount:";
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(413, 150);
      this.Controls.Add((Control) this.guiCodeLbl);
      this.Controls.Add((Control) this.guiCodeDescriptionTxt);
      this.Controls.Add((Control) this.guiCodeSearchBtn);
      this.Controls.Add((Control) this.guiCodeTxt);
      this.Controls.Add((Control) this.guiAmountLbl);
      this.Controls.Add((Control) this.guiAmountTxt);
      this.Controls.Add((Control) this.guiCancelBtn);
      this.Controls.Add((Control) this.guiOkBtn);
      this.Controls.Add((Control) this.guiTypeCbo);
      this.Controls.Add((Control) this.guiTypeLbl);
      this.Name = nameof (DlgRewards);
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "Rewards";
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
