﻿// Decompiled with JetBrains decompiler
// Type: MissionEditor.Gui.DlgCompletions
// Assembly: MissionEditor, Version=1.6.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 00D14066-6848-4104-A662-B9B4BF67FB6E
// Assembly location: D:\Server\eab\Tools\MissionEditor.exe

using CommonTools;
using MissionEditor.Database;
using MissionEditor.Nodes;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace MissionEditor.Gui
{
  public class DlgCompletions : Form
  {
    private IContainer components;
    private Label guiTypeLbl;
    private ComboBox guiTypeCbo;
    private Button guiOkBtn;
    private Button guiCancelBtn;
    private TextBox guiValueTxt;
    private Label guiAmountLbl;
    private Label guiValueLbl;
    private TextBox guiValueDescriptionTxt;
    private Button guiValueSearchBtn;
    private TextBox guiAmountTxt;
    private Label guiDataLbl;
    private TextBox guiDataDescriptionTxt;
    private Button guiDataSearchBtn;
    private TextBox guiDataTxt;
    private bool m_madeSelection;
    private Completion m_completion;
    private CodeDescSearch[] m_codeSearch;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.guiTypeLbl = new Label();
      this.guiTypeCbo = new ComboBox();
      this.guiOkBtn = new Button();
      this.guiCancelBtn = new Button();
      this.guiValueTxt = new TextBox();
      this.guiAmountLbl = new Label();
      this.guiValueLbl = new Label();
      this.guiValueDescriptionTxt = new TextBox();
      this.guiValueSearchBtn = new Button();
      this.guiAmountTxt = new TextBox();
      this.guiDataLbl = new Label();
      this.guiDataDescriptionTxt = new TextBox();
      this.guiDataSearchBtn = new Button();
      this.guiDataTxt = new TextBox();
      this.SuspendLayout();
      this.guiTypeLbl.AutoSize = true;
      this.guiTypeLbl.Location = new Point(23, 13);
      this.guiTypeLbl.Name = "guiTypeLbl";
      this.guiTypeLbl.Size = new Size(34, 13);
      this.guiTypeLbl.TabIndex = 0;
      this.guiTypeLbl.Text = "Type:";
      this.guiTypeCbo.DropDownStyle = ComboBoxStyle.DropDownList;
      this.guiTypeCbo.FormattingEnabled = true;
      this.guiTypeCbo.Location = new Point(63, 10);
      this.guiTypeCbo.Name = "guiTypeCbo";
      this.guiTypeCbo.Size = new Size(338, 21);
      this.guiTypeCbo.TabIndex = 1;
      this.guiTypeCbo.SelectedIndexChanged += new EventHandler(this.onTypeSelected);
      this.guiOkBtn.Location = new Point(245, 115);
      this.guiOkBtn.Name = "guiOkBtn";
      this.guiOkBtn.Size = new Size(75, 23);
      this.guiOkBtn.TabIndex = 12;
      this.guiOkBtn.Text = "Ok";
      this.guiOkBtn.UseVisualStyleBackColor = true;
      this.guiOkBtn.Click += new EventHandler(this.onOk);
      this.guiCancelBtn.Location = new Point(326, 115);
      this.guiCancelBtn.Name = "guiCancelBtn";
      this.guiCancelBtn.Size = new Size(75, 23);
      this.guiCancelBtn.TabIndex = 13;
      this.guiCancelBtn.Text = "Cancel";
      this.guiCancelBtn.UseVisualStyleBackColor = true;
      this.guiCancelBtn.Click += new EventHandler(this.onCancel);
      this.guiValueTxt.Location = new Point(63, 40);
      this.guiValueTxt.Name = "guiValueTxt";
      this.guiValueTxt.Size = new Size(43, 20);
      this.guiValueTxt.TabIndex = 3;
      this.guiAmountLbl.AutoSize = true;
      this.guiAmountLbl.Location = new Point(11, 96);
      this.guiAmountLbl.Name = "guiAmountLbl";
      this.guiAmountLbl.Size = new Size(46, 13);
      this.guiAmountLbl.TabIndex = 10;
      this.guiAmountLbl.Text = "Amount:";
      this.guiValueLbl.AutoSize = true;
      this.guiValueLbl.Location = new Point(23, 43);
      this.guiValueLbl.Name = "guiValueLbl";
      this.guiValueLbl.Size = new Size(37, 13);
      this.guiValueLbl.TabIndex = 2;
      this.guiValueLbl.Text = "Value:";
      this.guiValueDescriptionTxt.Location = new Point(111, 40);
      this.guiValueDescriptionTxt.Name = "guiValueDescriptionTxt";
      this.guiValueDescriptionTxt.ReadOnly = true;
      this.guiValueDescriptionTxt.Size = new Size(231, 20);
      this.guiValueDescriptionTxt.TabIndex = 4;
      this.guiValueDescriptionTxt.TabStop = false;
      this.guiValueSearchBtn.Location = new Point(348, 35);
      this.guiValueSearchBtn.Name = "guiValueSearchBtn";
      this.guiValueSearchBtn.Size = new Size(52, 23);
      this.guiValueSearchBtn.TabIndex = 5;
      this.guiValueSearchBtn.Text = "Search";
      this.guiValueSearchBtn.UseVisualStyleBackColor = true;
      this.guiAmountTxt.Location = new Point(63, 93);
      this.guiAmountTxt.Name = "guiAmountTxt";
      this.guiAmountTxt.Size = new Size(88, 20);
      this.guiAmountTxt.TabIndex = 11;
      this.guiDataLbl.AutoSize = true;
      this.guiDataLbl.Location = new Point(23, 69);
      this.guiDataLbl.Name = "guiDataLbl";
      this.guiDataLbl.Size = new Size(33, 13);
      this.guiDataLbl.TabIndex = 6;
      this.guiDataLbl.Text = "Data:";
      this.guiDataDescriptionTxt.Location = new Point(112, 66);
      this.guiDataDescriptionTxt.Name = "guiDataDescriptionTxt";
      this.guiDataDescriptionTxt.ReadOnly = true;
      this.guiDataDescriptionTxt.Size = new Size(231, 20);
      this.guiDataDescriptionTxt.TabIndex = 8;
      this.guiDataDescriptionTxt.TabStop = false;
      this.guiDataSearchBtn.Location = new Point(348, 64);
      this.guiDataSearchBtn.Name = "guiDataSearchBtn";
      this.guiDataSearchBtn.Size = new Size(52, 23);
      this.guiDataSearchBtn.TabIndex = 9;
      this.guiDataSearchBtn.Text = "Search";
      this.guiDataSearchBtn.UseVisualStyleBackColor = true;
      this.guiDataTxt.Location = new Point(63, 66);
      this.guiDataTxt.Name = "guiDataTxt";
      this.guiDataTxt.Size = new Size(43, 20);
      this.guiDataTxt.TabIndex = 7;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(413, 150);
      this.Controls.Add((Control) this.guiDataLbl);
      this.Controls.Add((Control) this.guiDataDescriptionTxt);
      this.Controls.Add((Control) this.guiDataSearchBtn);
      this.Controls.Add((Control) this.guiDataTxt);
      this.Controls.Add((Control) this.guiAmountTxt);
      this.Controls.Add((Control) this.guiAmountLbl);
      this.Controls.Add((Control) this.guiValueLbl);
      this.Controls.Add((Control) this.guiValueDescriptionTxt);
      this.Controls.Add((Control) this.guiValueSearchBtn);
      this.Controls.Add((Control) this.guiValueTxt);
      this.Controls.Add((Control) this.guiCancelBtn);
      this.Controls.Add((Control) this.guiOkBtn);
      this.Controls.Add((Control) this.guiTypeCbo);
      this.Controls.Add((Control) this.guiTypeLbl);
      this.Name = nameof (DlgCompletions);
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "Completions";
      this.Load += new EventHandler(this.DlgCompletions_Load);
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public DlgCompletions()
    {
      this.InitializeComponent();
      this.m_completion = (Completion) null;
      this.m_codeSearch = new CodeDescSearch[2];
      this.m_codeSearch[0] = new CodeDescSearch(this.guiValueTxt, this.guiValueDescriptionTxt, this.guiValueSearchBtn);
      this.m_codeSearch[1] = new CodeDescSearch(this.guiDataTxt, this.guiDataDescriptionTxt, this.guiDataSearchBtn);
      this.guiTypeCbo.Items.Clear();
      Enumeration.AddSortedByName<CompletionType>(this.guiTypeCbo);
      this.guiTypeCbo.SelectedIndex = 0;
    }

    protected override void OnShown(EventArgs e)
    {
      base.OnShown(e);
      this.m_madeSelection = false;
      this.guiTypeCbo.Focus();
    }

    public void editCompletion(Completion completion)
    {
      this.m_completion = completion;
      this.guiTypeCbo.SelectedItem = (object) this.m_completion.getCompletionType();
    }

    private void onTypeSelected(object sender, EventArgs e)
    {
      this.guiValueTxt.Enabled = false;
      this.guiValueSearchBtn.Enabled = false;
      this.guiDataTxt.Enabled = false;
      this.guiDataSearchBtn.Enabled = false;
      this.guiAmountTxt.Enabled = false;
      switch ((CompletionType) this.guiTypeCbo.SelectedItem)
      {
        case CompletionType.Talk_To_Npc:
          this.guiValueTxt.Enabled = true;
          this.guiValueSearchBtn.Enabled = true;
          this.m_codeSearch[0].setDataType(DataConfiguration.DataType.npc);
          break;
        case CompletionType.Give_Item:
          this.guiValueTxt.Enabled = true;
          this.guiValueSearchBtn.Enabled = true;
          this.m_codeSearch[0].setDataType(DataConfiguration.DataType.item);
          this.guiAmountTxt.Enabled = true;
          break;
        case CompletionType.Receive_Item:
          this.guiAmountTxt.Enabled = true;
          break;
        case CompletionType.Fight_Mob:
          this.guiValueTxt.Enabled = true;
          this.guiValueSearchBtn.Enabled = true;
          this.m_codeSearch[0].setDataType(DataConfiguration.DataType.mob);
          this.guiAmountTxt.Enabled = true;
          break;
        case CompletionType.Current_Sector:
          this.guiValueTxt.Enabled = true;
          this.guiValueSearchBtn.Enabled = true;
          this.m_codeSearch[0].setDataType(DataConfiguration.DataType.sector);
          break;
        case CompletionType.Nearest_Nav:
          this.guiValueTxt.Enabled = true;
          this.guiValueSearchBtn.Enabled = true;
          this.m_codeSearch[0].setDataType(DataConfiguration.DataType.sector_object);
          break;
        case CompletionType.Obtain_Items:
          this.guiValueTxt.Enabled = true;
          this.guiValueSearchBtn.Enabled = true;
          this.m_codeSearch[0].setDataType(DataConfiguration.DataType.item);
          break;
        case CompletionType.Use_Skill_On_Mob_Type:
          this.guiValueTxt.Enabled = true;
          this.guiValueSearchBtn.Enabled = true;
          this.m_codeSearch[0].setDataType(DataConfiguration.DataType.mob);
          this.guiDataTxt.Enabled = true;
          this.guiDataSearchBtn.Enabled = true;
          this.m_codeSearch[1].setDataType(DataConfiguration.DataType.skill);
          break;
        case CompletionType.Use_Skill_On_Object:
          this.guiValueTxt.Enabled = true;
          this.guiValueSearchBtn.Enabled = true;
          this.m_codeSearch[0].setDataType(DataConfiguration.DataType.sector_object);
          this.guiDataTxt.Enabled = true;
          this.guiDataSearchBtn.Enabled = true;
          this.m_codeSearch[1].setDataType(DataConfiguration.DataType.skill);
          break;
        case CompletionType.Scan_Object:
          this.guiValueTxt.Enabled = true;
          this.guiValueSearchBtn.Enabled = true;
          this.m_codeSearch[0].setDataType(DataConfiguration.DataType.sector_object);
          break;
        case CompletionType.Possess_Item:
          this.guiValueTxt.Enabled = true;
          this.guiValueSearchBtn.Enabled = true;
          this.m_codeSearch[0].setDataType(DataConfiguration.DataType.item);
          this.guiAmountTxt.Enabled = true;
          break;
        case CompletionType.Give_Credits:
          this.guiAmountTxt.Enabled = true;
          break;
        case CompletionType.Arrive_At:
          this.guiValueTxt.Enabled = true;
          this.guiValueSearchBtn.Enabled = true;
          this.m_codeSearch[0].setDataType(DataConfiguration.DataType.sector_object);
          break;
        case CompletionType.Nav_Message:
          this.guiValueTxt.Enabled = true;
          this.guiValueSearchBtn.Enabled = true;
          this.m_codeSearch[0].setDataType(DataConfiguration.DataType.sector_object);
          this.guiAmountTxt.Enabled = true;
          break;
        case CompletionType.Proximity_To_Space_Npc:
          this.guiValueTxt.Enabled = true;
          this.guiValueSearchBtn.Enabled = true;
          this.m_codeSearch[0].setDataType(DataConfiguration.DataType.sector_object);
          break;
        case CompletionType.Talk_Space_Npc:
          this.guiValueTxt.Enabled = true;
          this.guiValueSearchBtn.Enabled = true;
          this.m_codeSearch[0].setDataType(DataConfiguration.DataType.sector_object);
          break;
        case CompletionType.Obtain_Blueprint:
          this.guiValueTxt.Enabled = true;
          this.guiValueSearchBtn.Enabled = true;
          this.m_codeSearch[0].setDataType(DataConfiguration.DataType.item);
          break;
        case CompletionType.Play_Sound:
          this.guiValueTxt.Enabled = true;
          this.guiValueSearchBtn.Enabled = false;
          break;
      }
    }

    private void onOk(object sender, EventArgs e)
    {
      this.m_completion = new Completion();
      string data1 = "";
      string data2 = "";
      int result = -1;
      CompletionType selectedItem = (CompletionType) this.guiTypeCbo.SelectedItem;
      this.m_completion.setCompletionType(selectedItem);
      if (this.guiValueTxt.Enabled)
        data1 = this.guiValueTxt.Text;
      if (this.guiDataTxt.Enabled)
        data2 = this.guiDataTxt.Text;
      if (this.guiAmountTxt.Enabled && !int.TryParse(this.guiAmountTxt.Text, out result))
        result = -1;
      switch (selectedItem)
      {
        case CompletionType.Talk_To_Npc:
          this.m_completion.setValue(data1);
          break;
        case CompletionType.Give_Item:
          this.m_completion.setValue(data1);
          this.m_completion.setCount(result);
          break;
        case CompletionType.Receive_Item:
          this.m_completion.setValue(result.ToString());
          break;
        case CompletionType.Fight_Mob:
          this.m_completion.setValue(data1);
          this.m_completion.setCount(result);
          break;
        case CompletionType.Current_Sector:
          this.m_completion.setValue(data1);
          break;
        case CompletionType.Nearest_Nav:
        case CompletionType.Scan_Object:
          this.m_completion.setValue(data1);
          break;
        case CompletionType.Obtain_Items:
        case CompletionType.Obtain_Blueprint:
          this.m_completion.setValue(data1);
          break;
        case CompletionType.Use_Skill_On_Mob_Type:
          this.m_completion.setValue(data1);
          this.m_completion.setData(data2);
          break;
        case CompletionType.Use_Skill_On_Object:
          this.m_completion.setValue(data1);
          this.m_completion.setData(data2);
          break;
        case CompletionType.Possess_Item:
          this.m_completion.setValue(data1);
          this.m_completion.setCount(result);
          break;
        case CompletionType.Give_Credits:
          this.m_completion.setValue(result.ToString());
          break;
        case CompletionType.Arrive_At:
          this.m_completion.setValue(data1);
          break;
        case CompletionType.Nav_Message:
          this.m_completion.setValue(data1);
          this.m_completion.setCount(result);
          break;
        case CompletionType.Proximity_To_Space_Npc:
          this.m_completion.setValue(data1);
          break;
        case CompletionType.Talk_Space_Npc:
          this.m_completion.setValue(data1);
          break;
        case CompletionType.Play_Sound:
          this.m_completion.setCharData(data1);
          this.m_completion.setValue("1");
          break;
      }
      this.m_completion.addValidations();
      string errorMessage;
      if (DataConfiguration.validate(out errorMessage))
      {
        this.m_madeSelection = true;
        this.Close();
      }
      else
      {
        int num = (int) MessageBox.Show(errorMessage);
      }
    }

    private void onCancel(object sender, EventArgs e)
    {
      this.Close();
    }

    public bool getValues(out Completion completion)
    {
      completion = this.m_completion;
      return this.m_madeSelection;
    }

    private void DlgCompletions_Load(object sender, EventArgs e)
    {
    }
  }
}
