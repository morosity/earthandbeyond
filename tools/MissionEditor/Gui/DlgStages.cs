﻿// Decompiled with JetBrains decompiler
// Type: MissionEditor.Gui.DlgStages
// Assembly: MissionEditor, Version=1.6.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 00D14066-6848-4104-A662-B9B4BF67FB6E
// Assembly location: D:\Server\eab\Tools\MissionEditor.exe

using MissionEditor.Database;
using MissionEditor.Nodes;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace MissionEditor.Gui
{
  public class DlgStages : Form
  {
    private bool m_madeSelection;
    private Stage m_stage;
    private IContainer components;
    private Label guiDescriptionLbl;
    private TextBox guiDescriptionTxt;
    private Button guiCancelBtn;
    private Button guiOkBtn;
    private Label guiIdLbl;
    private TextBox guiIdTxt;

    public DlgStages()
    {
      this.InitializeComponent();
      this.m_stage = (Stage) null;
    }

    protected override void OnShown(EventArgs e)
    {
      base.OnShown(e);
      this.guiDescriptionTxt.Text = "";
      this.m_madeSelection = false;
      this.guiDescriptionTxt.Focus();
    }

    public void setId(int id)
    {
      this.guiIdTxt.Text = id.ToString();
    }

    public void editStage(Stage stage)
    {
      this.m_stage = stage;
    }

    private void onOk(object sender, EventArgs e)
    {
      this.m_stage = new Stage();
      this.m_stage.setId(this.guiIdTxt.Text);
      this.m_stage.setDescription(this.guiDescriptionTxt.Text);
      this.m_stage.addValidations(Stage.ValidationType.FromDialog);
      string errorMessage;
      if (DataConfiguration.validate(out errorMessage))
      {
        this.m_madeSelection = true;
        this.Close();
      }
      else
      {
        int num = (int) MessageBox.Show(errorMessage);
      }
    }

    private void onCancel(object sender, EventArgs e)
    {
      this.Close();
    }

    public bool getValues(out Stage stage)
    {
      stage = this.m_stage;
      return this.m_madeSelection;
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.guiDescriptionLbl = new Label();
      this.guiDescriptionTxt = new TextBox();
      this.guiCancelBtn = new Button();
      this.guiOkBtn = new Button();
      this.guiIdLbl = new Label();
      this.guiIdTxt = new TextBox();
      this.SuspendLayout();
      this.guiDescriptionLbl.AutoSize = true;
      this.guiDescriptionLbl.Location = new Point(3, 35);
      this.guiDescriptionLbl.Name = "guiDescriptionLbl";
      this.guiDescriptionLbl.Size = new Size(63, 13);
      this.guiDescriptionLbl.TabIndex = 2;
      this.guiDescriptionLbl.Text = "Description:";
      this.guiDescriptionTxt.Location = new Point(72, 32);
      this.guiDescriptionTxt.Multiline = true;
      this.guiDescriptionTxt.Name = "guiDescriptionTxt";
      this.guiDescriptionTxt.Size = new Size(433, 78);
      this.guiDescriptionTxt.TabIndex = 3;
      this.guiCancelBtn.Location = new Point(430, 116);
      this.guiCancelBtn.Name = "guiCancelBtn";
      this.guiCancelBtn.Size = new Size(75, 23);
      this.guiCancelBtn.TabIndex = 5;
      this.guiCancelBtn.Text = "Cancel";
      this.guiCancelBtn.UseVisualStyleBackColor = true;
      this.guiCancelBtn.Click += new EventHandler(this.onCancel);
      this.guiOkBtn.Location = new Point(349, 116);
      this.guiOkBtn.Name = "guiOkBtn";
      this.guiOkBtn.Size = new Size(75, 23);
      this.guiOkBtn.TabIndex = 4;
      this.guiOkBtn.Text = "Ok";
      this.guiOkBtn.UseVisualStyleBackColor = true;
      this.guiOkBtn.Click += new EventHandler(this.onOk);
      this.guiIdLbl.AutoSize = true;
      this.guiIdLbl.Location = new Point(47, 9);
      this.guiIdLbl.Name = "guiIdLbl";
      this.guiIdLbl.Size = new Size(19, 13);
      this.guiIdLbl.TabIndex = 0;
      this.guiIdLbl.Text = "Id:";
      this.guiIdTxt.Enabled = false;
      this.guiIdTxt.Location = new Point(72, 6);
      this.guiIdTxt.Name = "guiIdTxt";
      this.guiIdTxt.Size = new Size(46, 20);
      this.guiIdTxt.TabIndex = 1;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(517, 151);
      this.Controls.Add((Control) this.guiIdTxt);
      this.Controls.Add((Control) this.guiIdLbl);
      this.Controls.Add((Control) this.guiCancelBtn);
      this.Controls.Add((Control) this.guiOkBtn);
      this.Controls.Add((Control) this.guiDescriptionTxt);
      this.Controls.Add((Control) this.guiDescriptionLbl);
      this.Name = nameof (DlgStages);
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = nameof (DlgStages);
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
