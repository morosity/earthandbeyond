﻿// Decompiled with JetBrains decompiler
// Type: MissionEditor.Gui.TabStages
// Assembly: MissionEditor, Version=1.6.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 00D14066-6848-4104-A662-B9B4BF67FB6E
// Assembly location: D:\Server\eab\Tools\MissionEditor.exe

using CommonTools;
using CommonTools.Database;
using CommonTools.Gui;
using MissionEditor.Nodes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using TalkTreeEditor;

namespace MissionEditor.Gui
{
  public class TabStages : UserControl
  {
    private FrmMission m_parent;
    private Mission m_mission;
    private Stage m_stage;
    private DlgRewards m_dlgRewards;
    private DlgCompletions m_dlgCompletions;
    private FrmTalkTree m_frmTalkTree;
    private ListView m_stageTable;
    private IContainer components;
    private Label guiDescriptionLbl;
    private TextBox guiDescriptionTxt;
    private Button guiCompletionsRemoveBtn;
    private Button guiCompletionsAddBtn;
    private Label guiCompletionsLbl;
    private ListView guiCompletionsTbl;
    private ColumnHeader guiColumnHeaderType;
    private ColumnHeader guiColumnHeaderValue;
    private Button guiRewardsRemoveBtn;
    private Button guiRewardsAddBtn;
    private Label guiRewardsLbl;
    private ListView guiRewardsTbl;
    private ColumnHeader columnHeader1;
    private ColumnHeader columnHeader2;
    private Button guiTalkTreeBtn;
    private Button guiRewardsEditBtn;
    private Button guiCompletionsEditBtn;
    private Label guiStagesLbl;
    private ComboBox guiStagesCbo;
    private Button guiStagesAddBtn;
    private Button guiCompletionUpBtn;
    private Button guiCompletionDownBtn;
    private Button guiRewardUpBtn;
    private Button guiRewardDownBtn;

    public TabStages(FrmMission parent, ListView stageTable)
    {
      this.InitializeComponent();
      this.m_parent = parent;
      this.m_stageTable = stageTable;
      this.m_mission = (Mission) null;
      this.m_stage = (Stage) null;
      this.m_dlgRewards = new DlgRewards();
      this.m_dlgCompletions = new DlgCompletions();
      this.m_frmTalkTree = new FrmTalkTree();
      TableButtonHandler tableButtonHandler1 = new TableButtonHandler(this.guiCompletionsTbl, this.guiCompletionsAddBtn, this.guiCompletionsRemoveBtn, this.guiCompletionsEditBtn, this.guiCompletionUpBtn, this.guiCompletionDownBtn);
      this.guiCompletionUpBtn.Click += new EventHandler(this.onCompletionReordered);
      this.guiCompletionDownBtn.Click += new EventHandler(this.onCompletionReordered);
      TableButtonHandler tableButtonHandler2 = new TableButtonHandler(this.guiRewardsTbl, this.guiRewardsAddBtn, this.guiRewardsRemoveBtn, this.guiRewardsEditBtn, this.guiRewardUpBtn, this.guiRewardDownBtn);
      this.guiRewardUpBtn.Click += new EventHandler(this.onRewardReordered);
      this.guiRewardDownBtn.Click += new EventHandler(this.onRewardReordered);
    }

    public void setMission(Mission mission)
    {
      this.m_mission = mission;
      this.guiStagesCbo.Items.Clear();
      this.guiDescriptionTxt.Text = "";
      this.guiCompletionsTbl.Items.Clear();
      this.guiRewardsTbl.Items.Clear();
      if (!this.m_mission.hasStages())
        return;
      foreach (object stage in this.m_mission.getStages())
        this.guiStagesCbo.Items.Add(stage);
      this.selectStage(this.m_mission.getStages()[0]);
    }

    public void selectStage(Stage stage)
    {
      bool wasMuted;
      this.m_parent.setMuteFieldChanges(true, out wasMuted);
      this.m_stage = stage;
      this.guiStagesCbo.SelectedItem = (object) this.m_stage;
      this.guiDescriptionTxt.Enabled = stage.getId().CompareTo("0") != 0;
      this.guiDescriptionTxt.Text = this.m_stage.getDescription();
      this.guiCompletionsTbl.Items.Clear();
      if (this.m_stage.hasCompletions())
      {
        foreach (Completion completion in this.m_stage.getCompletions())
        {
          ListViewItem listViewItem = new ListViewItem(new string[2]
          {
            completion.getCompletionType().ToString(),
            completion.getFormattedValue().ToString()
          });
          this.guiCompletionsTbl.Items.Add(listViewItem);
          listViewItem.Tag = (object) completion;
        }
        this.guiCompletionsTbl.Items[0].Selected = true;
      }
      this.guiRewardsTbl.Items.Clear();
      if (this.m_stage.hasRewards())
      {
        foreach (Reward reward in this.m_stage.getRewards())
        {
          ListViewItem listViewItem = new ListViewItem(new string[2]
          {
            reward.getRewardType().ToString(),
            reward.getFormattedValue()
          });
          this.guiRewardsTbl.Items.Add(listViewItem);
          listViewItem.Tag = (object) reward;
        }
        this.guiRewardsTbl.Items[0].Selected = true;
      }
      this.m_parent.setMuteFieldChanges(wasMuted, out wasMuted);
    }

    private void onAddStage(object sender, EventArgs e)
    {
      this.m_parent.onAddStage(false);
    }

    private void onStageSelected(object sender, EventArgs e)
    {
      this.selectStage((Stage) this.guiStagesCbo.SelectedItem);
    }

    private void onLeaveDescription(object sender, EventArgs e)
    {
      if (this.m_parent.getFieldChangesMuted())
        return;
      int index = this.guiStagesCbo.Items.IndexOf((object) this.m_stage);
      if (index == -1)
        return;
      this.guiStagesCbo.Items.RemoveAt(index);
      this.guiStagesCbo.Items.Insert(index, (object) this.m_stage);
      this.guiStagesCbo.SelectedIndex = index;
    }

    private void onSimpleChanged(object sender, EventArgs e)
    {
      this.m_parent.onChanged();
    }

    private void onChanged(object sender, EventArgs e)
    {
      if (this.m_parent.getFieldChangesMuted())
        return;
      if (((Control) sender).Name.Equals(this.guiDescriptionTxt.Name))
      {
        this.m_stage.setDescription(this.guiDescriptionTxt.Text);
        foreach (ListViewItem listViewItem in this.m_stageTable.Items)
        {
          if (listViewItem.Tag.Equals((object) this.m_stage))
          {
            listViewItem.SubItems[1].Text = this.m_stage.getDescription();
            break;
          }
        }
      }
      this.m_parent.onChanged();
    }

    private void onCompletionSelected(object sender, EventArgs e)
    {
      int num = (int) MessageBox.Show(nameof (onCompletionSelected));
      this.guiCompletionsRemoveBtn.Enabled = ((ListView) sender).SelectedItems.Count != 0;
    }

    private void onCompletionAdd(object sender, EventArgs e)
    {
      int num = (int) this.m_dlgCompletions.ShowDialog();
      Completion completion;
      if (!this.m_dlgCompletions.getValues(out completion))
        return;
      this.m_parent.onChanged();
      this.m_stage.addCompletion(completion);
      this.guiCompletionsTbl.Items.Add(new ListViewItem(new string[2]
      {
        completion.getCompletionType().ToString(),
        completion.getFormattedValue().ToString()
      })
      {
        Tag = (object) completion
      });
      if (!this.m_stage.getId().Equals("0") || !completion.getCompletionType().Equals((object) CompletionType.Talk_To_Npc))
        return;
      this.m_parent.setMissionType(MissionType.Npc, completion.getValue());
    }

    private void onCompletionEdit(object sender, EventArgs e)
    {
      this.m_dlgCompletions.editCompletion((Completion) this.guiCompletionsTbl.SelectedItems[0].Tag);
      this.m_parent.onChanged();
    }

    private void onCompletionRemove(object sender, EventArgs e)
    {
      this.m_parent.onChanged();
      foreach (ListViewItem selectedItem in this.guiCompletionsTbl.SelectedItems)
      {
        this.m_stage.removeCompletion((Completion) selectedItem.Tag);
        this.guiCompletionsTbl.Items.Remove(selectedItem);
      }
    }

    private void onCompletionReordered(object sender, EventArgs e)
    {
      this.m_stage.clearCompletions();
      int index1 = this.guiCompletionsTbl.SelectedItems[0].Index;
      for (int index2 = 0; index2 < this.guiCompletionsTbl.Items.Count; ++index2)
      {
        Completion tag = (Completion) this.guiCompletionsTbl.Items[index2].Tag;
        this.guiCompletionsTbl.Items.RemoveAt(index2);
        ListViewItem listViewItem = new ListViewItem(new string[2]
        {
          tag.getCompletionType().ToString(),
          tag.getFormattedValue().ToString()
        });
        listViewItem.Tag = (object) tag;
        this.guiCompletionsTbl.Items.Insert(index2, listViewItem);
        if (index2 == index1)
          listViewItem.Selected = true;
        this.m_stage.addCompletion(tag);
      }
      this.m_parent.onChanged();
    }

    private void onRewardSelected(object sender, EventArgs e)
    {
      int num = (int) MessageBox.Show(nameof (onRewardSelected));
      ListView listView = (ListView) sender;
      this.guiRewardsRemoveBtn.Enabled = listView.SelectedItems.Count != 0;
      this.guiRewardDownBtn.Enabled = listView.SelectedItems.Count == 1;
      this.guiRewardUpBtn.Enabled = listView.SelectedItems.Count == 1;
    }

    private void onRewardsAdd(object sender, EventArgs e)
    {
      int num = (int) this.m_dlgRewards.ShowDialog();
      Reward reward;
      if (!this.m_dlgRewards.getValues(out reward))
        return;
      this.m_parent.onChanged();
      this.m_stage.addReward(reward);
      this.guiRewardsTbl.Items.Add(new ListViewItem(new string[2]
      {
        reward.getRewardType().ToString(),
        reward.getFormattedValue().ToString()
      })
      {
        Tag = (object) reward
      });
    }

    private void onRewardEdit(object sender, EventArgs e)
    {
      this.m_dlgRewards.editReward((Reward) this.guiRewardsTbl.SelectedItems[0].Tag);
      this.m_parent.onChanged();
    }

    private void onRewardsRemove(object sender, EventArgs e)
    {
      this.m_parent.onChanged();
      foreach (ListViewItem selectedItem in this.guiRewardsTbl.SelectedItems)
      {
        this.m_stage.removeReward((Reward) selectedItem.Tag);
        this.guiRewardsTbl.Items.Remove(selectedItem);
      }
    }

    private void onRewardReordered(object sender, EventArgs e)
    {
      this.m_stage.clearRewards();
      int index1 = this.guiRewardsTbl.SelectedItems[0].Index;
      for (int index2 = 0; index2 < this.guiRewardsTbl.Items.Count; ++index2)
      {
        Reward tag = (Reward) this.guiRewardsTbl.Items[index2].Tag;
        this.guiRewardsTbl.Items.RemoveAt(index2);
        ListViewItem listViewItem = new ListViewItem(new string[2]
        {
          tag.getRewardType().ToString(),
          tag.getFormattedValue().ToString()
        });
        listViewItem.Tag = (object) tag;
        this.guiRewardsTbl.Items.Insert(index2, listViewItem);
        if (index2 == index1)
          listViewItem.Selected = true;
        this.m_stage.addReward(tag);
      }
      this.m_parent.onChanged();
    }

    private void onTalkTree(object sender, EventArgs e)
    {
      string conversation = (string) null;
      if (this.m_stage.hasTalkTrees())
      {
        StringWriter stringWriter = new StringWriter();
        this.m_stage.getTalkTreesXML(stringWriter);
        conversation = stringWriter.ToString();
      }
      this.m_frmTalkTree.setConversation(conversation);
      List<CodeValue> stages = new List<CodeValue>();
      foreach (Stage stage in this.m_mission.getStages())
      {
        int code = int.Parse(stage.getId());
        stages.Add(CodeValue.FormattedLeft(code, stage.getDescription()));
      }
      this.m_frmTalkTree.setStages(stages);
      int num = (int) this.m_frmTalkTree.ShowDialog();
      if (!this.m_frmTalkTree.getConversation(out conversation))
        return;
      this.m_parent.onChanged();
      XmlDocument xmlDocument = new XmlDocument();
      xmlDocument.Load((TextReader) new StringReader("<Chat>" + conversation + "</Chat>"));
      this.m_stage.clearTalkTrees();
      foreach (XmlNode childNode in xmlDocument.DocumentElement.ChildNodes)
      {
        TalkTree talkTree = new TalkTree();
        talkTree.fromXML(childNode);
        this.m_stage.addTalkTree(talkTree);
      }
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (TabStages));
      this.guiDescriptionLbl = new Label();
      this.guiDescriptionTxt = new TextBox();
      this.guiCompletionsRemoveBtn = new Button();
      this.guiCompletionsAddBtn = new Button();
      this.guiCompletionsLbl = new Label();
      this.guiCompletionsTbl = new ListView();
      this.guiColumnHeaderType = new ColumnHeader();
      this.guiColumnHeaderValue = new ColumnHeader();
      this.guiRewardsRemoveBtn = new Button();
      this.guiRewardsAddBtn = new Button();
      this.guiRewardsLbl = new Label();
      this.guiRewardsTbl = new ListView();
      this.columnHeader1 = new ColumnHeader();
      this.columnHeader2 = new ColumnHeader();
      this.guiTalkTreeBtn = new Button();
      this.guiRewardsEditBtn = new Button();
      this.guiCompletionsEditBtn = new Button();
      this.guiStagesLbl = new Label();
      this.guiStagesCbo = new ComboBox();
      this.guiStagesAddBtn = new Button();
      this.guiCompletionUpBtn = new Button();
      this.guiCompletionDownBtn = new Button();
      this.guiRewardUpBtn = new Button();
      this.guiRewardDownBtn = new Button();
      this.SuspendLayout();
      this.guiDescriptionLbl.AutoSize = true;
      this.guiDescriptionLbl.Location = new Point(18, 46);
      this.guiDescriptionLbl.Name = "guiDescriptionLbl";
      this.guiDescriptionLbl.Size = new Size(63, 13);
      this.guiDescriptionLbl.TabIndex = 3;
      this.guiDescriptionLbl.Text = "Description:";
      this.guiDescriptionTxt.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.guiDescriptionTxt.Location = new Point(87, 43);
      this.guiDescriptionTxt.Multiline = true;
      this.guiDescriptionTxt.Name = "guiDescriptionTxt";
      this.guiDescriptionTxt.Size = new Size(437, 49);
      this.guiDescriptionTxt.TabIndex = 4;
      this.guiDescriptionTxt.TextChanged += new EventHandler(this.onChanged);
      this.guiDescriptionTxt.Leave += new EventHandler(this.onLeaveDescription);
      this.guiCompletionsRemoveBtn.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.guiCompletionsRemoveBtn.Enabled = false;
      this.guiCompletionsRemoveBtn.Location = new Point(449, 156);
      this.guiCompletionsRemoveBtn.Name = "guiCompletionsRemoveBtn";
      this.guiCompletionsRemoveBtn.Size = new Size(75, 23);
      this.guiCompletionsRemoveBtn.TabIndex = 9;
      this.guiCompletionsRemoveBtn.Text = "Remove";
      this.guiCompletionsRemoveBtn.UseVisualStyleBackColor = true;
      this.guiCompletionsRemoveBtn.Click += new EventHandler(this.onCompletionRemove);
      this.guiCompletionsAddBtn.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.guiCompletionsAddBtn.Location = new Point(449, 98);
      this.guiCompletionsAddBtn.Name = "guiCompletionsAddBtn";
      this.guiCompletionsAddBtn.Size = new Size(75, 23);
      this.guiCompletionsAddBtn.TabIndex = 7;
      this.guiCompletionsAddBtn.Text = "Add";
      this.guiCompletionsAddBtn.UseVisualStyleBackColor = true;
      this.guiCompletionsAddBtn.Click += new EventHandler(this.onCompletionAdd);
      this.guiCompletionsLbl.AutoSize = true;
      this.guiCompletionsLbl.Location = new Point(14, 98);
      this.guiCompletionsLbl.Name = "guiCompletionsLbl";
      this.guiCompletionsLbl.Size = new Size(67, 13);
      this.guiCompletionsLbl.TabIndex = 5;
      this.guiCompletionsLbl.Text = "Completions:";
      this.guiCompletionsTbl.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.guiCompletionsTbl.Columns.AddRange(new ColumnHeader[2]
      {
        this.guiColumnHeaderType,
        this.guiColumnHeaderValue
      });
      this.guiCompletionsTbl.FullRowSelect = true;
      this.guiCompletionsTbl.HideSelection = false;
      this.guiCompletionsTbl.Location = new Point(87, 98);
      this.guiCompletionsTbl.Name = "guiCompletionsTbl";
      this.guiCompletionsTbl.Size = new Size(356, 189);
      this.guiCompletionsTbl.TabIndex = 6;
      this.guiCompletionsTbl.UseCompatibleStateImageBehavior = false;
      this.guiCompletionsTbl.View = View.Details;
      this.guiColumnHeaderType.Text = "Type";
      this.guiColumnHeaderType.Width = 130;
      this.guiColumnHeaderValue.Text = "Value";
      this.guiColumnHeaderValue.Width = 200;
      this.guiRewardsRemoveBtn.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.guiRewardsRemoveBtn.Enabled = false;
      this.guiRewardsRemoveBtn.Location = new Point(449, 351);
      this.guiRewardsRemoveBtn.Name = "guiRewardsRemoveBtn";
      this.guiRewardsRemoveBtn.Size = new Size(75, 23);
      this.guiRewardsRemoveBtn.TabIndex = 14;
      this.guiRewardsRemoveBtn.Text = "Remove";
      this.guiRewardsRemoveBtn.UseVisualStyleBackColor = true;
      this.guiRewardsRemoveBtn.Click += new EventHandler(this.onRewardsRemove);
      this.guiRewardsAddBtn.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.guiRewardsAddBtn.Location = new Point(449, 293);
      this.guiRewardsAddBtn.Name = "guiRewardsAddBtn";
      this.guiRewardsAddBtn.Size = new Size(75, 23);
      this.guiRewardsAddBtn.TabIndex = 12;
      this.guiRewardsAddBtn.Text = "Add";
      this.guiRewardsAddBtn.UseVisualStyleBackColor = true;
      this.guiRewardsAddBtn.Click += new EventHandler(this.onRewardsAdd);
      this.guiRewardsLbl.AutoSize = true;
      this.guiRewardsLbl.Location = new Point(29, 293);
      this.guiRewardsLbl.Name = "guiRewardsLbl";
      this.guiRewardsLbl.Size = new Size(52, 13);
      this.guiRewardsLbl.TabIndex = 10;
      this.guiRewardsLbl.Text = "Rewards:";
      this.guiRewardsTbl.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.guiRewardsTbl.Columns.AddRange(new ColumnHeader[2]
      {
        this.columnHeader1,
        this.columnHeader2
      });
      this.guiRewardsTbl.FullRowSelect = true;
      this.guiRewardsTbl.HideSelection = false;
      this.guiRewardsTbl.Location = new Point(87, 293);
      this.guiRewardsTbl.Name = "guiRewardsTbl";
      this.guiRewardsTbl.Size = new Size(356, 158);
      this.guiRewardsTbl.TabIndex = 11;
      this.guiRewardsTbl.UseCompatibleStateImageBehavior = false;
      this.guiRewardsTbl.View = View.Details;
      this.columnHeader1.Text = "Type";
      this.columnHeader1.Width = 130;
      this.columnHeader2.Text = "Value";
      this.columnHeader2.Width = 200;
      this.guiTalkTreeBtn.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.guiTalkTreeBtn.Location = new Point(87, 457);
      this.guiTalkTreeBtn.Name = "guiTalkTreeBtn";
      this.guiTalkTreeBtn.Size = new Size(356, 53);
      this.guiTalkTreeBtn.TabIndex = 15;
      this.guiTalkTreeBtn.Text = "Talk Tree";
      this.guiTalkTreeBtn.UseVisualStyleBackColor = true;
      this.guiTalkTreeBtn.Click += new EventHandler(this.onTalkTree);
      this.guiRewardsEditBtn.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.guiRewardsEditBtn.Enabled = false;
      this.guiRewardsEditBtn.Location = new Point(449, 322);
      this.guiRewardsEditBtn.Name = "guiRewardsEditBtn";
      this.guiRewardsEditBtn.Size = new Size(75, 23);
      this.guiRewardsEditBtn.TabIndex = 13;
      this.guiRewardsEditBtn.Text = "Edit";
      this.guiRewardsEditBtn.UseVisualStyleBackColor = true;
      this.guiRewardsEditBtn.Click += new EventHandler(this.onRewardEdit);
      this.guiCompletionsEditBtn.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.guiCompletionsEditBtn.Enabled = false;
      this.guiCompletionsEditBtn.Location = new Point(449, (int) sbyte.MaxValue);
      this.guiCompletionsEditBtn.Name = "guiCompletionsEditBtn";
      this.guiCompletionsEditBtn.Size = new Size(75, 23);
      this.guiCompletionsEditBtn.TabIndex = 8;
      this.guiCompletionsEditBtn.Text = "Edit";
      this.guiCompletionsEditBtn.UseVisualStyleBackColor = true;
      this.guiCompletionsEditBtn.Click += new EventHandler(this.onCompletionEdit);
      this.guiStagesLbl.AutoSize = true;
      this.guiStagesLbl.Location = new Point(38, 13);
      this.guiStagesLbl.Name = "guiStagesLbl";
      this.guiStagesLbl.Size = new Size(43, 13);
      this.guiStagesLbl.TabIndex = 0;
      this.guiStagesLbl.Text = "Stages:";
      this.guiStagesCbo.DropDownStyle = ComboBoxStyle.DropDownList;
      this.guiStagesCbo.FormattingEnabled = true;
      this.guiStagesCbo.Location = new Point(87, 13);
      this.guiStagesCbo.Name = "guiStagesCbo";
      this.guiStagesCbo.Size = new Size(356, 21);
      this.guiStagesCbo.TabIndex = 1;
      this.guiStagesCbo.SelectedIndexChanged += new EventHandler(this.onStageSelected);
      this.guiStagesAddBtn.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.guiStagesAddBtn.Location = new Point(449, 11);
      this.guiStagesAddBtn.Name = "guiStagesAddBtn";
      this.guiStagesAddBtn.Size = new Size(75, 23);
      this.guiStagesAddBtn.TabIndex = 2;
      this.guiStagesAddBtn.Text = "Add";
      this.guiStagesAddBtn.UseVisualStyleBackColor = true;
      this.guiStagesAddBtn.Click += new EventHandler(this.onAddStage);
      this.guiCompletionUpBtn.Image = (Image) componentResourceManager.GetObject("guiCompletionUpBtn.Image");
      this.guiCompletionUpBtn.Location = new Point(449, 183);
      this.guiCompletionUpBtn.Margin = new Padding(1);
      this.guiCompletionUpBtn.Name = "guiCompletionUpBtn";
      this.guiCompletionUpBtn.Size = new Size(39, 35);
      this.guiCompletionUpBtn.TabIndex = 27;
      this.guiCompletionUpBtn.UseVisualStyleBackColor = true;
      this.guiCompletionUpBtn.Click += new EventHandler(this.onSimpleChanged);
      this.guiCompletionDownBtn.Image = (Image) componentResourceManager.GetObject("guiCompletionDownBtn.Image");
      this.guiCompletionDownBtn.Location = new Point(487, 183);
      this.guiCompletionDownBtn.Margin = new Padding(1);
      this.guiCompletionDownBtn.Name = "guiCompletionDownBtn";
      this.guiCompletionDownBtn.Size = new Size(39, 35);
      this.guiCompletionDownBtn.TabIndex = 26;
      this.guiCompletionDownBtn.UseVisualStyleBackColor = true;
      this.guiCompletionDownBtn.Click += new EventHandler(this.onSimpleChanged);
      this.guiRewardUpBtn.Image = (Image) componentResourceManager.GetObject("guiRewardUpBtn.Image");
      this.guiRewardUpBtn.Location = new Point(449, 378);
      this.guiRewardUpBtn.Margin = new Padding(1);
      this.guiRewardUpBtn.Name = "guiRewardUpBtn";
      this.guiRewardUpBtn.Size = new Size(39, 35);
      this.guiRewardUpBtn.TabIndex = 29;
      this.guiRewardUpBtn.UseVisualStyleBackColor = true;
      this.guiRewardUpBtn.Click += new EventHandler(this.onSimpleChanged);
      this.guiRewardDownBtn.Image = (Image) componentResourceManager.GetObject("guiRewardDownBtn.Image");
      this.guiRewardDownBtn.Location = new Point(487, 378);
      this.guiRewardDownBtn.Margin = new Padding(1);
      this.guiRewardDownBtn.Name = "guiRewardDownBtn";
      this.guiRewardDownBtn.Size = new Size(39, 35);
      this.guiRewardDownBtn.TabIndex = 28;
      this.guiRewardDownBtn.UseVisualStyleBackColor = true;
      this.guiRewardDownBtn.Click += new EventHandler(this.onSimpleChanged);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.AutoSize = true;
      this.BackColor = Color.PaleGreen;
      this.Controls.Add((Control) this.guiRewardUpBtn);
      this.Controls.Add((Control) this.guiRewardDownBtn);
      this.Controls.Add((Control) this.guiCompletionUpBtn);
      this.Controls.Add((Control) this.guiCompletionDownBtn);
      this.Controls.Add((Control) this.guiStagesAddBtn);
      this.Controls.Add((Control) this.guiStagesCbo);
      this.Controls.Add((Control) this.guiStagesLbl);
      this.Controls.Add((Control) this.guiCompletionsEditBtn);
      this.Controls.Add((Control) this.guiRewardsEditBtn);
      this.Controls.Add((Control) this.guiTalkTreeBtn);
      this.Controls.Add((Control) this.guiRewardsRemoveBtn);
      this.Controls.Add((Control) this.guiRewardsAddBtn);
      this.Controls.Add((Control) this.guiRewardsLbl);
      this.Controls.Add((Control) this.guiRewardsTbl);
      this.Controls.Add((Control) this.guiCompletionsRemoveBtn);
      this.Controls.Add((Control) this.guiCompletionsAddBtn);
      this.Controls.Add((Control) this.guiCompletionsLbl);
      this.Controls.Add((Control) this.guiCompletionsTbl);
      this.Controls.Add((Control) this.guiDescriptionTxt);
      this.Controls.Add((Control) this.guiDescriptionLbl);
      this.Name = nameof (TabStages);
      this.Size = new Size(550, 525);
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
