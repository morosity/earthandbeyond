﻿// Decompiled with JetBrains decompiler
// Type: MissionEditor.Gui.FrmMission
// Assembly: MissionEditor, Version=1.6.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 00D14066-6848-4104-A662-B9B4BF67FB6E
// Assembly location: D:\Server\eab\Tools\MissionEditor.exe

using CommonTools;
using CommonTools.Database;
using CommonTools.Gui;
using MissionEditor.Database;
using MissionEditor.Nodes;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Xml;

namespace MissionEditor.Gui
{
  public class FrmMission : Form
  {
    private Mission m_mission;
    private TabMission m_tabMission;
    private TabStages m_tabStages;
    private DlgSearch m_dlgSearch;
    private DlgEditXml m_dlgEditXml;
    private DlgReport m_dlgReport;
    private FrmMission.State m_state;
    private string m_currentMissionId;
    private bool m_fieldChangesMuted;
    private IContainer components;
    private TabControl guiTabControl;
    private TabPage guiTabPageMission;
    private TabPage guiTabPageStages;
    private ToolStrip toolbar;
    private ToolStripButton guiAddBtn;
    private ToolStripButton guiDeleteBtn;
    private ToolStripButton guiSaveBtn;
    private ToolStripButton guiCancelBtn;
    private ToolStripButton guiSearchBtn;
    private ToolStripLabel guiSearchLbl;
    private ToolStripTextBox guiSearchTxt;
    private Label guiStatusLbl;
    private ToolStripButton guiEditBtn;
    private ToolStripButton guiReportBtn;
    private MenuStrip menuStrip1;
    private ToolStripMenuItem fileToolStripMenuItem;
    private ToolStripMenuItem exitToolStripMenuItem;
    private ToolStripMenuItem menuEdit;
    private ToolStripMenuItem menuAdd;
    private ToolStripMenuItem menuDelete;
    private ToolStripSeparator toolStripSeparator1;
    private ToolStripMenuItem menuSave;
    private ToolStripMenuItem menuCancel;
    private ToolStripSeparator toolStripSeparator5;
    private ToolStripMenuItem menuSearch;

    public FrmMission()
    {
      this.InitializeComponent();
      this.Text = this.Text + " " + LoginData.ApplicationVersion;
      DataConfiguration.init();
      this.m_mission = new Mission();
      this.m_dlgEditXml = (DlgEditXml) null;
      this.m_dlgReport = (DlgReport) null;
      this.m_dlgSearch = new DlgSearch();
      this.m_dlgSearch.configure(Net7.Tables.missions);
      this.m_fieldChangesMuted = false;
      this.configureTabs();
      this.setState(FrmMission.State.View);
    }

    private void configureTabs()
    {
      this.m_tabMission = new TabMission(this);
      this.guiTabPageMission.Controls.Add((Control) this.m_tabMission);
      this.m_tabStages = new TabStages(this, this.m_tabMission.getStageTable());
      this.guiTabPageStages.Controls.Add((Control) this.m_tabStages);
    }

    private void setState(FrmMission.State state)
    {
      this.m_state = state;
      bool flag = this.m_state.Equals((object) FrmMission.State.View);
      this.guiAddBtn.Enabled = flag;
      this.guiDeleteBtn.Enabled = flag;
      this.guiSaveBtn.Enabled = !flag;
      this.guiCancelBtn.Enabled = !flag;
      this.guiEditBtn.Enabled = flag;
      this.guiSearchBtn.Enabled = flag;
      this.guiSearchTxt.Enabled = flag;
    }

    public Mission getMission()
    {
      return this.m_mission;
    }

    protected override void OnShown(EventArgs e)
    {
      base.OnShown(e);
      string firstMissionId = MissionEditor.Database.Database.getFirstMissionId();
      if (firstMissionId == null)
        return;
      this.loadMission(firstMissionId);
    }

    public void loadMission(string missionId)
    {
      bool wasMuted;
      this.setMuteFieldChanges(true, out wasMuted);
      this.Cursor = Cursors.WaitCursor;
      if (missionId != null)
      {
        this.guiStatusLbl.Text = "Retrieving mission " + missionId;
        this.Refresh();
        this.m_mission = MissionEditor.Database.Database.getMission(missionId);
      }
      if (this.m_mission == null)
      {
        int num1 = (int) MessageBox.Show("The mission id '" + missionId + "' does not exist");
      }
      else
      {
        try
        {
          DateTime now = DateTime.Now;
          this.guiStatusLbl.Text = "Parsing mission " + this.m_mission.getId();
          this.guiStatusLbl.Refresh();
          this.m_mission.parseXml();
          string errorMessage;
          bool flag = DataConfiguration.validate(out errorMessage);
          this.guiStatusLbl.Text = (DateTime.Now - now).TotalMilliseconds.ToString() + " milliseconds";
          this.m_tabMission.setMission(this.m_mission);
          this.m_tabStages.setMission(this.m_mission);
          if (!flag)
            throw new Exception(errorMessage);
          this.m_currentMissionId = this.m_mission.getId();
          this.guiSearchTxt.Text = this.m_currentMissionId;
        }
        catch (XmlException ex)
        {
          int num2 = (int) MessageBox.Show(ex.Message);
        }
        catch (Exception ex)
        {
          int num2 = (int) MessageBox.Show(ex.Message);
        }
      }
      this.Cursor = Cursors.Default;
      this.setMuteFieldChanges(wasMuted, out wasMuted);
    }

    private void reloadMission(string xmlMission)
    {
      if (this.m_dlgEditXml == null)
        this.m_dlgEditXml = new DlgEditXml();
      this.m_dlgEditXml.setXml(xmlMission);
      int num = (int) this.m_dlgEditXml.ShowDialog();
      if (!this.m_dlgEditXml.getValues(out xmlMission))
        return;
      this.setState(FrmMission.State.Edit);
      this.m_mission.clear();
      this.m_mission.setXml(xmlMission);
      this.loadMission((string) null);
    }

    public void setMuteFieldChanges(bool mute, out bool wasMuted)
    {
      wasMuted = this.m_fieldChangesMuted;
      this.m_fieldChangesMuted = mute;
    }

    public bool getFieldChangesMuted()
    {
      return this.m_fieldChangesMuted;
    }

    public void onChanged()
    {
      if (this.m_fieldChangesMuted || !this.m_state.Equals((object) FrmMission.State.View))
        return;
      this.setState(FrmMission.State.Edit);
    }

    public void onChangedStages()
    {
      bool wasMuted;
      this.setMuteFieldChanges(true, out wasMuted);
      this.m_tabStages.setMission(this.m_mission);
      this.setMuteFieldChanges(wasMuted, out wasMuted);
    }

    public void onAddStage(bool showDialog)
    {
      this.m_tabMission.addStage(showDialog);
    }

    public void onStageSelected(Stage stage)
    {
      bool wasMuted;
      this.setMuteFieldChanges(true, out wasMuted);
      this.m_tabStages.selectStage(stage);
      this.setMuteFieldChanges(wasMuted, out wasMuted);
    }

    private void onRecordAdd(object sender, EventArgs e)
    {
      this.setState(FrmMission.State.Add);
      if (this.m_mission == null)
        this.m_mission = new Mission();
      this.m_mission.clear();
      this.m_mission.setId(MissionEditor.Database.Database.getNextMissionId());
      this.m_mission.setName("");
      bool wasMuted;
      this.setMuteFieldChanges(true, out wasMuted);
      this.m_tabMission.setMission(this.m_mission);
      this.m_tabStages.setMission(this.m_mission);
      this.setMuteFieldChanges(wasMuted, out wasMuted);
      this.guiTabControl.SelectedIndex = 0;
    }

    private void onRecordDelete(object sender, EventArgs e)
    {
      if (!MessageBox.Show("Do you really want to delete this record?", "Deletion Confirmation", MessageBoxButtons.YesNo).Equals((object) DialogResult.Yes))
        return;
      MissionEditor.Database.Database.deleteMission(this.m_mission);
      this.loadMission(MissionEditor.Database.Database.getFirstMissionId());
    }

    public bool isMissionValid(out string error)
    {
      this.m_mission.addFullValidations();
      return DataConfiguration.validate(out error);
    }

    private void onRecordSave(object sender, EventArgs e)
    {
      this.m_mission.addFullValidations();
      string errorMessage;
      if (DataConfiguration.validate(out errorMessage))
      {
        if (this.m_state.Equals((object) FrmMission.State.Add))
        {
          MissionEditor.Database.Database.setMission(this.m_mission, true);
          this.m_tabMission.setMission(this.m_mission);
          this.m_currentMissionId = this.m_mission.getId();
          this.guiSearchTxt.Text = this.m_currentMissionId;
        }
        else
          MissionEditor.Database.Database.setMission(this.m_mission, false);
        this.setState(FrmMission.State.View);
      }
      else
      {
        int num = (int) MessageBox.Show(errorMessage);
      }
    }

    private void onRecordUndo(object sender, EventArgs e)
    {
      this.setState(FrmMission.State.View);
      this.loadMission(this.m_currentMissionId);
    }

    private void onRecordEdit(object sender, EventArgs e)
    {
      this.reloadMission(this.m_mission.getXML());
    }

    private void onRecordSearch(object sender, EventArgs e)
    {
      int num = (int) this.m_dlgSearch.ShowDialog();
      string selectedId = this.m_dlgSearch.getSelectedId();
      if (selectedId.Length == 0)
        return;
      this.guiTabControl.SelectedIndex = 0;
      this.guiSearchTxt.Text = selectedId;
      this.loadMission(selectedId);
    }

    private void onRecordSearch(object sender, KeyEventArgs e)
    {
      if (!e.KeyCode.Equals((object) Keys.Return))
        return;
      this.guiTabControl.SelectedIndex = 0;
      this.loadMission(this.guiSearchTxt.Text);
    }

    private void onRecordReport(object sender, EventArgs e)
    {
      this.Cursor = Cursors.WaitCursor;
      if (this.m_dlgReport == null)
        this.m_dlgReport = new DlgReport();
      this.m_dlgReport.set(this.m_mission.getReport());
      this.Cursor = Cursors.Default;
      int num = (int) this.m_dlgReport.ShowDialog();
    }

    public void setMissionType(MissionType missionType, string missionKey)
    {
      this.m_tabMission.setMissionType(missionType, missionKey);
      this.onChanged();
    }

    private void onExit(object sender, EventArgs e)
    {
      Application.Exit();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (FrmMission));
      this.guiTabControl = new TabControl();
      this.guiTabPageMission = new TabPage();
      this.guiTabPageStages = new TabPage();
      this.toolbar = new ToolStrip();
      this.guiAddBtn = new ToolStripButton();
      this.guiDeleteBtn = new ToolStripButton();
      this.guiSaveBtn = new ToolStripButton();
      this.guiCancelBtn = new ToolStripButton();
      this.guiEditBtn = new ToolStripButton();
      this.guiReportBtn = new ToolStripButton();
      this.guiSearchBtn = new ToolStripButton();
      this.guiSearchLbl = new ToolStripLabel();
      this.guiSearchTxt = new ToolStripTextBox();
      this.guiStatusLbl = new Label();
      this.menuStrip1 = new MenuStrip();
      this.fileToolStripMenuItem = new ToolStripMenuItem();
      this.exitToolStripMenuItem = new ToolStripMenuItem();
      this.menuEdit = new ToolStripMenuItem();
      this.menuAdd = new ToolStripMenuItem();
      this.menuDelete = new ToolStripMenuItem();
      this.toolStripSeparator1 = new ToolStripSeparator();
      this.menuSave = new ToolStripMenuItem();
      this.menuCancel = new ToolStripMenuItem();
      this.toolStripSeparator5 = new ToolStripSeparator();
      this.menuSearch = new ToolStripMenuItem();
      this.guiTabControl.SuspendLayout();
      this.toolbar.SuspendLayout();
      this.menuStrip1.SuspendLayout();
      this.SuspendLayout();
      this.guiTabControl.Controls.Add((Control) this.guiTabPageMission);
      this.guiTabControl.Controls.Add((Control) this.guiTabPageStages);
      this.guiTabControl.Location = new Point(0, 66);
      this.guiTabControl.Name = "guiTabControl";
      this.guiTabControl.SelectedIndex = 0;
      this.guiTabControl.Size = new Size(550, 546);
      this.guiTabControl.TabIndex = 1;
      this.guiTabPageMission.Location = new Point(4, 22);
      this.guiTabPageMission.Name = "guiTabPageMission";
      this.guiTabPageMission.Padding = new Padding(3);
      this.guiTabPageMission.Size = new Size(542, 520);
      this.guiTabPageMission.TabIndex = 0;
      this.guiTabPageMission.Text = "Mission";
      this.guiTabPageMission.UseVisualStyleBackColor = true;
      this.guiTabPageStages.Location = new Point(4, 22);
      this.guiTabPageStages.Name = "guiTabPageStages";
      this.guiTabPageStages.Padding = new Padding(3);
      this.guiTabPageStages.Size = new Size(542, 520);
      this.guiTabPageStages.TabIndex = 1;
      this.guiTabPageStages.Text = "Stages";
      this.guiTabPageStages.UseVisualStyleBackColor = true;
      this.toolbar.ImageScalingSize = new Size(32, 32);
      this.toolbar.Items.AddRange(new ToolStripItem[9]
      {
        (ToolStripItem) this.guiAddBtn,
        (ToolStripItem) this.guiDeleteBtn,
        (ToolStripItem) this.guiSaveBtn,
        (ToolStripItem) this.guiCancelBtn,
        (ToolStripItem) this.guiEditBtn,
        (ToolStripItem) this.guiReportBtn,
        (ToolStripItem) this.guiSearchBtn,
        (ToolStripItem) this.guiSearchLbl,
        (ToolStripItem) this.guiSearchTxt
      });
      this.toolbar.Location = new Point(0, 24);
      this.toolbar.Name = "toolbar";
      this.toolbar.Size = new Size(549, 39);
      this.toolbar.TabIndex = 0;
      this.toolbar.Text = "toolStrip";
      this.guiAddBtn.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.guiAddBtn.Image = (Image) componentResourceManager.GetObject("guiAddBtn.Image");
      this.guiAddBtn.ImageTransparentColor = Color.Magenta;
      this.guiAddBtn.Margin = new Padding(0, 1, 10, 2);
      this.guiAddBtn.Name = "guiAddBtn";
      this.guiAddBtn.Size = new Size(36, 36);
      this.guiAddBtn.Text = "&New";
      this.guiAddBtn.ToolTipText = "Create a new record";
      this.guiAddBtn.Click += new EventHandler(this.onRecordAdd);
      this.guiDeleteBtn.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.guiDeleteBtn.Enabled = false;
      this.guiDeleteBtn.Image = (Image) componentResourceManager.GetObject("guiDeleteBtn.Image");
      this.guiDeleteBtn.ImageTransparentColor = Color.Magenta;
      this.guiDeleteBtn.Margin = new Padding(0, 1, 10, 2);
      this.guiDeleteBtn.Name = "guiDeleteBtn";
      this.guiDeleteBtn.Size = new Size(36, 36);
      this.guiDeleteBtn.Text = "&Delete";
      this.guiDeleteBtn.ToolTipText = "Delete the current record";
      this.guiDeleteBtn.Click += new EventHandler(this.onRecordDelete);
      this.guiSaveBtn.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.guiSaveBtn.Enabled = false;
      this.guiSaveBtn.Image = (Image) componentResourceManager.GetObject("guiSaveBtn.Image");
      this.guiSaveBtn.ImageTransparentColor = Color.Magenta;
      this.guiSaveBtn.Margin = new Padding(0, 1, 10, 2);
      this.guiSaveBtn.Name = "guiSaveBtn";
      this.guiSaveBtn.Size = new Size(36, 36);
      this.guiSaveBtn.Text = "&Save";
      this.guiSaveBtn.ToolTipText = "Save the modifications";
      this.guiSaveBtn.Click += new EventHandler(this.onRecordSave);
      this.guiCancelBtn.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.guiCancelBtn.Enabled = false;
      this.guiCancelBtn.Image = (Image) componentResourceManager.GetObject("guiCancelBtn.Image");
      this.guiCancelBtn.ImageTransparentColor = Color.Magenta;
      this.guiCancelBtn.Margin = new Padding(0, 1, 10, 2);
      this.guiCancelBtn.Name = "guiCancelBtn";
      this.guiCancelBtn.Size = new Size(36, 36);
      this.guiCancelBtn.Text = "&Cancel";
      this.guiCancelBtn.ToolTipText = "Cancel the modifications";
      this.guiCancelBtn.Click += new EventHandler(this.onRecordUndo);
      this.guiEditBtn.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.guiEditBtn.Image = (Image) componentResourceManager.GetObject("guiEditBtn.Image");
      this.guiEditBtn.ImageTransparentColor = Color.Magenta;
      this.guiEditBtn.Margin = new Padding(0, 1, 10, 2);
      this.guiEditBtn.Name = "guiEditBtn";
      this.guiEditBtn.Size = new Size(36, 36);
      this.guiEditBtn.Text = "toolStripButton1";
      this.guiEditBtn.ToolTipText = "Edit the XML data";
      this.guiEditBtn.Click += new EventHandler(this.onRecordEdit);
      this.guiReportBtn.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.guiReportBtn.Image = (Image) componentResourceManager.GetObject("guiReportBtn.Image");
      this.guiReportBtn.ImageTransparentColor = Color.Magenta;
      this.guiReportBtn.Margin = new Padding(0, 1, 10, 2);
      this.guiReportBtn.Name = "guiReportBtn";
      this.guiReportBtn.Size = new Size(36, 36);
      this.guiReportBtn.Text = "toolStripButton1";
      this.guiReportBtn.ToolTipText = "Report the data";
      this.guiReportBtn.Click += new EventHandler(this.onRecordReport);
      this.guiSearchBtn.DisplayStyle = ToolStripItemDisplayStyle.Image;
      this.guiSearchBtn.Image = (Image) componentResourceManager.GetObject("guiSearchBtn.Image");
      this.guiSearchBtn.ImageTransparentColor = Color.Magenta;
      this.guiSearchBtn.Margin = new Padding(0, 1, 10, 2);
      this.guiSearchBtn.Name = "guiSearchBtn";
      this.guiSearchBtn.Size = new Size(36, 36);
      this.guiSearchBtn.Text = "btnSearch";
      this.guiSearchBtn.ToolTipText = "Search for a mission";
      this.guiSearchBtn.Click += new EventHandler(this.onRecordSearch);
      this.guiSearchLbl.Name = "guiSearchLbl";
      this.guiSearchLbl.Size = new Size(75, 36);
      this.guiSearchLbl.Text = "Search by ID:";
      this.guiSearchTxt.Margin = new Padding(1, 0, 0, 0);
      this.guiSearchTxt.Name = "guiSearchTxt";
      this.guiSearchTxt.Size = new Size(75, 39);
      this.guiSearchTxt.ToolTipText = "Search for an mission via its ID";
      this.guiSearchTxt.KeyDown += new KeyEventHandler(this.onRecordSearch);
      this.guiStatusLbl.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
      this.guiStatusLbl.AutoSize = true;
      this.guiStatusLbl.Location = new Point(6, 618);
      this.guiStatusLbl.Name = "guiStatusLbl";
      this.guiStatusLbl.Size = new Size(0, 13);
      this.guiStatusLbl.TabIndex = 2;
      this.menuStrip1.Items.AddRange(new ToolStripItem[2]
      {
        (ToolStripItem) this.fileToolStripMenuItem,
        (ToolStripItem) this.menuEdit
      });
      this.menuStrip1.Location = new Point(0, 0);
      this.menuStrip1.Name = "menuStrip1";
      this.menuStrip1.Size = new Size(549, 24);
      this.menuStrip1.TabIndex = 3;
      this.menuStrip1.Text = "menuStrip1";
      this.fileToolStripMenuItem.DropDownItems.AddRange(new ToolStripItem[1]
      {
        (ToolStripItem) this.exitToolStripMenuItem
      });
      this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
      this.fileToolStripMenuItem.Size = new Size(37, 20);
      this.fileToolStripMenuItem.Text = "File";
      this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
      this.exitToolStripMenuItem.ShortcutKeys = Keys.F4 | Keys.Alt;
      this.exitToolStripMenuItem.Size = new Size(152, 22);
      this.exitToolStripMenuItem.Text = "Exit";
      this.exitToolStripMenuItem.Click += new EventHandler(this.onExit);
      this.menuEdit.DropDownItems.AddRange(new ToolStripItem[7]
      {
        (ToolStripItem) this.menuAdd,
        (ToolStripItem) this.menuDelete,
        (ToolStripItem) this.toolStripSeparator1,
        (ToolStripItem) this.menuSave,
        (ToolStripItem) this.menuCancel,
        (ToolStripItem) this.toolStripSeparator5,
        (ToolStripItem) this.menuSearch
      });
      this.menuEdit.Name = "menuEdit";
      this.menuEdit.Size = new Size(39, 20);
      this.menuEdit.Text = "Edit";
      this.menuAdd.Name = "menuAdd";
      this.menuAdd.ShortcutKeys = Keys.N | Keys.Control;
      this.menuAdd.Size = new Size(152, 22);
      this.menuAdd.Text = "Add";
      this.menuAdd.Click += new EventHandler(this.onRecordAdd);
      this.menuDelete.Name = "menuDelete";
      this.menuDelete.Size = new Size(152, 22);
      this.menuDelete.Text = "Delete";
      this.menuDelete.Click += new EventHandler(this.onRecordDelete);
      this.toolStripSeparator1.Name = "toolStripSeparator1";
      this.toolStripSeparator1.Size = new Size(149, 6);
      this.menuSave.Name = "menuSave";
      this.menuSave.ShortcutKeys = Keys.S | Keys.Control;
      this.menuSave.Size = new Size(152, 22);
      this.menuSave.Text = "Save";
      this.menuSave.Click += new EventHandler(this.onRecordSave);
      this.menuCancel.Name = "menuCancel";
      this.menuCancel.ShortcutKeys = Keys.Z | Keys.Control;
      this.menuCancel.Size = new Size(152, 22);
      this.menuCancel.Text = "Cancel";
      this.menuCancel.Click += new EventHandler(this.onRecordUndo);
      this.toolStripSeparator5.Name = "toolStripSeparator5";
      this.toolStripSeparator5.Size = new Size(149, 6);
      this.menuSearch.Name = "menuSearch";
      this.menuSearch.ShortcutKeys = Keys.F | Keys.Control;
      this.menuSearch.Size = new Size(152, 22);
      this.menuSearch.Text = "Search";
      this.menuSearch.Click += new EventHandler(this.onRecordSearch);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.AutoSize = true;
      this.AutoSizeMode = AutoSizeMode.GrowAndShrink;
      this.ClientSize = new Size(549, 634);
      this.Controls.Add((Control) this.guiStatusLbl);
      this.Controls.Add((Control) this.toolbar);
      this.Controls.Add((Control) this.menuStrip1);
      this.Controls.Add((Control) this.guiTabControl);
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.Name = nameof (FrmMission);
      this.SizeGripStyle = SizeGripStyle.Hide;
      this.StartPosition = FormStartPosition.CenterScreen;
      this.Text = "Mission Editor";
      this.guiTabControl.ResumeLayout(false);
      this.toolbar.ResumeLayout(false);
      this.toolbar.PerformLayout();
      this.menuStrip1.ResumeLayout(false);
      this.menuStrip1.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public enum State
    {
      View,
      Edit,
      Add,
    }
  }
}
