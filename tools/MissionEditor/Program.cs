﻿// Decompiled with JetBrains decompiler
// Type: MissionEditor.Program
// Assembly: MissionEditor, Version=1.6.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 00D14066-6848-4104-A662-B9B4BF67FB6E
// Assembly location: D:\Server\eab\Tools\MissionEditor.exe

using CommonTools.Database;
using CommonTools.Gui;
using MissionEditor.Gui;
using Singleton;
using System;
using System.Windows.Forms;

namespace MissionEditor
{
  internal static class Program
  {
    public static FrmMission m_frmMission;

    [STAThread]
    private static void Main(string[] args)
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      Program.ApplicationAction applicationAction = Program.ApplicationAction.Run;
      Login login = new Login();
      if (applicationAction.Equals((object) Program.ApplicationAction.UpdateVersion))
        login.updateVersion();
      int num1 = (int) login.ShowDialog();
      if (!login.isValid())
        return;
      switch (applicationAction)
      {
        case Program.ApplicationAction.GenerateDatabaseStructure:
          Get<DB>.Instance.makeDatabaseVariables();
          break;
        case Program.ApplicationAction.Run:
          Program.m_frmMission = new FrmMission();
          int num2 = (int) Program.m_frmMission.ShowDialog();
          break;
        case Program.ApplicationAction.Search:
          DlgSearch dlgSearch = new DlgSearch();
          if (false)
            dlgSearch.configure(Net7.Tables.item_base);
          else
            dlgSearch.configure(Net7.Tables.missions);
          int num3 = (int) dlgSearch.ShowDialog();
          break;
      }
    }

    private enum ApplicationAction
    {
      GenerateDatabaseStructure,
      UpdateVersion,
      Run,
      Search,
    }
  }
}
