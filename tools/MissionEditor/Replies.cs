﻿// Decompiled with JetBrains decompiler
// Type: MissionEditor.Replies
// Assembly: MissionEditor, Version=1.6.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 00D14066-6848-4104-A662-B9B4BF67FB6E
// Assembly location: D:\Server\eab\Tools\MissionEditor.exe

using System.Windows.Forms;

namespace MissionEditor
{
  internal class Replies
  {
    public TreeNode treeNode;
    public ComboBox typeField;
    public TextBox idField;
    public TextBox textField;
    public Button selectField;

    public Replies(TreeNode treeNode, ComboBox type, TextBox id, TextBox text, Button select)
    {
      this.treeNode = treeNode;
      this.typeField = type;
      this.idField = id;
      this.textField = text;
      this.selectField = select;
    }
  }
}
