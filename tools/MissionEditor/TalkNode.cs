﻿// Decompiled with JetBrains decompiler
// Type: MissionEditor.TalkNode
// Assembly: MissionEditor, Version=1.6.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 00D14066-6848-4104-A662-B9B4BF67FB6E
// Assembly location: D:\Server\eab\Tools\MissionEditor.exe

using System.Windows.Forms;

namespace MissionEditor
{
  internal class TalkNode
  {
    public TreeNode parentNode;
    public TalkNodeTypes type;
    public string id;
    public string text;

    public TalkNode()
    {
      this.parentNode = (TreeNode) null;
      this.type = TalkNodeTypes.None;
      this.id = "";
      this.text = "";
    }

    public TalkNode(TreeNode parentNode, TalkNodeTypes type, string id, string text)
    {
      this.parentNode = parentNode;
      this.type = type;
      this.id = id;
      this.text = text;
    }

    public override string ToString()
    {
      if (this.parentNode == null)
        return this.id + ": " + this.text;
      switch (this.type)
      {
        case TalkNodeTypes.Branch:
          return this.id + ") " + this.text;
        case TalkNodeTypes.Trade:
          return "=> " + this.id;
        default:
          return "";
      }
    }
  }
}
