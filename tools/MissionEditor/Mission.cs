﻿// Decompiled with JetBrains decompiler
// Type: MissionEditor.Mission
// Assembly: MissionEditor, Version=1.6.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 00D14066-6848-4104-A662-B9B4BF67FB6E
// Assembly location: D:\Server\eab\Tools\MissionEditor.exe

using CommonTools;
using MissionEditor.Database;
using MissionEditor.Nodes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Xml;

namespace MissionEditor
{
  public class Mission
  {
    private string m_id;
    private bool m_forfeitable;
    private int m_allowedTime;
    private string m_name;
    private string m_summary;
    private int m_overallLevel;
    private string m_xml;
    private MissionType m_missionType;
    private string m_key;
    private SecLevels m_minSecLevel;
    private List<Condition> m_conditions;
    private List<Stage> m_stages;

    public Mission()
    {
      this.m_conditions = (List<Condition>) null;
      this.m_stages = (List<Stage>) null;
      this.clear();
      this.m_name = "";
      this.m_key = "";
    }

    public void clear()
    {
      this.m_id = "";
      this.m_forfeitable = true;
      this.m_allowedTime = 0;
      this.m_summary = "";
      this.m_overallLevel = -1;
      this.m_xml = "";
      if (this.hasConditions())
        this.m_conditions.Clear();
      if (!this.hasStages())
        return;
      this.m_stages.Clear();
    }

    public void setId(string id)
    {
      this.m_id = id;
    }

    public string getId()
    {
      return this.m_id;
    }

    public void setMinSecLvl(SecLevels minSecLevel)
    {
      this.m_minSecLevel = minSecLevel;
    }

    public SecLevels getMinSecLvl()
    {
      return this.m_minSecLevel;
    }

    public void setForfeitable(bool forfeitable)
    {
      this.m_forfeitable = forfeitable;
    }

    public bool isForfeitable()
    {
      return this.m_forfeitable;
    }

    public void setAllowedTime(int time)
    {
      this.m_allowedTime = time;
    }

    public int getAllowedTime()
    {
      return this.m_allowedTime;
    }

    public void setName(string name)
    {
      this.m_name = name;
    }

    public string getName()
    {
      return this.m_name;
    }

    public void setSummary(string summary)
    {
      this.m_summary = summary;
    }

    public string getSummary()
    {
      return this.m_summary;
    }

    public void setOverallLevel(int overallLevel)
    {
      this.m_overallLevel = overallLevel;
    }

    public int getOverallLevel()
    {
      return this.m_overallLevel;
    }

    public void setXml(string xml)
    {
      this.m_xml = xml;
    }

    public string getXml()
    {
      return this.m_xml;
    }

    public void setType(MissionType type)
    {
      this.m_missionType = type;
    }

    public MissionType getType()
    {
      return this.m_missionType;
    }

    public void setKey(string key)
    {
      this.m_key = key;
    }

    public string getKey()
    {
      return this.m_key;
    }

    public void addCondition(Condition condition)
    {
      if (this.m_conditions == null)
        this.m_conditions = new List<Condition>();
      this.m_conditions.Add(condition);
    }

    public bool hasConditions()
    {
      if (this.m_conditions != null)
        return this.m_conditions.Count != 0;
      return false;
    }

    public List<Condition> getConditions()
    {
      return this.m_conditions;
    }

    public void removeCondition(Condition condition)
    {
      if (this.m_conditions == null)
        return;
      this.m_conditions.Remove(condition);
    }

    public void clearConditions()
    {
      if (this.m_conditions == null)
        return;
      this.m_conditions.Clear();
    }

    public void addStage(Stage stage)
    {
      if (this.m_stages == null)
        this.m_stages = new List<Stage>();
      this.m_stages.Add(stage);
    }

    public bool hasStages()
    {
      if (this.m_stages != null)
        return this.m_stages.Count != 0;
      return false;
    }

    public List<Stage> getStages()
    {
      return this.m_stages;
    }

    public void removeStage(Stage stage)
    {
      if (this.m_stages == null)
        return;
      this.m_stages.Remove(stage);
    }

    public void clearStages()
    {
      if (this.m_stages == null)
        return;
      this.m_stages.Clear();
    }

    public void parseXml()
    {
      XmlDocument xmlDocument = new XmlDocument();
      xmlDocument.Load((TextReader) new StringReader(CommonTools.Xml.tagStart("Missions") + this.getXml() + CommonTools.Xml.tagEnd("Missions")));
      XmlNode documentElement = (XmlNode) xmlDocument.DocumentElement;
      if (!documentElement.HasChildNodes)
        throw new Exception("Invalid XML structure for: " + documentElement.Name + "\nShould not have a node without children");
      XmlNodeList childNodes = documentElement.ChildNodes;
      for (int index = 0; index <= childNodes.Count - 1; ++index)
      {
        XmlNode childNode = documentElement.ChildNodes[index];
        if (!childNode.Name.Equals(nameof (Mission)))
          throw new Exception("Unexpected node: " + childNode.Name + " in theMissions tag");
        this.clear();
        this.fromXML(childNode);
      }
    }

    private void fromXML(XmlNode xmlNode)
    {
      string str;
      CommonTools.Xml.getAttribute(xmlNode, "ID", true, out str);
      this.setId(str);
      if (CommonTools.Xml.getAttribute(xmlNode, "forfeitable", false, out str))
        this.setForfeitable(str.Equals("1"));
      int num1;
      CommonTools.Xml.getAttribute(xmlNode, "time", true, out num1);
      this.setAllowedTime(num1);
      if (CommonTools.Xml.getAttribute(xmlNode, "Overall", false, out num1))
        this.setOverallLevel(num1);
      foreach (XmlNode childNode in xmlNode.ChildNodes)
      {
        if (childNode.Name.Equals("Name"))
        {
          CommonTools.Xml.getValue(childNode, true, out str);
          this.setName(str);
        }
        else if (childNode.Name.Equals("Summary"))
        {
          CommonTools.Xml.getValue(childNode, true, out str);
          this.setSummary(str);
        }
        else if (childNode.Name.Equals("Condition"))
        {
          Condition condition = new Condition();
          condition.fromXml(childNode);
          this.addCondition(condition);
        }
        else if (childNode.Name.Equals("Stage"))
        {
          Stage stage = new Stage();
          stage.fromXml(childNode);
          this.addStage(stage);
        }
        else
        {
          int num2 = (int) MessageBox.Show("Unexpected node: " + childNode.Name + " in theMission tag");
        }
      }
      this.addValidations();
    }

    public string getXML()
    {
      StringWriter stringWriter = new StringWriter();
      stringWriter.WriteLine(CommonTools.Xml.tagStart() + nameof (Mission) + CommonTools.Xml.attribute("ID", this.getId()) + CommonTools.Xml.attribute("forfeitable", this.isForfeitable()) + CommonTools.Xml.attribute("time", this.getAllowedTime()) + CommonTools.Xml.tagEnd());
      stringWriter.WriteLine(CommonTools.Xml.tag("Name", this.getName()));
      stringWriter.WriteLine(CommonTools.Xml.tag("Summary", this.getSummary()));
      if (this.hasConditions())
      {
        foreach (Condition condition in this.getConditions())
          condition.getXML(stringWriter);
      }
      if (this.hasStages())
      {
        foreach (Stage stage in this.getStages())
          stage.getXML(stringWriter);
      }
      stringWriter.WriteLine(CommonTools.Xml.tagEnd(nameof (Mission)));
      return stringWriter.ToString();
    }

    public string getReport()
    {
      StringWriter stringWriter = new StringWriter();
      stringWriter.WriteLine("<HTML>");
      stringWriter.WriteLine("<BODY>");
      stringWriter.WriteLine("<H1>" + this.getName() + "</H1>");
      stringWriter.WriteLine(this.getSummary());
      stringWriter.WriteLine("<BR><BR><TABLE border=\"1\">");
      stringWriter.WriteLine("<TR><TH>ID</TH><TH>Forfeitable</TH><TH>Allowed Time</TH></TR>");
      stringWriter.WriteLine("<TR><TD>" + this.getId() + "</TD><TD>" + (this.isForfeitable() ? "Yes" : "&nbsp;") + "</TD><TD>" + (this.getAllowedTime() == 0 ? "Unlimited" : this.getAllowedTime().ToString()) + "</TD></TR>");
      stringWriter.WriteLine("</TABLE>");
      stringWriter.WriteLine("</BODY>");
      stringWriter.WriteLine("</HTML>");
      if (this.hasConditions())
      {
        stringWriter.WriteLine("<BR><TABLE border=\"1\">");
        stringWriter.WriteLine("<thead><tr><th colspan=\"2\">Conditions</th></tr></thead>");
        foreach (Condition condition in this.getConditions())
          condition.getReport(stringWriter);
        stringWriter.WriteLine("</TABLE>");
      }
      if (this.hasStages())
      {
        foreach (Stage stage in this.getStages())
        {
          stringWriter.WriteLine("<BR><TABLE border=\"1\">");
          stringWriter.WriteLine("<thead><tr><th colspan=\"2\">Stage " + stage.getId() + "</th></tr></thead>");
          if (stage.getId().CompareTo("0") != 0)
            stringWriter.WriteLine("<thead><tr><th colspan=\"2\">" + stage.getDescription() + "</th></tr></thead>");
          stringWriter.WriteLine("<tbody>");
          stage.getReport(stringWriter);
          stringWriter.WriteLine("</tbody></TABLE>");
        }
      }
      return stringWriter.ToString();
    }

    public void addValidations()
    {
      if (this.getId().Length == 0)
        DataConfiguration.addValidation("The mission Id is required.");
      else if (this.getKey().Length == 0)
        DataConfiguration.addValidation("The search key is mandatory.");
      else if (this.getName().Length == 0)
        DataConfiguration.addValidation("The mission name is mandatory.");
      else if (this.getSummary().Length == 0)
        DataConfiguration.addValidation("The mission summary is mandatory.");
      else if (this.m_overallLevel != -1 && (this.m_overallLevel < 1 || this.m_overallLevel > 150))
        DataConfiguration.addValidation("The overall level must be between 1 and 150");
      else if (!this.hasConditions())
        DataConfiguration.addValidation("Conditions are mandatory");
      else if (!this.hasStages())
      {
        DataConfiguration.addValidation("Stages are mandatory");
      }
      else
      {
        switch (this.getType())
        {
          case MissionType.Npc:
            DataConfiguration.addValidation(DataConfiguration.DataType.npc, this.getKey());
            break;
          case MissionType.Sector:
            DataConfiguration.addValidation(DataConfiguration.DataType.sector, this.getKey());
            break;
        }
        List<Stage> stages = this.getStages();
        for (int index = 0; index < stages.Count; ++index)
        {
          Stage stage = stages[index];
          if (int.Parse(stage.getId()) != index)
          {
            DataConfiguration.addValidation("Stage ID '" + (object) index + "' is missing or out of sequence (Stage: " + stage.getId() + ")");
            break;
          }
          stage.addValidations(Stage.ValidationType.Complete);
        }
      }
    }

    public void addFullValidations()
    {
      this.addValidations();
      if (this.hasConditions())
      {
        foreach (Condition condition in this.getConditions())
          condition.addValidations();
      }
      if (!this.hasStages())
        return;
      foreach (Stage stage in this.getStages())
      {
        stage.addValidations(Stage.ValidationType.Complete);
        if (stage.hasCompletions())
        {
          foreach (Completion completion in stage.getCompletions())
            completion.addValidations();
        }
        if (stage.hasRewards())
        {
          foreach (Reward reward in stage.getRewards())
            reward.addValidations();
        }
        if (stage.hasTalkTrees())
        {
          foreach (TalkTree talkTree in stage.getTalkTrees())
            talkTree.addValidations();
        }
      }
    }
  }
}
