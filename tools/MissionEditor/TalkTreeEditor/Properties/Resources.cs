﻿// Decompiled with JetBrains decompiler
// Type: TalkTreeEditor.Properties.Resources
// Assembly: MissionEditor, Version=1.6.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 00D14066-6848-4104-A662-B9B4BF67FB6E
// Assembly location: D:\Server\eab\Tools\MissionEditor.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace TalkTreeEditor.Properties
{
  [DebuggerNonUserCode]
  [CompilerGenerated]
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "2.0.0.0")]
  internal class Resources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal Resources()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (object.ReferenceEquals((object) TalkTreeEditor.Properties.Resources.resourceMan, (object) null))
          TalkTreeEditor.Properties.Resources.resourceMan = new ResourceManager("TalkTreeEditor.Properties.Resources", typeof (TalkTreeEditor.Properties.Resources).Assembly);
        return TalkTreeEditor.Properties.Resources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return TalkTreeEditor.Properties.Resources.resourceCulture;
      }
      set
      {
        TalkTreeEditor.Properties.Resources.resourceCulture = value;
      }
    }
  }
}
