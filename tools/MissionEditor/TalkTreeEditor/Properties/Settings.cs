﻿// Decompiled with JetBrains decompiler
// Type: TalkTreeEditor.Properties.Settings
// Assembly: MissionEditor, Version=1.6.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 00D14066-6848-4104-A662-B9B4BF67FB6E
// Assembly location: D:\Server\eab\Tools\MissionEditor.exe

using System.CodeDom.Compiler;
using System.Configuration;
using System.Runtime.CompilerServices;

namespace TalkTreeEditor.Properties
{
  [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "9.0.0.0")]
  [CompilerGenerated]
  internal sealed class Settings : ApplicationSettingsBase
  {
    private static Settings defaultInstance = (Settings) SettingsBase.Synchronized((SettingsBase) new Settings());

    public static Settings Default
    {
      get
      {
        return Settings.defaultInstance;
      }
    }
  }
}
