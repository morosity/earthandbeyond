﻿// Decompiled with JetBrains decompiler
// Type: FactionEditor.Properties.Settings
// Assembly: FactionEditor, Version=1.2.0.0, Culture=neutral, PublicKeyToken=2175665f88d36cc3
// MVID: 282C8E24-8545-40F1-BFC3-620198B9AA47
// Assembly location: D:\Server\eab\Tools\FactionEditor.exe

using SmartAssembly.SmartExceptionsCore;
using System;
using System.CodeDom.Compiler;
using System.Configuration;
using System.Runtime.CompilerServices;

namespace FactionEditor.Properties
{
  [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "9.0.0.0")]
  [CompilerGenerated]
  internal sealed class Settings : ApplicationSettingsBase
  {
    private static Settings \u0001;

    public static Settings Default
    {
      get
      {
        try
        {
          Settings settings = Settings.\u0001;
          return settings;
        }
        catch (Exception ex)
        {
          throw UnhandledException.\u0003(ex);
        }
      }
    }

    public Settings()
    {
      try
      {
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex, (object) this);
      }
    }

    static Settings()
    {
      try
      {
        // ISSUE: reference to a compiler-generated field
        // ISSUE: object of a compiler-generated type is created
        Settings.\u0001 = (Settings) \u009F\u0006.\u001D\u0011((SettingsBase) new Settings());
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex);
      }
    }
  }
}
