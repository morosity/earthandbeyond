﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: FactionEditor, Version=1.2.0.0, Culture=neutral, PublicKeyToken=2175665f88d36cc3
// MVID: 282C8E24-8545-40F1-BFC3-620198B9AA47
// Assembly location: D:\Server\eab\Tools\FactionEditor.exe

using \u0001;
using \u0006;
using \u0007;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraLayout;
using DevExpress.XtraLayout.Utils;
using DevExpress.XtraPivotGrid;
using DevExpress.XtraTab;
using SmartAssembly.SmartExceptionsCore;
using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace \u0007
{
  internal sealed class \u0003 : XtraForm
  {
    private DataTable \u0001 = new DataTable();
    private DataTable \u0002 = new DataTable();
    private string \u0001 = \u0003.\u0001(9458);
    private string \u0002 = \u0003.\u0001(9616);
    private string \u0003 = \u0003.\u0001(10186);
    [NonSerialized]
    internal static \u0002 \u0001;
    private \u0002 \u0001;
    private IContainer \u0001;
    private BarManager \u0001;
    private Bar \u0001;
    private Bar \u0002;
    private BarDockControl \u0001;
    private BarDockControl \u0002;
    private BarDockControl \u0003;
    private BarDockControl \u0004;
    private BarButtonItem \u0001;
    private BarButtonItem \u0002;
    private BarButtonItem \u0003;
    private BarButtonItem \u0004;
    private XtraTabControl \u0001;
    private XtraTabPage \u0001;
    private LayoutControl \u0001;
    private GridControl \u0001;
    private GridView \u0001;
    private GridColumn \u0001;
    private GridColumn \u0002;
    private MemoEdit \u0001;
    private TextEdit \u0001;
    private TextEdit \u0002;
    private CheckEdit \u0001;
    private LayoutControlGroup \u0001;
    private LayoutControlItem \u0001;
    private LayoutControlItem \u0002;
    private LayoutControlItem \u0003;
    private LayoutControlItem \u0004;
    private LayoutControlItem \u0005;
    private SimpleSeparator \u0001;
    private XtraTabPage \u0002;
    private PivotGridControl \u0001;
    private PivotGridField \u0001;
    private PivotGridField \u0002;
    private PivotGridField \u0003;
    private PivotGridField \u0004;
    private RepositoryItemTextEdit \u0001;
    private BarButtonItem \u0005;

    public \u0003([In] ref \u0002 obj0)
    {
      try
      {
        this.\u0007();
        this.\u0001 = obj0;
      }
      catch (Exception ex)
      {
        \u0002 obj = obj0;
        throw UnhandledException.\u0003(ex, (object) this, (object) obj);
      }
    }

    private void \u0003([In] object obj0, [In] EventArgs obj1)
    {
      try
      {
        \u001B\u0006.\u007E\u009C\u000F((object) this.\u0001);
      }
      catch (Exception ex)
      {
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u0003()
    {
      try
      {
        \u001B\u0006.\u007E\u0003\u0012((object) this.\u0001);
        \u008A\u0005.\u007E\u0004\u0012((object) this.\u0001, \u0007.\u0001.\u0003(this.\u0001));
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex, (object) this);
      }
    }

    private void \u0004()
    {
      try
      {
        \u0007.\u0001.\u0003(this.\u0001);
        \u0007.\u0001.\u0003(this.\u0001, this.\u0001);
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex, (object) this);
      }
    }

    private int \u0003()
    {
      DataTable dataTable1;
      try
      {
        dataTable1 = \u0007.\u0001.\u0003(\u0003.\u0001(10444));
        return \u008D\u0005.\u0098\u0005(\u001D\u0005.\u007E\u0011\u0004(\u008D\u0002.\u007E\u0097\u0011((object) \u0012\u0006.\u007E\u009B\u0011((object) \u0092\u0002.\u007E\u0002\u0012((object) dataTable1), 0), \u0003.\u0001(10497))));
      }
      catch (Exception ex)
      {
        DataTable dataTable2 = dataTable1;
        throw UnhandledException.\u0003(ex, (object) dataTable2, (object) this);
      }
    }

    private void \u0005()
    {
      try
      {
        \u001B\u0006.\u007E\u0003\u0012((object) this.\u0002);
        \u008A\u0005.\u007E\u0004\u0012((object) this.\u0002, \u0007.\u0001.\u0003(this.\u0002));
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex, (object) this);
      }
    }

    private void \u0006()
    {
      try
      {
        \u0007.\u0001.\u0003(this.\u0002);
        \u0007.\u0001.\u0003(this.\u0002, this.\u0003);
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex, (object) this);
      }
    }

    private void \u0004([In] object obj0, [In] EventArgs obj1)
    {
      try
      {
        this.\u0003();
        this.\u0005();
        Binding binding1 = \u000F\u0007.\u007E\u0081\u000F((object) \u0088\u0002.\u007E\u0080\u000E((object) this.\u0001), \u0003.\u0001(10518), (object) this.\u0001, \u0003.\u0001(10531), true, DataSourceUpdateMode.OnPropertyChanged);
        Binding binding2 = \u000F\u0007.\u007E\u0081\u000F((object) \u0088\u0002.\u007E\u0080\u000E((object) this.\u0001), \u0003.\u0001(10518), (object) this.\u0001, \u0003.\u0001(10540), true, DataSourceUpdateMode.OnPropertyChanged);
        Binding binding3 = \u000F\u0007.\u007E\u0081\u000F((object) \u0088\u0002.\u007E\u0080\u000E((object) this.\u0002), \u0003.\u0001(10518), (object) this.\u0001, \u0003.\u0001(10557), true, DataSourceUpdateMode.OnPropertyChanged);
        Binding binding4 = \u000F\u0007.\u007E\u0081\u000F((object) \u0088\u0002.\u007E\u0080\u000E((object) this.\u0001), \u0003.\u0001(10518), (object) this.\u0001, \u0003.\u0001(10570), true, DataSourceUpdateMode.OnPropertyChanged);
        \u0080\u0002.\u007E\u0081\u0002((object) this.\u0001, (object) this.\u0001);
        \u0080\u0002.\u007E\u0006((object) this.\u0001, (object) this.\u0002);
      }
      catch (Exception ex)
      {
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u0003([In] object obj0, [In] ItemClickEventArgs obj1)
    {
      object[] objArray1;
      try
      {
        \u0001\u0003 obj = \u0001\u0003.\u007E\u009C\u0011;
        DataRowCollection dataRowCollection = \u0092\u0002.\u007E\u0002\u0012((object) this.\u0001);
        objArray1 = new object[5]
        {
          (object) (this.\u0003() + 1),
          (object) \u0003.\u0001(10587),
          (object) \u0003.\u0001(10587),
          (object) 1,
          (object) \u0003.\u0001(10587)
        };
        object[] objArray2 = objArray1;
        DataRow dataRow = obj((object) dataRowCollection, objArray2);
        \u001B\u0006.\u007E\u0008\u0002((object) this.\u0001);
      }
      catch (Exception ex)
      {
        object[] objArray2 = objArray1;
        object obj = obj0;
        ItemClickEventArgs itemClickEventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) objArray2, (object) this, obj, (object) itemClickEventArgs);
      }
    }

    private void \u0004([In] object obj0, [In] ItemClickEventArgs obj1)
    {
      try
      {
        this.\u0004();
        this.\u0006();
        this.\u0005();
      }
      catch (Exception ex)
      {
        object obj = obj0;
        ItemClickEventArgs itemClickEventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) this, obj, (object) itemClickEventArgs);
      }
    }

    private void \u0005([In] object obj0, [In] ItemClickEventArgs obj1)
    {
      int num;
      GridView gridView1;
      GridControl gridControl1;
      try
      {
        gridControl1 = this.\u0001;
        gridView1 = \u0087\u0005.\u007E\u0082\u0002((object) \u0006\u0007.\u007E\u0080\u0002((object) gridControl1), 0) as GridView;
        num = \u0080\u0006.\u007E\u0007\u0002((object) gridView1);
        if (num < 0)
          return;
        \u0017\u0004.\u007E\u000E\u0002((object) gridView1, num);
      }
      catch (Exception ex)
      {
        GridControl gridControl2 = gridControl1;
        GridView gridView2 = gridView1;
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) num;
        object obj = obj0;
        ItemClickEventArgs itemClickEventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) gridControl2, (object) gridView2, (object) local, (object) this, obj, (object) itemClickEventArgs);
      }
    }

    private void \u0006([In] object obj0, [In] ItemClickEventArgs obj1)
    {
      try
      {
        this.\u0003();
        this.\u0005();
      }
      catch (Exception ex)
      {
        object obj = obj0;
        ItemClickEventArgs itemClickEventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) this, obj, (object) itemClickEventArgs);
      }
    }

    private void \u0003([In] object obj0, [In] EditValueChangedEventArgs obj1)
    {
      PivotDrillDownDataSource drillDownDataSource1;
      try
      {
        drillDownDataSource1 = \u0012\u0005.\u007E\u0016((object) obj1);
        if (\u0002\u0004.\u007E\u0015((object) obj1) == this.\u0003 && \u0080\u0006.\u007E\u0086\u0002((object) drillDownDataSource1) > 0)
          \u001D\u0002.\u007E\u0084\u0002((object) \u008E\u0003.\u007E\u0087\u0002((object) drillDownDataSource1, 0), (PivotGridFieldBase) this.\u0003, (object) \u0003\u0007.\u0089\u0005(\u0097\u0005.\u007E\u000F\u0003((object) \u0094\u0006.\u007E\u0017((object) obj1))));
        if (\u0002\u0004.\u007E\u0015((object) obj1) != this.\u0004 || \u0080\u0006.\u007E\u0086\u0002((object) drillDownDataSource1) <= 0)
          return;
        \u001D\u0002.\u007E\u0084\u0002((object) \u008E\u0003.\u007E\u0087\u0002((object) drillDownDataSource1, 0), (PivotGridFieldBase) this.\u0004, (object) \u0003\u0007.\u0089\u0005(\u0097\u0005.\u007E\u000F\u0003((object) \u0094\u0006.\u007E\u0017((object) obj1))));
      }
      catch (Exception ex)
      {
        PivotDrillDownDataSource drillDownDataSource2 = drillDownDataSource1;
        object obj = obj0;
        EditValueChangedEventArgs changedEventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) drillDownDataSource2, (object) this, obj, (object) changedEventArgs);
      }
    }

    private void \u0007([In] object obj0, [In] ItemClickEventArgs obj1)
    {
      \u0004 obj2;
      try
      {
        obj2 = new \u0004();
        int num = (int) \u0089\u0006.\u007E\u009F\u000F((object) obj2);
      }
      catch (Exception ex)
      {
        \u0004 obj3 = obj2;
        object obj4 = obj0;
        ItemClickEventArgs itemClickEventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) obj3, (object) this, obj4, (object) itemClickEventArgs);
      }
    }

    protected override void Dispose([In] bool obj0)
    {
      try
      {
        if (obj0 && this.\u0001 != null)
          \u001B\u0006.\u007E\u001E\u0004((object) this.\u0001);
        \u0098\u0006.\u0087\u0003((object) this, obj0);
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<bool> local = (ValueType) obj0;
        throw UnhandledException.\u0003(ex, (object) this, (object) local);
      }
    }

    private void \u0007()
    {
      PivotGridStyleFormatCondition styleFormatCondition1;
      Bar[] barArray1;
      BarItem[] barItemArray1;
      LinkPersistInfo[] linkPersistInfoArray1;
      XtraTabPage[] xtraTabPageArray1;
      BaseView[] baseViewArray1;
      GridColumn[] gridColumnArray1;
      BaseLayoutItem[] baseLayoutItemArray1;
      PivotGridField[] pivotGridFieldArray1;
      PivotGridStyleFormatCondition styleFormatCondition2;
      PivotGridStyleFormatCondition styleFormatCondition3;
      PivotGridStyleFormatCondition styleFormatCondition4;
      PivotGridStyleFormatCondition[] styleFormatConditionArray1;
      RepositoryItem[] repositoryItemArray1;
      try
      {
        this.\u0001 = (IContainer) new Container();
        styleFormatCondition2 = new PivotGridStyleFormatCondition();
        styleFormatCondition3 = new PivotGridStyleFormatCondition();
        styleFormatCondition4 = new PivotGridStyleFormatCondition();
        styleFormatCondition1 = new PivotGridStyleFormatCondition();
        this.\u0003 = new PivotGridField();
        this.\u0001 = new RepositoryItemTextEdit();
        this.\u0004 = new PivotGridField();
        this.\u0001 = new BarManager(this.\u0001);
        this.\u0001 = new Bar();
        this.\u0001 = new BarButtonItem();
        this.\u0002 = new BarButtonItem();
        this.\u0003 = new BarButtonItem();
        this.\u0004 = new BarButtonItem();
        this.\u0005 = new BarButtonItem();
        this.\u0002 = new Bar();
        this.\u0001 = new BarDockControl();
        this.\u0002 = new BarDockControl();
        this.\u0003 = new BarDockControl();
        this.\u0004 = new BarDockControl();
        this.\u0001 = new XtraTabControl();
        this.\u0001 = new XtraTabPage();
        this.\u0001 = new LayoutControl();
        this.\u0001 = new MemoEdit();
        this.\u0001 = new TextEdit();
        this.\u0002 = new TextEdit();
        this.\u0001 = new CheckEdit();
        this.\u0001 = new GridControl();
        this.\u0001 = new GridView();
        this.\u0001 = new GridColumn();
        this.\u0002 = new GridColumn();
        this.\u0001 = new LayoutControlGroup();
        this.\u0001 = new LayoutControlItem();
        this.\u0002 = new LayoutControlItem();
        this.\u0003 = new LayoutControlItem();
        this.\u0004 = new LayoutControlItem();
        this.\u0005 = new LayoutControlItem();
        this.\u0001 = new SimpleSeparator();
        this.\u0002 = new XtraTabPage();
        this.\u0001 = new PivotGridControl();
        this.\u0001 = new PivotGridField();
        this.\u0002 = new PivotGridField();
        \u001B\u0006.\u007E\u0018\u0011((object) this.\u0001);
        \u001B\u0006.\u007E\u0018\u0011((object) this.\u0001);
        \u001B\u0006.\u007E\u0018\u0011((object) this.\u0001);
        \u001B\u0006.\u007E\u0015\u000F((object) this.\u0001);
        \u001B\u0006.\u007E\u0015\u000F((object) this.\u0001);
        \u001B\u0006.\u007E\u0018\u0011((object) this.\u0001);
        \u001B\u0006.\u007E\u0015\u000F((object) this.\u0001);
        \u001B\u0006.\u007E\u0018\u0011((object) \u008E\u0004.\u007E\u001E\u0003((object) this.\u0001));
        \u001B\u0006.\u007E\u0018\u0011((object) \u009D\u0002.\u007E\u0011\u0003((object) this.\u0001));
        \u001B\u0006.\u007E\u0018\u0011((object) \u009D\u0002.\u007E\u0011\u0003((object) this.\u0002));
        \u001B\u0006.\u007E\u0018\u0011((object) \u0097\u0006.\u007E\u0018\u0003((object) this.\u0001));
        \u001B\u0006.\u007E\u0018\u0011((object) this.\u0001);
        \u001B\u0006.\u007E\u0018\u0011((object) this.\u0001);
        \u001B\u0006.\u007E\u0018\u0011((object) this.\u0001);
        \u001B\u0006.\u007E\u0018\u0011((object) this.\u0001);
        \u001B\u0006.\u007E\u0018\u0011((object) this.\u0002);
        \u001B\u0006.\u007E\u0018\u0011((object) this.\u0003);
        \u001B\u0006.\u007E\u0018\u0011((object) this.\u0004);
        \u001B\u0006.\u007E\u0018\u0011((object) this.\u0005);
        \u001B\u0006.\u007E\u0018\u0011((object) this.\u0001);
        \u001B\u0006.\u007E\u0015\u000F((object) this.\u0002);
        \u001B\u0006.\u007E\u0018\u0011((object) this.\u0001);
        \u001B\u0006.\u0088\u0003((object) this);
        \u008C\u0004.\u007E\u008B\u0002((object) this.\u0003, PivotGridAllowedAreas.FilterArea | PivotGridAllowedAreas.DataArea);
        \u008A\u0004.\u007E\u0089\u0002((object) this.\u0003, PivotArea.DataArea);
        \u0017\u0004.\u007E\u008A\u0002((object) this.\u0003, 0);
        \u001B\u0005.\u007E\u008D\u0002((object) this.\u0003, \u0003.\u0001(10588));
        \u0091\u0006.\u007E\u001A((object) this.\u0003, (RepositoryItem) this.\u0001);
        \u001B\u0005.\u007E\u008C\u0002((object) this.\u0003, \u0003.\u0001(10609));
        \u001B\u0005.\u007E\u0088\u0002((object) this.\u0003, \u0003.\u0001(10630));
        \u0098\u0006.\u007E\u0005\u0003((object) this.\u0001, false);
        \u0016\u0006.\u007E\u0006\u0003((object) this.\u0001, BorderStyles.NoBorder);
        \u001B\u0005.\u007E\u0004\u0003((object) this.\u0001, \u0003.\u0001(10647));
        \u008C\u0004.\u007E\u008B\u0002((object) this.\u0004, PivotGridAllowedAreas.FilterArea | PivotGridAllowedAreas.DataArea);
        \u0017\u0004.\u007E\u008A\u0002((object) this.\u0004, 0);
        \u001B\u0005.\u007E\u008D\u0002((object) this.\u0004, \u0003.\u0001(10680));
        \u0091\u0006.\u007E\u001A((object) this.\u0004, (RepositoryItem) this.\u0001);
        \u001B\u0005.\u007E\u008C\u0002((object) this.\u0004, \u0003.\u0001(10697));
        \u001B\u0005.\u007E\u0088\u0002((object) this.\u0004, \u0003.\u0001(10714));
        \u0017\u0003 obj1 = \u0017\u0003.\u007E\u0084;
        Bars bars = \u009A\u0003.\u007E\u0090((object) this.\u0001);
        barArray1 = new Bar[2]{ this.\u0001, this.\u0002 };
        Bar[] barArray2 = barArray1;
        obj1((object) bars, barArray2);
        int num1 = \u009F\u0004.\u007E\u009B((object) \u0096\u0003.\u007E\u008F((object) this.\u0001), this.\u0001);
        int num2 = \u009F\u0004.\u007E\u009B((object) \u0096\u0003.\u007E\u008F((object) this.\u0001), this.\u0002);
        int num3 = \u009F\u0004.\u007E\u009B((object) \u0096\u0003.\u007E\u008F((object) this.\u0001), this.\u0003);
        int num4 = \u009F\u0004.\u007E\u009B((object) \u0096\u0003.\u007E\u008F((object) this.\u0001), this.\u0004);
        \u0093\u0004.\u007E\u0091((object) this.\u0001, (Control) this);
        \u001A\u0003 obj2 = \u001A\u0003.\u007E\u009D;
        BarItems barItems = \u007F\u0004.\u007E\u0092((object) this.\u0001);
        barItemArray1 = new BarItem[5]
        {
          (BarItem) this.\u0001,
          (BarItem) this.\u0003,
          (BarItem) this.\u0002,
          (BarItem) this.\u0004,
          (BarItem) this.\u0005
        };
        BarItem[] barItemArray2 = barItemArray1;
        obj2((object) barItems, barItemArray2);
        \u0013\u0003.\u007E\u0093((object) this.\u0001, this.\u0001);
        \u0017\u0004.\u007E\u008E((object) this.\u0001, 5);
        \u0013\u0003.\u007E\u0094((object) this.\u0001, this.\u0002);
        \u001B\u0005.\u007E\u001E((object) this.\u0001, \u0003.\u0001(10735));
        \u0017\u0004.\u007E\u0083((object) this.\u0001, 0);
        \u0017\u0004.\u007E\u0082((object) this.\u0001, 0);
        \u0013\u0007.\u007E\u0080((object) this.\u0001, BarDockStyle.Top);
        \u0082\u0003 obj3 = \u0082\u0003.\u007E\u009E;
        LinksInfo linksInfo = \u001F\u0004.\u007E\u0081((object) this.\u0001);
        linkPersistInfoArray1 = new LinkPersistInfo[5]
        {
          new LinkPersistInfo((BarItem) this.\u0001),
          new LinkPersistInfo((BarItem) this.\u0002),
          new LinkPersistInfo((BarItem) this.\u0003),
          new LinkPersistInfo((BarItem) this.\u0004, true),
          new LinkPersistInfo((BarItem) this.\u0005)
        };
        LinkPersistInfo[] linkPersistInfoArray2 = linkPersistInfoArray1;
        obj3((object) linksInfo, linkPersistInfoArray2);
        \u0098\u0006.\u007E\u0086((object) \u0098\u0003.\u007E\u001D((object) this.\u0001), false);
        \u0098\u0006.\u007E\u0087((object) \u0098\u0003.\u007E\u001D((object) this.\u0001), true);
        \u0098\u0006.\u007E\u0088((object) \u0098\u0003.\u007E\u001D((object) this.\u0001), true);
        \u0098\u0006.\u007E\u008A((object) \u0098\u0003.\u007E\u001D((object) this.\u0001), true);
        \u0098\u0006.\u007E\u008B((object) \u0098\u0003.\u007E\u001D((object) this.\u0001), true);
        \u001B\u0005.\u007E\u001F((object) this.\u0001, \u0003.\u0001(10735));
        \u001B\u0005.\u007E\u0098((object) this.\u0001, \u0003.\u0001(10748));
        // ISSUE: reference to a compiler-generated method
        \u009C\u0005.\u007E\u0097((object) this.\u0001, (Image) \u0005.\u0003());
        \u0017\u0004.\u007E\u0096((object) this.\u0001, 0);
        \u001B\u0005.\u007E\u0095((object) this.\u0001, \u0003.\u0001(10753));
        \u001E\u0005.\u007E\u0099((object) this.\u0001, new ItemClickEventHandler(this.\u0003));
        \u001B\u0005.\u007E\u0098((object) this.\u0002, \u0003.\u0001(10770));
        // ISSUE: reference to a compiler-generated method
        \u009C\u0005.\u007E\u0097((object) this.\u0002, (Image) \u0005.\u0004());
        \u0017\u0004.\u007E\u0096((object) this.\u0002, 2);
        \u001B\u0005.\u007E\u0095((object) this.\u0002, \u0003.\u0001(10779));
        \u001E\u0005.\u007E\u0099((object) this.\u0002, new ItemClickEventHandler(this.\u0004));
        \u001B\u0005.\u007E\u0098((object) this.\u0003, \u0003.\u0001(10788));
        // ISSUE: reference to a compiler-generated method
        \u009C\u0005.\u007E\u0097((object) this.\u0003, (Image) \u0005.\u0007());
        \u0017\u0004.\u007E\u0096((object) this.\u0003, 1);
        \u001B\u0005.\u007E\u0095((object) this.\u0003, \u0003.\u0001(10797));
        \u001E\u0005.\u007E\u0099((object) this.\u0003, new ItemClickEventHandler(this.\u0005));
        \u001B\u0005.\u007E\u0098((object) this.\u0004, \u0003.\u0001(10810));
        // ISSUE: reference to a compiler-generated method
        \u009C\u0005.\u007E\u0097((object) this.\u0004, (Image) \u0005.\u0008());
        \u0017\u0004.\u007E\u0096((object) this.\u0004, 3);
        \u001B\u0005.\u007E\u0095((object) this.\u0004, \u0003.\u0001(10823));
        \u001E\u0005.\u007E\u0099((object) this.\u0004, new ItemClickEventHandler(this.\u0006));
        \u001B\u0005.\u007E\u0098((object) this.\u0005, \u0003.\u0001(10836));
        // ISSUE: reference to a compiler-generated method
        \u009C\u0005.\u007E\u0097((object) this.\u0005, (Image) \u0005.\u0005());
        \u0017\u0004.\u007E\u0096((object) this.\u0005, 4);
        \u001B\u0005.\u007E\u0095((object) this.\u0005, \u0003.\u0001(10845));
        \u001E\u0005.\u007E\u0099((object) this.\u0005, new ItemClickEventHandler(this.\u0007));
        \u001B\u0005.\u007E\u001E((object) this.\u0002, \u0003.\u0001(10858));
        \u0098\u0004.\u007E\u007F((object) this.\u0002, BarCanDockStyle.Bottom);
        \u0017\u0004.\u007E\u0083((object) this.\u0002, 0);
        \u0017\u0004.\u007E\u0082((object) this.\u0002, 0);
        \u0013\u0007.\u007E\u0080((object) this.\u0002, BarDockStyle.Bottom);
        \u0098\u0006.\u007E\u0086((object) \u0098\u0003.\u007E\u001D((object) this.\u0002), false);
        \u0098\u0006.\u007E\u0089((object) \u0098\u0003.\u007E\u001D((object) this.\u0002), false);
        \u0098\u0006.\u007E\u008B((object) \u0098\u0003.\u007E\u001D((object) this.\u0002), true);
        \u001B\u0005.\u007E\u001F((object) this.\u0002, \u0003.\u0001(10858));
        \u009C\u0006.\u007E\u0018\u000E((object) this.\u0001, AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
        \u000F\u0004.\u007E\u008C\u000E((object) this.\u0001, new Point(0, 32));
        \u001B\u0005.\u007E\u008F\u000E((object) this.\u0001, \u0003.\u0001(10875));
        \u0004\u0006.\u007E\u007F\u0003((object) this.\u0001, this.\u0001);
        \u0018\u0004.\u007E\u0092\u000E((object) this.\u0001, new Size(826, 534));
        \u0017\u0004.\u007E\u0093\u000E((object) this.\u0001, 6);
        \u0015\u0004 obj4 = \u0015\u0004.\u007E\u0084\u0003;
        XtraTabPageCollection tabPageCollection = \u0080\u0005.\u007E\u0080\u0003((object) this.\u0001);
        xtraTabPageArray1 = new XtraTabPage[2]
        {
          this.\u0001,
          this.\u0002
        };
        XtraTabPage[] xtraTabPageArray2 = xtraTabPageArray1;
        obj4((object) tabPageCollection, xtraTabPageArray2);
        \u0093\u0004.\u007E\u0016\u000F((object) \u009E\u0002.\u007E\u001F\u000E((object) this.\u0001), (Control) this.\u0001);
        \u001B\u0005.\u007E\u008F\u000E((object) this.\u0001, \u0003.\u0001(10896));
        \u0018\u0004.\u007E\u0083\u0003((object) this.\u0001, new Size(822, 509));
        \u001B\u0005.\u007E\u0096\u000E((object) this.\u0001, \u0003.\u0001(10917));
        \u0093\u0004.\u007E\u0016\u000F((object) \u009E\u0002.\u007E\u001F\u000E((object) this.\u0001), (Control) this.\u0001);
        \u0093\u0004.\u007E\u0016\u000F((object) \u009E\u0002.\u007E\u001F\u000E((object) this.\u0001), (Control) this.\u0001);
        \u0093\u0004.\u007E\u0016\u000F((object) \u009E\u0002.\u007E\u001F\u000E((object) this.\u0001), (Control) this.\u0002);
        \u0093\u0004.\u007E\u0016\u000F((object) \u009E\u0002.\u007E\u001F\u000E((object) this.\u0001), (Control) this.\u0001);
        \u0093\u0004.\u007E\u0016\u000F((object) \u009E\u0002.\u007E\u001F\u000E((object) this.\u0001), (Control) this.\u0001);
        \u0011\u0007.\u007E\u0081\u000E((object) this.\u0001, DockStyle.Fill);
        \u000F\u0004.\u007E\u008C\u000E((object) this.\u0001, new Point(0, 0));
        \u001B\u0005.\u007E\u008F\u000E((object) this.\u0001, \u0003.\u0001(10938));
        \u0092\u0006.\u007E\u009B\u0002((object) this.\u0001, this.\u0001);
        \u0018\u0004.\u007E\u0092\u000E((object) this.\u0001, new Size(822, 509));
        \u0017\u0004.\u007E\u0093\u000E((object) this.\u0001, 0);
        \u001B\u0005.\u007E\u0096\u000E((object) this.\u0001, \u0003.\u0001(10938));
        \u000F\u0004.\u007E\u008C\u000E((object) this.\u0001, new Point(85, 60));
        \u0015\u0006.\u007E\u000E\u0003((object) this.\u0001, (IDXMenuManager) this.\u0001);
        \u001B\u0005.\u007E\u008F\u000E((object) this.\u0001, \u0003.\u0001(10959));
        \u0018\u0004.\u007E\u0092\u000E((object) this.\u0001, new Size(725, 124));
        \u001F\u0006.\u007E\u0008\u0003((object) this.\u0001, (IStyleController) this.\u0001);
        \u0017\u0004.\u007E\u0093\u000E((object) this.\u0001, 6);
        \u000F\u0004.\u007E\u008C\u000E((object) this.\u0001, new Point(85, 12));
        \u0015\u0006.\u007E\u000E\u0003((object) this.\u0001, (IDXMenuManager) this.\u0001);
        \u001B\u0005.\u007E\u008F\u000E((object) this.\u0001, \u0003.\u0001(10976));
        \u0018\u0004.\u007E\u0092\u000E((object) this.\u0001, new Size(595, 20));
        \u001F\u0006.\u007E\u0008\u0003((object) this.\u0001, (IStyleController) this.\u0001);
        \u0017\u0004.\u007E\u0093\u000E((object) this.\u0001, 5);
        \u000F\u0004.\u007E\u008C\u000E((object) this.\u0002, new Point(85, 36));
        \u0015\u0006.\u007E\u000E\u0003((object) this.\u0002, (IDXMenuManager) this.\u0001);
        \u001B\u0005.\u007E\u008F\u000E((object) this.\u0002, \u0003.\u0001(10993));
        \u0018\u0004.\u007E\u0092\u000E((object) this.\u0002, new Size(595, 20));
        \u001F\u0006.\u007E\u0008\u0003((object) this.\u0002, (IStyleController) this.\u0001);
        \u0017\u0004.\u007E\u0093\u000E((object) this.\u0002, 4);
        \u000F\u0004.\u007E\u008C\u000E((object) this.\u0001, new Point(684, 12));
        \u0015\u0006.\u007E\u000E\u0003((object) this.\u0001, (IDXMenuManager) this.\u0001);
        \u001B\u0005.\u007E\u008F\u000E((object) this.\u0001, \u0003.\u0001(11006));
        \u001B\u0005.\u007E\u0014\u0003((object) \u0097\u0006.\u007E\u0018\u0003((object) this.\u0001), \u0003.\u0001(11023));
        \u001C\u0005.\u007E\u0013\u0003((object) \u0097\u0006.\u007E\u0018\u0003((object) this.\u0001), CheckStyles.Style16);
        \u0011\u0006.\u007E\u0012\u0003((object) \u0097\u0006.\u007E\u0018\u0003((object) this.\u0001), StyleIndeterminate.Inactive);
        \u0080\u0002.\u007E\u0015\u0003((object) \u0097\u0006.\u007E\u0018\u0003((object) this.\u0001), (object) (sbyte) 1);
        \u0080\u0002.\u007E\u0016\u0003((object) \u0097\u0006.\u007E\u0018\u0003((object) this.\u0001), (object) (sbyte) 0);
        \u0018\u0004.\u007E\u0092\u000E((object) this.\u0001, new Size(126, 22));
        \u001F\u0006.\u007E\u0008\u0003((object) this.\u0001, (IStyleController) this.\u0001);
        \u0017\u0004.\u007E\u0093\u000E((object) this.\u0001, 7);
        \u009C\u0006.\u007E\u0018\u000E((object) this.\u0001, AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
        \u000F\u0004.\u007E\u008C\u000E((object) this.\u0001, new Point(12, 190));
        \u0097\u0004.\u007E\u007F\u0002((object) this.\u0001, (BaseView) this.\u0001);
        \u0015\u0006.\u007E\u0019\u0003((object) this.\u0001, (IDXMenuManager) this.\u0001);
        \u001B\u0005.\u007E\u008F\u000E((object) this.\u0001, \u0003.\u0001(11040));
        \u0018\u0004.\u007E\u0092\u000E((object) this.\u0001, new Size(798, 307));
        \u0017\u0004.\u007E\u0093\u000E((object) this.\u0001, 8);
        \u007F\u0003 obj5 = \u007F\u0003.\u007E\u0083\u0002;
        ViewRepositoryCollection repositoryCollection = \u008F\u0004.\u007E\u001F\u0002((object) this.\u0001);
        baseViewArray1 = new BaseView[1]
        {
          (BaseView) this.\u0001
        };
        BaseView[] baseViewArray2 = baseViewArray1;
        obj5((object) repositoryCollection, baseViewArray2);
        \u0005\u0004 obj6 = \u0005\u0004.\u007E\u001A\u0002;
        GridColumnCollection columnCollection = \u009D\u0003.\u007E\u0006\u0002((object) this.\u0001);
        gridColumnArray1 = new GridColumn[2]
        {
          this.\u0001,
          this.\u0002
        };
        GridColumn[] gridColumnArray2 = gridColumnArray1;
        obj6((object) columnCollection, gridColumnArray2);
        \u0004\u0003.\u007E\u0005\u0002((object) this.\u0001, this.\u0001);
        \u001B\u0005.\u007E\u0004\u0002((object) this.\u0001, \u0003.\u0001(11057));
        \u0098\u0006.\u007E\u001B\u0002((object) \u008F\u0005.\u007E\u0010\u0002((object) this.\u0001), false);
        \u0098\u0006.\u007E\u0002\u0002((object) \u0080\u0004.\u007E\u0011\u0002((object) this.\u0001), false);
        \u0098\u0006.\u007E\u0003\u0002((object) \u0080\u0004.\u007E\u0011\u0002((object) this.\u0001), false);
        \u001B\u0005.\u007E\u0015\u0002((object) this.\u0001, \u0003.\u0001(11070));
        \u001B\u0005.\u007E\u0016\u0002((object) this.\u0001, \u0003.\u0001(11083));
        \u001B\u0005.\u007E\u0013\u0002((object) this.\u0001, \u0003.\u0001(11100));
        \u0098\u0006.\u007E\u001C\u0002((object) \u0014\u0003.\u007E\u0014\u0002((object) this.\u0001), false);
        \u0098\u0006.\u007E\u001D\u0002((object) \u0014\u0003.\u007E\u0014\u0002((object) this.\u0001), true);
        \u0098\u0006.\u007E\u0018\u0002((object) this.\u0001, true);
        \u0017\u0004.\u007E\u0017\u0002((object) this.\u0001, 0);
        \u0017\u0004.\u007E\u0019\u0002((object) this.\u0001, 81);
        \u001B\u0005.\u007E\u0015\u0002((object) this.\u0002, \u0003.\u0001(11117));
        \u001B\u0005.\u007E\u0016\u0002((object) this.\u0002, \u0003.\u0001(10531));
        \u001B\u0005.\u007E\u0013\u0002((object) this.\u0002, \u0003.\u0001(11134));
        \u0098\u0006.\u007E\u001C\u0002((object) \u0014\u0003.\u007E\u0014\u0002((object) this.\u0002), false);
        \u0098\u0006.\u007E\u001D\u0002((object) \u0014\u0003.\u007E\u0014\u0002((object) this.\u0002), true);
        \u0098\u0006.\u007E\u0018\u0002((object) this.\u0002, true);
        \u0017\u0004.\u007E\u0017\u0002((object) this.\u0002, 1);
        \u0017\u0004.\u007E\u0019\u0002((object) this.\u0002, 262);
        \u001B\u0005.\u007E\u0097\u0002((object) this.\u0001, \u0003.\u0001(11151));
        \u007F\u0005.\u007E\u0001\u0003((object) this.\u0001, DefaultBoolean.True);
        \u0098\u0006.\u007E\u009F\u0002((object) this.\u0001, false);
        \u008E\u0002 obj7 = \u008E\u0002.\u007E\u0099\u0002;
        LayoutGroupItemCollection groupItemCollection = \u0097\u0003.\u007E\u0003\u0003((object) this.\u0001);
        baseLayoutItemArray1 = new BaseLayoutItem[6]
        {
          (BaseLayoutItem) this.\u0001,
          (BaseLayoutItem) this.\u0002,
          (BaseLayoutItem) this.\u0003,
          (BaseLayoutItem) this.\u0004,
          (BaseLayoutItem) this.\u0005,
          (BaseLayoutItem) this.\u0001
        };
        BaseLayoutItem[] baseLayoutItemArray2 = baseLayoutItemArray1;
        obj7((object) groupItemCollection, baseLayoutItemArray2);
        \u000F\u0004.\u007E\u0094\u0002((object) this.\u0001, new Point(0, 0));
        \u001B\u0005.\u007E\u008F\u0002((object) this.\u0001, \u0003.\u0001(11180));
        \u0018\u0004.\u007E\u0095\u0002((object) this.\u0001, new Size(822, 509));
        \u0091\u0004.\u007E\u0093\u0002((object) this.\u0001, new Padding(0, 0, 0, 0));
        \u001B\u0005.\u007E\u0096\u0002((object) this.\u0001, \u0003.\u0001(11180));
        \u0098\u0006.\u007E\u0090\u0002((object) this.\u0001, false);
        \u0093\u0004.\u007E\u009D\u0002((object) this.\u0001, (Control) this.\u0002);
        \u001B\u0005.\u007E\u0097\u0002((object) this.\u0001, \u0003.\u0001(11189));
        \u000F\u0004.\u007E\u0094\u0002((object) this.\u0001, new Point(0, 24));
        \u001B\u0005.\u007E\u008F\u0002((object) this.\u0001, \u0003.\u0001(11202));
        \u0018\u0004.\u007E\u0095\u0002((object) this.\u0001, new Size(672, 24));
        \u001B\u0005.\u007E\u0096\u0002((object) this.\u0001, \u0003.\u0001(11189));
        \u0018\u0004.\u007E\u0091\u0002((object) this.\u0001, new Size(69, 13));
        \u0093\u0004.\u007E\u009D\u0002((object) this.\u0002, (Control) this.\u0001);
        \u001B\u0005.\u007E\u0097\u0002((object) this.\u0002, \u0003.\u0001(11227));
        \u000F\u0004.\u007E\u0094\u0002((object) this.\u0002, new Point(0, 0));
        \u001B\u0005.\u007E\u008F\u0002((object) this.\u0002, \u0003.\u0001(11248));
        \u0018\u0004.\u007E\u0095\u0002((object) this.\u0002, new Size(672, 24));
        \u001B\u0005.\u007E\u0096\u0002((object) this.\u0002, \u0003.\u0001(11227));
        \u0018\u0004.\u007E\u0091\u0002((object) this.\u0002, new Size(69, 13));
        \u0093\u0004.\u007E\u009D\u0002((object) this.\u0003, (Control) this.\u0001);
        \u001B\u0005.\u007E\u0097\u0002((object) this.\u0003, \u0003.\u0001(11273));
        \u000F\u0004.\u007E\u0094\u0002((object) this.\u0003, new Point(0, 48));
        \u001B\u0005.\u007E\u008F\u0002((object) this.\u0003, \u0003.\u0001(11290));
        \u0018\u0004.\u007E\u0095\u0002((object) this.\u0003, new Size(802, 128));
        \u001B\u0005.\u007E\u0096\u0002((object) this.\u0003, \u0003.\u0001(11273));
        \u0018\u0004.\u007E\u0091\u0002((object) this.\u0003, new Size(69, 13));
        \u0093\u0004.\u007E\u009D\u0002((object) this.\u0004, (Control) this.\u0001);
        \u001B\u0005.\u007E\u0097\u0002((object) this.\u0004, \u0003.\u0001(11315));
        \u000F\u0004.\u007E\u0094\u0002((object) this.\u0004, new Point(672, 0));
        \u001B\u0005.\u007E\u008F\u0002((object) this.\u0004, \u0003.\u0001(11315));
        \u0018\u0004.\u007E\u0095\u0002((object) this.\u0004, new Size(130, 48));
        \u001B\u0005.\u007E\u0096\u0002((object) this.\u0004, \u0003.\u0001(11315));
        \u0018\u0004.\u007E\u0091\u0002((object) this.\u0004, new Size(0, 0));
        \u0017\u0004.\u007E\u0092\u0002((object) this.\u0004, 0);
        \u0098\u0006.\u007E\u0090\u0002((object) this.\u0004, false);
        \u0093\u0004.\u007E\u009D\u0002((object) this.\u0005, (Control) this.\u0001);
        \u001B\u0005.\u007E\u0097\u0002((object) this.\u0005, \u0003.\u0001(11340));
        \u000F\u0004.\u007E\u0094\u0002((object) this.\u0005, new Point(0, 178));
        \u001B\u0005.\u007E\u008F\u0002((object) this.\u0005, \u0003.\u0001(11340));
        \u0018\u0004.\u007E\u0095\u0002((object) this.\u0005, new Size(802, 311));
        \u001B\u0005.\u007E\u0096\u0002((object) this.\u0005, \u0003.\u0001(11340));
        \u0018\u0004.\u007E\u0091\u0002((object) this.\u0005, new Size(0, 0));
        \u0017\u0004.\u007E\u0092\u0002((object) this.\u0005, 0);
        \u0098\u0006.\u007E\u0090\u0002((object) this.\u0005, false);
        \u001B\u0005.\u007E\u0097\u0002((object) this.\u0001, \u0003.\u0001(11365));
        \u000F\u0004.\u007E\u0094\u0002((object) this.\u0001, new Point(0, 176));
        \u001B\u0005.\u007E\u008F\u0002((object) this.\u0001, \u0003.\u0001(11365));
        \u0018\u0004.\u007E\u0095\u0002((object) this.\u0001, new Size(802, 2));
        \u001B\u0005.\u007E\u0096\u0002((object) this.\u0001, \u0003.\u0001(11365));
        \u0093\u0004.\u007E\u0016\u000F((object) \u009E\u0002.\u007E\u001F\u000E((object) this.\u0002), (Control) this.\u0001);
        \u001B\u0005.\u007E\u008F\u000E((object) this.\u0002, \u0003.\u0001(11390));
        \u0018\u0004.\u007E\u0083\u0003((object) this.\u0002, new Size(822, 509));
        \u001B\u0005.\u007E\u0096\u000E((object) this.\u0002, \u0003.\u0001(11411));
        \u009C\u0006.\u007E\u0018\u000E((object) this.\u0001, AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
        \u0096\u0004.\u007E\u007F\u000E((object) this.\u0001, \u0080\u0003.\u0082\u000F());
        \u0016\u0003 obj8 = \u0016\u0003.\u007E\u001B;
        PivotGridFieldCollection gridFieldCollection = \u009C\u0003.\u007E\u0007((object) this.\u0001);
        pivotGridFieldArray1 = new PivotGridField[4]
        {
          this.\u0001,
          this.\u0002,
          this.\u0003,
          this.\u0004
        };
        PivotGridField[] pivotGridFieldArray2 = pivotGridFieldArray1;
        obj8((object) gridFieldCollection, pivotGridFieldArray2);
        \u0013\u0004.\u007E\u008B\u0003((object) \u0094\u0002.\u007E\u001A\u0003((object) styleFormatCondition2), \u008B\u0005.\u008F\u0010());
        \u0098\u0006.\u007E\u008D\u0003((object) \u001F\u0003.\u007E\u008C\u0003((object) \u0094\u0002.\u007E\u001A\u0003((object) styleFormatCondition2)), true);
        \u009E\u0003.\u007E\u001B\u0003((object) styleFormatCondition2, FormatConditionEnum.Less);
        \u0084\u0002.\u007E\u0013((object) styleFormatCondition2, (PivotGridFieldBase) this.\u0003);
        \u001B\u0005.\u007E\u0014((object) styleFormatCondition2, \u0003.\u0001(10630));
        \u0080\u0002.\u007E\u001C\u0003((object) styleFormatCondition2, (object) \u0003.\u0001(11432));
        \u0013\u0004.\u007E\u008B\u0003((object) \u0094\u0002.\u007E\u001A\u0003((object) styleFormatCondition3), \u008B\u0005.\u008F\u0010());
        \u0098\u0006.\u007E\u008D\u0003((object) \u001F\u0003.\u007E\u008C\u0003((object) \u0094\u0002.\u007E\u001A\u0003((object) styleFormatCondition3)), true);
        \u009E\u0003.\u007E\u001B\u0003((object) styleFormatCondition3, FormatConditionEnum.Less);
        \u0084\u0002.\u007E\u0013((object) styleFormatCondition3, (PivotGridFieldBase) this.\u0004);
        \u001B\u0005.\u007E\u0014((object) styleFormatCondition3, \u0003.\u0001(10714));
        \u0080\u0002.\u007E\u001C\u0003((object) styleFormatCondition3, (object) \u0003.\u0001(11432));
        \u0013\u0004.\u007E\u008B\u0003((object) \u0094\u0002.\u007E\u001A\u0003((object) styleFormatCondition4), \u008B\u0005.\u008E\u0010());
        \u0098\u0006.\u007E\u008D\u0003((object) \u001F\u0003.\u007E\u008C\u0003((object) \u0094\u0002.\u007E\u001A\u0003((object) styleFormatCondition4)), true);
        \u009E\u0003.\u007E\u001B\u0003((object) styleFormatCondition4, FormatConditionEnum.Greater);
        \u0084\u0002.\u007E\u0013((object) styleFormatCondition4, (PivotGridFieldBase) this.\u0004);
        \u001B\u0005.\u007E\u0014((object) styleFormatCondition4, \u0003.\u0001(10714));
        \u0080\u0002.\u007E\u001C\u0003((object) styleFormatCondition4, (object) \u0003.\u0001(11432));
        \u0013\u0004.\u007E\u008B\u0003((object) \u0094\u0002.\u007E\u001A\u0003((object) styleFormatCondition1), \u008B\u0005.\u008E\u0010());
        \u0098\u0006.\u007E\u008D\u0003((object) \u001F\u0003.\u007E\u008C\u0003((object) \u0094\u0002.\u007E\u001A\u0003((object) styleFormatCondition1)), true);
        \u009E\u0003.\u007E\u001B\u0003((object) styleFormatCondition1, FormatConditionEnum.Greater);
        \u0084\u0002.\u007E\u0013((object) styleFormatCondition1, (PivotGridFieldBase) this.\u0003);
        \u001B\u0005.\u007E\u0014((object) styleFormatCondition1, \u0003.\u0001(10630));
        \u0080\u0002.\u007E\u001C\u0003((object) styleFormatCondition1, (object) \u0003.\u0001(11432));
        \u0019\u0003 obj9 = \u0019\u0003.\u007E\u0011;
        PivotGridFormatConditionCollection conditionCollection = \u009B\u0003.\u007E\u0008((object) this.\u0001);
        styleFormatConditionArray1 = new PivotGridStyleFormatCondition[4]
        {
          styleFormatCondition2,
          styleFormatCondition3,
          styleFormatCondition4,
          styleFormatCondition1
        };
        PivotGridStyleFormatCondition[] styleFormatConditionArray2 = styleFormatConditionArray1;
        obj9((object) conditionCollection, styleFormatConditionArray2);
        \u000F\u0004.\u007E\u008C\u000E((object) this.\u0001, new Point(-2, 0));
        \u001B\u0005.\u007E\u008F\u000E((object) this.\u0001, \u0003.\u0001(11437));
        \u0098\u0006.\u007E\u008E\u0002((object) \u009F\u0003.\u007E\u000E((object) this.\u0001), false);
        \u000E\u0007 obj10 = \u000E\u0007.\u007E\u0081\u0003;
        RepositoryItemCollection repositoryItemCollection = \u008D\u0004.\u007E\u0010((object) this.\u0001);
        repositoryItemArray1 = new RepositoryItem[1]
        {
          (RepositoryItem) this.\u0001
        };
        RepositoryItem[] repositoryItemArray2 = repositoryItemArray1;
        obj10((object) repositoryItemCollection, repositoryItemArray2);
        \u0018\u0004.\u007E\u0092\u000E((object) this.\u0001, new Size(826, 508));
        \u0017\u0004.\u007E\u0093\u000E((object) this.\u0001, 0);
        \u0081\u0003.\u007E\u000F((object) this.\u0001, new EditValueChangedEventHandler(this.\u0003));
        \u008C\u0004.\u007E\u008B\u0002((object) this.\u0001, PivotGridAllowedAreas.ColumnArea);
        \u008A\u0004.\u007E\u0089\u0002((object) this.\u0001, PivotArea.ColumnArea);
        \u0017\u0004.\u007E\u008A\u0002((object) this.\u0001, 0);
        \u001B\u0005.\u007E\u008D\u0002((object) this.\u0001, \u0003.\u0001(11446));
        \u001B\u0005.\u007E\u008C\u0002((object) this.\u0001, \u0003.\u0001(11463));
        \u001B\u0005.\u007E\u0088\u0002((object) this.\u0001, \u0003.\u0001(11484));
        \u008C\u0004.\u007E\u008B\u0002((object) this.\u0002, PivotGridAllowedAreas.RowArea);
        \u008A\u0004.\u007E\u0089\u0002((object) this.\u0002, PivotArea.RowArea);
        \u0017\u0004.\u007E\u008A\u0002((object) this.\u0002, 0);
        \u001B\u0005.\u007E\u008D\u0002((object) this.\u0002, \u0003.\u0001(11509));
        \u001B\u0005.\u007E\u008C\u0002((object) this.\u0002, \u0003.\u0001(11530));
        \u001B\u0005.\u007E\u0088\u0002((object) this.\u0002, \u0003.\u0001(11559));
        \u0089\u0003.\u0019\u000F((object) this, new SizeF(6f, 13f));
        \u0010\u0005.\u001A\u000F((object) this, AutoScaleMode.Font);
        \u0018\u0004.\u0090\u000F((object) this, new Size(826, 594));
        \u0093\u0004.\u007E\u0016\u000F((object) \u009E\u0002.\u001F\u000E((object) this), (Control) this.\u0001);
        \u0093\u0004.\u007E\u0016\u000F((object) \u009E\u0002.\u001F\u000E((object) this), (Control) this.\u0003);
        \u0093\u0004.\u007E\u0016\u000F((object) \u009E\u0002.\u001F\u000E((object) this), (Control) this.\u0004);
        \u0093\u0004.\u007E\u0016\u000F((object) \u009E\u0002.\u001F\u000E((object) this), (Control) this.\u0002);
        \u0093\u0004.\u007E\u0016\u000F((object) \u009E\u0002.\u001F\u000E((object) this), (Control) this.\u0001);
        \u0098\u0006.\u007E\u008E\u0003((object) \u001C\u0002.\u008A\u0003((object) this), true);
        \u001B\u0005.\u008F\u000E((object) this, \u0003.\u0001(11584));
        \u001B\u0005.\u007E\u0096\u000E((object) this, \u0003.\u0001(11593));
        \u009A\u0005.\u009A\u000F((object) this, new EventHandler(this.\u0004));
        \u009A\u0005.\u009B\u000F((object) this, new EventHandler(this.\u0003));
        \u001B\u0006.\u007E\u0019\u0011((object) this.\u0001);
        \u001B\u0006.\u007E\u0019\u0011((object) this.\u0001);
        \u001B\u0006.\u007E\u0019\u0011((object) this.\u0001);
        \u0098\u0006.\u007E\u0010\u000F((object) this.\u0001, false);
        \u0098\u0006.\u007E\u0010\u000F((object) this.\u0001, false);
        \u001B\u0006.\u007E\u0019\u0011((object) this.\u0001);
        \u0098\u0006.\u007E\u0010\u000F((object) this.\u0001, false);
        \u001B\u0006.\u007E\u0019\u0011((object) \u008E\u0004.\u007E\u001E\u0003((object) this.\u0001));
        \u001B\u0006.\u007E\u0019\u0011((object) \u009D\u0002.\u007E\u0011\u0003((object) this.\u0001));
        \u001B\u0006.\u007E\u0019\u0011((object) \u009D\u0002.\u007E\u0011\u0003((object) this.\u0002));
        \u001B\u0006.\u007E\u0019\u0011((object) \u0097\u0006.\u007E\u0018\u0003((object) this.\u0001));
        \u001B\u0006.\u007E\u0019\u0011((object) this.\u0001);
        \u001B\u0006.\u007E\u0019\u0011((object) this.\u0001);
        \u001B\u0006.\u007E\u0019\u0011((object) this.\u0001);
        \u001B\u0006.\u007E\u0019\u0011((object) this.\u0001);
        \u001B\u0006.\u007E\u0019\u0011((object) this.\u0002);
        \u001B\u0006.\u007E\u0019\u0011((object) this.\u0003);
        \u001B\u0006.\u007E\u0019\u0011((object) this.\u0004);
        \u001B\u0006.\u007E\u0019\u0011((object) this.\u0005);
        \u001B\u0006.\u007E\u0019\u0011((object) this.\u0001);
        \u0098\u0006.\u007E\u0010\u000F((object) this.\u0002, false);
        \u001B\u0006.\u007E\u0019\u0011((object) this.\u0001);
        \u0098\u0006.\u0089\u0003((object) this, false);
      }
      catch (Exception ex)
      {
        object[] objArray = new object[15]
        {
          (object) styleFormatCondition2,
          (object) styleFormatCondition3,
          (object) styleFormatCondition4,
          (object) styleFormatCondition1,
          (object) barArray1,
          (object) barItemArray1,
          (object) linkPersistInfoArray1,
          (object) xtraTabPageArray1,
          (object) baseViewArray1,
          (object) gridColumnArray1,
          (object) baseLayoutItemArray1,
          (object) pivotGridFieldArray1,
          (object) styleFormatConditionArray1,
          (object) repositoryItemArray1,
          (object) this
        };
        throw UnhandledException.\u0003(ex, objArray);
      }
    }

    static \u0003()
    {
      \u0003.\u0003();
    }
  }
}
