﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: FactionEditor, Version=1.2.0.0, Culture=neutral, PublicKeyToken=2175665f88d36cc3
// MVID: 282C8E24-8545-40F1-BFC3-620198B9AA47
// Assembly location: D:\Server\eab\Tools\FactionEditor.exe

using \u0001;
using \u0006;
using \u0007;
using SmartAssembly.SmartExceptionsCore;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace \u0007
{
  internal sealed class \u0002 : Form
  {
    [NonSerialized]
    internal static \u0002 \u0001;
    private IContainer \u0001;
    private Label \u0001;

    protected override void Dispose([In] bool obj0)
    {
      try
      {
        if (obj0 && this.\u0001 != null)
          \u001B\u0006.\u007E\u001E\u0004((object) this.\u0001);
        \u0098\u0006.\u009D\u000F((object) this, obj0);
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<bool> local = (ValueType) obj0;
        throw UnhandledException.\u0003(ex, (object) this, (object) local);
      }
    }

    private void \u0003()
    {
      try
      {
        this.\u0001 = new Label();
        \u001B\u0006.\u0015\u000F((object) this);
        \u0098\u0006.\u007E\u0019\u000E((object) this.\u0001, true);
        \u0013\u0004.\u007E\u001A\u000E((object) this.\u0001, \u008B\u0005.\u008D\u0010());
        \u0013\u0004.\u007E\u0087\u000E((object) this.\u0001, \u008B\u0005.\u0090\u0010());
        \u000F\u0004.\u007E\u008C\u000E((object) this.\u0001, new Point(13, 254));
        \u001B\u0005.\u007E\u008F\u000E((object) this.\u0001, \u0002.\u0001(9001));
        \u0018\u0004.\u007E\u0092\u000E((object) this.\u0001, new Size(0, 13));
        \u0017\u0004.\u007E\u0093\u000E((object) this.\u0001, 0);
        \u0089\u0003.\u0019\u000F((object) this, new SizeF(6f, 13f));
        \u0010\u0005.\u001A\u000F((object) this, AutoScaleMode.Font);
        \u0014\u0007.\u007E\u001B\u000E((object) this, ImageLayout.None);
        \u0018\u0004.\u0090\u000F((object) this, new Size(453, 279));
        \u0098\u0006.\u0091\u000F((object) this, false);
        \u0093\u0004.\u007E\u0016\u000F((object) \u009E\u0002.\u001F\u000E((object) this), (Control) this.\u0001);
        \u0014\u0005.\u008E\u000F((object) this, FormBorderStyle.None);
        \u001B\u0005.\u008F\u000E((object) this, \u0002.\u0001(9438));
        \u0098\u0006.\u0096\u000F((object) this, false);
        \u0098\u0006.\u0095\u000F((object) this, false);
        \u0015\u0007.\u0098\u000F((object) this, FormStartPosition.CenterScreen);
        \u001B\u0005.\u007E\u0096\u000E((object) this, \u0002.\u0001(9438));
        \u0098\u0006.\u0099\u000F((object) this, true);
        \u0083\u0004.\u009D\u000E((object) this, new PaintEventHandler(this.\u0003));
        \u0098\u0006.\u0010\u000F((object) this, false);
        \u001B\u0006.\u000E\u000F((object) this);
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex, (object) this);
      }
    }

    public \u0002()
    {
      try
      {
        this.\u0003();
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex, (object) this);
      }
    }

    public void \u0003([In] string obj0)
    {
      try
      {
        \u001B\u0005.\u007E\u0096\u000E((object) this.\u0001, obj0);
        \u001B\u0006.\u007E\u000F\u000F((object) this);
        this.\u0004();
      }
      catch (Exception ex)
      {
        string str = obj0;
        throw UnhandledException.\u0003(ex, (object) this, (object) str);
      }
    }

    private void \u0004()
    {
    }

    private void \u0003([In] object obj0, [In] PaintEventArgs obj1)
    {
    }

    protected override void OnPaintBackground([In] PaintEventArgs obj0)
    {
      Graphics graphics1;
      try
      {
        graphics1 = \u0008\u0004.\u007E\u0018\u000F((object) obj0);
        // ISSUE: reference to a compiler-generated method
        \u0084\u0006.\u007E\u0096\u0010((object) graphics1, (Image) \u0005.\u0006(), new Rectangle(0, 0, \u0080\u0006.\u009A\u000E((object) this), \u0080\u0006.\u0088\u000E((object) this)));
      }
      catch (Exception ex)
      {
        Graphics graphics2 = graphics1;
        PaintEventArgs paintEventArgs = obj0;
        throw UnhandledException.\u0003(ex, (object) graphics2, (object) this, (object) paintEventArgs);
      }
    }

    static \u0002()
    {
      \u0003.\u0003();
    }
  }
}
