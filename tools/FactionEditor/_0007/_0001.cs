﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: FactionEditor, Version=1.2.0.0, Culture=neutral, PublicKeyToken=2175665f88d36cc3
// MVID: 282C8E24-8545-40F1-BFC3-620198B9AA47
// Assembly location: D:\Server\eab\Tools\FactionEditor.exe

using \u0001;
using MySql.Data.MySqlClient;
using SmartAssembly.SmartExceptionsCore;
using System;
using System.Data;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace \u0007
{
  internal static class \u0001
  {
    [NonSerialized]
    internal static \u0002 \u0001;
    private static MySqlConnection \u0001;
    private static Timer \u0001;
    private static string \u0001;
    private static string \u0002;
    private static string \u0003;
    private static int \u0001;
    private static bool \u0001;

    [SpecialName]
    public static string \u0003()
    {
      try
      {
        return \u0007.\u0001.\u0001;
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex);
      }
    }

    [SpecialName]
    public static void \u0003([In] string obj0)
    {
      try
      {
        \u0007.\u0001.\u0001 = obj0;
      }
      catch (Exception ex)
      {
        string str = obj0;
        throw UnhandledException.\u0003(ex, (object) str);
      }
    }

    [SpecialName]
    public static string \u0004()
    {
      try
      {
        return \u0007.\u0001.\u0002;
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex);
      }
    }

    [SpecialName]
    public static void \u0004([In] string obj0)
    {
      try
      {
        \u0007.\u0001.\u0002 = obj0;
      }
      catch (Exception ex)
      {
        string str = obj0;
        throw UnhandledException.\u0003(ex, (object) str);
      }
    }

    [SpecialName]
    public static string \u0005()
    {
      try
      {
        return \u0007.\u0001.\u0003;
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex);
      }
    }

    [SpecialName]
    public static void \u0005([In] string obj0)
    {
      try
      {
        \u0007.\u0001.\u0003 = obj0;
      }
      catch (Exception ex)
      {
        string str = obj0;
        throw UnhandledException.\u0003(ex, (object) str);
      }
    }

    [SpecialName]
    public static int \u0003()
    {
      try
      {
        return \u0007.\u0001.\u0001;
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex);
      }
    }

    [SpecialName]
    public static void \u0003([In] int obj0)
    {
      try
      {
        \u0007.\u0001.\u0001 = obj0;
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) obj0;
        throw UnhandledException.\u0003(ex, (object) local);
      }
    }

    public static string \u0003([In] string obj0)
    {
      string[] strArray1;
      try
      {
        strArray1 = new string[11]
        {
          \u0007.\u0001.\u0001(9264),
          obj0,
          \u0007.\u0001.\u0001(9341),
          \u0007.\u0001.\u0001,
          \u0007.\u0001.\u0001(9350),
          \u0007.\u0001.\u0001.ToString(),
          \u0007.\u0001.\u0001(9359),
          \u0007.\u0001.\u0002,
          \u0007.\u0001.\u0001(9376),
          \u0007.\u0001.\u0003,
          \u0007.\u0001.\u0001(9393)
        };
        return \u0007\u0004.\u0096\u0004(strArray1);
      }
      catch (Exception ex)
      {
        string[] strArray2 = strArray1;
        string str = obj0;
        throw UnhandledException.\u0003(ex, (object) strArray2, (object) str);
      }
    }

    public static void \u0003()
    {
      try
      {
        \u0007.\u0001.\u0001 = new MySqlConnection(\u0007.\u0001.\u0003(\u0007.\u0001.\u0001(8834)));
        \u001B\u0006.\u007E\u000F\u0012((object) \u0007.\u0001.\u0001);
        \u0007.\u0001.\u0001 = true;
        \u0007.\u0001.\u0001 = new Timer();
        \u0017\u0004.\u007E\u0087\u0010((object) \u0007.\u0001.\u0001, 120000);
        \u0098\u0006.\u007E\u0086\u0010((object) \u0007.\u0001.\u0001, true);
        \u009A\u0005.\u007E\u0084\u0010((object) \u0007.\u0001.\u0001, new EventHandler(\u0007.\u0001.\u0003));
        \u001B\u0006.\u007E\u0088\u0010((object) \u0007.\u0001.\u0001);
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex);
      }
    }

    private static void \u0003([In] object obj0, [In] EventArgs obj1)
    {
      try
      {
        \u0007.\u0001.\u0004();
      }
      catch (Exception ex)
      {
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0003(ex, obj, (object) eventArgs);
      }
    }

    public static void \u0004()
    {
      try
      {
        if (!\u0007.\u0001.\u0001 || \u0001\u0007.\u007E\u0004((object) \u0007.\u0001.\u0001))
          return;
        \u0007.\u0001.\u0003();
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex);
      }
    }

    public static void \u0003([In] DataTable obj0)
    {
      int num;
      try
      {
        if (obj0 == null)
          return;
        for (num = 0; num < \u0080\u0006.\u007E\u0096\u0011((object) \u0092\u0002.\u007E\u0002\u0012((object) obj0)); ++num)
        {
          if (\u001E\u0006.\u007E\u009A\u0011((object) \u0012\u0006.\u007E\u009B\u0011((object) \u0092\u0002.\u007E\u0002\u0012((object) obj0), num), DataRowVersion.Proposed))
            \u001B\u0006.\u007E\u0099\u0011((object) \u0012\u0006.\u007E\u009B\u0011((object) \u0092\u0002.\u007E\u0002\u0012((object) obj0), num));
        }
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) num;
        DataTable dataTable = obj0;
        throw UnhandledException.\u0003(ex, (object) local, (object) dataTable);
      }
    }

    public static void \u0003([In] DataTable obj0, [In] string obj1)
    {
      MySqlCommandBuilder sqlCommandBuilder1;
      MySqlDataAdapter adapter;
      try
      {
        if (\u009B\u0002.\u0080\u0004(\u001D\u0005.\u007E\u0007\u0012((object) \u0007.\u0001.\u0001), \u0007.\u0001.\u0001(8834)))
          \u001B\u0005.\u007E\u000E\u0012((object) \u0007.\u0001.\u0001, \u0007.\u0001.\u0001(8834));
        adapter = new MySqlDataAdapter(obj1, \u0007.\u0001.\u0001);
        sqlCommandBuilder1 = new MySqlCommandBuilder(adapter);
        \u0089\u0004.\u007E\u0006\u0012((object) sqlCommandBuilder1, ConflictOption.OverwriteChanges);
        int num = \u0091\u0002.\u007E\u0011\u0012((object) adapter, obj0);
      }
      catch (Exception ex)
      {
        MySqlDataAdapter mySqlDataAdapter = adapter;
        MySqlCommandBuilder sqlCommandBuilder2 = sqlCommandBuilder1;
        DataTable dataTable = obj0;
        string str = obj1;
        throw UnhandledException.\u0003(ex, (object) mySqlDataAdapter, (object) sqlCommandBuilder2, (object) dataTable, (object) str);
      }
    }

    public static DataTable \u0003([In] string obj0)
    {
      MySqlDataAdapter mySqlDataAdapter1;
      DataTable dataTable1;
      try
      {
        dataTable1 = new DataTable();
        if (\u009B\u0002.\u0080\u0004(\u001D\u0005.\u007E\u0007\u0012((object) \u0007.\u0001.\u0001), \u0007.\u0001.\u0001(8834)))
          \u001B\u0005.\u007E\u000E\u0012((object) \u0007.\u0001.\u0001, \u0007.\u0001.\u0001(8834));
        mySqlDataAdapter1 = new MySqlDataAdapter(obj0, \u0007.\u0001.\u0001);
        int num = \u0091\u0002.\u007E\u0010\u0012((object) mySqlDataAdapter1, dataTable1);
        return dataTable1;
      }
      catch (Exception ex)
      {
        DataTable dataTable2 = dataTable1;
        MySqlDataAdapter mySqlDataAdapter2 = mySqlDataAdapter1;
        string str = obj0;
        throw UnhandledException.\u0003(ex, (object) dataTable2, (object) mySqlDataAdapter2, (object) str);
      }
    }

    static \u0001()
    {
      \u0003.\u0003();
    }
  }
}
