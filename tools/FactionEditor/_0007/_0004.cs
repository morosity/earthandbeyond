﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: FactionEditor, Version=1.2.0.0, Culture=neutral, PublicKeyToken=2175665f88d36cc3
// MVID: 282C8E24-8545-40F1-BFC3-620198B9AA47
// Assembly location: D:\Server\eab\Tools\FactionEditor.exe

using \u0001;
using \u0007;
using SmartAssembly.SmartExceptionsCore;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace \u0007
{
  internal sealed class \u0004 : Form
  {
    [NonSerialized]
    internal static \u0002 \u0001;
    private IContainer \u0001;
    private TableLayoutPanel \u0001;
    private PictureBox \u0001;
    private Label \u0001;
    private Label \u0002;
    private Label \u0003;
    private Label \u0004;
    private TextBox \u0001;
    private Button \u0001;

    public \u0004()
    {
      try
      {
        this.\u0003();
        \u001B\u0005.\u007E\u0096\u000E((object) this, \u0019\u0006.\u0090\u0004(\u0004.\u0001(11659), (object) this.\u0003()));
        \u001B\u0005.\u007E\u0096\u000E((object) this.\u0001, this.\u0006());
        \u001B\u0005.\u007E\u0096\u000E((object) this.\u0002, \u0019\u0006.\u0090\u0004(\u0004.\u0001(11672), (object) this.\u0004()));
        \u001B\u0005.\u007E\u0096\u000E((object) this.\u0003, this.\u0007());
        \u001B\u0005.\u007E\u0096\u000E((object) this.\u0004, this.\u0008());
        \u001B\u0005.\u007E\u0096\u000E((object) this.\u0001, this.\u0005());
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex, (object) this);
      }
    }

    [SpecialName]
    public string \u0003()
    {
      AssemblyTitleAttribute assemblyTitleAttribute1;
      object[] objArray1;
      try
      {
        // ISSUE: type reference
        objArray1 = \u0012\u0003.\u007E\u001A\u0007((object) \u0005\u0005.\u007F\u0007(), \u0081\u0005.\u0008\u0006(__typeref (AssemblyTitleAttribute)), false);
        if (objArray1.Length > 0)
          goto label_3;
label_2:
        return \u0018\u0006.\u0089\u0008(\u001D\u0005.\u007E\u0013\u0007((object) \u0005\u0005.\u007F\u0007()));
label_3:
        assemblyTitleAttribute1 = (AssemblyTitleAttribute) objArray1[0];
        if (\u009B\u0002.\u0080\u0004(\u001D\u0005.\u007E\u008B\u0007((object) assemblyTitleAttribute1), \u0004.\u0001(10632)))
          return \u001D\u0005.\u007E\u008B\u0007((object) assemblyTitleAttribute1);
        goto label_2;
      }
      catch (Exception ex)
      {
        object[] objArray2 = objArray1;
        AssemblyTitleAttribute assemblyTitleAttribute2 = assemblyTitleAttribute1;
        throw UnhandledException.\u0003(ex, (object) objArray2, (object) assemblyTitleAttribute2, (object) this);
      }
    }

    [SpecialName]
    public string \u0004()
    {
      try
      {
        return \u001D\u0005.\u007E\u0011\u0004((object) \u000F\u0003.\u007E\u008E\u0007((object) \u0090\u0004.\u007E\u0014\u0007((object) \u0005\u0005.\u007F\u0007())));
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex, (object) this);
      }
    }

    [SpecialName]
    public string \u0005()
    {
      object[] objArray1;
      try
      {
        // ISSUE: type reference
        objArray1 = \u0012\u0003.\u007E\u001A\u0007((object) \u0005\u0005.\u007F\u0007(), \u0081\u0005.\u0008\u0006(__typeref (AssemblyDescriptionAttribute)), false);
        if (objArray1.Length == 0)
          return \u0004.\u0001(10632);
        return \u001D\u0005.\u007E\u0089\u0007((object) (AssemblyDescriptionAttribute) objArray1[0]);
      }
      catch (Exception ex)
      {
        object[] objArray2 = objArray1;
        throw UnhandledException.\u0003(ex, (object) objArray2, (object) this);
      }
    }

    [SpecialName]
    public string \u0006()
    {
      object[] objArray1;
      try
      {
        // ISSUE: type reference
        objArray1 = \u0012\u0003.\u007E\u001A\u0007((object) \u0005\u0005.\u007F\u0007(), \u0081\u0005.\u0008\u0006(__typeref (AssemblyProductAttribute)), false);
        if (objArray1.Length == 0)
          return \u0004.\u0001(10632);
        return \u001D\u0005.\u007E\u0084\u0007((object) (AssemblyProductAttribute) objArray1[0]);
      }
      catch (Exception ex)
      {
        object[] objArray2 = objArray1;
        throw UnhandledException.\u0003(ex, (object) objArray2, (object) this);
      }
    }

    [SpecialName]
    public string \u0007()
    {
      object[] objArray1;
      try
      {
        // ISSUE: type reference
        objArray1 = \u0012\u0003.\u007E\u001A\u0007((object) \u0005\u0005.\u007F\u0007(), \u0081\u0005.\u0008\u0006(__typeref (AssemblyCopyrightAttribute)), false);
        if (objArray1.Length == 0)
          return \u0004.\u0001(10632);
        return \u001D\u0005.\u007E\u0082\u0007((object) (AssemblyCopyrightAttribute) objArray1[0]);
      }
      catch (Exception ex)
      {
        object[] objArray2 = objArray1;
        throw UnhandledException.\u0003(ex, (object) objArray2, (object) this);
      }
    }

    [SpecialName]
    public string \u0008()
    {
      object[] objArray1;
      try
      {
        // ISSUE: type reference
        objArray1 = \u0012\u0003.\u007E\u001A\u0007((object) \u0005\u0005.\u007F\u0007(), \u0081\u0005.\u0008\u0006(__typeref (AssemblyCompanyAttribute)), false);
        if (objArray1.Length == 0)
          return \u0004.\u0001(10632);
        return \u001D\u0005.\u007E\u0087\u0007((object) (AssemblyCompanyAttribute) objArray1[0]);
      }
      catch (Exception ex)
      {
        object[] objArray2 = objArray1;
        throw UnhandledException.\u0003(ex, (object) objArray2, (object) this);
      }
    }

    protected override void Dispose([In] bool obj0)
    {
      try
      {
        if (obj0 && this.\u0001 != null)
          \u001B\u0006.\u007E\u001E\u0004((object) this.\u0001);
        \u0098\u0006.\u009D\u000F((object) this, obj0);
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<bool> local = (ValueType) obj0;
        throw UnhandledException.\u0003(ex, (object) this, (object) local);
      }
    }

    private void \u0003()
    {
      ComponentResourceManager componentResourceManager1;
      try
      {
        // ISSUE: type reference
        componentResourceManager1 = new ComponentResourceManager(\u0081\u0005.\u0008\u0006(__typeref (\u0004)));
        this.\u0001 = new TableLayoutPanel();
        this.\u0001 = new PictureBox();
        this.\u0001 = new Label();
        this.\u0002 = new Label();
        this.\u0003 = new Label();
        this.\u0004 = new Label();
        this.\u0001 = new TextBox();
        this.\u0001 = new Button();
        \u001B\u0006.\u007E\u0015\u000F((object) this.\u0001);
        \u001B\u0006.\u007E\u0018\u0011((object) this.\u0001);
        \u001B\u0006.\u0015\u000F((object) this);
        \u0017\u0004.\u007E\u001A\u0010((object) this.\u0001, 2);
        int num1 = \u0092\u0005.\u007E\u0081\u0010((object) \u008C\u0005.\u007E\u001D\u0010((object) this.\u0001), new ColumnStyle(SizeType.Percent, 33f));
        int num2 = \u0092\u0005.\u007E\u0081\u0010((object) \u008C\u0005.\u007E\u001D\u0010((object) this.\u0001), new ColumnStyle(SizeType.Percent, 67f));
        \u0083\u0003.\u007E\u001F\u0010((object) \u0083\u0002.\u007E\u0019\u0010((object) this.\u0001), (Control) this.\u0001, 0, 0);
        \u0083\u0003.\u007E\u001F\u0010((object) \u0083\u0002.\u007E\u0019\u0010((object) this.\u0001), (Control) this.\u0001, 1, 0);
        \u0083\u0003.\u007E\u001F\u0010((object) \u0083\u0002.\u007E\u0019\u0010((object) this.\u0001), (Control) this.\u0002, 1, 1);
        \u0083\u0003.\u007E\u001F\u0010((object) \u0083\u0002.\u007E\u0019\u0010((object) this.\u0001), (Control) this.\u0003, 1, 2);
        \u0083\u0003.\u007E\u001F\u0010((object) \u0083\u0002.\u007E\u0019\u0010((object) this.\u0001), (Control) this.\u0004, 1, 3);
        \u0083\u0003.\u007E\u001F\u0010((object) \u0083\u0002.\u007E\u0019\u0010((object) this.\u0001), (Control) this.\u0001, 1, 4);
        \u0083\u0003.\u007E\u001F\u0010((object) \u0083\u0002.\u007E\u0019\u0010((object) this.\u0001), (Control) this.\u0001, 1, 5);
        \u0011\u0007.\u007E\u0081\u000E((object) this.\u0001, DockStyle.Fill);
        \u000F\u0004.\u007E\u008C\u000E((object) this.\u0001, new Point(9, 9));
        \u001B\u0005.\u007E\u008F\u000E((object) this.\u0001, \u0004.\u0001(11689));
        \u0017\u0004.\u007E\u001B\u0010((object) this.\u0001, 6);
        int num3 = \u001C\u0006.\u007E\u0082\u0010((object) \u001D\u0006.\u007E\u001C\u0010((object) this.\u0001), new RowStyle(SizeType.Percent, 10f));
        int num4 = \u001C\u0006.\u007E\u0082\u0010((object) \u001D\u0006.\u007E\u001C\u0010((object) this.\u0001), new RowStyle(SizeType.Percent, 10f));
        int num5 = \u001C\u0006.\u007E\u0082\u0010((object) \u001D\u0006.\u007E\u001C\u0010((object) this.\u0001), new RowStyle(SizeType.Percent, 10f));
        int num6 = \u001C\u0006.\u007E\u0082\u0010((object) \u001D\u0006.\u007E\u001C\u0010((object) this.\u0001), new RowStyle(SizeType.Percent, 10f));
        int num7 = \u001C\u0006.\u007E\u0082\u0010((object) \u001D\u0006.\u007E\u001C\u0010((object) this.\u0001), new RowStyle(SizeType.Percent, 50f));
        int num8 = \u001C\u0006.\u007E\u0082\u0010((object) \u001D\u0006.\u007E\u001C\u0010((object) this.\u0001), new RowStyle(SizeType.Percent, 10f));
        \u0018\u0004.\u007E\u0092\u000E((object) this.\u0001, new Size(417, 265));
        \u0017\u0004.\u007E\u0093\u000E((object) this.\u0001, 0);
        \u0011\u0007.\u007E\u0081\u000E((object) this.\u0001, DockStyle.Fill);
        \u009C\u0005.\u007E\u0014\u0010((object) this.\u0001, (Image) \u008D\u0002.\u007E\u000E\u0008((object) componentResourceManager1, \u0004.\u0001(11714)));
        \u000F\u0004.\u007E\u008C\u000E((object) this.\u0001, new Point(3, 3));
        \u001B\u0005.\u007E\u008F\u000E((object) this.\u0001, \u0004.\u0001(11743));
        \u0002\u0007.\u007E\u001E\u0010((object) this.\u0001, (Control) this.\u0001, 6);
        \u0018\u0004.\u007E\u0092\u000E((object) this.\u0001, new Size(131, 259));
        \u0007\u0003.\u007E\u0015\u0010((object) this.\u0001, PictureBoxSizeMode.StretchImage);
        \u0017\u0004.\u007E\u0017\u0010((object) this.\u0001, 12);
        \u0098\u0006.\u007E\u0016\u0010((object) this.\u0001, false);
        \u0011\u0007.\u007E\u0081\u000E((object) this.\u0001, DockStyle.Fill);
        \u000F\u0004.\u007E\u008C\u000E((object) this.\u0001, new Point(143, 0));
        \u009C\u0002.\u007E\u008D\u000E((object) this.\u0001, new Padding(6, 0, 3, 0));
        \u0018\u0004.\u007E\u008E\u000E((object) this.\u0001, new Size(0, 17));
        \u001B\u0005.\u007E\u008F\u000E((object) this.\u0001, \u0004.\u0001(11764));
        \u0018\u0004.\u007E\u0092\u000E((object) this.\u0001, new Size(271, 17));
        \u0017\u0004.\u007E\u0093\u000E((object) this.\u0001, 19);
        \u001B\u0005.\u007E\u0096\u000E((object) this.\u0001, \u0004.\u0001(11789));
        \u0012\u0004.\u007E\u0005\u0010((object) this.\u0001, ContentAlignment.MiddleLeft);
        \u0011\u0007.\u007E\u0081\u000E((object) this.\u0002, DockStyle.Fill);
        \u000F\u0004.\u007E\u008C\u000E((object) this.\u0002, new Point(143, 26));
        \u009C\u0002.\u007E\u008D\u000E((object) this.\u0002, new Padding(6, 0, 3, 0));
        \u0018\u0004.\u007E\u008E\u000E((object) this.\u0002, new Size(0, 17));
        \u001B\u0005.\u007E\u008F\u000E((object) this.\u0002, \u0004.\u0001(11806));
        \u0018\u0004.\u007E\u0092\u000E((object) this.\u0002, new Size(271, 17));
        \u0017\u0004.\u007E\u0093\u000E((object) this.\u0002, 0);
        \u001B\u0005.\u007E\u0096\u000E((object) this.\u0002, \u0004.\u0001(11823));
        \u0012\u0004.\u007E\u0005\u0010((object) this.\u0002, ContentAlignment.MiddleLeft);
        \u0011\u0007.\u007E\u0081\u000E((object) this.\u0003, DockStyle.Fill);
        \u000F\u0004.\u007E\u008C\u000E((object) this.\u0003, new Point(143, 52));
        \u009C\u0002.\u007E\u008D\u000E((object) this.\u0003, new Padding(6, 0, 3, 0));
        \u0018\u0004.\u007E\u008E\u000E((object) this.\u0003, new Size(0, 17));
        \u001B\u0005.\u007E\u008F\u000E((object) this.\u0003, \u0004.\u0001(11836));
        \u0018\u0004.\u007E\u0092\u000E((object) this.\u0003, new Size(271, 17));
        \u0017\u0004.\u007E\u0093\u000E((object) this.\u0003, 21);
        \u001B\u0005.\u007E\u0096\u000E((object) this.\u0003, \u0004.\u0001(11857));
        \u0012\u0004.\u007E\u0005\u0010((object) this.\u0003, ContentAlignment.MiddleLeft);
        \u0011\u0007.\u007E\u0081\u000E((object) this.\u0004, DockStyle.Fill);
        \u000F\u0004.\u007E\u008C\u000E((object) this.\u0004, new Point(143, 78));
        \u009C\u0002.\u007E\u008D\u000E((object) this.\u0004, new Padding(6, 0, 3, 0));
        \u0018\u0004.\u007E\u008E\u000E((object) this.\u0004, new Size(0, 17));
        \u001B\u0005.\u007E\u008F\u000E((object) this.\u0004, \u0004.\u0001(11870));
        \u0018\u0004.\u007E\u0092\u000E((object) this.\u0004, new Size(271, 17));
        \u0017\u0004.\u007E\u0093\u000E((object) this.\u0004, 22);
        \u001B\u0005.\u007E\u0096\u000E((object) this.\u0004, \u0004.\u0001(11895));
        \u0012\u0004.\u007E\u0005\u0010((object) this.\u0004, ContentAlignment.MiddleLeft);
        \u0011\u0007.\u007E\u0081\u000E((object) this.\u0001, DockStyle.Fill);
        \u000F\u0004.\u007E\u008C\u000E((object) this.\u0001, new Point(143, 107));
        \u009C\u0002.\u007E\u008D\u000E((object) this.\u0001, new Padding(6, 3, 3, 3));
        \u0098\u0006.\u007E\u0084\u000F((object) this.\u0001, true);
        \u001B\u0005.\u007E\u008F\u000E((object) this.\u0001, \u0004.\u0001(11912));
        \u0098\u0006.\u007E\u0086\u000F((object) this.\u0001, true);
        \u0099\u0004.\u007E\u0089\u000F((object) this.\u0001, ScrollBars.Both);
        \u0018\u0004.\u007E\u0092\u000E((object) this.\u0001, new Size(271, 126));
        \u0017\u0004.\u007E\u0093\u000E((object) this.\u0001, 23);
        \u0098\u0006.\u007E\u0094\u000E((object) this.\u0001, false);
        \u001B\u0005.\u007E\u0096\u000E((object) this.\u0001, \u0004.\u0001(11937));
        \u009C\u0006.\u007E\u0018\u000E((object) this.\u0001, AnchorStyles.Bottom | AnchorStyles.Right);
        \u0012\u0007.\u007E\u001E\u000F((object) this.\u0001, DialogResult.Cancel);
        \u000F\u0004.\u007E\u008C\u000E((object) this.\u0001, new Point(339, 239));
        \u001B\u0005.\u007E\u008F\u000E((object) this.\u0001, \u0004.\u0001(11954));
        \u0018\u0004.\u007E\u0092\u000E((object) this.\u0001, new Size(75, 23));
        \u0017\u0004.\u007E\u0093\u000E((object) this.\u0001, 24);
        \u001B\u0005.\u007E\u0096\u000E((object) this.\u0001, \u0004.\u0001(7126));
        \u009A\u0004.\u008C\u000F((object) this, (IButtonControl) this.\u0001);
        \u0089\u0003.\u0019\u000F((object) this, new SizeF(6f, 13f));
        \u0010\u0005.\u001A\u000F((object) this, AutoScaleMode.Font);
        \u0018\u0004.\u0090\u000F((object) this, new Size(435, 283));
        \u0093\u0004.\u007E\u0016\u000F((object) \u009E\u0002.\u001F\u000E((object) this), (Control) this.\u0001);
        \u0014\u0005.\u008E\u000F((object) this, FormBorderStyle.FixedDialog);
        \u0098\u0006.\u0093\u000F((object) this, false);
        \u0098\u0006.\u0094\u000F((object) this, false);
        \u001B\u0005.\u008F\u000E((object) this, \u0004.\u0001(10881));
        \u009C\u0002.\u009C\u000E((object) this, new Padding(9));
        \u0098\u0006.\u0096\u000F((object) this, false);
        \u0098\u0006.\u0095\u000F((object) this, false);
        \u0015\u0007.\u0098\u000F((object) this, FormStartPosition.CenterParent);
        \u001B\u0005.\u007E\u0096\u000E((object) this, \u0004.\u0001(10881));
        \u0098\u0006.\u007E\u0010\u000F((object) this.\u0001, false);
        \u001B\u0006.\u007E\u000E\u000F((object) this.\u0001);
        \u001B\u0006.\u007E\u0019\u0011((object) this.\u0001);
        \u0098\u0006.\u0010\u000F((object) this, false);
      }
      catch (Exception ex)
      {
        ComponentResourceManager componentResourceManager2 = componentResourceManager1;
        throw UnhandledException.\u0003(ex, (object) componentResourceManager2, (object) this);
      }
    }

    static \u0004()
    {
      \u0003.\u0003();
    }
  }
}
