﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: FactionEditor, Version=1.2.0.0, Culture=neutral, PublicKeyToken=2175665f88d36cc3
// MVID: 282C8E24-8545-40F1-BFC3-620198B9AA47
// Assembly location: D:\Server\eab\Tools\FactionEditor.exe

using \u0001;
using \u0005;
using System;
using System.Drawing;
using System.Runtime.InteropServices;

namespace \u0005
{
  internal sealed class \u0003
  {
    [NonSerialized]
    internal static \u0002 \u0001;

    [DllImport("shell32", EntryPoint = "ExtractIconEx")]
    private static extern int \u0003([In] string obj0, [In] int obj1, [In] ref int obj2, [In] ref int obj3, [In] int obj4);

    [DllImport("user32", EntryPoint = "DrawText", CharSet = CharSet.Unicode)]
    private static extern int \u0003([In] IntPtr obj0, [In] string obj1, [In] int obj2, [In] ref \u0003.\u0001 obj3, [In] int obj4);

    [DllImport("gdi32.dll", EntryPoint = "SelectObject")]
    private static extern IntPtr \u0003([In] IntPtr obj0, [In] IntPtr obj1);

    [DllImport("kernel32.Dll", EntryPoint = "GetVersionEx")]
    private static extern short \u0003([In] ref \u0003.\u0002 obj0);

    public static Icon \u0003()
    {
      try
      {
        return \u0003.\u0004();
      }
      catch (Exception ex)
      {
        return \u0006.\u0003(\u0003.\u0001(6017));
      }
    }

    private static Icon \u0004()
    {
      int num1 = 0;
      int num2 = 0;
      if (\u0003.\u0003(\u0011\u0004.\u000F\u000E(), -1, ref num2, ref num2, 1) > 0)
      {
        \u0003.\u0003(\u0011\u0004.\u000F\u000E(), 0, ref num1, ref num2, 1);
        if (num1 != 0)
          return \u0007\u0005.\u009C\u0010(new IntPtr(num1));
      }
      return (Icon) null;
    }

    internal static string \u0003()
    {
      try
      {
        return \u0003.\u0004();
      }
      catch (Exception ex)
      {
        return string.Empty;
      }
    }

    private static string \u0004()
    {
      \u0003.\u0002 obj = new \u0003.\u0002();
      // ISSUE: type reference
      obj.\u0001 = \u0094\u0005.\u0016\u0008(\u0081\u0005.\u0008\u0006(__typeref (\u0003.\u0002)));
      int num = (int) \u0003.\u0003(ref obj);
      return obj.\u0001;
    }

    internal static int \u0003([In] Graphics obj0, [In] string obj1, [In] Font obj2, [In] int obj3)
    {
      try
      {
        return \u0003.\u0005(obj0, obj1, obj2, obj3);
      }
      catch (Exception ex1)
      {
        try
        {
          return \u0095\u0004.\u0087\u0005((double) \u0003.\u0004(obj0, obj1, obj2, obj3) * 1.1);
        }
        catch (Exception ex2)
        {
        }
      }
      return 0;
    }

    private static int \u0004([In] Graphics obj0, [In] string obj1, [In] Font obj2, [In] int obj3)
    {
      return \u0082\u0006.\u0007\u0011(\u001F\u0005.\u007E\u0094\u0010((object) obj0, obj1, obj2, obj3)).Height;
    }

    private static int \u0005([In] Graphics obj0, [In] string obj1, [In] Font obj2, [In] int obj3)
    {
      \u0003.\u0001 obj = new \u0003.\u0001(new Rectangle(0, 0, obj3, 10000));
      int num1 = 3088;
      IntPtr num2 = \u0090\u0002.\u007E\u0091\u0010((object) obj0);
      IntPtr num3 = \u0090\u0002.\u007E\u0012\u0011((object) obj2);
      IntPtr num4 = \u0003.\u0003(num2, num3);
      \u0003.\u0003(num2, obj1, -1, ref obj, num1);
      \u0003.\u0003(num2, num4);
      \u008A\u0003.\u007E\u0092\u0010((object) obj0, num2);
      return obj.\u0004 - obj.\u0002;
    }

    static \u0003()
    {
      \u0003.\u0003();
    }

    private struct \u0001
    {
      public int \u0001;
      public int \u0002;
      public int \u0003;
      public int \u0004;

      public \u0001([In] Rectangle obj0)
      {
        this.\u0001 = obj0.Left;
        this.\u0002 = obj0.Top;
        this.\u0004 = obj0.Bottom;
        this.\u0003 = obj0.Right;
      }
    }

    private struct \u0002
    {
      public int \u0001;
      public int \u0002;
      public int \u0003;
      public int \u0004;
      public int \u0005;
      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
      public string \u0001;
    }
  }
}
