﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: FactionEditor, Version=1.2.0.0, Culture=neutral, PublicKeyToken=2175665f88d36cc3
// MVID: 282C8E24-8545-40F1-BFC3-620198B9AA47
// Assembly location: D:\Server\eab\Tools\FactionEditor.exe

using \u0003;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace \u0005
{
  internal sealed class \u0002 : EventArgs
  {
    private bool \u0002 = true;
    private bool \u0003 = true;
    private \u0006 \u0001;
    private Exception \u0001;
    private bool \u0001;
    private bool \u0004;

    [SpecialName]
    public Exception \u0003()
    {
      return this.\u0001;
    }

    [SpecialName]
    public bool \u0003()
    {
      return this.\u0001;
    }

    [SpecialName]
    public bool \u0004()
    {
      return this.\u0002;
    }

    [SpecialName]
    public bool \u0005()
    {
      return this.\u0003;
    }

    internal void \u0003()
    {
      this.\u0001 = true;
    }

    internal void \u0004()
    {
      this.\u0002 = false;
    }

    internal void \u0005()
    {
      this.\u0003 = false;
    }

    [SpecialName]
    public bool \u0006()
    {
      return this.\u0004;
    }

    [SpecialName]
    public void \u0003([In] bool obj0)
    {
      this.\u0004 = obj0;
    }

    public void \u0006()
    {
      if (!this.\u0001)
        return;
      this.\u0001.\u0003();
    }

    public bool \u0007()
    {
      if (!this.\u0002)
        return false;
      return this.\u0001.\u0003();
    }

    internal \u0002([In] \u0006 obj0, [In] Exception obj1)
    {
      this.\u0001 = obj0;
      this.\u0001 = obj1;
    }
  }
}
