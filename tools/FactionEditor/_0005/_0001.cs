﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: FactionEditor, Version=1.2.0.0, Culture=neutral, PublicKeyToken=2175665f88d36cc3
// MVID: 282C8E24-8545-40F1-BFC3-620198B9AA47
// Assembly location: D:\Server\eab\Tools\FactionEditor.exe

using \u0001;
using \u0005;
using System;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace \u0005
{
  internal sealed class \u0001 : Control
  {
    private Label \u0001 = new Label();
    private Timer \u0001 = new Timer();
    private bool \u0002 = true;
    private string \u0001 = string.Empty;
    private float \u0001 = 1f;
    private float \u0002 = 1f;
    [NonSerialized]
    internal static \u0002 \u0001;
    private Image \u0001;
    private bool \u0001;

    [SpecialName]
    public override string get_Text()
    {
      return \u001D\u0005.\u0095\u000E((object) this);
    }

    [SpecialName]
    public override void set_Text([In] string obj0)
    {
      \u001B\u0005.\u0096\u000E((object) this, obj0);
      \u001B\u0006.\u007E\u000F\u000F((object) this);
    }

    public void \u0003()
    {
      \u0098\u0006.\u007E\u0086\u0010((object) this.\u0001, false);
      this.\u0001 = (Image) null;
      this.\u0001 = false;
      this.\u0001 = string.Empty;
      \u001B\u0006.\u007E\u000F\u000F((object) this);
      \u0017\u0004.\u0089\u000E((object) this, 16);
    }

    public void \u0004()
    {
      \u0098\u0006.\u007E\u0086\u0010((object) this.\u0001, true);
      this.\u0001 = (Image) \u0006.\u0003(\u0005.\u0001.\u0001(5960));
      this.\u0001 = true;
      \u001B\u0006.\u007E\u000F\u000F((object) this);
    }

    public void \u0005()
    {
      this.\u0003(string.Empty);
    }

    public void \u0003([In] string obj0)
    {
      this.\u0001 = obj0;
      \u0098\u0006.\u007E\u0086\u0010((object) this.\u0001, false);
      this.\u0001 = (Image) \u0006.\u0003(\u0080\u0006.\u007E\u0082\u0004((object) obj0) > 0 ? \u0005.\u0001.\u0001(5978) : \u0005.\u0001.\u0001(5973));
      this.\u0002 = true;
      this.\u0001 = true;
      if (\u0080\u0006.\u007E\u0082\u0004((object) obj0) > 0)
        \u0017\u0004.\u0089\u000E((object) this, 100);
      \u001B\u0006.\u007E\u000F\u000F((object) this);
    }

    protected override void OnResize([In] EventArgs obj0)
    {
      \u009D\u0004.\u007E\u0012\u000F((object) this.\u0001, \u0088\u0006.\u0086\u0005(22f * this.\u0001), \u0088\u0006.\u0086\u0005(this.\u0002), \u0080\u0006.\u009A\u000E((object) this) - \u0088\u0006.\u0086\u0005(22f * this.\u0001), \u0080\u0006.\u0088\u000E((object) this) - \u0088\u0006.\u0086\u0005(this.\u0002));
      \u0010\u0003.\u0008\u000F((object) this, obj0);
    }

    protected override void ScaleCore([In] float obj0, [In] float obj1)
    {
      this.\u0001 = obj0;
      this.\u0002 = obj1;
      \u0086\u0004.\u0011\u000F((object) this, obj0, obj1);
      \u0010\u0003.\u007E\u0008\u000F((object) this, EventArgs.Empty);
    }

    protected override void OnPaint([In] PaintEventArgs obj0)
    {
      \u0094\u0004.\u0007\u000F((object) this, obj0);
      if (\u0001\u0007.\u0015\u0011((object) this))
      {
        this.\u0001 = (Image) \u0006.\u0003(\u0005.\u0001.\u0001(5960));
        this.\u0001 = true;
      }
      if (this.\u0001 != null && this.\u0002)
        \u001E\u0004.\u007E\u0097\u0010((object) \u0008\u0004.\u007E\u0018\u000F((object) obj0), this.\u0001, new Rectangle(0, 0, \u0088\u0006.\u0086\u0005(16f * this.\u0001), \u0088\u0006.\u0086\u0005(16f * this.\u0002)), new Rectangle(0, 0, 16, 16), GraphicsUnit.Pixel);
      if (this.\u0001)
        \u001B\u0005.\u007E\u0096\u000E((object) this.\u0001, \u0080\u0006.\u007E\u0082\u0004((object) this.\u0001) > 0 ? \u0082\u0005.\u0095\u0004(\u001D\u0005.\u0095\u000E((object) this), \u0005.\u0001.\u0001(5987), this.\u0001, \u0005.\u0001.\u0001(5992)) : \u001D\u0005.\u0095\u000E((object) this));
      else
        \u001B\u0005.\u007E\u0096\u000E((object) this.\u0001, string.Empty);
    }

    public \u0001()
    {
      \u0017\u0004.\u007E\u0087\u0010((object) this.\u0001, 250);
      \u009A\u0005.\u007E\u0084\u0010((object) this.\u0001, new EventHandler(this.\u0003));
      \u0008\u0007.\u007E\u0004\u0010((object) this.\u0001, FlatStyle.System);
      \u0093\u0004.\u007E\u0016\u000F((object) \u009E\u0002.\u001F\u000E((object) this), (Control) this.\u0001);
      \u001A\u0005.\u0013\u000F((object) this, ControlStyles.UserPaint | ControlStyles.ResizeRedraw | ControlStyles.SupportsTransparentBackColor | ControlStyles.AllPaintingInWmPaint | ControlStyles.DoubleBuffer, true);
      \u0098\u0006.\u0094\u000E((object) this, false);
    }

    public \u0001([In] string obj0)
      : this()
    {
      \u001B\u0005.\u0096\u000E((object) this, \u0008\u0003.\u0093\u0004(\u0005.\u0001.\u0001(5997), obj0));
    }

    protected override void Dispose([In] bool obj0)
    {
      if (obj0 && this.\u0001 != null)
        \u001B\u0006.\u007E\u008B\u0010((object) this.\u0001);
      \u0098\u0006.\u0002\u000F((object) this, obj0);
    }

    private void \u0003([In] object obj0, [In] EventArgs obj1)
    {
      this.\u0002 = !this.\u0002;
      \u001B\u0006.\u007E\u000F\u000F((object) this);
    }

    static \u0001()
    {
      \u0003.\u0003();
    }
  }
}
