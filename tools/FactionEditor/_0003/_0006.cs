﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: FactionEditor, Version=1.2.0.0, Culture=neutral, PublicKeyToken=2175665f88d36cc3
// MVID: 282C8E24-8545-40F1-BFC3-620198B9AA47
// Assembly location: D:\Server\eab\Tools\FactionEditor.exe

using \u0002;
using \u0003;
using \u0004;
using Microsoft.Win32;
using SmartAssembly.SmartExceptionsCore;
using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Web.Services.Protocols;
using System.Xml;

namespace \u0003
{
  internal abstract class \u0006
  {
    private char[] \u0001 = new char[0];
    private ArrayList \u0001 = new ArrayList();
    private ArrayList \u0002 = new ArrayList();
    private Hashtable \u0003 = new Hashtable();
    private ArrayList \u0003 = new ArrayList();
    private Hashtable \u0004 = new Hashtable();
    [NonSerialized]
    internal static \u0001.\u0002 \u0001;
    private static string \u0001;
    internal static bool \u0001;
    private IWebProxy \u0001;
    private static \u0006 \u0001;
    private Exception \u0001;
    private Hashtable \u0001;
    private Hashtable \u0002;
    private XmlTextWriter \u0001;
    private EventHandler \u0001;
    private \u0004.\u0002 \u0001;

    [SpecialName]
    [MethodImpl(MethodImplOptions.Synchronized)]
    public void \u0003([In] EventHandler obj0)
    {
      this.\u0001 = (EventHandler) \u0010\u0004.\u0013\u0005((Delegate) this.\u0001, (Delegate) obj0);
    }

    [SpecialName]
    [MethodImpl(MethodImplOptions.Synchronized)]
    public void \u0003([In] \u0004.\u0002 obj0)
    {
      this.\u0001 = (\u0004.\u0002) \u0010\u0004.\u0013\u0005((Delegate) this.\u0001, (Delegate) obj0);
    }

    protected abstract void \u0003([In] \u0005.\u0002 obj0);

    protected abstract void \u0003([In] \u0004.\u0005 obj0);

    protected abstract void \u0003([In] \u0004.\u0004 obj0);

    [SecurityPermission(SecurityAction.Demand, UnmanagedCode = true)]
    public static void \u0003([In] \u0006 obj0)
    {
      if (obj0 == null)
        return;
      \u0006.\u0001 = obj0;
      \u0084\u0005.\u007E\u001E\u0005((object) \u0095\u0006.\u001C\u0005(), new UnhandledExceptionEventHandler(obj0.\u0003));
      \u001E\u0003.\u0012\u000E(new ThreadExceptionEventHandler(obj0.\u0003));
    }

    [SpecialName]
    public char[] \u0003()
    {
      string str;
      if (this.\u0001.Length == 0 && (str = \u001D\u0005.\u007E\u008D\u0004((object) \u0006.\u0001(3736))) != null)
      {
        if (!\u009B\u0002.\u007F\u0004(str, \u0006.\u0001(3749)))
        {
          if (\u009B\u0002.\u007F\u0004(str, \u0006.\u0001(3736)))
            this.\u0001 = new char[58]
            {
              '\x0001',
              '\x0002',
              '\x0003',
              '\x0004',
              '\x0005',
              '\x0006',
              '\a',
              '\b',
              '\x000E',
              '\x000F',
              '\x0010',
              '\x0011',
              '\x0012',
              '\x0013',
              '\x0014',
              '\x0015',
              '\x0016',
              '\x0017',
              '\x0018',
              '\x0019',
              '\x001A',
              '\x001B',
              '\x001C',
              '\x001D',
              '\x001E',
              '\x001F',
              '\x007F',
              '\x0080',
              '\x0081',
              '\x0082',
              '\x0083',
              '\x0084',
              '\x0086',
              '\x0087',
              '\x0088',
              '\x0089',
              '\x008A',
              '\x008B',
              '\x008C',
              '\x008D',
              '\x008E',
              '\x008F',
              '\x0090',
              '\x0091',
              '\x0092',
              '\x0093',
              '\x0094',
              '\x0095',
              '\x0096',
              '\x0097',
              '\x0098',
              '\x0099',
              '\x009A',
              '\x009B',
              '\x009C',
              '\x009D',
              '\x009E',
              '\x009F'
            };
        }
        else
          this.\u0001 = new char[62]
          {
            'a',
            'b',
            'c',
            'd',
            'e',
            'f',
            'g',
            'h',
            'i',
            'j',
            'k',
            'l',
            'm',
            'n',
            'o',
            'p',
            'q',
            'r',
            's',
            't',
            'u',
            'v',
            'w',
            'x',
            'y',
            'z',
            'A',
            'B',
            'C',
            'D',
            'E',
            'F',
            'G',
            'H',
            'I',
            'J',
            'K',
            'L',
            'M',
            'N',
            'O',
            'P',
            'Q',
            'R',
            'S',
            'T',
            'U',
            'V',
            'W',
            'X',
            'Y',
            'Z',
            '0',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9'
          };
      }
      return this.\u0001;
    }

    public static void \u0003([In] Exception obj0, [In] object[] obj1)
    {
      try
      {
        if (obj0 != null && obj0 is SecurityException && (\u009B\u0002.\u007F\u0004(\u0006.\u0001, \u0006.\u0001(3758)) && \u0006.\u0001.\u0003((SecurityException) obj0)))
          return;
        \u0006.\u0001.\u0003(UnhandledException.\u0003(obj0, obj1), false);
      }
      catch
      {
        \u009E\u0005.\u0014\u000E();
      }
    }

    public static Exception \u0003([In] Exception obj0, [In] object[] obj1)
    {
      \u0006.\u0004(obj0, obj1);
      return (Exception) new SoapException(\u001D\u0005.\u007E\u009F\u0004((object) obj0), SoapException.ServerFaultCode);
    }

    public static void \u0004([In] Exception obj0, [In] object[] obj1)
    {
      try
      {
        if (\u0006.\u0001 == null)
        {
          foreach (Type type in \u001D\u0004.\u007E\u0017\u0007((object) \u0005\u0005.\u007F\u0007()))
          {
            if (type != null && \u0005\u0006.\u007E\u0011\u0006((object) type) != null)
            {
              // ISSUE: type reference
              if (\u0005\u0006.\u007E\u0011\u0006((object) type) == \u0081\u0005.\u0008\u0006(__typeref (\u0006)))
              {
                try
                {
                  \u0006.\u0001 = (\u0006) \u0086\u0002.\u0015\u0005(type, true);
                  if (\u0006.\u0001 != null)
                    break;
                }
                catch
                {
                }
              }
            }
          }
        }
        if (\u0006.\u0001 == null)
          return;
        \u0006.\u0001.\u0003(UnhandledException.\u0003(obj0, obj1), true);
      }
      catch
      {
      }
    }

    private void \u0003([In] object obj0, [In] ThreadExceptionEventArgs obj1)
    {
      try
      {
        if (\u000F\u0006.\u007E\u001B\u0011((object) obj1) is SecurityException && (\u009B\u0002.\u007F\u0004(\u0006.\u0001, \u0006.\u0001(3758)) && this.\u0003(\u000F\u0006.\u007E\u001B\u0011((object) obj1) as SecurityException)))
          return;
        this.\u0003(\u000F\u0006.\u007E\u001B\u0011((object) obj1), true);
      }
      catch
      {
      }
    }

    private void \u0003([In] object obj0, [In] UnhandledExceptionEventArgs obj1)
    {
      try
      {
        if (\u0097\u0005.\u007E\u0087\u0006((object) obj1) is SecurityException && (\u009B\u0002.\u007F\u0004(\u0006.\u0001, \u0006.\u0001(3758)) && this.\u0003(\u0097\u0005.\u007E\u0087\u0006((object) obj1) as SecurityException)) || !(\u0097\u0005.\u007E\u0087\u0006((object) obj1) is Exception))
          return;
        this.\u0003((Exception) \u0097\u0005.\u007E\u0087\u0006((object) obj1), !\u0001\u0007.\u007E\u0088\u0006((object) obj1));
      }
      catch
      {
      }
    }

    private string \u0003([In] object obj0)
    {
      try
      {
        if (obj0 == null)
          return string.Empty;
        if (obj0 is int)
          return ((int) obj0).ToString(\u0006.\u0001(3763));
        if (obj0 is long)
          return ((long) obj0).ToString(\u0006.\u0001(3763));
        if (obj0 is short)
          return ((short) obj0).ToString(\u0006.\u0001(3763));
        if (obj0 is uint)
          return ((uint) obj0).ToString(\u0006.\u0001(3763));
        if (obj0 is ulong)
          return ((ulong) obj0).ToString(\u0006.\u0001(3763));
        if (obj0 is ushort)
          return ((ushort) obj0).ToString(\u0006.\u0001(3763));
        if (obj0 is byte)
          return ((byte) obj0).ToString(\u0006.\u0001(3763));
        if (obj0 is sbyte)
          return ((sbyte) obj0).ToString(\u0006.\u0001(3763));
        if (obj0 is IntPtr)
          return ((IntPtr) obj0).ToInt64().ToString(\u0006.\u0001(3763));
        if (obj0 is UIntPtr)
          return ((UIntPtr) obj0).ToUInt64().ToString(\u0006.\u0001(3763));
      }
      catch
      {
      }
      return string.Empty;
    }

    private string \u0003([In] string obj0)
    {
      if (\u0003\u0004.\u007E\u008B\u0004((object) obj0, \u0006.\u0001(3768)) && \u0003\u0004.\u007E\u0087\u0004((object) obj0, \u0006.\u0001(3789)))
        return \u0006.\u0001(3810);
      return obj0;
    }

    private void \u0003([In] object obj0, [In] FieldInfo obj1)
    {
      string str1 = obj1 == null ? (string) null : \u001D\u0005.\u007E\u0002\u0006((object) obj1);
      string str2 = obj1 == null ? \u0006.\u0001(3892) : \u0006.\u0001(3883);
      if (obj0 == null)
      {
        \u001B\u0005.\u007E\u0087\u0011((object) this.\u0001, str2);
        if (obj1 != null)
        {
          if (\u0001\u0007.\u007E\u0099\u0007((object) obj1))
            \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(3901), \u0006.\u0001(3758));
          Type type = \u0005\u0006.\u007E\u0096\u0007((object) obj1);
          if (type != null && \u0001\u0007.\u007E\u001E\u0006((object) type))
          {
            this.\u0003(\u0005\u0006.\u007E\u001F\u0006((object) type));
            if (\u0001\u0007.\u007E\u001B\u0006((object) type))
              \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(3910), \u0006.\u0001(3758));
            if (\u0001\u0007.\u007E\u001C\u0006((object) type))
              \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(3919), \u0006.\u0001(3758));
            if (\u0001\u0007.\u007E\u001A\u0006((object) type))
              \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(3932), \u0080\u0006.\u007E\u0010\u0006((object) type).ToString());
          }
          else
            this.\u0003(type);
        }
        if (str1 != null)
          this.\u0003(str1);
        \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(3941), \u0006.\u0001(3758));
        \u001B\u0006.\u007E\u0088\u0011((object) this.\u0001);
      }
      else
      {
        Type type = \u0005\u0006.\u007E\u0014\u0004(obj0);
        string str3 = (string) null;
        string str4 = (string) null;
        if (obj0 is string)
          str3 = \u0006.\u0001(3950);
        if (str3 == null)
        {
          if (\u0001\u0007.\u007E\u001D\u0006((object) type) || obj0 is IntPtr || obj0 is UIntPtr)
          {
            str3 = \u001D\u0005.\u007E\u000E\u0006((object) type);
            if (obj0 is char)
            {
              int num = (int) (char) obj0;
              StringBuilder stringBuilder1 = new StringBuilder();
              if (num >= 32)
              {
                StringBuilder stringBuilder2 = \u0006\u0004.\u007E\u009A\u0004((object) stringBuilder1, '\'');
                StringBuilder stringBuilder3 = \u0006\u0004.\u007E\u009A\u0004((object) stringBuilder1, (char) obj0);
                StringBuilder stringBuilder4 = \u0095\u0005.\u007E\u0099\u0004((object) stringBuilder1, \u0006.\u0001(3971));
              }
              StringBuilder stringBuilder5 = \u0095\u0005.\u007E\u0099\u0004((object) stringBuilder1, \u0006.\u0001(3976));
              StringBuilder stringBuilder6 = \u0095\u0005.\u007E\u0099\u0004((object) stringBuilder1, num.ToString(\u0006.\u0001(3763)));
              StringBuilder stringBuilder7 = \u0006\u0004.\u007E\u009A\u0004((object) stringBuilder1, ')');
              str4 = \u001D\u0005.\u007E\u0011\u0004((object) stringBuilder1);
            }
            if (obj0 is bool)
              str4 = \u001D\u0005.\u007E\u008C\u0004((object) \u001D\u0005.\u007E\u0011\u0004(obj0));
            if (str4 == null)
            {
              string str5 = string.Empty;
              try
              {
                str5 = this.\u0003(obj0);
              }
              catch
              {
              }
              if (\u0080\u0006.\u007E\u0082\u0004((object) str5) > 0)
              {
                StringBuilder stringBuilder1 = new StringBuilder();
                StringBuilder stringBuilder2 = \u0095\u0005.\u007E\u0099\u0004((object) stringBuilder1, \u001D\u0005.\u007E\u0011\u0004(obj0));
                StringBuilder stringBuilder3 = \u0095\u0005.\u007E\u0099\u0004((object) stringBuilder1, \u0006.\u0001(3981));
                StringBuilder stringBuilder4 = \u0095\u0005.\u007E\u0099\u0004((object) stringBuilder1, str5);
                StringBuilder stringBuilder5 = \u0006\u0004.\u007E\u009A\u0004((object) stringBuilder1, ')');
                str4 = \u001D\u0005.\u007E\u0011\u0004((object) stringBuilder1);
              }
              else
                str4 = \u001D\u0005.\u007E\u0011\u0004(obj0);
            }
          }
          else if (\u0001\u0007.\u007E\u0018\u0006((object) type) && \u0090\u0005.\u007E\u0006\u0006((object) type) != \u0090\u0005.\u007E\u0006\u0006((object) \u0005\u0006.\u0014\u0004((object) this)))
            str3 = \u001D\u0005.\u007E\u000E\u0006((object) type);
        }
        if (str3 != null)
        {
          \u001B\u0005.\u007E\u0087\u0011((object) this.\u0001, str2);
          if (obj1 != null && \u0001\u0007.\u007E\u0099\u0007((object) obj1))
            \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(3901), \u0006.\u0001(3758));
          this.\u0003(type);
          if (str1 != null)
            this.\u0003(str1);
          if (\u0001\u0007.\u007E\u0019\u0006((object) type))
            str4 = \u001D\u0005.\u007E\u0011\u0004(obj0);
          if (obj0 is Guid)
            str4 = \u008C\u0006.\u0094\u0004(\u0006.\u0001(3415), \u001D\u0005.\u007E\u0011\u0004(obj0), \u0006.\u0001(3990));
          if (str4 == null)
            str4 = \u008C\u0006.\u0094\u0004(\u0006.\u0001(3995), \u001D\u0005.\u007E\u0011\u0004(obj0), \u0006.\u0001(3995));
          \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4000), this.\u0003(str4));
          \u001B\u0006.\u007E\u0088\u0011((object) this.\u0001);
        }
        else
        {
          \u001B\u0005.\u007E\u0087\u0011((object) this.\u0001, str2);
          if (obj1 != null && \u0001\u0007.\u007E\u0099\u0007((object) obj1))
            \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(3901), \u0006.\u0001(3758));
          int num = -1;
          for (int index = 0; index < \u0080\u0006.\u007E\u0098\u0006((object) this.\u0001); ++index)
          {
            try
            {
              if (\u0011\u0003.\u007E\u0012\u0004(\u0013\u0006.\u007E\u0099\u0006((object) this.\u0001, index), obj0))
              {
                num = index;
                break;
              }
            }
            catch
            {
            }
          }
          if (num == -1)
            num = \u0002\u0005.\u007E\u009A\u0006((object) this.\u0001, obj0);
          \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4009), num.ToString());
          if (str1 != null)
            this.\u0003(str1);
          \u001B\u0006.\u007E\u0088\u0011((object) this.\u0001);
        }
      }
    }

    private void \u0003([In] string obj0)
    {
      int num = this.\u0003(obj0);
      if (num != -1)
        \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4014), num.ToString());
      else
        \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4023), obj0);
    }

    private \u0006.\u0003 \u0003([In] Type obj0)
    {
      \u0006.\u0003 obj = \u0006.\u0003.\u0003();
      if (obj0 != null && \u008A\u0002.\u007E\u0016\u0007((object) \u0008\u0005.\u007E\u0007\u0006((object) obj0), \u0006.\u0001(4032)) != null)
      {
        obj.\u0001 = ((\u0080\u0006.\u007E\u0004\u0006((object) obj0) & 16777215) - 1).ToString();
        Assembly assembly = \u0008\u0005.\u007E\u0007\u0006((object) obj0);
        obj.\u0001 = new \u0006.\u0002(\u001C\u0003.\u007E\u008F\u0007((object) \u0090\u0005.\u007E\u0019\u0007((object) assembly)).ToString(\u0006.\u0001(4093)), \u001D\u0005.\u007E\u0015\u0007((object) assembly));
      }
      return obj;
    }

    private int \u0003([In] \u0006.\u0003 obj0)
    {
      string str = \u001D\u0005.\u007E\u008D\u0004((object) obj0.\u0001.\u0001);
      if (\u0011\u0003.\u007E\u0002\u0007((object) this.\u0004, (object) str))
        return (int) \u0082\u0004.\u007E\u0003\u0007((object) this.\u0004, (object) str);
      int num = \u0002\u0005.\u007E\u009A\u0006((object) this.\u0003, (object) obj0.\u0001);
      \u0015\u0005.\u007E\u009F\u0006((object) this.\u0004, (object) str, (object) num);
      return num;
    }

    private void \u0003([In] Type obj0)
    {
      if (obj0 == null)
        return;
      try
      {
        \u0006.\u0003 obj = this.\u0003(obj0);
        if (!obj.\u0003())
        {
          \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4098), obj.\u0001);
          int num = this.\u0003(obj);
          if (num <= 0)
            return;
          \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4111), num.ToString());
        }
        else
        {
          string str1 = \u001D\u0005.\u007E\u000E\u0006((object) obj0);
          int num1;
          if (\u0011\u0003.\u007E\u0002\u0007((object) this.\u0003, (object) str1))
          {
            num1 = (int) \u0082\u0004.\u007E\u0003\u0007((object) this.\u0003, (object) str1);
          }
          else
          {
            StringBuilder stringBuilder1 = new StringBuilder();
            string str2 = \u001D\u0005.\u007E\u008D\u0007((object) \u0090\u0004.\u007E\u0014\u0007((object) \u0008\u0005.\u007E\u0007\u0006((object) obj0)));
            if (\u0080\u0006.\u007E\u0082\u0004((object) str2) > 0 && \u009B\u0002.\u0080\u0004(str2, \u0006.\u0001(4124)))
            {
              StringBuilder stringBuilder2 = \u0006\u0004.\u007E\u009A\u0004((object) stringBuilder1, '[');
              StringBuilder stringBuilder3 = \u0095\u0005.\u007E\u0099\u0004((object) stringBuilder1, str2);
              StringBuilder stringBuilder4 = \u0006\u0004.\u007E\u009A\u0004((object) stringBuilder1, ']');
            }
            string str3 = \u001D\u0005.\u007E\u000F\u0006((object) obj0);
            if (\u0080\u0006.\u007E\u0082\u0004((object) str3) > 0)
            {
              StringBuilder stringBuilder2 = \u0095\u0005.\u007E\u0099\u0004((object) stringBuilder1, str3);
              StringBuilder stringBuilder3 = \u0006\u0004.\u007E\u009A\u0004((object) stringBuilder1, '.');
            }
            if (\u0001\u0007.\u007E\u001E\u0006((object) obj0))
              obj0 = \u0005\u0006.\u007E\u001F\u0006((object) obj0);
            int num2 = \u0014\u0004.\u007E\u008A\u0004((object) str1, \u0006.\u0001(4137));
            if (num2 > 0)
            {
              string str4 = \u0089\u0002.\u007E\u008F\u0004((object) \u0002\u0003.\u007E\u0086\u0004((object) str1, \u0080\u0006.\u007E\u0082\u0004((object) str3) + 1, num2 - \u0080\u0006.\u007E\u0082\u0004((object) str3)), \u0006.\u0001(4137), \u0006.\u0001(4142));
              StringBuilder stringBuilder2 = \u0095\u0005.\u007E\u0099\u0004((object) stringBuilder1, str4);
            }
            StringBuilder stringBuilder5 = \u0095\u0005.\u007E\u0099\u0004((object) stringBuilder1, \u001D\u0005.\u007E\u0002\u0006((object) obj0));
            num1 = \u0002\u0005.\u007E\u009A\u0006((object) this.\u0002, (object) \u001D\u0005.\u007E\u0011\u0004((object) stringBuilder1));
            \u0015\u0005.\u007E\u009F\u0006((object) this.\u0003, (object) str1, (object) num1);
          }
          \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4147), num1.ToString());
        }
      }
      catch
      {
      }
    }

    private int \u0003([In] string obj0)
    {
      try
      {
        bool flag1 = this.\u0003()[0] == '\x0001';
        if (obj0 == null || \u0080\u0006.\u007E\u0082\u0004((object) obj0) == 0 || flag1 && \u0080\u0006.\u007E\u0082\u0004((object) obj0) > 4 || !flag1 && \u0003\u0003.\u007E\u0081\u0004((object) obj0, 0) != '#')
          return -1;
        int num = 0;
        for (int index1 = \u0080\u0006.\u007E\u0082\u0004((object) obj0) - 1; index1 >= 0 && (flag1 || index1 != 0); --index1)
        {
          char ch = \u0003\u0003.\u007E\u0081\u0004((object) obj0, index1);
          bool flag2 = false;
          for (int index2 = 0; index2 < this.\u0003().Length; ++index2)
          {
            if ((int) this.\u0003()[index2] == (int) ch)
            {
              num = num * this.\u0003().Length + index2;
              flag2 = true;
              break;
            }
          }
          if (!flag2)
            return -1;
        }
        return num;
      }
      catch
      {
        return -1;
      }
    }

    protected virtual Guid \u0003()
    {
      return Guid.Empty;
    }

    private string \u0003()
    {
      try
      {
        return \u0011\u0004.\u000F\u000E();
      }
      catch
      {
        return \u0006.\u0001(4160);
      }
    }

    private Assembly[] \u0003()
    {
      try
      {
        return \u008F\u0002.\u007E\u001D\u0005((object) \u0095\u0006.\u001C\u0005());
      }
      catch
      {
        return new Assembly[1]{ this.\u0003() };
      }
    }

    private Assembly \u0003()
    {
      try
      {
        return \u0005\u0005.\u007F\u0007();
      }
      catch
      {
        return (Assembly) null;
      }
    }

    private byte[] \u0003([In] string obj0)
    {
      MemoryStream memoryStream = new MemoryStream();
      this.\u0001 = new XmlTextWriter((Stream) memoryStream, (Encoding) new UTF8Encoding(false));
      \u001B\u0006.\u007E\u0084\u0011((object) this.\u0001);
      \u001B\u0005.\u007E\u0087\u0011((object) this.\u0001, \u0006.\u0001(4165));
      \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4198), \u001D\u0005.\u007E\u008D\u0004((object) \u0006.\u0001(4215)));
      \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4268), \u007F\u0006.\u000E\u0005().ToString(\u0006.\u0001(4281)));
      \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4286), this.\u0003());
      Guid guid = this.\u0003();
      if (\u0081\u0004.\u0092\u0005(guid, Guid.Empty))
        \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4295), guid.ToString(\u0006.\u0001(4093)));
      if (\u0080\u0006.\u007E\u0082\u0004((object) obj0) > 0)
        \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4304), obj0);
      if (\u0080\u0006.\u007E\u0098\u0006((object) this.\u0003) > 0)
        \u001B\u0006.\u007E\u009B\u0006((object) this.\u0003);
      int num1 = \u0002\u0005.\u007E\u009A\u0006((object) this.\u0003, (object) new \u0006.\u0002(\u0006.\u0001(4215), string.Empty));
      if (\u0080\u0006.\u007E\u0005\u0007((object) this.\u0004) > 0)
        \u001B\u0006.\u007E\u0001\u0007((object) this.\u0004);
      \u0015\u0005.\u007E\u009F\u0006((object) this.\u0004, (object) \u0006.\u0001(4215), (object) 0);
      \u001B\u0005.\u007E\u0087\u0011((object) this.\u0001, \u0006.\u0001(4317));
      Assembly assembly1 = this.\u0003();
      foreach (Assembly assembly2 in this.\u0003())
      {
        if (assembly2 != null)
        {
          \u001B\u0005.\u007E\u0087\u0011((object) this.\u0001, \u0006.\u0001(4111));
          try
          {
            \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4023), \u001D\u0005.\u007E\u0015\u0007((object) assembly2));
            \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4334), \u001D\u0005.\u007E\u0013\u0007((object) assembly2));
            if (assembly2 == assembly1)
              \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4347), \u0006.\u0001(3758));
          }
          catch
          {
          }
          \u001B\u0006.\u007E\u0088\u0011((object) this.\u0001);
        }
      }
      \u001B\u0006.\u007E\u0088\u0011((object) this.\u0001);
      \u001B\u0005.\u007E\u0087\u0011((object) this.\u0001, \u0006.\u0001(4356));
      if (this.\u0001 != null && \u0080\u0006.\u007E\u0005\u0007((object) this.\u0001) > 0)
      {
        IEnumerator enumerator = \u0003\u0005.\u007E\u0015\u0004((object) \u000E\u0004.\u007E\u0004\u0007((object) this.\u0001));
        try
        {
          while (\u0001\u0007.\u007E\u001C\u0004((object) enumerator))
          {
            string str1 = (string) \u0097\u0005.\u007E\u001D\u0004((object) enumerator);
            \u001B\u0005.\u007E\u0087\u0011((object) this.\u0001, \u0006.\u0001(4381));
            \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4023), str1);
            string str2 = (string) \u0082\u0004.\u007E\u0003\u0007((object) this.\u0001, (object) str1);
            if (str2 == null)
              \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(3941), \u0006.\u0001(3758));
            else
              \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4000), \u008C\u0006.\u0094\u0004(\u0006.\u0001(3995), str2, \u0006.\u0001(3995)));
            \u001B\u0006.\u007E\u0088\u0011((object) this.\u0001);
          }
        }
        finally
        {
          IDisposable disposable = enumerator as IDisposable;
          if (disposable != null)
            \u001B\u0006.\u007E\u001E\u0004((object) disposable);
        }
      }
      \u001B\u0006.\u007E\u0088\u0011((object) this.\u0001);
      if (this.\u0002 != null && \u0080\u0006.\u007E\u0005\u0007((object) this.\u0002) > 0)
      {
        \u001B\u0005.\u007E\u0087\u0011((object) this.\u0001, \u0006.\u0001(4402));
        IEnumerator enumerator = \u0003\u0005.\u007E\u0015\u0004((object) \u000E\u0004.\u007E\u0004\u0007((object) this.\u0002));
        try
        {
          while (\u0001\u0007.\u007E\u001C\u0004((object) enumerator))
          {
            string str = (string) \u0097\u0005.\u007E\u001D\u0004((object) enumerator);
            \u001B\u0005.\u007E\u0087\u0011((object) this.\u0001, \u0006.\u0001(4423));
            \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4440), str);
            \u0006.\u0001 obj = (\u0006.\u0001) \u0082\u0004.\u007E\u0003\u0007((object) this.\u0002, (object) str);
            \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4445), obj.\u0001);
            \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4458), obj.\u0001.ToString());
            if (\u0080\u0006.\u007E\u0082\u0004((object) obj.\u0003) > 0)
              \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4467), obj.\u0003);
            else
              \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4476), obj.\u0002);
            \u001B\u0006.\u007E\u0088\u0011((object) this.\u0001);
          }
        }
        finally
        {
          IDisposable disposable = enumerator as IDisposable;
          if (disposable != null)
            \u001B\u0006.\u007E\u001E\u0004((object) disposable);
        }
        \u001B\u0006.\u007E\u0088\u0011((object) this.\u0001);
      }
      \u001B\u0005.\u007E\u0087\u0011((object) this.\u0001, \u0006.\u0001(4485));
      try
      {
        \u0088\u0004.\u007E\u008D\u0011((object) this.\u0001, \u0006.\u0001(4510), \u001D\u0005.\u007E\u0011\u0004((object) \u000F\u0003.\u007E\u0001\u0006((object) \u0014\u0006.\u008E\u0005())));
        \u0088\u0004.\u007E\u008D\u0011((object) this.\u0001, \u0006.\u0001(4523), \u001D\u0005.\u007E\u0011\u0004((object) \u0096\u0005.\u007E\u009F\u0005((object) \u0014\u0006.\u008E\u0005())));
        \u0088\u0004.\u007E\u008D\u0011((object) this.\u0001, \u0006.\u0001(4540), \u0005.\u0003.\u0003());
      }
      catch
      {
      }
      \u001B\u0006.\u007E\u0088\u0011((object) this.\u0001);
      ArrayList arrayList = new ArrayList();
      for (Exception exception = this.\u0001; exception != null; exception = \u000F\u0006.\u007E\u0001\u0005((object) exception))
      {
        Type type = \u0005\u0006.\u007E\u0004\u0005((object) exception);
        if (\u009B\u0002.\u007F\u0004(\u001D\u0005.\u007E\u0002\u0006((object) type), \u0006.\u0001(4557)) && \u009B\u0002.\u007F\u0004(\u001D\u0005.\u007E\u000F\u0006((object) type), \u0006.\u0001(4582)))
        {
          int num2 = \u0002\u0005.\u007E\u009A\u0006((object) arrayList, (object) exception);
        }
        else
          \u0096\u0002.\u007E\u009D\u0006((object) arrayList, 0, (object) exception);
      }
      \u001B\u0005.\u007E\u0087\u0011((object) this.\u0001, \u0006.\u0001(4627));
      int num3 = \u0080\u0006.\u007E\u0098\u0006((object) arrayList);
      int num4 = 0;
      IEnumerator enumerator1 = \u0003\u0005.\u007E\u009C\u0006((object) arrayList);
      try
      {
        while (\u0001\u0007.\u007E\u001C\u0004((object) enumerator1))
        {
          Exception exception = (Exception) \u0097\u0005.\u007E\u001D\u0004((object) enumerator1);
          ++num4;
          if (num4 > 100 && num4 == num3 - 100)
          {
            \u001B\u0005.\u007E\u0087\u0011((object) this.\u0001, \u0006.\u0001(4644));
            \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4665), num3.ToString());
            \u001B\u0006.\u007E\u0088\u0011((object) this.\u0001);
          }
          else if (num4 <= 100 || num4 > num3 - 100)
          {
            Type type = \u0005\u0006.\u007E\u0004\u0005((object) exception);
            if (\u009B\u0002.\u007F\u0004(\u001D\u0005.\u007E\u0002\u0006((object) type), \u0006.\u0001(4557)) && \u009B\u0002.\u007F\u0004(\u001D\u0005.\u007E\u000F\u0006((object) type), \u0006.\u0001(4582)))
            {
              int num2 = 0;
              int num5 = -1;
              object[] objArray = (object[]) null;
              \u0006.\u0003 obj1 = \u0006.\u0003.\u0003();
              bool flag = true;
              try
              {
                num2 = (int) \u0082\u0004.\u007E\u0097\u0007((object) \u0005\u0007.\u007E\u0015\u0006((object) type, \u0006.\u0001(4690)), (object) exception);
                num5 = (int) \u0082\u0004.\u007E\u0097\u0007((object) \u0005\u0007.\u007E\u0015\u0006((object) type, \u0006.\u0001(4703)), (object) exception);
                objArray = (object[]) \u0082\u0004.\u007E\u0097\u0007((object) \u0005\u0007.\u007E\u0015\u0006((object) type, \u0006.\u0001(4716)), (object) exception);
                obj1 = this.\u0003(type);
              }
              catch
              {
                flag = false;
              }
              if (flag && !obj1.\u0003())
              {
                \u001B\u0005.\u007E\u0087\u0011((object) this.\u0001, \u0006.\u0001(4729));
                \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4690), num2.ToString());
                int num6 = this.\u0003(obj1);
                if (num6 > 0)
                  \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4111), num6.ToString());
                if (num5 != -1)
                  \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4703), num5.ToString());
                foreach (object obj2 in objArray)
                {
                  try
                  {
                    this.\u0003(obj2, (FieldInfo) null);
                  }
                  catch
                  {
                  }
                }
                \u001B\u0006.\u007E\u0088\u0011((object) this.\u0001);
              }
            }
            else
            {
              \u001B\u0005.\u007E\u0087\u0011((object) this.\u0001, \u0006.\u0001(4746));
              try
              {
                this.\u0003(\u0005\u0006.\u007E\u0004\u0005((object) exception));
                string str1 = \u0006.\u0001(4160);
                try
                {
                  str1 = \u001D\u0005.\u007E\u009F\u0004((object) exception);
                }
                catch
                {
                }
                \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4759), str1);
                string str2 = \u001D\u0005.\u007E\u008E\u0004((object) \u001D\u0005.\u007E\u0002\u0005((object) exception));
                \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4772), str2);
                int num2 = \u008E\u0005.\u007E\u0088\u0004((object) str2, ' ');
                string str3 = \u008B\u0002.\u007E\u0084\u0004((object) str2, num2 + 1);
                int num5 = \u0014\u0004.\u007E\u0089\u0004((object) str3, \u0006.\u0001(4801));
                if (num5 != -1)
                  str3 = \u0002\u0003.\u007E\u0086\u0004((object) str3, 0, num5);
                \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4806), str3);
              }
              catch
              {
              }
              \u001B\u0006.\u007E\u0088\u0011((object) this.\u0001);
            }
          }
        }
      }
      finally
      {
        IDisposable disposable = enumerator1 as IDisposable;
        if (disposable != null)
          \u001B\u0006.\u007E\u001E\u0004((object) disposable);
      }
      \u001B\u0006.\u007E\u0088\u0011((object) this.\u0001);
      \u001B\u0005.\u007E\u0087\u0011((object) this.\u0001, \u0006.\u0001(4716));
      int num7 = \u0080\u0006.\u007E\u0098\u0006((object) this.\u0001);
      for (int index1 = 0; index1 < \u0080\u0006.\u007E\u0098\u0006((object) this.\u0001); ++index1)
      {
        object obj1 = \u0013\u0006.\u007E\u0099\u0006((object) this.\u0001, index1);
        Type type = \u0005\u0006.\u007E\u0014\u0004(obj1);
        \u001B\u0005.\u007E\u0087\u0011((object) this.\u0001, \u0006.\u0001(4815));
        \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4009), index1.ToString());
        string str = (string) null;
        try
        {
          str = \u001D\u0005.\u007E\u0011\u0004(obj1);
          str = !\u009B\u0002.\u007F\u0004(str, \u001D\u0005.\u007E\u000E\u0006((object) type)) ? (!\u0001\u0007.\u007E\u0019\u0006((object) type) ? (!(obj1 is Guid) ? \u008C\u0006.\u0094\u0004(\u0006.\u0001(3995), str, \u0006.\u0001(3995)) : \u008C\u0006.\u0094\u0004(\u0006.\u0001(3415), str, \u0006.\u0001(3990))) : \u0084\u0004.\u001F\u0004(type, obj1, \u0006.\u0001(4828))) : (string) null;
        }
        catch
        {
        }
        if (str != null)
          \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4000), this.\u0003(str));
        if (\u0001\u0007.\u007E\u001E\u0006((object) type))
        {
          this.\u0003(\u0005\u0006.\u007E\u001F\u0006((object) type));
          if (\u0001\u0007.\u007E\u001B\u0006((object) type))
            \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(3910), \u0006.\u0001(3758));
          if (\u0001\u0007.\u007E\u001C\u0006((object) type))
            \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(3919), \u0006.\u0001(3758));
          if (\u0001\u0007.\u007E\u001A\u0006((object) type))
          {
            Array array = (Array) obj1;
            \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(3932), \u0080\u0006.\u007E\u001B\u0004((object) array).ToString());
            StringBuilder stringBuilder1 = new StringBuilder();
            for (int index2 = 0; index2 < \u0080\u0006.\u007E\u001B\u0004((object) array); ++index2)
            {
              if (index2 > 0)
              {
                StringBuilder stringBuilder2 = \u0006\u0004.\u007E\u009A\u0004((object) stringBuilder1, ',');
              }
              StringBuilder stringBuilder3 = \u0095\u0002.\u007E\u009B\u0004((object) stringBuilder1, \u0096\u0006.\u007E\u001A\u0004((object) array, index2));
            }
            \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4458), \u001D\u0005.\u007E\u0011\u0004((object) stringBuilder1));
            if (\u0080\u0006.\u007E\u001B\u0004((object) array) == 1)
            {
              int num2 = \u0080\u0006.\u007E\u0019\u0004((object) array);
              for (int index2 = 0; index2 < num2; ++index2)
              {
                if (index2 == 10)
                {
                  if (num2 > 16)
                    index2 = num2 - 5;
                }
                try
                {
                  this.\u0003(\u0013\u0006.\u007E\u0018\u0004((object) array, index2), (FieldInfo) null);
                }
                catch
                {
                }
              }
            }
          }
        }
        else
        {
          this.\u0003(type);
          if (index1 < num7)
          {
            if (obj1 is IEnumerable)
            {
              \u001B\u0005.\u007E\u0087\u0011((object) this.\u0001, \u0006.\u0001(4833));
              try
              {
                int num2 = 0;
                IEnumerator enumerator2 = \u0003\u0005.\u007E\u0015\u0004((object) (IEnumerable) obj1);
                try
                {
                  while (\u0001\u0007.\u007E\u001C\u0004((object) enumerator2))
                  {
                    object obj2 = \u0097\u0005.\u007E\u001D\u0004((object) enumerator2);
                    if (num2 > 20)
                    {
                      \u0088\u0004.\u007E\u008D\u0011((object) this.\u0001, \u0006.\u0001(4850), string.Empty);
                      break;
                    }
                    this.\u0003(obj2, (FieldInfo) null);
                    ++num2;
                  }
                }
                finally
                {
                  IDisposable disposable = enumerator2 as IDisposable;
                  if (disposable != null)
                    \u001B\u0006.\u007E\u001E\u0004((object) disposable);
                }
              }
              catch
              {
              }
              \u001B\u0006.\u007E\u0088\u0011((object) this.\u0001);
            }
            bool flag = true;
            while (type != null)
            {
              if (!flag)
              {
                \u001B\u0005.\u007E\u0087\u0011((object) this.\u0001, \u0006.\u0001(4859));
                this.\u0003(type);
                \u001B\u0006.\u007E\u0088\u0011((object) this.\u0001);
              }
              FieldInfo[] fieldInfoArray = \u0092\u0004.\u007E\u0016\u0006((object) type, BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
              if (fieldInfoArray.Length > 0)
              {
                for (int index2 = 0; index2 < fieldInfoArray.Length; ++index2)
                {
                  try
                  {
                    if (!\u0001\u0007.\u007E\u009B\u0007((object) fieldInfoArray[index2]))
                    {
                      if (\u0001\u0007.\u007E\u0099\u0007((object) fieldInfoArray[index2]))
                      {
                        if (\u0001\u0007.\u007E\u009A\u0007((object) fieldInfoArray[index2]))
                          continue;
                      }
                      this.\u0003(\u0082\u0004.\u007E\u0097\u0007((object) fieldInfoArray[index2], obj1), fieldInfoArray[index2]);
                    }
                  }
                  catch
                  {
                  }
                }
              }
              type = \u0005\u0006.\u007E\u0011\u0006((object) type);
              flag = false;
              if (this.\u0003(type).\u0003())
                break;
            }
          }
        }
        \u001B\u0006.\u007E\u0088\u0011((object) this.\u0001);
      }
      \u001B\u0006.\u007E\u0088\u0011((object) this.\u0001);
      \u001B\u0005.\u007E\u0087\u0011((object) this.\u0001, \u0006.\u0001(4876));
      \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4889), \u0080\u0006.\u007E\u0098\u0006((object) this.\u0002).ToString());
      for (int index = 0; index < \u0080\u0006.\u007E\u0098\u0006((object) this.\u0002); ++index)
      {
        string empty = string.Empty;
        string str;
        try
        {
          str = \u001D\u0005.\u007E\u0011\u0004(\u0013\u0006.\u007E\u0099\u0006((object) this.\u0002, index));
        }
        catch (Exception ex)
        {
          str = \u0093\u0005.\u0092\u0004((object) '"', (object) \u001D\u0005.\u007E\u009F\u0004((object) ex), (object) '"');
        }
        \u0088\u0004.\u007E\u008D\u0011((object) this.\u0001, \u0006.\u0001(4147), str);
      }
      \u001B\u0006.\u007E\u0088\u0011((object) this.\u0001);
      \u001B\u0005.\u007E\u0087\u0011((object) this.\u0001, \u0006.\u0001(4898));
      \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4889), \u0080\u0006.\u007E\u0098\u0006((object) this.\u0003).ToString());
      for (int index = 0; index < \u0080\u0006.\u007E\u0098\u0006((object) this.\u0003); ++index)
      {
        \u001B\u0005.\u007E\u0087\u0011((object) this.\u0001, \u0006.\u0001(4198));
        \u0006.\u0002 obj = (\u0006.\u0002) \u0013\u0006.\u007E\u0099\u0006((object) this.\u0003, index);
        \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4009), obj.\u0001);
        if (\u0080\u0006.\u007E\u0082\u0004((object) obj.\u0002) > 0)
          \u0088\u0004.\u007E\u0089\u0011((object) this.\u0001, \u0006.\u0001(4915), obj.\u0002);
        \u001B\u0006.\u007E\u0088\u0011((object) this.\u0001);
      }
      \u001B\u0006.\u007E\u0088\u0011((object) this.\u0001);
      \u001B\u0006.\u007E\u0088\u0011((object) this.\u0001);
      \u001B\u0006.\u007E\u0086\u0011((object) this.\u0001);
      \u001B\u0006.\u007E\u008C\u0011((object) this.\u0001);
      \u001B\u0006.\u007E\u001B\u0008((object) memoryStream);
      return \u0087\u0002.\u007E\u0088\u0008((object) memoryStream);
    }

    internal bool \u0003([In] string obj0)
    {
      try
      {
        byte[] numArray1 = this.\u0003(\u0093\u0003.\u0093\u0005().ToString(\u0006.\u0001(4093)));
        FileStream fileStream = \u001C\u0004.\u0084\u0008(obj0);
        byte[] numArray2 = \u0003.\u0005.\u0003(\u0006.\u0004(numArray1), \u0006.\u0001(4928));
        byte[] numArray3 = \u0003\u0006.\u007E\u0003\u0008((object) \u0016\u0007.\u0002\u0008(), \u0006.\u0001(4215));
        \u0099\u0006.\u007E\u001F\u0008((object) fileStream, numArray3, 0, numArray3.Length);
        \u0099\u0006.\u007E\u001F\u0008((object) fileStream, numArray2, 0, numArray2.Length);
        \u001B\u0006.\u007E\u001A\u0008((object) fileStream);
        return true;
      }
      catch (ThreadAbortException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        return false;
      }
    }

    internal void \u0003()
    {
      try
      {
        string str1 = \u0011\u0004.\u008B\u0008();
        this.\u0003(str1);
        string str2 = this.\u0004(\u0006.\u0001(4286));
        if (\u0080\u0006.\u007E\u0082\u0004((object) str2) > 0 && !\u0003\u0004.\u007E\u0087\u0004((object) str2, \u0006.\u0001(5254)))
          str2 = \u0008\u0003.\u0093\u0004(str2, \u0006.\u0001(5254));
        Process process = \u0007\u0007.\u0082\u0011(\u0008\u0003.\u0093\u0004(str2, \u0006.\u0001(5259)), \u008C\u0006.\u0094\u0004(\u0006.\u0001(5288), str1, \u0006.\u0001(3995)));
        if (this.\u0001 == null)
          return;
        \u0006\u0006.\u007E\u0090\u0005((object) this.\u0001, (object) this, EventArgs.Empty);
      }
      catch (ThreadAbortException ex)
      {
      }
      catch (Exception ex)
      {
        this.\u0003(new \u0004.\u0005(ex));
      }
    }

    internal bool \u0003()
    {
      try
      {
        if (this.\u0001 != null)
          goto label_35;
label_1:
        byte[] numArray1;
        try
        {
          numArray1 = this.\u0003(string.Empty);
        }
        catch (Exception ex)
        {
          int num = -1;
          try
          {
            StackTrace stackTrace = new StackTrace(ex);
            if (\u0080\u0006.\u007E\u000F\u0007((object) stackTrace) > 0)
            {
              StackFrame stackFrame = \u0099\u0005.\u007E\u0010\u0007((object) stackTrace, \u0080\u0006.\u007E\u000F\u0007((object) stackTrace) - 1);
              num = \u0080\u0006.\u007E\u0012\u0007((object) stackFrame);
            }
          }
          catch
          {
          }
          if (this.\u0001 != null)
            this.\u0001((object) this, new \u0004.\u0003(\u0003.\u0002.\u0001, \u008B\u0006.\u0091\u0004(\u0006.\u0001(5317), (object) \u001D\u0005.\u007E\u009F\u0004((object) ex), (object) num)));
          return false;
        }
        byte[] numArray2 = \u0006.\u0004(numArray1);
        if (numArray2 == null)
        {
          if (this.\u0001 != null)
            this.\u0001((object) this, new \u0004.\u0003(\u0003.\u0002.\u0001, \u0006.\u0001));
          return false;
        }
        byte[] numArray3 = \u0003.\u0005.\u0003(numArray2, \u0006.\u0001(4928));
        if (numArray3 == null)
        {
          if (this.\u0001 != null)
            this.\u0001((object) this, new \u0004.\u0003(\u0003.\u0002.\u0001, \u0003.\u0005.\u0001));
          return false;
        }
        if (this.\u0001 != null)
          this.\u0001((object) this, new \u0004.\u0003(\u0003.\u0002.\u0002));
        \u0006 obj = new \u0006(\u0006.\u0001(5350));
        if (this.\u0001 != null)
          obj.\u0003(this.\u0001);
        string str1 = obj.\u0003();
        if (\u009B\u0002.\u007F\u0004(str1, \u0006.\u0001(5403)))
        {
          if (this.\u0001 != null)
            this.\u0001((object) this, new \u0004.\u0003(\u0003.\u0002.\u0003));
          byte[] numArray4 = \u0003\u0006.\u007E\u0003\u0008((object) \u0016\u0007.\u0002\u0008(), \u0006.\u0001(4215));
          byte[] numArray5 = new byte[numArray4.Length + numArray3.Length];
          \u0086\u0006.\u0016\u0004((Array) numArray4, (Array) numArray5, numArray4.Length);
          \u0016\u0004.\u0017\u0004((Array) numArray3, 0, (Array) numArray5, numArray4.Length, numArray3.Length);
          string str2 = obj.\u0003(numArray5);
          if (\u0003\u0004.\u007E\u008B\u0004((object) str2, \u0006.\u0001(5408)))
          {
            if (this.\u0001 != null)
              this.\u0001((object) this, new \u0004.\u0003(\u0003.\u0002.\u0003, str2));
            return false;
          }
          if (this.\u0001 != null)
          {
            \u0004.\u0003 e = new \u0004.\u0003(\u0003.\u0002.\u0004);
            e.\u0003(str2);
            this.\u0001((object) this, e);
          }
          return true;
        }
        string str3 = str1;
        if (this.\u0001 != null)
          this.\u0001((object) this, new \u0004.\u0003(\u0003.\u0002.\u0002, str3));
        return false;
label_35:
        this.\u0001((object) this, new \u0004.\u0003(\u0003.\u0002.\u0001));
        goto label_1;
      }
      catch (ThreadAbortException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.\u0003(new \u0004.\u0005(ex));
        return false;
      }
    }

    private string \u0004([In] string obj0)
    {
      try
      {
        RegistryKey registryKey = \u009E\u0006.\u007E\u0011\u0008((object) Registry.LocalMachine, \u0006.\u0001(5413)) ?? \u009E\u0006.\u007E\u0011\u0008((object) Registry.LocalMachine, \u0006.\u0001(5446));
        if (registryKey == null)
          return string.Empty;
        string str = (string) \u009B\u0004.\u007E\u0012\u0008((object) registryKey, obj0, (object) string.Empty);
        \u001B\u0006.\u007E\u0010\u0008((object) registryKey);
        return str;
      }
      catch
      {
        return string.Empty;
      }
    }

    internal bool \u0003([In] SecurityException obj0)
    {
      \u0004.\u0004 obj = new \u0004.\u0004(obj0);
      this.\u0003(obj);
      if (obj.\u0005())
        return false;
      if (!obj.\u0004())
        \u009E\u0005.\u0014\u000E();
      return true;
    }

    internal string \u0004()
    {
      string str1 = Guid.Empty.ToString(\u0006.\u0001(4093));
      try
      {
        string str2 = this.\u0004(\u0006.\u0001(4286));
        if (\u0080\u0006.\u007E\u0082\u0004((object) str2) > 0 && !\u0003\u0004.\u007E\u0087\u0004((object) str2, \u0006.\u0001(5254)))
          str2 = \u0008\u0003.\u0093\u0004(str2, \u0006.\u0001(5254));
        string url = \u0008\u0003.\u0093\u0004(str2, \u0006.\u0001(5495));
        if (\u0010\u0006.\u0083\u0008(url))
        {
          XmlTextReader xmlTextReader = new XmlTextReader(url);
          if (\u0001\u0007.\u007E\u008F\u0011((object) xmlTextReader))
            str1 = \u0092\u0003.\u007E\u008E\u0011((object) xmlTextReader, \u0006.\u0001(5532));
          \u001B\u0006.\u007E\u0090\u0011((object) xmlTextReader);
        }
      }
      catch
      {
      }
      return str1;
    }

    internal void \u0003([In] Exception obj0, [In] bool obj1)
    {
      if (obj0 == null || obj0 is ThreadAbortException)
        return;
      \u0006.\u0001 = true;
      bool flag = true;
      try
      {
        this.\u0001 = obj0;
        this.\u0001 = (Hashtable) null;
        this.\u0002 = (Hashtable) null;
        \u0005.\u0002 obj2 = new \u0005.\u0002(this, obj0);
        if (\u009B\u0002.\u007F\u0004(\u001D\u0005.\u007E\u008C\u0004((object) \u0006.\u0001(5350)), \u001D\u0005.\u007E\u008C\u0004((object) this.\u0004())))
          obj2.\u0003();
        \u009B\u0002 obj3 = \u009B\u0002.\u007F\u0004;
        string str1 = \u001D\u0005.\u007E\u008C\u0004((object) \u0006.\u0001(5350));
        Guid empty = Guid.Empty;
        string str2 = \u001D\u0005.\u007E\u008C\u0004((object) empty.ToString(\u0006.\u0001(4093)));
        if (obj3(str1, str2))
          obj2.\u0004();
        if (!obj1)
          obj2.\u0005();
        this.\u0003(obj2);
        flag = !obj2.\u0006();
      }
      catch (ThreadAbortException ex)
      {
      }
      catch (Exception ex)
      {
        this.\u0003(new \u0004.\u0005(ex));
      }
      this.\u0001 = (Exception) null;
      this.\u0001 = (Hashtable) null;
      this.\u0002 = (Hashtable) null;
      \u0006.\u0001 = false;
      if (!flag)
        return;
      foreach (Assembly assembly in \u008F\u0002.\u007E\u001D\u0005((object) \u0095\u0006.\u001C\u0005()))
      {
        try
        {
          string str = \u001D\u0005.\u007E\u0015\u0007((object) assembly);
          if (\u0003\u0004.\u007E\u0087\u0004((object) str, \u0006.\u0001(5545)))
          {
            if (\u0003\u0004.\u007E\u008B\u0004((object) str, \u0006.\u0001(5570)))
            {
              object obj2 = \u0099\u0003.\u007E\u0093\u0007((object) \u008C\u0003.\u007E\u009C\u0007((object) \u0019\u0004.\u007E\u0017\u0006((object) \u008A\u0002.\u007E\u0016\u0007((object) assembly, \u0006.\u0001(5603)), \u0006.\u0001(5640))), (object) null, (object[]) null);
              object obj3 = \u0099\u0003.\u007E\u0093\u0007((object) \u009B\u0005.\u007E\u0012\u0006((object) \u0005\u0006.\u007E\u0014\u0004(obj2), \u0006.\u0001(5653), new Type[0]), obj2, (object[]) null);
            }
          }
        }
        catch
        {
        }
      }
      \u009E\u0005.\u0014\u000E();
      try
      {
        \u009D\u0006.\u008D\u0005(0);
      }
      catch
      {
      }
    }

    static \u0006()
    {
      \u0001.\u0003.\u0003();
      \u0006.\u0001 = \u0006.\u0001(3758);
      \u0006.\u0001 = false;
      \u0006.\u0001 = (\u0006) null;
    }

    private struct \u0001
    {
      public string \u0001;
      public string \u0002;
      public string \u0003;
      public int \u0001;
    }

    private struct \u0002
    {
      public string \u0001;
      public string \u0002;

      public \u0002([In] string obj0, [In] string obj1)
      {
        this.\u0001 = obj0;
        this.\u0002 = obj1;
      }
    }

    private struct \u0003
    {
      public string \u0001;
      public \u0006.\u0002 \u0001;

      [SpecialName]
      public bool \u0003()
      {
        return \u0080\u0006.\u007E\u0082\u0004((object) this.\u0001) == 0;
      }

      [SpecialName]
      public static \u0006.\u0003 \u0003()
      {
        return new \u0006.\u0003(string.Empty, string.Empty, string.Empty);
      }

      public \u0003([In] string obj0, [In] string obj1, [In] string obj2)
      {
        this.\u0001 = obj0;
        this.\u0001 = new \u0006.\u0002(obj1, obj2);
      }
    }
  }
}
