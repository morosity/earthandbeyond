﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: FactionEditor, Version=1.2.0.0, Culture=neutral, PublicKeyToken=2175665f88d36cc3
// MVID: 282C8E24-8545-40F1-BFC3-620198B9AA47
// Assembly location: D:\Server\eab\Tools\FactionEditor.exe

using \u0001;
using \u0003;
using \u0005;
using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace \u0003
{
  internal sealed class \u0004 : Control
  {
    private int \u0001 = 99;
    private Bitmap \u0001 = \u0006.\u0003(\u0004.\u0001(3384));
    private Bitmap \u0002 = \u0006.\u0003(\u0004.\u0001(3393));
    private Timer \u0001 = new Timer();
    private float \u0001 = 1f;
    private float \u0002 = 1f;
    [NonSerialized]
    internal static \u0002 \u0001;

    protected override void OnVisibleChanged([In] EventArgs obj0)
    {
      \u0010\u0003.\u0006\u000F((object) this, obj0);
      if (\u0001\u0007.\u0015\u0011((object) this))
        return;
      this.\u0003(\u0001\u0007.\u0098\u000E((object) this));
    }

    private void \u0003([In] bool obj0)
    {
      \u0098\u0006.\u007E\u0086\u0010((object) this.\u0001, obj0);
      this.\u0001 = 0;
      \u001B\u0006.\u007E\u000F\u000F((object) this);
    }

    protected override void OnResize([In] EventArgs obj0)
    {
      \u0018\u0004.\u0092\u000E((object) this, new Size(\u0088\u0006.\u0086\u0005(250f * this.\u0001), \u0088\u0006.\u0086\u0005(42f * this.\u0002)));
      \u0010\u0003.\u0008\u000F((object) this, obj0);
    }

    protected override void ScaleCore([In] float obj0, [In] float obj1)
    {
      this.\u0001 = obj0;
      this.\u0002 = obj1;
      \u0086\u0004.\u0011\u000F((object) this, obj0, obj1);
      \u0010\u0003.\u007E\u0008\u000F((object) this, EventArgs.Empty);
    }

    protected override void Dispose([In] bool obj0)
    {
      if (obj0 && this.\u0001 != null)
        \u001B\u0006.\u007E\u008B\u0010((object) this.\u0001);
      \u0098\u0006.\u0002\u000F((object) this, obj0);
    }

    protected override void OnPaint([In] PaintEventArgs obj0)
    {
      \u0094\u0004.\u0007\u000F((object) this, obj0);
      if (this.\u0002 != null)
        \u001E\u0004.\u007E\u0097\u0010((object) \u0008\u0004.\u007E\u0018\u000F((object) obj0), (Image) this.\u0002, new Rectangle(0, 0, \u0088\u0006.\u0086\u0005(250f * this.\u0001), \u0088\u0006.\u0086\u0005(42f * this.\u0002)), new Rectangle(0, 0, 250, 42), GraphicsUnit.Pixel);
      if (this.\u0001 == null || this.\u0001 <= 0)
        return;
      \u008B\u0003.\u007E\u0099\u0010((object) \u0008\u0004.\u007E\u0018\u000F((object) obj0), new Rectangle(\u0088\u0006.\u0086\u0005(46f * this.\u0001), 0, \u0088\u0006.\u0086\u0005(165f * this.\u0001), \u0088\u0006.\u0086\u0005(34f * this.\u0002)));
      \u008D\u0003.\u007E\u0098\u0010((object) \u0008\u0004.\u007E\u0018\u000F((object) obj0), (Image) this.\u0001, new Rectangle(\u0088\u0006.\u0086\u0005((float) (this.\u0001 - 6) * this.\u0001), \u0088\u0006.\u0086\u0005(16f * this.\u0002), \u0088\u0006.\u0086\u0005(40f * this.\u0001), \u0088\u0006.\u0086\u0005(12f * this.\u0002)), 0, 0, 40, 12, GraphicsUnit.Pixel);
    }

    private void \u0003([In] object obj0, [In] EventArgs obj1)
    {
      this.\u0001 += 11;
      if (this.\u0001 > 198)
        goto label_2;
label_1:
      \u001B\u0006.\u007E\u000F\u000F((object) this);
      return;
label_2:
      this.\u0001 = 0;
      goto label_1;
    }

    public \u0004()
    {
      this.\u0001.Interval = 85;
      \u009A\u0005.\u007E\u0084\u0010((object) this.\u0001, new EventHandler(this.\u0003));
      \u0018\u0004.\u0092\u000E((object) this, new Size(250, 42));
      \u0098\u0006.\u0094\u000E((object) this, false);
      \u001A\u0005.\u0013\u000F((object) this, ControlStyles.UserPaint | ControlStyles.ResizeRedraw | ControlStyles.SupportsTransparentBackColor | ControlStyles.AllPaintingInWmPaint | ControlStyles.DoubleBuffer, true);
    }

    static \u0004()
    {
      \u0001.\u0003.\u0003();
    }
  }
}
