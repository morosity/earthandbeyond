﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: FactionEditor, Version=1.2.0.0, Culture=neutral, PublicKeyToken=2175665f88d36cc3
// MVID: 282C8E24-8545-40F1-BFC3-620198B9AA47
// Assembly location: D:\Server\eab\Tools\FactionEditor.exe

using \u0001;
using \u0003;
using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Cryptography;

namespace \u0003
{
  internal sealed class \u0005
  {
    [NonSerialized]
    internal static \u0002 \u0001;
    public static string \u0001;

    public static byte[] \u0003([In] byte[] obj0, [In] string obj1)
    {
      if (\u0003\u0004.\u007E\u008B\u0004((object) obj1, \u0005.\u0001(3413)))
      {
        \u0005.\u0001 = \u0005.\u0001(3418);
        return (byte[]) null;
      }
      try
      {
        RijndaelManaged rijndaelManaged = new RijndaelManaged();
        RSACryptoServiceProvider cryptoServiceProvider = new RSACryptoServiceProvider();
        \u001B\u0005.\u007E\u0099\u0008((object) cryptoServiceProvider, obj1);
        \u001B\u0006.\u007E\u0004\u000E((object) rijndaelManaged);
        \u001B\u0006.\u007E\u0005\u000E((object) rijndaelManaged);
        byte[] numArray1 = new byte[48];
        \u0016\u0004.\u0080\u0005((Array) \u0087\u0002.\u007E\u009F\u0008((object) rijndaelManaged), 0, (Array) numArray1, 0, 32);
        \u0016\u0004.\u0080\u0005((Array) \u0087\u0002.\u007E\u009D\u0008((object) rijndaelManaged), 0, (Array) numArray1, 32, 16);
        MemoryStream memoryStream = new MemoryStream();
        try
        {
          byte[] numArray2 = \u0098\u0002.\u007E\u0008\u000E((object) cryptoServiceProvider, numArray1, false);
          \u0086\u0005.\u007E\u007F\u0008((object) memoryStream, (byte) 1);
          \u0086\u0005.\u007E\u007F\u0008((object) memoryStream, \u0094\u0003.\u0083\u0005(numArray2.Length / 8));
          \u0099\u0006.\u007E\u001F\u0008((object) memoryStream, numArray2, 0, numArray2.Length);
        }
        catch (CryptographicException ex1)
        {
          try
          {
            byte[] numArray2 = new byte[16];
            byte[] numArray3 = new byte[16];
            \u0016\u0004.\u0080\u0005((Array) \u0087\u0002.\u007E\u009F\u0008((object) rijndaelManaged), 0, (Array) numArray2, 0, 16);
            \u0016\u0004.\u0080\u0005((Array) \u0087\u0002.\u007E\u009F\u0008((object) rijndaelManaged), 16, (Array) numArray3, 0, 16);
            byte[] numArray4 = \u0098\u0002.\u007E\u0008\u000E((object) cryptoServiceProvider, numArray2, false);
            byte[] numArray5 = \u0098\u0002.\u007E\u0008\u000E((object) cryptoServiceProvider, numArray3, false);
            byte[] numArray6 = \u0098\u0002.\u007E\u0008\u000E((object) cryptoServiceProvider, \u0087\u0002.\u007E\u009D\u0008((object) rijndaelManaged), false);
            \u0086\u0005.\u007E\u007F\u0008((object) memoryStream, (byte) 2);
            \u0086\u0005.\u007E\u007F\u0008((object) memoryStream, \u0094\u0003.\u0083\u0005(numArray4.Length / 8));
            \u0099\u0006.\u007E\u001F\u0008((object) memoryStream, numArray4, 0, numArray4.Length);
            \u0099\u0006.\u007E\u001F\u0008((object) memoryStream, numArray5, 0, numArray5.Length);
            \u0099\u0006.\u007E\u001F\u0008((object) memoryStream, numArray6, 0, numArray6.Length);
          }
          catch (CryptographicException ex2)
          {
            \u0005.\u0001 = \u0005.\u0001(3511);
            return (byte[]) null;
          }
        }
        CryptoStream cryptoStream = new CryptoStream((Stream) memoryStream, \u000E\u0006.\u007E\u0002\u000E((object) rijndaelManaged), CryptoStreamMode.Write);
        \u0099\u0006.\u007E\u001F\u0008((object) cryptoStream, obj0, 0, obj0.Length);
        \u001B\u0006.\u007E\u009B\u0008((object) cryptoStream);
        return \u0087\u0002.\u007E\u0088\u0008((object) memoryStream);
      }
      catch (Exception ex)
      {
        \u0005.\u0001 = \u0008\u0003.\u0093\u0004(\u0005.\u0001(3717), \u001D\u0005.\u007E\u009F\u0004((object) ex));
        return (byte[]) null;
      }
    }

    static \u0005()
    {
      \u0001.\u0003.\u0003();
    }
  }
}
