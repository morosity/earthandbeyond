﻿// Decompiled with JetBrains decompiler
// Type: SmartAssembly.SmartExceptionsCore.UnhandledException
// Assembly: FactionEditor, Version=1.2.0.0, Culture=neutral, PublicKeyToken=2175665f88d36cc3
// MVID: 282C8E24-8545-40F1-BFC3-620198B9AA47
// Assembly location: D:\Server\eab\Tools\FactionEditor.exe

using \u0001;
using \u0003;
using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace SmartAssembly.SmartExceptionsCore
{
  [Serializable]
  public sealed class UnhandledException : Exception
  {
    [NonSerialized]
    internal static \u0002 \u0001;
    public int MethodID;
    public object[] Objects;
    public int ILOffset;
    private Exception previousException;

    internal Exception \u0003()
    {
      return this.previousException;
    }

    public static Exception \u0003([In] Exception obj0)
    {
      return UnhandledException.\u0003(obj0, new object[0]);
    }

    public static Exception \u0003([In] Exception obj0, [In] object obj1)
    {
      return UnhandledException.\u0003(obj0, new object[1]
      {
        obj1
      });
    }

    public static Exception \u0003([In] Exception obj0, [In] object obj1, [In] object obj2)
    {
      return UnhandledException.\u0003(obj0, new object[2]
      {
        obj1,
        obj2
      });
    }

    public static Exception \u0003([In] Exception obj0, [In] object obj1, [In] object obj2, [In] object obj3)
    {
      return UnhandledException.\u0003(obj0, new object[3]
      {
        obj1,
        obj2,
        obj3
      });
    }

    public static Exception \u0003([In] Exception obj0, [In] object obj1, [In] object obj2, [In] object obj3, [In] object obj4)
    {
      return UnhandledException.\u0003(obj0, new object[4]
      {
        obj1,
        obj2,
        obj3,
        obj4
      });
    }

    public static Exception \u0003([In] Exception obj0, [In] object obj1, [In] object obj2, [In] object obj3, [In] object obj4, [In] object obj5)
    {
      return UnhandledException.\u0003(obj0, new object[5]
      {
        obj1,
        obj2,
        obj3,
        obj4,
        obj5
      });
    }

    public static Exception \u0003([In] Exception obj0, [In] object obj1, [In] object obj2, [In] object obj3, [In] object obj4, [In] object obj5, [In] object obj6)
    {
      return UnhandledException.\u0003(obj0, new object[6]
      {
        obj1,
        obj2,
        obj3,
        obj4,
        obj5,
        obj6
      });
    }

    public static Exception \u0003([In] Exception obj0, [In] object obj1, [In] object obj2, [In] object obj3, [In] object obj4, [In] object obj5, [In] object obj6, [In] object obj7)
    {
      return UnhandledException.\u0003(obj0, new object[7]
      {
        obj1,
        obj2,
        obj3,
        obj4,
        obj5,
        obj6,
        obj7
      });
    }

    public static Exception \u0003([In] Exception obj0, [In] object obj1, [In] object obj2, [In] object obj3, [In] object obj4, [In] object obj5, [In] object obj6, [In] object obj7, [In] object obj8)
    {
      return UnhandledException.\u0003(obj0, new object[8]
      {
        obj1,
        obj2,
        obj3,
        obj4,
        obj5,
        obj6,
        obj7,
        obj8
      });
    }

    public static Exception \u0003([In] Exception obj0, [In] object obj1, [In] object obj2, [In] object obj3, [In] object obj4, [In] object obj5, [In] object obj6, [In] object obj7, [In] object obj8, [In] object obj9)
    {
      return UnhandledException.\u0003(obj0, new object[9]
      {
        obj1,
        obj2,
        obj3,
        obj4,
        obj5,
        obj6,
        obj7,
        obj8,
        obj9
      });
    }

    public static Exception \u0003([In] Exception obj0, [In] object obj1, [In] object obj2, [In] object obj3, [In] object obj4, [In] object obj5, [In] object obj6, [In] object obj7, [In] object obj8, [In] object obj9, [In] object obj10)
    {
      return UnhandledException.\u0003(obj0, new object[10]
      {
        obj1,
        obj2,
        obj3,
        obj4,
        obj5,
        obj6,
        obj7,
        obj8,
        obj9,
        obj10
      });
    }

    public static Exception \u0003([In] Exception obj0, [In] object[] obj1)
    {
      if (\u0006.\u0001)
        return (Exception) null;
      int num1 = -1;
      int num2 = -1;
      try
      {
        StackTrace stackTrace = new StackTrace(obj0);
        if (\u0080\u0006.\u007E\u000F\u0007((object) stackTrace) > 0)
        {
          StackFrame stackFrame = \u0099\u0005.\u007E\u0010\u0007((object) stackTrace, \u0080\u0006.\u007E\u000F\u0007((object) stackTrace) - 1);
          num2 = (\u0080\u0006.\u007E\u0004\u0006((object) \u008D\u0006.\u007E\u0011\u0007((object) stackFrame)) & 16777215) - 1;
          num1 = \u0080\u0006.\u007E\u0012\u0007((object) stackFrame);
        }
      }
      catch
      {
      }
      UnhandledException unhandledException = new UnhandledException(num2, obj1, num1, obj0);
      if (obj0 is UnhandledException)
      {
        Exception exception = (obj0 as UnhandledException).\u0003();
        if (exception != null)
          obj0 = exception;
      }
      Exception exception1 = obj0;
      while (\u000F\u0006.\u007E\u0001\u0005((object) exception1) != null)
        exception1 = \u000F\u0006.\u007E\u0001\u0005((object) exception1);
      try
      {
        // ISSUE: type reference
        FieldInfo fieldInfo = \u001A\u0006.\u007E\u0014\u0006((object) \u0081\u0005.\u0008\u0006(__typeref (Exception)), UnhandledException.\u0001(6183), BindingFlags.Instance | BindingFlags.NonPublic);
        \u0015\u0005.\u007E\u0098\u0007((object) fieldInfo, (object) exception1, (object) unhandledException);
      }
      catch
      {
      }
      return obj0;
    }

    public override void GetObjectData([In] SerializationInfo obj0, [In] StreamingContext obj1)
    {
      \u0017\u0005.\u0003\u0005((object) this, obj0, obj1);
      // ISSUE: type reference
      \u0016\u0005.\u007E\u009E\u0007((object) obj0, UnhandledException.\u0001(6204), (object) this.MethodID, \u0081\u0005.\u0008\u0006(__typeref (int)));
      // ISSUE: type reference
      \u0016\u0005.\u007E\u009E\u0007((object) obj0, UnhandledException.\u0001(6241), (object) this.ILOffset, \u0081\u0005.\u0008\u0006(__typeref (int)));
      // ISSUE: type reference
      \u0016\u0005.\u007E\u009E\u0007((object) obj0, UnhandledException.\u0001(6278), (object) this.previousException, \u0081\u0005.\u0008\u0006(__typeref (Exception)));
      int num = this.Objects == null ? 0 : this.Objects.Length;
      // ISSUE: type reference
      \u0016\u0005.\u007E\u009E\u0007((object) obj0, UnhandledException.\u0001(6327), (object) num, \u0081\u0005.\u0008\u0006(__typeref (int)));
      for (int index = 0; index < num; ++index)
      {
        // ISSUE: type reference
        \u0016\u0005.\u007E\u009E\u0007((object) obj0, \u0019\u0006.\u0090\u0004(UnhandledException.\u0001(6372), (object) index), this.Objects[index], \u0081\u0005.\u0008\u0006(__typeref (object)));
      }
    }

    internal UnhandledException([In] SerializationInfo obj0, [In] StreamingContext obj1)
      : base(obj0, obj1)
    {
      this.MethodID = \u0014\u0004.\u007E\u0001\u0008((object) obj0, UnhandledException.\u0001(6204));
      this.ILOffset = \u0014\u0004.\u007E\u0001\u0008((object) obj0, UnhandledException.\u0001(6241));
      // ISSUE: type reference
      this.previousException = (Exception) \u007F\u0002.\u007E\u009F\u0007((object) obj0, UnhandledException.\u0001(6278), \u0081\u0005.\u0008\u0006(__typeref (Exception)));
      int length = \u0014\u0004.\u007E\u0001\u0008((object) obj0, UnhandledException.\u0001(6327));
      this.Objects = new object[length];
      for (int index = 0; index < length; ++index)
      {
        // ISSUE: type reference
        this.Objects[index] = \u007F\u0002.\u007E\u009F\u0007((object) obj0, \u0019\u0006.\u0090\u0004(UnhandledException.\u0001(6372), (object) index), \u0081\u0005.\u0008\u0006(__typeref (object)));
      }
    }

    internal UnhandledException([In] int obj0, [In] object[] obj1, [In] int obj2, [In] Exception obj3)
      : base(string.Format(UnhandledException.\u0001(6417), (object) obj0, (object) obj2))
    {
      this.MethodID = obj0;
      this.Objects = obj1;
      this.ILOffset = obj2;
      this.previousException = obj3;
    }

    static UnhandledException()
    {
      \u0001.\u0003.\u0003();
    }
  }
}
