﻿// Decompiled with JetBrains decompiler
// Type: SmartAssembly.SmartExceptionsCore.ReportingService
// Assembly: FactionEditor, Version=1.2.0.0, Culture=neutral, PublicKeyToken=2175665f88d36cc3
// MVID: 282C8E24-8545-40F1-BFC3-620198B9AA47
// Assembly location: D:\Server\eab\Tools\FactionEditor.exe

using \u0001;
using System;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

namespace SmartAssembly.SmartExceptionsCore
{
  [WebServiceBinding(Name = "ReportingServiceSoap", Namespace = "http://www.smartassembly.com/webservices/Reporting/")]
  internal sealed class ReportingService : SoapHttpClientProtocol
  {
    [NonSerialized]
    internal static \u0002 \u0001;

    public ReportingService(string serverURL)
    {
      \u001B\u0005.\u0094\u0003((object) this, \u0008\u0003.\u0093\u0004(serverURL, ReportingService.\u0001(5838)));
      \u0017\u0004.\u0095\u0003((object) this, 180000);
    }

    [SoapDocumentMethod("http://www.smartassembly.com/webservices/Reporting/UploadReport2")]
    public string UploadReport2(string licenseID, [XmlElement(DataType = "base64Binary")] byte[] data)
    {
      return (string) \u0004\u0005.\u0098\u0003((object) this, ReportingService.\u0001(5859), new object[2]
      {
        (object) licenseID,
        (object) data
      })[0];
    }

    static ReportingService()
    {
      \u0003.\u0003();
    }
  }
}
