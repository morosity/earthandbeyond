﻿// Decompiled with JetBrains decompiler
// Type: SmartAssembly.SmartExceptionsCore.UploadReportLoginService
// Assembly: FactionEditor, Version=1.2.0.0, Culture=neutral, PublicKeyToken=2175665f88d36cc3
// MVID: 282C8E24-8545-40F1-BFC3-620198B9AA47
// Assembly location: D:\Server\eab\Tools\FactionEditor.exe

using \u0001;
using System;
using System.Web.Services;
using System.Web.Services.Protocols;

namespace SmartAssembly.SmartExceptionsCore
{
  [WebServiceBinding(Name = "LoginServiceSoap", Namespace = "http://www.smartassembly.com/webservices/UploadReportLogin/")]
  internal sealed class UploadReportLoginService : SoapHttpClientProtocol
  {
    [NonSerialized]
    internal static \u0002 \u0001;

    public UploadReportLoginService()
    {
      \u001B\u0005.\u0094\u0003((object) this, UploadReportLoginService.\u0001(5735));
      \u0017\u0004.\u0095\u0003((object) this, 30000);
    }

    [SoapDocumentMethod("http://www.smartassembly.com/webservices/UploadReportLogin/GetServerURL")]
    public string GetServerURL(string licenseID)
    {
      return (string) \u0004\u0005.\u0098\u0003((object) this, UploadReportLoginService.\u0001(5820), new object[1]
      {
        (object) licenseID
      })[0];
    }

    static UploadReportLoginService()
    {
      \u0003.\u0003();
    }
  }
}
