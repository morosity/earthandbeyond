﻿// Decompiled with JetBrains decompiler
// Type: SmartAssembly.Attributes.PoweredByAttribute
// Assembly: FactionEditor, Version=1.2.0.0, Culture=neutral, PublicKeyToken=2175665f88d36cc3
// MVID: 282C8E24-8545-40F1-BFC3-620198B9AA47
// Assembly location: D:\Server\eab\Tools\FactionEditor.exe

using SmartAssembly.SmartExceptionsCore;
using System;

namespace SmartAssembly.Attributes
{
  public sealed class PoweredByAttribute : Attribute
  {
    public PoweredByAttribute(string s)
    {
      try
      {
      }
      catch (Exception ex)
      {
        string str = s;
        throw UnhandledException.\u0003(ex, (object) this, (object) str);
      }
    }
  }
}
