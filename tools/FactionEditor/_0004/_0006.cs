﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: FactionEditor, Version=1.2.0.0, Culture=neutral, PublicKeyToken=2175665f88d36cc3
// MVID: 282C8E24-8545-40F1-BFC3-620198B9AA47
// Assembly location: D:\Server\eab\Tools\FactionEditor.exe

using \u0001;
using \u0004;
using SmartAssembly.SmartExceptionsCore;
using System;
using System.Net;
using System.Runtime.InteropServices;

namespace \u0004
{
  internal sealed class \u0006
  {
    private string \u0002 = string.Empty;
    [NonSerialized]
    internal static \u0002 \u0001;
    private string \u0001;
    private IWebProxy \u0001;

    public void \u0003([In] IWebProxy obj0)
    {
      this.\u0001 = obj0;
    }

    public string \u0003()
    {
      try
      {
        UploadReportLoginService reportLoginService = new UploadReportLoginService();
        if (this.\u0001 != null)
          \u0004\u0007.\u007E\u0096\u0003((object) reportLoginService, this.\u0001);
        this.\u0002 = reportLoginService.GetServerURL(this.\u0001);
        if (\u0080\u0006.\u007E\u0082\u0004((object) this.\u0002) == 0)
          throw new ApplicationException(\u0006.\u0001(5881));
        return \u0003\u0004.\u007E\u008B\u0004((object) this.\u0002, \u0006.\u0001(5453)) ? this.\u0002 : \u0006.\u0001(5448);
      }
      catch (Exception ex)
      {
        return \u0008\u0003.\u0093\u0004(\u0006.\u0001(5922), \u001D\u0005.\u007E\u009F\u0004((object) ex));
      }
    }

    public string \u0003([In] byte[] obj0)
    {
      try
      {
        ReportingService reportingService = new ReportingService(this.\u0002);
        if (this.\u0001 != null)
          \u0004\u0007.\u007E\u0096\u0003((object) reportingService, this.\u0001);
        return reportingService.UploadReport2(this.\u0001, obj0);
      }
      catch (Exception ex)
      {
        return \u0008\u0003.\u0093\u0004(\u0006.\u0001(5939), \u001D\u0005.\u007E\u009F\u0004((object) ex));
      }
    }

    public \u0006([In] string obj0)
    {
      this.\u0001 = obj0;
    }

    static \u0006()
    {
      \u0003.\u0003();
    }
  }
}
