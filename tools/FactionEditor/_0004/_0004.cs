﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: FactionEditor, Version=1.2.0.0, Culture=neutral, PublicKeyToken=2175665f88d36cc3
// MVID: 282C8E24-8545-40F1-BFC3-620198B9AA47
// Assembly location: D:\Server\eab\Tools\FactionEditor.exe

using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;

namespace \u0004
{
  internal sealed class \u0004 : EventArgs
  {
    private string \u0001 = string.Empty;
    private bool \u0003 = true;
    private SecurityException \u0001;
    private bool \u0001;
    private bool \u0002;

    [SpecialName]
    public SecurityException \u0003()
    {
      return this.\u0001;
    }

    [SpecialName]
    public string \u0003()
    {
      return this.\u0001;
    }

    [SpecialName]
    public bool \u0003()
    {
      return this.\u0003;
    }

    [SpecialName]
    public bool \u0004()
    {
      return this.\u0001;
    }

    [SpecialName]
    public void \u0003([In] bool obj0)
    {
      this.\u0001 = obj0;
    }

    [SpecialName]
    public bool \u0005()
    {
      return this.\u0002;
    }

    public \u0004([In] SecurityException obj0)
    {
      this.\u0001 = obj0;
    }

    public \u0004([In] SecurityException obj0, [In] bool obj1)
      : this(obj0)
    {
      this.\u0003 = obj1;
    }

    public \u0004([In] string obj0, [In] bool obj1)
      : this(new SecurityException(obj0), obj1)
    {
      this.\u0001 = obj0;
    }
  }
}
