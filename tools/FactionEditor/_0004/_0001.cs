﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: FactionEditor, Version=1.2.0.0, Culture=neutral, PublicKeyToken=2175665f88d36cc3
// MVID: 282C8E24-8545-40F1-BFC3-620198B9AA47
// Assembly location: D:\Server\eab\Tools\FactionEditor.exe

using \u0001;
using \u0005;
using System;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace \u0004
{
  internal sealed class \u0001 : Control
  {
    private Label \u0001 = new Label();
    private float \u0001 = 1f;
    private float \u0002 = 1f;
    [NonSerialized]
    internal static \u0002 \u0001;
    private Image \u0001;
    private Icon \u0001;
    private Bitmap \u0001;
    private \u0005.\u0005 \u0001;

    [SpecialName]
    public void \u0003([In] \u0005.\u0005 obj0)
    {
      if (this.\u0001 == obj0)
        return;
      this.\u0001 = obj0;
      switch (this.\u0001)
      {
        case \u0005.\u0005.\u0002:
          this.\u0001 = \u0006.\u0003(\u0004.\u0001.\u0001(5691));
          break;
        case \u0005.\u0005.\u0003:
          this.\u0001 = \u0006.\u0003(\u0004.\u0001.\u0001(5704));
          break;
        default:
          this.\u0001 = (Bitmap) null;
          break;
      }
      \u001B\u0006.\u007E\u000F\u000F((object) this);
    }

    [SpecialName]
    public override string get_Text()
    {
      return \u001D\u0005.\u007E\u0095\u000E((object) this.\u0001);
    }

    [SpecialName]
    public override void set_Text([In] string obj0)
    {
      \u001B\u0005.\u007E\u0096\u000E((object) this.\u0001, obj0);
    }

    protected override void Dispose([In] bool obj0)
    {
      if (obj0)
        goto label_7;
label_6:
      \u0098\u0006.\u0002\u000F((object) this, obj0);
      return;
label_7:
      if (this.\u0001 != null)
      {
        \u001B\u0006.\u007E\u009B\u0010((object) this.\u0001);
        this.\u0001 = (Icon) null;
      }
      if (this.\u0001 != null)
      {
        \u001B\u0006.\u007E\u008B\u0010((object) this.\u0001);
        this.\u0001 = (Image) null;
      }
      if (this.\u0001 != null)
      {
        \u001B\u0006.\u007E\u008B\u0010((object) this.\u0001);
        this.\u0001 = (Bitmap) null;
        goto label_6;
      }
      else
        goto label_6;
    }

    protected override void OnResize([In] EventArgs obj0)
    {
      \u009D\u0004.\u007E\u0012\u000F((object) this.\u0001, \u0088\u0006.\u0086\u0005(13f * this.\u0001), \u0088\u0006.\u0086\u0005(15f * this.\u0002), \u0080\u0006.\u009A\u000E((object) this) - \u0088\u0006.\u0086\u0005(69f * this.\u0001), \u0080\u0006.\u0088\u000E((object) this) - \u0088\u0006.\u0086\u0005(18f * this.\u0002));
      \u0010\u0003.\u0008\u000F((object) this, obj0);
    }

    protected override void ScaleCore([In] float obj0, [In] float obj1)
    {
      this.\u0001 = obj0;
      this.\u0002 = obj1;
      \u0086\u0004.\u0011\u000F((object) this, obj0, obj1);
      \u0010\u0003.\u007E\u0008\u000F((object) this, EventArgs.Empty);
    }

    protected override void OnPaint([In] PaintEventArgs obj0)
    {
      \u0094\u0004.\u0007\u000F((object) this, obj0);
      \u0088\u0003.\u007E\u0093\u0010((object) \u0008\u0004.\u007E\u0018\u000F((object) obj0), \u009D\u0005.\u000E\u0011(), 0, \u0095\u0003.\u001E\u000E((object) this).Height - 2, \u0095\u0003.\u001E\u000E((object) this).Width, \u0095\u0003.\u001E\u000E((object) this).Height - 2);
      \u0088\u0003.\u007E\u0093\u0010((object) \u0008\u0004.\u007E\u0018\u000F((object) obj0), \u009D\u0005.\u000F\u0011(), 0, \u0095\u0003.\u001E\u000E((object) this).Height - 1, \u0095\u0003.\u001E\u000E((object) this).Width, \u0095\u0003.\u001E\u000E((object) this).Height - 1);
      Rectangle rectangle = new Rectangle(\u0095\u0003.\u001E\u000E((object) this).Width - \u0088\u0006.\u0086\u0005(48f * this.\u0001), \u0088\u0006.\u0086\u0005(11f * this.\u0002), \u0088\u0006.\u0086\u0005(32f * this.\u0001), \u0088\u0006.\u0086\u0005(32f * this.\u0002));
      if (this.\u0001 != null)
      {
        \u001E\u0004.\u007E\u0097\u0010((object) \u0008\u0004.\u007E\u0018\u000F((object) obj0), this.\u0001, rectangle, new Rectangle(0, 0, 32, 32), GraphicsUnit.Pixel);
      }
      else
      {
        if (this.\u0001 == null)
          return;
        \u001E\u0002.\u007E\u0095\u0010((object) \u0008\u0004.\u007E\u0018\u000F((object) obj0), this.\u0001, rectangle);
        if (this.\u0001 == null)
          return;
        \u001E\u0004.\u007E\u0097\u0010((object) \u0008\u0004.\u007E\u0018\u000F((object) obj0), (Image) this.\u0001, new Rectangle(rectangle.Right - \u0088\u0006.\u0086\u0005(12f * this.\u0001), rectangle.Bottom - \u0088\u0006.\u0086\u0005(12f * this.\u0002), \u0088\u0006.\u0086\u0005(16f * this.\u0001), \u0088\u0006.\u0086\u0005(16f * this.\u0002)), new Rectangle(0, 0, 16, 16), GraphicsUnit.Pixel);
      }
    }

    protected override void OnFontChanged([In] EventArgs obj0)
    {
      try
      {
        \u009F\u0005.\u007E\u0084\u000E((object) this.\u0001, new Font(\u0001\u0005.\u007E\u0083\u000E((object) this), FontStyle.Bold));
        \u0010\u0003.\u0005\u000F((object) this, obj0);
      }
      catch
      {
      }
    }

    public \u0001()
    {
      try
      {
        \u0008\u0007.\u007E\u0004\u0010((object) this.\u0001, FlatStyle.System);
        \u009F\u0005.\u007E\u0084\u000E((object) this.\u0001, new Font(\u0001\u0005.\u007E\u0083\u000E((object) this), FontStyle.Bold));
      }
      catch
      {
      }
      \u0093\u0004.\u007E\u0016\u000F((object) \u009E\u0002.\u001F\u000E((object) this), (Control) this.\u0001);
      \u0013\u0004.\u007E\u001A\u000E((object) this, \u008B\u0005.\u0008\u0011());
      \u0098\u0006.\u0094\u000E((object) this, false);
      \u0011\u0007.\u007E\u0081\u000E((object) this, DockStyle.Top);
      \u0017\u0004.\u0089\u000E((object) this, 58);
      \u001A\u0005.\u0013\u000F((object) this, ControlStyles.UserPaint | ControlStyles.ResizeRedraw | ControlStyles.SupportsTransparentBackColor | ControlStyles.AllPaintingInWmPaint | ControlStyles.DoubleBuffer, true);
      this.\u0001 = \u0003.\u0003();
      \u0010\u0003.\u007E\u0008\u000F((object) this, EventArgs.Empty);
    }

    public \u0001([In] string obj0)
      : this()
    {
      \u001B\u0005.\u007E\u0096\u000E((object) this.\u0001, obj0);
    }

    static \u0001()
    {
      \u0003.\u0003();
    }
  }
}
