﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: FactionEditor, Version=1.2.0.0, Culture=neutral, PublicKeyToken=2175665f88d36cc3
// MVID: 282C8E24-8545-40F1-BFC3-620198B9AA47
// Assembly location: D:\Server\eab\Tools\FactionEditor.exe

using \u0001;
using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace \u0006
{
  internal sealed class \u0004 : Form
  {
    private \u0005.\u0004 \u0001 = new \u0005.\u0004();
    private Button \u0001 = new Button();
    private Button \u0002 = new Button();
    private \u0004.\u0001 \u0001 = new \u0004.\u0001(string.Format(\u0006.\u0004.\u0001(8104), (object) \u0006.\u0004.\u0001(7058)));
    private \u0003.\u0003 \u0001 = new \u0003.\u0003();
    [NonSerialized]
    internal static \u0002 \u0001;
    private \u0004.\u0004 \u0001;

    private void \u0003()
    {
      \u001B\u0006.\u0015\u000F((object) this);
      \u009C\u0006.\u007E\u0018\u000E((object) this.\u0002, AnchorStyles.Bottom | AnchorStyles.Right);
      \u0008\u0007.\u007E\u001B\u000F((object) this.\u0002, FlatStyle.System);
      \u0018\u0004.\u007E\u0092\u000E((object) this.\u0002, new Size(100, 24));
      \u000F\u0004.\u007E\u008C\u000E((object) this.\u0002, new Point(408 - \u0080\u0006.\u007E\u009A\u000E((object) this.\u0002), 188));
      \u0017\u0004.\u007E\u0093\u000E((object) this.\u0002, 0);
      \u001B\u0005.\u007E\u0096\u000E((object) this.\u0002, \u0006.\u0004.\u0001(8082));
      \u009A\u0005.\u007E\u009B\u000E((object) this.\u0002, new EventHandler(this.\u0004));
      \u009C\u0006.\u007E\u0018\u000E((object) this.\u0001, AnchorStyles.Bottom | AnchorStyles.Right);
      \u0008\u0007.\u007E\u001B\u000F((object) this.\u0001, FlatStyle.System);
      \u0018\u0004.\u007E\u0092\u000E((object) this.\u0001, new Size(100, 24));
      \u000F\u0004.\u007E\u008C\u000E((object) this.\u0001, new Point(\u0080\u0006.\u007E\u008A\u000E((object) this.\u0002) - \u0080\u0006.\u007E\u009A\u000E((object) this.\u0001) - 6, 188));
      \u0017\u0004.\u007E\u0093\u000E((object) this.\u0001, 1);
      \u001B\u0005.\u007E\u0096\u000E((object) this.\u0001, \u0006.\u0004.\u0001(8091));
      \u009A\u0005.\u007E\u009B\u000E((object) this.\u0001, new EventHandler(this.\u0003));
      \u009C\u0006.\u007E\u0018\u000E((object) this.\u0001, AnchorStyles.Bottom | AnchorStyles.Left);
      \u009D\u0004.\u007E\u0012\u000F((object) this.\u0001, 6, 186, 120, 32);
      this.\u0001.\u0003(\u0005.\u0005.\u0003);
      \u009C\u0006.\u007E\u0018\u000E((object) this.\u0001, AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right);
      \u000F\u0004.\u007E\u008C\u000E((object) this.\u0001, new Point(20, 72));
      \u0018\u0004.\u007E\u0092\u000E((object) this.\u0001, new Size(382, 13));
      \u0018\u0004.\u007E\u008D\u000F((object) this, new Size(5, 13));
      \u0018\u0004.\u0090\u000F((object) this, new Size(418, 224));
      \u0098\u0006.\u0091\u000F((object) this, false);
      \u000E\u0003.\u007E\u0017\u000F((object) \u009E\u0002.\u001F\u000E((object) this), new Control[5]
      {
        (Control) this.\u0001,
        (Control) this.\u0001,
        (Control) this.\u0002,
        (Control) this.\u0001,
        (Control) this.\u0001
      });
      \u0014\u0005.\u008E\u000F((object) this, FormBorderStyle.FixedSingle);
      \u0098\u0006.\u0093\u000F((object) this, false);
      \u0098\u0006.\u0094\u000F((object) this, false);
      \u0098\u0006.\u0095\u000F((object) this, false);
      \u0015\u0007.\u0098\u000F((object) this, FormStartPosition.CenterScreen);
      \u0098\u0006.\u0010\u000F((object) this, false);
    }

    private void \u0003([In] object obj0, [In] EventArgs obj1)
    {
      this.\u0001.\u0003(true);
      \u001B\u0006.\u009C\u000F((object) this);
    }

    private void \u0004([In] object obj0, [In] EventArgs obj1)
    {
      this.\u0001.\u0003(false);
      \u001B\u0006.\u009C\u000F((object) this);
    }

    public \u0004([In] \u0004.\u0004 obj0)
    {
      this.\u0003();
      this.Icon = \u0005.\u0003.\u0003();
      \u001B\u0005.\u007E\u0096\u000E((object) this, \u0006.\u0004.\u0001(7058));
      if (\u0080\u0006.\u007E\u0082\u0004((object) \u001D\u0005.\u007E\u0095\u000E((object) this)) == 0)
        \u001B\u0005.\u007E\u0096\u000E((object) this, \u0006.\u0004.\u0001(8205));
      this.\u0001 = obj0;
      if (!obj0.\u0003())
        \u0098\u0006.\u007E\u0099\u000E((object) this.\u0001, false);
      if (\u0080\u0006.\u007E\u0082\u0004((object) obj0.\u0003()) > 0)
      {
        \u001B\u0005.\u007E\u0096\u000E((object) this.\u0001, obj0.\u0003());
      }
      else
      {
        StringBuilder stringBuilder1 = new StringBuilder();
        StringBuilder stringBuilder2 = \u0095\u0005.\u007E\u0099\u0004((object) stringBuilder1, \u0019\u0006.\u0090\u0004(\u0006.\u0004.\u0001(8226), (object) \u0006.\u0004.\u0001(7058)));
        if (obj0.\u0003())
        {
          StringBuilder stringBuilder3 = \u0095\u0005.\u007E\u0099\u0004((object) stringBuilder1, \u0006.\u0004.\u0001(8520));
        }
        StringBuilder stringBuilder4 = \u0095\u0005.\u007E\u0099\u0004((object) stringBuilder1, \u001D\u0005.\u007E\u009F\u0004((object) obj0.\u0003()));
        \u001B\u0005.\u007E\u0096\u000E((object) this.\u0001, \u001D\u0005.\u007E\u0011\u0004((object) stringBuilder1));
      }
      int height = \u0080\u0006.\u007E\u001C\u000E((object) this.\u0001) + 60;
      if (height <= \u0095\u0003.\u008F\u000F((object) this).Height)
        return;
      \u0018\u0004.\u0090\u000F((object) this, new Size(\u0095\u0003.\u008F\u000F((object) this).Width, height));
    }

    static \u0004()
    {
      \u0001.\u0003.\u0003();
    }
  }
}
