﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: FactionEditor, Version=1.2.0.0, Culture=neutral, PublicKeyToken=2175665f88d36cc3
// MVID: 282C8E24-8545-40F1-BFC3-620198B9AA47
// Assembly location: D:\Server\eab\Tools\FactionEditor.exe

using \u0001;
using DevExpress.LookAndFeel;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using SmartAssembly.SmartExceptionsCore;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace \u0006
{
  internal sealed class \u0006 : XtraForm
  {
    public bool \u0001 = true;
    [NonSerialized]
    internal static \u0002 \u0001;
    public bool \u0002;
    private IContainer \u0001;
    private Label \u0001;
    private Label \u0002;
    public TextBox \u0001;
    public TextBox \u0002;
    private Button \u0001;
    private Button \u0002;
    private Label \u0003;
    public TextBox \u0003;
    public TextBox \u0004;
    private Label \u0004;
    private DefaultLookAndFeel \u0001;

    public \u0006()
    {
      try
      {
        this.\u0004();
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex, (object) this);
      }
    }

    private void \u0003([In] object obj0, [In] EventArgs obj1)
    {
      MySqlConnection mySqlConnection1;
      try
      {
        try
        {
          this.\u0003();
          mySqlConnection1 = new MySqlConnection(\u0007.\u0001.\u0003(\u0006.\u0006.\u0001(8819)));
          \u001B\u0006.\u007E\u000F\u0012((object) mySqlConnection1);
          \u001B\u0006.\u007E\u0008\u0012((object) mySqlConnection1);
          this.\u0001 = false;
          \u001B\u0006.\u009C\u000F((object) this);
        }
        catch (Exception ex)
        {
          int num = (int) \u000E\u0005.\u000F\u0010(\u0008\u0003.\u0093\u0004(\u0006.\u0006.\u0001(8828), \u001D\u0005.\u007E\u009F\u0004((object) ex)));
        }
      }
      catch (Exception ex)
      {
        MySqlConnection mySqlConnection2 = mySqlConnection1;
        Exception exception = ex;
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) mySqlConnection2, (object) exception, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u0004([In] object obj0, [In] EventArgs obj1)
    {
      try
      {
        this.\u0001 = true;
        \u001B\u0006.\u009C\u000F((object) this);
      }
      catch (Exception ex)
      {
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u0005([In] object obj0, [In] EventArgs obj1)
    {
      try
      {
        this.\u0002 = true;
      }
      catch (Exception ex)
      {
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u0003([In] object obj0, [In] KeyPressEventArgs obj1)
    {
      char ch;
      MySqlConnection mySqlConnection1;
      try
      {
        ch = \u0013\u0005.\u007E\u0001\u0010((object) obj1);
        if (!ch.Equals('\r'))
          return;
        try
        {
          this.\u0003();
          mySqlConnection1 = new MySqlConnection(\u0007.\u0001.\u0003(\u0006.\u0006.\u0001(8819)));
          \u001B\u0006.\u007E\u000F\u0012((object) mySqlConnection1);
          \u001B\u0006.\u007E\u0008\u0012((object) mySqlConnection1);
          this.\u0001 = false;
          \u001B\u0006.\u009C\u000F((object) this);
        }
        catch (Exception ex)
        {
          int num = (int) \u000E\u0005.\u000F\u0010(\u0008\u0003.\u0093\u0004(\u0006.\u0006.\u0001(8828), \u001D\u0005.\u007E\u009F\u0004((object) ex)));
        }
      }
      catch (Exception ex)
      {
        MySqlConnection mySqlConnection2 = mySqlConnection1;
        Exception exception = ex;
        // ISSUE: variable of a boxed type
        __Boxed<char> local = (ValueType) ch;
        object obj = obj0;
        KeyPressEventArgs keyPressEventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) mySqlConnection2, (object) exception, (object) local, (object) this, obj, (object) keyPressEventArgs);
      }
    }

    private void \u0006([In] object obj0, [In] EventArgs obj1)
    {
      int num1;
      string[] strArray1;
      int num2;
      int num3;
      try
      {
        \u001B\u0005 obj = \u001B\u0005.\u007E\u0096\u000E;
        strArray1 = new string[7];
        strArray1[0] = \u001D\u0005.\u007E\u0095\u000E((object) this);
        strArray1[1] = \u0006.\u0006.\u0001(8893);
        string[] strArray2 = strArray1;
        int index1 = 2;
        num1 = \u0080\u0006.\u007E\u008C\u0006((object) \u000F\u0003.\u007E\u008E\u0007((object) \u0090\u0004.\u007E\u0014\u0007((object) \u0005\u0005.\u007F\u0007())));
        string str1 = num1.ToString();
        strArray2[index1] = str1;
        strArray1[3] = \u0006.\u0006.\u0001(8898);
        string[] strArray3 = strArray1;
        int index2 = 4;
        num2 = \u0080\u0006.\u007E\u008D\u0006((object) \u000F\u0003.\u007E\u008E\u0007((object) \u0090\u0004.\u007E\u0014\u0007((object) \u0005\u0005.\u007F\u0007())));
        string str2 = num2.ToString();
        strArray3[index2] = str2;
        strArray1[5] = \u0006.\u0006.\u0001(8903);
        string[] strArray4 = strArray1;
        int index3 = 6;
        num3 = \u0080\u0006.\u007E\u008E\u0006((object) \u000F\u0003.\u007E\u008E\u0007((object) \u0090\u0004.\u007E\u0014\u0007((object) \u0005\u0005.\u007F\u0007())));
        string str3 = num3.ToString();
        strArray4[index3] = str3;
        string str4 = \u0007\u0004.\u0096\u0004(strArray1);
        obj((object) this, str4);
      }
      catch (Exception ex)
      {
        string[] strArray2 = strArray1;
        // ISSUE: variable of a boxed type
        __Boxed<int> local1 = (ValueType) num1;
        // ISSUE: variable of a boxed type
        __Boxed<int> local2 = (ValueType) num2;
        // ISSUE: variable of a boxed type
        __Boxed<int> local3 = (ValueType) num3;
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) strArray2, (object) local1, (object) local2, (object) local3, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u0003()
    {
      XmlTextWriter xmlTextWriter1;
      int num;
      try
      {
        \u0007.\u0001.\u0004(\u001D\u0005.\u007E\u0095\u000E((object) this.\u0001));
        \u0007.\u0001.\u0005(\u001D\u0005.\u007E\u0095\u000E((object) this.\u0002));
        \u0007.\u0001.\u0003(\u001D\u0005.\u007E\u0095\u000E((object) this.\u0003));
        \u0007.\u0001.\u0003(\u0001\u0006.\u008A\u0005(\u001D\u0005.\u007E\u0095\u000E((object) this.\u0004), 10));
        xmlTextWriter1 = new XmlTextWriter(\u0008\u0003.\u0093\u0004(\u0011\u0004.\u0010\u000E(), \u0006.\u0006.\u0001(8916)), (Encoding) null);
        \u008E\u0006.\u007E\u0094\u0011((object) xmlTextWriter1, Formatting.Indented);
        \u001B\u0006.\u007E\u0084\u0011((object) xmlTextWriter1);
        \u001B\u0005.\u007E\u0087\u0011((object) xmlTextWriter1, \u0006.\u0006.\u0001(8933));
        \u001B\u0005.\u007E\u0087\u0011((object) xmlTextWriter1, \u0006.\u0006.\u0001(8942));
        \u001B\u0005.\u007E\u008A\u0011((object) xmlTextWriter1, \u001D\u0005.\u007E\u0011\u0004((object) \u0007.\u0001.\u0003()));
        \u001B\u0006.\u007E\u0088\u0011((object) xmlTextWriter1);
        \u001B\u0005.\u007E\u0087\u0011((object) xmlTextWriter1, \u0006.\u0006.\u0001(8951));
        \u001B\u0005 obj = \u001B\u0005.\u007E\u008A\u0011;
        XmlTextWriter xmlTextWriter2 = xmlTextWriter1;
        num = \u0007.\u0001.\u0003();
        string str = num.ToString();
        obj((object) xmlTextWriter2, str);
        \u001B\u0006.\u007E\u0088\u0011((object) xmlTextWriter1);
        \u001B\u0005.\u007E\u0087\u0011((object) xmlTextWriter1, \u0006.\u0006.\u0001(8960));
        \u001B\u0005.\u007E\u008A\u0011((object) xmlTextWriter1, \u001D\u0005.\u007E\u0095\u000E((object) this.\u0001));
        \u001B\u0006.\u007E\u0088\u0011((object) xmlTextWriter1);
        \u001B\u0005.\u007E\u0087\u0011((object) xmlTextWriter1, \u0006.\u0006.\u0001(8969));
        \u001B\u0005.\u007E\u008A\u0011((object) xmlTextWriter1, \u001D\u0005.\u007E\u0095\u000E((object) this.\u0002));
        \u001B\u0006.\u007E\u0088\u0011((object) xmlTextWriter1);
        \u001B\u0006.\u007E\u0088\u0011((object) xmlTextWriter1);
        \u001B\u0006.\u007E\u0086\u0011((object) xmlTextWriter1);
        \u001B\u0006.\u007E\u008B\u0011((object) xmlTextWriter1);
      }
      catch (Exception ex)
      {
        XmlTextWriter xmlTextWriter2 = xmlTextWriter1;
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) num;
        throw UnhandledException.\u0003(ex, (object) xmlTextWriter2, (object) local, (object) this);
      }
    }

    protected override void Dispose([In] bool obj0)
    {
      try
      {
        if (obj0 && this.\u0001 != null)
          \u001B\u0006.\u007E\u001E\u0004((object) this.\u0001);
        \u0098\u0006.\u0087\u0003((object) this, obj0);
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<bool> local = (ValueType) obj0;
        throw UnhandledException.\u0003(ex, (object) this, (object) local);
      }
    }

    private void \u0004()
    {
      ComponentResourceManager componentResourceManager1;
      try
      {
        this.\u0001 = (IContainer) new Container();
        // ISSUE: type reference
        componentResourceManager1 = new ComponentResourceManager(\u0081\u0005.\u0008\u0006(__typeref (\u0006.\u0006)));
        this.\u0001 = new Label();
        this.\u0002 = new Label();
        this.\u0001 = new TextBox();
        this.\u0002 = new TextBox();
        this.\u0001 = new Button();
        this.\u0002 = new Button();
        this.\u0003 = new Label();
        this.\u0003 = new TextBox();
        this.\u0004 = new TextBox();
        this.\u0004 = new Label();
        this.\u0001 = new DefaultLookAndFeel(this.\u0001);
        \u001B\u0006.\u0088\u0003((object) this);
        \u0098\u0006.\u007E\u0019\u000E((object) this.\u0001, true);
        \u000F\u0004.\u007E\u008C\u000E((object) this.\u0001, new Point(37, 15));
        \u001B\u0005.\u007E\u008F\u000E((object) this.\u0001, \u0006.\u0006.\u0001(8978));
        \u0018\u0004.\u007E\u0092\u000E((object) this.\u0001, new Size(59, 13));
        \u0017\u0004.\u007E\u0093\u000E((object) this.\u0001, 0);
        \u001B\u0005.\u007E\u0096\u000E((object) this.\u0001, \u0006.\u0006.\u0001(8987));
        \u0098\u0006.\u007E\u0019\u000E((object) this.\u0002, true);
        \u000F\u0004.\u007E\u008C\u000E((object) this.\u0002, new Point(39, 41));
        \u001B\u0005.\u007E\u008F\u000E((object) this.\u0002, \u0006.\u0006.\u0001(9000));
        \u0018\u0004.\u007E\u0092\u000E((object) this.\u0002, new Size(57, 13));
        \u0017\u0004.\u007E\u0093\u000E((object) this.\u0002, 1);
        \u001B\u0005.\u007E\u0096\u000E((object) this.\u0002, \u0006.\u0006.\u0001(9009));
        \u000F\u0004.\u007E\u008C\u000E((object) this.\u0001, new Point(101, 12));
        \u001B\u0005.\u007E\u008F\u000E((object) this.\u0001, \u0006.\u0006.\u0001(9022));
        \u0018\u0004.\u007E\u0092\u000E((object) this.\u0001, new Size(142, 21));
        \u0017\u0004.\u007E\u0093\u000E((object) this.\u0001, 3);
        \u009A\u0005.\u007E\u0097\u000E((object) this.\u0001, new EventHandler(this.\u0005));
        \u001A\u0004.\u007E\u009E\u000E((object) this.\u0001, new KeyPressEventHandler(this.\u0003));
        \u000F\u0004.\u007E\u008C\u000E((object) this.\u0002, new Point(101, 38));
        \u001B\u0005.\u007E\u008F\u000E((object) this.\u0002, \u0006.\u0006.\u0001(9043));
        \u0018\u0003.\u007E\u0088\u000F((object) this.\u0002, '*');
        \u0018\u0004.\u007E\u0092\u000E((object) this.\u0002, new Size(142, 21));
        \u0017\u0004.\u007E\u0093\u000E((object) this.\u0002, 4);
        \u009A\u0005.\u007E\u0097\u000E((object) this.\u0002, new EventHandler(this.\u0005));
        \u001A\u0004.\u007E\u009E\u000E((object) this.\u0002, new KeyPressEventHandler(this.\u0003));
        \u000F\u0004.\u007E\u008C\u000E((object) this.\u0001, new Point(18, 123));
        \u001B\u0005.\u007E\u008F\u000E((object) this.\u0001, \u0006.\u0006.\u0001(9064));
        \u0018\u0004.\u007E\u0092\u000E((object) this.\u0001, new Size(77, 28));
        \u0017\u0004.\u007E\u0093\u000E((object) this.\u0001, 8);
        \u001B\u0005.\u007E\u0096\u000E((object) this.\u0001, \u0006.\u0006.\u0001(9077));
        \u0098\u0006.\u007E\u001C\u000F((object) this.\u0001, true);
        \u009A\u0005.\u007E\u009B\u000E((object) this.\u0001, new EventHandler(this.\u0004));
        \u000F\u0004.\u007E\u008C\u000E((object) this.\u0002, new Point(189, 123));
        \u001B\u0005.\u007E\u008F\u000E((object) this.\u0002, \u0006.\u0006.\u0001(9086));
        \u0018\u0004.\u007E\u0092\u000E((object) this.\u0002, new Size(77, 28));
        \u0017\u0004.\u007E\u0093\u000E((object) this.\u0002, 7);
        \u001B\u0005.\u007E\u0096\u000E((object) this.\u0002, \u0006.\u0006.\u0001(9103));
        \u0098\u0006.\u007E\u001C\u000F((object) this.\u0002, true);
        \u009A\u0005.\u007E\u009B\u000E((object) this.\u0002, new EventHandler(this.\u0003));
        \u0098\u0006.\u007E\u0019\u000E((object) this.\u0003, true);
        \u000F\u0004.\u007E\u008C\u000E((object) this.\u0003, new Point(35, 67));
        \u001B\u0005.\u007E\u008F\u000E((object) this.\u0003, \u0006.\u0006.\u0001(9112));
        \u0018\u0004.\u007E\u0092\u000E((object) this.\u0003, new Size(55, 13));
        \u0017\u0004.\u007E\u0093\u000E((object) this.\u0003, 2);
        \u001B\u0005.\u007E\u0096\u000E((object) this.\u0003, \u0006.\u0006.\u0001(9121));
        \u000F\u0004.\u007E\u008C\u000E((object) this.\u0003, new Point(101, 64));
        \u001B\u0005.\u007E\u008F\u000E((object) this.\u0003, \u0006.\u0006.\u0001(9134));
        \u0018\u0004.\u007E\u0092\u000E((object) this.\u0003, new Size(142, 21));
        \u0017\u0004.\u007E\u0093\u000E((object) this.\u0003, 5);
        \u009A\u0005.\u007E\u0097\u000E((object) this.\u0003, new EventHandler(this.\u0005));
        \u001A\u0004.\u007E\u009E\u000E((object) this.\u0003, new KeyPressEventHandler(this.\u0003));
        \u000F\u0004.\u007E\u008C\u000E((object) this.\u0004, new Point(101, 90));
        \u001B\u0005.\u007E\u008F\u000E((object) this.\u0004, \u0006.\u0006.\u0001(9147));
        \u0018\u0004.\u007E\u0092\u000E((object) this.\u0004, new Size(61, 21));
        \u0017\u0004.\u007E\u0093\u000E((object) this.\u0004, 6);
        \u009A\u0005.\u007E\u0097\u000E((object) this.\u0004, new EventHandler(this.\u0005));
        \u001A\u0004.\u007E\u009E\u000E((object) this.\u0004, new KeyPressEventHandler(this.\u0003));
        \u0098\u0006.\u007E\u0019\u000E((object) this.\u0004, true);
        \u000F\u0004.\u007E\u008C\u000E((object) this.\u0004, new Point(35, 93));
        \u001B\u0005.\u007E\u008F\u000E((object) this.\u0004, \u0006.\u0006.\u0001(9160));
        \u0018\u0004.\u007E\u0092\u000E((object) this.\u0004, new Size(53, 13));
        \u0017\u0004.\u007E\u0093\u000E((object) this.\u0004, 8);
        \u001B\u0005.\u007E\u0096\u000E((object) this.\u0004, \u0006.\u0006.\u0001(9169));
        \u001B\u0005.\u007E\u008F\u0003((object) \u001C\u0002.\u007E\u0091\u0003((object) this.\u0001), \u0006.\u0006.\u0001(9182));
        \u0089\u0003.\u0019\u000F((object) this, new SizeF(6f, 13f));
        \u0010\u0005.\u001A\u000F((object) this, AutoScaleMode.Font);
        \u0018\u0004.\u0090\u000F((object) this, new Size(278, 158));
        \u0093\u0004.\u007E\u0016\u000F((object) \u009E\u0002.\u001F\u000E((object) this), (Control) this.\u0004);
        \u0093\u0004.\u007E\u0016\u000F((object) \u009E\u0002.\u001F\u000E((object) this), (Control) this.\u0004);
        \u0093\u0004.\u007E\u0016\u000F((object) \u009E\u0002.\u001F\u000E((object) this), (Control) this.\u0002);
        \u0093\u0004.\u007E\u0016\u000F((object) \u009E\u0002.\u001F\u000E((object) this), (Control) this.\u0001);
        \u0093\u0004.\u007E\u0016\u000F((object) \u009E\u0002.\u001F\u000E((object) this), (Control) this.\u0003);
        \u0093\u0004.\u007E\u0016\u000F((object) \u009E\u0002.\u001F\u000E((object) this), (Control) this.\u0002);
        \u0093\u0004.\u007E\u0016\u000F((object) \u009E\u0002.\u001F\u000E((object) this), (Control) this.\u0001);
        \u0093\u0004.\u007E\u0016\u000F((object) \u009E\u0002.\u001F\u000E((object) this), (Control) this.\u0003);
        \u0093\u0004.\u007E\u0016\u000F((object) \u009E\u0002.\u001F\u000E((object) this), (Control) this.\u0002);
        \u0093\u0004.\u007E\u0016\u000F((object) \u009E\u0002.\u001F\u000E((object) this), (Control) this.\u0001);
        \u0014\u0005.\u008E\u000F((object) this, FormBorderStyle.FixedDialog);
        \u0093\u0006.\u0092\u000F((object) this, (Icon) \u008D\u0002.\u007E\u000E\u0008((object) componentResourceManager1, \u0006.\u0006.\u0001(9199)));
        \u001B\u0005.\u007E\u008F\u0003((object) \u001C\u0002.\u008A\u0003((object) this), \u0006.\u0006.\u0001(9182));
        \u0098\u0006.\u007E\u008E\u0003((object) \u001C\u0002.\u008A\u0003((object) this), true);
        \u0098\u0006.\u0093\u000F((object) this, false);
        \u0098\u0006.\u0094\u000F((object) this, false);
        \u001B\u0005.\u008F\u000E((object) this, \u0006.\u0006.\u0001(9103));
        \u0015\u0007.\u0098\u000F((object) this, FormStartPosition.CenterScreen);
        \u001B\u0005.\u007E\u0096\u000E((object) this, \u0006.\u0006.\u0001(9216));
        \u009A\u0005.\u009A\u000F((object) this, new EventHandler(this.\u0006));
        \u0098\u0006.\u0089\u0003((object) this, false);
        \u001B\u0006.\u000E\u000F((object) this);
      }
      catch (Exception ex)
      {
        ComponentResourceManager componentResourceManager2 = componentResourceManager1;
        throw UnhandledException.\u0003(ex, (object) componentResourceManager2, (object) this);
      }
    }

    static \u0006()
    {
      \u0003.\u0003();
    }
  }
}
