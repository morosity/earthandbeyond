﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: FactionEditor, Version=1.2.0.0, Culture=neutral, PublicKeyToken=2175665f88d36cc3
// MVID: 282C8E24-8545-40F1-BFC3-620198B9AA47
// Assembly location: D:\Server\eab\Tools\FactionEditor.exe

using \u0001;
using SmartAssembly.SmartExceptionsCore;
using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;

namespace \u0002
{
  internal sealed class \u0002
  {
    private static Assembly \u0001;
    private static string[] \u0001;

    internal static void \u0003()
    {
      try
      {
        try
        {
          AppDomain.CurrentDomain.ResourceResolve += new ResolveEventHandler(\u0002.\u0002.\u0003);
        }
        catch (Exception ex)
        {
        }
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex);
      }
    }

    internal static Assembly \u0003([In] object obj0, [In] ResolveEventArgs obj1)
    {
      string[] strArray1;
      string name;
      int index;
      try
      {
        if (\u0002.\u0002.\u0001 == null)
        {
          Monitor.Enter((object) (strArray1 = \u0002.\u0002.\u0001));
          try
          {
            \u0002.\u0002.\u0001 = Assembly.Load(\u0006.\u0003(83));
            if (\u0002.\u0002.\u0001 != null)
              \u0002.\u0002.\u0001 = \u0002.\u0002.\u0001.GetManifestResourceNames();
          }
          finally
          {
            Monitor.Exit((object) strArray1);
          }
        }
        name = obj1.Name;
        for (index = 0; index < \u0002.\u0002.\u0001.Length; ++index)
        {
          if (\u0002.\u0002.\u0001[index] == name)
            return \u0002.\u0002.\u0001;
        }
        return (Assembly) null;
      }
      catch (Exception ex)
      {
        string str = name;
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) index;
        string[] strArray2 = strArray1;
        object obj = obj0;
        ResolveEventArgs resolveEventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) str, (object) local, (object) strArray2, obj, (object) resolveEventArgs);
      }
    }

    static \u0002()
    {
      try
      {
        \u0002.\u0002.\u0001 = (Assembly) null;
        \u0002.\u0002.\u0001 = new string[0];
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex);
      }
    }
  }
}
