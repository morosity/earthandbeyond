﻿using SmartAssembly.Attributes;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyFileVersion("1.2.0.0")]
[assembly: Guid("51cee6cd-9428-4d1e-840a-fb8cfbfc3298")]
[assembly: SuppressIldasm]
[assembly: PoweredBy("Powered by {smartassembly}")]
[assembly: AssemblyTitle("FactionEditor")]
[assembly: AssemblyCompany("Net7 Entertainment")]
[assembly: AssemblyCopyright("Copyright © Net7 Entertainment 2009")]
[assembly: AssemblyProduct("FactionEditor")]
[assembly: AssemblyDescription("Faction Editor")]
[assembly: AssemblyVersion("1.2.0.0")]
