﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: MobEditor, Version=1.1.0.2, Culture=neutral, PublicKeyToken=016197d45384ac33
// MVID: 35831B81-08C7-444B-81F1-3EE10E122F41
// Assembly location: D:\Server\eab\Tools\N7 Mob Editor.exe

using \u0001;
using \u0005;
using System;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace \u0005
{
  internal sealed class \u0001 : Control
  {
    private Label \u0001 = new Label();
    private Timer \u0001 = new Timer();
    private bool \u0002 = true;
    private string \u0001 = string.Empty;
    private float \u0001 = 1f;
    private float \u0002 = 1f;
    [NonSerialized]
    internal static \u0002 \u0001;
    private Image \u0001;
    private bool \u0001;

    [SpecialName]
    public override string get_Text()
    {
      return \u009D\u0007.\u009E\u0010((object) this);
    }

    [SpecialName]
    public override void set_Text([In] string obj0)
    {
      \u0097\u0005.\u009F\u0010((object) this, obj0);
      \u0001\u0007.\u007E\u001A\u0011((object) this);
    }

    public void \u0003()
    {
      \u0011\u0003.\u007E\u0080\u0013((object) this.\u0001, false);
      this.\u0001 = (Image) null;
      this.\u0001 = false;
      this.\u0001 = string.Empty;
      \u0001\u0007.\u007E\u001A\u0011((object) this);
      \u008A\u0004.\u0092\u0010((object) this, 16);
    }

    public void \u0004()
    {
      \u0011\u0003.\u007E\u0080\u0013((object) this.\u0001, true);
      this.\u0001 = (Image) \u0006.\u0003(\u0005.\u0001.\u0001(5663));
      this.\u0001 = true;
      \u0001\u0007.\u007E\u001A\u0011((object) this);
    }

    public void \u0005()
    {
      this.\u0003(string.Empty);
    }

    public void \u0003([In] string obj0)
    {
      this.\u0001 = obj0;
      \u0011\u0003.\u007E\u0080\u0013((object) this.\u0001, false);
      this.\u0001 = (Image) \u0006.\u0003(\u0008\u0007.\u007E\u008E\u0005((object) obj0) > 0 ? \u0005.\u0001.\u0001(5681) : \u0005.\u0001.\u0001(5676));
      this.\u0002 = true;
      this.\u0001 = true;
      if (\u0008\u0007.\u007E\u008E\u0005((object) obj0) > 0)
        \u008A\u0004.\u0092\u0010((object) this, 100);
      \u0001\u0007.\u007E\u001A\u0011((object) this);
    }

    protected override void OnResize([In] EventArgs obj0)
    {
      \u0089\u0003.\u007E\u001D\u0011((object) this.\u0001, \u0018\u0007.\u0094\u0006(22f * this.\u0001), \u0018\u0007.\u0094\u0006(this.\u0002), \u0008\u0007.\u0005\u0011((object) this) - \u0018\u0007.\u0094\u0006(22f * this.\u0001), \u0008\u0007.\u0091\u0010((object) this) - \u0018\u0007.\u0094\u0006(this.\u0002));
      \u0002\u0006.\u0018\u0011((object) this, obj0);
    }

    protected override void ScaleCore([In] float obj0, [In] float obj1)
    {
      this.\u0001 = obj0;
      this.\u0002 = obj1;
      \u009A\u0004.\u001C\u0011((object) this, obj0, obj1);
      \u0002\u0006.\u007E\u0018\u0011((object) this, EventArgs.Empty);
    }

    protected override void OnPaint([In] PaintEventArgs obj0)
    {
      \u0097\u0004.\u0017\u0011((object) this, obj0);
      if (\u0093\u0007.\u0088\u0013((object) this))
      {
        this.\u0001 = (Image) \u0006.\u0003(\u0005.\u0001.\u0001(5663));
        this.\u0001 = true;
      }
      if (this.\u0001 != null && this.\u0002)
        \u0014\u0005.\u007E\u009A\u000F((object) \u000F\u0003.\u007E\u0082\u0011((object) obj0), this.\u0001, new Rectangle(0, 0, \u0018\u0007.\u0094\u0006(16f * this.\u0001), \u0018\u0007.\u0094\u0006(16f * this.\u0002)), new Rectangle(0, 0, 16, 16), GraphicsUnit.Pixel);
      if (this.\u0001)
        \u0097\u0005.\u007E\u009F\u0010((object) this.\u0001, \u0008\u0007.\u007E\u008E\u0005((object) this.\u0001) > 0 ? \u0001\u0006.\u0004\u0006(\u009D\u0007.\u009E\u0010((object) this), \u0005.\u0001.\u0001(5690), this.\u0001, \u0005.\u0001.\u0001(5695)) : \u009D\u0007.\u009E\u0010((object) this));
      else
        \u0097\u0005.\u007E\u009F\u0010((object) this.\u0001, string.Empty);
    }

    public \u0001()
    {
      \u008A\u0004.\u007E\u0081\u0013((object) this.\u0001, 250);
      \u0011\u0006.\u007E\u007F\u0013((object) this.\u0001, new EventHandler(this.\u0003));
      \u0001\u0005.\u007E\u0089\u0012((object) this.\u0001, FlatStyle.System);
      \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u0088\u0010((object) this), (Control) this.\u0001);
      \u0093\u0006.\u001E\u0011((object) this, ControlStyles.UserPaint | ControlStyles.ResizeRedraw | ControlStyles.SupportsTransparentBackColor | ControlStyles.AllPaintingInWmPaint | ControlStyles.DoubleBuffer, true);
      \u0011\u0003.\u009D\u0010((object) this, false);
    }

    public \u0001([In] string obj0)
      : this()
    {
      \u0097\u0005.\u009F\u0010((object) this, \u0014\u0003.\u0002\u0006(\u0005.\u0001.\u0001(5700), obj0));
    }

    protected override void Dispose([In] bool obj0)
    {
      if (obj0 && this.\u0001 != null)
        \u0001\u0007.\u007E\u008F\u000F((object) this.\u0001);
      \u0011\u0003.\u0012\u0011((object) this, obj0);
    }

    private void \u0003([In] object obj0, [In] EventArgs obj1)
    {
      this.\u0002 = !this.\u0002;
      \u0001\u0007.\u007E\u001A\u0011((object) this);
    }

    static \u0001()
    {
      \u0003.\u0003();
    }
  }
}
