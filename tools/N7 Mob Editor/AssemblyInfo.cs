﻿using SmartAssembly.Attributes;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyFileVersion("1.1.0.2")]
[assembly: Guid("efeaca96-5a10-48f6-9642-1c60f9b2d503")]
[assembly: SuppressIldasm]
[assembly: PoweredBy("Powered by {smartassembly}")]
[assembly: AssemblyTitle("MobEditor")]
[assembly: AssemblyCompany("Net7 Entertainment")]
[assembly: AssemblyCopyright("Copyright © Net7 Entertainment 2009")]
[assembly: AssemblyProduct("Mob Editor")]
[assembly: AssemblyDescription("Edits mobs for Net7")]
[assembly: AssemblyVersion("1.1.0.2")]
