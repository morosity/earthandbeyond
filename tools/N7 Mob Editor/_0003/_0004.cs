﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: MobEditor, Version=1.1.0.2, Culture=neutral, PublicKeyToken=016197d45384ac33
// MVID: 35831B81-08C7-444B-81F1-3EE10E122F41
// Assembly location: D:\Server\eab\Tools\N7 Mob Editor.exe

using \u0001;
using \u0003;
using \u0005;
using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace \u0003
{
  internal sealed class \u0004 : Control
  {
    private int \u0001 = 99;
    private Bitmap \u0001 = \u0006.\u0003(\u0004.\u0001(3087));
    private Bitmap \u0002 = \u0006.\u0003(\u0004.\u0001(3096));
    private Timer \u0001 = new Timer();
    private float \u0001 = 1f;
    private float \u0002 = 1f;
    [NonSerialized]
    internal static \u0002 \u0001;

    protected override void OnVisibleChanged([In] EventArgs obj0)
    {
      \u0002\u0006.\u0016\u0011((object) this, obj0);
      if (\u0093\u0007.\u0088\u0013((object) this))
        return;
      this.\u0003(\u0093\u0007.\u0002\u0011((object) this));
    }

    private void \u0003([In] bool obj0)
    {
      \u0011\u0003.\u007E\u0080\u0013((object) this.\u0001, obj0);
      this.\u0001 = 0;
      \u0001\u0007.\u007E\u001A\u0011((object) this);
    }

    protected override void OnResize([In] EventArgs obj0)
    {
      \u0082\u0004.\u009B\u0010((object) this, new Size(\u0018\u0007.\u0094\u0006(250f * this.\u0001), \u0018\u0007.\u0094\u0006(42f * this.\u0002)));
      \u0002\u0006.\u0018\u0011((object) this, obj0);
    }

    protected override void ScaleCore([In] float obj0, [In] float obj1)
    {
      this.\u0001 = obj0;
      this.\u0002 = obj1;
      \u009A\u0004.\u001C\u0011((object) this, obj0, obj1);
      \u0002\u0006.\u007E\u0018\u0011((object) this, EventArgs.Empty);
    }

    protected override void Dispose([In] bool obj0)
    {
      if (obj0 && this.\u0001 != null)
        \u0001\u0007.\u007E\u008F\u000F((object) this.\u0001);
      \u0011\u0003.\u0012\u0011((object) this, obj0);
    }

    protected override void OnPaint([In] PaintEventArgs obj0)
    {
      \u0097\u0004.\u0017\u0011((object) this, obj0);
      if (this.\u0002 != null)
        \u0014\u0005.\u007E\u009A\u000F((object) \u000F\u0003.\u007E\u0082\u0011((object) obj0), (Image) this.\u0002, new Rectangle(0, 0, \u0018\u0007.\u0094\u0006(250f * this.\u0001), \u0018\u0007.\u0094\u0006(42f * this.\u0002)), new Rectangle(0, 0, 250, 42), GraphicsUnit.Pixel);
      if (this.\u0001 == null || this.\u0001 <= 0)
        return;
      \u008E\u0003.\u007E\u009C\u000F((object) \u000F\u0003.\u007E\u0082\u0011((object) obj0), new Rectangle(\u0018\u0007.\u0094\u0006(46f * this.\u0001), 0, \u0018\u0007.\u0094\u0006(165f * this.\u0001), \u0018\u0007.\u0094\u0006(34f * this.\u0002)));
      \u0082\u0005.\u007E\u009B\u000F((object) \u000F\u0003.\u007E\u0082\u0011((object) obj0), (Image) this.\u0001, new Rectangle(\u0018\u0007.\u0094\u0006((float) (this.\u0001 - 6) * this.\u0001), \u0018\u0007.\u0094\u0006(16f * this.\u0002), \u0018\u0007.\u0094\u0006(40f * this.\u0001), \u0018\u0007.\u0094\u0006(12f * this.\u0002)), 0, 0, 40, 12, GraphicsUnit.Pixel);
    }

    private void \u0003([In] object obj0, [In] EventArgs obj1)
    {
      this.\u0001 += 11;
      if (this.\u0001 > 198)
        goto label_2;
label_1:
      \u0001\u0007.\u007E\u001A\u0011((object) this);
      return;
label_2:
      this.\u0001 = 0;
      goto label_1;
    }

    public \u0004()
    {
      this.\u0001.Interval = 85;
      \u0011\u0006.\u007E\u007F\u0013((object) this.\u0001, new EventHandler(this.\u0003));
      \u0082\u0004.\u009B\u0010((object) this, new Size(250, 42));
      \u0011\u0003.\u009D\u0010((object) this, false);
      \u0093\u0006.\u001E\u0011((object) this, ControlStyles.UserPaint | ControlStyles.ResizeRedraw | ControlStyles.SupportsTransparentBackColor | ControlStyles.AllPaintingInWmPaint | ControlStyles.DoubleBuffer, true);
    }

    static \u0004()
    {
      \u0001.\u0003.\u0003();
    }
  }
}
