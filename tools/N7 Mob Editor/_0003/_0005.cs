﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: MobEditor, Version=1.1.0.2, Culture=neutral, PublicKeyToken=016197d45384ac33
// MVID: 35831B81-08C7-444B-81F1-3EE10E122F41
// Assembly location: D:\Server\eab\Tools\N7 Mob Editor.exe

using \u0001;
using \u0003;
using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Cryptography;

namespace \u0003
{
  internal sealed class \u0005
  {
    [NonSerialized]
    internal static \u0002 \u0001;
    public static string \u0001;

    public static byte[] \u0003([In] byte[] obj0, [In] string obj1)
    {
      if (\u0094\u0003.\u007E\u0097\u0005((object) obj1, \u0005.\u0001(3116)))
      {
        \u0005.\u0001 = \u0005.\u0001(3121);
        return (byte[]) null;
      }
      try
      {
        RijndaelManaged rijndaelManaged = new RijndaelManaged();
        RSACryptoServiceProvider cryptoServiceProvider = new RSACryptoServiceProvider();
        \u0097\u0005.\u007E\u0007\u000F((object) cryptoServiceProvider, obj1);
        \u0001\u0007.\u007E\u0016\u000F((object) rijndaelManaged);
        \u0001\u0007.\u007E\u0017\u000F((object) rijndaelManaged);
        byte[] numArray1 = new byte[48];
        \u001E\u0006.\u008F\u0006((Array) \u0088\u0002.\u007E\u0012\u000F((object) rijndaelManaged), 0, (Array) numArray1, 0, 32);
        \u001E\u0006.\u008F\u0006((Array) \u0088\u0002.\u007E\u0010\u000F((object) rijndaelManaged), 0, (Array) numArray1, 32, 16);
        MemoryStream memoryStream = new MemoryStream();
        try
        {
          byte[] numArray2 = \u009A\u0002.\u007E\u001A\u000F((object) cryptoServiceProvider, numArray1, false);
          \u0005\u0006.\u007E\u008D\u000E((object) memoryStream, (byte) 1);
          \u0005\u0006.\u007E\u008D\u000E((object) memoryStream, \u009E\u0003.\u0092\u0006(numArray2.Length / 8));
          \u008D\u0007.\u007E\u008C\u000E((object) memoryStream, numArray2, 0, numArray2.Length);
        }
        catch (CryptographicException ex1)
        {
          try
          {
            byte[] numArray2 = new byte[16];
            byte[] numArray3 = new byte[16];
            \u001E\u0006.\u008F\u0006((Array) \u0088\u0002.\u007E\u0012\u000F((object) rijndaelManaged), 0, (Array) numArray2, 0, 16);
            \u001E\u0006.\u008F\u0006((Array) \u0088\u0002.\u007E\u0012\u000F((object) rijndaelManaged), 16, (Array) numArray3, 0, 16);
            byte[] numArray4 = \u009A\u0002.\u007E\u001A\u000F((object) cryptoServiceProvider, numArray2, false);
            byte[] numArray5 = \u009A\u0002.\u007E\u001A\u000F((object) cryptoServiceProvider, numArray3, false);
            byte[] numArray6 = \u009A\u0002.\u007E\u001A\u000F((object) cryptoServiceProvider, \u0088\u0002.\u007E\u0010\u000F((object) rijndaelManaged), false);
            \u0005\u0006.\u007E\u008D\u000E((object) memoryStream, (byte) 2);
            \u0005\u0006.\u007E\u008D\u000E((object) memoryStream, \u009E\u0003.\u0092\u0006(numArray4.Length / 8));
            \u008D\u0007.\u007E\u008C\u000E((object) memoryStream, numArray4, 0, numArray4.Length);
            \u008D\u0007.\u007E\u008C\u000E((object) memoryStream, numArray5, 0, numArray5.Length);
            \u008D\u0007.\u007E\u008C\u000E((object) memoryStream, numArray6, 0, numArray6.Length);
          }
          catch (CryptographicException ex2)
          {
            \u0005.\u0001 = \u0005.\u0001(3214);
            return (byte[]) null;
          }
        }
        CryptoStream cryptoStream = new CryptoStream((Stream) memoryStream, \u009F\u0002.\u007E\u0014\u000F((object) rijndaelManaged), CryptoStreamMode.Write);
        \u008D\u0007.\u007E\u008C\u000E((object) cryptoStream, obj0, 0, obj0.Length);
        \u0001\u0007.\u007E\u000E\u000F((object) cryptoStream);
        return \u0088\u0002.\u007E\u0095\u000E((object) memoryStream);
      }
      catch (Exception ex)
      {
        \u0005.\u0001 = \u0014\u0003.\u0002\u0006(\u0005.\u0001(3420), \u009D\u0007.\u007E\u0013\u0006((object) ex));
        return (byte[]) null;
      }
    }

    static \u0005()
    {
      \u0001.\u0003.\u0003();
    }
  }
}
