﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: MobEditor, Version=1.1.0.2, Culture=neutral, PublicKeyToken=016197d45384ac33
// MVID: 35831B81-08C7-444B-81F1-3EE10E122F41
// Assembly location: D:\Server\eab\Tools\N7 Mob Editor.exe

using \u0002;
using \u0003;
using \u0004;
using Microsoft.Win32;
using SmartAssembly.SmartExceptionsCore;
using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Web.Services.Protocols;
using System.Xml;

namespace \u0003
{
  internal abstract class \u0006
  {
    private char[] \u0001 = new char[0];
    private ArrayList \u0001 = new ArrayList();
    private ArrayList \u0002 = new ArrayList();
    private Hashtable \u0003 = new Hashtable();
    private ArrayList \u0003 = new ArrayList();
    private Hashtable \u0004 = new Hashtable();
    [NonSerialized]
    internal static \u0001.\u0002 \u0001;
    private static string \u0001;
    internal static bool \u0001;
    private IWebProxy \u0001;
    private static \u0006 \u0001;
    private Exception \u0001;
    private Hashtable \u0001;
    private Hashtable \u0002;
    private XmlTextWriter \u0001;
    private EventHandler \u0001;
    private \u0004.\u0002 \u0001;

    [SpecialName]
    [MethodImpl(MethodImplOptions.Synchronized)]
    public void \u0003([In] EventHandler obj0)
    {
      this.\u0001 = (EventHandler) \u001D\u0007.\u0080\u0006((Delegate) this.\u0001, (Delegate) obj0);
    }

    [SpecialName]
    [MethodImpl(MethodImplOptions.Synchronized)]
    public void \u0003([In] \u0004.\u0002 obj0)
    {
      this.\u0001 = (\u0004.\u0002) \u001D\u0007.\u0080\u0006((Delegate) this.\u0001, (Delegate) obj0);
    }

    protected abstract void \u0003([In] \u0005.\u0002 obj0);

    protected abstract void \u0003([In] \u0004.\u0005 obj0);

    protected abstract void \u0003([In] \u0004.\u0004 obj0);

    [SecurityPermission(SecurityAction.Demand, UnmanagedCode = true)]
    public static void \u0003([In] \u0006 obj0)
    {
      if (obj0 == null)
        return;
      \u0006.\u0001 = obj0;
      \u0084\u0006.\u007E\u008C\u0006((object) \u0088\u0003.\u008A\u0006(), new UnhandledExceptionEventHandler(obj0.\u0003));
      \u000F\u0007.\u001A\u0010(new ThreadExceptionEventHandler(obj0.\u0003));
    }

    [SpecialName]
    public char[] \u0003()
    {
      string str;
      if (this.\u0001.Length == 0 && (str = \u009D\u0007.\u007E\u0099\u0005((object) \u0006.\u0001(3439))) != null)
      {
        if (!\u0005\u0003.\u008B\u0005(str, \u0006.\u0001(3452)))
        {
          if (\u0005\u0003.\u008B\u0005(str, \u0006.\u0001(3439)))
            this.\u0001 = new char[58]
            {
              '\x0001',
              '\x0002',
              '\x0003',
              '\x0004',
              '\x0005',
              '\x0006',
              '\a',
              '\b',
              '\x000E',
              '\x000F',
              '\x0010',
              '\x0011',
              '\x0012',
              '\x0013',
              '\x0014',
              '\x0015',
              '\x0016',
              '\x0017',
              '\x0018',
              '\x0019',
              '\x001A',
              '\x001B',
              '\x001C',
              '\x001D',
              '\x001E',
              '\x001F',
              '\x007F',
              '\x0080',
              '\x0081',
              '\x0082',
              '\x0083',
              '\x0084',
              '\x0086',
              '\x0087',
              '\x0088',
              '\x0089',
              '\x008A',
              '\x008B',
              '\x008C',
              '\x008D',
              '\x008E',
              '\x008F',
              '\x0090',
              '\x0091',
              '\x0092',
              '\x0093',
              '\x0094',
              '\x0095',
              '\x0096',
              '\x0097',
              '\x0098',
              '\x0099',
              '\x009A',
              '\x009B',
              '\x009C',
              '\x009D',
              '\x009E',
              '\x009F'
            };
        }
        else
          this.\u0001 = new char[62]
          {
            'a',
            'b',
            'c',
            'd',
            'e',
            'f',
            'g',
            'h',
            'i',
            'j',
            'k',
            'l',
            'm',
            'n',
            'o',
            'p',
            'q',
            'r',
            's',
            't',
            'u',
            'v',
            'w',
            'x',
            'y',
            'z',
            'A',
            'B',
            'C',
            'D',
            'E',
            'F',
            'G',
            'H',
            'I',
            'J',
            'K',
            'L',
            'M',
            'N',
            'O',
            'P',
            'Q',
            'R',
            'S',
            'T',
            'U',
            'V',
            'W',
            'X',
            'Y',
            'Z',
            '0',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9'
          };
      }
      return this.\u0001;
    }

    public static void \u0003([In] Exception obj0, [In] object[] obj1)
    {
      try
      {
        if (obj0 != null && obj0 is SecurityException && (\u0005\u0003.\u008B\u0005(\u0006.\u0001, \u0006.\u0001(3461)) && \u0006.\u0001.\u0003((SecurityException) obj0)))
          return;
        \u0006.\u0001.\u0003(UnhandledException.\u0003(obj0, obj1), false);
      }
      catch
      {
        \u0086\u0006.\u001C\u0010();
      }
    }

    public static Exception \u0003([In] Exception obj0, [In] object[] obj1)
    {
      \u0006.\u0004(obj0, obj1);
      return (Exception) new SoapException(\u009D\u0007.\u007E\u0013\u0006((object) obj0), SoapException.ServerFaultCode);
    }

    public static void \u0004([In] Exception obj0, [In] object[] obj1)
    {
      try
      {
        if (\u0006.\u0001 == null)
        {
          foreach (Type type in \u0093\u0002.\u007E\u0083\u0008((object) \u0013\u0007.\u008D\u0008()))
          {
            if (type != null && \u0008\u0005.\u007E\u001E\u0007((object) type) != null)
            {
              // ISSUE: type reference
              if (\u0008\u0005.\u007E\u001E\u0007((object) type) == \u0014\u0006.\u001A\u0007(__typeref (\u0006)))
              {
                try
                {
                  \u0006.\u0001 = (\u0006) \u0082\u0002.\u0082\u0006(type, true);
                  if (\u0006.\u0001 != null)
                    break;
                }
                catch
                {
                }
              }
            }
          }
        }
        if (\u0006.\u0001 == null)
          return;
        \u0006.\u0001.\u0003(UnhandledException.\u0003(obj0, obj1), true);
      }
      catch
      {
      }
    }

    private void \u0003([In] object obj0, [In] ThreadExceptionEventArgs obj1)
    {
      try
      {
        if (\u0003\u0008.\u007E\u008E\u0013((object) obj1) is SecurityException && (\u0005\u0003.\u008B\u0005(\u0006.\u0001, \u0006.\u0001(3461)) && this.\u0003(\u0003\u0008.\u007E\u008E\u0013((object) obj1) as SecurityException)))
          return;
        this.\u0003(\u0003\u0008.\u007E\u008E\u0013((object) obj1), true);
      }
      catch
      {
      }
    }

    private void \u0003([In] object obj0, [In] UnhandledExceptionEventArgs obj1)
    {
      try
      {
        if (\u001F\u0006.\u007E\u0094\u0007((object) obj1) is SecurityException && (\u0005\u0003.\u008B\u0005(\u0006.\u0001, \u0006.\u0001(3461)) && this.\u0003(\u001F\u0006.\u007E\u0094\u0007((object) obj1) as SecurityException)) || !(\u001F\u0006.\u007E\u0094\u0007((object) obj1) is Exception))
          return;
        this.\u0003((Exception) \u001F\u0006.\u007E\u0094\u0007((object) obj1), !\u0093\u0007.\u007E\u0095\u0007((object) obj1));
      }
      catch
      {
      }
    }

    private string \u0003([In] object obj0)
    {
      try
      {
        if (obj0 == null)
          return string.Empty;
        if (obj0 is int)
          return ((int) obj0).ToString(\u0006.\u0001(3466));
        if (obj0 is long)
          return ((long) obj0).ToString(\u0006.\u0001(3466));
        if (obj0 is short)
          return ((short) obj0).ToString(\u0006.\u0001(3466));
        if (obj0 is uint)
          return ((uint) obj0).ToString(\u0006.\u0001(3466));
        if (obj0 is ulong)
          return ((ulong) obj0).ToString(\u0006.\u0001(3466));
        if (obj0 is ushort)
          return ((ushort) obj0).ToString(\u0006.\u0001(3466));
        if (obj0 is byte)
          return ((byte) obj0).ToString(\u0006.\u0001(3466));
        if (obj0 is sbyte)
          return ((sbyte) obj0).ToString(\u0006.\u0001(3466));
        if (obj0 is IntPtr)
          return ((IntPtr) obj0).ToInt64().ToString(\u0006.\u0001(3466));
        if (obj0 is UIntPtr)
          return ((UIntPtr) obj0).ToUInt64().ToString(\u0006.\u0001(3466));
      }
      catch
      {
      }
      return string.Empty;
    }

    private string \u0003([In] string obj0)
    {
      if (\u0094\u0003.\u007E\u0097\u0005((object) obj0, \u0006.\u0001(3471)) && \u0094\u0003.\u007E\u0093\u0005((object) obj0, \u0006.\u0001(3492)))
        return \u0006.\u0001(3513);
      return obj0;
    }

    private void \u0003([In] object obj0, [In] FieldInfo obj1)
    {
      string str1 = obj1 == null ? (string) null : \u009D\u0007.\u007E\u0014\u0007((object) obj1);
      string str2 = obj1 == null ? \u0006.\u0001(3595) : \u0006.\u0001(3586);
      if (obj0 == null)
      {
        \u0097\u0005.\u007E\u001E\u000F((object) this.\u0001, str2);
        if (obj1 != null)
        {
          if (\u0093\u0007.\u007E\u0007\u000E((object) obj1))
            \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(3604), \u0006.\u0001(3461));
          Type type = \u0008\u0005.\u007E\u0004\u000E((object) obj1);
          if (type != null && \u0093\u0007.\u007E\u008B\u0007((object) type))
          {
            this.\u0003(\u0008\u0005.\u007E\u008C\u0007((object) type));
            if (\u0093\u0007.\u007E\u0088\u0007((object) type))
              \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(3613), \u0006.\u0001(3461));
            if (\u0093\u0007.\u007E\u0089\u0007((object) type))
              \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(3622), \u0006.\u0001(3461));
            if (\u0093\u0007.\u007E\u0087\u0007((object) type))
              \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(3635), \u0008\u0007.\u007E\u001D\u0007((object) type).ToString());
          }
          else
            this.\u0003(type);
        }
        if (str1 != null)
          this.\u0003(str1);
        \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(3644), \u0006.\u0001(3461));
        \u0001\u0007.\u007E\u001F\u000F((object) this.\u0001);
      }
      else
      {
        Type type = \u0008\u0005.\u007E\u001F\u0005(obj0);
        string str3 = (string) null;
        string str4 = (string) null;
        if (obj0 is string)
          str3 = \u0006.\u0001(3653);
        if (str3 == null)
        {
          if (\u0093\u0007.\u007E\u008A\u0007((object) type) || obj0 is IntPtr || obj0 is UIntPtr)
          {
            str3 = \u009D\u0007.\u007E\u001B\u0007((object) type);
            if (obj0 is char)
            {
              int num = (int) (char) obj0;
              StringBuilder stringBuilder1 = new StringBuilder();
              if (num >= 32)
              {
                StringBuilder stringBuilder2 = \u0002\u0004.\u007E\u000E\u0006((object) stringBuilder1, '\'');
                StringBuilder stringBuilder3 = \u0002\u0004.\u007E\u000E\u0006((object) stringBuilder1, (char) obj0);
                StringBuilder stringBuilder4 = \u008C\u0007.\u007E\u0008\u0006((object) stringBuilder1, \u0006.\u0001(3674));
              }
              StringBuilder stringBuilder5 = \u008C\u0007.\u007E\u0008\u0006((object) stringBuilder1, \u0006.\u0001(3679));
              StringBuilder stringBuilder6 = \u008C\u0007.\u007E\u0008\u0006((object) stringBuilder1, num.ToString(\u0006.\u0001(3466)));
              StringBuilder stringBuilder7 = \u0002\u0004.\u007E\u000E\u0006((object) stringBuilder1, ')');
              str4 = \u009D\u0007.\u007E\u001C\u0005((object) stringBuilder1);
            }
            if (obj0 is bool)
              str4 = \u009D\u0007.\u007E\u0098\u0005((object) \u009D\u0007.\u007E\u001C\u0005(obj0));
            if (str4 == null)
            {
              string str5 = string.Empty;
              try
              {
                str5 = this.\u0003(obj0);
              }
              catch
              {
              }
              if (\u0008\u0007.\u007E\u008E\u0005((object) str5) > 0)
              {
                StringBuilder stringBuilder1 = new StringBuilder();
                StringBuilder stringBuilder2 = \u008C\u0007.\u007E\u0008\u0006((object) stringBuilder1, \u009D\u0007.\u007E\u001C\u0005(obj0));
                StringBuilder stringBuilder3 = \u008C\u0007.\u007E\u0008\u0006((object) stringBuilder1, \u0006.\u0001(3684));
                StringBuilder stringBuilder4 = \u008C\u0007.\u007E\u0008\u0006((object) stringBuilder1, str5);
                StringBuilder stringBuilder5 = \u0002\u0004.\u007E\u000E\u0006((object) stringBuilder1, ')');
                str4 = \u009D\u0007.\u007E\u001C\u0005((object) stringBuilder1);
              }
              else
                str4 = \u009D\u0007.\u007E\u001C\u0005(obj0);
            }
          }
          else if (\u0093\u0007.\u007E\u0084\u0007((object) type) && \u0087\u0002.\u007E\u0018\u0007((object) type) != \u0087\u0002.\u007E\u0018\u0007((object) \u0008\u0005.\u001F\u0005((object) this)))
            str3 = \u009D\u0007.\u007E\u001B\u0007((object) type);
        }
        if (str3 != null)
        {
          \u0097\u0005.\u007E\u001E\u000F((object) this.\u0001, str2);
          if (obj1 != null && \u0093\u0007.\u007E\u0007\u000E((object) obj1))
            \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(3604), \u0006.\u0001(3461));
          this.\u0003(type);
          if (str1 != null)
            this.\u0003(str1);
          if (\u0093\u0007.\u007E\u0086\u0007((object) type))
            str4 = \u009D\u0007.\u007E\u001C\u0005(obj0);
          if (obj0 is Guid)
            str4 = \u001C\u0007.\u0003\u0006(\u0006.\u0001(3118), \u009D\u0007.\u007E\u001C\u0005(obj0), \u0006.\u0001(3693));
          if (str4 == null)
            str4 = \u001C\u0007.\u0003\u0006(\u0006.\u0001(3698), \u009D\u0007.\u007E\u001C\u0005(obj0), \u0006.\u0001(3698));
          \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(3703), this.\u0003(str4));
          \u0001\u0007.\u007E\u001F\u000F((object) this.\u0001);
        }
        else
        {
          \u0097\u0005.\u007E\u001E\u000F((object) this.\u0001, str2);
          if (obj1 != null && \u0093\u0007.\u007E\u0007\u000E((object) obj1))
            \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(3604), \u0006.\u0001(3461));
          int num = -1;
          for (int index = 0; index < \u0008\u0007.\u007E\u0006\u0008((object) this.\u0001); ++index)
          {
            try
            {
              if (\u0099\u0005.\u007E\u001D\u0005(\u0097\u0006.\u007E\u0007\u0008((object) this.\u0001, index), obj0))
              {
                num = index;
                break;
              }
            }
            catch
            {
            }
          }
          if (num == -1)
            num = \u001D\u0005.\u007E\u0008\u0008((object) this.\u0001, obj0);
          \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(3712), num.ToString());
          if (str1 != null)
            this.\u0003(str1);
          \u0001\u0007.\u007E\u001F\u000F((object) this.\u0001);
        }
      }
    }

    private void \u0003([In] string obj0)
    {
      int num = this.\u0003(obj0);
      if (num != -1)
        \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(3717), num.ToString());
      else
        \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(3726), obj0);
    }

    private \u0006.\u0003 \u0003([In] Type obj0)
    {
      \u0006.\u0003 obj = \u0006.\u0003.\u0003();
      if (obj0 != null && \u0091\u0002.\u007E\u0082\u0008((object) \u0003\u0006.\u007E\u0019\u0007((object) obj0), \u0006.\u0001(3735)) != null)
      {
        obj.\u0001 = ((\u0008\u0007.\u007E\u0016\u0007((object) obj0) & 16777215) - 1).ToString();
        Assembly assembly = \u0003\u0006.\u007E\u0019\u0007((object) obj0);
        obj.\u0001 = new \u0006.\u0002(\u009C\u0004.\u007E\u009C\u0008((object) \u0087\u0002.\u007E\u0086\u0008((object) assembly)).ToString(\u0006.\u0001(3796)), \u009D\u0007.\u007E\u0081\u0008((object) assembly));
      }
      return obj;
    }

    private int \u0003([In] \u0006.\u0003 obj0)
    {
      string str = \u009D\u0007.\u007E\u0099\u0005((object) obj0.\u0001.\u0001);
      if (\u0099\u0005.\u007E\u0014\u0008((object) this.\u0004, (object) str))
        return (int) \u0095\u0004.\u007E\u0015\u0008((object) this.\u0004, (object) str);
      int num = \u001D\u0005.\u007E\u0008\u0008((object) this.\u0003, (object) obj0.\u0001);
      \u001A\u0005.\u007E\u0012\u0008((object) this.\u0004, (object) str, (object) num);
      return num;
    }

    private void \u0003([In] Type obj0)
    {
      if (obj0 == null)
        return;
      try
      {
        \u0006.\u0003 obj = this.\u0003(obj0);
        if (!obj.\u0003())
        {
          \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(3801), obj.\u0001);
          int num = this.\u0003(obj);
          if (num <= 0)
            return;
          \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(3814), num.ToString());
        }
        else
        {
          string str1 = \u009D\u0007.\u007E\u001B\u0007((object) obj0);
          int num1;
          if (\u0099\u0005.\u007E\u0014\u0008((object) this.\u0003, (object) str1))
          {
            num1 = (int) \u0095\u0004.\u007E\u0015\u0008((object) this.\u0003, (object) str1);
          }
          else
          {
            StringBuilder stringBuilder1 = new StringBuilder();
            string str2 = \u009D\u0007.\u007E\u009A\u0008((object) \u008F\u0006.\u007E\u0080\u0008((object) \u0003\u0006.\u007E\u0019\u0007((object) obj0)));
            if (\u0008\u0007.\u007E\u008E\u0005((object) str2) > 0 && \u0005\u0003.\u008C\u0005(str2, \u0006.\u0001(3827)))
            {
              StringBuilder stringBuilder2 = \u0002\u0004.\u007E\u000E\u0006((object) stringBuilder1, '[');
              StringBuilder stringBuilder3 = \u008C\u0007.\u007E\u0008\u0006((object) stringBuilder1, str2);
              StringBuilder stringBuilder4 = \u0002\u0004.\u007E\u000E\u0006((object) stringBuilder1, ']');
            }
            string str3 = \u009D\u0007.\u007E\u001C\u0007((object) obj0);
            if (\u0008\u0007.\u007E\u008E\u0005((object) str3) > 0)
            {
              StringBuilder stringBuilder2 = \u008C\u0007.\u007E\u0008\u0006((object) stringBuilder1, str3);
              StringBuilder stringBuilder3 = \u0002\u0004.\u007E\u000E\u0006((object) stringBuilder1, '.');
            }
            if (\u0093\u0007.\u007E\u008B\u0007((object) obj0))
              obj0 = \u0008\u0005.\u007E\u008C\u0007((object) obj0);
            int num2 = \u0087\u0004.\u007E\u0096\u0005((object) str1, \u0006.\u0001(3840));
            if (num2 > 0)
            {
              string str4 = \u008A\u0002.\u007E\u009B\u0005((object) \u009A\u0005.\u007E\u0091\u0005((object) str1, \u0008\u0007.\u007E\u008E\u0005((object) str3) + 1, num2 - \u0008\u0007.\u007E\u008E\u0005((object) str3)), \u0006.\u0001(3840), \u0006.\u0001(3845));
              StringBuilder stringBuilder2 = \u008C\u0007.\u007E\u0008\u0006((object) stringBuilder1, str4);
            }
            StringBuilder stringBuilder5 = \u008C\u0007.\u007E\u0008\u0006((object) stringBuilder1, \u009D\u0007.\u007E\u0014\u0007((object) obj0));
            num1 = \u001D\u0005.\u007E\u0008\u0008((object) this.\u0002, (object) \u009D\u0007.\u007E\u001C\u0005((object) stringBuilder1));
            \u001A\u0005.\u007E\u0012\u0008((object) this.\u0003, (object) str1, (object) num1);
          }
          \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(3850), num1.ToString());
        }
      }
      catch
      {
      }
    }

    private int \u0003([In] string obj0)
    {
      try
      {
        bool flag1 = this.\u0003()[0] == '\x0001';
        if (obj0 == null || \u0008\u0007.\u007E\u008E\u0005((object) obj0) == 0 || flag1 && \u0008\u0007.\u007E\u008E\u0005((object) obj0) > 4 || !flag1 && \u0006\u0006.\u007E\u008D\u0005((object) obj0, 0) != '#')
          return -1;
        int num = 0;
        for (int index1 = \u0008\u0007.\u007E\u008E\u0005((object) obj0) - 1; index1 >= 0 && (flag1 || index1 != 0); --index1)
        {
          char ch = \u0006\u0006.\u007E\u008D\u0005((object) obj0, index1);
          bool flag2 = false;
          for (int index2 = 0; index2 < this.\u0003().Length; ++index2)
          {
            if ((int) this.\u0003()[index2] == (int) ch)
            {
              num = num * this.\u0003().Length + index2;
              flag2 = true;
              break;
            }
          }
          if (!flag2)
            return -1;
        }
        return num;
      }
      catch
      {
        return -1;
      }
    }

    protected virtual Guid \u0003()
    {
      return Guid.Empty;
    }

    private string \u0003()
    {
      try
      {
        return \u0083\u0004.\u0017\u0010();
      }
      catch
      {
        return \u0006.\u0001(3863);
      }
    }

    private Assembly[] \u0003()
    {
      try
      {
        return \u0097\u0002.\u007E\u008B\u0006((object) \u0088\u0003.\u008A\u0006());
      }
      catch
      {
        return new Assembly[1]{ this.\u0003() };
      }
    }

    private Assembly \u0003()
    {
      try
      {
        return \u0013\u0007.\u008D\u0008();
      }
      catch
      {
        return (Assembly) null;
      }
    }

    private byte[] \u0003([In] string obj0)
    {
      MemoryStream memoryStream = new MemoryStream();
      this.\u0001 = new XmlTextWriter((Stream) memoryStream, (Encoding) new UTF8Encoding(false));
      \u0001\u0007.\u007E\u001C\u000F((object) this.\u0001);
      \u0097\u0005.\u007E\u001E\u000F((object) this.\u0001, \u0006.\u0001(3868));
      \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(3901), \u009D\u0007.\u007E\u0099\u0005((object) \u0006.\u0001(3918)));
      \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(3971), \u008C\u0002.\u001C\u0006().ToString(\u0006.\u0001(3984)));
      \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(3989), this.\u0003());
      Guid guid = this.\u0003();
      if (\u0010\u0005.\u009F\u0006(guid, Guid.Empty))
        \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(3998), guid.ToString(\u0006.\u0001(3796)));
      if (\u0008\u0007.\u007E\u008E\u0005((object) obj0) > 0)
        \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(4007), obj0);
      if (\u0008\u0007.\u007E\u0006\u0008((object) this.\u0003) > 0)
        \u0001\u0007.\u007E\u000E\u0008((object) this.\u0003);
      int num1 = \u001D\u0005.\u007E\u0008\u0008((object) this.\u0003, (object) new \u0006.\u0002(\u0006.\u0001(3918), string.Empty));
      if (\u0008\u0007.\u007E\u0017\u0008((object) this.\u0004) > 0)
        \u0001\u0007.\u007E\u0013\u0008((object) this.\u0004);
      \u001A\u0005.\u007E\u0012\u0008((object) this.\u0004, (object) \u0006.\u0001(3918), (object) 0);
      \u0097\u0005.\u007E\u001E\u000F((object) this.\u0001, \u0006.\u0001(4020));
      Assembly assembly1 = this.\u0003();
      foreach (Assembly assembly2 in this.\u0003())
      {
        if (assembly2 != null)
        {
          \u0097\u0005.\u007E\u001E\u000F((object) this.\u0001, \u0006.\u0001(3814));
          try
          {
            \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(3726), \u009D\u0007.\u007E\u0081\u0008((object) assembly2));
            \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(4037), \u009D\u0007.\u007E\u007F\u0008((object) assembly2));
            if (assembly2 == assembly1)
              \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(4050), \u0006.\u0001(3461));
          }
          catch
          {
          }
          \u0001\u0007.\u007E\u001F\u000F((object) this.\u0001);
        }
      }
      \u0001\u0007.\u007E\u001F\u000F((object) this.\u0001);
      \u0097\u0005.\u007E\u001E\u000F((object) this.\u0001, \u0006.\u0001(4059));
      if (this.\u0001 != null && \u0008\u0007.\u007E\u0017\u0008((object) this.\u0001) > 0)
      {
        IEnumerator enumerator = \u0090\u0002.\u007E\u007F\u0005((object) \u0084\u0002.\u007E\u0016\u0008((object) this.\u0001));
        try
        {
          while (\u0093\u0007.\u007E\u0087\u0005((object) enumerator))
          {
            string str1 = (string) \u001F\u0006.\u007E\u0088\u0005((object) enumerator);
            \u0097\u0005.\u007E\u001E\u000F((object) this.\u0001, \u0006.\u0001(4084));
            \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(3726), str1);
            string str2 = (string) \u0095\u0004.\u007E\u0015\u0008((object) this.\u0001, (object) str1);
            if (str2 == null)
              \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(3644), \u0006.\u0001(3461));
            else
              \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(3703), \u001C\u0007.\u0003\u0006(\u0006.\u0001(3698), str2, \u0006.\u0001(3698)));
            \u0001\u0007.\u007E\u001F\u000F((object) this.\u0001);
          }
        }
        finally
        {
          IDisposable disposable = enumerator as IDisposable;
          if (disposable != null)
            \u0001\u0007.\u007E\u0089\u0005((object) disposable);
        }
      }
      \u0001\u0007.\u007E\u001F\u000F((object) this.\u0001);
      if (this.\u0002 != null && \u0008\u0007.\u007E\u0017\u0008((object) this.\u0002) > 0)
      {
        \u0097\u0005.\u007E\u001E\u000F((object) this.\u0001, \u0006.\u0001(4105));
        IEnumerator enumerator = \u0090\u0002.\u007E\u007F\u0005((object) \u0084\u0002.\u007E\u0016\u0008((object) this.\u0002));
        try
        {
          while (\u0093\u0007.\u007E\u0087\u0005((object) enumerator))
          {
            string str = (string) \u001F\u0006.\u007E\u0088\u0005((object) enumerator);
            \u0097\u0005.\u007E\u001E\u000F((object) this.\u0001, \u0006.\u0001(4126));
            \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(4143), str);
            \u0006.\u0001 obj = (\u0006.\u0001) \u0095\u0004.\u007E\u0015\u0008((object) this.\u0002, (object) str);
            \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(4148), obj.\u0001);
            \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(4161), obj.\u0001.ToString());
            if (\u0008\u0007.\u007E\u008E\u0005((object) obj.\u0003) > 0)
              \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(4170), obj.\u0003);
            else
              \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(4179), obj.\u0002);
            \u0001\u0007.\u007E\u001F\u000F((object) this.\u0001);
          }
        }
        finally
        {
          IDisposable disposable = enumerator as IDisposable;
          if (disposable != null)
            \u0001\u0007.\u007E\u0089\u0005((object) disposable);
        }
        \u0001\u0007.\u007E\u001F\u000F((object) this.\u0001);
      }
      \u0097\u0005.\u007E\u001E\u000F((object) this.\u0001, \u0006.\u0001(4188));
      try
      {
        \u009C\u0006.\u007E\u0083\u000F((object) this.\u0001, \u0006.\u0001(4213), \u009D\u0007.\u007E\u001C\u0005((object) \u0002\u0005.\u007E\u0013\u0007((object) \u0003\u0007.\u009B\u0006())));
        \u009C\u0006.\u007E\u0083\u000F((object) this.\u0001, \u0006.\u0001(4226), \u009D\u0007.\u007E\u001C\u0005((object) \u0091\u0003.\u007E\u0012\u0007((object) \u0003\u0007.\u009B\u0006())));
        \u009C\u0006.\u007E\u0083\u000F((object) this.\u0001, \u0006.\u0001(4243), \u0005.\u0003.\u0003());
      }
      catch
      {
      }
      \u0001\u0007.\u007E\u001F\u000F((object) this.\u0001);
      ArrayList arrayList = new ArrayList();
      for (Exception exception = this.\u0001; exception != null; exception = \u0003\u0008.\u007E\u0014\u0006((object) exception))
      {
        Type type = \u0008\u0005.\u007E\u0017\u0006((object) exception);
        if (\u0005\u0003.\u008B\u0005(\u009D\u0007.\u007E\u0014\u0007((object) type), \u0006.\u0001(4260)) && \u0005\u0003.\u008B\u0005(\u009D\u0007.\u007E\u001C\u0007((object) type), \u0006.\u0001(4285)))
        {
          int num2 = \u001D\u0005.\u007E\u0008\u0008((object) arrayList, (object) exception);
        }
        else
          \u0098\u0002.\u007E\u0010\u0008((object) arrayList, 0, (object) exception);
      }
      \u0097\u0005.\u007E\u001E\u000F((object) this.\u0001, \u0006.\u0001(4330));
      int num3 = \u0008\u0007.\u007E\u0006\u0008((object) arrayList);
      int num4 = 0;
      IEnumerator enumerator1 = \u0090\u0002.\u007E\u000F\u0008((object) arrayList);
      try
      {
        while (\u0093\u0007.\u007E\u0087\u0005((object) enumerator1))
        {
          Exception exception = (Exception) \u001F\u0006.\u007E\u0088\u0005((object) enumerator1);
          ++num4;
          if (num4 > 100 && num4 == num3 - 100)
          {
            \u0097\u0005.\u007E\u001E\u000F((object) this.\u0001, \u0006.\u0001(4347));
            \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(4368), num3.ToString());
            \u0001\u0007.\u007E\u001F\u000F((object) this.\u0001);
          }
          else if (num4 <= 100 || num4 > num3 - 100)
          {
            Type type = \u0008\u0005.\u007E\u0017\u0006((object) exception);
            if (\u0005\u0003.\u008B\u0005(\u009D\u0007.\u007E\u0014\u0007((object) type), \u0006.\u0001(4260)) && \u0005\u0003.\u008B\u0005(\u009D\u0007.\u007E\u001C\u0007((object) type), \u0006.\u0001(4285)))
            {
              int num2 = 0;
              int num5 = -1;
              object[] objArray = (object[]) null;
              \u0006.\u0003 obj1 = \u0006.\u0003.\u0003();
              bool flag = true;
              try
              {
                num2 = (int) \u0095\u0004.\u007E\u0005\u000E((object) \u0087\u0003.\u007E\u0081\u0007((object) type, \u0006.\u0001(4393)), (object) exception);
                num5 = (int) \u0095\u0004.\u007E\u0005\u000E((object) \u0087\u0003.\u007E\u0081\u0007((object) type, \u0006.\u0001(4406)), (object) exception);
                objArray = (object[]) \u0095\u0004.\u007E\u0005\u000E((object) \u0087\u0003.\u007E\u0081\u0007((object) type, \u0006.\u0001(4419)), (object) exception);
                obj1 = this.\u0003(type);
              }
              catch
              {
                flag = false;
              }
              if (flag && !obj1.\u0003())
              {
                \u0097\u0005.\u007E\u001E\u000F((object) this.\u0001, \u0006.\u0001(4432));
                \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(4393), num2.ToString());
                int num6 = this.\u0003(obj1);
                if (num6 > 0)
                  \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(3814), num6.ToString());
                if (num5 != -1)
                  \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(4406), num5.ToString());
                foreach (object obj2 in objArray)
                {
                  try
                  {
                    this.\u0003(obj2, (FieldInfo) null);
                  }
                  catch
                  {
                  }
                }
                \u0001\u0007.\u007E\u001F\u000F((object) this.\u0001);
              }
            }
            else
            {
              \u0097\u0005.\u007E\u001E\u000F((object) this.\u0001, \u0006.\u0001(4449));
              try
              {
                this.\u0003(\u0008\u0005.\u007E\u0017\u0006((object) exception));
                string str1 = \u0006.\u0001(3863);
                try
                {
                  str1 = \u009D\u0007.\u007E\u0013\u0006((object) exception);
                }
                catch
                {
                }
                \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(4462), str1);
                string str2 = \u009D\u0007.\u007E\u009A\u0005((object) \u009D\u0007.\u007E\u0015\u0006((object) exception));
                \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(4475), str2);
                int num2 = \u0016\u0006.\u007E\u0094\u0005((object) str2, ' ');
                string str3 = \u008D\u0002.\u007E\u0090\u0005((object) str2, num2 + 1);
                int num5 = \u0087\u0004.\u007E\u0095\u0005((object) str3, \u0006.\u0001(4504));
                if (num5 != -1)
                  str3 = \u009A\u0005.\u007E\u0091\u0005((object) str3, 0, num5);
                \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(4509), str3);
              }
              catch
              {
              }
              \u0001\u0007.\u007E\u001F\u000F((object) this.\u0001);
            }
          }
        }
      }
      finally
      {
        IDisposable disposable = enumerator1 as IDisposable;
        if (disposable != null)
          \u0001\u0007.\u007E\u0089\u0005((object) disposable);
      }
      \u0001\u0007.\u007E\u001F\u000F((object) this.\u0001);
      \u0097\u0005.\u007E\u001E\u000F((object) this.\u0001, \u0006.\u0001(4419));
      int num7 = \u0008\u0007.\u007E\u0006\u0008((object) this.\u0001);
      for (int index1 = 0; index1 < \u0008\u0007.\u007E\u0006\u0008((object) this.\u0001); ++index1)
      {
        object obj1 = \u0097\u0006.\u007E\u0007\u0008((object) this.\u0001, index1);
        Type type = \u0008\u0005.\u007E\u001F\u0005(obj1);
        \u0097\u0005.\u007E\u001E\u000F((object) this.\u0001, \u0006.\u0001(4518));
        \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(3712), index1.ToString());
        string str = (string) null;
        try
        {
          str = \u009D\u0007.\u007E\u001C\u0005(obj1);
          str = !\u0005\u0003.\u008B\u0005(str, \u009D\u0007.\u007E\u001B\u0007((object) type)) ? (!\u0093\u0007.\u007E\u0086\u0007((object) type) ? (!(obj1 is Guid) ? \u001C\u0007.\u0003\u0006(\u0006.\u0001(3698), str, \u0006.\u0001(3698)) : \u001C\u0007.\u0003\u0006(\u0006.\u0001(3118), str, \u0006.\u0001(3693))) : \u008D\u0004.\u008A\u0005(type, obj1, \u0006.\u0001(4531))) : (string) null;
        }
        catch
        {
        }
        if (str != null)
          \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(3703), this.\u0003(str));
        if (\u0093\u0007.\u007E\u008B\u0007((object) type))
        {
          this.\u0003(\u0008\u0005.\u007E\u008C\u0007((object) type));
          if (\u0093\u0007.\u007E\u0088\u0007((object) type))
            \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(3613), \u0006.\u0001(3461));
          if (\u0093\u0007.\u007E\u0089\u0007((object) type))
            \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(3622), \u0006.\u0001(3461));
          if (\u0093\u0007.\u007E\u0087\u0007((object) type))
          {
            Array array = (Array) obj1;
            \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(3635), \u0008\u0007.\u007E\u0086\u0005((object) array).ToString());
            StringBuilder stringBuilder1 = new StringBuilder();
            for (int index2 = 0; index2 < \u0008\u0007.\u007E\u0086\u0005((object) array); ++index2)
            {
              if (index2 > 0)
              {
                StringBuilder stringBuilder2 = \u0002\u0004.\u007E\u000E\u0006((object) stringBuilder1, ',');
              }
              StringBuilder stringBuilder3 = \u0094\u0004.\u007E\u000F\u0006((object) stringBuilder1, \u008A\u0007.\u007E\u0084\u0005((object) array, index2));
            }
            \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(4161), \u009D\u0007.\u007E\u001C\u0005((object) stringBuilder1));
            if (\u0008\u0007.\u007E\u0086\u0005((object) array) == 1)
            {
              int num2 = \u0008\u0007.\u007E\u0083\u0005((object) array);
              for (int index2 = 0; index2 < num2; ++index2)
              {
                if (index2 == 10)
                {
                  if (num2 > 16)
                    index2 = num2 - 5;
                }
                try
                {
                  this.\u0003(\u0097\u0006.\u007E\u0082\u0005((object) array, index2), (FieldInfo) null);
                }
                catch
                {
                }
              }
            }
          }
        }
        else
        {
          this.\u0003(type);
          if (index1 < num7)
          {
            if (obj1 is IEnumerable)
            {
              \u0097\u0005.\u007E\u001E\u000F((object) this.\u0001, \u0006.\u0001(4536));
              try
              {
                int num2 = 0;
                IEnumerator enumerator2 = \u0090\u0002.\u007E\u007F\u0005((object) (IEnumerable) obj1);
                try
                {
                  while (\u0093\u0007.\u007E\u0087\u0005((object) enumerator2))
                  {
                    object obj2 = \u001F\u0006.\u007E\u0088\u0005((object) enumerator2);
                    if (num2 > 20)
                    {
                      \u009C\u0006.\u007E\u0083\u000F((object) this.\u0001, \u0006.\u0001(4553), string.Empty);
                      break;
                    }
                    this.\u0003(obj2, (FieldInfo) null);
                    ++num2;
                  }
                }
                finally
                {
                  IDisposable disposable = enumerator2 as IDisposable;
                  if (disposable != null)
                    \u0001\u0007.\u007E\u0089\u0005((object) disposable);
                }
              }
              catch
              {
              }
              \u0001\u0007.\u007E\u001F\u000F((object) this.\u0001);
            }
            bool flag = true;
            while (type != null)
            {
              if (!flag)
              {
                \u0097\u0005.\u007E\u001E\u000F((object) this.\u0001, \u0006.\u0001(4562));
                this.\u0003(type);
                \u0001\u0007.\u007E\u001F\u000F((object) this.\u0001);
              }
              FieldInfo[] fieldInfoArray = \u0008\u0006.\u007E\u0082\u0007((object) type, BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
              if (fieldInfoArray.Length > 0)
              {
                for (int index2 = 0; index2 < fieldInfoArray.Length; ++index2)
                {
                  try
                  {
                    if (!\u0093\u0007.\u007E\u000E\u000E((object) fieldInfoArray[index2]))
                    {
                      if (\u0093\u0007.\u007E\u0007\u000E((object) fieldInfoArray[index2]))
                      {
                        if (\u0093\u0007.\u007E\u0008\u000E((object) fieldInfoArray[index2]))
                          continue;
                      }
                      this.\u0003(\u0095\u0004.\u007E\u0005\u000E((object) fieldInfoArray[index2], obj1), fieldInfoArray[index2]);
                    }
                  }
                  catch
                  {
                  }
                }
              }
              type = \u0008\u0005.\u007E\u001E\u0007((object) type);
              flag = false;
              if (this.\u0003(type).\u0003())
                break;
            }
          }
        }
        \u0001\u0007.\u007E\u001F\u000F((object) this.\u0001);
      }
      \u0001\u0007.\u007E\u001F\u000F((object) this.\u0001);
      \u0097\u0005.\u007E\u001E\u000F((object) this.\u0001, \u0006.\u0001(4579));
      \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(4592), \u0008\u0007.\u007E\u0006\u0008((object) this.\u0002).ToString());
      for (int index = 0; index < \u0008\u0007.\u007E\u0006\u0008((object) this.\u0002); ++index)
      {
        string empty = string.Empty;
        string str;
        try
        {
          str = \u009D\u0007.\u007E\u001C\u0005(\u0097\u0006.\u007E\u0007\u0008((object) this.\u0002, index));
        }
        catch (Exception ex)
        {
          str = \u0099\u0006.\u009F\u0005((object) '"', (object) \u009D\u0007.\u007E\u0013\u0006((object) ex), (object) '"');
        }
        \u009C\u0006.\u007E\u0083\u000F((object) this.\u0001, \u0006.\u0001(3850), str);
      }
      \u0001\u0007.\u007E\u001F\u000F((object) this.\u0001);
      \u0097\u0005.\u007E\u001E\u000F((object) this.\u0001, \u0006.\u0001(4601));
      \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(4592), \u0008\u0007.\u007E\u0006\u0008((object) this.\u0003).ToString());
      for (int index = 0; index < \u0008\u0007.\u007E\u0006\u0008((object) this.\u0003); ++index)
      {
        \u0097\u0005.\u007E\u001E\u000F((object) this.\u0001, \u0006.\u0001(3901));
        \u0006.\u0002 obj = (\u0006.\u0002) \u0097\u0006.\u007E\u0007\u0008((object) this.\u0003, index);
        \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(3712), obj.\u0001);
        if (\u0008\u0007.\u007E\u008E\u0005((object) obj.\u0002) > 0)
          \u009C\u0006.\u007E\u007F\u000F((object) this.\u0001, \u0006.\u0001(4618), obj.\u0002);
        \u0001\u0007.\u007E\u001F\u000F((object) this.\u0001);
      }
      \u0001\u0007.\u007E\u001F\u000F((object) this.\u0001);
      \u0001\u0007.\u007E\u001F\u000F((object) this.\u0001);
      \u0001\u0007.\u007E\u001D\u000F((object) this.\u0001);
      \u0001\u0007.\u007E\u0082\u000F((object) this.\u0001);
      \u0001\u0007.\u007E\u0088\u000E((object) memoryStream);
      return \u0088\u0002.\u007E\u0095\u000E((object) memoryStream);
    }

    internal bool \u0003([In] string obj0)
    {
      try
      {
        byte[] numArray1 = this.\u0003(\u0089\u0002.\u0001\u0007().ToString(\u0006.\u0001(3796)));
        FileStream fileStream = \u0082\u0003.\u0092\u000E(obj0);
        byte[] numArray2 = \u0003.\u0005.\u0003(\u0006.\u0004(numArray1), \u0006.\u0001(4631));
        byte[] numArray3 = \u0003\u0003.\u007E\u0015\u000E((object) \u0096\u0003.\u0014\u000E(), \u0006.\u0001(3918));
        \u008D\u0007.\u007E\u008C\u000E((object) fileStream, numArray3, 0, numArray3.Length);
        \u008D\u0007.\u007E\u008C\u000E((object) fileStream, numArray2, 0, numArray2.Length);
        \u0001\u0007.\u007E\u0087\u000E((object) fileStream);
        return true;
      }
      catch (ThreadAbortException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        return false;
      }
    }

    internal void \u0003()
    {
      try
      {
        string str1 = \u0083\u0004.\u0098\u000E();
        this.\u0003(str1);
        string str2 = this.\u0004(\u0006.\u0001(3989));
        if (\u0008\u0007.\u007E\u008E\u0005((object) str2) > 0 && !\u0094\u0003.\u007E\u0093\u0005((object) str2, \u0006.\u0001(4957)))
          str2 = \u0014\u0003.\u0002\u0006(str2, \u0006.\u0001(4957));
        Process process = \u008F\u0004.\u0096\u0013(\u0014\u0003.\u0002\u0006(str2, \u0006.\u0001(4962)), \u001C\u0007.\u0003\u0006(\u0006.\u0001(4991), str1, \u0006.\u0001(3698)));
        if (this.\u0001 == null)
          return;
        \u0017\u0007.\u007E\u009D\u0006((object) this.\u0001, (object) this, EventArgs.Empty);
      }
      catch (ThreadAbortException ex)
      {
      }
      catch (Exception ex)
      {
        this.\u0003(new \u0004.\u0005(ex));
      }
    }

    internal bool \u0003()
    {
      try
      {
        if (this.\u0001 != null)
          goto label_35;
label_1:
        byte[] numArray1;
        try
        {
          numArray1 = this.\u0003(string.Empty);
        }
        catch (Exception ex)
        {
          int num = -1;
          try
          {
            StackTrace stackTrace = new StackTrace(ex);
            if (\u0008\u0007.\u007E\u001C\u0008((object) stackTrace) > 0)
            {
              StackFrame stackFrame = \u000F\u0005.\u007E\u001D\u0008((object) stackTrace, \u0008\u0007.\u007E\u001C\u0008((object) stackTrace) - 1);
              num = \u0008\u0007.\u007E\u001F\u0008((object) stackFrame);
            }
          }
          catch
          {
          }
          if (this.\u0001 != null)
            this.\u0001((object) this, new \u0004.\u0003(\u0003.\u0002.\u0001, \u001B\u0007.\u009E\u0005(\u0006.\u0001(5020), (object) \u009D\u0007.\u007E\u0013\u0006((object) ex), (object) num)));
          return false;
        }
        byte[] numArray2 = \u0006.\u0004(numArray1);
        if (numArray2 == null)
        {
          if (this.\u0001 != null)
            this.\u0001((object) this, new \u0004.\u0003(\u0003.\u0002.\u0001, \u0006.\u0001));
          return false;
        }
        byte[] numArray3 = \u0003.\u0005.\u0003(numArray2, \u0006.\u0001(4631));
        if (numArray3 == null)
        {
          if (this.\u0001 != null)
            this.\u0001((object) this, new \u0004.\u0003(\u0003.\u0002.\u0001, \u0003.\u0005.\u0001));
          return false;
        }
        if (this.\u0001 != null)
          this.\u0001((object) this, new \u0004.\u0003(\u0003.\u0002.\u0002));
        \u0006 obj = new \u0006(\u0006.\u0001(5053));
        if (this.\u0001 != null)
          obj.\u0003(this.\u0001);
        string str1 = obj.\u0003();
        if (\u0005\u0003.\u008B\u0005(str1, \u0006.\u0001(5106)))
        {
          if (this.\u0001 != null)
            this.\u0001((object) this, new \u0004.\u0003(\u0003.\u0002.\u0003));
          byte[] numArray4 = \u0003\u0003.\u007E\u0015\u000E((object) \u0096\u0003.\u0014\u000E(), \u0006.\u0001(3918));
          byte[] numArray5 = new byte[numArray4.Length + numArray3.Length];
          \u0083\u0005.\u0080\u0005((Array) numArray4, (Array) numArray5, numArray4.Length);
          \u001E\u0006.\u0081\u0005((Array) numArray3, 0, (Array) numArray5, numArray4.Length, numArray3.Length);
          string str2 = obj.\u0003(numArray5);
          if (\u0094\u0003.\u007E\u0097\u0005((object) str2, \u0006.\u0001(5111)))
          {
            if (this.\u0001 != null)
              this.\u0001((object) this, new \u0004.\u0003(\u0003.\u0002.\u0003, str2));
            return false;
          }
          if (this.\u0001 != null)
          {
            \u0004.\u0003 e = new \u0004.\u0003(\u0003.\u0002.\u0004);
            e.\u0003(str2);
            this.\u0001((object) this, e);
          }
          return true;
        }
        string str3 = str1;
        if (this.\u0001 != null)
          this.\u0001((object) this, new \u0004.\u0003(\u0003.\u0002.\u0002, str3));
        return false;
label_35:
        this.\u0001((object) this, new \u0004.\u0003(\u0003.\u0002.\u0001));
        goto label_1;
      }
      catch (ThreadAbortException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.\u0003(new \u0004.\u0005(ex));
        return false;
      }
    }

    private string \u0004([In] string obj0)
    {
      try
      {
        RegistryKey registryKey = \u0081\u0002.\u007E\u001E\u000E((object) Registry.LocalMachine, \u0006.\u0001(5116)) ?? \u0081\u0002.\u007E\u001E\u000E((object) Registry.LocalMachine, \u0006.\u0001(5149));
        if (registryKey == null)
          return string.Empty;
        string str = (string) \u001F\u0002.\u007E\u001F\u000E((object) registryKey, obj0, (object) string.Empty);
        \u0001\u0007.\u007E\u001D\u000E((object) registryKey);
        return str;
      }
      catch
      {
        return string.Empty;
      }
    }

    internal bool \u0003([In] SecurityException obj0)
    {
      \u0004.\u0004 obj = new \u0004.\u0004(obj0);
      this.\u0003(obj);
      if (obj.\u0005())
        return false;
      if (!obj.\u0004())
        \u0086\u0006.\u001C\u0010();
      return true;
    }

    internal string \u0004()
    {
      string str1 = Guid.Empty.ToString(\u0006.\u0001(3796));
      try
      {
        string str2 = this.\u0004(\u0006.\u0001(3989));
        if (\u0008\u0007.\u007E\u008E\u0005((object) str2) > 0 && !\u0094\u0003.\u007E\u0093\u0005((object) str2, \u0006.\u0001(4957)))
          str2 = \u0014\u0003.\u0002\u0006(str2, \u0006.\u0001(4957));
        string url = \u0014\u0003.\u0002\u0006(str2, \u0006.\u0001(5198));
        if (\u0091\u0006.\u0091\u000E(url))
        {
          XmlTextReader xmlTextReader = new XmlTextReader(url);
          if (\u0093\u0007.\u007E\u0086\u000F((object) xmlTextReader))
            str1 = \u009C\u0003.\u007E\u0084\u000F((object) xmlTextReader, \u0006.\u0001(5235));
          \u0001\u0007.\u007E\u0087\u000F((object) xmlTextReader);
        }
      }
      catch
      {
      }
      return str1;
    }

    internal void \u0003([In] Exception obj0, [In] bool obj1)
    {
      if (obj0 == null || obj0 is ThreadAbortException)
        return;
      \u0006.\u0001 = true;
      bool flag = true;
      try
      {
        this.\u0001 = obj0;
        this.\u0001 = (Hashtable) null;
        this.\u0002 = (Hashtable) null;
        \u0005.\u0002 obj2 = new \u0005.\u0002(this, obj0);
        if (\u0005\u0003.\u008B\u0005(\u009D\u0007.\u007E\u0098\u0005((object) \u0006.\u0001(5053)), \u009D\u0007.\u007E\u0098\u0005((object) this.\u0004())))
          obj2.\u0003();
        \u0005\u0003 obj3 = \u0005\u0003.\u008B\u0005;
        string str1 = \u009D\u0007.\u007E\u0098\u0005((object) \u0006.\u0001(5053));
        Guid empty = Guid.Empty;
        string str2 = \u009D\u0007.\u007E\u0098\u0005((object) empty.ToString(\u0006.\u0001(3796)));
        if (obj3(str1, str2))
          obj2.\u0004();
        if (!obj1)
          obj2.\u0005();
        this.\u0003(obj2);
        flag = !obj2.\u0006();
      }
      catch (ThreadAbortException ex)
      {
      }
      catch (Exception ex)
      {
        this.\u0003(new \u0004.\u0005(ex));
      }
      this.\u0001 = (Exception) null;
      this.\u0001 = (Hashtable) null;
      this.\u0002 = (Hashtable) null;
      \u0006.\u0001 = false;
      if (!flag)
        return;
      foreach (Assembly assembly in \u0097\u0002.\u007E\u008B\u0006((object) \u0088\u0003.\u008A\u0006()))
      {
        try
        {
          string str = \u009D\u0007.\u007E\u0081\u0008((object) assembly);
          if (\u0094\u0003.\u007E\u0093\u0005((object) str, \u0006.\u0001(5248)))
          {
            if (\u0094\u0003.\u007E\u0097\u0005((object) str, \u0006.\u0001(5273)))
            {
              object obj2 = \u001E\u0007.\u007E\u0001\u000E((object) \u0087\u0005.\u007E\u000F\u000E((object) \u000E\u0006.\u007E\u0083\u0007((object) \u0091\u0002.\u007E\u0082\u0008((object) assembly, \u0006.\u0001(5306)), \u0006.\u0001(5343))), (object) null, (object[]) null);
              object obj3 = \u001E\u0007.\u007E\u0001\u000E((object) \u0003\u0005.\u007E\u001F\u0007((object) \u0008\u0005.\u007E\u001F\u0005(obj2), \u0006.\u0001(5356), new Type[0]), obj2, (object[]) null);
            }
          }
        }
        catch
        {
        }
      }
      \u0086\u0006.\u001C\u0010();
      try
      {
        \u0092\u0007.\u009A\u0006(0);
      }
      catch
      {
      }
    }

    static \u0006()
    {
      \u0001.\u0003.\u0003();
      \u0006.\u0001 = \u0006.\u0001(3461);
      \u0006.\u0001 = false;
      \u0006.\u0001 = (\u0006) null;
    }

    private struct \u0001
    {
      public string \u0001;
      public string \u0002;
      public string \u0003;
      public int \u0001;
    }

    private struct \u0002
    {
      public string \u0001;
      public string \u0002;

      public \u0002([In] string obj0, [In] string obj1)
      {
        this.\u0001 = obj0;
        this.\u0002 = obj1;
      }
    }

    private struct \u0003
    {
      public string \u0001;
      public \u0006.\u0002 \u0001;

      [SpecialName]
      public bool \u0003()
      {
        return \u0008\u0007.\u007E\u008E\u0005((object) this.\u0001) == 0;
      }

      [SpecialName]
      public static \u0006.\u0003 \u0003()
      {
        return new \u0006.\u0003(string.Empty, string.Empty, string.Empty);
      }

      public \u0003([In] string obj0, [In] string obj1, [In] string obj2)
      {
        this.\u0001 = obj0;
        this.\u0001 = new \u0006.\u0002(obj1, obj2);
      }
    }
  }
}
