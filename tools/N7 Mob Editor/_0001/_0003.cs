﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: MobEditor, Version=1.1.0.2, Culture=neutral, PublicKeyToken=016197d45384ac33
// MVID: 35831B81-08C7-444B-81F1-3EE10E122F41
// Assembly location: D:\Server\eab\Tools\N7 Mob Editor.exe

using \u0001;
using SmartAssembly.SmartExceptionsCore;
using System;
using System.Diagnostics;
using System.Reflection;
using System.Reflection.Emit;

namespace \u0001
{
  internal static class \u0003
  {
    public static void \u0003()
    {
      StackFrame frame;
      Type declaringType;
      FieldInfo[] fields;
      int index1;
      FieldInfo fieldInfo;
      Type[] typeArray;
      DynamicMethod dynamicMethod;
      ILGenerator ilGenerator;
      MethodInfo[] methods;
      int index2;
      MethodInfo meth;
      StackTrace stackTrace;
      try
      {
        stackTrace = new StackTrace();
        frame = stackTrace.GetFrame(1);
        declaringType = frame.GetMethod().DeclaringType;
        fields = declaringType.GetFields(BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.GetField);
        for (index1 = 0; index1 < fields.Length; ++index1)
        {
          fieldInfo = fields[index1];
          try
          {
            if (fieldInfo.FieldType == typeof (\u0002))
            {
              string empty = string.Empty;
              Type returnType = typeof (string);
              typeArray = new Type[1]{ typeof (int) };
              Type[] parameterTypes = typeArray;
              Type owner = declaringType;
              int num = 1;
              dynamicMethod = new DynamicMethod(empty, returnType, parameterTypes, owner, num != 0);
              ilGenerator = dynamicMethod.GetILGenerator();
              ilGenerator.Emit(OpCodes.Ldarg_0);
              methods = typeof (\u0006).GetMethods(BindingFlags.Static | BindingFlags.Public);
              for (index2 = 0; index2 < methods.Length; ++index2)
              {
                meth = methods[index2];
                if (meth.ReturnType == typeof (string))
                {
                  ilGenerator.Emit(OpCodes.Ldc_I4, fieldInfo.MetadataToken & 16777215);
                  ilGenerator.Emit(OpCodes.Sub);
                  ilGenerator.Emit(OpCodes.Call, meth);
                  break;
                }
              }
              ilGenerator.Emit(OpCodes.Ret);
              fieldInfo.SetValue((object) null, (object) (\u0002) dynamicMethod.CreateDelegate(typeof (\u0002)));
              break;
            }
          }
          catch
          {
          }
        }
      }
      catch (Exception ex)
      {
        object[] objArray = new object[12]
        {
          (object) stackTrace,
          (object) frame,
          (object) declaringType,
          (object) fieldInfo,
          (object) dynamicMethod,
          (object) ilGenerator,
          (object) meth,
          (object) fields,
          (object) index1,
          (object) typeArray,
          (object) methods,
          (object) index2
        };
        throw UnhandledException.\u0003(ex, objArray);
      }
    }
  }
}
