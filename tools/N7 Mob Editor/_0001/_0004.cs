﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: MobEditor, Version=1.1.0.2, Culture=neutral, PublicKeyToken=016197d45384ac33
// MVID: 35831B81-08C7-444B-81F1-3EE10E122F41
// Assembly location: D:\Server\eab\Tools\N7 Mob Editor.exe

using \u0001;
using SmartAssembly.SmartExceptionsCore;
using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.InteropServices;

namespace \u0001
{
  internal static class \u0004
  {
    private static ModuleHandle \u0001;
    private static char[] \u0001;

    public static void \u0003([In] int obj0)
    {
      Type typeFromHandle;
      FieldInfo[] fields;
      int index1;
      FieldInfo fieldInfo;
      string name;
      bool flag;
      int num1;
      int index2;
      char ch;
      int index3;
      MethodInfo methodFromHandle;
      Delegate @delegate;
      ParameterInfo[] parameters;
      int length;
      Type[] parameterTypes;
      int index4;
      DynamicMethod dynamicMethod;
      ILGenerator ilGenerator;
      int num2;
      try
      {
        try
        {
          typeFromHandle = Type.GetTypeFromHandle(\u0004.\u0001.ResolveTypeHandle(33554433 + obj0));
        }
        catch
        {
          return;
        }
        fields = typeFromHandle.GetFields(BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.GetField);
        for (index1 = 0; index1 < fields.Length; ++index1)
        {
          fieldInfo = fields[index1];
          name = fieldInfo.Name;
          flag = false;
          num1 = 0;
          for (index2 = name.Length - 1; index2 >= 0; --index2)
          {
            ch = name[index2];
            if (ch == '~')
            {
              flag = true;
              break;
            }
            for (index3 = 0; index3 < 58; ++index3)
            {
              if ((int) \u0004.\u0001[index3] == (int) ch)
              {
                num1 = num1 * 58 + index3;
                break;
              }
            }
          }
          try
          {
            methodFromHandle = (MethodInfo) MethodBase.GetMethodFromHandle(\u0004.\u0001.ResolveMethodHandle(num1 + 167772161));
          }
          catch
          {
            continue;
          }
          if (methodFromHandle.IsStatic)
          {
            try
            {
              @delegate = Delegate.CreateDelegate(fieldInfo.FieldType, methodFromHandle);
            }
            catch (Exception ex)
            {
              continue;
            }
          }
          else
          {
            parameters = methodFromHandle.GetParameters();
            length = parameters.Length + 1;
            parameterTypes = new Type[length];
            parameterTypes[0] = typeof (object);
            for (index4 = 1; index4 < length; ++index4)
              parameterTypes[index4] = parameters[index4 - 1].ParameterType;
            dynamicMethod = new DynamicMethod(string.Empty, methodFromHandle.ReturnType, parameterTypes, typeFromHandle, true);
            ilGenerator = dynamicMethod.GetILGenerator();
            ilGenerator.Emit(OpCodes.Ldarg_0);
            if (length > 1)
              ilGenerator.Emit(OpCodes.Ldarg_1);
            if (length > 2)
              ilGenerator.Emit(OpCodes.Ldarg_2);
            if (length > 3)
              ilGenerator.Emit(OpCodes.Ldarg_3);
            if (length > 4)
            {
              for (num2 = 4; num2 < length; ++num2)
                ilGenerator.Emit(OpCodes.Ldarg_S, num2);
            }
            ilGenerator.Emit(flag ? OpCodes.Callvirt : OpCodes.Call, methodFromHandle);
            ilGenerator.Emit(OpCodes.Ret);
            try
            {
              @delegate = dynamicMethod.CreateDelegate(typeFromHandle);
            }
            catch
            {
              continue;
            }
          }
          try
          {
            fieldInfo.SetValue((object) null, (object) @delegate);
          }
          catch
          {
          }
        }
      }
      catch (Exception ex)
      {
        object[] objArray = new object[20]
        {
          (object) typeFromHandle,
          (object) fieldInfo,
          (object) name,
          (object) flag,
          (object) num1,
          (object) index2,
          (object) ch,
          (object) index3,
          (object) methodFromHandle,
          (object) @delegate,
          (object) parameters,
          (object) length,
          (object) parameterTypes,
          (object) index4,
          (object) dynamicMethod,
          (object) ilGenerator,
          (object) num2,
          (object) fields,
          (object) index1,
          (object) obj0
        };
        throw UnhandledException.\u0003(ex, objArray);
      }
    }

    static \u0004()
    {
      Type type1;
      try
      {
        \u0004.\u0001 = new char[58]
        {
          '\x0001',
          '\x0002',
          '\x0003',
          '\x0004',
          '\x0005',
          '\x0006',
          '\a',
          '\b',
          '\x000E',
          '\x000F',
          '\x0010',
          '\x0011',
          '\x0012',
          '\x0013',
          '\x0014',
          '\x0015',
          '\x0016',
          '\x0017',
          '\x0018',
          '\x0019',
          '\x001A',
          '\x001B',
          '\x001C',
          '\x001D',
          '\x001E',
          '\x001F',
          '\x007F',
          '\x0080',
          '\x0081',
          '\x0082',
          '\x0083',
          '\x0084',
          '\x0086',
          '\x0087',
          '\x0088',
          '\x0089',
          '\x008A',
          '\x008B',
          '\x008C',
          '\x008D',
          '\x008E',
          '\x008F',
          '\x0090',
          '\x0091',
          '\x0092',
          '\x0093',
          '\x0094',
          '\x0095',
          '\x0096',
          '\x0097',
          '\x0098',
          '\x0099',
          '\x009A',
          '\x009B',
          '\x009C',
          '\x009D',
          '\x009E',
          '\x009F'
        };
        type1 = typeof (MulticastDelegate);
        if (type1 == null)
          return;
        \u0004.\u0001 = Assembly.GetExecutingAssembly().GetModules()[0].ModuleHandle;
      }
      catch (Exception ex)
      {
        Type type2 = type1;
        throw UnhandledException.\u0003(ex, (object) type2);
      }
    }
  }
}
