﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: MobEditor, Version=1.1.0.2, Culture=neutral, PublicKeyToken=016197d45384ac33
// MVID: 35831B81-08C7-444B-81F1-3EE10E122F41
// Assembly location: D:\Server\eab\Tools\N7 Mob Editor.exe

using \u0001;
using \u0008;
using MySql.Data.MySqlClient;
using SmartAssembly.SmartExceptionsCore;
using System;
using System.Collections;
using System.Data;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace \u0008
{
  internal static class \u0002
  {
    [NonSerialized]
    internal static \u0002 \u0001;
    private static MySqlConnection \u0001;
    private static Timer \u0001;
    private static string \u0001;
    private static string \u0002;
    private static string \u0003;
    private static int \u0001;
    private static bool \u0001;

    [SpecialName]
    public static string \u0003()
    {
      try
      {
        return \u0002.\u0001;
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex);
      }
    }

    [SpecialName]
    public static void \u0003([In] string obj0)
    {
      try
      {
        \u0002.\u0001 = obj0;
      }
      catch (Exception ex)
      {
        string str = obj0;
        throw UnhandledException.\u0003(ex, (object) str);
      }
    }

    [SpecialName]
    public static string \u0004()
    {
      try
      {
        return \u0002.\u0002;
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex);
      }
    }

    [SpecialName]
    public static void \u0004([In] string obj0)
    {
      try
      {
        \u0002.\u0002 = obj0;
      }
      catch (Exception ex)
      {
        string str = obj0;
        throw UnhandledException.\u0003(ex, (object) str);
      }
    }

    [SpecialName]
    public static string \u0005()
    {
      try
      {
        return \u0002.\u0003;
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex);
      }
    }

    [SpecialName]
    public static void \u0005([In] string obj0)
    {
      try
      {
        \u0002.\u0003 = obj0;
      }
      catch (Exception ex)
      {
        string str = obj0;
        throw UnhandledException.\u0003(ex, (object) str);
      }
    }

    [SpecialName]
    public static int \u0003()
    {
      try
      {
        return \u0002.\u0001;
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex);
      }
    }

    [SpecialName]
    public static void \u0003([In] int obj0)
    {
      try
      {
        \u0002.\u0001 = obj0;
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) obj0;
        throw UnhandledException.\u0003(ex, (object) local);
      }
    }

    public static string \u0003([In] string obj0)
    {
      string[] strArray1;
      try
      {
        strArray1 = new string[11]
        {
          \u0002.\u0001(17942),
          obj0,
          \u0002.\u0001(18019),
          \u0002.\u0001,
          \u0002.\u0001(18028),
          \u0002.\u0001.ToString(),
          \u0002.\u0001(18037),
          \u0002.\u0002,
          \u0002.\u0001(18054),
          \u0002.\u0003,
          \u0002.\u0001(18071)
        };
        return \u001C\u0004.\u0005\u0006(strArray1);
      }
      catch (Exception ex)
      {
        string[] strArray2 = strArray1;
        string str = obj0;
        throw UnhandledException.\u0003(ex, (object) strArray2, (object) str);
      }
    }

    public static void \u0003()
    {
      try
      {
        \u0002.\u0001 = new MySqlConnection(\u0002.\u0003(\u0002.\u0001(16719)));
        \u0001\u0007.\u007E\u001E\u0014((object) \u0002.\u0001);
        \u0002.\u0001 = true;
        \u0002.\u0001 = new Timer();
        \u008A\u0004.\u007E\u0081\u0013((object) \u0002.\u0001, 120000);
        \u0011\u0003.\u007E\u0080\u0013((object) \u0002.\u0001, true);
        \u0011\u0006.\u007E\u007F\u0013((object) \u0002.\u0001, new EventHandler(\u0002.\u0003));
        \u0001\u0007.\u007E\u0082\u0013((object) \u0002.\u0001);
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex);
      }
    }

    private static void \u0003([In] object obj0, [In] EventArgs obj1)
    {
      try
      {
        \u0002.\u0004();
      }
      catch (Exception ex)
      {
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0003(ex, obj, (object) eventArgs);
      }
    }

    public static void \u0004()
    {
      try
      {
        if (!\u0002.\u0001 || \u0093\u0007.\u007E\u009C\u0002((object) \u0002.\u0001))
          return;
        \u0002.\u0003();
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex);
      }
    }

    public static void \u0003([In] DataSet obj0)
    {
      int num1;
      int num2;
      try
      {
        if (obj0 == null)
          return;
        for (num1 = 0; num1 < \u0008\u0007.\u007E\u0098\u0013((object) \u009D\u0002.\u007E\u0007\u0014((object) obj0)); ++num1)
        {
          for (num2 = 0; num2 < \u0008\u0007.\u007E\u0098\u0013((object) \u0084\u0004.\u007E\u0012\u0014((object) \u0013\u0006.\u007E\u0017\u0014((object) \u009D\u0002.\u007E\u0007\u0014((object) obj0), num1))); ++num2)
          {
            if (\u0004\u0008.\u007E\u0002\u0014((object) \u0081\u0006.\u007E\u0003\u0014((object) \u0084\u0004.\u007E\u0012\u0014((object) \u0013\u0006.\u007E\u0017\u0014((object) \u009D\u0002.\u007E\u0007\u0014((object) obj0), num1)), num2), DataRowVersion.Proposed))
              \u0001\u0007.\u007E\u0001\u0014((object) \u0081\u0006.\u007E\u0003\u0014((object) \u0084\u0004.\u007E\u0012\u0014((object) \u0013\u0006.\u007E\u0017\u0014((object) \u009D\u0002.\u007E\u0007\u0014((object) obj0), num1)), num2));
          }
        }
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<int> local1 = (ValueType) num1;
        // ISSUE: variable of a boxed type
        __Boxed<int> local2 = (ValueType) num2;
        DataSet dataSet = obj0;
        throw UnhandledException.\u0003(ex, (object) local1, (object) local2, (object) dataSet);
      }
    }

    public static DataRow \u0003([In] DataRow obj0)
    {
      string str1;
      DataColumn dataColumn1;
      IEnumerator enumerator1;
      IDisposable disposable1;
      try
      {
        enumerator1 = \u0090\u0002.\u007E\u0099\u0013((object) \u0004\u0003.\u007E\u0011\u0014((object) \u0096\u0002.\u007E\u009C\u0013((object) obj0)));
        try
        {
          while (\u0093\u0007.\u007E\u0087\u0005((object) enumerator1))
          {
            dataColumn1 = (DataColumn) \u001F\u0006.\u007E\u0088\u0005((object) enumerator1);
            \u009D\u0007 obj = \u009D\u0007.\u007E\u0014\u0007;
            Type type = \u0008\u0005.\u007E\u009B\u0013((object) dataColumn1);
            if ((str1 = obj((object) type)) != null)
            {
              if (!\u0005\u0003.\u008B\u0005(str1, \u0002.\u0001(18108)) && !\u0005\u0003.\u008B\u0005(str1, \u0002.\u0001(18117)))
              {
                if (!\u0005\u0003.\u008B\u0005(str1, \u0002.\u0001(18126)))
                {
                  if (!\u0005\u0003.\u008B\u0005(str1, \u0002.\u0001(18135)) && !\u0005\u0003.\u008B\u0005(str1, \u0002.\u0001(18144)))
                  {
                    if (\u0005\u0003.\u008B\u0005(str1, \u0002.\u0001(18153)))
                      \u0004\u0004.\u007E\u009E\u0013((object) obj0, \u009D\u0007.\u007E\u009A\u0013((object) dataColumn1), (object) \u0002.\u0001(16369));
                  }
                  else
                    \u0004\u0004.\u007E\u009E\u0013((object) obj0, \u009D\u0007.\u007E\u009A\u0013((object) dataColumn1), (object) 0.0f);
                }
                else
                  \u0004\u0004.\u007E\u009E\u0013((object) obj0, \u009D\u0007.\u007E\u009A\u0013((object) dataColumn1), (object) (sbyte) 0);
              }
              else
                \u0004\u0004.\u007E\u009E\u0013((object) obj0, \u009D\u0007.\u007E\u009A\u0013((object) dataColumn1), (object) 0);
            }
          }
        }
        finally
        {
          disposable1 = enumerator1 as IDisposable;
          if (disposable1 != null)
            \u0001\u0007.\u007E\u0089\u0005((object) disposable1);
        }
        return obj0;
      }
      catch (Exception ex)
      {
        DataColumn dataColumn2 = dataColumn1;
        IEnumerator enumerator2 = enumerator1;
        string str2 = str1;
        IDisposable disposable2 = disposable1;
        DataRow dataRow = obj0;
        throw UnhandledException.\u0003(ex, (object) dataColumn2, (object) enumerator2, (object) str2, (object) disposable2, (object) dataRow);
      }
    }

    public static void \u0003([In] DataTable obj0, [In] string obj1)
    {
      MySqlCommandBuilder sqlCommandBuilder1;
      MySqlDataAdapter adapter;
      try
      {
        if (\u0005\u0003.\u008C\u0005(\u009D\u0007.\u007E\u001B\u0014((object) \u0002.\u0001), \u0002.\u0001(16719)))
          \u0097\u0005.\u007E\u001D\u0014((object) \u0002.\u0001, \u0002.\u0001(16719));
        adapter = new MySqlDataAdapter(obj1, \u0002.\u0001);
        sqlCommandBuilder1 = new MySqlCommandBuilder(adapter);
        \u0089\u0007.\u007E\u001A\u0014((object) sqlCommandBuilder1, ConflictOption.OverwriteChanges);
        int num = \u0001\u0003.\u007E\u0081\u0014((object) adapter, obj0);
      }
      catch (Exception ex)
      {
        MySqlDataAdapter mySqlDataAdapter = adapter;
        MySqlCommandBuilder sqlCommandBuilder2 = sqlCommandBuilder1;
        DataTable dataTable = obj0;
        string str = obj1;
        throw UnhandledException.\u0003(ex, (object) mySqlDataAdapter, (object) sqlCommandBuilder2, (object) dataTable, (object) str);
      }
    }

    public static DataSet \u0003([In] string obj0, [In] string obj1)
    {
      MySqlDataAdapter adapter;
      DataSet dataSet1;
      try
      {
        dataSet1 = new DataSet();
        if (\u0005\u0003.\u008B\u0005(obj1, \u0002.\u0001(9380)))
          obj1 = \u0002.\u0001(18162);
        if (\u0005\u0003.\u008C\u0005(\u009D\u0007.\u007E\u001B\u0014((object) \u0002.\u0001), \u0002.\u0001(16719)))
          \u0097\u0005.\u007E\u001D\u0014((object) \u0002.\u0001, \u0002.\u0001(16719));
        adapter = new MySqlDataAdapter(obj0, \u0002.\u0001);
        MySqlCommandBuilder sqlCommandBuilder = new MySqlCommandBuilder(adapter);
        DataTable[] dataTableArray = \u0015\u0006.\u007E\u001F\u0014((object) adapter, dataSet1, SchemaType.Source, obj1);
        return dataSet1;
      }
      catch (Exception ex)
      {
        DataSet dataSet2 = dataSet1;
        MySqlDataAdapter mySqlDataAdapter = adapter;
        string str1 = obj0;
        string str2 = obj1;
        throw UnhandledException.\u0003(ex, (object) dataSet2, (object) mySqlDataAdapter, (object) str1, (object) str2);
      }
    }

    public static DataSet \u0004([In] string obj0, [In] string obj1)
    {
      MySqlDataAdapter adapter;
      DataSet dataSet1;
      try
      {
        dataSet1 = new DataSet();
        if (\u0005\u0003.\u008B\u0005(obj1, \u0002.\u0001(9380)))
          obj1 = \u0002.\u0001(18162);
        if (\u0005\u0003.\u008C\u0005(\u009D\u0007.\u007E\u001B\u0014((object) \u0002.\u0001), \u0002.\u0001(16719)))
          \u0097\u0005.\u007E\u001D\u0014((object) \u0002.\u0001, \u0002.\u0001(16719));
        adapter = new MySqlDataAdapter(obj0, \u0002.\u0001);
        MySqlCommandBuilder sqlCommandBuilder = new MySqlCommandBuilder(adapter);
        DataTable[] dataTableArray = \u0015\u0006.\u007E\u001F\u0014((object) adapter, dataSet1, SchemaType.Source, obj1);
        int num = \u001F\u0003.\u007E\u007F\u0014((object) adapter, dataSet1, obj1);
        return dataSet1;
      }
      catch (Exception ex)
      {
        DataSet dataSet2 = dataSet1;
        MySqlDataAdapter mySqlDataAdapter = adapter;
        string str1 = obj0;
        string str2 = obj1;
        throw UnhandledException.\u0003(ex, (object) dataSet2, (object) mySqlDataAdapter, (object) str1, (object) str2);
      }
    }

    public static DataTable \u0003([In] string obj0)
    {
      MySqlDataAdapter mySqlDataAdapter1;
      DataTable dataTable1;
      try
      {
        dataTable1 = new DataTable();
        if (\u0005\u0003.\u008C\u0005(\u009D\u0007.\u007E\u001B\u0014((object) \u0002.\u0001), \u0002.\u0001(16719)))
          \u0097\u0005.\u007E\u001D\u0014((object) \u0002.\u0001, \u0002.\u0001(16719));
        mySqlDataAdapter1 = new MySqlDataAdapter(obj0, \u0002.\u0001);
        int num = \u0001\u0003.\u007E\u0080\u0014((object) mySqlDataAdapter1, dataTable1);
        return dataTable1;
      }
      catch (Exception ex)
      {
        DataTable dataTable2 = dataTable1;
        MySqlDataAdapter mySqlDataAdapter2 = mySqlDataAdapter1;
        string str = obj0;
        throw UnhandledException.\u0003(ex, (object) dataTable2, (object) mySqlDataAdapter2, (object) str);
      }
    }

    public static int \u0003([In] string obj0)
    {
      MySqlCommand mySqlCommand1;
      try
      {
        if (\u0005\u0003.\u008C\u0005(\u009D\u0007.\u007E\u001B\u0014((object) \u0002.\u0001), \u0002.\u0001(16719)))
          \u0097\u0005.\u007E\u001D\u0014((object) \u0002.\u0001, \u0002.\u0001(16719));
        mySqlCommand1 = new MySqlCommand(obj0, \u0002.\u0001);
        return \u0008\u0007.\u007E\u0019\u0014((object) mySqlCommand1);
      }
      catch (Exception ex)
      {
        MySqlCommand mySqlCommand2 = mySqlCommand1;
        int num;
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) num;
        string str = obj0;
        throw UnhandledException.\u0003(ex, (object) mySqlCommand2, (object) local, (object) str);
      }
    }

    static \u0002()
    {
      \u0003.\u0003();
    }
  }
}
