﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: MobEditor, Version=1.1.0.2, Culture=neutral, PublicKeyToken=016197d45384ac33
// MVID: 35831B81-08C7-444B-81F1-3EE10E122F41
// Assembly location: D:\Server\eab\Tools\N7 Mob Editor.exe

using \u0001;
using SmartAssembly.SmartExceptionsCore;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace \u0008
{
  internal sealed class \u0001 : Form
  {
    [NonSerialized]
    internal static \u0002 \u0001;
    private IContainer \u0001;
    private TableLayoutPanel \u0001;
    private PictureBox \u0001;
    private Label \u0001;
    private Label \u0002;
    private Label \u0003;
    private Label \u0004;
    private TextBox \u0001;
    private Button \u0001;

    protected override void Dispose([In] bool obj0)
    {
      try
      {
        if (obj0 && this.\u0001 != null)
          \u0001\u0007.\u007E\u0089\u0005((object) this.\u0001);
        \u0011\u0003.\u0082\u0012((object) this, obj0);
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<bool> local = (ValueType) obj0;
        throw UnhandledException.\u0003(ex, (object) this, (object) local);
      }
    }

    private void \u0003()
    {
      ComponentResourceManager componentResourceManager1;
      try
      {
        // ISSUE: type reference
        componentResourceManager1 = new ComponentResourceManager(\u0014\u0006.\u001A\u0007(__typeref (\u0008.\u0001)));
        this.\u0001 = new TableLayoutPanel();
        this.\u0001 = new PictureBox();
        this.\u0001 = new Label();
        this.\u0002 = new Label();
        this.\u0003 = new Label();
        this.\u0004 = new Label();
        this.\u0001 = new TextBox();
        this.\u0001 = new Button();
        \u0001\u0007.\u007E\u007F\u0011((object) this.\u0001);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0001);
        \u0001\u0007.\u007F\u0011((object) this);
        \u008A\u0004.\u007E\u0015\u0013((object) this.\u0001, 2);
        int num1 = \u0086\u0002.\u007E\u001D\u0013((object) \u007F\u0004.\u007E\u0018\u0013((object) this.\u0001), new ColumnStyle(SizeType.Percent, 33f));
        int num2 = \u0086\u0002.\u007E\u001D\u0013((object) \u007F\u0004.\u007E\u0018\u0013((object) this.\u0001), new ColumnStyle(SizeType.Percent, 67f));
        \u009A\u0007.\u007E\u001A\u0013((object) \u0096\u0004.\u007E\u0014\u0013((object) this.\u0001), (Control) this.\u0001, 0, 0);
        \u009A\u0007.\u007E\u001A\u0013((object) \u0096\u0004.\u007E\u0014\u0013((object) this.\u0001), (Control) this.\u0001, 1, 0);
        \u009A\u0007.\u007E\u001A\u0013((object) \u0096\u0004.\u007E\u0014\u0013((object) this.\u0001), (Control) this.\u0002, 1, 1);
        \u009A\u0007.\u007E\u001A\u0013((object) \u0096\u0004.\u007E\u0014\u0013((object) this.\u0001), (Control) this.\u0003, 1, 2);
        \u009A\u0007.\u007E\u001A\u0013((object) \u0096\u0004.\u007E\u0014\u0013((object) this.\u0001), (Control) this.\u0004, 1, 3);
        \u009A\u0007.\u007E\u001A\u0013((object) \u0096\u0004.\u007E\u0014\u0013((object) this.\u0001), (Control) this.\u0001, 1, 4);
        \u009A\u0007.\u007E\u001A\u0013((object) \u0096\u0004.\u007E\u0014\u0013((object) this.\u0001), (Control) this.\u0001, 1, 5);
        \u009E\u0004.\u007E\u008B\u0010((object) this.\u0001, DockStyle.Fill);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0001, new Point(9, 9));
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0001, \u0008.\u0001.\u0001(17615));
        \u008A\u0004.\u007E\u0016\u0013((object) this.\u0001, 6);
        int num3 = \u0081\u0005.\u007E\u001E\u0013((object) \u0001\u0008.\u007E\u0017\u0013((object) this.\u0001), new RowStyle(SizeType.Percent, 10f));
        int num4 = \u0081\u0005.\u007E\u001E\u0013((object) \u0001\u0008.\u007E\u0017\u0013((object) this.\u0001), new RowStyle(SizeType.Percent, 10f));
        int num5 = \u0081\u0005.\u007E\u001E\u0013((object) \u0001\u0008.\u007E\u0017\u0013((object) this.\u0001), new RowStyle(SizeType.Percent, 10f));
        int num6 = \u0081\u0005.\u007E\u001E\u0013((object) \u0001\u0008.\u007E\u0017\u0013((object) this.\u0001), new RowStyle(SizeType.Percent, 10f));
        int num7 = \u0081\u0005.\u007E\u001E\u0013((object) \u0001\u0008.\u007E\u0017\u0013((object) this.\u0001), new RowStyle(SizeType.Percent, 50f));
        int num8 = \u0081\u0005.\u007E\u001E\u0013((object) \u0001\u0008.\u007E\u0017\u0013((object) this.\u0001), new RowStyle(SizeType.Percent, 10f));
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0001, new Size(417, 265));
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0001, 0);
        \u009E\u0004.\u007E\u008B\u0010((object) this.\u0001, DockStyle.Fill);
        \u0081\u0004.\u007E\u000F\u0013((object) this.\u0001, (Image) \u008B\u0005.\u007E\u001B\u000E((object) componentResourceManager1, \u0008.\u0001.\u0001(17640)));
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0001, new Point(3, 3));
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0001, \u0008.\u0001.\u0001(17669));
        \u0016\u0005.\u007E\u0019\u0013((object) this.\u0001, (Control) this.\u0001, 6);
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0001, new Size(131, 259));
        \u008A\u0003.\u007E\u0010\u0013((object) this.\u0001, PictureBoxSizeMode.StretchImage);
        \u008A\u0004.\u007E\u0012\u0013((object) this.\u0001, 12);
        \u0011\u0003.\u007E\u0011\u0013((object) this.\u0001, false);
        \u009E\u0004.\u007E\u008B\u0010((object) this.\u0001, DockStyle.Fill);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0001, new Point(143, 0));
        \u008A\u0005.\u007E\u0096\u0010((object) this.\u0001, new Padding(6, 0, 3, 0));
        \u0082\u0004.\u007E\u0097\u0010((object) this.\u0001, new Size(0, 17));
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0001, \u0008.\u0001.\u0001(17690));
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0001, new Size(271, 17));
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0001, 19);
        \u0097\u0005.\u007E\u009F\u0010((object) this.\u0001, \u0008.\u0001.\u0001(17715));
        \u0094\u0006.\u007E\u008A\u0012((object) this.\u0001, ContentAlignment.MiddleLeft);
        \u009E\u0004.\u007E\u008B\u0010((object) this.\u0002, DockStyle.Fill);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0002, new Point(143, 26));
        \u008A\u0005.\u007E\u0096\u0010((object) this.\u0002, new Padding(6, 0, 3, 0));
        \u0082\u0004.\u007E\u0097\u0010((object) this.\u0002, new Size(0, 17));
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0002, \u0008.\u0001.\u0001(17732));
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0002, new Size(271, 17));
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0002, 0);
        \u0097\u0005.\u007E\u009F\u0010((object) this.\u0002, \u0008.\u0001.\u0001(17749));
        \u0094\u0006.\u007E\u008A\u0012((object) this.\u0002, ContentAlignment.MiddleLeft);
        \u009E\u0004.\u007E\u008B\u0010((object) this.\u0003, DockStyle.Fill);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0003, new Point(143, 52));
        \u008A\u0005.\u007E\u0096\u0010((object) this.\u0003, new Padding(6, 0, 3, 0));
        \u0082\u0004.\u007E\u0097\u0010((object) this.\u0003, new Size(0, 17));
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0003, \u0008.\u0001.\u0001(17762));
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0003, new Size(271, 17));
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0003, 21);
        \u0097\u0005.\u007E\u009F\u0010((object) this.\u0003, \u0008.\u0001.\u0001(17783));
        \u0094\u0006.\u007E\u008A\u0012((object) this.\u0003, ContentAlignment.MiddleLeft);
        \u009E\u0004.\u007E\u008B\u0010((object) this.\u0004, DockStyle.Fill);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0004, new Point(143, 78));
        \u008A\u0005.\u007E\u0096\u0010((object) this.\u0004, new Padding(6, 0, 3, 0));
        \u0082\u0004.\u007E\u0097\u0010((object) this.\u0004, new Size(0, 17));
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0004, \u0008.\u0001.\u0001(17796));
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0004, new Size(271, 17));
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0004, 22);
        \u0097\u0005.\u007E\u009F\u0010((object) this.\u0004, \u0008.\u0001.\u0001(17821));
        \u0094\u0006.\u007E\u008A\u0012((object) this.\u0004, ContentAlignment.MiddleLeft);
        \u009E\u0004.\u007E\u008B\u0010((object) this.\u0001, DockStyle.Fill);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0001, new Point(143, 107));
        \u008A\u0005.\u007E\u0096\u0010((object) this.\u0001, new Padding(6, 3, 3, 3));
        \u0011\u0003.\u007E\u0005\u0012((object) this.\u0001, true);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0001, \u0008.\u0001.\u0001(17838));
        \u0011\u0003.\u007E\u0006\u0012((object) this.\u0001, true);
        \u0091\u0005.\u007E\u000E\u0012((object) this.\u0001, ScrollBars.Both);
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0001, new Size(271, 126));
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0001, 23);
        \u0011\u0003.\u007E\u009D\u0010((object) this.\u0001, false);
        \u0097\u0005.\u007E\u009F\u0010((object) this.\u0001, \u0008.\u0001.\u0001(17863));
        \u008B\u0004.\u007E\u007F\u0010((object) this.\u0001, AnchorStyles.Bottom | AnchorStyles.Right);
        \u009F\u0004.\u007E\u008F\u0011((object) this.\u0001, DialogResult.Cancel);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0001, new Point(339, 239));
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0001, \u0008.\u0001.\u0001(17880));
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0001, new Size(75, 23));
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0001, 24);
        \u0097\u0005.\u007E\u009F\u0010((object) this.\u0001, \u0008.\u0001.\u0001(6975));
        \u0096\u0006.\u0011\u0012((object) this, (IButtonControl) this.\u0001);
        \u008D\u0003.\u0083\u0011((object) this, new SizeF(6f, 13f));
        \u0090\u0003.\u0084\u0011((object) this, AutoScaleMode.Font);
        \u0082\u0004.\u0015\u0012((object) this, new Size(435, 283));
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u0088\u0010((object) this), (Control) this.\u0001);
        \u0086\u0007.\u0013\u0012((object) this, FormBorderStyle.FixedDialog);
        \u0011\u0003.\u0018\u0012((object) this, false);
        \u0011\u0003.\u0019\u0012((object) this, false);
        \u0097\u0005.\u0098\u0010((object) this, \u0008.\u0001.\u0001(17893));
        \u008A\u0005.\u0007\u0011((object) this, new Padding(9));
        \u0011\u0003.\u001B\u0012((object) this, false);
        \u0011\u0003.\u001A\u0012((object) this, false);
        \u0007\u0005.\u001E\u0012((object) this, FormStartPosition.CenterParent);
        \u0097\u0005.\u007E\u009F\u0010((object) this, \u0008.\u0001.\u0001(17893));
        \u0011\u0003.\u007E\u001B\u0011((object) this.\u0001, false);
        \u0001\u0007.\u007E\u0019\u0011((object) this.\u0001);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0001);
        \u0011\u0003.\u001B\u0011((object) this, false);
      }
      catch (Exception ex)
      {
        ComponentResourceManager componentResourceManager2 = componentResourceManager1;
        throw UnhandledException.\u0003(ex, (object) componentResourceManager2, (object) this);
      }
    }

    public \u0001()
    {
      try
      {
        this.\u0003();
        \u0097\u0005.\u007E\u009F\u0010((object) this, \u009F\u0006.\u009D\u0005(\u0008.\u0001.\u0001(17902), (object) this.\u0003()));
        \u0097\u0005.\u007E\u009F\u0010((object) this.\u0001, this.\u0006());
        \u0097\u0005.\u007E\u009F\u0010((object) this.\u0002, \u009F\u0006.\u009D\u0005(\u0008.\u0001.\u0001(17915), (object) this.\u0004()));
        \u0097\u0005.\u007E\u009F\u0010((object) this.\u0003, this.\u0007());
        \u0097\u0005.\u007E\u009F\u0010((object) this.\u0004, this.\u0008());
        \u0097\u0005.\u007E\u009F\u0010((object) this.\u0001, this.\u0005());
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex, (object) this);
      }
    }

    [SpecialName]
    public string \u0003()
    {
      AssemblyTitleAttribute assemblyTitleAttribute1;
      object[] objArray1;
      try
      {
        // ISSUE: type reference
        objArray1 = \u0017\u0005.\u007E\u0087\u0008((object) \u0013\u0007.\u008D\u0008(), \u0014\u0006.\u001A\u0007(__typeref (AssemblyTitleAttribute)), false);
        if (objArray1.Length > 0)
          goto label_3;
label_2:
        return \u009D\u0006.\u0096\u000E(\u009D\u0007.\u007E\u007F\u0008((object) \u0013\u0007.\u008D\u0008()));
label_3:
        assemblyTitleAttribute1 = (AssemblyTitleAttribute) objArray1[0];
        if (\u0005\u0003.\u008C\u0005(\u009D\u0007.\u007E\u0098\u0008((object) assemblyTitleAttribute1), \u0008.\u0001.\u0001(9370)))
          return \u009D\u0007.\u007E\u0098\u0008((object) assemblyTitleAttribute1);
        goto label_2;
      }
      catch (Exception ex)
      {
        object[] objArray2 = objArray1;
        AssemblyTitleAttribute assemblyTitleAttribute2 = assemblyTitleAttribute1;
        throw UnhandledException.\u0003(ex, (object) objArray2, (object) assemblyTitleAttribute2, (object) this);
      }
    }

    [SpecialName]
    public string \u0004()
    {
      try
      {
        return \u009D\u0007.\u007E\u001C\u0005((object) \u0002\u0005.\u007E\u009B\u0008((object) \u008F\u0006.\u007E\u0080\u0008((object) \u0013\u0007.\u008D\u0008())));
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex, (object) this);
      }
    }

    [SpecialName]
    public string \u0005()
    {
      object[] objArray1;
      try
      {
        // ISSUE: type reference
        objArray1 = \u0017\u0005.\u007E\u0087\u0008((object) \u0013\u0007.\u008D\u0008(), \u0014\u0006.\u001A\u0007(__typeref (AssemblyDescriptionAttribute)), false);
        if (objArray1.Length == 0)
          return \u0008.\u0001.\u0001(9370);
        return \u009D\u0007.\u007E\u0096\u0008((object) (AssemblyDescriptionAttribute) objArray1[0]);
      }
      catch (Exception ex)
      {
        object[] objArray2 = objArray1;
        throw UnhandledException.\u0003(ex, (object) objArray2, (object) this);
      }
    }

    [SpecialName]
    public string \u0006()
    {
      object[] objArray1;
      try
      {
        // ISSUE: type reference
        objArray1 = \u0017\u0005.\u007E\u0087\u0008((object) \u0013\u0007.\u008D\u0008(), \u0014\u0006.\u001A\u0007(__typeref (AssemblyProductAttribute)), false);
        if (objArray1.Length == 0)
          return \u0008.\u0001.\u0001(9370);
        return \u009D\u0007.\u007E\u0092\u0008((object) (AssemblyProductAttribute) objArray1[0]);
      }
      catch (Exception ex)
      {
        object[] objArray2 = objArray1;
        throw UnhandledException.\u0003(ex, (object) objArray2, (object) this);
      }
    }

    [SpecialName]
    public string \u0007()
    {
      object[] objArray1;
      try
      {
        // ISSUE: type reference
        objArray1 = \u0017\u0005.\u007E\u0087\u0008((object) \u0013\u0007.\u008D\u0008(), \u0014\u0006.\u001A\u0007(__typeref (AssemblyCopyrightAttribute)), false);
        if (objArray1.Length == 0)
          return \u0008.\u0001.\u0001(9370);
        return \u009D\u0007.\u007E\u0090\u0008((object) (AssemblyCopyrightAttribute) objArray1[0]);
      }
      catch (Exception ex)
      {
        object[] objArray2 = objArray1;
        throw UnhandledException.\u0003(ex, (object) objArray2, (object) this);
      }
    }

    [SpecialName]
    public string \u0008()
    {
      object[] objArray1;
      try
      {
        // ISSUE: type reference
        objArray1 = \u0017\u0005.\u007E\u0087\u0008((object) \u0013\u0007.\u008D\u0008(), \u0014\u0006.\u001A\u0007(__typeref (AssemblyCompanyAttribute)), false);
        if (objArray1.Length == 0)
          return \u0008.\u0001.\u0001(9370);
        return \u009D\u0007.\u007E\u0094\u0008((object) (AssemblyCompanyAttribute) objArray1[0]);
      }
      catch (Exception ex)
      {
        object[] objArray2 = objArray1;
        throw UnhandledException.\u0003(ex, (object) objArray2, (object) this);
      }
    }

    static \u0001()
    {
      \u0003.\u0003();
    }
  }
}
