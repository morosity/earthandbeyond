﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: MobEditor, Version=1.1.0.2, Culture=neutral, PublicKeyToken=016197d45384ac33
// MVID: 35831B81-08C7-444B-81F1-3EE10E122F41
// Assembly location: D:\Server\eab\Tools\N7 Mob Editor.exe

using \u0001;
using \u0004;
using SmartAssembly.SmartExceptionsCore;
using System;
using System.Net;
using System.Runtime.InteropServices;

namespace \u0004
{
  internal sealed class \u0006
  {
    private string \u0002 = string.Empty;
    [NonSerialized]
    internal static \u0002 \u0001;
    private string \u0001;
    private IWebProxy \u0001;

    public void \u0003([In] IWebProxy obj0)
    {
      this.\u0001 = obj0;
    }

    public string \u0003()
    {
      try
      {
        UploadReportLoginService reportLoginService = new UploadReportLoginService();
        if (this.\u0001 != null)
          \u0098\u0006.\u007E\u0002\u0005((object) reportLoginService, this.\u0001);
        this.\u0002 = reportLoginService.GetServerURL(this.\u0001);
        if (\u0008\u0007.\u007E\u008E\u0005((object) this.\u0002) == 0)
          throw new ApplicationException(\u0006.\u0001(5584));
        return \u0094\u0003.\u007E\u0097\u0005((object) this.\u0002, \u0006.\u0001(5156)) ? this.\u0002 : \u0006.\u0001(5151);
      }
      catch (Exception ex)
      {
        return \u0014\u0003.\u0002\u0006(\u0006.\u0001(5625), \u009D\u0007.\u007E\u0013\u0006((object) ex));
      }
    }

    public string \u0003([In] byte[] obj0)
    {
      try
      {
        ReportingService reportingService = new ReportingService(this.\u0002);
        if (this.\u0001 != null)
          \u0098\u0006.\u007E\u0002\u0005((object) reportingService, this.\u0001);
        return reportingService.UploadReport2(this.\u0001, obj0);
      }
      catch (Exception ex)
      {
        return \u0014\u0003.\u0002\u0006(\u0006.\u0001(5642), \u009D\u0007.\u007E\u0013\u0006((object) ex));
      }
    }

    public \u0006([In] string obj0)
    {
      this.\u0001 = obj0;
    }

    static \u0006()
    {
      \u0003.\u0003();
    }
  }
}
