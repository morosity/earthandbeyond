﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: MobEditor, Version=1.1.0.2, Culture=neutral, PublicKeyToken=016197d45384ac33
// MVID: 35831B81-08C7-444B-81F1-3EE10E122F41
// Assembly location: D:\Server\eab\Tools\N7 Mob Editor.exe

using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;

namespace \u0004
{
  internal sealed class \u0004 : EventArgs
  {
    private string \u0001 = string.Empty;
    private bool \u0003 = true;
    private SecurityException \u0001;
    private bool \u0001;
    private bool \u0002;

    [SpecialName]
    public SecurityException \u0003()
    {
      return this.\u0001;
    }

    [SpecialName]
    public string \u0003()
    {
      return this.\u0001;
    }

    [SpecialName]
    public bool \u0003()
    {
      return this.\u0003;
    }

    [SpecialName]
    public bool \u0004()
    {
      return this.\u0001;
    }

    [SpecialName]
    public void \u0003([In] bool obj0)
    {
      this.\u0001 = obj0;
    }

    [SpecialName]
    public bool \u0005()
    {
      return this.\u0002;
    }

    public \u0004([In] SecurityException obj0)
    {
      this.\u0001 = obj0;
    }

    public \u0004([In] SecurityException obj0, [In] bool obj1)
      : this(obj0)
    {
      this.\u0003 = obj1;
    }

    public \u0004([In] string obj0, [In] bool obj1)
      : this(new SecurityException(obj0), obj1)
    {
      this.\u0001 = obj0;
    }
  }
}
