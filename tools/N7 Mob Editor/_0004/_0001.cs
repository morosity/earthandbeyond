﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: MobEditor, Version=1.1.0.2, Culture=neutral, PublicKeyToken=016197d45384ac33
// MVID: 35831B81-08C7-444B-81F1-3EE10E122F41
// Assembly location: D:\Server\eab\Tools\N7 Mob Editor.exe

using \u0001;
using \u0005;
using System;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace \u0004
{
  internal sealed class \u0001 : Control
  {
    private Label \u0001 = new Label();
    private float \u0001 = 1f;
    private float \u0002 = 1f;
    [NonSerialized]
    internal static \u0002 \u0001;
    private Image \u0001;
    private Icon \u0001;
    private Bitmap \u0001;
    private \u0005.\u0005 \u0001;

    [SpecialName]
    public void \u0003([In] \u0005.\u0005 obj0)
    {
      if (this.\u0001 == obj0)
        return;
      this.\u0001 = obj0;
      switch (this.\u0001)
      {
        case \u0005.\u0005.\u0002:
          this.\u0001 = \u0006.\u0003(\u0004.\u0001.\u0001(5394));
          break;
        case \u0005.\u0005.\u0003:
          this.\u0001 = \u0006.\u0003(\u0004.\u0001.\u0001(5407));
          break;
        default:
          this.\u0001 = (Bitmap) null;
          break;
      }
      \u0001\u0007.\u007E\u001A\u0011((object) this);
    }

    [SpecialName]
    public override string get_Text()
    {
      return \u009D\u0007.\u007E\u009E\u0010((object) this.\u0001);
    }

    [SpecialName]
    public override void set_Text([In] string obj0)
    {
      \u0097\u0005.\u007E\u009F\u0010((object) this.\u0001, obj0);
    }

    protected override void Dispose([In] bool obj0)
    {
      if (obj0)
        goto label_7;
label_6:
      \u0011\u0003.\u0012\u0011((object) this, obj0);
      return;
label_7:
      if (this.\u0001 != null)
      {
        \u0001\u0007.\u007E\u009E\u000F((object) this.\u0001);
        this.\u0001 = (Icon) null;
      }
      if (this.\u0001 != null)
      {
        \u0001\u0007.\u007E\u008F\u000F((object) this.\u0001);
        this.\u0001 = (Image) null;
      }
      if (this.\u0001 != null)
      {
        \u0001\u0007.\u007E\u008F\u000F((object) this.\u0001);
        this.\u0001 = (Bitmap) null;
        goto label_6;
      }
      else
        goto label_6;
    }

    protected override void OnResize([In] EventArgs obj0)
    {
      \u0089\u0003.\u007E\u001D\u0011((object) this.\u0001, \u0018\u0007.\u0094\u0006(13f * this.\u0001), \u0018\u0007.\u0094\u0006(15f * this.\u0002), \u0008\u0007.\u0005\u0011((object) this) - \u0018\u0007.\u0094\u0006(69f * this.\u0001), \u0008\u0007.\u0091\u0010((object) this) - \u0018\u0007.\u0094\u0006(18f * this.\u0002));
      \u0002\u0006.\u0018\u0011((object) this, obj0);
    }

    protected override void ScaleCore([In] float obj0, [In] float obj1)
    {
      this.\u0001 = obj0;
      this.\u0002 = obj1;
      \u009A\u0004.\u001C\u0011((object) this, obj0, obj1);
      \u0002\u0006.\u007E\u0018\u0011((object) this, EventArgs.Empty);
    }

    protected override void OnPaint([In] PaintEventArgs obj0)
    {
      \u0097\u0004.\u0017\u0011((object) this, obj0);
      \u0007\u0007.\u007E\u0096\u000F((object) \u000F\u0003.\u007E\u0082\u0011((object) obj0), \u009F\u0005.\u0011\u0010(), 0, \u0084\u0005.\u0086\u0010((object) this).Height - 2, \u0084\u0005.\u0086\u0010((object) this).Width, \u0084\u0005.\u0086\u0010((object) this).Height - 2);
      \u0007\u0007.\u007E\u0096\u000F((object) \u000F\u0003.\u007E\u0082\u0011((object) obj0), \u009F\u0005.\u0012\u0010(), 0, \u0084\u0005.\u0086\u0010((object) this).Height - 1, \u0084\u0005.\u0086\u0010((object) this).Width, \u0084\u0005.\u0086\u0010((object) this).Height - 1);
      Rectangle rectangle = new Rectangle(\u0084\u0005.\u0086\u0010((object) this).Width - \u0018\u0007.\u0094\u0006(48f * this.\u0001), \u0018\u0007.\u0094\u0006(11f * this.\u0002), \u0018\u0007.\u0094\u0006(32f * this.\u0001), \u0018\u0007.\u0094\u0006(32f * this.\u0002));
      if (this.\u0001 != null)
      {
        \u0014\u0005.\u007E\u009A\u000F((object) \u000F\u0003.\u007E\u0082\u0011((object) obj0), this.\u0001, rectangle, new Rectangle(0, 0, 32, 32), GraphicsUnit.Pixel);
      }
      else
      {
        if (this.\u0001 == null)
          return;
        \u0004\u0007.\u007E\u0098\u000F((object) \u000F\u0003.\u007E\u0082\u0011((object) obj0), this.\u0001, rectangle);
        if (this.\u0001 == null)
          return;
        \u0014\u0005.\u007E\u009A\u000F((object) \u000F\u0003.\u007E\u0082\u0011((object) obj0), (Image) this.\u0001, new Rectangle(rectangle.Right - \u0018\u0007.\u0094\u0006(12f * this.\u0001), rectangle.Bottom - \u0018\u0007.\u0094\u0006(12f * this.\u0002), \u0018\u0007.\u0094\u0006(16f * this.\u0001), \u0018\u0007.\u0094\u0006(16f * this.\u0002)), new Rectangle(0, 0, 16, 16), GraphicsUnit.Pixel);
      }
    }

    protected override void OnFontChanged([In] EventArgs obj0)
    {
      try
      {
        \u0088\u0007.\u007E\u008E\u0010((object) this.\u0001, new Font(\u001D\u0004.\u007E\u008D\u0010((object) this), FontStyle.Bold));
        \u0002\u0006.\u0015\u0011((object) this, obj0);
      }
      catch
      {
      }
    }

    public \u0001()
    {
      try
      {
        \u0001\u0005.\u007E\u0089\u0012((object) this.\u0001, FlatStyle.System);
        \u0088\u0007.\u007E\u008E\u0010((object) this.\u0001, new Font(\u001D\u0004.\u007E\u008D\u0010((object) this), FontStyle.Bold));
      }
      catch
      {
      }
      \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u0088\u0010((object) this), (Control) this.\u0001);
      \u001F\u0004.\u007E\u0081\u0010((object) this, \u0093\u0003.\u0010\u0010());
      \u0011\u0003.\u009D\u0010((object) this, false);
      \u009E\u0004.\u007E\u008B\u0010((object) this, DockStyle.Top);
      \u008A\u0004.\u0092\u0010((object) this, 58);
      \u0093\u0006.\u001E\u0011((object) this, ControlStyles.UserPaint | ControlStyles.ResizeRedraw | ControlStyles.SupportsTransparentBackColor | ControlStyles.AllPaintingInWmPaint | ControlStyles.DoubleBuffer, true);
      this.\u0001 = \u0003.\u0003();
      \u0002\u0006.\u007E\u0018\u0011((object) this, EventArgs.Empty);
    }

    public \u0001([In] string obj0)
      : this()
    {
      \u0097\u0005.\u007E\u009F\u0010((object) this.\u0001, obj0);
    }

    static \u0001()
    {
      \u0003.\u0003();
    }
  }
}
