﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: MobEditor, Version=1.1.0.2, Culture=neutral, PublicKeyToken=016197d45384ac33
// MVID: 35831B81-08C7-444B-81F1-3EE10E122F41
// Assembly location: D:\Server\eab\Tools\N7 Mob Editor.exe

using \u0001;
using \u0006;
using SmartAssembly.SmartExceptionsCore;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace \u0006
{
  internal sealed class \u0006 : Form
  {
    [NonSerialized]
    internal static \u0002 \u0001;
    private IContainer \u0001;
    private Label \u0001;

    public \u0006()
    {
      try
      {
        this.\u0004();
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex, (object) this);
      }
    }

    public void \u0003([In] string obj0)
    {
      try
      {
        \u0097\u0005.\u007E\u009F\u0010((object) this.\u0001, obj0);
        \u0001\u0007.\u007E\u001A\u0011((object) this);
        this.\u0003();
      }
      catch (Exception ex)
      {
        string str = obj0;
        throw UnhandledException.\u0003(ex, (object) this, (object) str);
      }
    }

    private void \u0003()
    {
    }

    private void \u0003([In] object obj0, [In] PaintEventArgs obj1)
    {
    }

    protected override void OnPaintBackground([In] PaintEventArgs obj0)
    {
      Graphics graphics1;
      try
      {
        graphics1 = \u000F\u0003.\u007E\u0082\u0011((object) obj0);
        // ISSUE: reference to a compiler-generated method
        \u0012\u0006.\u007E\u0099\u000F((object) graphics1, (Image) \u0005.\u0006(), new Rectangle(0, 0, \u0008\u0007.\u0005\u0011((object) this), \u0008\u0007.\u0091\u0010((object) this)));
      }
      catch (Exception ex)
      {
        Graphics graphics2 = graphics1;
        PaintEventArgs paintEventArgs = obj0;
        throw UnhandledException.\u0003(ex, (object) graphics2, (object) this, (object) paintEventArgs);
      }
    }

    protected override void Dispose([In] bool obj0)
    {
      try
      {
        if (obj0 && this.\u0001 != null)
          \u0001\u0007.\u007E\u0089\u0005((object) this.\u0001);
        \u0011\u0003.\u0082\u0012((object) this, obj0);
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<bool> local = (ValueType) obj0;
        throw UnhandledException.\u0003(ex, (object) this, (object) local);
      }
    }

    private void \u0004()
    {
      try
      {
        this.\u0001 = new Label();
        \u0001\u0007.\u007F\u0011((object) this);
        \u0011\u0003.\u007E\u0080\u0010((object) this.\u0001, true);
        \u001F\u0004.\u007E\u0081\u0010((object) this.\u0001, \u0093\u0003.\u0091\u000F());
        \u001F\u0004.\u007E\u0090\u0010((object) this.\u0001, \u0093\u0003.\u0093\u000F());
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0001, new Point(13, 254));
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0001, \u0006.\u0006.\u0001(8513));
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0001, new Size(0, 13));
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0001, 0);
        \u008D\u0003.\u0083\u0011((object) this, new SizeF(6f, 13f));
        \u0090\u0003.\u0084\u0011((object) this, AutoScaleMode.Font);
        \u0087\u0006.\u007E\u0082\u0010((object) this, ImageLayout.None);
        \u0082\u0004.\u0015\u0012((object) this, new Size(453, 279));
        \u0011\u0003.\u0016\u0012((object) this, false);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u0088\u0010((object) this), (Control) this.\u0001);
        \u0086\u0007.\u0013\u0012((object) this, FormBorderStyle.None);
        \u0097\u0005.\u0098\u0010((object) this, \u0006.\u0006.\u0001(8522));
        \u0011\u0003.\u001B\u0012((object) this, false);
        \u0011\u0003.\u001A\u0012((object) this, false);
        \u0007\u0005.\u001E\u0012((object) this, FormStartPosition.CenterScreen);
        \u0097\u0005.\u007E\u009F\u0010((object) this, \u0006.\u0006.\u0001(8522));
        \u0011\u0003.\u001F\u0012((object) this, true);
        \u0098\u0003.\u0008\u0011((object) this, new PaintEventHandler(this.\u0003));
        \u0011\u0003.\u001B\u0011((object) this, false);
        \u0001\u0007.\u0019\u0011((object) this);
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex, (object) this);
      }
    }

    static \u0006()
    {
      \u0003.\u0003();
    }
  }
}
