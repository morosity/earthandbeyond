﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: MobEditor, Version=1.1.0.2, Culture=neutral, PublicKeyToken=016197d45384ac33
// MVID: 35831B81-08C7-444B-81F1-3EE10E122F41
// Assembly location: D:\Server\eab\Tools\N7 Mob Editor.exe

using \u0001;
using \u0007;
using \u0008;
using SmartAssembly.SmartExceptionsCore;
using System;
using System.Data;
using System.Runtime.InteropServices;

namespace \u0007
{
  internal sealed class \u0006
  {
    [NonSerialized]
    internal static \u0002 \u0001;
    private DataTable \u0001;

    public \u0006()
    {
      string str1;
      try
      {
        str1 = \u0006.\u0001(17500);
        this.\u0001 = \u0002.\u0003(str1);
      }
      catch (Exception ex)
      {
        string str2 = str1;
        throw UnhandledException.\u0003(ex, (object) str2, (object) this);
      }
    }

    public DataRow[] \u0003([In] string obj0)
    {
      string str1;
      try
      {
        str1 = \u008A\u0002.\u007E\u009B\u0005((object) obj0, \u0006.\u0001(17529), \u0006.\u0001(17534));
        return \u0016\u0003.\u007E\u0015\u0014((object) this.\u0001, \u001C\u0007.\u0003\u0006(\u0006.\u0001(17539), str1, \u0006.\u0001(17529)));
      }
      catch (Exception ex)
      {
        DataRow[] dataRowArray1;
        DataRow[] dataRowArray2 = dataRowArray1;
        string str2 = str1;
        string str3 = obj0;
        throw UnhandledException.\u0003(ex, (object) dataRowArray2, (object) str2, (object) this, (object) str3);
      }
    }

    public string \u0003([In] int obj0)
    {
      string str1;
      DataRow[] dataRowArray1;
      string str2;
      string str3;
      try
      {
        dataRowArray1 = \u0016\u0003.\u007E\u0015\u0014((object) this.\u0001, \u0099\u0006.\u009F\u0005((object) \u0006.\u0001(17560), (object) obj0, (object) \u0006.\u0001(17529)));
        str1 = \u009D\u0007.\u007E\u001C\u0005(\u008B\u0005.\u007E\u009D\u0013((object) dataRowArray1[0], \u0006.\u0001(17228)));
        string str4 = \u009D\u0007.\u007E\u001C\u0005(\u008B\u0005.\u007E\u009D\u0013((object) dataRowArray1[0], \u0006.\u0001(17241)));
        str2 = \u0006.\u0001(3384);
        if (\u0094\u0003.\u007E\u0092\u0005((object) str1, \u0006.\u0001(17263)) || \u0094\u0003.\u007E\u0092\u0005((object) str1, \u0006.\u0001(17272)) || (\u0094\u0003.\u007E\u0092\u0005((object) str1, \u0006.\u0001(17281)) || \u0094\u0003.\u007E\u0092\u0005((object) str1, \u0006.\u0001(17290))))
        {
          str3 = \u008D\u0002.\u007E\u009C\u0005((object) str1, \u0008\u0007.\u007E\u008E\u0005((object) str1) - 4);
          if (\u0094\u0003.\u007E\u0092\u0005((object) str1, \u0006.\u0001(17573)) || \u0094\u0003.\u007E\u0092\u0005((object) str1, \u0006.\u0001(17578)))
          {
            str2 = \u0014\u0003.\u0002\u0006(str3, \u0006.\u0001(17583));
            str2 = \u0014\u0003.\u0002\u0006(\u0006.\u0001(17592), str2);
          }
          else
            str2 = \u0014\u0003.\u0002\u0006(str3, \u0006.\u0001(17299));
        }
        return str2;
      }
      catch (Exception ex)
      {
        DataRow[] dataRowArray2 = dataRowArray1;
        string str4 = str1;
        string str5 = str2;
        string str6 = str3;
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) obj0;
        throw UnhandledException.\u0003(ex, (object) dataRowArray2, (object) str4, (object) str5, (object) str6, (object) this, (object) local);
      }
    }

    static \u0006()
    {
      \u0003.\u0003();
    }
  }
}
