﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: MobEditor, Version=1.1.0.2, Culture=neutral, PublicKeyToken=016197d45384ac33
// MVID: 35831B81-08C7-444B-81F1-3EE10E122F41
// Assembly location: D:\Server\eab\Tools\N7 Mob Editor.exe

using \u0001;
using \u0007;
using DevExpress.Data;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Layout;
using DevExpress.XtraLayout;
using DevExpress.XtraLayout.Utils;
using SmartAssembly.SmartExceptionsCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace \u0007
{
  internal sealed class \u0001 : XtraForm
  {
    private Hashtable \u0001 = new Hashtable();
    [NonSerialized]
    internal static \u0002 \u0001;
    private DataTable \u0001;
    private \u0006 \u0001;
    private string \u0001;
    public int \u0001;
    private IContainer \u0001;
    private GridControl \u0001;
    private LayoutView \u0001;
    private GridView \u0001;
    private LayoutView \u0002;
    private LayoutViewColumn \u0001;
    private LayoutViewColumn \u0002;
    private LayoutViewColumn \u0003;
    private LayoutViewColumn \u0004;
    private RepositoryItemPictureEdit \u0001;
    private BarManager \u0001;
    private Bar \u0001;
    private BarStaticItem \u0001;
    private BarEditItem \u0001;
    private RepositoryItemLookUpEdit \u0001;
    private Bar \u0002;
    private BarDockControl \u0001;
    private BarDockControl \u0002;
    private BarDockControl \u0003;
    private BarDockControl \u0004;
    private BarStaticItem \u0002;
    private BarEditItem \u0002;
    private RepositoryItemComboBox \u0001;
    private RepositoryItemLookUpEdit \u0002;
    private LayoutViewField \u0001;
    private LayoutViewField \u0002;
    private LayoutViewField \u0003;
    private LayoutViewField \u0004;
    private LayoutViewColumn \u0005;
    private LayoutViewField \u0005;
    private LayoutViewCard \u0001;

    public \u0001()
    {
      try
      {
        this.\u0003();
        this.\u0001 = \u0083\u0004.\u0018\u0010();
        this.\u0001 = \u0003.\u0001.\u0001;
        this.\u0001 = \u0003.\u0001.\u0001;
        \u0080\u0002.\u007E\u0080\u0004((object) this.\u0001, (object) \u0003.\u0001.\u0002);
        \u0097\u0005.\u007E\u0081\u0004((object) this.\u0001, \u0007.\u0001.\u0001(8542));
        \u0097\u0005.\u007E\u0082\u0004((object) this.\u0001, \u0007.\u0001.\u0001(8547));
        \u0080\u0002.\u007E\u008A\u0002((object) this.\u0001, (object) 2);
        \u0080\u0002.\u007E\u008A\u0002((object) this.\u0002, (object) \u0007.\u0001.\u0001(5782));
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex, (object) this);
      }
    }

    private void \u0003([In] object obj0, [In] EventArgs obj1)
    {
      string str1;
      try
      {
        if (\u001F\u0006.\u007E\u0089\u0002((object) this.\u0001) == null || (\u001F\u0006.\u007E\u0089\u0002((object) this.\u0002) == null || this.\u0001 == null))
          return;
        str1 = \u0014\u0003.\u0002\u0006(\u0007.\u0001.\u0001(8556), \u009D\u0007.\u007E\u001C\u0005(\u001F\u0006.\u007E\u0089\u0002((object) this.\u0001)));
        if (\u0005\u0003.\u008C\u0005(\u009D\u0007.\u007E\u001C\u0005(\u001F\u0006.\u007E\u0089\u0002((object) this.\u0002)), \u0007.\u0001.\u0001(5782)))
          str1 = \u001C\u0007.\u0003\u0006(str1, \u0007.\u0001.\u0001(8569), \u009D\u0007.\u007E\u001C\u0005(\u001F\u0006.\u007E\u0089\u0002((object) this.\u0002)));
        try
        {
          \u0080\u0002.\u007E\u0089((object) this.\u0001, (object) ((IEnumerable<DataRow>) \u0016\u0003.\u007E\u0015\u0014((object) this.\u0001, str1)).CopyToDataTable<DataRow>());
        }
        catch (Exception ex)
        {
        }
      }
      catch (Exception ex)
      {
        string str2 = str1;
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) str2, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u0003([In] object obj0, [In] CustomColumnDataEventArgs obj1)
    {
      LayoutView layoutView1;
      string str1;
      string str2;
      Image image1;
      try
      {
        if (!\u0005\u0003.\u008B\u0005(\u009D\u0007.\u007E\u0015((object) \u0019\u0004.\u007E\u001E((object) obj1)), \u0007.\u0001.\u0001(8590)) || !\u0093\u0007.\u007E\u007F((object) obj1))
          return;
        layoutView1 = obj0 as LayoutView;
        str1 = \u009D\u0007.\u007E\u001C\u0005(\u0002\u0008.\u007E\u0006((object) layoutView1, \u0008\u0007.\u007E\u001F((object) obj1), \u0007.\u0001.\u0001(8599)));
        str2 = this.\u0001.\u0003(\u0010\u0006.\u0006\u0007(str1));
        if (!\u0099\u0005.\u007E\u0014\u0008((object) this.\u0001, (object) str2))
        {
          image1 = (Image) null;
          try
          {
            image1 = \u0013\u0004.\u008E\u000F(\u001C\u0007.\u0003\u0006(this.\u0001, \u0007.\u0001.\u0001(8612), str2));
          }
          catch (Exception ex)
          {
            int num = (int) \u001B\u0005.\u0005\u0013(\u009D\u0007.\u007E\u0013\u0006((object) ex));
          }
          \u001A\u0005.\u007E\u0012\u0008((object) this.\u0001, (object) str2, (object) image1);
        }
        \u0080\u0002.\u007E\u0080((object) obj1, \u0095\u0004.\u007E\u0015\u0008((object) this.\u0001, (object) str2));
      }
      catch (Exception ex)
      {
        LayoutView layoutView2 = layoutView1;
        string str3 = str1;
        string str4 = str2;
        Image image2 = image1;
        Exception exception = ex;
        object obj = obj0;
        CustomColumnDataEventArgs columnDataEventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) layoutView2, (object) str3, (object) str4, (object) image2, (object) exception, (object) this, obj, (object) columnDataEventArgs);
      }
    }

    private void \u0004([In] object obj0, [In] EventArgs obj1)
    {
    }

    private void \u0005([In] object obj0, [In] EventArgs obj1)
    {
      try
      {
        if (!\u0093\u0007.\u0002\u0011((object) this))
          return;
        this.\u0001 = 0;
      }
      catch (Exception ex)
      {
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u0006([In] object obj0, [In] EventArgs obj1)
    {
      int num;
      LayoutView layoutView1;
      GridControl gridControl1;
      try
      {
        gridControl1 = obj0 as GridControl;
        layoutView1 = \u0092\u0005.\u007E\u008B((object) \u0014\u0004.\u007E\u0088((object) gridControl1), 0) as LayoutView;
        num = \u0008\u0007.\u007E\u0005((object) layoutView1);
        if (num < 0)
          return;
        this.\u0001 = \u0010\u0006.\u0006\u0007(\u009D\u0007.\u007E\u001C\u0005(\u007F\u0006.\u007E\u0007((object) layoutView1, num, (GridColumn) \u0006\u0004.\u007E\u0096((object) \u0005\u0004.\u007E\u008D((object) layoutView1), \u0007.\u0001.\u0001(8542)))));
        \u0001\u0007.\u0081\u0012((object) this);
      }
      catch (Exception ex)
      {
        GridControl gridControl2 = gridControl1;
        LayoutView layoutView2 = layoutView1;
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) num;
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) gridControl2, (object) layoutView2, (object) local, (object) this, obj, (object) eventArgs);
      }
    }

    protected override void Dispose([In] bool obj0)
    {
      try
      {
        if (obj0 && this.\u0001 != null)
          \u0001\u0007.\u007E\u0089\u0005((object) this.\u0001);
        \u0011\u0003.\u008C\u0004((object) this, obj0);
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<bool> local = (ValueType) obj0;
        throw UnhandledException.\u0003(ex, (object) this, (object) local);
      }
    }

    private void \u0003()
    {
      RepositoryItem[] repositoryItemArray1;
      BaseView[] baseViewArray1;
      LayoutViewColumn[] layoutViewColumnArray1;
      BaseLayoutItem[] baseLayoutItemArray1;
      BaseLayoutItem[] baseLayoutItemArray2;
      Bar[] barArray1;
      BarItem[] barItemArray1;
      RepositoryItem[] repositoryItemArray2;
      LinkPersistInfo[] linkPersistInfoArray1;
      EditorButton[] editorButtonArray1;
      EditorButton[] editorButtonArray2;
      object[] objArray1;
      EditorButton[] editorButtonArray3;
      ComponentResourceManager componentResourceManager;
      try
      {
        this.\u0001 = (IContainer) new Container();
        // ISSUE: type reference
        componentResourceManager = new ComponentResourceManager(\u0014\u0006.\u001A\u0007(__typeref (\u0007.\u0001)));
        this.\u0001 = new GridControl();
        this.\u0002 = new LayoutView();
        this.\u0001 = new LayoutViewColumn();
        this.\u0001 = new LayoutViewField();
        this.\u0002 = new LayoutViewColumn();
        this.\u0002 = new LayoutViewField();
        this.\u0003 = new LayoutViewColumn();
        this.\u0003 = new LayoutViewField();
        this.\u0004 = new LayoutViewColumn();
        this.\u0001 = new RepositoryItemPictureEdit();
        this.\u0004 = new LayoutViewField();
        this.\u0005 = new LayoutViewColumn();
        this.\u0005 = new LayoutViewField();
        this.\u0001 = new LayoutViewCard();
        this.\u0001 = new LayoutView();
        this.\u0001 = new GridView();
        this.\u0001 = new BarManager(this.\u0001);
        this.\u0001 = new Bar();
        this.\u0001 = new BarStaticItem();
        this.\u0001 = new BarEditItem();
        this.\u0001 = new RepositoryItemLookUpEdit();
        this.\u0002 = new BarStaticItem();
        this.\u0002 = new BarEditItem();
        this.\u0001 = new RepositoryItemComboBox();
        this.\u0002 = new Bar();
        this.\u0001 = new BarDockControl();
        this.\u0002 = new BarDockControl();
        this.\u0003 = new BarDockControl();
        this.\u0004 = new BarDockControl();
        this.\u0002 = new RepositoryItemLookUpEdit();
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0002);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0002);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0003);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0004);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0005);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0002);
        \u0001\u0007.\u008D\u0004((object) this);
        \u009E\u0004.\u007E\u008B\u0010((object) this.\u0001, DockStyle.Fill);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0001, new Point(0, 24));
        \u0095\u0006.\u007E\u0087((object) this.\u0001, (BaseView) this.\u0002);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0001, \u0007.\u0001.\u0001(8625));
        \u0088\u0004 obj1 = \u0088\u0004.\u007E\u0087\u0004;
        RepositoryItemCollection repositoryItemCollection1 = \u0017\u0003.\u007E\u0006\u0004((object) this.\u0001);
        repositoryItemArray1 = new RepositoryItem[1]
        {
          (RepositoryItem) this.\u0001
        };
        RepositoryItem[] repositoryItemArray3 = repositoryItemArray1;
        obj1((object) repositoryItemCollection1, repositoryItemArray3);
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0001, new Size(700, 320));
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0001, 0);
        \u0018\u0003 obj2 = \u0018\u0003.\u007E\u009E;
        ViewRepositoryCollection repositoryCollection = \u0007\u0004.\u007E\u0086((object) this.\u0001);
        baseViewArray1 = new BaseView[3]
        {
          (BaseView) this.\u0002,
          (BaseView) this.\u0001,
          (BaseView) this.\u0001
        };
        BaseView[] baseViewArray2 = baseViewArray1;
        obj2((object) repositoryCollection, baseViewArray2);
        \u0011\u0006.\u007E\u000E\u0011((object) this.\u0001, new EventHandler(this.\u0006));
        \u008A\u0004.\u007E\u0090((object) this.\u0002, 0);
        \u0082\u0004.\u007E\u008E((object) this.\u0002, new Size(211, 209));
        \u008A\u0004.\u007E\u0091((object) this.\u0002, 0);
        \u0080\u0003 obj3 = \u0080\u0003.\u007E\u0097;
        LayoutViewColumnCollection columnCollection = \u0005\u0004.\u007E\u008D((object) this.\u0002);
        layoutViewColumnArray1 = new LayoutViewColumn[5]
        {
          this.\u0001,
          this.\u0002,
          this.\u0003,
          this.\u0004,
          this.\u0005
        };
        LayoutViewColumn[] layoutViewColumnArray2 = layoutViewColumnArray1;
        obj3((object) columnCollection, layoutViewColumnArray2);
        \u008C\u0003.\u007E\u0003((object) this.\u0002, this.\u0001);
        \u008F\u0007 obj4 = \u008F\u0007.\u007E\u0083\u0003;
        HiddenItemsCollection hiddenItemsCollection = \u0006\u0007.\u007E\u0093((object) this.\u0002);
        baseLayoutItemArray1 = new BaseLayoutItem[1]
        {
          (BaseLayoutItem) this.\u0003
        };
        BaseLayoutItem[] baseLayoutItemArray3 = baseLayoutItemArray1;
        obj4((object) hiddenItemsCollection, baseLayoutItemArray3);
        \u0097\u0005.\u007E\u0002((object) this.\u0002, \u0007.\u0001.\u0001(8638));
        \u007F\u0002.\u007E\u009C((object) \u0008\u0004.\u007E\u008F((object) this.\u0002), CardsAlignment.Near);
        \u0011\u0003.\u007E\u009B((object) \u0008\u0004.\u007E\u008F((object) this.\u0002), true);
        \u0082\u0007.\u007E\u009D((object) \u0008\u0004.\u007E\u008F((object) this.\u0002), LayoutViewMode.MultiColumn);
        \u009E\u0007.\u007E\u008C((object) this.\u0002, this.\u0001);
        \u0082\u0006.\u007E\u000E((object) this.\u0002, new CustomColumnDataEventHandler(this.\u0003));
        \u0097\u0005.\u007E\u0014((object) this.\u0001, \u0007.\u0001.\u0001(3857));
        \u0097\u0005.\u007E\u0016((object) this.\u0001, \u0007.\u0001.\u0001(8547));
        \u008F\u0002.\u007E\u0099((object) this.\u0001, this.\u0001);
        \u0097\u0005.\u007E\u0012((object) this.\u0001, \u0007.\u0001.\u0001(8655));
        \u0011\u0003.\u007E\u0082((object) \u000E\u0004.\u007E\u0013((object) this.\u0001), false);
        \u0011\u0003.\u007E\u0083((object) \u000E\u0004.\u007E\u0013((object) this.\u0001), true);
        \u008A\u0004.\u007E\u0091\u0003((object) this.\u0001, 145);
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0001, new Point(0, 0));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0001, \u0007.\u0001.\u0001(8680));
        \u001A\u0004.\u007E\u001B\u0003((object) this.\u0001, new Padding(5, 5, 5, 5));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0001, new Size(191, 26));
        \u001A\u0006.\u007E\u0081\u0003((object) this.\u0001, Locations.Default);
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u0001, new Size(31, 13));
        \u008A\u0004.\u007E\u0019\u0003((object) this.\u0001, 5);
        \u0097\u0005.\u007E\u0014((object) this.\u0002, \u0007.\u0001.\u0001(8725));
        \u0097\u0005.\u007E\u0016((object) this.\u0002, \u0007.\u0001.\u0001(8734));
        \u008F\u0002.\u007E\u0099((object) this.\u0002, this.\u0002);
        \u0097\u0005.\u007E\u0012((object) this.\u0002, \u0007.\u0001.\u0001(8743));
        \u0011\u0003.\u007E\u0082((object) \u000E\u0004.\u007E\u0013((object) this.\u0002), false);
        \u0011\u0003.\u007E\u0083((object) \u000E\u0004.\u007E\u0013((object) this.\u0002), true);
        \u008A\u0004.\u007E\u0091\u0003((object) this.\u0002, 34);
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0002, new Point(0, 26));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0002, \u0007.\u0001.\u0001(8768));
        \u001A\u0004.\u007E\u001B\u0003((object) this.\u0002, new Padding(5, 5, 5, 5));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0002, new Size(80, 26));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u0002, new Size(31, 13));
        \u008A\u0004.\u007E\u0019\u0003((object) this.\u0002, 5);
        \u0097\u0005.\u007E\u0014((object) this.\u0003, \u0007.\u0001.\u0001(8813));
        \u008F\u0002.\u007E\u0099((object) this.\u0003, this.\u0003);
        \u0097\u0005.\u007E\u0012((object) this.\u0003, \u0007.\u0001.\u0001(8826));
        \u0011\u0003.\u007E\u0082((object) \u000E\u0004.\u007E\u0013((object) this.\u0003), false);
        \u0011\u0003.\u007E\u0083((object) \u000E\u0004.\u007E\u0013((object) this.\u0003), true);
        \u008A\u0004.\u007E\u0091\u0003((object) this.\u0003, 20);
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0003, new Point(0, 0));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0003, \u0007.\u0001.\u0001(8851));
        \u001A\u0004.\u007E\u001B\u0003((object) this.\u0003, new Padding(5, 5, 5, 5));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0003, new Size(243, 84));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u0003, new Size(0, 0));
        \u008A\u0004.\u007E\u0019\u0003((object) this.\u0003, 0);
        \u0011\u0003.\u007E\u0017\u0003((object) this.\u0003, false);
        \u0097\u0005.\u007E\u0014((object) this.\u0004, \u0007.\u0001.\u0001(8896));
        \u0005\u0007.\u007E\u0017((object) this.\u0004, (RepositoryItem) this.\u0001);
        \u0097\u0005.\u007E\u0016((object) this.\u0004, \u0007.\u0001.\u0001(8590));
        \u008F\u0002.\u007E\u0099((object) this.\u0004, this.\u0004);
        \u0097\u0005.\u007E\u0012((object) this.\u0004, \u0007.\u0001.\u0001(8909));
        \u0011\u0003.\u007E\u0082((object) \u000E\u0004.\u007E\u0013((object) this.\u0004), false);
        \u0011\u0003.\u007E\u0083((object) \u000E\u0004.\u007E\u0013((object) this.\u0004), true);
        \u0080\u0006.\u007E\u001B((object) this.\u0004, UnboundColumnType.Object);
        \u0097\u0005.\u007E\u0093\u0003((object) this.\u0001, \u0007.\u0001.\u0001(8918));
        \u0011\u0003.\u007E\u0096\u0003((object) this.\u0001, true);
        \u0011\u0003.\u007E\u0010\u0004((object) this.\u0001, false);
        \u0099\u0007.\u007E\u0011\u0004((object) this.\u0001, PictureSizeMode.Squeeze);
        \u008A\u0004.\u007E\u0091\u0003((object) this.\u0004, 181);
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0004, new Point(0, 52));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0004, \u0007.\u0001.\u0001(8955));
        \u001A\u0004.\u007E\u001B\u0003((object) this.\u0004, new Padding(5, 5, 5, 5));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0004, new Size(191, 100));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u0004, new Size(0, 0));
        \u008A\u0004.\u007E\u0019\u0003((object) this.\u0004, 0);
        \u0011\u0003.\u007E\u0017\u0003((object) this.\u0004, false);
        \u0097\u0005.\u007E\u0014((object) this.\u0005, \u0007.\u0001.\u0001(3843));
        \u0097\u0005.\u007E\u0016((object) this.\u0005, \u0007.\u0001.\u0001(8542));
        \u008F\u0002.\u007E\u0099((object) this.\u0005, this.\u0005);
        \u0097\u0005.\u007E\u0012((object) this.\u0005, \u0007.\u0001.\u0001(9000));
        \u008A\u0004.\u007E\u0091\u0003((object) this.\u0005, 65);
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0005, new Point(80, 26));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0005, \u0007.\u0001.\u0001(9025));
        \u001A\u0004.\u007E\u001B\u0003((object) this.\u0005, new Padding(5, 5, 5, 5));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0005, new Size(111, 26));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u0005, new Size(31, 13));
        \u008A\u0004.\u007E\u0019\u0003((object) this.\u0005, 5);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u0001, \u0007.\u0001.\u0001(9074));
        \u001C\u0006.\u007E\u008E\u0003((object) this.\u0001, GroupElementLocation.AfterText);
        \u008F\u0007 obj5 = \u008F\u0007.\u007E\u0083\u0003;
        LayoutGroupItemCollection groupItemCollection = \u008B\u0007.\u007E\u0090\u0003((object) this.\u0001);
        baseLayoutItemArray2 = new BaseLayoutItem[4]
        {
          (BaseLayoutItem) this.\u0002,
          (BaseLayoutItem) this.\u0001,
          (BaseLayoutItem) this.\u0005,
          (BaseLayoutItem) this.\u0004
        };
        BaseLayoutItem[] baseLayoutItemArray4 = baseLayoutItemArray2;
        obj5((object) groupItemCollection, baseLayoutItemArray4);
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0001, \u0007.\u0001.\u0001(9091));
        \u008A\u0004.\u007E\u0092\u0003((object) \u0002\u0003.\u007E\u008D\u0003((object) this.\u0001), 5);
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u0001, \u0007.\u0001.\u0001(9074));
        \u008C\u0003.\u007E\u0003((object) this.\u0001, this.\u0001);
        \u0097\u0005.\u007E\u0002((object) this.\u0001, \u0007.\u0001.\u0001(9112));
        \u009E\u0007.\u007E\u008C((object) this.\u0001, (LayoutViewCard) null);
        \u008C\u0003.\u007E\u0003((object) this.\u0001, this.\u0001);
        \u0097\u0005.\u007E\u0002((object) this.\u0001, \u0007.\u0001.\u0001(9129));
        \u0083\u0003 obj6 = \u0083\u0003.\u007E\u000F\u0002;
        Bars bars = \u009F\u0003.\u007E\u001B\u0002((object) this.\u0001);
        barArray1 = new Bar[2]{ this.\u0001, this.\u0002 };
        Bar[] barArray2 = barArray1;
        obj6((object) bars, barArray2);
        int num1 = \u0092\u0002.\u007E\u008D\u0002((object) \u0017\u0006.\u007E\u001A\u0002((object) this.\u0001), this.\u0001);
        int num2 = \u0092\u0002.\u007E\u008D\u0002((object) \u0017\u0006.\u007E\u001A\u0002((object) this.\u0001), this.\u0002);
        int num3 = \u0092\u0002.\u007E\u008D\u0002((object) \u0017\u0006.\u007E\u001A\u0002((object) this.\u0001), this.\u0003);
        int num4 = \u0092\u0002.\u007E\u008D\u0002((object) \u0017\u0006.\u007E\u001A\u0002((object) this.\u0001), this.\u0004);
        \u008B\u0006.\u007E\u001C\u0002((object) this.\u0001, (Control) this);
        \u009F\u0007 obj7 = \u009F\u0007.\u007E\u0091\u0002;
        BarItems barItems = \u0015\u0004.\u007E\u001D\u0002((object) this.\u0001);
        barItemArray1 = new BarItem[4]
        {
          (BarItem) this.\u0001,
          (BarItem) this.\u0001,
          (BarItem) this.\u0002,
          (BarItem) this.\u0002
        };
        BarItem[] barItemArray2 = barItemArray1;
        obj7((object) barItems, barItemArray2);
        \u008C\u0006.\u007E\u001E\u0002((object) this.\u0001, this.\u0001);
        \u008A\u0004.\u007E\u0019\u0002((object) this.\u0001, 5);
        \u0088\u0004 obj8 = \u0088\u0004.\u007E\u0087\u0004;
        RepositoryItemCollection repositoryItemCollection2 = \u0017\u0003.\u007E\u0007\u0004((object) this.\u0001);
        repositoryItemArray2 = new RepositoryItem[3]
        {
          (RepositoryItem) this.\u0001,
          (RepositoryItem) this.\u0002,
          (RepositoryItem) this.\u0001
        };
        RepositoryItem[] repositoryItemArray4 = repositoryItemArray2;
        obj8((object) repositoryItemCollection2, repositoryItemArray4);
        \u008C\u0006.\u007E\u001F\u0002((object) this.\u0001, this.\u0002);
        \u0097\u0005.\u007E\u0003\u0002((object) this.\u0001, \u0007.\u0001.\u0001(9142));
        \u008A\u0004.\u007E\u000E\u0002((object) this.\u0001, 0);
        \u008A\u0004.\u007E\u0008\u0002((object) this.\u0001, 0);
        \u0091\u0007.\u007E\u0006\u0002((object) this.\u0001, BarDockStyle.Top);
        \u008F\u0005 obj9 = \u008F\u0005.\u007E\u0094\u0002;
        LinksInfo linksInfo = \u0096\u0005.\u007E\u0007\u0002((object) this.\u0001);
        linkPersistInfoArray1 = new LinkPersistInfo[4]
        {
          new LinkPersistInfo(BarLinkUserDefines.PaintStyle, (BarItem) this.\u0001, BarItemPaintStyle.Caption),
          new LinkPersistInfo(BarLinkUserDefines.Width, (BarItem) this.\u0001, \u0007.\u0001.\u0001(9155), false, true, true, 148),
          new LinkPersistInfo((BarItem) this.\u0002),
          new LinkPersistInfo(BarLinkUserDefines.Width, (BarItem) this.\u0002, \u0007.\u0001.\u0001(9155), false, true, true, 63)
        };
        LinkPersistInfo[] linkPersistInfoArray2 = linkPersistInfoArray1;
        obj9((object) linksInfo, linkPersistInfoArray2);
        \u0011\u0003.\u007E\u0015\u0002((object) \u0092\u0004.\u007E\u0001\u0002((object) this.\u0001), true);
        \u0011\u0003.\u007E\u0016\u0002((object) \u0092\u0004.\u007E\u0001\u0002((object) this.\u0001), true);
        \u0097\u0005.\u007E\u0004\u0002((object) this.\u0001, \u0007.\u0001.\u0001(9142));
        \u001B\u0006.\u007E\u0081\u0002((object) this.\u0001, BorderStyles.NoBorder);
        \u0097\u0005.\u007E\u0084\u0002((object) this.\u0001, \u0007.\u0001.\u0001(9156));
        \u008A\u0004.\u007E\u0080\u0002((object) this.\u0001, 1);
        \u0097\u0005.\u007E\u007F\u0002((object) this.\u0001, \u0007.\u0001.\u0001(9173));
        \u0080\u0004.\u007E\u0093\u0002((object) this.\u0001, StringAlignment.Near);
        \u0097\u0005.\u007E\u0084\u0002((object) this.\u0001, \u0007.\u0001.\u0001(9156));
        \u0005\u0007.\u007E\u008B\u0002((object) this.\u0001, (RepositoryItem) this.\u0001);
        \u008A\u0004.\u007E\u0080\u0002((object) this.\u0001, 0);
        \u0097\u0005.\u007E\u007F\u0002((object) this.\u0001, \u0007.\u0001.\u0001(9194));
        \u008A\u0004.\u007E\u0083\u0002((object) this.\u0001, 130);
        \u0011\u0006.\u007E\u008C\u0002((object) this.\u0001, new EventHandler(this.\u0003));
        \u0011\u0003.\u007E\u0095\u0003((object) this.\u0001, false);
        \u0008\u0003 obj10 = \u0008\u0003.\u007E\u0003\u0004;
        EditorButtonCollection buttonCollection1 = \u0002\u0007.\u007E\u0099\u0003((object) this.\u0001);
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray4 = editorButtonArray1;
        obj10((object) buttonCollection1, editorButtonArray4);
        \u0097\u0005.\u007E\u0093\u0003((object) this.\u0001, \u0007.\u0001.\u0001(9211));
        \u0097\u0005.\u007E\u0094\u0003((object) this.\u0001, \u0007.\u0001.\u0001(9224));
        \u001B\u0006.\u007E\u0081\u0002((object) this.\u0002, BorderStyles.NoBorder);
        \u0097\u0005.\u007E\u0084\u0002((object) this.\u0002, \u0007.\u0001.\u0001(9245));
        \u008A\u0004.\u007E\u0080\u0002((object) this.\u0002, 2);
        \u0097\u0005.\u007E\u007F\u0002((object) this.\u0002, \u0007.\u0001.\u0001(9262));
        \u0080\u0004.\u007E\u0093\u0002((object) this.\u0002, StringAlignment.Near);
        \u0097\u0005.\u007E\u0084\u0002((object) this.\u0002, \u0007.\u0001.\u0001(9283));
        \u0005\u0007.\u007E\u008B\u0002((object) this.\u0002, (RepositoryItem) this.\u0001);
        \u008A\u0004.\u007E\u0080\u0002((object) this.\u0002, 4);
        \u0097\u0005.\u007E\u007F\u0002((object) this.\u0002, \u0007.\u0001.\u0001(9283));
        \u0011\u0006.\u007E\u008C\u0002((object) this.\u0002, new EventHandler(this.\u0003));
        \u0011\u0003.\u007E\u0095\u0003((object) this.\u0001, false);
        \u0008\u0003 obj11 = \u0008\u0003.\u007E\u0003\u0004;
        EditorButtonCollection buttonCollection2 = \u0002\u0007.\u007E\u0099\u0003((object) this.\u0001);
        editorButtonArray2 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray5 = editorButtonArray2;
        obj11((object) buttonCollection2, editorButtonArray5);
        \u0006\u0005 obj12 = \u0006\u0005.\u007E\u001F\u0004;
        ComboBoxItemCollection boxItemCollection = \u008D\u0005.\u007E\u001B\u0004((object) this.\u0001);
        objArray1 = new object[10]
        {
          (object) \u0007.\u0001.\u0001(9155),
          (object) \u0007.\u0001.\u0001(3592),
          (object) \u0007.\u0001.\u0001(9300),
          (object) \u0007.\u0001.\u0001(9305),
          (object) \u0007.\u0001.\u0001(9310),
          (object) \u0007.\u0001.\u0001(9315),
          (object) \u0007.\u0001.\u0001(9320),
          (object) \u0007.\u0001.\u0001(9325),
          (object) \u0007.\u0001.\u0001(9330),
          (object) \u0007.\u0001.\u0001(9335)
        };
        object[] objArray2 = objArray1;
        obj12((object) boxItemCollection, objArray2);
        \u0097\u0005.\u007E\u0093\u0003((object) this.\u0001, \u0007.\u0001.\u0001(9340));
        \u0097\u0005.\u007E\u0003\u0002((object) this.\u0002, \u0007.\u0001.\u0001(9357));
        \u009D\u0004.\u007E\u0005\u0002((object) this.\u0002, BarCanDockStyle.Bottom);
        \u008A\u0004.\u007E\u000E\u0002((object) this.\u0002, 0);
        \u008A\u0004.\u007E\u0008\u0002((object) this.\u0002, 0);
        \u0091\u0007.\u007E\u0006\u0002((object) this.\u0002, BarDockStyle.Bottom);
        \u0011\u0003.\u007E\u0011\u0002((object) \u0092\u0004.\u007E\u0001\u0002((object) this.\u0002), false);
        \u0011\u0003.\u007E\u0014\u0002((object) \u0092\u0004.\u007E\u0001\u0002((object) this.\u0002), false);
        \u0011\u0003.\u007E\u0016\u0002((object) \u0092\u0004.\u007E\u0001\u0002((object) this.\u0002), true);
        \u0097\u0005.\u007E\u0004\u0002((object) this.\u0002, \u0007.\u0001.\u0001(9357));
        \u0011\u0003.\u007E\u0095\u0003((object) this.\u0002, false);
        \u0008\u0003 obj13 = \u0008\u0003.\u007E\u0003\u0004;
        EditorButtonCollection buttonCollection3 = \u0002\u0007.\u007E\u0099\u0003((object) this.\u0002);
        editorButtonArray3 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray6 = editorButtonArray3;
        obj13((object) buttonCollection3, editorButtonArray6);
        \u0097\u0005.\u007E\u0093\u0003((object) this.\u0002, \u0007.\u0001.\u0001(9374));
        \u008D\u0003.\u0083\u0011((object) this, new SizeF(6f, 13f));
        \u0090\u0003.\u0084\u0011((object) this, AutoScaleMode.Font);
        \u0082\u0004.\u0015\u0012((object) this, new Size(700, 366));
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u0088\u0010((object) this), (Control) this.\u0001);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u0088\u0010((object) this), (Control) this.\u0003);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u0088\u0010((object) this), (Control) this.\u0004);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u0088\u0010((object) this), (Control) this.\u0002);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u0088\u0010((object) this), (Control) this.\u0001);
        \u001D\u0006.\u0017\u0012((object) this, (Icon) \u008B\u0005.\u007E\u001B\u000E((object) componentResourceManager, \u0007.\u0001.\u0001(9411)));
        \u0097\u0005.\u0098\u0010((object) this, \u0007.\u0001.\u0001(9428));
        \u0097\u0005.\u007E\u009F\u0010((object) this, \u0007.\u0001.\u0001(9441));
        \u0011\u0006.\u000E\u0011((object) this, new EventHandler(this.\u0004));
        \u0011\u0006.\u0004\u0011((object) this, new EventHandler(this.\u0005));
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0002);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0002);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0003);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0004);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0005);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0002);
        \u0011\u0003.\u008E\u0004((object) this, false);
      }
      catch (Exception ex)
      {
        object[] objArray2 = new object[15]
        {
          (object) componentResourceManager,
          (object) repositoryItemArray1,
          (object) baseViewArray1,
          (object) layoutViewColumnArray1,
          (object) baseLayoutItemArray1,
          (object) baseLayoutItemArray2,
          (object) barArray1,
          (object) barItemArray1,
          (object) repositoryItemArray2,
          (object) linkPersistInfoArray1,
          (object) editorButtonArray1,
          (object) editorButtonArray2,
          (object) objArray1,
          (object) editorButtonArray3,
          (object) this
        };
        throw UnhandledException.\u0003(ex, objArray2);
      }
    }

    static \u0001()
    {
      \u0003.\u0003();
    }
  }
}
