﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: MobEditor, Version=1.1.0.2, Culture=neutral, PublicKeyToken=016197d45384ac33
// MVID: 35831B81-08C7-444B-81F1-3EE10E122F41
// Assembly location: D:\Server\eab\Tools\N7 Mob Editor.exe

using \u0001;
using \u0007;
using \u0008;
using DevExpress.LookAndFeel;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using SmartAssembly.SmartExceptionsCore;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace \u0007
{
  internal sealed class \u0004 : XtraForm
  {
    public bool \u0001 = true;
    [NonSerialized]
    internal static \u0002 \u0001;
    public bool \u0002;
    private IContainer \u0001;
    private Label \u0001;
    private Label \u0002;
    public TextBox \u0001;
    public TextBox \u0002;
    private Button \u0001;
    private Button \u0002;
    private Label \u0003;
    public TextBox \u0003;
    public TextBox \u0004;
    private Label \u0004;
    private DefaultLookAndFeel \u0001;

    public \u0004()
    {
      try
      {
        this.\u0004();
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex, (object) this);
      }
    }

    private void \u0003([In] object obj0, [In] EventArgs obj1)
    {
      MySqlConnection mySqlConnection1;
      try
      {
        try
        {
          this.\u0003();
          mySqlConnection1 = new MySqlConnection(\u0002.\u0003(\u0004.\u0001(16681)));
          \u0001\u0007.\u007E\u001E\u0014((object) mySqlConnection1);
          \u0001\u0007.\u007E\u001C\u0014((object) mySqlConnection1);
          this.\u0001 = false;
          \u0001\u0007.\u0081\u0012((object) this);
        }
        catch (Exception ex)
        {
          int num = (int) \u001B\u0005.\u0005\u0013(\u0014\u0003.\u0002\u0006(\u0004.\u0001(16690), \u009D\u0007.\u007E\u0013\u0006((object) ex)));
        }
      }
      catch (Exception ex)
      {
        MySqlConnection mySqlConnection2 = mySqlConnection1;
        Exception exception = ex;
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) mySqlConnection2, (object) exception, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u0004([In] object obj0, [In] EventArgs obj1)
    {
      try
      {
        this.\u0001 = true;
        \u0001\u0007.\u0081\u0012((object) this);
      }
      catch (Exception ex)
      {
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u0005([In] object obj0, [In] EventArgs obj1)
    {
      try
      {
        this.\u0002 = true;
      }
      catch (Exception ex)
      {
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u0003([In] object obj0, [In] KeyPressEventArgs obj1)
    {
      char ch;
      MySqlConnection mySqlConnection1;
      try
      {
        ch = \u008E\u0005.\u007E\u0086\u0012((object) obj1);
        if (!ch.Equals('\r'))
          return;
        try
        {
          this.\u0003();
          mySqlConnection1 = new MySqlConnection(\u0002.\u0003(\u0004.\u0001(16681)));
          \u0001\u0007.\u007E\u001E\u0014((object) mySqlConnection1);
          \u0001\u0007.\u007E\u001C\u0014((object) mySqlConnection1);
          this.\u0001 = false;
          \u0001\u0007.\u0081\u0012((object) this);
        }
        catch (Exception ex)
        {
          int num = (int) \u001B\u0005.\u0005\u0013(\u0014\u0003.\u0002\u0006(\u0004.\u0001(16690), \u009D\u0007.\u007E\u0013\u0006((object) ex)));
        }
      }
      catch (Exception ex)
      {
        MySqlConnection mySqlConnection2 = mySqlConnection1;
        Exception exception = ex;
        // ISSUE: variable of a boxed type
        __Boxed<char> local = (ValueType) ch;
        object obj = obj0;
        KeyPressEventArgs keyPressEventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) mySqlConnection2, (object) exception, (object) local, (object) this, obj, (object) keyPressEventArgs);
      }
    }

    private void \u0006([In] object obj0, [In] EventArgs obj1)
    {
      int num1;
      string[] strArray1;
      int num2;
      int num3;
      try
      {
        \u0097\u0005 obj = \u0097\u0005.\u007E\u009F\u0010;
        strArray1 = new string[7];
        strArray1[0] = \u009D\u0007.\u007E\u009E\u0010((object) this);
        strArray1[1] = \u0004.\u0001(16755);
        string[] strArray2 = strArray1;
        int index1 = 2;
        num1 = \u0008\u0007.\u007E\u0099\u0007((object) \u0002\u0005.\u007E\u009B\u0008((object) \u008F\u0006.\u007E\u0080\u0008((object) \u0013\u0007.\u008D\u0008())));
        string str1 = num1.ToString();
        strArray2[index1] = str1;
        strArray1[3] = \u0004.\u0001(16760);
        string[] strArray3 = strArray1;
        int index2 = 4;
        num2 = \u0008\u0007.\u007E\u009A\u0007((object) \u0002\u0005.\u007E\u009B\u0008((object) \u008F\u0006.\u007E\u0080\u0008((object) \u0013\u0007.\u008D\u0008())));
        string str2 = num2.ToString();
        strArray3[index2] = str2;
        strArray1[5] = \u0004.\u0001(16765);
        string[] strArray4 = strArray1;
        int index3 = 6;
        num3 = \u0008\u0007.\u007E\u009B\u0007((object) \u0002\u0005.\u007E\u009B\u0008((object) \u008F\u0006.\u007E\u0080\u0008((object) \u0013\u0007.\u008D\u0008())));
        string str3 = num3.ToString();
        strArray4[index3] = str3;
        string str4 = \u001C\u0004.\u0005\u0006(strArray1);
        obj((object) this, str4);
      }
      catch (Exception ex)
      {
        string[] strArray2 = strArray1;
        // ISSUE: variable of a boxed type
        __Boxed<int> local1 = (ValueType) num1;
        // ISSUE: variable of a boxed type
        __Boxed<int> local2 = (ValueType) num2;
        // ISSUE: variable of a boxed type
        __Boxed<int> local3 = (ValueType) num3;
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) strArray2, (object) local1, (object) local2, (object) local3, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u0003()
    {
      XmlTextWriter xmlTextWriter1;
      int num;
      try
      {
        \u0002.\u0004(\u009D\u0007.\u007E\u009E\u0010((object) this.\u0001));
        \u0002.\u0005(\u009D\u0007.\u007E\u009E\u0010((object) this.\u0002));
        \u0002.\u0003(\u009D\u0007.\u007E\u009E\u0010((object) this.\u0003));
        \u0002.\u0003(\u0089\u0006.\u0097\u0006(\u009D\u0007.\u007E\u009E\u0010((object) this.\u0004), 10));
        xmlTextWriter1 = new XmlTextWriter(\u0014\u0003.\u0002\u0006(\u0083\u0004.\u0018\u0010(), \u0004.\u0001(16585)), (Encoding) null);
        \u0019\u0007.\u007E\u008B\u000F((object) xmlTextWriter1, Formatting.Indented);
        \u0001\u0007.\u007E\u001C\u000F((object) xmlTextWriter1);
        \u0097\u0005.\u007E\u001E\u000F((object) xmlTextWriter1, \u0004.\u0001(16778));
        \u0097\u0005.\u007E\u001E\u000F((object) xmlTextWriter1, \u0004.\u0001(16787));
        \u0097\u0005.\u007E\u0080\u000F((object) xmlTextWriter1, \u009D\u0007.\u007E\u001C\u0005((object) \u0002.\u0003()));
        \u0001\u0007.\u007E\u001F\u000F((object) xmlTextWriter1);
        \u0097\u0005.\u007E\u001E\u000F((object) xmlTextWriter1, \u0004.\u0001(16796));
        \u0097\u0005 obj = \u0097\u0005.\u007E\u0080\u000F;
        XmlTextWriter xmlTextWriter2 = xmlTextWriter1;
        num = \u0002.\u0003();
        string str = num.ToString();
        obj((object) xmlTextWriter2, str);
        \u0001\u0007.\u007E\u001F\u000F((object) xmlTextWriter1);
        \u0097\u0005.\u007E\u001E\u000F((object) xmlTextWriter1, \u0004.\u0001(16805));
        \u0097\u0005.\u007E\u0080\u000F((object) xmlTextWriter1, \u009D\u0007.\u007E\u009E\u0010((object) this.\u0001));
        \u0001\u0007.\u007E\u001F\u000F((object) xmlTextWriter1);
        \u0097\u0005.\u007E\u001E\u000F((object) xmlTextWriter1, \u0004.\u0001(16814));
        \u0097\u0005.\u007E\u0080\u000F((object) xmlTextWriter1, \u009D\u0007.\u007E\u009E\u0010((object) this.\u0002));
        \u0001\u0007.\u007E\u001F\u000F((object) xmlTextWriter1);
        \u0001\u0007.\u007E\u001F\u000F((object) xmlTextWriter1);
        \u0001\u0007.\u007E\u001D\u000F((object) xmlTextWriter1);
        \u0001\u0007.\u007E\u0081\u000F((object) xmlTextWriter1);
      }
      catch (Exception ex)
      {
        XmlTextWriter xmlTextWriter2 = xmlTextWriter1;
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) num;
        throw UnhandledException.\u0003(ex, (object) xmlTextWriter2, (object) local, (object) this);
      }
    }

    protected override void Dispose([In] bool obj0)
    {
      try
      {
        if (obj0 && this.\u0001 != null)
          \u0001\u0007.\u007E\u0089\u0005((object) this.\u0001);
        \u0011\u0003.\u008C\u0004((object) this, obj0);
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<bool> local = (ValueType) obj0;
        throw UnhandledException.\u0003(ex, (object) this, (object) local);
      }
    }

    private void \u0004()
    {
      ComponentResourceManager componentResourceManager1;
      try
      {
        this.\u0001 = (IContainer) new Container();
        // ISSUE: type reference
        componentResourceManager1 = new ComponentResourceManager(\u0014\u0006.\u001A\u0007(__typeref (\u0004)));
        this.\u0001 = new Label();
        this.\u0002 = new Label();
        this.\u0001 = new TextBox();
        this.\u0002 = new TextBox();
        this.\u0001 = new Button();
        this.\u0002 = new Button();
        this.\u0003 = new Label();
        this.\u0003 = new TextBox();
        this.\u0004 = new TextBox();
        this.\u0004 = new Label();
        this.\u0001 = new DefaultLookAndFeel(this.\u0001);
        \u0001\u0007.\u008D\u0004((object) this);
        \u0011\u0003.\u007E\u0080\u0010((object) this.\u0001, true);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0001, new Point(37, 15));
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0001, \u0004.\u0001(8703));
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0001, new Size(59, 13));
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0001, 0);
        \u0097\u0005.\u007E\u009F\u0010((object) this.\u0001, \u0004.\u0001(16823));
        \u0011\u0003.\u007E\u0080\u0010((object) this.\u0002, true);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0002, new Point(39, 41));
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0002, \u0004.\u0001(16836));
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0002, new Size(57, 13));
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0002, 1);
        \u0097\u0005.\u007E\u009F\u0010((object) this.\u0002, \u0004.\u0001(16845));
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0001, new Point(101, 12));
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0001, \u0004.\u0001(16858));
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0001, new Size(142, 21));
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0001, 3);
        \u0011\u0006.\u007E\u0001\u0011((object) this.\u0001, new EventHandler(this.\u0005));
        \u001A\u0007.\u007E\u000F\u0011((object) this.\u0001, new KeyPressEventHandler(this.\u0003));
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0002, new Point(101, 38));
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0002, \u0004.\u0001(16879));
        \u0097\u0003.\u007E\u0008\u0012((object) this.\u0002, '*');
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0002, new Size(142, 21));
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0002, 4);
        \u0011\u0006.\u007E\u0001\u0011((object) this.\u0002, new EventHandler(this.\u0005));
        \u001A\u0007.\u007E\u000F\u0011((object) this.\u0002, new KeyPressEventHandler(this.\u0003));
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0001, new Point(18, 123));
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0001, \u0004.\u0001(16900));
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0001, new Size(77, 28));
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0001, 8);
        \u0097\u0005.\u007E\u009F\u0010((object) this.\u0001, \u0004.\u0001(16913));
        \u0011\u0003.\u007E\u008D\u0011((object) this.\u0001, true);
        \u0011\u0006.\u007E\u0006\u0011((object) this.\u0001, new EventHandler(this.\u0004));
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0002, new Point(189, 123));
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0002, \u0004.\u0001(16922));
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0002, new Size(77, 28));
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0002, 7);
        \u0097\u0005.\u007E\u009F\u0010((object) this.\u0002, \u0004.\u0001(16939));
        \u0011\u0003.\u007E\u008D\u0011((object) this.\u0002, true);
        \u0011\u0006.\u007E\u0006\u0011((object) this.\u0002, new EventHandler(this.\u0003));
        \u0011\u0003.\u007E\u0080\u0010((object) this.\u0003, true);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0003, new Point(35, 67));
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0003, \u0004.\u0001(16948));
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0003, new Size(55, 13));
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0003, 2);
        \u0097\u0005.\u007E\u009F\u0010((object) this.\u0003, \u0004.\u0001(16957));
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0003, new Point(101, 64));
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0003, \u0004.\u0001(16970));
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0003, new Size(142, 21));
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0003, 5);
        \u0011\u0006.\u007E\u0001\u0011((object) this.\u0003, new EventHandler(this.\u0005));
        \u001A\u0007.\u007E\u000F\u0011((object) this.\u0003, new KeyPressEventHandler(this.\u0003));
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0004, new Point(101, 90));
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0004, \u0004.\u0001(16983));
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0004, new Size(61, 21));
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0004, 6);
        \u0011\u0006.\u007E\u0001\u0011((object) this.\u0004, new EventHandler(this.\u0005));
        \u001A\u0007.\u007E\u000F\u0011((object) this.\u0004, new KeyPressEventHandler(this.\u0003));
        \u0011\u0003.\u007E\u0080\u0010((object) this.\u0004, true);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0004, new Point(35, 93));
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0004, \u0004.\u0001(16996));
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0004, new Size(53, 13));
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0004, 8);
        \u0097\u0005.\u007E\u009F\u0010((object) this.\u0004, \u0004.\u0001(17005));
        \u0097\u0005.\u007E\u009A\u0004((object) \u009A\u0006.\u007E\u009C\u0004((object) this.\u0001), \u0004.\u0001(12619));
        \u008D\u0003.\u0083\u0011((object) this, new SizeF(6f, 13f));
        \u0090\u0003.\u0084\u0011((object) this, AutoScaleMode.Font);
        \u0082\u0004.\u0015\u0012((object) this, new Size(278, 158));
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u0088\u0010((object) this), (Control) this.\u0004);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u0088\u0010((object) this), (Control) this.\u0004);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u0088\u0010((object) this), (Control) this.\u0002);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u0088\u0010((object) this), (Control) this.\u0001);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u0088\u0010((object) this), (Control) this.\u0003);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u0088\u0010((object) this), (Control) this.\u0002);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u0088\u0010((object) this), (Control) this.\u0001);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u0088\u0010((object) this), (Control) this.\u0003);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u0088\u0010((object) this), (Control) this.\u0002);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u0088\u0010((object) this), (Control) this.\u0001);
        \u0086\u0007.\u0013\u0012((object) this, FormBorderStyle.FixedDialog);
        \u001D\u0006.\u0017\u0012((object) this, (Icon) \u008B\u0005.\u007E\u001B\u000E((object) componentResourceManager1, \u0004.\u0001(9598)));
        \u0097\u0005.\u007E\u009A\u0004((object) \u009A\u0006.\u008F\u0004((object) this), \u0004.\u0001(12619));
        \u0011\u0003.\u007E\u0099\u0004((object) \u009A\u0006.\u008F\u0004((object) this), true);
        \u0011\u0003.\u0018\u0012((object) this, false);
        \u0011\u0003.\u0019\u0012((object) this, false);
        \u0097\u0005.\u0098\u0010((object) this, \u0004.\u0001(16939));
        \u0007\u0005.\u001E\u0012((object) this, FormStartPosition.CenterScreen);
        \u0097\u0005.\u007E\u009F\u0010((object) this, \u0004.\u0001(17018));
        \u0011\u0006.\u007F\u0012((object) this, new EventHandler(this.\u0006));
        \u0011\u0003.\u008E\u0004((object) this, false);
        \u0001\u0007.\u0019\u0011((object) this);
      }
      catch (Exception ex)
      {
        ComponentResourceManager componentResourceManager2 = componentResourceManager1;
        throw UnhandledException.\u0003(ex, (object) componentResourceManager2, (object) this);
      }
    }

    static \u0004()
    {
      \u0003.\u0003();
    }
  }
}
