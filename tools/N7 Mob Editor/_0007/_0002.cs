﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: MobEditor, Version=1.1.0.2, Culture=neutral, PublicKeyToken=016197d45384ac33
// MVID: 35831B81-08C7-444B-81F1-3EE10E122F41
// Assembly location: D:\Server\eab\Tools\N7 Mob Editor.exe

using \u0001;
using \u0006;
using \u0007;
using \u0008;
using DevExpress.Data;
using DevExpress.LookAndFeel;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Layout;
using DevExpress.XtraLayout;
using DevExpress.XtraLayout.Utils;
using DevExpress.XtraTab;
using DevExpress.XtraVerticalGrid;
using DevExpress.XtraVerticalGrid.Rows;
using SmartAssembly.SmartExceptionsCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace \u0007
{
  internal sealed class \u0002 : XtraForm
  {
    private Hashtable \u0001 = new Hashtable();
    public DataTable \u0001 = new DataTable();
    public DataTable \u0002 = new DataTable();
    private DataTable \u0003 = new DataTable();
    private DataTable \u0004 = new DataTable();
    private DataTable \u0005 = new DataTable();
    private DataSet \u0001 = new DataSet();
    private string \u0002 = \u0002.\u0001(12512);
    private string \u0003 = \u0002.\u0001(12601);
    [NonSerialized]
    internal static \u0002 \u0001;
    private IContainer \u0001;
    private BarManager \u0001;
    private Bar \u0001;
    private Bar \u0002;
    private BarDockControl \u0001;
    private BarDockControl \u0002;
    private BarDockControl \u0003;
    private BarDockControl \u0004;
    private BarSubItem \u0001;
    private BarButtonItem \u0001;
    private Bar \u0003;
    private BarButtonItem \u0002;
    private BarButtonItem \u0003;
    private BarButtonItem \u0004;
    private BarButtonItem \u0005;
    private BarButtonItem \u0006;
    private BarButtonItem \u0007;
    private BarSubItem \u0002;
    private BarButtonItem \u0008;
    private BarSubItem \u0003;
    private BarButtonItem \u000E;
    private PanelControl \u0001;
    private DefaultLookAndFeel \u0001;
    private ContextMenuStrip \u0001;
    private ToolStripMenuItem \u0001;
    private ToolStripMenuItem \u0002;
    private ContextMenuStrip \u0002;
    private ToolStripMenuItem \u0003;
    private ToolStripMenuItem \u0004;
    private SplitContainerControl \u0001;
    private XtraTabControl \u0001;
    private XtraTabPage \u0001;
    private LayoutControl \u0001;
    private LookUpEdit \u0001;
    private LookUpEdit \u0002;
    private TextEdit \u0001;
    private ColorEdit \u0001;
    private LookUpEdit \u0003;
    private MemoEdit \u0001;
    private TextEdit \u0002;
    private ComboBoxEdit \u0001;
    private TextEdit \u0003;
    private LayoutControlGroup \u0001;
    private LayoutControlItem \u0001;
    private LayoutControlItem \u0002;
    private LayoutControlItem \u0003;
    private LayoutControlItem \u0004;
    private LayoutControlItem \u0005;
    private LayoutControlItem \u0006;
    private LayoutControlItem \u0007;
    private LayoutControlItem \u0008;
    private LayoutControlItem \u000E;
    private XtraTabPage \u0002;
    private LayoutControl \u0002;
    private VGridControl \u0001;
    private RepositoryItemTextEdit \u0001;
    private CategoryRow \u0001;
    private EditorRow \u0001;
    private EditorRow \u0002;
    private EditorRow \u0003;
    private GridControl \u0001;
    private LayoutView \u0001;
    private LayoutViewColumn \u0001;
    private RepositoryItemPictureEdit \u0001;
    private LayoutViewField \u0001;
    private LayoutViewColumn \u0002;
    private LayoutViewField \u0002;
    private LayoutViewColumn \u0003;
    private RepositoryItemLookUpEdit \u0001;
    private LayoutViewField \u0003;
    private LayoutViewCard \u0001;
    private RepositoryItemImageComboBox \u0001;
    private GridView \u0001;
    private LayoutControlGroup \u0002;
    private LayoutControlItem \u000F;
    private LayoutControlItem \u0010;
    private XtraTabPage \u0003;
    private LayoutControl \u0003;
    private GridControl \u0002;
    private LayoutView \u0002;
    private LayoutViewColumn \u0004;
    private RepositoryItemPictureEdit \u0002;
    private LayoutViewField \u0004;
    private LayoutViewColumn \u0005;
    private LayoutViewField \u0005;
    private LayoutViewColumn \u0006;
    private RepositoryItemLookUpEdit \u0002;
    private LayoutViewField \u0006;
    private LayoutViewCard \u0002;
    private VGridControl \u0002;
    private RepositoryItemTextEdit \u0002;
    private CategoryRow \u0002;
    private EditorRow \u0004;
    private EditorRow \u0005;
    private LayoutControlGroup \u0003;
    private LayoutControlItem \u0011;
    private LayoutControlItem \u0012;
    private GridControl \u0003;
    private GridView \u0002;
    private GridColumn \u0001;
    private GridColumn \u0002;
    private GridColumn \u0003;
    private TextEdit \u0004;
    private LayoutControlItem \u0013;
    private SimpleButton \u0001;
    private PictureEdit \u0001;
    private LayoutControlItem \u0014;
    private LayoutControlItem \u0015;
    private XtraTabPage \u0004;
    private LayoutControl \u0004;
    private LayoutControlGroup \u0004;
    private LookUpEdit \u0004;
    private LookUpEdit \u0005;
    private LookUpEdit \u0006;
    private LookUpEdit \u0007;
    private LookUpEdit \u0008;
    private LookUpEdit \u000E;
    private LookUpEdit \u000F;
    private LookUpEdit \u0010;
    private LayoutControlItem \u0016;
    private LayoutControlItem \u0017;
    private LayoutControlItem \u0018;
    private LayoutControlItem \u0019;
    private LayoutControlItem \u001A;
    private LayoutControlItem \u001B;
    private LayoutControlItem \u001C;
    private LayoutControlItem \u001D;
    private LookUpEdit \u0011;
    private LookUpEdit \u0012;
    private LayoutControlItem \u001E;
    private LayoutControlItem \u001F;
    private \u0006.\u0006 \u0001;
    private string \u0001;
    private int \u0001;
    private int \u0002;
    private bool \u0001;
    public \u0007.\u0006 \u0001;

    protected override void Dispose([In] bool obj0)
    {
      try
      {
        if (obj0 && this.\u0001 != null)
          \u0001\u0007.\u007E\u0089\u0005((object) this.\u0001);
        \u0011\u0003.\u008C\u0004((object) this, obj0);
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<bool> local = (ValueType) obj0;
        throw UnhandledException.\u0003(ex, (object) this, (object) local);
      }
    }

    private void \u0003()
    {
      Bar[] barArray1;
      BarItem[] barItemArray1;
      LinkPersistInfo[] linkPersistInfoArray1;
      LinkPersistInfo[] linkPersistInfoArray2;
      LinkPersistInfo[] linkPersistInfoArray3;
      LinkPersistInfo[] linkPersistInfoArray4;
      LinkPersistInfo[] linkPersistInfoArray5;
      XtraTabPage[] xtraTabPageArray1;
      EditorButton[] editorButtonArray1;
      EditorButton[] editorButtonArray2;
      EditorButton[] editorButtonArray3;
      EditorButton[] editorButtonArray4;
      EditorButton[] editorButtonArray5;
      BaseLayoutItem[] baseLayoutItemArray1;
      RepositoryItem[] repositoryItemArray1;
      BaseRow[] baseRowArray1;
      BaseRow[] baseRowArray2;
      RepositoryItem[] repositoryItemArray2;
      BaseView[] baseViewArray1;
      ToolStripItem[] toolStripItemArray1;
      LayoutViewColumn[] layoutViewColumnArray1;
      EditorButton[] editorButtonArray6;
      BaseLayoutItem[] baseLayoutItemArray2;
      EditorButton[] editorButtonArray7;
      BaseLayoutItem[] baseLayoutItemArray3;
      RepositoryItem[] repositoryItemArray3;
      BaseView[] baseViewArray2;
      ToolStripItem[] toolStripItemArray2;
      LayoutViewColumn[] layoutViewColumnArray2;
      EditorButton[] editorButtonArray8;
      BaseLayoutItem[] baseLayoutItemArray4;
      RepositoryItem[] repositoryItemArray4;
      BaseRow[] baseRowArray3;
      BaseRow[] baseRowArray4;
      BaseLayoutItem[] baseLayoutItemArray5;
      EditorButton[] editorButtonArray9;
      EditorButton[] editorButtonArray10;
      EditorButton[] editorButtonArray11;
      EditorButton[] editorButtonArray12;
      EditorButton[] editorButtonArray13;
      EditorButton[] editorButtonArray14;
      EditorButton[] editorButtonArray15;
      EditorButton[] editorButtonArray16;
      EditorButton[] editorButtonArray17;
      EditorButton[] editorButtonArray18;
      BaseLayoutItem[] baseLayoutItemArray6;
      BaseView[] baseViewArray3;
      GridColumn[] gridColumnArray1;
      ComponentResourceManager componentResourceManager;
      try
      {
        this.\u0001 = (IContainer) new Container();
        // ISSUE: type reference
        componentResourceManager = new ComponentResourceManager(\u0014\u0006.\u001A\u0007(__typeref (\u0002)));
        this.\u0001 = new BarManager(this.\u0001);
        this.\u0001 = new Bar();
        this.\u0001 = new BarSubItem();
        this.\u0006 = new BarButtonItem();
        this.\u0007 = new BarButtonItem();
        this.\u0001 = new BarButtonItem();
        this.\u0002 = new BarSubItem();
        this.\u0008 = new BarButtonItem();
        this.\u0003 = new BarSubItem();
        this.\u000E = new BarButtonItem();
        this.\u0002 = new Bar();
        this.\u0003 = new Bar();
        this.\u0002 = new BarButtonItem();
        this.\u0003 = new BarButtonItem();
        this.\u0004 = new BarButtonItem();
        this.\u0005 = new BarButtonItem();
        this.\u0001 = new BarDockControl();
        this.\u0002 = new BarDockControl();
        this.\u0003 = new BarDockControl();
        this.\u0004 = new BarDockControl();
        this.\u0001 = new PanelControl();
        this.\u0001 = new SplitContainerControl();
        this.\u0001 = new XtraTabControl();
        this.\u0001 = new XtraTabPage();
        this.\u0001 = new LayoutControl();
        this.\u0001 = new SimpleButton();
        this.\u0004 = new TextEdit();
        this.\u0001 = new LookUpEdit();
        this.\u0002 = new LookUpEdit();
        this.\u0001 = new TextEdit();
        this.\u0001 = new PictureEdit();
        this.\u0001 = new ColorEdit();
        this.\u0003 = new LookUpEdit();
        this.\u0001 = new MemoEdit();
        this.\u0002 = new TextEdit();
        this.\u0001 = new ComboBoxEdit();
        this.\u0003 = new TextEdit();
        this.\u0001 = new LayoutControlGroup();
        this.\u0001 = new LayoutControlItem();
        this.\u0002 = new LayoutControlItem();
        this.\u0003 = new LayoutControlItem();
        this.\u0004 = new LayoutControlItem();
        this.\u0014 = new LayoutControlItem();
        this.\u0006 = new LayoutControlItem();
        this.\u0007 = new LayoutControlItem();
        this.\u0008 = new LayoutControlItem();
        this.\u000E = new LayoutControlItem();
        this.\u0005 = new LayoutControlItem();
        this.\u0013 = new LayoutControlItem();
        this.\u0015 = new LayoutControlItem();
        this.\u0002 = new XtraTabPage();
        this.\u0002 = new LayoutControl();
        this.\u0001 = new VGridControl();
        this.\u0001 = new RepositoryItemTextEdit();
        this.\u0001 = new CategoryRow();
        this.\u0001 = new EditorRow();
        this.\u0002 = new EditorRow();
        this.\u0003 = new EditorRow();
        this.\u0001 = new GridControl();
        this.\u0001 = new ContextMenuStrip(this.\u0001);
        this.\u0001 = new ToolStripMenuItem();
        this.\u0002 = new ToolStripMenuItem();
        this.\u0001 = new LayoutView();
        this.\u0001 = new LayoutViewColumn();
        this.\u0001 = new RepositoryItemPictureEdit();
        this.\u0001 = new LayoutViewField();
        this.\u0002 = new LayoutViewColumn();
        this.\u0002 = new LayoutViewField();
        this.\u0003 = new LayoutViewColumn();
        this.\u0001 = new RepositoryItemLookUpEdit();
        this.\u0003 = new LayoutViewField();
        this.\u0001 = new LayoutViewCard();
        this.\u0001 = new RepositoryItemImageComboBox();
        this.\u0001 = new GridView();
        this.\u0002 = new LayoutControlGroup();
        this.\u000F = new LayoutControlItem();
        this.\u0010 = new LayoutControlItem();
        this.\u0003 = new XtraTabPage();
        this.\u0003 = new LayoutControl();
        this.\u0002 = new GridControl();
        this.\u0002 = new ContextMenuStrip(this.\u0001);
        this.\u0003 = new ToolStripMenuItem();
        this.\u0004 = new ToolStripMenuItem();
        this.\u0002 = new LayoutView();
        this.\u0004 = new LayoutViewColumn();
        this.\u0002 = new RepositoryItemPictureEdit();
        this.\u0004 = new LayoutViewField();
        this.\u0005 = new LayoutViewColumn();
        this.\u0005 = new LayoutViewField();
        this.\u0006 = new LayoutViewColumn();
        this.\u0002 = new RepositoryItemLookUpEdit();
        this.\u0006 = new LayoutViewField();
        this.\u0002 = new LayoutViewCard();
        this.\u0002 = new VGridControl();
        this.\u0002 = new RepositoryItemTextEdit();
        this.\u0002 = new CategoryRow();
        this.\u0004 = new EditorRow();
        this.\u0005 = new EditorRow();
        this.\u0003 = new LayoutControlGroup();
        this.\u0011 = new LayoutControlItem();
        this.\u0012 = new LayoutControlItem();
        this.\u0004 = new XtraTabPage();
        this.\u0004 = new LayoutControl();
        this.\u0011 = new LookUpEdit();
        this.\u0012 = new LookUpEdit();
        this.\u0004 = new LookUpEdit();
        this.\u0008 = new LookUpEdit();
        this.\u0005 = new LookUpEdit();
        this.\u0006 = new LookUpEdit();
        this.\u000E = new LookUpEdit();
        this.\u0007 = new LookUpEdit();
        this.\u0010 = new LookUpEdit();
        this.\u000F = new LookUpEdit();
        this.\u0004 = new LayoutControlGroup();
        this.\u0018 = new LayoutControlItem();
        this.\u0019 = new LayoutControlItem();
        this.\u0017 = new LayoutControlItem();
        this.\u0016 = new LayoutControlItem();
        this.\u001A = new LayoutControlItem();
        this.\u001E = new LayoutControlItem();
        this.\u001B = new LayoutControlItem();
        this.\u001C = new LayoutControlItem();
        this.\u001D = new LayoutControlItem();
        this.\u001F = new LayoutControlItem();
        this.\u0003 = new GridControl();
        this.\u0002 = new GridView();
        this.\u0001 = new GridColumn();
        this.\u0002 = new GridColumn();
        this.\u0003 = new GridColumn();
        this.\u0001 = new DefaultLookAndFeel(this.\u0001);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u007F\u0011((object) this.\u0001);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u007F\u0011((object) this.\u0001);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u007F\u0011((object) this.\u0001);
        \u0001\u0007.\u007E\u007F\u0011((object) this.\u0001);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u007F\u0011((object) this.\u0001);
        \u0001\u0007.\u007E\u008B\u0013((object) \u009E\u0005.\u007E\u0001\u0004((object) this.\u0004));
        \u0001\u0007.\u007E\u008B\u0013((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0001));
        \u0001\u0007.\u007E\u008B\u0013((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0002));
        \u0001\u0007.\u007E\u008B\u0013((object) \u009E\u0005.\u007E\u0001\u0004((object) this.\u0001));
        \u0001\u0007.\u007E\u008B\u0013((object) \u001E\u0003.\u007E\u0014\u0004((object) this.\u0001));
        \u0001\u0007.\u007E\u008B\u0013((object) \u0098\u0004.\u007E\u0016\u0004((object) this.\u0001));
        \u0001\u0007.\u007E\u008B\u0013((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0003));
        \u0001\u0007.\u007E\u008B\u0013((object) \u0095\u0005.\u007E\u000E\u0004((object) this.\u0001));
        \u0001\u0007.\u007E\u008B\u0013((object) \u009E\u0005.\u007E\u0001\u0004((object) this.\u0002));
        \u0001\u0007.\u007E\u008B\u0013((object) \u009B\u0002.\u007E\u001C\u0004((object) this.\u0001));
        \u0001\u0007.\u007E\u008B\u0013((object) \u009E\u0005.\u007E\u0001\u0004((object) this.\u0003));
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0002);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0003);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0004);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0014);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0006);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0007);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0008);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u000E);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0005);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0013);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0015);
        \u0001\u0007.\u007E\u007F\u0011((object) this.\u0002);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0002);
        \u0001\u0007.\u007E\u007F\u0011((object) this.\u0002);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u007F\u0011((object) this.\u0001);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0002);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0003);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0002);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u000F);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0010);
        \u0001\u0007.\u007E\u007F\u0011((object) this.\u0003);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0003);
        \u0001\u0007.\u007E\u007F\u0011((object) this.\u0003);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0002);
        \u0001\u0007.\u007E\u007F\u0011((object) this.\u0002);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0002);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0002);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0004);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0005);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0002);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0006);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0002);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0002);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0002);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0003);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0011);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0012);
        \u0001\u0007.\u007E\u007F\u0011((object) this.\u0004);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0004);
        \u0001\u0007.\u007E\u007F\u0011((object) this.\u0004);
        \u0001\u0007.\u007E\u008B\u0013((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0011));
        \u0001\u0007.\u007E\u008B\u0013((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0012));
        \u0001\u0007.\u007E\u008B\u0013((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0004));
        \u0001\u0007.\u007E\u008B\u0013((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0008));
        \u0001\u0007.\u007E\u008B\u0013((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0005));
        \u0001\u0007.\u007E\u008B\u0013((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0006));
        \u0001\u0007.\u007E\u008B\u0013((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u000E));
        \u0001\u0007.\u007E\u008B\u0013((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0007));
        \u0001\u0007.\u007E\u008B\u0013((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0010));
        \u0001\u0007.\u007E\u008B\u0013((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u000F));
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0004);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0018);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0019);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0017);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0016);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u001A);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u001E);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u001B);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u001C);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u001D);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u001F);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0003);
        \u0001\u0007.\u007E\u008B\u0013((object) this.\u0002);
        \u0001\u0007.\u008D\u0004((object) this);
        \u0083\u0003 obj1 = \u0083\u0003.\u007E\u000F\u0002;
        Bars bars = \u009F\u0003.\u007E\u001B\u0002((object) this.\u0001);
        barArray1 = new Bar[3]
        {
          this.\u0001,
          this.\u0002,
          this.\u0003
        };
        Bar[] barArray2 = barArray1;
        obj1((object) bars, barArray2);
        int num1 = \u0092\u0002.\u007E\u008D\u0002((object) \u0017\u0006.\u007E\u001A\u0002((object) this.\u0001), this.\u0001);
        int num2 = \u0092\u0002.\u007E\u008D\u0002((object) \u0017\u0006.\u007E\u001A\u0002((object) this.\u0001), this.\u0002);
        int num3 = \u0092\u0002.\u007E\u008D\u0002((object) \u0017\u0006.\u007E\u001A\u0002((object) this.\u0001), this.\u0003);
        int num4 = \u0092\u0002.\u007E\u008D\u0002((object) \u0017\u0006.\u007E\u001A\u0002((object) this.\u0001), this.\u0004);
        \u008B\u0006.\u007E\u001C\u0002((object) this.\u0001, (Control) this);
        \u009F\u0007 obj2 = \u009F\u0007.\u007E\u0091\u0002;
        BarItems barItems = \u0015\u0004.\u007E\u001D\u0002((object) this.\u0001);
        barItemArray1 = new BarItem[12]
        {
          (BarItem) this.\u0001,
          (BarItem) this.\u0001,
          (BarItem) this.\u0002,
          (BarItem) this.\u0003,
          (BarItem) this.\u0004,
          (BarItem) this.\u0005,
          (BarItem) this.\u0006,
          (BarItem) this.\u0007,
          (BarItem) this.\u0002,
          (BarItem) this.\u0008,
          (BarItem) this.\u0003,
          (BarItem) this.\u000E
        };
        BarItem[] barItemArray2 = barItemArray1;
        obj2((object) barItems, barItemArray2);
        \u008C\u0006.\u007E\u001E\u0002((object) this.\u0001, this.\u0001);
        \u008A\u0004.\u007E\u0019\u0002((object) this.\u0001, 15);
        \u008C\u0006.\u007E\u001F\u0002((object) this.\u0001, this.\u0002);
        \u0097\u0005.\u007E\u0003\u0002((object) this.\u0001, \u0002.\u0001(9179));
        \u008A\u0004.\u007E\u000E\u0002((object) this.\u0001, 0);
        \u008A\u0004.\u007E\u0008\u0002((object) this.\u0001, 0);
        \u0091\u0007.\u007E\u0006\u0002((object) this.\u0001, BarDockStyle.Top);
        \u008F\u0005 obj3 = \u008F\u0005.\u007E\u0094\u0002;
        LinksInfo linksInfo1 = \u0096\u0005.\u007E\u0007\u0002((object) this.\u0001);
        linkPersistInfoArray1 = new LinkPersistInfo[3]
        {
          new LinkPersistInfo((BarItem) this.\u0001),
          new LinkPersistInfo((BarItem) this.\u0002),
          new LinkPersistInfo((BarItem) this.\u0003)
        };
        LinkPersistInfo[] linkPersistInfoArray6 = linkPersistInfoArray1;
        obj3((object) linksInfo1, linkPersistInfoArray6);
        \u0011\u0003.\u007E\u0011\u0002((object) \u0092\u0004.\u007E\u0001\u0002((object) this.\u0001), false);
        \u0011\u0003.\u007E\u0012\u0002((object) \u0092\u0004.\u007E\u0001\u0002((object) this.\u0001), true);
        \u0011\u0003.\u007E\u0013\u0002((object) \u0092\u0004.\u007E\u0001\u0002((object) this.\u0001), true);
        \u0011\u0003.\u007E\u0015\u0002((object) \u0092\u0004.\u007E\u0001\u0002((object) this.\u0001), true);
        \u0011\u0003.\u007E\u0016\u0002((object) \u0092\u0004.\u007E\u0001\u0002((object) this.\u0001), true);
        \u0097\u0005.\u007E\u0004\u0002((object) this.\u0001, \u0002.\u0001(9179));
        \u0097\u0005.\u007E\u0084\u0002((object) this.\u0001, \u0002.\u0001(9491));
        \u008A\u0004.\u007E\u0080\u0002((object) this.\u0001, 1);
        \u008F\u0005 obj4 = \u008F\u0005.\u007E\u0094\u0002;
        LinksInfo linksInfo2 = \u0096\u0005.\u007E\u008F\u0002((object) this.\u0001);
        linkPersistInfoArray2 = new LinkPersistInfo[3]
        {
          new LinkPersistInfo((BarItem) this.\u0006),
          new LinkPersistInfo((BarItem) this.\u0007),
          new LinkPersistInfo((BarItem) this.\u0001, true)
        };
        LinkPersistInfo[] linkPersistInfoArray7 = linkPersistInfoArray2;
        obj4((object) linksInfo2, linkPersistInfoArray7);
        \u0097\u0005.\u007E\u007F\u0002((object) this.\u0001, \u0002.\u0001(9500));
        \u0097\u0005.\u007E\u0084\u0002((object) this.\u0006, \u0002.\u0001(9517));
        // ISSUE: reference to a compiler-generated method
        \u0081\u0004.\u007E\u0082\u0002((object) this.\u0006, (Image) \u0005.\u0003());
        \u008A\u0004.\u007E\u0080\u0002((object) this.\u0006, 7);
        \u0097\u0005.\u007E\u007F\u0002((object) this.\u0006, \u0002.\u0001(9526));
        \u0097\u0005.\u007E\u0084\u0002((object) this.\u0007, \u0002.\u0001(9547));
        // ISSUE: reference to a compiler-generated method
        \u0081\u0004.\u007E\u0082\u0002((object) this.\u0007, (Image) \u0005.\u0004());
        \u008A\u0004.\u007E\u0080\u0002((object) this.\u0007, 8);
        \u0097\u0005.\u007E\u007F\u0002((object) this.\u0007, \u0002.\u0001(9556));
        \u0097\u0005.\u007E\u0084\u0002((object) this.\u0001, \u0002.\u0001(9577));
        \u008A\u0004.\u007E\u0080\u0002((object) this.\u0001, 2);
        \u0097\u0005.\u007E\u007F\u0002((object) this.\u0001, \u0002.\u0001(9586));
        \u0097\u0005.\u007E\u0084\u0002((object) this.\u0002, \u0002.\u0001(9607));
        \u008A\u0004.\u007E\u0080\u0002((object) this.\u0002, 9);
        \u008F\u0005 obj5 = \u008F\u0005.\u007E\u0094\u0002;
        LinksInfo linksInfo3 = \u0096\u0005.\u007E\u008F\u0002((object) this.\u0002);
        linkPersistInfoArray3 = new LinkPersistInfo[1]
        {
          new LinkPersistInfo((BarItem) this.\u0008)
        };
        LinkPersistInfo[] linkPersistInfoArray8 = linkPersistInfoArray3;
        obj5((object) linksInfo3, linkPersistInfoArray8);
        \u0097\u0005.\u007E\u007F\u0002((object) this.\u0002, \u0002.\u0001(9616));
        \u0097\u0005.\u007E\u0084\u0002((object) this.\u0008, \u0002.\u0001(9633));
        // ISSUE: reference to a compiler-generated method
        \u0081\u0004.\u007E\u0082\u0002((object) this.\u0008, (Image) \u0005.\u0008());
        \u008A\u0004.\u007E\u0080\u0002((object) this.\u0008, 10);
        \u0097\u0005.\u007E\u007F\u0002((object) this.\u0008, \u0002.\u0001(9646));
        \u0097\u0005.\u007E\u0084\u0002((object) this.\u0003, \u0002.\u0001(9667));
        \u008A\u0004.\u007E\u0080\u0002((object) this.\u0003, 12);
        \u008F\u0005 obj6 = \u008F\u0005.\u007E\u0094\u0002;
        LinksInfo linksInfo4 = \u0096\u0005.\u007E\u008F\u0002((object) this.\u0003);
        linkPersistInfoArray4 = new LinkPersistInfo[1]
        {
          new LinkPersistInfo((BarItem) this.\u000E)
        };
        LinkPersistInfo[] linkPersistInfoArray9 = linkPersistInfoArray4;
        obj6((object) linksInfo4, linkPersistInfoArray9);
        \u0097\u0005.\u007E\u007F\u0002((object) this.\u0003, \u0002.\u0001(9676));
        \u0097\u0005.\u007E\u0084\u0002((object) this.\u000E, \u0002.\u0001(9693));
        // ISSUE: reference to a compiler-generated method
        \u0081\u0004.\u007E\u0082\u0002((object) this.\u000E, (Image) \u0005.\u0005());
        \u008A\u0004.\u007E\u0080\u0002((object) this.\u000E, 13);
        \u0097\u0005.\u007E\u007F\u0002((object) this.\u000E, \u0002.\u0001(9706));
        \u0012\u0005.\u007E\u0086\u0002((object) this.\u000E, new ItemClickEventHandler(this.\u0007));
        \u0097\u0005.\u007E\u0003\u0002((object) this.\u0002, \u0002.\u0001(9394));
        \u009D\u0004.\u007E\u0005\u0002((object) this.\u0002, BarCanDockStyle.Bottom);
        \u008A\u0004.\u007E\u000E\u0002((object) this.\u0002, 0);
        \u008A\u0004.\u007E\u0008\u0002((object) this.\u0002, 0);
        \u0091\u0007.\u007E\u0006\u0002((object) this.\u0002, BarDockStyle.Bottom);
        \u0011\u0003.\u007E\u0011\u0002((object) \u0092\u0004.\u007E\u0001\u0002((object) this.\u0002), false);
        \u0011\u0003.\u007E\u0014\u0002((object) \u0092\u0004.\u007E\u0001\u0002((object) this.\u0002), false);
        \u0011\u0003.\u007E\u0016\u0002((object) \u0092\u0004.\u007E\u0001\u0002((object) this.\u0002), true);
        \u0097\u0005.\u007E\u0004\u0002((object) this.\u0002, \u0002.\u0001(9394));
        \u0097\u0005.\u007E\u0003\u0002((object) this.\u0003, \u0002.\u0001(9727));
        \u008A\u0004.\u007E\u000E\u0002((object) this.\u0003, 0);
        \u008A\u0004.\u007E\u0008\u0002((object) this.\u0003, 1);
        \u0091\u0007.\u007E\u0006\u0002((object) this.\u0003, BarDockStyle.Top);
        \u0090\u0006.\u007E\u0002\u0002((object) this.\u0003, new Point(394, 188));
        \u008F\u0005 obj7 = \u008F\u0005.\u007E\u0094\u0002;
        LinksInfo linksInfo5 = \u0096\u0005.\u007E\u0007\u0002((object) this.\u0003);
        linkPersistInfoArray5 = new LinkPersistInfo[4]
        {
          new LinkPersistInfo((BarItem) this.\u0002),
          new LinkPersistInfo((BarItem) this.\u0003),
          new LinkPersistInfo((BarItem) this.\u0004),
          new LinkPersistInfo((BarItem) this.\u0005, true)
        };
        LinkPersistInfo[] linkPersistInfoArray10 = linkPersistInfoArray5;
        obj7((object) linksInfo5, linkPersistInfoArray10);
        \u0011\u0003.\u007E\u0011\u0002((object) \u0092\u0004.\u007E\u0001\u0002((object) this.\u0003), false);
        \u0011\u0003.\u007E\u0010\u0002((object) \u0092\u0004.\u007E\u0001\u0002((object) this.\u0003), true);
        \u0011\u0003.\u007E\u0012\u0002((object) \u0092\u0004.\u007E\u0001\u0002((object) this.\u0003), true);
        \u0011\u0003.\u007E\u0013\u0002((object) \u0092\u0004.\u007E\u0001\u0002((object) this.\u0003), true);
        \u0011\u0003.\u007E\u0016\u0002((object) \u0092\u0004.\u007E\u0001\u0002((object) this.\u0003), true);
        \u0097\u0005.\u007E\u0004\u0002((object) this.\u0003, \u0002.\u0001(9727));
        \u0097\u0005.\u007E\u0084\u0002((object) this.\u0002, \u0002.\u0001(9740));
        // ISSUE: reference to a compiler-generated method
        \u0081\u0004.\u007E\u0082\u0002((object) this.\u0002, (Image) \u0005.\u0003());
        \u008A\u0004.\u007E\u0080\u0002((object) this.\u0002, 3);
        \u0097\u0005.\u007E\u007F\u0002((object) this.\u0002, \u0002.\u0001(9745));
        \u0012\u0005.\u007E\u0086\u0002((object) this.\u0002, new ItemClickEventHandler(this.\u0004));
        \u0097\u0005.\u007E\u0084\u0002((object) this.\u0003, \u0002.\u0001(9754));
        // ISSUE: reference to a compiler-generated method
        \u0081\u0004.\u007E\u0082\u0002((object) this.\u0003, (Image) \u0005.\u0004());
        \u008A\u0004.\u007E\u0080\u0002((object) this.\u0003, 4);
        \u0097\u0005.\u007E\u007F\u0002((object) this.\u0003, \u0002.\u0001(9763));
        \u0012\u0005.\u007E\u0086\u0002((object) this.\u0003, new ItemClickEventHandler(this.\u0003));
        \u0097\u0005.\u007E\u0084\u0002((object) this.\u0004, \u0002.\u0001(9772));
        // ISSUE: reference to a compiler-generated method
        \u0081\u0004.\u007E\u0082\u0002((object) this.\u0004, (Image) \u0005.\u0007());
        \u008A\u0004.\u007E\u0080\u0002((object) this.\u0004, 5);
        \u0097\u0005.\u007E\u007F\u0002((object) this.\u0004, \u0002.\u0001(9781));
        \u0012\u0005.\u007E\u0086\u0002((object) this.\u0004, new ItemClickEventHandler(this.\u0006));
        \u0097\u0005.\u007E\u0084\u0002((object) this.\u0005, \u0002.\u0001(9794));
        // ISSUE: reference to a compiler-generated method
        \u0081\u0004.\u007E\u0082\u0002((object) this.\u0005, (Image) \u0005.\u0008());
        \u008A\u0004.\u007E\u0080\u0002((object) this.\u0005, 6);
        \u0097\u0005.\u007E\u007F\u0002((object) this.\u0005, \u0002.\u0001(9807));
        \u0012\u0005.\u007E\u0086\u0002((object) this.\u0005, new ItemClickEventHandler(this.\u0005));
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) this.\u0001), (Control) this.\u0001);
        \u009E\u0004.\u007E\u008B\u0010((object) this.\u0001, DockStyle.Fill);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0001, new Point(0, 51));
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0001, \u0002.\u0001(9820));
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0001, new Size(630, 482));
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0001, 4);
        \u009E\u0004.\u007E\u008B\u0010((object) this.\u0001, DockStyle.Fill);
        \u0011\u0003.\u007E\u0096\u0004((object) this.\u0001, false);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0001, new Point(3, 3));
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0001, \u0002.\u0001(9841));
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) \u009C\u0002.\u007E\u0097\u0004((object) this.\u0001)), (Control) this.\u0001);
        \u0097\u0005.\u007E\u009F\u0010((object) \u009C\u0002.\u007E\u0097\u0004((object) this.\u0001), \u0002.\u0001(9874));
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) \u009C\u0002.\u007E\u0098\u0004((object) this.\u0001)), (Control) this.\u0003);
        \u0097\u0005.\u007E\u009F\u0010((object) \u009C\u0002.\u007E\u0098\u0004((object) this.\u0001), \u0002.\u0001(9883));
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0001, new Size(624, 476));
        \u008A\u0004.\u007E\u0095\u0004((object) this.\u0001, 305);
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0001, 8);
        \u0097\u0005.\u007E\u009F\u0010((object) this.\u0001, \u0002.\u0001(9841));
        \u009E\u0004.\u007E\u008B\u0010((object) this.\u0001, DockStyle.Fill);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0001, new Point(0, 0));
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0001, \u0002.\u0001(9892));
        \u0011\u0007.\u007E\u0018\u0004((object) this.\u0001, this.\u0001);
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0001, new Size(624, 305));
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0001, 7);
        \u0095\u0002 obj8 = \u0095\u0002.\u007E\u008A\u0004;
        XtraTabPageCollection tabPageCollection = \u000F\u0004.\u007E\u0019\u0004((object) this.\u0001);
        xtraTabPageArray1 = new XtraTabPage[4]
        {
          this.\u0001,
          this.\u0002,
          this.\u0003,
          this.\u0004
        };
        XtraTabPage[] xtraTabPageArray2 = xtraTabPageArray1;
        obj8((object) tabPageCollection, xtraTabPageArray2);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) this.\u0001), (Control) this.\u0001);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0001, \u0002.\u0001(9913));
        \u0082\u0004.\u007E\u0089\u0004((object) this.\u0001, new Size(620, 280));
        \u0097\u0005.\u007E\u009F\u0010((object) this.\u0001, \u0002.\u0001(9926));
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) this.\u0001), (Control) this.\u0001);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) this.\u0001), (Control) this.\u0004);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) this.\u0001), (Control) this.\u0001);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) this.\u0001), (Control) this.\u0002);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) this.\u0001), (Control) this.\u0001);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) this.\u0001), (Control) this.\u0001);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) this.\u0001), (Control) this.\u0001);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) this.\u0001), (Control) this.\u0003);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) this.\u0001), (Control) this.\u0001);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) this.\u0001), (Control) this.\u0002);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) this.\u0001), (Control) this.\u0001);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) this.\u0001), (Control) this.\u0003);
        \u009E\u0004.\u007E\u008B\u0010((object) this.\u0001, DockStyle.Fill);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0001, new Point(0, 0));
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0001, \u0002.\u0001(9939));
        \u008E\u0006.\u007E\u0086\u0003((object) this.\u0001, this.\u0001);
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0001, new Size(620, 280));
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0001, 0);
        \u0097\u0005.\u007E\u009F\u0010((object) this.\u0001, \u0002.\u0001(9939));
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0001, new Point(520, 12));
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0001, \u0002.\u0001(9960));
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0001, new Size(88, 22));
        \u0012\u0007.\u007E\u009A\u0003((object) this.\u0001, (IStyleController) this.\u0001);
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0001, 16);
        \u0097\u0005.\u007E\u009F\u0010((object) this.\u0001, \u0002.\u0001(9977));
        \u0011\u0006.\u007E\u0006\u0011((object) this.\u0001, new EventHandler(this.\u0010));
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0004, new Point(89, 12));
        \u0099\u0002.\u007E\u009B\u0003((object) this.\u0004, (IDXMenuManager) this.\u0001);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0004, \u0002.\u0001(9998));
        \u001F\u0004.\u007E\u0091\u0004((object) \u0093\u0004.\u007E\u0097\u0003((object) \u009E\u0005.\u007E\u0001\u0004((object) this.\u0004)), \u0093\u0003.\u0092\u000F());
        \u0011\u0003.\u007E\u0092\u0004((object) \u0089\u0005.\u007E\u0090\u0004((object) \u0093\u0004.\u007E\u0097\u0003((object) \u009E\u0005.\u007E\u0001\u0004((object) this.\u0004))), true);
        \u0011\u0003.\u007E\u0096\u0003((object) \u009E\u0005.\u007E\u0001\u0004((object) this.\u0004), true);
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0004, new Size(190, 20));
        \u0012\u0007.\u007E\u009A\u0003((object) this.\u0004, (IStyleController) this.\u0001);
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0004, 16);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0001, new Point(89, 84));
        \u0099\u0002.\u007E\u009B\u0003((object) this.\u0001, (IDXMenuManager) this.\u0001);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0001, \u0002.\u0001(10007));
        \u0008\u0003 obj9 = \u0008\u0003.\u007E\u0003\u0004;
        EditorButtonCollection buttonCollection1 = \u0002\u0007.\u007E\u0099\u0003((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0001));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray19 = editorButtonArray1;
        obj9((object) buttonCollection1, editorButtonArray19);
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0001, new Size(190, 20));
        \u0012\u0007.\u007E\u009A\u0003((object) this.\u0001, (IStyleController) this.\u0001);
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0001, 14);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0002, new Point(89, 108));
        \u0099\u0002.\u007E\u009B\u0003((object) this.\u0002, (IDXMenuManager) this.\u0001);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0002, \u0002.\u0001(10016));
        \u0008\u0003 obj10 = \u0008\u0003.\u007E\u0003\u0004;
        EditorButtonCollection buttonCollection2 = \u0002\u0007.\u007E\u0099\u0003((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0002));
        editorButtonArray2 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray20 = editorButtonArray2;
        obj10((object) buttonCollection2, editorButtonArray20);
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0002, new Size(190, 20));
        \u0012\u0007.\u007E\u009A\u0003((object) this.\u0002, (IStyleController) this.\u0001);
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0002, 13);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0001, new Point(360, 12));
        \u0099\u0002.\u007E\u009B\u0003((object) this.\u0001, (IDXMenuManager) this.\u0001);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0001, \u0002.\u0001(10037));
        \u001F\u0004.\u007E\u0091\u0004((object) \u0093\u0004.\u007E\u0097\u0003((object) \u009E\u0005.\u007E\u0001\u0004((object) this.\u0001)), \u0093\u0003.\u0092\u000F());
        \u0011\u0003.\u007E\u0092\u0004((object) \u0089\u0005.\u007E\u0090\u0004((object) \u0093\u0004.\u007E\u0097\u0003((object) \u009E\u0005.\u007E\u0001\u0004((object) this.\u0001))), true);
        \u0011\u0003.\u007E\u0096\u0003((object) \u009E\u0005.\u007E\u0001\u0004((object) this.\u0001), true);
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0001, new Size(156, 20));
        \u0012\u0007.\u007E\u009A\u0003((object) this.\u0001, (IStyleController) this.\u0001);
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0001, 11);
        \u0011\u0006.\u007E\u009E\u0003((object) this.\u0001, new EventHandler(this.\u0006));
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0001, new Point(283, 60));
        \u0099\u0002.\u007E\u009B\u0003((object) this.\u0001, (IDXMenuManager) this.\u0001);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0001, \u0002.\u0001(10054));
        \u0011\u0003.\u007E\u0096\u0003((object) \u001E\u0003.\u007E\u0014\u0004((object) this.\u0001), true);
        \u0099\u0007.\u007E\u0011\u0004((object) \u001E\u0003.\u007E\u0014\u0004((object) this.\u0001), PictureSizeMode.Stretch);
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0001, new Size(325, 208));
        \u0012\u0007.\u007E\u009A\u0003((object) this.\u0001, (IStyleController) this.\u0001);
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0001, 10);
        \u0080\u0002.\u007E\u009D\u0003((object) this.\u0001, (object) Color.Empty);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0001, new Point(89, 156));
        \u0099\u0002.\u007E\u009B\u0003((object) this.\u0001, (IDXMenuManager) this.\u0001);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0001, \u0002.\u0001(10067));
        \u0008\u0003 obj11 = \u0008\u0003.\u007E\u0003\u0004;
        EditorButtonCollection buttonCollection3 = \u0002\u0007.\u007E\u0099\u0003((object) \u0098\u0004.\u007E\u0016\u0004((object) this.\u0001));
        editorButtonArray3 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray21 = editorButtonArray3;
        obj11((object) buttonCollection3, editorButtonArray21);
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0001, new Size(190, 20));
        \u0012\u0007.\u007E\u009A\u0003((object) this.\u0001, (IStyleController) this.\u0001);
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0001, 7);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0003, new Point(89, 132));
        \u0099\u0002.\u007E\u009B\u0003((object) this.\u0003, (IDXMenuManager) this.\u0001);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0003, \u0002.\u0001(10076));
        \u0008\u0003 obj12 = \u0008\u0003.\u007E\u0003\u0004;
        EditorButtonCollection buttonCollection4 = \u0002\u0007.\u007E\u0099\u0003((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0003));
        editorButtonArray4 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray22 = editorButtonArray4;
        obj12((object) buttonCollection4, editorButtonArray22);
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0003, new Size(190, 20));
        \u0012\u0007.\u007E\u009A\u0003((object) this.\u0003, (IStyleController) this.\u0001);
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0003, 6);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0001, new Point(89, 204));
        \u0099\u0002.\u007E\u009B\u0003((object) this.\u0001, (IDXMenuManager) this.\u0001);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0001, \u0002.\u0001(10089));
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0001, new Size(190, 64));
        \u0012\u0007.\u007E\u009A\u0003((object) this.\u0001, (IStyleController) this.\u0001);
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0001, 9);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0002, new Point(89, 180));
        \u0099\u0002.\u007E\u009B\u0003((object) this.\u0002, (IDXMenuManager) this.\u0001);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0002, \u0002.\u0001(10102));
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0002, new Size(190, 20));
        \u0012\u0007.\u007E\u009A\u0003((object) this.\u0002, (IStyleController) this.\u0001);
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0002, 8);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0001, new Point(89, 60));
        \u0099\u0002.\u007E\u009B\u0003((object) this.\u0001, (IDXMenuManager) this.\u0001);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0001, \u0002.\u0001(10111));
        \u0008\u0003 obj13 = \u0008\u0003.\u007E\u0003\u0004;
        EditorButtonCollection buttonCollection5 = \u0002\u0007.\u007E\u0099\u0003((object) \u009B\u0002.\u007E\u001C\u0004((object) this.\u0001));
        editorButtonArray5 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray23 = editorButtonArray5;
        obj13((object) buttonCollection5, editorButtonArray23);
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0001, new Size(190, 20));
        \u0012\u0007.\u007E\u009A\u0003((object) this.\u0001, (IStyleController) this.\u0001);
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0001, 5);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0003, new Point(89, 36));
        \u0099\u0002.\u007E\u009B\u0003((object) this.\u0003, (IDXMenuManager) this.\u0001);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0003, \u0002.\u0001(10120));
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0003, new Size(190, 20));
        \u0012\u0007.\u007E\u009A\u0003((object) this.\u0003, (IStyleController) this.\u0001);
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0003, 4);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u0001, \u0002.\u0001(10133));
        \u007F\u0007.\u007E\u008C\u0003((object) this.\u0001, DefaultBoolean.True);
        \u0011\u0003.\u007E\u008B\u0003((object) this.\u0001, false);
        \u008F\u0007 obj14 = \u008F\u0007.\u007E\u0083\u0003;
        LayoutGroupItemCollection groupItemCollection1 = \u008B\u0007.\u007E\u0090\u0003((object) this.\u0001);
        baseLayoutItemArray1 = new BaseLayoutItem[12]
        {
          (BaseLayoutItem) this.\u0001,
          (BaseLayoutItem) this.\u0002,
          (BaseLayoutItem) this.\u0003,
          (BaseLayoutItem) this.\u0004,
          (BaseLayoutItem) this.\u0014,
          (BaseLayoutItem) this.\u0006,
          (BaseLayoutItem) this.\u0007,
          (BaseLayoutItem) this.\u0008,
          (BaseLayoutItem) this.\u000E,
          (BaseLayoutItem) this.\u0005,
          (BaseLayoutItem) this.\u0013,
          (BaseLayoutItem) this.\u0015
        };
        BaseLayoutItem[] baseLayoutItemArray7 = baseLayoutItemArray1;
        obj14((object) groupItemCollection1, baseLayoutItemArray7);
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0001, new Point(0, 0));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0001, \u0002.\u0001(10133));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0001, new Size(620, 280));
        \u001A\u0004.\u007E\u001A\u0003((object) this.\u0001, new Padding(0, 0, 0, 0));
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u0001, \u0002.\u0001(10133));
        \u0011\u0003.\u007E\u0017\u0003((object) this.\u0001, false);
        \u008B\u0006.\u007E\u008A\u0003((object) this.\u0001, (Control) this.\u0001);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u0001, \u0002.\u0001(10162));
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0001, new Point(0, 48));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0001, \u0002.\u0001(10171));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0001, new Size(271, 24));
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u0001, \u0002.\u0001(10162));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u0001, new Size(73, 13));
        \u008B\u0006.\u007E\u008A\u0003((object) this.\u0002, (Control) this.\u0003);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u0002, \u0002.\u0001(10196));
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0002, new Point(0, 120));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0002, \u0002.\u0001(10209));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0002, new Size(271, 24));
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u0002, \u0002.\u0001(10196));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u0002, new Size(73, 13));
        \u008B\u0006.\u007E\u008A\u0003((object) this.\u0003, (Control) this.\u0001);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u0003, \u0002.\u0001(10234));
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0003, new Point(0, 144));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0003, \u0002.\u0001(10243));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0003, new Size(271, 24));
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u0003, \u0002.\u0001(10234));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u0003, new Size(73, 13));
        \u008B\u0006.\u007E\u008A\u0003((object) this.\u0004, (Control) this.\u0002);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u0004, \u0002.\u0001(10268));
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0004, new Point(0, 168));
        \u0082\u0004.\u007E\u001D\u0003((object) this.\u0004, new Size(271, 24));
        \u0082\u0004.\u007E\u001C\u0003((object) this.\u0004, new Size(271, 24));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0004, \u0002.\u0001(10277));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0004, new Size(271, 24));
        \u0012\u0004.\u007E\u0089\u0003((object) this.\u0004, SizeConstraintsType.Custom);
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u0004, \u0002.\u0001(10268));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u0004, new Size(73, 13));
        \u008B\u0006.\u007E\u008A\u0003((object) this.\u0014, (Control) this.\u0001);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u0014, \u0002.\u0001(10302));
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0014, new Point(271, 48));
        \u0082\u0004.\u007E\u001C\u0003((object) this.\u0014, new Size(24, 24));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0014, \u0002.\u0001(10302));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0014, new Size(329, 212));
        \u0012\u0004.\u007E\u0089\u0003((object) this.\u0014, SizeConstraintsType.Custom);
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u0014, \u0002.\u0001(10302));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u0014, new Size(0, 0));
        \u008A\u0004.\u007E\u0019\u0003((object) this.\u0014, 0);
        \u0011\u0003.\u007E\u0017\u0003((object) this.\u0014, false);
        \u008B\u0006.\u007E\u008A\u0003((object) this.\u0006, (Control) this.\u0001);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u0006, \u0002.\u0001(10327));
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0006, new Point(0, 72));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0006, \u0002.\u0001(10336));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0006, new Size(271, 24));
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u0006, \u0002.\u0001(10327));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u0006, new Size(73, 13));
        \u008B\u0006.\u007E\u008A\u0003((object) this.\u0007, (Control) this.\u0002);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u0007, \u0002.\u0001(10365));
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0007, new Point(0, 96));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0007, \u0002.\u0001(10386));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0007, new Size(271, 24));
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u0007, \u0002.\u0001(10365));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u0007, new Size(73, 13));
        \u008B\u0006.\u007E\u008A\u0003((object) this.\u0008, (Control) this.\u0001);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u0008, \u0002.\u0001(10415));
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0008, new Point(0, 192));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0008, \u0002.\u0001(10428));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0008, new Size(271, 68));
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u0008, \u0002.\u0001(10415));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u0008, new Size(73, 13));
        \u008B\u0006.\u007E\u008A\u0003((object) this.\u000E, (Control) this.\u0003);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u000E, \u0002.\u0001(10453));
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u000E, new Point(0, 24));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u000E, \u0002.\u0001(10466));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u000E, new Size(271, 24));
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u000E, \u0002.\u0001(10453));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u000E, new Size(73, 13));
        \u008B\u0006.\u007E\u008A\u0003((object) this.\u0005, (Control) this.\u0001);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u0005, \u0002.\u0001(10491));
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0005, new Point(271, 0));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0005, \u0002.\u0001(10512));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0005, new Size(237, 48));
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u0005, \u0002.\u0001(10491));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u0005, new Size(73, 13));
        \u008B\u0006.\u007E\u008A\u0003((object) this.\u0013, (Control) this.\u0004);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u0013, \u0002.\u0001(10537));
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0013, new Point(0, 0));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0013, \u0002.\u0001(10550));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0013, new Size(271, 24));
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u0013, \u0002.\u0001(10537));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u0013, new Size(73, 13));
        \u008B\u0006.\u007E\u008A\u0003((object) this.\u0015, (Control) this.\u0001);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u0015, \u0002.\u0001(10579));
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0015, new Point(508, 0));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0015, \u0002.\u0001(10579));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0015, new Size(92, 48));
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u0015, \u0002.\u0001(10579));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u0015, new Size(0, 0));
        \u008A\u0004.\u007E\u0019\u0003((object) this.\u0015, 0);
        \u0011\u0003.\u007E\u0017\u0003((object) this.\u0015, false);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) this.\u0002), (Control) this.\u0002);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0002, \u0002.\u0001(10608));
        \u0082\u0004.\u007E\u0089\u0004((object) this.\u0002, new Size(620, 280));
        \u0097\u0005.\u007E\u009F\u0010((object) this.\u0002, \u0002.\u0001(10625));
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) this.\u0002), (Control) this.\u0001);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) this.\u0002), (Control) this.\u0001);
        \u009E\u0004.\u007E\u008B\u0010((object) this.\u0002, DockStyle.Fill);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0002, new Point(0, 0));
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0002, \u0002.\u0001(10638));
        \u008E\u0006.\u007E\u0086\u0003((object) this.\u0002, this.\u0002);
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0002, new Size(620, 280));
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0002, 0);
        \u0097\u0005.\u007E\u009F\u0010((object) this.\u0002, \u0002.\u0001(10638));
        \u0005\u0005.\u007E\u0003\u0003((object) this.\u0001, LayoutViewStyle.SingleRecordView);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0001, new Point(417, 12));
        \u0082\u0004.\u007E\u0097\u0010((object) this.\u0001, new Size(191, 0));
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0001, \u0002.\u0001(10659));
        \u008A\u0004.\u007E\u009F\u0002((object) this.\u0001, 77);
        \u0088\u0004 obj15 = \u0088\u0004.\u007E\u0087\u0004;
        RepositoryItemCollection repositoryItemCollection1 = \u0017\u0003.\u007E\u0006\u0004((object) this.\u0001);
        repositoryItemArray1 = new RepositoryItem[1]
        {
          (RepositoryItem) this.\u0001
        };
        RepositoryItem[] repositoryItemArray5 = repositoryItemArray1;
        obj15((object) repositoryItemCollection1, repositoryItemArray5);
        \u008A\u0004.\u007E\u009E\u0002((object) this.\u0001, 123);
        \u008A\u0006 obj16 = \u008A\u0006.\u007E\u0014\u0003;
        VGridRows vgridRows1 = \u0013\u0005.\u007E\u0001\u0003((object) this.\u0001);
        baseRowArray1 = new BaseRow[1]
        {
          (BaseRow) this.\u0001
        };
        BaseRow[] baseRowArray5 = baseRowArray1;
        obj16((object) vgridRows1, baseRowArray5);
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0001, new Size(191, 256));
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0001, 7);
        \u0011\u0003.\u007E\u0095\u0003((object) this.\u0001, false);
        \u0097\u0005.\u007E\u0093\u0003((object) this.\u0001, \u0002.\u0001(10680));
        \u008A\u0006 obj17 = \u008A\u0006.\u007E\u0014\u0003;
        VGridRows vgridRows2 = \u0013\u0005.\u007E\u000F\u0003((object) this.\u0001);
        baseRowArray2 = new BaseRow[3]
        {
          (BaseRow) this.\u0001,
          (BaseRow) this.\u0002,
          (BaseRow) this.\u0003
        };
        BaseRow[] baseRowArray6 = baseRowArray2;
        obj17((object) vgridRows2, baseRowArray6);
        \u008A\u0004.\u007E\u0010\u0003((object) this.\u0001, 17);
        \u0097\u0005.\u007E\u000E\u0003((object) this.\u0001, \u0002.\u0001(10713));
        \u0097\u0005.\u007E\u0006\u0003((object) \u0016\u0007.\u007E\u0011\u0003((object) this.\u0001), \u0002.\u0001(10730));
        \u0097\u0005.\u007E\u000E\u0003((object) this.\u0001, \u0002.\u0001(10747));
        \u0097\u0005.\u007E\u0006\u0003((object) \u0016\u0007.\u007E\u0011\u0003((object) this.\u0001), \u0002.\u0001(10764));
        \u0097\u0005.\u007E\u0007\u0003((object) \u0016\u0007.\u007E\u0011\u0003((object) this.\u0001), \u0002.\u0001(10781));
        \u0005\u0007.\u007E\u0008\u0003((object) \u0016\u0007.\u007E\u0011\u0003((object) this.\u0001), (RepositoryItem) this.\u0001);
        \u0097\u0005.\u007E\u000E\u0003((object) this.\u0002, \u0002.\u0001(10798));
        \u0097\u0005.\u007E\u0006\u0003((object) \u0016\u0007.\u007E\u0011\u0003((object) this.\u0002), \u0002.\u0001(10815));
        \u0097\u0005.\u007E\u0007\u0003((object) \u0016\u0007.\u007E\u0011\u0003((object) this.\u0002), \u0002.\u0001(10832));
        \u0005\u0007.\u007E\u0008\u0003((object) \u0016\u0007.\u007E\u0011\u0003((object) this.\u0002), (RepositoryItem) this.\u0001);
        \u0097\u0005.\u007E\u000E\u0003((object) this.\u0003, \u0002.\u0001(10849));
        \u0097\u0005.\u007E\u0006\u0003((object) \u0016\u0007.\u007E\u0011\u0003((object) this.\u0003), \u0002.\u0001(10866));
        \u0097\u0005.\u007E\u0007\u0003((object) \u0016\u0007.\u007E\u0011\u0003((object) this.\u0003), \u0002.\u0001(10879));
        \u0005\u0007.\u007E\u0008\u0003((object) \u0016\u0007.\u007E\u0011\u0003((object) this.\u0003), (RepositoryItem) this.\u0001);
        \u009C\u0007.\u007E\u0087\u0010((object) this.\u0001, this.\u0001);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0001, new Point(12, 12));
        \u0095\u0006.\u007E\u0087((object) this.\u0001, (BaseView) this.\u0001);
        \u0099\u0002.\u007E\u0005\u0004((object) this.\u0001, (IDXMenuManager) this.\u0001);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0001, \u0002.\u0001(10884));
        \u0088\u0004 obj18 = \u0088\u0004.\u007E\u0087\u0004;
        RepositoryItemCollection repositoryItemCollection2 = \u0017\u0003.\u007E\u0006\u0004((object) this.\u0001);
        repositoryItemArray2 = new RepositoryItem[3]
        {
          (RepositoryItem) this.\u0001,
          (RepositoryItem) this.\u0001,
          (RepositoryItem) this.\u0001
        };
        RepositoryItem[] repositoryItemArray6 = repositoryItemArray2;
        obj18((object) repositoryItemCollection2, repositoryItemArray6);
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0001, new Size(401, 256));
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0001, 6);
        \u0018\u0003 obj19 = \u0018\u0003.\u007E\u009E;
        ViewRepositoryCollection repositoryCollection1 = \u0007\u0004.\u007E\u0086((object) this.\u0001);
        baseViewArray1 = new BaseView[2]
        {
          (BaseView) this.\u0001,
          (BaseView) this.\u0001
        };
        BaseView[] baseViewArray4 = baseViewArray1;
        obj19((object) repositoryCollection1, baseViewArray4);
        \u0018\u0006 obj20 = \u0018\u0006.\u007E\u0083\u0013;
        ToolStripItemCollection stripItemCollection1 = \u0092\u0006.\u007E\u0087\u0011((object) this.\u0001);
        toolStripItemArray1 = new ToolStripItem[2]
        {
          (ToolStripItem) this.\u0001,
          (ToolStripItem) this.\u0002
        };
        ToolStripItem[] toolStripItemArray3 = toolStripItemArray1;
        obj20((object) stripItemCollection1, toolStripItemArray3);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0001, \u0002.\u0001(10901));
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0001, new Size(135, 48));
        \u0097\u0005.\u007E\u0089\u0011((object) this.\u0001, \u0002.\u0001(10926));
        \u0082\u0004.\u007E\u008A\u0011((object) this.\u0001, new Size(134, 22));
        \u0097\u0005.\u007E\u008B\u0011((object) this.\u0001, \u0002.\u0001(10959));
        \u0011\u0006.\u007E\u0088\u0011((object) this.\u0001, new EventHandler(this.\u000E));
        \u0097\u0005.\u007E\u0089\u0011((object) this.\u0002, \u0002.\u0001(10972));
        \u0082\u0004.\u007E\u008A\u0011((object) this.\u0002, new Size(134, 22));
        \u0097\u0005.\u007E\u008B\u0011((object) this.\u0002, \u0002.\u0001(11009));
        \u0011\u0006.\u007E\u0088\u0011((object) this.\u0002, new EventHandler(this.\u000F));
        \u0082\u0004.\u007E\u008E((object) this.\u0001, new Size(147, 169));
        \u0080\u0003 obj21 = \u0080\u0003.\u007E\u0097;
        LayoutViewColumnCollection columnCollection1 = \u0005\u0004.\u007E\u008D((object) this.\u0001);
        layoutViewColumnArray1 = new LayoutViewColumn[3]
        {
          this.\u0001,
          this.\u0002,
          this.\u0003
        };
        LayoutViewColumn[] layoutViewColumnArray3 = layoutViewColumnArray1;
        obj21((object) columnCollection1, layoutViewColumnArray3);
        \u008C\u0003.\u007E\u0003((object) this.\u0001, this.\u0001);
        \u0097\u0005.\u007E\u0002((object) this.\u0001, \u0002.\u0001(9149));
        \u0001\u0004.\u007E\u009A((object) \u0008\u0004.\u007E\u008F((object) this.\u0001), LayoutCardArrangeRule.AllowPartialCards);
        \u007F\u0002.\u007E\u009C((object) \u0008\u0004.\u007E\u008F((object) this.\u0001), CardsAlignment.Near);
        \u0082\u0007.\u007E\u009D((object) \u0008\u0004.\u007E\u008F((object) this.\u0001), LayoutViewMode.MultiRow);
        \u009E\u0007.\u007E\u008C((object) this.\u0001, this.\u0001);
        \u0082\u0006.\u007E\u000E((object) this.\u0001, new CustomColumnDataEventHandler(this.\u0003));
        \u0097\u0005.\u007E\u0014((object) this.\u0001, \u0002.\u0001(8933));
        \u0005\u0007.\u007E\u0017((object) this.\u0001, (RepositoryItem) this.\u0001);
        \u0097\u0005.\u007E\u0016((object) this.\u0001, \u0002.\u0001(11026));
        \u008F\u0002.\u007E\u0099((object) this.\u0001, this.\u0001);
        \u0097\u0005.\u007E\u0012((object) this.\u0001, \u0002.\u0001(11035));
        \u0011\u0003.\u007E\u0082((object) \u000E\u0004.\u007E\u0013((object) this.\u0001), false);
        \u0011\u0003.\u007E\u0083((object) \u000E\u0004.\u007E\u0013((object) this.\u0001), true);
        \u0080\u0006.\u007E\u001B((object) this.\u0001, UnboundColumnType.Object);
        \u0097\u0005.\u007E\u0093\u0003((object) this.\u0001, \u0002.\u0001(8955));
        \u008A\u0004.\u007E\u0091\u0003((object) this.\u0001, 117);
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0001, new Point(0, 0));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0001, \u0002.\u0001(11052));
        \u001A\u0004.\u007E\u001B\u0003((object) this.\u0001, new Padding(5, 5, 5, 5));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0001, new Size((int) sbyte.MaxValue, 60));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u0001, new Size(0, 0));
        \u008A\u0004.\u007E\u0019\u0003((object) this.\u0001, 0);
        \u0011\u0003.\u007E\u0017\u0003((object) this.\u0001, false);
        \u0097\u0005.\u007E\u0014((object) this.\u0002, \u0002.\u0001(3894));
        \u0097\u0005.\u007E\u0016((object) this.\u0002, \u0002.\u0001(8584));
        \u008F\u0002.\u007E\u0099((object) this.\u0002, this.\u0002);
        \u0097\u0005.\u007E\u0012((object) this.\u0002, \u0002.\u0001(11089));
        \u0011\u0003.\u007E\u0082((object) \u000E\u0004.\u007E\u0013((object) this.\u0002), false);
        \u0011\u0003.\u007E\u0083((object) \u000E\u0004.\u007E\u0013((object) this.\u0002), true);
        \u008A\u0004.\u007E\u0091\u0003((object) this.\u0002, 117);
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0002, new Point(0, 60));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0002, \u0002.\u0001(11106));
        \u001A\u0004.\u007E\u001B\u0003((object) this.\u0002, new Padding(5, 5, 5, 5));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0002, new Size((int) sbyte.MaxValue, 26));
        \u0093\u0005.\u007E\u0088\u0003((object) this.\u0002, TextAlignModeItem.AutoSize);
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u0002, new Size(0, 0));
        \u008A\u0004.\u007E\u0019\u0003((object) this.\u0002, 0);
        \u0011\u0003.\u007E\u0017\u0003((object) this.\u0002, false);
        \u0097\u0005.\u007E\u0014((object) this.\u0003, \u0002.\u0001(11143));
        \u0005\u0007.\u007E\u0017((object) this.\u0003, (RepositoryItem) this.\u0001);
        \u0097\u0005.\u007E\u0016((object) this.\u0003, \u0002.\u0001(11152));
        \u008F\u0002.\u007E\u0099((object) this.\u0003, this.\u0003);
        \u0097\u0005.\u007E\u0012((object) this.\u0003, \u0002.\u0001(8692));
        \u0011\u0003.\u007E\u0082((object) \u000E\u0004.\u007E\u0013((object) this.\u0003), false);
        \u0011\u0003.\u007E\u0083((object) \u000E\u0004.\u007E\u0013((object) this.\u0003), true);
        \u0011\u0003.\u007E\u0095\u0003((object) this.\u0001, false);
        \u0008\u0003 obj22 = \u0008\u0003.\u007E\u0003\u0004;
        EditorButtonCollection buttonCollection6 = \u0002\u0007.\u007E\u0099\u0003((object) this.\u0001);
        editorButtonArray6 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray24 = editorButtonArray6;
        obj22((object) buttonCollection6, editorButtonArray24);
        \u0097\u0005.\u007E\u0093\u0003((object) this.\u0001, \u0002.\u0001(11165));
        \u008A\u0004.\u007E\u0091\u0003((object) this.\u0003, 84);
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0003, new Point(0, 86));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0003, \u0002.\u0001(8717));
        \u001A\u0004.\u007E\u001B\u0003((object) this.\u0003, new Padding(5, 5, 5, 5));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0003, new Size((int) sbyte.MaxValue, 26));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u0003, new Size(28, 13));
        \u008A\u0004.\u007E\u0019\u0003((object) this.\u0003, 5);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u0001, \u0002.\u0001(9111));
        \u001C\u0006.\u007E\u008E\u0003((object) this.\u0001, GroupElementLocation.AfterText);
        \u008F\u0007 obj23 = \u008F\u0007.\u007E\u0083\u0003;
        LayoutGroupItemCollection groupItemCollection2 = \u008B\u0007.\u007E\u0090\u0003((object) this.\u0001);
        baseLayoutItemArray2 = new BaseLayoutItem[3]
        {
          (BaseLayoutItem) this.\u0001,
          (BaseLayoutItem) this.\u0002,
          (BaseLayoutItem) this.\u0003
        };
        BaseLayoutItem[] baseLayoutItemArray8 = baseLayoutItemArray2;
        obj23((object) groupItemCollection2, baseLayoutItemArray8);
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0001, \u0002.\u0001(9128));
        \u008A\u0004.\u007E\u0092\u0003((object) \u0002\u0003.\u007E\u008D\u0003((object) this.\u0001), 5);
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u0001, \u0002.\u0001(9111));
        \u0011\u0003.\u007E\u0095\u0003((object) this.\u0001, false);
        \u0008\u0003 obj24 = \u0008\u0003.\u007E\u0003\u0004;
        EditorButtonCollection buttonCollection7 = \u0002\u0007.\u007E\u0099\u0003((object) this.\u0001);
        editorButtonArray7 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray25 = editorButtonArray7;
        obj24((object) buttonCollection7, editorButtonArray25);
        \u0097\u0005.\u007E\u0093\u0003((object) this.\u0001, \u0002.\u0001(11202));
        \u008C\u0003.\u007E\u0003((object) this.\u0001, this.\u0001);
        \u0097\u0005.\u007E\u0002((object) this.\u0001, \u0002.\u0001(11243));
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u0002, \u0002.\u0001(11256));
        \u007F\u0007.\u007E\u008C\u0003((object) this.\u0002, DefaultBoolean.True);
        \u0011\u0003.\u007E\u008B\u0003((object) this.\u0002, false);
        \u008F\u0007 obj25 = \u008F\u0007.\u007E\u0083\u0003;
        LayoutGroupItemCollection groupItemCollection3 = \u008B\u0007.\u007E\u0090\u0003((object) this.\u0002);
        baseLayoutItemArray3 = new BaseLayoutItem[2]
        {
          (BaseLayoutItem) this.\u000F,
          (BaseLayoutItem) this.\u0010
        };
        BaseLayoutItem[] baseLayoutItemArray9 = baseLayoutItemArray3;
        obj25((object) groupItemCollection3, baseLayoutItemArray9);
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0002, new Point(0, 0));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0002, \u0002.\u0001(11256));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0002, new Size(620, 280));
        \u001A\u0004.\u007E\u001A\u0003((object) this.\u0002, new Padding(0, 0, 0, 0));
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u0002, \u0002.\u0001(11256));
        \u0011\u0003.\u007E\u0017\u0003((object) this.\u0002, false);
        \u008B\u0006.\u007E\u008A\u0003((object) this.\u000F, (Control) this.\u0001);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u000F, \u0002.\u0001(11285));
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u000F, new Point(0, 0));
        \u0082\u0004.\u007E\u001C\u0003((object) this.\u000F, new Size(104, 24));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u000F, \u0002.\u0001(11285));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u000F, new Size(405, 260));
        \u0012\u0004.\u007E\u0089\u0003((object) this.\u000F, SizeConstraintsType.Custom);
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u000F, \u0002.\u0001(11285));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u000F, new Size(0, 0));
        \u008A\u0004.\u007E\u0019\u0003((object) this.\u000F, 0);
        \u0011\u0003.\u007E\u0017\u0003((object) this.\u000F, false);
        \u008B\u0006.\u007E\u008A\u0003((object) this.\u0010, (Control) this.\u0001);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u0010, \u0002.\u0001(11314));
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0010, new Point(405, 0));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0010, \u0002.\u0001(11314));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0010, new Size(195, 260));
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u0010, \u0002.\u0001(11314));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u0010, new Size(0, 0));
        \u008A\u0004.\u007E\u0019\u0003((object) this.\u0010, 0);
        \u0011\u0003.\u007E\u0017\u0003((object) this.\u0010, false);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) this.\u0003), (Control) this.\u0003);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0003, \u0002.\u0001(11339));
        \u0082\u0004.\u007E\u0089\u0004((object) this.\u0003, new Size(620, 280));
        \u0097\u0005.\u007E\u009F\u0010((object) this.\u0003, \u0002.\u0001(11356));
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) this.\u0003), (Control) this.\u0002);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) this.\u0003), (Control) this.\u0002);
        \u009E\u0004.\u007E\u008B\u0010((object) this.\u0003, DockStyle.Fill);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0003, new Point(0, 0));
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0003, \u0002.\u0001(11377));
        \u008E\u0006.\u007E\u0086\u0003((object) this.\u0003, this.\u0003);
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0003, new Size(620, 280));
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0003, 0);
        \u0097\u0005.\u007E\u009F\u0010((object) this.\u0003, \u0002.\u0001(11377));
        \u009C\u0007.\u007E\u0087\u0010((object) this.\u0002, this.\u0002);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0002, new Point(12, 12));
        \u0095\u0006.\u007E\u0087((object) this.\u0002, (BaseView) this.\u0002);
        \u0099\u0002.\u007E\u0005\u0004((object) this.\u0002, (IDXMenuManager) this.\u0001);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0002, \u0002.\u0001(11398));
        \u0088\u0004 obj26 = \u0088\u0004.\u007E\u0087\u0004;
        RepositoryItemCollection repositoryItemCollection3 = \u0017\u0003.\u007E\u0006\u0004((object) this.\u0002);
        repositoryItemArray3 = new RepositoryItem[2]
        {
          (RepositoryItem) this.\u0002,
          (RepositoryItem) this.\u0002
        };
        RepositoryItem[] repositoryItemArray7 = repositoryItemArray3;
        obj26((object) repositoryItemCollection3, repositoryItemArray7);
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0002, new Size(401, 256));
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0002, 9);
        \u0018\u0003 obj27 = \u0018\u0003.\u007E\u009E;
        ViewRepositoryCollection repositoryCollection2 = \u0007\u0004.\u007E\u0086((object) this.\u0002);
        baseViewArray2 = new BaseView[1]
        {
          (BaseView) this.\u0002
        };
        BaseView[] baseViewArray5 = baseViewArray2;
        obj27((object) repositoryCollection2, baseViewArray5);
        \u0018\u0006 obj28 = \u0018\u0006.\u007E\u0083\u0013;
        ToolStripItemCollection stripItemCollection2 = \u0092\u0006.\u007E\u0087\u0011((object) this.\u0002);
        toolStripItemArray2 = new ToolStripItem[2]
        {
          (ToolStripItem) this.\u0003,
          (ToolStripItem) this.\u0004
        };
        ToolStripItem[] toolStripItemArray4 = toolStripItemArray2;
        obj28((object) stripItemCollection2, toolStripItemArray4);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0002, \u0002.\u0001(10901));
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0002, new Size(135, 48));
        \u0097\u0005.\u007E\u0089\u0011((object) this.\u0003, \u0002.\u0001(11411));
        \u0082\u0004.\u007E\u008A\u0011((object) this.\u0003, new Size(134, 22));
        \u0097\u0005.\u007E\u008B\u0011((object) this.\u0003, \u0002.\u0001(10959));
        \u0011\u0006.\u007E\u0088\u0011((object) this.\u0003, new EventHandler(this.\u0007));
        \u0097\u0005.\u007E\u0089\u0011((object) this.\u0004, \u0002.\u0001(11436));
        \u0082\u0004.\u007E\u008A\u0011((object) this.\u0004, new Size(134, 22));
        \u0097\u0005.\u007E\u008B\u0011((object) this.\u0004, \u0002.\u0001(11009));
        \u0011\u0006.\u007E\u0088\u0011((object) this.\u0004, new EventHandler(this.\u0008));
        \u0082\u0004.\u007E\u008E((object) this.\u0002, new Size(157, 189));
        \u0080\u0003 obj29 = \u0080\u0003.\u007E\u0097;
        LayoutViewColumnCollection columnCollection2 = \u0005\u0004.\u007E\u008D((object) this.\u0002);
        layoutViewColumnArray2 = new LayoutViewColumn[3]
        {
          this.\u0004,
          this.\u0005,
          this.\u0006
        };
        LayoutViewColumn[] layoutViewColumnArray4 = layoutViewColumnArray2;
        obj29((object) columnCollection2, layoutViewColumnArray4);
        \u008C\u0003.\u007E\u0003((object) this.\u0002, this.\u0002);
        \u0097\u0005.\u007E\u0002((object) this.\u0002, \u0002.\u0001(8675));
        \u0001\u0004.\u007E\u009A((object) \u0008\u0004.\u007E\u008F((object) this.\u0002), LayoutCardArrangeRule.AllowPartialCards);
        \u007F\u0002.\u007E\u009C((object) \u0008\u0004.\u007E\u008F((object) this.\u0002), CardsAlignment.Near);
        \u0082\u0007.\u007E\u009D((object) \u0008\u0004.\u007E\u008F((object) this.\u0002), LayoutViewMode.MultiRow);
        \u009E\u0007.\u007E\u008C((object) this.\u0002, this.\u0002);
        \u0082\u0006.\u007E\u000E((object) this.\u0002, new CustomColumnDataEventHandler(this.\u0003));
        \u0097\u0005.\u007E\u0014((object) this.\u0004, \u0002.\u0001(8933));
        \u0005\u0007.\u007E\u0017((object) this.\u0004, (RepositoryItem) this.\u0002);
        \u0097\u0005.\u007E\u0016((object) this.\u0004, \u0002.\u0001(11026));
        \u008F\u0002.\u007E\u0099((object) this.\u0004, this.\u0004);
        \u0097\u0005.\u007E\u0012((object) this.\u0004, \u0002.\u0001(8780));
        \u0011\u0003.\u007E\u0082((object) \u000E\u0004.\u007E\u0013((object) this.\u0004), false);
        \u0011\u0003.\u007E\u0083((object) \u000E\u0004.\u007E\u0013((object) this.\u0004), true);
        \u0080\u0006.\u007E\u001B((object) this.\u0004, UnboundColumnType.Object);
        \u0097\u0005.\u007E\u0093\u0003((object) this.\u0002, \u0002.\u0001(11461));
        \u008A\u0004.\u007E\u0091\u0003((object) this.\u0004, (int) sbyte.MaxValue);
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0004, new Point(0, 0));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0004, \u0002.\u0001(11498));
        \u001A\u0004.\u007E\u001B\u0003((object) this.\u0004, new Padding(5, 5, 5, 5));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0004, new Size(137, 32));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u0004, new Size(0, 0));
        \u008A\u0004.\u007E\u0019\u0003((object) this.\u0004, 0);
        \u0011\u0003.\u007E\u0017\u0003((object) this.\u0004, false);
        \u0097\u0005.\u007E\u0014((object) this.\u0005, \u0002.\u0001(3894));
        \u0097\u0005.\u007E\u0016((object) this.\u0005, \u0002.\u0001(8584));
        \u008F\u0002.\u007E\u0099((object) this.\u0005, this.\u0005);
        \u0097\u0005.\u007E\u0012((object) this.\u0005, \u0002.\u0001(8863));
        \u0011\u0003.\u007E\u0082((object) \u000E\u0004.\u007E\u0013((object) this.\u0005), false);
        \u0011\u0003.\u007E\u0083((object) \u000E\u0004.\u007E\u0013((object) this.\u0005), true);
        \u008A\u0004.\u007E\u0091\u0003((object) this.\u0005, (int) sbyte.MaxValue);
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0005, new Point(0, 32));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0005, \u0002.\u0001(11535));
        \u001A\u0004.\u007E\u001B\u0003((object) this.\u0005, new Padding(5, 5, 5, 5));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0005, new Size(137, 26));
        \u0093\u0005.\u007E\u0088\u0003((object) this.\u0005, TextAlignModeItem.AutoSize);
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u0005, new Size(0, 0));
        \u008A\u0004.\u007E\u0019\u0003((object) this.\u0005, 0);
        \u0011\u0003.\u007E\u0017\u0003((object) this.\u0005, false);
        \u0097\u0005.\u007E\u0014((object) this.\u0006, \u0002.\u0001(11143));
        \u0005\u0007.\u007E\u0017((object) this.\u0006, (RepositoryItem) this.\u0002);
        \u0097\u0005.\u007E\u0016((object) this.\u0006, \u0002.\u0001(11152));
        \u008F\u0002.\u007E\u0099((object) this.\u0006, this.\u0006);
        \u0097\u0005.\u007E\u0012((object) this.\u0006, \u0002.\u0001(9037));
        \u0011\u0003.\u007E\u0082((object) \u000E\u0004.\u007E\u0013((object) this.\u0006), false);
        \u0011\u0003.\u007E\u0083((object) \u000E\u0004.\u007E\u0013((object) this.\u0006), true);
        \u0011\u0003.\u007E\u0095\u0003((object) this.\u0002, false);
        \u0008\u0003 obj30 = \u0008\u0003.\u007E\u0003\u0004;
        EditorButtonCollection buttonCollection8 = \u0002\u0007.\u007E\u0099\u0003((object) this.\u0002);
        editorButtonArray8 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray26 = editorButtonArray8;
        obj30((object) buttonCollection8, editorButtonArray26);
        \u0097\u0005.\u007E\u0093\u0003((object) this.\u0002, \u0002.\u0001(9411));
        \u008A\u0004.\u007E\u0091\u0003((object) this.\u0006, 94);
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0006, new Point(0, 58));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0006, \u0002.\u0001(11568));
        \u001A\u0004.\u007E\u001B\u0003((object) this.\u0006, new Padding(5, 5, 5, 5));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0006, new Size(137, 26));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u0006, new Size(28, 13));
        \u008A\u0004.\u007E\u0019\u0003((object) this.\u0006, 5);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u0002, \u0002.\u0001(9111));
        \u001C\u0006.\u007E\u008E\u0003((object) this.\u0002, GroupElementLocation.AfterText);
        \u008F\u0007 obj31 = \u008F\u0007.\u007E\u0083\u0003;
        LayoutGroupItemCollection groupItemCollection4 = \u008B\u0007.\u007E\u0090\u0003((object) this.\u0002);
        baseLayoutItemArray4 = new BaseLayoutItem[3]
        {
          (BaseLayoutItem) this.\u0004,
          (BaseLayoutItem) this.\u0005,
          (BaseLayoutItem) this.\u0006
        };
        BaseLayoutItem[] baseLayoutItemArray10 = baseLayoutItemArray4;
        obj31((object) groupItemCollection4, baseLayoutItemArray10);
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0002, \u0002.\u0001(11609));
        \u008A\u0004.\u007E\u0092\u0003((object) \u0002\u0003.\u007E\u008D\u0003((object) this.\u0002), 5);
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u0002, \u0002.\u0001(9111));
        \u0005\u0005.\u007E\u0003\u0003((object) this.\u0002, LayoutViewStyle.SingleRecordView);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0002, new Point(417, 12));
        \u0082\u0004.\u007E\u0097\u0010((object) this.\u0002, new Size(191, 0));
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0002, \u0002.\u0001(11630));
        \u008A\u0004.\u007E\u009F\u0002((object) this.\u0002, 83);
        \u0088\u0004 obj32 = \u0088\u0004.\u007E\u0087\u0004;
        RepositoryItemCollection repositoryItemCollection4 = \u0017\u0003.\u007E\u0006\u0004((object) this.\u0002);
        repositoryItemArray4 = new RepositoryItem[1]
        {
          (RepositoryItem) this.\u0002
        };
        RepositoryItem[] repositoryItemArray8 = repositoryItemArray4;
        obj32((object) repositoryItemCollection4, repositoryItemArray8);
        \u008A\u0004.\u007E\u009E\u0002((object) this.\u0002, 117);
        \u008A\u0006 obj33 = \u008A\u0006.\u007E\u0014\u0003;
        VGridRows vgridRows3 = \u0013\u0005.\u007E\u0001\u0003((object) this.\u0002);
        baseRowArray3 = new BaseRow[1]
        {
          (BaseRow) this.\u0002
        };
        BaseRow[] baseRowArray7 = baseRowArray3;
        obj33((object) vgridRows3, baseRowArray7);
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0002, new Size(191, 256));
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0002, 8);
        \u0011\u0003.\u007E\u0095\u0003((object) this.\u0002, false);
        \u0097\u0005.\u007E\u0093\u0003((object) this.\u0002, \u0002.\u0001(11643));
        \u008A\u0006 obj34 = \u008A\u0006.\u007E\u0014\u0003;
        VGridRows vgridRows4 = \u0013\u0005.\u007E\u000F\u0003((object) this.\u0002);
        baseRowArray4 = new BaseRow[2]
        {
          (BaseRow) this.\u0004,
          (BaseRow) this.\u0005
        };
        BaseRow[] baseRowArray8 = baseRowArray4;
        obj34((object) vgridRows4, baseRowArray8);
        \u0097\u0005.\u007E\u000E\u0003((object) this.\u0002, \u0002.\u0001(11676));
        \u0097\u0005.\u007E\u0006\u0003((object) \u0016\u0007.\u007E\u0011\u0003((object) this.\u0002), \u0002.\u0001(11356));
        \u0097\u0005.\u007E\u000E\u0003((object) this.\u0004, \u0002.\u0001(11693));
        \u0097\u0005.\u007E\u0006\u0003((object) \u0016\u0007.\u007E\u0011\u0003((object) this.\u0004), \u0002.\u0001(10815));
        \u0097\u0005.\u007E\u0007\u0003((object) \u0016\u0007.\u007E\u0011\u0003((object) this.\u0004), \u0002.\u0001(10832));
        \u0005\u0007.\u007E\u0008\u0003((object) \u0016\u0007.\u007E\u0011\u0003((object) this.\u0004), (RepositoryItem) this.\u0002);
        \u0097\u0005.\u007E\u000E\u0003((object) this.\u0005, \u0002.\u0001(11710));
        \u0097\u0005.\u007E\u0006\u0003((object) \u0016\u0007.\u007E\u0011\u0003((object) this.\u0005), \u0002.\u0001(10866));
        \u0097\u0005.\u007E\u0007\u0003((object) \u0016\u0007.\u007E\u0011\u0003((object) this.\u0005), \u0002.\u0001(10879));
        \u0005\u0007.\u007E\u0008\u0003((object) \u0016\u0007.\u007E\u0011\u0003((object) this.\u0005), (RepositoryItem) this.\u0002);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u0003, \u0002.\u0001(11727));
        \u007F\u0007.\u007E\u008C\u0003((object) this.\u0003, DefaultBoolean.True);
        \u0011\u0003.\u007E\u008B\u0003((object) this.\u0003, false);
        \u008F\u0007 obj35 = \u008F\u0007.\u007E\u0083\u0003;
        LayoutGroupItemCollection groupItemCollection5 = \u008B\u0007.\u007E\u0090\u0003((object) this.\u0003);
        baseLayoutItemArray5 = new BaseLayoutItem[2]
        {
          (BaseLayoutItem) this.\u0011,
          (BaseLayoutItem) this.\u0012
        };
        BaseLayoutItem[] baseLayoutItemArray11 = baseLayoutItemArray5;
        obj35((object) groupItemCollection5, baseLayoutItemArray11);
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0003, new Point(0, 0));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0003, \u0002.\u0001(11727));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0003, new Size(620, 280));
        \u001A\u0004.\u007E\u001A\u0003((object) this.\u0003, new Padding(0, 0, 0, 0));
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u0003, \u0002.\u0001(11727));
        \u0011\u0003.\u007E\u0017\u0003((object) this.\u0003, false);
        \u008B\u0006.\u007E\u008A\u0003((object) this.\u0011, (Control) this.\u0002);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u0011, \u0002.\u0001(11756));
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0011, new Point(405, 0));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0011, \u0002.\u0001(11756));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0011, new Size(195, 260));
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u0011, \u0002.\u0001(11756));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u0011, new Size(0, 0));
        \u008A\u0004.\u007E\u0019\u0003((object) this.\u0011, 0);
        \u0011\u0003.\u007E\u0017\u0003((object) this.\u0011, false);
        \u008B\u0006.\u007E\u008A\u0003((object) this.\u0012, (Control) this.\u0002);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u0012, \u0002.\u0001(11785));
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0012, new Point(0, 0));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0012, \u0002.\u0001(11785));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0012, new Size(405, 260));
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u0012, \u0002.\u0001(11785));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u0012, new Size(0, 0));
        \u008A\u0004.\u007E\u0019\u0003((object) this.\u0012, 0);
        \u0011\u0003.\u007E\u0017\u0003((object) this.\u0012, false);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) this.\u0004), (Control) this.\u0004);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0004, \u0002.\u0001(11814));
        \u0082\u0004.\u007E\u0089\u0004((object) this.\u0004, new Size(620, 280));
        \u0097\u0005.\u007E\u009F\u0010((object) this.\u0004, \u0002.\u0001(11827));
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) this.\u0004), (Control) this.\u0011);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) this.\u0004), (Control) this.\u0012);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) this.\u0004), (Control) this.\u0004);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) this.\u0004), (Control) this.\u0008);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) this.\u0004), (Control) this.\u0005);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) this.\u0004), (Control) this.\u0006);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) this.\u0004), (Control) this.\u000E);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) this.\u0004), (Control) this.\u0007);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) this.\u0004), (Control) this.\u0010);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u007E\u0088\u0010((object) this.\u0004), (Control) this.\u000F);
        \u009E\u0004.\u007E\u008B\u0010((object) this.\u0004, DockStyle.Fill);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0004, new Point(0, 0));
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0004, \u0002.\u0001(11836));
        \u008E\u0006.\u007E\u0086\u0003((object) this.\u0004, this.\u0004);
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0004, new Size(620, 280));
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0004, 0);
        \u0097\u0005.\u007E\u009F\u0010((object) this.\u0004, \u0002.\u0001(11836));
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0011, new Point(352, 84));
        \u0099\u0002.\u007E\u009B\u0003((object) this.\u0011, (IDXMenuManager) this.\u0001);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0011, \u0002.\u0001(11857));
        \u0008\u0003 obj36 = \u0008\u0003.\u007E\u0003\u0004;
        EditorButtonCollection buttonCollection9 = \u0002\u0007.\u007E\u0099\u0003((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0011));
        editorButtonArray9 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray27 = editorButtonArray9;
        obj36((object) buttonCollection9, editorButtonArray27);
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0011, new Size(256, 20));
        \u0012\u0007.\u007E\u009A\u0003((object) this.\u0011, (IStyleController) this.\u0004);
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0011, 14);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0012, new Point(352, 108));
        \u0099\u0002.\u007E\u009B\u0003((object) this.\u0012, (IDXMenuManager) this.\u0001);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0012, \u0002.\u0001(11870));
        \u0008\u0003 obj37 = \u0008\u0003.\u007E\u0003\u0004;
        EditorButtonCollection buttonCollection10 = \u0002\u0007.\u007E\u0099\u0003((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0012));
        editorButtonArray10 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray28 = editorButtonArray10;
        obj37((object) buttonCollection10, editorButtonArray28);
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0012, new Size(256, 20));
        \u0012\u0007.\u007E\u009A\u0003((object) this.\u0012, (IStyleController) this.\u0004);
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0012, 13);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0004, new Point(352, 60));
        \u0099\u0002.\u007E\u009B\u0003((object) this.\u0004, (IDXMenuManager) this.\u0001);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0004, \u0002.\u0001(11883));
        \u0008\u0003 obj38 = \u0008\u0003.\u007E\u0003\u0004;
        EditorButtonCollection buttonCollection11 = \u0002\u0007.\u007E\u0099\u0003((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0004));
        editorButtonArray11 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray29 = editorButtonArray11;
        obj38((object) buttonCollection11, editorButtonArray29);
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0004, new Size(256, 20));
        \u0012\u0007.\u007E\u009A\u0003((object) this.\u0004, (IStyleController) this.\u0004);
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0004, 12);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0008, new Point(52, 60));
        \u0099\u0002.\u007E\u009B\u0003((object) this.\u0008, (IDXMenuManager) this.\u0001);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0008, \u0002.\u0001(11896));
        \u0008\u0003 obj39 = \u0008\u0003.\u007E\u0003\u0004;
        EditorButtonCollection buttonCollection12 = \u0002\u0007.\u007E\u0099\u0003((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0008));
        editorButtonArray12 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray30 = editorButtonArray12;
        obj39((object) buttonCollection12, editorButtonArray30);
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0008, new Size(256, 20));
        \u0012\u0007.\u007E\u009A\u0003((object) this.\u0008, (IStyleController) this.\u0004);
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0008, 8);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0005, new Point(352, 36));
        \u0099\u0002.\u007E\u009B\u0003((object) this.\u0005, (IDXMenuManager) this.\u0001);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0005, \u0002.\u0001(11909));
        \u0008\u0003 obj40 = \u0008\u0003.\u007E\u0003\u0004;
        EditorButtonCollection buttonCollection13 = \u0002\u0007.\u007E\u0099\u0003((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0005));
        editorButtonArray13 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray31 = editorButtonArray13;
        obj40((object) buttonCollection13, editorButtonArray31);
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0005, new Size(256, 20));
        \u0012\u0007.\u007E\u009A\u0003((object) this.\u0005, (IStyleController) this.\u0004);
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0005, 11);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0006, new Point(353, 12));
        \u0099\u0002.\u007E\u009B\u0003((object) this.\u0006, (IDXMenuManager) this.\u0001);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0006, \u0002.\u0001(11922));
        \u0008\u0003 obj41 = \u0008\u0003.\u007E\u0003\u0004;
        EditorButtonCollection buttonCollection14 = \u0002\u0007.\u007E\u0099\u0003((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0006));
        editorButtonArray14 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray32 = editorButtonArray14;
        obj41((object) buttonCollection14, editorButtonArray32);
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0006, new Size((int) byte.MaxValue, 20));
        \u0012\u0007.\u007E\u009A\u0003((object) this.\u0006, (IStyleController) this.\u0004);
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0006, 10);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u000E, new Point(52, 36));
        \u0099\u0002.\u007E\u009B\u0003((object) this.\u000E, (IDXMenuManager) this.\u0001);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u000E, \u0002.\u0001(11935));
        \u0008\u0003 obj42 = \u0008\u0003.\u007E\u0003\u0004;
        EditorButtonCollection buttonCollection15 = \u0002\u0007.\u007E\u0099\u0003((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u000E));
        editorButtonArray15 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray33 = editorButtonArray15;
        obj42((object) buttonCollection15, editorButtonArray33);
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u000E, new Size(256, 20));
        \u0012\u0007.\u007E\u009A\u0003((object) this.\u000E, (IStyleController) this.\u0004);
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u000E, 5);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0007, new Point(52, 84));
        \u0099\u0002.\u007E\u009B\u0003((object) this.\u0007, (IDXMenuManager) this.\u0001);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0007, \u0002.\u0001(11948));
        \u0008\u0003 obj43 = \u0008\u0003.\u007E\u0003\u0004;
        EditorButtonCollection buttonCollection16 = \u0002\u0007.\u007E\u0099\u0003((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0007));
        editorButtonArray16 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray34 = editorButtonArray16;
        obj43((object) buttonCollection16, editorButtonArray34);
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0007, new Size(256, 20));
        \u0012\u0007.\u007E\u009A\u0003((object) this.\u0007, (IStyleController) this.\u0004);
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0007, 9);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0010, new Point(52, 12));
        \u0099\u0002.\u007E\u009B\u0003((object) this.\u0010, (IDXMenuManager) this.\u0001);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0010, \u0002.\u0001(11961));
        \u0008\u0003 obj44 = \u0008\u0003.\u007E\u0003\u0004;
        EditorButtonCollection buttonCollection17 = \u0002\u0007.\u007E\u0099\u0003((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0010));
        editorButtonArray17 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray35 = editorButtonArray17;
        obj44((object) buttonCollection17, editorButtonArray35);
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0010, new Size(257, 20));
        \u0012\u0007.\u007E\u009A\u0003((object) this.\u0010, (IStyleController) this.\u0004);
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0010, 6);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u000F, new Point(52, 108));
        \u0099\u0002.\u007E\u009B\u0003((object) this.\u000F, (IDXMenuManager) this.\u0001);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u000F, \u0002.\u0001(11974));
        \u0008\u0003 obj45 = \u0008\u0003.\u007E\u0003\u0004;
        EditorButtonCollection buttonCollection18 = \u0002\u0007.\u007E\u0099\u0003((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u000F));
        editorButtonArray18 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray36 = editorButtonArray18;
        obj45((object) buttonCollection18, editorButtonArray36);
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u000F, new Size(256, 20));
        \u0012\u0007.\u007E\u009A\u0003((object) this.\u000F, (IStyleController) this.\u0004);
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u000F, 4);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u0004, \u0002.\u0001(11987));
        \u007F\u0007.\u007E\u008C\u0003((object) this.\u0004, DefaultBoolean.True);
        \u0011\u0003.\u007E\u008B\u0003((object) this.\u0004, false);
        \u008F\u0007 obj46 = \u008F\u0007.\u007E\u0083\u0003;
        LayoutGroupItemCollection groupItemCollection6 = \u008B\u0007.\u007E\u0090\u0003((object) this.\u0004);
        baseLayoutItemArray6 = new BaseLayoutItem[10]
        {
          (BaseLayoutItem) this.\u0018,
          (BaseLayoutItem) this.\u0019,
          (BaseLayoutItem) this.\u0017,
          (BaseLayoutItem) this.\u0016,
          (BaseLayoutItem) this.\u001A,
          (BaseLayoutItem) this.\u001E,
          (BaseLayoutItem) this.\u001B,
          (BaseLayoutItem) this.\u001C,
          (BaseLayoutItem) this.\u001D,
          (BaseLayoutItem) this.\u001F
        };
        BaseLayoutItem[] baseLayoutItemArray12 = baseLayoutItemArray6;
        obj46((object) groupItemCollection6, baseLayoutItemArray12);
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0004, new Point(0, 0));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0004, \u0002.\u0001(11987));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0004, new Size(620, 280));
        \u001A\u0004.\u007E\u001A\u0003((object) this.\u0004, new Padding(0, 0, 0, 0));
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u0004, \u0002.\u0001(11987));
        \u0011\u0003.\u007E\u0017\u0003((object) this.\u0004, false);
        \u008B\u0006.\u007E\u008A\u0003((object) this.\u0018, (Control) this.\u000E);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u0018, \u0002.\u0001(12016));
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0018, new Point(0, 24));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0018, \u0002.\u0001(12029));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0018, new Size(300, 24));
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u0018, \u0002.\u0001(12016));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u0018, new Size(36, 13));
        \u008B\u0006.\u007E\u008A\u0003((object) this.\u0019, (Control) this.\u0008);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u0019, \u0002.\u0001(12038));
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0019, new Point(0, 48));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0019, \u0002.\u0001(12051));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0019, new Size(300, 24));
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u0019, \u0002.\u0001(12038));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u0019, new Size(36, 13));
        \u008B\u0006.\u007E\u008A\u0003((object) this.\u0017, (Control) this.\u0010);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u0017, \u0002.\u0001(12080));
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0017, new Point(0, 0));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0017, \u0002.\u0001(12093));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0017, new Size(301, 24));
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u0017, \u0002.\u0001(12080));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u0017, new Size(36, 13));
        \u008B\u0006.\u007E\u008A\u0003((object) this.\u0016, (Control) this.\u000F);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u0016, \u0002.\u0001(12102));
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u0016, new Point(0, 96));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u0016, \u0002.\u0001(12115));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u0016, new Size(300, 164));
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u0016, \u0002.\u0001(12102));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u0016, new Size(36, 13));
        \u008B\u0006.\u007E\u008A\u0003((object) this.\u001A, (Control) this.\u0007);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u001A, \u0002.\u0001(12144));
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u001A, new Point(0, 72));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u001A, \u0002.\u0001(12157));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u001A, new Size(300, 24));
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u001A, \u0002.\u0001(12144));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u001A, new Size(36, 13));
        \u008B\u0006.\u007E\u008A\u0003((object) this.\u001E, (Control) this.\u0012);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u001E, \u0002.\u0001(12186));
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u001E, new Point(300, 96));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u001E, \u0002.\u0001(12199));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u001E, new Size(300, 164));
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u001E, \u0002.\u0001(12186));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u001E, new Size(36, 13));
        \u008B\u0006.\u007E\u008A\u0003((object) this.\u001B, (Control) this.\u0006);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u001B, \u0002.\u0001(12228));
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u001B, new Point(301, 0));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u001B, \u0002.\u0001(12241));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u001B, new Size(299, 24));
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u001B, \u0002.\u0001(12228));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u001B, new Size(36, 13));
        \u008B\u0006.\u007E\u008A\u0003((object) this.\u001C, (Control) this.\u0005);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u001C, \u0002.\u0001(12270));
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u001C, new Point(300, 24));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u001C, \u0002.\u0001(12283));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u001C, new Size(300, 24));
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u001C, \u0002.\u0001(12270));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u001C, new Size(36, 13));
        \u008B\u0006.\u007E\u008A\u0003((object) this.\u001D, (Control) this.\u0004);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u001D, \u0002.\u0001(12312));
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u001D, new Point(300, 48));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u001D, \u0002.\u0001(12325));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u001D, new Size(300, 24));
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u001D, \u0002.\u0001(12312));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u001D, new Size(36, 13));
        \u008B\u0006.\u007E\u008A\u0003((object) this.\u001F, (Control) this.\u0011);
        \u0097\u0005.\u007E\u0080\u0003((object) this.\u001F, \u0002.\u0001(12354));
        \u0090\u0006.\u007E\u001E\u0003((object) this.\u001F, new Point(300, 72));
        \u0097\u0005.\u007E\u0016\u0003((object) this.\u001F, \u0002.\u0001(12367));
        \u0082\u0004.\u007E\u001F\u0003((object) this.\u001F, new Size(300, 24));
        \u0097\u0005.\u007E\u007F\u0003((object) this.\u001F, \u0002.\u0001(12354));
        \u0082\u0004.\u007E\u0018\u0003((object) this.\u001F, new Size(36, 13));
        \u009E\u0004.\u007E\u008B\u0010((object) this.\u0003, DockStyle.Fill);
        \u0090\u0006.\u007E\u0095\u0010((object) this.\u0003, new Point(0, 0));
        \u0095\u0006.\u007E\u0087((object) this.\u0003, (BaseView) this.\u0002);
        \u0099\u0002.\u007E\u0005\u0004((object) this.\u0003, (IDXMenuManager) this.\u0001);
        \u0097\u0005.\u007E\u0098\u0010((object) this.\u0003, \u0002.\u0001(12396));
        \u0082\u0004.\u007E\u009B\u0010((object) this.\u0003, new Size(624, 165));
        \u008A\u0004.\u007E\u009C\u0010((object) this.\u0003, 9);
        \u0018\u0003 obj47 = \u0018\u0003.\u007E\u009E;
        ViewRepositoryCollection repositoryCollection3 = \u0007\u0004.\u007E\u0086((object) this.\u0003);
        baseViewArray3 = new BaseView[1]
        {
          (BaseView) this.\u0002
        };
        BaseView[] baseViewArray6 = baseViewArray3;
        obj47((object) repositoryCollection3, baseViewArray6);
        \u0011\u0006.\u007E\u000E\u0011((object) this.\u0003, new EventHandler(this.\u0005));
        \u0019\u0003 obj48 = \u0019\u0003.\u007E\u001D;
        GridColumnCollection columnCollection3 = \u0011\u0004.\u007E\u0004((object) this.\u0002);
        gridColumnArray1 = new GridColumn[3]
        {
          this.\u0001,
          this.\u0002,
          this.\u0003
        };
        GridColumn[] gridColumnArray2 = gridColumnArray1;
        obj48((object) columnCollection3, gridColumnArray2);
        \u008C\u0003.\u007E\u0003((object) this.\u0002, this.\u0003);
        \u0097\u0005.\u007E\u0002((object) this.\u0002, \u0002.\u0001(9166));
        \u0011\u0003.\u007E\u0001((object) \u0016\u0004.\u007E\u0010((object) this.\u0002), false);
        \u0097\u0005.\u007E\u0014((object) this.\u0001, \u0002.\u0001(3880));
        \u0097\u0005.\u007E\u0016((object) this.\u0001, \u0002.\u0001(12409));
        \u0097\u0005.\u007E\u0012((object) this.\u0001, \u0002.\u0001(12418));
        \u0011\u0003.\u007E\u0082((object) \u000E\u0004.\u007E\u0013((object) this.\u0001), false);
        \u0011\u0003.\u007E\u0083((object) \u000E\u0004.\u007E\u0013((object) this.\u0001), true);
        \u0011\u0003.\u007E\u0019((object) this.\u0001, true);
        \u008A\u0004.\u007E\u0018((object) this.\u0001, 0);
        \u008A\u0004.\u007E\u001A((object) this.\u0001, 34);
        \u0097\u0005.\u007E\u0014((object) this.\u0002, \u0002.\u0001(3894));
        \u0097\u0005.\u007E\u0016((object) this.\u0002, \u0002.\u0001(8584));
        \u0097\u0005.\u007E\u0012((object) this.\u0002, \u0002.\u0001(12435));
        \u0011\u0003.\u007E\u0082((object) \u000E\u0004.\u007E\u0013((object) this.\u0002), false);
        \u0011\u0003.\u007E\u0083((object) \u000E\u0004.\u007E\u0013((object) this.\u0002), true);
        \u0011\u0003.\u007E\u0019((object) this.\u0002, true);
        \u008A\u0004.\u007E\u0018((object) this.\u0002, 1);
        \u008A\u0004.\u007E\u001A((object) this.\u0002, 489);
        \u0097\u0005.\u007E\u0014((object) this.\u0003, \u0002.\u0001(8762));
        \u0097\u0005.\u007E\u0016((object) this.\u0003, \u0002.\u0001(8771));
        \u0097\u0005.\u007E\u0012((object) this.\u0003, \u0002.\u0001(12452));
        \u0011\u0003.\u007E\u0082((object) \u000E\u0004.\u007E\u0013((object) this.\u0003), false);
        \u0011\u0003.\u007E\u0083((object) \u000E\u0004.\u007E\u0013((object) this.\u0003), true);
        \u0011\u0003.\u007E\u0019((object) this.\u0003, true);
        \u008A\u0004.\u007E\u0018((object) this.\u0003, 2);
        \u008A\u0004.\u007E\u001A((object) this.\u0003, 62);
        \u0097\u0005.\u007E\u009A\u0004((object) \u009A\u0006.\u007E\u009C\u0004((object) this.\u0001), \u0002.\u0001(12469));
        \u008D\u0003.\u0083\u0011((object) this, new SizeF(6f, 13f));
        \u0090\u0003.\u0084\u0011((object) this, AutoScaleMode.Font);
        \u0082\u0004.\u0015\u0012((object) this, new Size(630, 555));
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u0088\u0010((object) this), (Control) this.\u0001);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u0088\u0010((object) this), (Control) this.\u0003);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u0088\u0010((object) this), (Control) this.\u0004);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u0088\u0010((object) this), (Control) this.\u0002);
        \u008B\u0006.\u007E\u0080\u0011((object) \u001C\u0005.\u0088\u0010((object) this), (Control) this.\u0001);
        \u001D\u0006.\u0017\u0012((object) this, (Icon) \u008B\u0005.\u007E\u001B\u000E((object) componentResourceManager, \u0002.\u0001(9448)));
        \u0097\u0005.\u0098\u0010((object) this, \u0002.\u0001(12486));
        \u0097\u0005.\u007E\u009F\u0010((object) this, \u0002.\u0001(12495));
        \u0011\u0006.\u007F\u0012((object) this, new EventHandler(this.\u0004));
        \u0011\u0006.\u0080\u0012((object) this, new EventHandler(this.\u0003));
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0001);
        \u0011\u0003.\u007E\u001B\u0011((object) this.\u0001, false);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0001);
        \u0011\u0003.\u007E\u001B\u0011((object) this.\u0001, false);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0001);
        \u0011\u0003.\u007E\u001B\u0011((object) this.\u0001, false);
        \u0011\u0003.\u007E\u001B\u0011((object) this.\u0001, false);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0001);
        \u0011\u0003.\u007E\u001B\u0011((object) this.\u0001, false);
        \u0001\u0007.\u007E\u008C\u0013((object) \u009E\u0005.\u007E\u0001\u0004((object) this.\u0004));
        \u0001\u0007.\u007E\u008C\u0013((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0001));
        \u0001\u0007.\u007E\u008C\u0013((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0002));
        \u0001\u0007.\u007E\u008C\u0013((object) \u009E\u0005.\u007E\u0001\u0004((object) this.\u0001));
        \u0001\u0007.\u007E\u008C\u0013((object) \u001E\u0003.\u007E\u0014\u0004((object) this.\u0001));
        \u0001\u0007.\u007E\u008C\u0013((object) \u0098\u0004.\u007E\u0016\u0004((object) this.\u0001));
        \u0001\u0007.\u007E\u008C\u0013((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0003));
        \u0001\u0007.\u007E\u008C\u0013((object) \u0095\u0005.\u007E\u000E\u0004((object) this.\u0001));
        \u0001\u0007.\u007E\u008C\u0013((object) \u009E\u0005.\u007E\u0001\u0004((object) this.\u0002));
        \u0001\u0007.\u007E\u008C\u0013((object) \u009B\u0002.\u007E\u001C\u0004((object) this.\u0001));
        \u0001\u0007.\u007E\u008C\u0013((object) \u009E\u0005.\u007E\u0001\u0004((object) this.\u0003));
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0002);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0003);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0004);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0014);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0006);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0007);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0008);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u000E);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0005);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0013);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0015);
        \u0011\u0003.\u007E\u001B\u0011((object) this.\u0002, false);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0002);
        \u0011\u0003.\u007E\u001B\u0011((object) this.\u0002, false);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0001);
        \u0011\u0003.\u007E\u001B\u0011((object) this.\u0001, false);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0002);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0003);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0001);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0002);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u000F);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0010);
        \u0011\u0003.\u007E\u001B\u0011((object) this.\u0003, false);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0003);
        \u0011\u0003.\u007E\u001B\u0011((object) this.\u0003, false);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0002);
        \u0011\u0003.\u007E\u001B\u0011((object) this.\u0002, false);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0002);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0002);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0004);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0005);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0002);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0006);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0002);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0002);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0002);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0003);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0011);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0012);
        \u0011\u0003.\u007E\u001B\u0011((object) this.\u0004, false);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0004);
        \u0011\u0003.\u007E\u001B\u0011((object) this.\u0004, false);
        \u0001\u0007.\u007E\u008C\u0013((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0011));
        \u0001\u0007.\u007E\u008C\u0013((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0012));
        \u0001\u0007.\u007E\u008C\u0013((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0004));
        \u0001\u0007.\u007E\u008C\u0013((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0008));
        \u0001\u0007.\u007E\u008C\u0013((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0005));
        \u0001\u0007.\u007E\u008C\u0013((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0006));
        \u0001\u0007.\u007E\u008C\u0013((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u000E));
        \u0001\u0007.\u007E\u008C\u0013((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0007));
        \u0001\u0007.\u007E\u008C\u0013((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0010));
        \u0001\u0007.\u007E\u008C\u0013((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u000F));
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0004);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0018);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0019);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0017);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0016);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u001A);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u001E);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u001B);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u001C);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u001D);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u001F);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0003);
        \u0001\u0007.\u007E\u008C\u0013((object) this.\u0002);
        \u0011\u0003.\u008E\u0004((object) this, false);
      }
      catch (Exception ex)
      {
        object[] objArray = new object[50]
        {
          (object) componentResourceManager,
          (object) barArray1,
          (object) barItemArray1,
          (object) linkPersistInfoArray1,
          (object) linkPersistInfoArray2,
          (object) linkPersistInfoArray3,
          (object) linkPersistInfoArray4,
          (object) linkPersistInfoArray5,
          (object) xtraTabPageArray1,
          (object) editorButtonArray1,
          (object) editorButtonArray2,
          (object) editorButtonArray3,
          (object) editorButtonArray4,
          (object) editorButtonArray5,
          (object) baseLayoutItemArray1,
          (object) repositoryItemArray1,
          (object) baseRowArray1,
          (object) baseRowArray2,
          (object) repositoryItemArray2,
          (object) baseViewArray1,
          (object) toolStripItemArray1,
          (object) layoutViewColumnArray1,
          (object) editorButtonArray6,
          (object) baseLayoutItemArray2,
          (object) editorButtonArray7,
          (object) baseLayoutItemArray3,
          (object) repositoryItemArray3,
          (object) baseViewArray2,
          (object) toolStripItemArray2,
          (object) layoutViewColumnArray2,
          (object) editorButtonArray8,
          (object) baseLayoutItemArray4,
          (object) repositoryItemArray4,
          (object) baseRowArray3,
          (object) baseRowArray4,
          (object) baseLayoutItemArray5,
          (object) editorButtonArray9,
          (object) editorButtonArray10,
          (object) editorButtonArray11,
          (object) editorButtonArray12,
          (object) editorButtonArray13,
          (object) editorButtonArray14,
          (object) editorButtonArray15,
          (object) editorButtonArray16,
          (object) editorButtonArray17,
          (object) editorButtonArray18,
          (object) baseLayoutItemArray6,
          (object) baseViewArray3,
          (object) gridColumnArray1,
          (object) this
        };
        throw UnhandledException.\u0003(ex, objArray);
      }
    }

    public \u0002([In] ref \u0006.\u0006 obj0)
    {
      try
      {
        this.\u0003();
        this.\u0001 = Application.StartupPath;
        this.\u0001 = obj0;
      }
      catch (Exception ex)
      {
        \u0006.\u0006 obj = obj0;
        throw UnhandledException.\u0003(ex, (object) this, (object) obj);
      }
    }

    private void \u0003([In] object obj0, [In] EventArgs obj1)
    {
      try
      {
        \u0001\u0007.\u007E\u0081\u0012((object) this.\u0001);
      }
      catch (Exception ex)
      {
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u0004([In] object obj0, [In] EventArgs obj1)
    {
      int num1;
      DataTable dataTable1;
      try
      {
        this.\u0001.\u0003(\u0002.\u0001(13171));
        this.\u0001 = new \u0007.\u0006();
        this.\u0001.\u0003(\u0002.\u0001(13208));
        \u0098\u0007.\u007E\u0016\u0014((object) this.\u0003, \u0002.\u0003(this.\u0002));
        \u0080\u0002.\u007E\u0089((object) this.\u0003, (object) this.\u0003);
        this.\u0001.\u0003(\u0002.\u0001(13237));
        \u0098\u0007.\u007E\u0016\u0014((object) this.\u0004, \u0002.\u0003(\u0002.\u0001(13270)));
        \u0097\u0005.\u007E\u0082\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0003), \u0002.\u0001(8584));
        \u0097\u0005.\u007E\u0081\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0003), \u0002.\u0001(13347));
        \u0080\u0002.\u007E\u0080\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0003), (object) this.\u0004);
        this.\u0001.\u0003(\u0002.\u0001(13364));
        \u0098\u0007.\u007E\u0016\u0014((object) this.\u0005, \u0002.\u0003(\u0002.\u0001(13409)));
        \u0097\u0005.\u007E\u0082\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0001), \u0002.\u0001(3894));
        \u0097\u0005.\u007E\u0081\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0001), \u0002.\u0001(3871));
        \u0080\u0002.\u007E\u0080\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0001), (object) this.\u0005);
        for (num1 = 0; num1 < 67; ++num1)
        {
          int num2 = \u001D\u0005.\u007E\u001E\u0004((object) \u008D\u0005.\u007E\u001B\u0004((object) \u009B\u0002.\u007E\u001C\u0004((object) this.\u0001)), (object) num1);
        }
        this.\u0001.\u0003(\u0002.\u0001(13478));
        \u0098\u0007.\u007E\u0016\u0014((object) this.\u0001, \u0002.\u0003(\u0002.\u0001(13507)));
        \u0098\u0007.\u007E\u0016\u0014((object) this.\u0002, \u0002.\u0003(\u0002.\u0001(13645)));
        \u0080\u0002.\u007E\u0080\u0004((object) this.\u0001, (object) this.\u0002);
        \u0097\u0005.\u007E\u0082\u0004((object) this.\u0001, \u0002.\u0001(8584));
        \u0097\u0005.\u007E\u0081\u0004((object) this.\u0001, \u0002.\u0001(8579));
        \u0080\u0002.\u007E\u0080\u0004((object) this.\u0002, (object) this.\u0002);
        \u0097\u0005.\u007E\u0082\u0004((object) this.\u0002, \u0002.\u0001(8584));
        \u0097\u0005.\u007E\u0081\u0004((object) this.\u0002, \u0002.\u0001(8579));
        this.\u0001.\u0003(\u0002.\u0001(13714));
        dataTable1 = \u0002.\u0003(\u0002.\u0001(13743));
        \u0080\u0002.\u007E\u0080\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0010), (object) dataTable1);
        \u0097\u0005.\u007E\u0082\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0010), \u0002.\u0001(8584));
        \u0097\u0005.\u007E\u0081\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0010), \u0002.\u0001(13864));
        \u0080\u0002.\u007E\u0080\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u000E), (object) dataTable1);
        \u0097\u0005.\u007E\u0082\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u000E), \u0002.\u0001(8584));
        \u0097\u0005.\u007E\u0081\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u000E), \u0002.\u0001(13864));
        \u0080\u0002.\u007E\u0080\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0008), (object) dataTable1);
        \u0097\u0005.\u007E\u0082\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0008), \u0002.\u0001(8584));
        \u0097\u0005.\u007E\u0081\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0008), \u0002.\u0001(13864));
        \u0080\u0002.\u007E\u0080\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0007), (object) dataTable1);
        \u0097\u0005.\u007E\u0082\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0007), \u0002.\u0001(8584));
        \u0097\u0005.\u007E\u0081\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0007), \u0002.\u0001(13864));
        \u0080\u0002.\u007E\u0080\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u000F), (object) dataTable1);
        \u0097\u0005.\u007E\u0082\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u000F), \u0002.\u0001(8584));
        \u0097\u0005.\u007E\u0081\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u000F), \u0002.\u0001(13864));
        \u0080\u0002.\u007E\u0080\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0006), (object) dataTable1);
        \u0097\u0005.\u007E\u0082\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0006), \u0002.\u0001(8584));
        \u0097\u0005.\u007E\u0081\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0006), \u0002.\u0001(13864));
        \u0080\u0002.\u007E\u0080\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0005), (object) dataTable1);
        \u0097\u0005.\u007E\u0082\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0005), \u0002.\u0001(8584));
        \u0097\u0005.\u007E\u0081\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0005), \u0002.\u0001(13864));
        \u0080\u0002.\u007E\u0080\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0004), (object) dataTable1);
        \u0097\u0005.\u007E\u0082\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0004), \u0002.\u0001(8584));
        \u0097\u0005.\u007E\u0081\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0004), \u0002.\u0001(13864));
        \u0080\u0002.\u007E\u0080\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0011), (object) dataTable1);
        \u0097\u0005.\u007E\u0082\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0011), \u0002.\u0001(8584));
        \u0097\u0005.\u007E\u0081\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0011), \u0002.\u0001(13864));
        \u0080\u0002.\u007E\u0080\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0012), (object) dataTable1);
        \u0097\u0005.\u007E\u0082\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0012), \u0002.\u0001(8584));
        \u0097\u0005.\u007E\u0081\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0012), \u0002.\u0001(13864));
        \u0080\u0002.\u007E\u0080\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0002), (object) \u0002.\u0003(\u0002.\u0001(13877)));
        \u0097\u0005.\u007E\u0081\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0002), \u0002.\u0001(8579));
        \u0097\u0005.\u007E\u0082\u0004((object) \u001B\u0003.\u007E\u0086\u0004((object) this.\u0002), \u0002.\u0001(8584));
        this.\u0002 = this.\u0004();
        this.\u0001 = this.\u0003();
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) num1;
        DataTable dataTable2 = dataTable1;
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) local, (object) dataTable2, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u0003([In] int obj0)
    {
      try
      {
        if (obj0 != -1)
        {
          \u0001\u0007.\u007E\u0008\u0014((object) this.\u0001);
          \u0099\u0004.\u007E\u000F\u0014((object) this.\u0001, \u0002.\u0004(\u0001\u0006.\u0004\u0006(this.\u0003, \u0002.\u0001(13978), obj0.ToString(), \u0002.\u0001(14003)), \u0002.\u0001(14008)));
        }
        if (\u0008\u0007.\u007E\u0086\u0011((object) \u0094\u0007.\u007E\u008A\u0010((object) this.\u0003)) == 0)
        {
          Binding binding1 = \u0090\u0007.\u007E\u0003\u0012((object) \u0094\u0007.\u007E\u008A\u0010((object) this.\u0003), \u0002.\u0001(14021), (object) this.\u0001, \u0002.\u0001(14034), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding2 = \u0090\u0007.\u007E\u0003\u0012((object) \u0094\u0007.\u007E\u008A\u0010((object) this.\u0001), \u0002.\u0001(14021), (object) this.\u0001, \u0002.\u0001(14055), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding3 = \u0090\u0007.\u007E\u0003\u0012((object) \u0094\u0007.\u007E\u008A\u0010((object) this.\u0001), \u0002.\u0001(14021), (object) this.\u0001, \u0002.\u0001(14076), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding4 = \u0090\u0007.\u007E\u0003\u0012((object) \u0094\u0007.\u007E\u008A\u0010((object) this.\u0003), \u0002.\u0001(14021), (object) this.\u0001, \u0002.\u0001(14097), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding5 = \u0090\u0007.\u007E\u0003\u0012((object) \u0094\u0007.\u007E\u008A\u0010((object) this.\u0002), \u0002.\u0001(14021), (object) this.\u0001, \u0002.\u0001(14126), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding6 = \u0090\u0007.\u007E\u0003\u0012((object) \u0094\u0007.\u007E\u008A\u0010((object) this.\u0001), \u0002.\u0001(14021), (object) this.\u0001, \u0002.\u0001(14147), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding7 = \u0090\u0007.\u007E\u0003\u0012((object) \u0094\u0007.\u007E\u008A\u0010((object) this.\u0002), \u0002.\u0001(14021), (object) this.\u0001, \u0002.\u0001(14164), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding8 = \u0090\u0007.\u007E\u0003\u0012((object) \u0094\u0007.\u007E\u008A\u0010((object) this.\u0004), \u0002.\u0001(14021), (object) this.\u0001, \u0002.\u0001(14197), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding9 = \u0090\u0007.\u007E\u0003\u0012((object) \u0094\u0007.\u007E\u008A\u0010((object) this.\u0001), \u0002.\u0001(14021), (object) this.\u0001, \u0002.\u0001(14218), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding10 = \u0090\u0007.\u007E\u0003\u0012((object) \u0094\u0007.\u007E\u008A\u0010((object) this.\u0010), \u0002.\u0001(14021), (object) this.\u0001, \u0002.\u0001(14251), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding11 = \u0090\u0007.\u007E\u0003\u0012((object) \u0094\u0007.\u007E\u008A\u0010((object) this.\u000E), \u0002.\u0001(14021), (object) this.\u0001, \u0002.\u0001(14272), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding12 = \u0090\u0007.\u007E\u0003\u0012((object) \u0094\u0007.\u007E\u008A\u0010((object) this.\u0008), \u0002.\u0001(14021), (object) this.\u0001, \u0002.\u0001(14293), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding13 = \u0090\u0007.\u007E\u0003\u0012((object) \u0094\u0007.\u007E\u008A\u0010((object) this.\u0007), \u0002.\u0001(14021), (object) this.\u0001, \u0002.\u0001(14314), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding14 = \u0090\u0007.\u007E\u0003\u0012((object) \u0094\u0007.\u007E\u008A\u0010((object) this.\u000F), \u0002.\u0001(14021), (object) this.\u0001, \u0002.\u0001(14335), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding15 = \u0090\u0007.\u007E\u0003\u0012((object) \u0094\u0007.\u007E\u008A\u0010((object) this.\u0006), \u0002.\u0001(14021), (object) this.\u0001, \u0002.\u0001(14356), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding16 = \u0090\u0007.\u007E\u0003\u0012((object) \u0094\u0007.\u007E\u008A\u0010((object) this.\u0005), \u0002.\u0001(14021), (object) this.\u0001, \u0002.\u0001(14377), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding17 = \u0090\u0007.\u007E\u0003\u0012((object) \u0094\u0007.\u007E\u008A\u0010((object) this.\u0004), \u0002.\u0001(14021), (object) this.\u0001, \u0002.\u0001(14398), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding18 = \u0090\u0007.\u007E\u0003\u0012((object) \u0094\u0007.\u007E\u008A\u0010((object) this.\u0011), \u0002.\u0001(14021), (object) this.\u0001, \u0002.\u0001(14419), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding19 = \u0090\u0007.\u007E\u0003\u0012((object) \u0094\u0007.\u007E\u008A\u0010((object) this.\u0012), \u0002.\u0001(14021), (object) this.\u0001, \u0002.\u0001(14440), true, DataSourceUpdateMode.OnPropertyChanged);
        }
        this.\u0004(obj0);
        this.\u0005(obj0);
        this.\u0001 = this.\u0003();
        this.\u0002 = this.\u0004();
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) obj0;
        throw UnhandledException.\u0003(ex, (object) this, (object) local);
      }
    }

    private int \u0003()
    {
      DataTable dataTable1;
      try
      {
        dataTable1 = \u0002.\u0003(\u0002.\u0001(14461));
        return \u0010\u0006.\u0006\u0007(\u009D\u0007.\u007E\u001C\u0005(\u008B\u0005.\u007E\u009D\u0013((object) \u0081\u0006.\u007E\u0003\u0014((object) \u0084\u0004.\u007E\u0012\u0014((object) dataTable1), 0), \u0002.\u0001(14502))));
      }
      catch (Exception ex)
      {
        DataTable dataTable2 = dataTable1;
        throw UnhandledException.\u0003(ex, (object) dataTable2, (object) this);
      }
    }

    private int \u0004()
    {
      DataTable dataTable1;
      try
      {
        dataTable1 = \u0002.\u0003(\u0002.\u0001(14515));
        return \u0010\u0006.\u0006\u0007(\u009D\u0007.\u007E\u001C\u0005(\u008B\u0005.\u007E\u009D\u0013((object) \u0081\u0006.\u007E\u0003\u0014((object) \u0084\u0004.\u007E\u0012\u0014((object) dataTable1), 0), \u0002.\u0001(14560))));
      }
      catch (Exception ex)
      {
        DataTable dataTable2 = dataTable1;
        throw UnhandledException.\u0003(ex, (object) dataTable2, (object) this);
      }
    }

    private void \u0004([In] int obj0)
    {
      try
      {
        if (obj0 != -1)
          \u0099\u0004.\u007E\u000F\u0014((object) this.\u0001, \u0002.\u0004(\u001C\u0007.\u0003\u0006(\u0002.\u0001(14577), obj0.ToString(), \u0002.\u0001(14003)), \u0002.\u0001(15027)));
        \u0080\u0002.\u007E\u0089((object) this.\u0001, (object) this.\u0001);
        \u0097\u0005.\u007E\u008A((object) this.\u0001, \u0002.\u0001(15027));
        \u0080\u0002.\u007E\u0004\u0003((object) this.\u0001, (object) this.\u0001);
        \u0097\u0005.\u007E\u0005\u0003((object) this.\u0001, \u0002.\u0001(15027));
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) obj0;
        throw UnhandledException.\u0003(ex, (object) this, (object) local);
      }
    }

    private void \u0005([In] int obj0)
    {
      try
      {
        if (obj0 != -1)
          \u0099\u0004.\u007E\u000F\u0014((object) this.\u0001, \u0002.\u0004(\u001C\u0007.\u0003\u0006(\u0002.\u0001(15048), obj0.ToString(), \u0002.\u0001(14003)), \u0002.\u0001(15498)));
        \u0080\u0002.\u007E\u0089((object) this.\u0002, (object) this.\u0001);
        \u0097\u0005.\u007E\u008A((object) this.\u0002, \u0002.\u0001(15498));
        \u0080\u0002.\u007E\u0004\u0003((object) this.\u0002, (object) this.\u0001);
        \u0097\u0005.\u007E\u0005\u0003((object) this.\u0002, \u0002.\u0001(15498));
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) obj0;
        throw UnhandledException.\u0003(ex, (object) this, (object) local);
      }
    }

    private void \u0004()
    {
      try
      {
        \u0002.\u0003(this.\u0001);
        \u0002.\u0003(\u009B\u0006.\u007E\u0018\u0014((object) \u009D\u0002.\u007E\u0007\u0014((object) this.\u0001), \u0002.\u0001(15027)), \u0002.\u0001(15519));
        \u0002.\u0003(\u009B\u0006.\u007E\u0018\u0014((object) \u009D\u0002.\u007E\u0007\u0014((object) this.\u0001), \u0002.\u0001(15498)), \u0002.\u0001(15519));
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex, (object) this);
      }
    }

    private void \u0005()
    {
      try
      {
        \u0002.\u0003(this.\u0001);
        \u0002.\u0003(\u009B\u0006.\u007E\u0018\u0014((object) \u009D\u0002.\u007E\u0007\u0014((object) this.\u0001), \u0002.\u0001(14008)), this.\u0003);
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex, (object) this);
      }
    }

    private void \u0005([In] object obj0, [In] EventArgs obj1)
    {
      int num1;
      GridView gridView1;
      int num2;
      GridControl gridControl1;
      try
      {
        gridControl1 = obj0 as GridControl;
        gridView1 = \u0092\u0005.\u007E\u008B((object) \u0014\u0004.\u007E\u0088((object) gridControl1), 0) as GridView;
        num1 = \u0008\u0007.\u007E\u0005((object) gridView1);
        if (num1 < 0)
          return;
        num2 = \u0010\u0006.\u0006\u0007(\u009D\u0007.\u007E\u001C\u0005(\u007F\u0006.\u007E\u0007((object) gridView1, num1, \u001B\u0004.\u007E\u001C((object) \u0011\u0004.\u007E\u0004((object) gridView1), \u0002.\u0001(12409)))));
        this.\u0003(num2);
      }
      catch (Exception ex)
      {
        GridControl gridControl2 = gridControl1;
        GridView gridView2 = gridView1;
        // ISSUE: variable of a boxed type
        __Boxed<int> local1 = (ValueType) num1;
        // ISSUE: variable of a boxed type
        __Boxed<int> local2 = (ValueType) num2;
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) gridControl2, (object) gridView2, (object) local1, (object) local2, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u0006([In] object obj0, [In] EventArgs obj1)
    {
      string str1;
      string str2;
      try
      {
        if (\u001F\u0006.\u007E\u009C\u0003((object) this.\u0001) == null)
          return;
        str2 = \u0083\u0004.\u0018\u0010();
        str1 = this.\u0001.\u0003(\u0010\u0006.\u0006\u0007(\u009D\u0007.\u007E\u001C\u0005(\u001F\u0006.\u007E\u009C\u0003((object) this.\u0001))));
        \u0081\u0004.\u007E\u0013\u0004((object) this.\u0001, \u0013\u0004.\u008E\u000F(\u001C\u0007.\u0003\u0006(str2, \u0002.\u0001(15721), str1)));
      }
      catch (Exception ex)
      {
        string str3 = str2;
        string str4 = str1;
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) str3, (object) str4, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u0003([In] object obj0, [In] CustomColumnDataEventArgs obj1)
    {
      LayoutView layoutView1;
      string str1;
      string str2;
      Image image1;
      try
      {
        if (!\u0005\u0003.\u008B\u0005(\u009D\u0007.\u007E\u0015((object) \u0019\u0004.\u007E\u001E((object) obj1)), \u0002.\u0001(11026)) || !\u0093\u0007.\u007E\u007F((object) obj1))
          return;
        layoutView1 = obj0 as LayoutView;
        try
        {
          str1 = \u009D\u0007.\u007E\u001C\u0005(\u0002\u0008.\u007E\u0006((object) layoutView1, \u0008\u0007.\u007E\u001F((object) obj1), \u0002.\u0001(8636)));
          str2 = this.\u0001.\u0003(\u0010\u0006.\u0006\u0007(str1));
          if (!\u0099\u0005.\u007E\u0014\u0008((object) this.\u0001, (object) str2))
          {
            image1 = (Image) null;
            image1 = \u0013\u0004.\u008E\u000F(\u001C\u0007.\u0003\u0006(this.\u0001, \u0002.\u0001(8649), str2));
            \u001A\u0005.\u007E\u0012\u0008((object) this.\u0001, (object) str2, (object) image1);
          }
          \u0080\u0002.\u007E\u0080((object) obj1, \u0095\u0004.\u007E\u0015\u0008((object) this.\u0001, (object) str2));
        }
        catch (Exception ex)
        {
        }
      }
      catch (Exception ex)
      {
        LayoutView layoutView2 = layoutView1;
        string str3 = str1;
        string str4 = str2;
        Image image2 = image1;
        object obj = obj0;
        CustomColumnDataEventArgs columnDataEventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) layoutView2, (object) str3, (object) str4, (object) image2, (object) this, obj, (object) columnDataEventArgs);
      }
    }

    private void \u0003([In] object obj0, [In] ItemClickEventArgs obj1)
    {
      int num1;
      int num2;
      string str1;
      object[] objArray1;
      try
      {
        if (\u0015\u0007.\u0004\u0013(\u0002.\u0001(15734), \u0002.\u0001(9754), MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) != DialogResult.Yes)
          return;
        this.\u0005();
        this.\u0004();
        if (!this.\u0001)
          return;
        this.\u0001 = false;
        num1 = \u0010\u0006.\u0006\u0007(\u009D\u0007.\u007E\u001C\u0005(\u008B\u0005.\u007E\u009D\u0013((object) \u0081\u0006.\u007E\u0003\u0014((object) \u0084\u0004.\u007E\u0012\u0014((object) \u009B\u0006.\u007E\u0018\u0014((object) \u009D\u0002.\u007E\u0007\u0014((object) this.\u0001), \u0002.\u0001(14008))), 0), \u0002.\u0001(12409))));
        num2 = \u0010\u0006.\u0006\u0007(\u009D\u0007.\u007E\u001C\u0005(\u008B\u0005.\u007E\u009D\u0013((object) \u0081\u0006.\u007E\u0003\u0014((object) \u0084\u0004.\u007E\u0012\u0014((object) \u009B\u0006.\u007E\u0018\u0014((object) \u009D\u0002.\u007E\u0007\u0014((object) this.\u0001), \u0002.\u0001(14008))), 0), \u0002.\u0001(8771))));
        str1 = \u009D\u0007.\u007E\u001C\u0005(\u008B\u0005.\u007E\u009D\u0013((object) \u0081\u0006.\u007E\u0003\u0014((object) \u0084\u0004.\u007E\u0012\u0014((object) \u009B\u0006.\u007E\u0018\u0014((object) \u009D\u0002.\u007E\u0007\u0014((object) this.\u0001), \u0002.\u0001(14008))), 0), \u0002.\u0001(8584)));
        \u001A\u0003 obj = \u001A\u0003.\u007E\u0005\u0014;
        DataRowCollection dataRowCollection = \u0084\u0004.\u007E\u0012\u0014((object) this.\u0003);
        objArray1 = new object[3]
        {
          (object) num1,
          (object) str1,
          (object) num2
        };
        object[] objArray2 = objArray1;
        DataRow dataRow = obj((object) dataRowCollection, objArray2);
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<int> local1 = (ValueType) num1;
        // ISSUE: variable of a boxed type
        __Boxed<int> local2 = (ValueType) num2;
        string str2 = str1;
        object[] objArray2 = objArray1;
        object obj = obj0;
        ItemClickEventArgs itemClickEventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) local1, (object) local2, (object) str2, (object) objArray2, (object) this, obj, (object) itemClickEventArgs);
      }
    }

    private void \u0004([In] object obj0, [In] ItemClickEventArgs obj1)
    {
      DataRow dataRow1;
      int num;
      try
      {
        \u0001\u0007.\u007E\u0008\u0014((object) this.\u0001);
        \u0099\u0004.\u007E\u000F\u0014((object) this.\u0001, \u0002.\u0003(this.\u0003, \u0002.\u0001(14008)));
        \u0099\u0004.\u007E\u000F\u0014((object) this.\u0001, \u0002.\u0003(\u0002.\u0001(15787), \u0002.\u0001(15498)));
        \u0099\u0004.\u007E\u000F\u0014((object) this.\u0001, \u0002.\u0003(\u0002.\u0001(15787), \u0002.\u0001(15027)));
        this.\u0003(-1);
        dataRow1 = \u0002.\u0003(\u0004\u0005.\u007E\u0014\u0014((object) \u009B\u0006.\u007E\u0018\u0014((object) \u009D\u0002.\u007E\u0007\u0014((object) this.\u0001), \u0002.\u0001(14008))));
        \u0004\u0004 obj = \u0004\u0004.\u007E\u009E\u0013;
        DataRow dataRow2 = dataRow1;
        string str = \u0002.\u0001(12409);
        num = ++this.\u0002;
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) num;
        obj((object) dataRow2, str, (object) local);
        \u0004\u0004.\u007E\u009E\u0013((object) dataRow1, \u0002.\u0001(8584), (object) \u0002.\u0001(16181));
        \u0004\u0004.\u007E\u009E\u0013((object) dataRow1, \u0002.\u0001(16194), (object) \u0002.\u0001(16199));
        \u0004\u0004.\u007E\u009E\u0013((object) dataRow1, \u0002.\u0001(16208), (object) 1f);
        \u0018\u0004.\u007E\u0004\u0014((object) \u0084\u0004.\u007E\u0012\u0014((object) \u009B\u0006.\u007E\u0018\u0014((object) \u009D\u0002.\u007E\u0007\u0014((object) this.\u0001), \u0002.\u0001(14008))), dataRow1);
        this.\u0001 = true;
        \u0002.\u0003(this.\u0001);
      }
      catch (Exception ex)
      {
        DataRow dataRow2 = dataRow1;
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) num;
        object obj = obj0;
        ItemClickEventArgs itemClickEventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) dataRow2, (object) local, (object) this, obj, (object) itemClickEventArgs);
      }
    }

    private void \u0007([In] object obj0, [In] EventArgs obj1)
    {
      int num1;
      \u0007.\u0001 obj2;
      DataTable dataTable1;
      object[] objArray1;
      int num2;
      try
      {
        obj2 = new \u0007.\u0001();
        int num3 = (int) \u009E\u0002.\u007E\u0084\u0012((object) obj2);
        if (obj2.\u0001 == 0)
          return;
        num1 = \u0010\u0006.\u0006\u0007(\u009D\u0007.\u007E\u001C\u0005(\u008B\u0005.\u007E\u009D\u0013((object) \u0081\u0006.\u007E\u0003\u0014((object) \u0084\u0004.\u007E\u0012\u0014((object) \u009B\u0006.\u007E\u0018\u0014((object) \u009D\u0002.\u007E\u0007\u0014((object) this.\u0001), \u0002.\u0001(14008))), 0), \u0002.\u0001(12409))));
        dataTable1 = ((IEnumerable<DataRow>) \u0016\u0003.\u007E\u0015\u0014((object) this.\u0001, \u0014\u0003.\u0002\u0006(\u0002.\u0001(16217), obj2.\u0001.ToString()))).CopyToDataTable<DataRow>();
        \u001A\u0003 obj3 = \u001A\u0003.\u007E\u0005\u0014;
        DataRowCollection dataRowCollection = \u0084\u0004.\u007E\u0012\u0014((object) \u009B\u0006.\u007E\u0018\u0014((object) \u009D\u0002.\u007E\u0007\u0014((object) this.\u0001), \u0002.\u0001(15498)));
        objArray1 = new object[11];
        object[] objArray2 = objArray1;
        int index = 0;
        num2 = ++this.\u0001;
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) num2;
        objArray2[index] = (object) local;
        objArray1[1] = (object) num1;
        objArray1[2] = (object) obj2.\u0001;
        objArray1[3] = (object) 0;
        objArray1[4] = (object) 0;
        objArray1[5] = (object) 1;
        objArray1[6] = (object) 0;
        objArray1[7] = (object) \u0010\u0006.\u0006\u0007(\u009D\u0007.\u007E\u001C\u0005(\u008B\u0005.\u007E\u009D\u0013((object) \u0081\u0006.\u007E\u0003\u0014((object) \u0084\u0004.\u007E\u0012\u0014((object) dataTable1), 0), \u0002.\u0001(8771))));
        objArray1[8] = (object) \u009D\u0007.\u007E\u001C\u0005(\u008B\u0005.\u007E\u009D\u0013((object) \u0081\u0006.\u007E\u0003\u0014((object) \u0084\u0004.\u007E\u0012\u0014((object) dataTable1), 0), \u0002.\u0001(8584)));
        objArray1[9] = (object) \u0010\u0006.\u0006\u0007(\u009D\u0007.\u007E\u001C\u0005(\u008B\u0005.\u007E\u009D\u0013((object) \u0081\u0006.\u007E\u0003\u0014((object) \u0084\u0004.\u007E\u0012\u0014((object) dataTable1), 0), \u0002.\u0001(8636))));
        objArray1[10] = (object) \u0010\u0006.\u0006\u0007(\u009D\u0007.\u007E\u001C\u0005(\u008B\u0005.\u007E\u009D\u0013((object) \u0081\u0006.\u007E\u0003\u0014((object) \u0084\u0004.\u007E\u0012\u0014((object) dataTable1), 0), \u0002.\u0001(16226))));
        object[] objArray3 = objArray1;
        DataRow dataRow = obj3((object) dataRowCollection, objArray3);
      }
      catch (Exception ex)
      {
        \u0007.\u0001 obj3 = obj2;
        // ISSUE: variable of a boxed type
        __Boxed<int> local1 = (ValueType) num1;
        DataTable dataTable2 = dataTable1;
        object[] objArray2 = objArray1;
        // ISSUE: variable of a boxed type
        __Boxed<int> local2 = (ValueType) num2;
        object obj4 = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) obj3, (object) local1, (object) dataTable2, (object) objArray2, (object) local2, (object) this, obj4, (object) eventArgs);
      }
    }

    private void \u0008([In] object obj0, [In] EventArgs obj1)
    {
      int num;
      LayoutView layoutView1;
      GridControl gridControl1;
      try
      {
        gridControl1 = this.\u0002;
        layoutView1 = \u0092\u0005.\u007E\u008B((object) \u0014\u0004.\u007E\u0088((object) gridControl1), 0) as LayoutView;
        num = \u0008\u0007.\u007E\u0005((object) layoutView1);
        if (num < 0)
          return;
        \u008A\u0004.\u007E\u0008((object) layoutView1, num);
      }
      catch (Exception ex)
      {
        GridControl gridControl2 = gridControl1;
        LayoutView layoutView2 = layoutView1;
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) num;
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) gridControl2, (object) layoutView2, (object) local, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u000E([In] object obj0, [In] EventArgs obj1)
    {
      int num1;
      \u0007.\u0001 obj2;
      DataTable dataTable1;
      object[] objArray1;
      int num2;
      try
      {
        obj2 = new \u0007.\u0001();
        int num3 = (int) \u009E\u0002.\u007E\u0084\u0012((object) obj2);
        if (obj2.\u0001 == 0)
          return;
        num1 = \u0010\u0006.\u0006\u0007(\u009D\u0007.\u007E\u001C\u0005(\u008B\u0005.\u007E\u009D\u0013((object) \u0081\u0006.\u007E\u0003\u0014((object) \u0084\u0004.\u007E\u0012\u0014((object) \u009B\u0006.\u007E\u0018\u0014((object) \u009D\u0002.\u007E\u0007\u0014((object) this.\u0001), \u0002.\u0001(14008))), 0), \u0002.\u0001(12409))));
        dataTable1 = ((IEnumerable<DataRow>) \u0016\u0003.\u007E\u0015\u0014((object) this.\u0001, \u0014\u0003.\u0002\u0006(\u0002.\u0001(16217), obj2.\u0001.ToString()))).CopyToDataTable<DataRow>();
        \u001A\u0003 obj3 = \u001A\u0003.\u007E\u0005\u0014;
        DataRowCollection dataRowCollection = \u0084\u0004.\u007E\u0012\u0014((object) \u009B\u0006.\u007E\u0018\u0014((object) \u009D\u0002.\u007E\u0007\u0014((object) this.\u0001), \u0002.\u0001(15027)));
        objArray1 = new object[11];
        object[] objArray2 = objArray1;
        int index = 0;
        num2 = ++this.\u0001;
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) num2;
        objArray2[index] = (object) local;
        objArray1[1] = (object) num1;
        objArray1[2] = (object) obj2.\u0001;
        objArray1[3] = (object) 0;
        objArray1[4] = (object) 0;
        objArray1[5] = (object) 0;
        objArray1[6] = (object) 0;
        objArray1[7] = (object) \u0010\u0006.\u0006\u0007(\u009D\u0007.\u007E\u001C\u0005(\u008B\u0005.\u007E\u009D\u0013((object) \u0081\u0006.\u007E\u0003\u0014((object) \u0084\u0004.\u007E\u0012\u0014((object) dataTable1), 0), \u0002.\u0001(8771))));
        objArray1[8] = (object) \u009D\u0007.\u007E\u001C\u0005(\u008B\u0005.\u007E\u009D\u0013((object) \u0081\u0006.\u007E\u0003\u0014((object) \u0084\u0004.\u007E\u0012\u0014((object) dataTable1), 0), \u0002.\u0001(8584)));
        objArray1[9] = (object) \u0010\u0006.\u0006\u0007(\u009D\u0007.\u007E\u001C\u0005(\u008B\u0005.\u007E\u009D\u0013((object) \u0081\u0006.\u007E\u0003\u0014((object) \u0084\u0004.\u007E\u0012\u0014((object) dataTable1), 0), \u0002.\u0001(8636))));
        objArray1[10] = (object) \u0010\u0006.\u0006\u0007(\u009D\u0007.\u007E\u001C\u0005(\u008B\u0005.\u007E\u009D\u0013((object) \u0081\u0006.\u007E\u0003\u0014((object) \u0084\u0004.\u007E\u0012\u0014((object) dataTable1), 0), \u0002.\u0001(16226))));
        object[] objArray3 = objArray1;
        DataRow dataRow = obj3((object) dataRowCollection, objArray3);
      }
      catch (Exception ex)
      {
        \u0007.\u0001 obj3 = obj2;
        // ISSUE: variable of a boxed type
        __Boxed<int> local1 = (ValueType) num1;
        DataTable dataTable2 = dataTable1;
        object[] objArray2 = objArray1;
        // ISSUE: variable of a boxed type
        __Boxed<int> local2 = (ValueType) num2;
        object obj4 = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) obj3, (object) local1, (object) dataTable2, (object) objArray2, (object) local2, (object) this, obj4, (object) eventArgs);
      }
    }

    private void \u000F([In] object obj0, [In] EventArgs obj1)
    {
      int num;
      LayoutView layoutView1;
      GridControl gridControl1;
      try
      {
        gridControl1 = this.\u0001;
        layoutView1 = \u0092\u0005.\u007E\u008B((object) \u0014\u0004.\u007E\u0088((object) gridControl1), 0) as LayoutView;
        num = \u0008\u0007.\u007E\u0005((object) layoutView1);
        if (num < 0)
          return;
        \u008A\u0004.\u007E\u0008((object) layoutView1, num);
      }
      catch (Exception ex)
      {
        GridControl gridControl2 = gridControl1;
        LayoutView layoutView2 = layoutView1;
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) num;
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) gridControl2, (object) layoutView2, (object) local, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u0006()
    {
      try
      {
        \u0001\u0007.\u007E\u0013\u0014((object) this.\u0003);
        \u0098\u0007.\u007E\u0016\u0014((object) this.\u0003, \u0002.\u0003(this.\u0002));
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex, (object) this);
      }
    }

    private void \u0005([In] object obj0, [In] ItemClickEventArgs obj1)
    {
      int num;
      try
      {
        this.\u0006();
        num = \u0010\u0006.\u0006\u0007(\u009D\u0007.\u007E\u001C\u0005(\u008B\u0005.\u007E\u009D\u0013((object) \u0081\u0006.\u007E\u0003\u0014((object) \u0084\u0004.\u007E\u0012\u0014((object) \u009B\u0006.\u007E\u0018\u0014((object) \u009D\u0002.\u007E\u0007\u0014((object) this.\u0001), \u0002.\u0001(14008))), 0), \u0002.\u0001(12409))));
        this.\u0003(num);
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) num;
        object obj = obj0;
        ItemClickEventArgs itemClickEventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) local, (object) this, obj, (object) itemClickEventArgs);
      }
    }

    private void \u0006([In] object obj0, [In] ItemClickEventArgs obj1)
    {
      int num1;
      try
      {
        if (this.\u0001)
        {
          int num2 = (int) \u0015\u0007.\u0004\u0013(\u0002.\u0001(16235), \u0002.\u0001(4338), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        else
        {
          if (\u0015\u0007.\u0004\u0013(\u0002.\u0001(16296), \u0002.\u0001(16353), MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) != DialogResult.Yes)
            return;
          num1 = \u0010\u0006.\u0006\u0007(\u009D\u0007.\u007E\u001C\u0005(\u008B\u0005.\u007E\u009D\u0013((object) \u0081\u0006.\u007E\u0003\u0014((object) \u0084\u0004.\u007E\u0012\u0014((object) \u009B\u0006.\u007E\u0018\u0014((object) \u009D\u0002.\u007E\u0007\u0014((object) this.\u0001), \u0002.\u0001(14008))), 0), \u0002.\u0001(12409))));
          \u0002.\u0003(\u001C\u0007.\u0003\u0006(\u0002.\u0001(16382), num1.ToString(), \u0002.\u0001(14003)));
          this.\u0006();
        }
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) num1;
        object obj = obj0;
        ItemClickEventArgs itemClickEventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) local, (object) this, obj, (object) itemClickEventArgs);
      }
    }

    private void \u0010([In] object obj0, [In] EventArgs obj1)
    {
      \u0005 obj2;
      try
      {
        obj2 = new \u0005();
        int num = (int) \u009E\u0002.\u007E\u0084\u0012((object) obj2);
        if (obj2.\u0001)
          return;
        \u0080\u0002.\u007E\u009D\u0003((object) this.\u0001, (object) obj2.\u0001);
      }
      catch (Exception ex)
      {
        \u0005 obj3 = obj2;
        object obj4 = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) obj3, (object) this, obj4, (object) eventArgs);
      }
    }

    private void \u0007([In] object obj0, [In] ItemClickEventArgs obj1)
    {
      \u0008.\u0001 obj2;
      try
      {
        obj2 = new \u0008.\u0001();
        int num = (int) \u009E\u0002.\u007E\u0084\u0012((object) obj2);
      }
      catch (Exception ex)
      {
        \u0008.\u0001 obj3 = obj2;
        object obj4 = obj0;
        ItemClickEventArgs itemClickEventArgs = obj1;
        throw UnhandledException.\u0003(ex, (object) obj3, (object) this, obj4, (object) itemClickEventArgs);
      }
    }

    static \u0002()
    {
      \u0003.\u0003();
    }
  }
}
