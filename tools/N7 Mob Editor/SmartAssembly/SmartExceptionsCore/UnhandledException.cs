﻿// Decompiled with JetBrains decompiler
// Type: SmartAssembly.SmartExceptionsCore.UnhandledException
// Assembly: MobEditor, Version=1.1.0.2, Culture=neutral, PublicKeyToken=016197d45384ac33
// MVID: 35831B81-08C7-444B-81F1-3EE10E122F41
// Assembly location: D:\Server\eab\Tools\N7 Mob Editor.exe

using \u0001;
using \u0003;
using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace SmartAssembly.SmartExceptionsCore
{
  [Serializable]
  public sealed class UnhandledException : Exception
  {
    [NonSerialized]
    internal static \u0002 \u0001;
    public int MethodID;
    public object[] Objects;
    public int ILOffset;
    private Exception previousException;

    internal Exception \u0003()
    {
      return this.previousException;
    }

    public static Exception \u0003([In] Exception obj0)
    {
      return UnhandledException.\u0003(obj0, new object[0]);
    }

    public static Exception \u0003([In] Exception obj0, [In] object obj1)
    {
      return UnhandledException.\u0003(obj0, new object[1]
      {
        obj1
      });
    }

    public static Exception \u0003([In] Exception obj0, [In] object obj1, [In] object obj2)
    {
      return UnhandledException.\u0003(obj0, new object[2]
      {
        obj1,
        obj2
      });
    }

    public static Exception \u0003([In] Exception obj0, [In] object obj1, [In] object obj2, [In] object obj3)
    {
      return UnhandledException.\u0003(obj0, new object[3]
      {
        obj1,
        obj2,
        obj3
      });
    }

    public static Exception \u0003([In] Exception obj0, [In] object obj1, [In] object obj2, [In] object obj3, [In] object obj4)
    {
      return UnhandledException.\u0003(obj0, new object[4]
      {
        obj1,
        obj2,
        obj3,
        obj4
      });
    }

    public static Exception \u0003([In] Exception obj0, [In] object obj1, [In] object obj2, [In] object obj3, [In] object obj4, [In] object obj5)
    {
      return UnhandledException.\u0003(obj0, new object[5]
      {
        obj1,
        obj2,
        obj3,
        obj4,
        obj5
      });
    }

    public static Exception \u0003([In] Exception obj0, [In] object obj1, [In] object obj2, [In] object obj3, [In] object obj4, [In] object obj5, [In] object obj6)
    {
      return UnhandledException.\u0003(obj0, new object[6]
      {
        obj1,
        obj2,
        obj3,
        obj4,
        obj5,
        obj6
      });
    }

    public static Exception \u0003([In] Exception obj0, [In] object obj1, [In] object obj2, [In] object obj3, [In] object obj4, [In] object obj5, [In] object obj6, [In] object obj7)
    {
      return UnhandledException.\u0003(obj0, new object[7]
      {
        obj1,
        obj2,
        obj3,
        obj4,
        obj5,
        obj6,
        obj7
      });
    }

    public static Exception \u0003([In] Exception obj0, [In] object obj1, [In] object obj2, [In] object obj3, [In] object obj4, [In] object obj5, [In] object obj6, [In] object obj7, [In] object obj8)
    {
      return UnhandledException.\u0003(obj0, new object[8]
      {
        obj1,
        obj2,
        obj3,
        obj4,
        obj5,
        obj6,
        obj7,
        obj8
      });
    }

    public static Exception \u0003([In] Exception obj0, [In] object obj1, [In] object obj2, [In] object obj3, [In] object obj4, [In] object obj5, [In] object obj6, [In] object obj7, [In] object obj8, [In] object obj9)
    {
      return UnhandledException.\u0003(obj0, new object[9]
      {
        obj1,
        obj2,
        obj3,
        obj4,
        obj5,
        obj6,
        obj7,
        obj8,
        obj9
      });
    }

    public static Exception \u0003([In] Exception obj0, [In] object obj1, [In] object obj2, [In] object obj3, [In] object obj4, [In] object obj5, [In] object obj6, [In] object obj7, [In] object obj8, [In] object obj9, [In] object obj10)
    {
      return UnhandledException.\u0003(obj0, new object[10]
      {
        obj1,
        obj2,
        obj3,
        obj4,
        obj5,
        obj6,
        obj7,
        obj8,
        obj9,
        obj10
      });
    }

    public static Exception \u0003([In] Exception obj0, [In] object[] obj1)
    {
      if (\u0006.\u0001)
        return (Exception) null;
      int num1 = -1;
      int num2 = -1;
      try
      {
        StackTrace stackTrace = new StackTrace(obj0);
        if (\u0008\u0007.\u007E\u001C\u0008((object) stackTrace) > 0)
        {
          StackFrame stackFrame = \u000F\u0005.\u007E\u001D\u0008((object) stackTrace, \u0008\u0007.\u007E\u001C\u0008((object) stackTrace) - 1);
          num2 = (\u0008\u0007.\u007E\u0016\u0007((object) \u0005\u0008.\u007E\u001E\u0008((object) stackFrame)) & 16777215) - 1;
          num1 = \u0008\u0007.\u007E\u001F\u0008((object) stackFrame);
        }
      }
      catch
      {
      }
      UnhandledException unhandledException = new UnhandledException(num2, obj1, num1, obj0);
      if (obj0 is UnhandledException)
      {
        Exception exception = (obj0 as UnhandledException).\u0003();
        if (exception != null)
          obj0 = exception;
      }
      Exception exception1 = obj0;
      while (\u0003\u0008.\u007E\u0014\u0006((object) exception1) != null)
        exception1 = \u0003\u0008.\u007E\u0014\u0006((object) exception1);
      try
      {
        // ISSUE: type reference
        FieldInfo fieldInfo = \u0087\u0007.\u007E\u0080\u0007((object) \u0014\u0006.\u001A\u0007(__typeref (Exception)), UnhandledException.\u0001(5886), BindingFlags.Instance | BindingFlags.NonPublic);
        \u001A\u0005.\u007E\u0006\u000E((object) fieldInfo, (object) exception1, (object) unhandledException);
      }
      catch
      {
      }
      return obj0;
    }

    public override void GetObjectData([In] SerializationInfo obj0, [In] StreamingContext obj1)
    {
      \u0080\u0007.\u0016\u0006((object) this, obj0, obj1);
      // ISSUE: type reference
      \u007F\u0003.\u007E\u0011\u000E((object) obj0, UnhandledException.\u0001(5907), (object) this.MethodID, \u0014\u0006.\u001A\u0007(__typeref (int)));
      // ISSUE: type reference
      \u007F\u0003.\u007E\u0011\u000E((object) obj0, UnhandledException.\u0001(5944), (object) this.ILOffset, \u0014\u0006.\u001A\u0007(__typeref (int)));
      // ISSUE: type reference
      \u007F\u0003.\u007E\u0011\u000E((object) obj0, UnhandledException.\u0001(5981), (object) this.previousException, \u0014\u0006.\u001A\u0007(__typeref (Exception)));
      int num = this.Objects == null ? 0 : this.Objects.Length;
      // ISSUE: type reference
      \u007F\u0003.\u007E\u0011\u000E((object) obj0, UnhandledException.\u0001(6030), (object) num, \u0014\u0006.\u001A\u0007(__typeref (int)));
      for (int index = 0; index < num; ++index)
      {
        // ISSUE: type reference
        \u007F\u0003.\u007E\u0011\u000E((object) obj0, \u009F\u0006.\u009D\u0005(UnhandledException.\u0001(6075), (object) index), this.Objects[index], \u0014\u0006.\u001A\u0007(__typeref (object)));
      }
    }

    internal UnhandledException([In] SerializationInfo obj0, [In] StreamingContext obj1)
      : base(obj0, obj1)
    {
      this.MethodID = \u0087\u0004.\u007E\u0013\u000E((object) obj0, UnhandledException.\u0001(5907));
      this.ILOffset = \u0087\u0004.\u007E\u0013\u000E((object) obj0, UnhandledException.\u0001(5944));
      // ISSUE: type reference
      this.previousException = (Exception) \u008E\u0002.\u007E\u0012\u000E((object) obj0, UnhandledException.\u0001(5981), \u0014\u0006.\u001A\u0007(__typeref (Exception)));
      int length = \u0087\u0004.\u007E\u0013\u000E((object) obj0, UnhandledException.\u0001(6030));
      this.Objects = new object[length];
      for (int index = 0; index < length; ++index)
      {
        // ISSUE: type reference
        this.Objects[index] = \u008E\u0002.\u007E\u0012\u000E((object) obj0, \u009F\u0006.\u009D\u0005(UnhandledException.\u0001(6075), (object) index), \u0014\u0006.\u001A\u0007(__typeref (object)));
      }
    }

    internal UnhandledException([In] int obj0, [In] object[] obj1, [In] int obj2, [In] Exception obj3)
      : base(string.Format(UnhandledException.\u0001(6120), (object) obj0, (object) obj2))
    {
      this.MethodID = obj0;
      this.Objects = obj1;
      this.ILOffset = obj2;
      this.previousException = obj3;
    }

    static UnhandledException()
    {
      \u0001.\u0003.\u0003();
    }
  }
}
