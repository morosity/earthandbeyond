﻿// Decompiled with JetBrains decompiler
// Type: SmartAssembly.SmartExceptionsCore.ReportingService
// Assembly: MobEditor, Version=1.1.0.2, Culture=neutral, PublicKeyToken=016197d45384ac33
// MVID: 35831B81-08C7-444B-81F1-3EE10E122F41
// Assembly location: D:\Server\eab\Tools\N7 Mob Editor.exe

using \u0001;
using System;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

namespace SmartAssembly.SmartExceptionsCore
{
  [WebServiceBinding(Name = "ReportingServiceSoap", Namespace = "http://www.smartassembly.com/webservices/Reporting/")]
  internal sealed class ReportingService : SoapHttpClientProtocol
  {
    [NonSerialized]
    internal static \u0002 \u0001;

    public ReportingService(string serverURL)
    {
      \u0097\u0005.\u009F\u0004((object) this, \u0014\u0003.\u0002\u0006(serverURL, ReportingService.\u0001(5541)));
      \u008A\u0004.\u0001\u0005((object) this, 180000);
    }

    [SoapDocumentMethod("http://www.smartassembly.com/webservices/Reporting/UploadReport2")]
    public string UploadReport2(string licenseID, [XmlElement(DataType = "base64Binary")] byte[] data)
    {
      return (string) \u001F\u0005.\u0004\u0005((object) this, ReportingService.\u0001(5562), new object[2]
      {
        (object) licenseID,
        (object) data
      })[0];
    }

    static ReportingService()
    {
      \u0003.\u0003();
    }
  }
}
