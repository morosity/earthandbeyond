﻿// Decompiled with JetBrains decompiler
// Type: SmartAssembly.SmartExceptionsCore.UploadReportLoginService
// Assembly: MobEditor, Version=1.1.0.2, Culture=neutral, PublicKeyToken=016197d45384ac33
// MVID: 35831B81-08C7-444B-81F1-3EE10E122F41
// Assembly location: D:\Server\eab\Tools\N7 Mob Editor.exe

using \u0001;
using System;
using System.Web.Services;
using System.Web.Services.Protocols;

namespace SmartAssembly.SmartExceptionsCore
{
  [WebServiceBinding(Name = "LoginServiceSoap", Namespace = "http://www.smartassembly.com/webservices/UploadReportLogin/")]
  internal sealed class UploadReportLoginService : SoapHttpClientProtocol
  {
    [NonSerialized]
    internal static \u0002 \u0001;

    public UploadReportLoginService()
    {
      \u0097\u0005.\u009F\u0004((object) this, UploadReportLoginService.\u0001(5438));
      \u008A\u0004.\u0001\u0005((object) this, 30000);
    }

    [SoapDocumentMethod("http://www.smartassembly.com/webservices/UploadReportLogin/GetServerURL")]
    public string GetServerURL(string licenseID)
    {
      return (string) \u001F\u0005.\u0004\u0005((object) this, UploadReportLoginService.\u0001(5523), new object[1]
      {
        (object) licenseID
      })[0];
    }

    static UploadReportLoginService()
    {
      \u0003.\u0003();
    }
  }
}
