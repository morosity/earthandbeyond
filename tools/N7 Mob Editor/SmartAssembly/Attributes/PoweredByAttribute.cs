﻿// Decompiled with JetBrains decompiler
// Type: SmartAssembly.Attributes.PoweredByAttribute
// Assembly: MobEditor, Version=1.1.0.2, Culture=neutral, PublicKeyToken=016197d45384ac33
// MVID: 35831B81-08C7-444B-81F1-3EE10E122F41
// Assembly location: D:\Server\eab\Tools\N7 Mob Editor.exe

using SmartAssembly.SmartExceptionsCore;
using System;

namespace SmartAssembly.Attributes
{
  public sealed class PoweredByAttribute : Attribute
  {
    public PoweredByAttribute(string s)
    {
      try
      {
      }
      catch (Exception ex)
      {
        string str = s;
        throw UnhandledException.\u0003(ex, (object) this, (object) str);
      }
    }
  }
}
