﻿// Decompiled with JetBrains decompiler
// Type: MobEditor.Properties.Settings
// Assembly: MobEditor, Version=1.1.0.2, Culture=neutral, PublicKeyToken=016197d45384ac33
// MVID: 35831B81-08C7-444B-81F1-3EE10E122F41
// Assembly location: D:\Server\eab\Tools\N7 Mob Editor.exe

using SmartAssembly.SmartExceptionsCore;
using System;
using System.CodeDom.Compiler;
using System.Configuration;
using System.Runtime.CompilerServices;

namespace MobEditor.Properties
{
  [CompilerGenerated]
  [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "9.0.0.0")]
  internal sealed class Settings : ApplicationSettingsBase
  {
    private static Settings \u0001;

    public static Settings Default
    {
      get
      {
        try
        {
          Settings settings = Settings.\u0001;
          return settings;
        }
        catch (Exception ex)
        {
          throw UnhandledException.\u0003(ex);
        }
      }
    }

    public Settings()
    {
      try
      {
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex, (object) this);
      }
    }

    static Settings()
    {
      try
      {
        // ISSUE: reference to a compiler-generated field
        // ISSUE: object of a compiler-generated type is created
        Settings.\u0001 = (Settings) \u0010\u0003.\u0090\u0013((SettingsBase) new Settings());
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0003(ex);
      }
    }
  }
}
