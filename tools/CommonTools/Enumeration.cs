﻿// Decompiled with JetBrains decompiler
// Type: CommonTools.Enumeration
// Assembly: CommonTools, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 665008E1-3954-4D3F-8606-DD931B438B82
// Assembly location: D:\Server\eab\Tools\CommonTools.DLL

using CommonTools.Database;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace CommonTools
{
  public static class Enumeration
  {
    public static void AddSortedByName<T>(ComboBox cbo)
    {
      Type enumType = typeof (T);
      if (!enumType.IsEnum)
        throw new InvalidOperationException("This type is not an enumeration");
      string[] names = Enum.GetNames(enumType);
      Array.Sort<string>(names);
      foreach (string str in names)
      {
        T enumValue;
        if (Enumeration.TryParse<T>(str, out enumValue))
          cbo.Items.Add((object) enumValue);
      }
    }

    public static ColumnData.ColumnDataInfo[] ToColumnDataInfo<T>(bool sortByName)
    {
      Type enumType = typeof (T);
      if (!enumType.IsEnum)
        throw new InvalidOperationException("This type is not an enumeration");
      string[] names = Enum.GetNames(enumType);
      if (sortByName)
        Array.Sort<string>(names);
      List<ColumnData.ColumnDataInfo> columnDataInfoList = new List<ColumnData.ColumnDataInfo>();
      foreach (string str in names)
      {
        Enum enumValue = (Enum) Enum.Parse(enumType, str);
        columnDataInfoList.Add(new ColumnData.ColumnDataInfo()
        {
          DataType = ColumnData.GetDataType(enumValue),
          TypeIsString = ColumnData.IsDataTypeString(enumValue),
          Name = ColumnData.GetName(enumValue)
        });
      }
      return columnDataInfoList.ToArray();
    }

    public static bool TryParse<T>(string value, out T enumValue)
    {
      Type enumType = typeof (T);
      if (!enumType.IsEnum)
        throw new InvalidOperationException("This type is not an enumeration");
      enumValue = (T) Enum.GetValues(enumType).GetValue(0);
      if (string.IsNullOrEmpty(value))
        return false;
      try
      {
        enumValue = (T) Enum.Parse(enumType, value);
      }
      catch (ArgumentException ex)
      {
        return false;
      }
      return true;
    }

    public static string GetString<T>(object value)
    {
      T enumValue;
      if (Enumeration.TryParse<T>(value.ToString(), out enumValue))
        return enumValue.ToString();
      return "";
    }
  }
}
