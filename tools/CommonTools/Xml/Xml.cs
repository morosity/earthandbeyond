﻿// Decompiled with JetBrains decompiler
// Type: CommonTools.Xml
// Assembly: CommonTools, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 665008E1-3954-4D3F-8606-DD931B438B82
// Assembly location: D:\Server\eab\Tools\CommonTools.DLL

using System;
using System.Xml;

namespace CommonTools
{
  public static class Xml
  {
    public static bool getAttribute(XmlNode xmlNode, string xmlTag, bool required, out string value)
    {
      foreach (XmlAttribute attribute in (XmlNamedNodeMap) xmlNode.Attributes)
      {
        if (xmlNode == null)
          throw new Exception("The specified node is null");
        if (attribute.Name.Equals(xmlTag))
        {
          value = attribute.Value;
          return true;
        }
      }
      if (required)
        throw new XmlException("The mandatory XML attribute '" + xmlTag + "' was not specified in the node.\n\n" + xmlNode.InnerXml);
      value = "";
      return false;
    }

    public static bool getAttribute(XmlNode xmlNode, string xmlTag, bool required, out int value)
    {
      value = 0;
      string s;
      bool attribute = CommonTools.Xml.getAttribute(xmlNode, xmlTag, required, out s);
      if (attribute && !int.TryParse(s, out value))
        throw new XmlException("The value '" + s + "' of the XML attribute '" + xmlTag + "' could not be converted to an integer.\n\n" + xmlNode.InnerXml);
      return attribute;
    }

    public static bool getValue(XmlNode xmlNode, bool required, out string value)
    {
      value = (string) null;
      if (xmlNode == null)
        throw new Exception("The specified node is null");
      if (xmlNode.InnerText == null)
        throw new XmlException("The node '" + xmlNode.Name + "' was expected to contain a value within its start and end.\n\n" + xmlNode.OuterXml);
      value = xmlNode.InnerText;
      if (required && (value == null || value.Length == 0))
        throw new XmlException("The value of the node '" + xmlNode.Name + "' is empty.\n\n" + xmlNode.InnerXml);
      return value != null;
    }

    public static bool getValue(XmlNode xmlNode, bool required, out int value)
    {
      value = 0;
      string s;
      bool flag = CommonTools.Xml.getValue(xmlNode, required, out s);
      if (flag && !int.TryParse(s, out value))
        throw new XmlException("The value '" + s + "' of the node '" + xmlNode.Name + "' could not be converted to an integer.\n\n" + xmlNode.InnerXml);
      return flag;
    }

    public static string tagStart()
    {
      return "<";
    }

    public static string tagStart(string tag)
    {
      return CommonTools.Xml.tagStart() + tag + CommonTools.Xml.tagEnd();
    }

    public static string tag(string tag, string value)
    {
      return CommonTools.Xml.tagStart(tag) + value + CommonTools.Xml.tagEnd(tag);
    }

    public static string tagEnd(string tag)
    {
      return CommonTools.Xml.tagStart() + "/" + tag + CommonTools.Xml.tagEnd();
    }

    public static string tagEnd()
    {
      return ">";
    }

    public static string attribute(string attrib, string value)
    {
      return " " + attrib + "=\"" + value + "\"";
    }

    public static string attribute(string attrib, int value)
    {
      return CommonTools.Xml.attribute(attrib, value.ToString());
    }

    public static string attribute(string attrib, bool value)
    {
      return CommonTools.Xml.attribute(attrib, value ? "1" : "0");
    }
  }
}
