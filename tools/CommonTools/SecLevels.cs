﻿// Decompiled with JetBrains decompiler
// Type: CommonTools.SecLevels
// Assembly: CommonTools, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 665008E1-3954-4D3F-8606-DD931B438B82
// Assembly location: D:\Server\eab\Tools\CommonTools.DLL

namespace CommonTools
{
  public enum SecLevels
  {
    USER = 0,
    HELP = 20, // 0x00000014
    BETA = 30, // 0x0000001E
    BETAP = 40, // 0x00000028
    GM = 50, // 0x00000032
    DGM = 60, // 0x0000003C
    HGM = 70, // 0x00000046
    Dev = 80, // 0x00000050
    SDev = 90, // 0x0000005A
    Admin = 100, // 0x00000064
  }
}
