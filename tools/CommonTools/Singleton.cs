﻿// Decompiled with JetBrains decompiler
// Type: Singleton.Get`1
// Assembly: CommonTools, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 665008E1-3954-4D3F-8606-DD931B438B82
// Assembly location: D:\Server\eab\Tools\CommonTools.DLL

using System;

namespace Singleton
{
  public class Get<T> where T : new()
  {
    protected Get()
    {
      if ((object) Get<T>.Instance != null)
        throw new Exception("You have tried to create a new singleton class where you should have instanced it. Replace your \"new class()\" with \"class.Instance\"");
    }

    public static T Instance
    {
      get
      {
        return Get<T>.SingletonCreator.instance;
      }
    }

    private class SingletonCreator
    {
      internal static readonly T instance = new T();
    }
  }
}
