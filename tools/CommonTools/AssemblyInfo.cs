﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyProduct("CommonTools")]
[assembly: AssemblyCopyright("Copyright © Hewlett-Packard Company 2008")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCompany("Hewlett-Packard Company")]
[assembly: AssemblyConfiguration("")]
[assembly: Guid("b0464680-31e6-42a5-9040-10457bac886b")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: ComVisible(false)]
[assembly: AssemblyTitle("CommonTools")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyVersion("1.0.0.0")]
