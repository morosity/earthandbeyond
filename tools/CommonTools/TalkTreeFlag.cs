﻿// Decompiled with JetBrains decompiler
// Type: CommonTools.TalkTreeFlag
// Assembly: CommonTools, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 665008E1-3954-4D3F-8606-DD931B438B82
// Assembly location: D:\Server\eab\Tools\CommonTools.DLL

namespace CommonTools
{
  public enum TalkTreeFlag
  {
    Trade = 1,
    Postpone_Mission = 2,
    Drop_Mission = 3,
    More = 4,
    Mission_Goto_Stage = 5,
    Mission_Completed = 6,
  }
}
