﻿// Decompiled with JetBrains decompiler
// Type: CommonTools.RewardType
// Assembly: CommonTools, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 665008E1-3954-4D3F-8606-DD931B438B82
// Assembly location: D:\Server\eab\Tools\CommonTools.DLL

namespace CommonTools
{
  public enum RewardType
  {
    Credits = 1,
    Explore_XP = 2,
    Combat_XP = 3,
    Trade_XP = 4,
    Faction = 5,
    Item_ID = 6,
    Hull_Upgrade = 7,
    Run_Script = 8,
    Award_Skill = 9,
    Advance_Mission = 10, // 0x0000000A
    Item_Blueprint = 11, // 0x0000000B
    Play_Sound_Reward = 12, // 0x0000000C
  }
}
