﻿// Decompiled with JetBrains decompiler
// Type: CommonTools.CompletionType
// Assembly: CommonTools, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 665008E1-3954-4D3F-8606-DD931B438B82
// Assembly location: D:\Server\eab\Tools\CommonTools.DLL

namespace CommonTools
{
  public enum CompletionType
  {
    Talk_To_Npc = 1,
    Give_Item = 2,
    Receive_Item = 3,
    Fight_Mob = 4,
    Current_Sector = 5,
    Nearest_Nav = 6,
    Obtain_Items = 8,
    Use_Skill_On_Mob_Type = 10, // 0x0000000A
    Use_Skill_On_Object = 11, // 0x0000000B
    Scan_Object = 12, // 0x0000000C
    Possess_Item = 13, // 0x0000000D
    Give_Credits = 14, // 0x0000000E
    Arrive_At = 15, // 0x0000000F
    Nav_Message = 17, // 0x00000011
    Proximity_To_Space_Npc = 18, // 0x00000012
    Talk_Space_Npc = 19, // 0x00000013
    Obtain_Blueprint = 20, // 0x00000014
    Play_Sound = 23, // 0x00000017
  }
}
