﻿// Decompiled with JetBrains decompiler
// Type: CommonTools.Gui.SearchCriteria
// Assembly: CommonTools, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 665008E1-3954-4D3F-8606-DD931B438B82
// Assembly location: D:\Server\eab\Tools\CommonTools.DLL

namespace CommonTools.Gui
{
  public class SearchCriteria
  {
    private static string[] m_comparatorSymbol = new string[6]
    {
      "<",
      "<=",
      "=",
      ">=",
      ">",
      "like"
    };
    public string column;
    public int comparisonSymbol;
    public string criteria;
    public string subQuery;
    public string sqlParameter;
    public string sqlValue;

    public SearchCriteria()
    {
      this.comparisonSymbol = 0;
      this.criteria = "";
    }

    public void Clear()
    {
      this.comparisonSymbol = 0;
      this.criteria = "";
      this.subQuery = "";
      this.sqlParameter = "";
      this.sqlValue = "";
    }

    public bool IsEmpty()
    {
      return this.subQuery == null || this.subQuery.Length == 0;
    }

    public override string ToString()
    {
      string str1 = this.subQuery;
      string str2 = "?" + this.sqlParameter;
      if (str1.Contains(str2))
      {
        int length = str1.IndexOf(str2);
        str1 = str1.Substring(0, length) + this.sqlValue + str1.Substring(length + str2.Length);
      }
      return str1;
    }

    public string getQuery()
    {
      return this.column + " " + this.getComparatorSymbol() + " ?" + this.sqlParameter;
    }

    public string getComparatorSymbol()
    {
      return SearchCriteria.m_comparatorSymbol[this.comparisonSymbol];
    }

    public static string getComparatorSymbol(int symbol)
    {
      return SearchCriteria.m_comparatorSymbol[symbol];
    }
  }
}
