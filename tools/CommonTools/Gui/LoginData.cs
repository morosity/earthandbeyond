﻿// Decompiled with JetBrains decompiler
// Type: CommonTools.Gui.LoginData
// Assembly: CommonTools, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 665008E1-3954-4D3F-8606-DD931B438B82
// Assembly location: D:\Server\eab\Tools\CommonTools.DLL

using System.Reflection;

namespace CommonTools.Gui
{
  public static class LoginData
  {
    private static string m_Host;
    private static int m_Port;
    private static string m_User;
    private static string m_Pass;
    private static string m_ApplicationVersion;

    public static string Host
    {
      get
      {
        return LoginData.m_Host;
      }
      set
      {
        LoginData.m_Host = value;
      }
    }

    public static int Port
    {
      get
      {
        return LoginData.m_Port;
      }
      set
      {
        LoginData.m_Port = value;
      }
    }

    public static string User
    {
      get
      {
        return LoginData.m_User;
      }
      set
      {
        LoginData.m_User = value;
      }
    }

    public static string Pass
    {
      get
      {
        return LoginData.m_Pass;
      }
      set
      {
        LoginData.m_Pass = value;
      }
    }

    public static string ConnStr(string DB)
    {
      return "Connect Timeout=30;Persist Security Info=False;Database=" + DB + ";Host=" + LoginData.m_Host + ";Port=" + LoginData.m_Port.ToString() + ";Username=" + LoginData.m_User + ";Password=" + LoginData.m_Pass;
    }

    public static string ApplicationVersion
    {
      get
      {
        AssemblyName name = Assembly.GetEntryAssembly().GetName();
        int num = name.Version.Major;
        string major = num.ToString();
        num = name.Version.Minor;
        string minor = num.ToString();
        num = name.Version.Build;
        string build = num.ToString();
        num = name.Version.Revision;
        string revision = num.ToString();
        LoginData.m_ApplicationVersion = LoginData.FormattedVersion(major, minor, build, revision);
        return LoginData.m_ApplicationVersion;
      }
    }

    public static string FormattedVersion(string version)
    {
      int length = version.IndexOf('.');
      int num1 = -1;
      int num2 = -1;
      int num3 = -1;
      if (length == -1)
      {
        length = version.Length;
      }
      else
      {
        num1 = version.IndexOf('.', length + 1);
        if (num1 == -1)
        {
          num1 = version.Length;
        }
        else
        {
          num2 = version.IndexOf('.', num1 + 1);
          if (num2 == -1)
          {
            num2 = version.Length;
          }
          else
          {
            num3 = version.IndexOf('.', num2 + 1);
            if (num3 == -1)
              num3 = version.Length;
          }
        }
      }
      return LoginData.FormattedVersion(length == -1 ? "?" : version.Substring(0, length), length == -1 || num1 == -1 ? "?" : version.Substring(length + 1, num1 - length - 1), num1 == -1 || num2 == -1 ? "?" : version.Substring(num1 + 1, num2 - num1 - 1), num2 == -1 || num3 == -1 ? "?" : version.Substring(num2 + 1, num3 - num2 - 1));
    }

    public static string FormattedVersion(string major, string minor, string build, string revision)
    {
      return "v" + (major.Length == 0 ? "?" : major) + "." + (minor.Length == 0 ? "?" : minor) + " Build " + (build.Length == 0 ? "?" : build) + "." + (revision.Length == 0 ? "?" : revision);
    }
  }
}
