﻿// Decompiled with JetBrains decompiler
// Type: CommonTools.Gui.Login
// Assembly: CommonTools, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 665008E1-3954-4D3F-8606-DD931B438B82
// Assembly location: D:\Server\eab\Tools\CommonTools.DLL

using CommonTools.Database;
using MySql.Data.MySqlClient;
using Singleton;
using System;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using ToolAuthDLL;

namespace CommonTools.Gui
{
  public class Login : Form
  {
    private bool m_Cancel = false;
    private bool m_updateVersion = false;
    private IContainer components = (IContainer) null;
    private bool m_HasChanged;
    private Label label1;
    private Label label2;
    public TextBox LoginUsername;
    public TextBox LoginPassword;
    private Button ExitLogin;
    private Button LoginButton;
    private Label label3;
    public TextBox SQLServer;
    public TextBox SQLPort;
    private Label lable9;
    private CheckBox cMySQL;

    public Login()
    {
      this.InitializeComponent();
    }

    public bool isValid()
    {
      return !this.m_Cancel;
    }

    protected override void OnShown(EventArgs e)
    {
      base.OnShown(e);
      this.readConfiguration();
      this.displayConfiguration();
    }

    private void readConfiguration()
    {
      try
      {
        DataSet dataSet = new DataSet();
        int num = (int) dataSet.ReadXml(Application.StartupPath + "\\Config.xml");
        DataRow row = dataSet.Tables[0].Rows[0];
        LoginData.Host = row.ItemArray[0].ToString();
        LoginData.Port = int.Parse(row.ItemArray[1].ToString());
        LoginData.User = row.ItemArray[2].ToString();
        LoginData.Pass = row.ItemArray[3].ToString();
      }
      catch (Exception ex)
      {
        LoginData.Host = "net-7.org";
        LoginData.Port = 3307;
        LoginData.User = "";
        LoginData.Pass = "";
      }
    }

    private void displayConfiguration()
    {
      this.LoginUsername.Text = LoginData.User;
      this.LoginPassword.Text = LoginData.Pass;
      this.SQLServer.Text = LoginData.Host;
      this.SQLPort.Text = LoginData.Port.ToString();
    }

    private void writeConfiguration()
    {
      LoginData.User = this.LoginUsername.Text;
      LoginData.Pass = this.LoginPassword.Text;
      LoginData.Host = this.SQLServer.Text;
      LoginData.Port = Convert.ToInt32(this.SQLPort.Text, 10);
      XmlTextWriter xmlTextWriter = new XmlTextWriter(Application.StartupPath + "\\Config.xml", (Encoding) null);
      xmlTextWriter.Formatting = Formatting.Indented;
      xmlTextWriter.WriteStartDocument();
      xmlTextWriter.WriteStartElement("Confg");
      xmlTextWriter.WriteStartElement("Host");
      xmlTextWriter.WriteString(LoginData.Host.ToString());
      xmlTextWriter.WriteEndElement();
      xmlTextWriter.WriteStartElement("Port");
      xmlTextWriter.WriteString(LoginData.Port.ToString());
      xmlTextWriter.WriteEndElement();
      xmlTextWriter.WriteStartElement("User");
      xmlTextWriter.WriteString(LoginData.User.ToString());
      xmlTextWriter.WriteEndElement();
      xmlTextWriter.WriteStartElement("Pass");
      xmlTextWriter.WriteString(LoginData.Pass.ToString());
      xmlTextWriter.WriteEndElement();
      xmlTextWriter.WriteEndElement();
      xmlTextWriter.WriteEndDocument();
      xmlTextWriter.Close();
    }

    private void LoginButton_Click(object sender, EventArgs e)
    {
      this.acceptedLoginInformation();
    }

    private void ExitLogin_Click(object sender, EventArgs e)
    {
      this.m_Cancel = true;
      this.Close();
    }

    private void LoginChange(object sender, EventArgs e)
    {
      this.m_HasChanged = true;
    }

    private void onEnterKey(object sender, KeyPressEventArgs e)
    {
      if (!e.KeyChar.Equals('\r'))
        return;
      this.acceptedLoginInformation();
    }

    public void updateVersion()
    {
      this.m_updateVersion = true;
    }

    private void GetLoginInfo()
    {
      HttpWebRequest httpWebRequest = (HttpWebRequest) WebRequest.Create("http://net-7.org/PHPTools/GMLogin.php?user=" + this.LoginUsername.Text + "&pass=" + this.LoginPassword.Text);
      httpWebRequest.Timeout = 30000;
      using (HttpWebResponse response = (HttpWebResponse) httpWebRequest.GetResponse())
      {
        string end = new StreamReader(response.GetResponseStream()).ReadToEnd();
        if (end.Split(':')[0] == "300")
          throw new ArgumentException("Invalid login");
        LoginData.User = end.Split(':')[1].Split(',')[0];
        LoginData.Pass = end.Split(':')[1].Split(',')[1];
      }
    }

    private void acceptedLoginInformation()
    {
      MySqlConnection mySqlConnection = (MySqlConnection) null;
      this.m_Cancel = true;
      try
      {
        LoginData.User = this.LoginUsername.Text;
        LoginData.Pass = this.LoginPassword.Text;
        if (!this.cMySQL.Checked)
        {
          ClientData clientData = new ToolAuthDLL.Login().CheckLogin(this.LoginUsername.Text, this.LoginPassword.Text);
          LoginData.User = clientData.MySQLUser;
          LoginData.Pass = clientData.MySQLPass;
        }
        LoginData.Host = this.SQLServer.Text;
        LoginData.Port = Convert.ToInt32(this.SQLPort.Text, 10);
        mySqlConnection = new MySqlConnection(LoginData.ConnStr("net7"));
        mySqlConnection.Open();
        mySqlConnection.Close();
        if (this.m_updateVersion)
        {
          AssemblyName name = Assembly.GetEntryAssembly().GetName();
          Get<DB>.Instance.setVersion(name.Name, name.Version.ToString());
          int num = (int) MessageBox.Show("Database entry for " + name.Name + " has been updated to " + LoginData.ApplicationVersion, "Version Updated", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        else
        {
          AssemblyName name = Assembly.GetEntryAssembly().GetName();
          string version = Get<DB>.Instance.getVersion(name.Name);
          if (name.Version.ToString().CompareTo(version) != 0)
          {
            int num = (int) MessageBox.Show(name.Name + " " + LoginData.ApplicationVersion + " is outdated.\nPlease update your editor to " + LoginData.FormattedVersion(version), "Incorrect Version", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
          }
          else
          {
            if (this.m_HasChanged)
              this.writeConfiguration();
            this.m_Cancel = false;
            this.Close();
          }
        }
      }
      catch (Exception ex)
      {
        int num1 = (int) MessageBox.Show(ex.Message + "\n" + ex.StackTrace + "\n", "Connection Error (" + ex.TargetSite.Name + ")");
        foreach (DictionaryEntry dictionaryEntry in ex.Data)
        {
          int num2 = (int) MessageBox.Show(string.Format("    The key is '{0}' and the value is: {1}", dictionaryEntry.Key, dictionaryEntry.Value));
        }
      }
      finally
      {
        if (mySqlConnection != null)
          mySqlConnection.Close();
      }
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      ComponentResourceManager componentResourceManager = new ComponentResourceManager(typeof (Login));
      this.label1 = new Label();
      this.label2 = new Label();
      this.LoginUsername = new TextBox();
      this.LoginPassword = new TextBox();
      this.ExitLogin = new Button();
      this.LoginButton = new Button();
      this.label3 = new Label();
      this.SQLServer = new TextBox();
      this.SQLPort = new TextBox();
      this.lable9 = new Label();
      this.cMySQL = new CheckBox();
      this.SuspendLayout();
      this.label1.AutoSize = true;
      this.label1.Location = new Point(37, 15);
      this.label1.Name = "label1";
      this.label1.Size = new Size(58, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Username:";
      this.label2.AutoSize = true;
      this.label2.Location = new Point(39, 41);
      this.label2.Name = "label2";
      this.label2.Size = new Size(56, 13);
      this.label2.TabIndex = 2;
      this.label2.Text = "Password:";
      this.LoginUsername.Location = new Point(101, 12);
      this.LoginUsername.Name = "LoginUsername";
      this.LoginUsername.Size = new Size(142, 20);
      this.LoginUsername.TabIndex = 1;
      this.LoginUsername.TextChanged += new EventHandler(this.LoginChange);
      this.LoginUsername.KeyPress += new KeyPressEventHandler(this.onEnterKey);
      this.LoginPassword.Location = new Point(101, 38);
      this.LoginPassword.Name = "LoginPassword";
      this.LoginPassword.PasswordChar = '*';
      this.LoginPassword.Size = new Size(142, 20);
      this.LoginPassword.TabIndex = 3;
      this.LoginPassword.TextChanged += new EventHandler(this.LoginChange);
      this.LoginPassword.KeyPress += new KeyPressEventHandler(this.onEnterKey);
      this.ExitLogin.Location = new Point(18, 123);
      this.ExitLogin.Name = "ExitLogin";
      this.ExitLogin.Size = new Size(77, 28);
      this.ExitLogin.TabIndex = 8;
      this.ExitLogin.Text = "Cancel";
      this.ExitLogin.UseVisualStyleBackColor = true;
      this.ExitLogin.Click += new EventHandler(this.ExitLogin_Click);
      this.LoginButton.Location = new Point(189, 123);
      this.LoginButton.Name = "LoginButton";
      this.LoginButton.Size = new Size(77, 28);
      this.LoginButton.TabIndex = 9;
      this.LoginButton.Text = nameof (Login);
      this.LoginButton.UseVisualStyleBackColor = true;
      this.LoginButton.Click += new EventHandler(this.LoginButton_Click);
      this.label3.AutoSize = true;
      this.label3.Location = new Point(35, 67);
      this.label3.Name = "label3";
      this.label3.Size = new Size(56, 13);
      this.label3.TabIndex = 4;
      this.label3.Text = "SQL Host:";
      this.SQLServer.Location = new Point(101, 64);
      this.SQLServer.Name = "SQLServer";
      this.SQLServer.Size = new Size(142, 20);
      this.SQLServer.TabIndex = 5;
      this.SQLServer.TextChanged += new EventHandler(this.LoginChange);
      this.SQLServer.KeyPress += new KeyPressEventHandler(this.onEnterKey);
      this.SQLPort.Location = new Point(101, 90);
      this.SQLPort.Name = "SQLPort";
      this.SQLPort.Size = new Size(61, 20);
      this.SQLPort.TabIndex = 7;
      this.SQLPort.TextChanged += new EventHandler(this.LoginChange);
      this.SQLPort.KeyPress += new KeyPressEventHandler(this.onEnterKey);
      this.lable9.AutoSize = true;
      this.lable9.Location = new Point(35, 93);
      this.lable9.Name = "lable9";
      this.lable9.Size = new Size(53, 13);
      this.lable9.TabIndex = 6;
      this.lable9.Text = "SQL Port:";
      this.cMySQL.AutoSize = true;
      this.cMySQL.Location = new Point(168, 92);
      this.cMySQL.Name = "cMySQL";
      this.cMySQL.Size = new Size(92, 17);
      this.cMySQL.TabIndex = 11;
      this.cMySQL.Text = "Direct MySQL";
      this.cMySQL.UseVisualStyleBackColor = true;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(278, 158);
      this.Controls.Add((Control) this.cMySQL);
      this.Controls.Add((Control) this.SQLPort);
      this.Controls.Add((Control) this.lable9);
      this.Controls.Add((Control) this.LoginButton);
      this.Controls.Add((Control) this.ExitLogin);
      this.Controls.Add((Control) this.SQLServer);
      this.Controls.Add((Control) this.LoginPassword);
      this.Controls.Add((Control) this.LoginUsername);
      this.Controls.Add((Control) this.label3);
      this.Controls.Add((Control) this.label2);
      this.Controls.Add((Control) this.label1);
      this.FormBorderStyle = FormBorderStyle.FixedSingle;
      this.Icon = (Icon) componentResourceManager.GetObject("$this.Icon");
      this.MaximizeBox = false;
      this.Name = nameof (Login);
      this.StartPosition = FormStartPosition.CenterScreen;
      this.Text = nameof (Login);
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
