﻿// Decompiled with JetBrains decompiler
// Type: CommonTools.Gui.DlgSearch
// Assembly: CommonTools, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 665008E1-3954-4D3F-8606-DD931B438B82
// Assembly location: D:\Server\eab\Tools\CommonTools.DLL

using CommonTools.Database;
using Singleton;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace CommonTools.Gui
{
  public class DlgSearch : Form
  {
    private IContainer components = (IContainer) null;
    private ListBox guiSearchCriteriaTbl;
    private Button guiRemoveSearchCriteriaBtn;
    private Button guiEditSearchCriteriaBtn;
    private Button guiAddSearchCriteriaBtn;
    private Button guiSearchBtn;
    private Label guiResultLbl;
    private ListView guiResultTbl;
    private ColumnHeader searchResultListId;
    private ColumnHeader searchResultListName;
    private ColumnHeader searchResultListCategory;
    private ColumnHeader searchResultListSubCategory;
    private ColumnHeader searchResultListLevel;
    private ColumnHeader searchResultListStatus;
    private Button guiCancelBtn;
    private Button guiOkBtn;
    private ListViewColumnSorter lvwColumnSorter;
    private string m_selectedId;
    private DlgSearchCriteria m_dlgSearchCriteria;
    private Net7.Tables m_table;
    private Enum[] m_columns;
    private bool m_adjustedColumnWidths;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.guiSearchCriteriaTbl = new ListBox();
      this.guiRemoveSearchCriteriaBtn = new Button();
      this.guiEditSearchCriteriaBtn = new Button();
      this.guiAddSearchCriteriaBtn = new Button();
      this.guiSearchBtn = new Button();
      this.guiResultLbl = new Label();
      this.guiResultTbl = new ListView();
      this.searchResultListId = new ColumnHeader();
      this.searchResultListName = new ColumnHeader();
      this.searchResultListCategory = new ColumnHeader();
      this.searchResultListSubCategory = new ColumnHeader();
      this.searchResultListLevel = new ColumnHeader();
      this.searchResultListStatus = new ColumnHeader();
      this.guiCancelBtn = new Button();
      this.guiOkBtn = new Button();
      this.SuspendLayout();
      this.guiSearchCriteriaTbl.FormattingEnabled = true;
      this.guiSearchCriteriaTbl.Location = new Point(12, 12);
      this.guiSearchCriteriaTbl.Name = "guiSearchCriteriaTbl";
      this.guiSearchCriteriaTbl.Size = new Size(444, 95);
      this.guiSearchCriteriaTbl.TabIndex = 28;
      this.guiRemoveSearchCriteriaBtn.Location = new Point(276, 113);
      this.guiRemoveSearchCriteriaBtn.Name = "guiRemoveSearchCriteriaBtn";
      this.guiRemoveSearchCriteriaBtn.Size = new Size(87, 22);
      this.guiRemoveSearchCriteriaBtn.TabIndex = 27;
      this.guiRemoveSearchCriteriaBtn.Text = "&Remove";
      this.guiRemoveSearchCriteriaBtn.UseVisualStyleBackColor = true;
      this.guiRemoveSearchCriteriaBtn.Click += new EventHandler(this.onRemoveClick);
      this.guiEditSearchCriteriaBtn.Location = new Point(183, 113);
      this.guiEditSearchCriteriaBtn.Name = "guiEditSearchCriteriaBtn";
      this.guiEditSearchCriteriaBtn.Size = new Size(87, 22);
      this.guiEditSearchCriteriaBtn.TabIndex = 26;
      this.guiEditSearchCriteriaBtn.Text = "&Edit";
      this.guiEditSearchCriteriaBtn.UseVisualStyleBackColor = true;
      this.guiEditSearchCriteriaBtn.Click += new EventHandler(this.onEditClick);
      this.guiAddSearchCriteriaBtn.Location = new Point(90, 113);
      this.guiAddSearchCriteriaBtn.Name = "guiAddSearchCriteriaBtn";
      this.guiAddSearchCriteriaBtn.Size = new Size(87, 22);
      this.guiAddSearchCriteriaBtn.TabIndex = 25;
      this.guiAddSearchCriteriaBtn.Text = "&Add";
      this.guiAddSearchCriteriaBtn.UseVisualStyleBackColor = true;
      this.guiAddSearchCriteriaBtn.Click += new EventHandler(this.onAddClick);
      this.guiSearchBtn.Location = new Point(482, 12);
      this.guiSearchBtn.Name = "guiSearchBtn";
      this.guiSearchBtn.Size = new Size(125, 95);
      this.guiSearchBtn.TabIndex = 22;
      this.guiSearchBtn.Text = "&Search";
      this.guiSearchBtn.UseVisualStyleBackColor = true;
      this.guiSearchBtn.Click += new EventHandler(this.onSearchClick);
      this.guiResultLbl.AutoSize = true;
      this.guiResultLbl.Location = new Point(484, 113);
      this.guiResultLbl.Name = "guiResultLbl";
      this.guiResultLbl.Size = new Size(72, 13);
      this.guiResultLbl.TabIndex = 23;
      this.guiResultLbl.Text = "Search result:";
      this.guiResultTbl.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.guiResultTbl.Columns.AddRange(new ColumnHeader[6]
      {
        this.searchResultListId,
        this.searchResultListName,
        this.searchResultListCategory,
        this.searchResultListSubCategory,
        this.searchResultListLevel,
        this.searchResultListStatus
      });
      this.guiResultTbl.FullRowSelect = true;
      this.guiResultTbl.Location = new Point(8, 141);
      this.guiResultTbl.MultiSelect = false;
      this.guiResultTbl.Name = "guiResultTbl";
      this.guiResultTbl.Size = new Size(599, 302);
      this.guiResultTbl.TabIndex = 24;
      this.guiResultTbl.UseCompatibleStateImageBehavior = false;
      this.guiResultTbl.View = View.Details;
      this.guiResultTbl.DoubleClick += new EventHandler(this.onOkClick);
      this.guiResultTbl.ColumnClick += new ColumnClickEventHandler(this.onResultColumnClick);
      this.searchResultListId.Text = "ID";
      this.searchResultListId.Width = 57;
      this.searchResultListName.Text = "Name";
      this.searchResultListName.Width = 200;
      this.searchResultListCategory.Text = "Category";
      this.searchResultListCategory.Width = 120;
      this.searchResultListSubCategory.Text = "SubCategory";
      this.searchResultListSubCategory.Width = 124;
      this.searchResultListLevel.Text = "Level";
      this.searchResultListLevel.Width = 38;
      this.searchResultListStatus.Text = "Status";
      this.searchResultListStatus.Width = 38;
      this.guiCancelBtn.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.guiCancelBtn.Location = new Point(532, 449);
      this.guiCancelBtn.Name = "guiCancelBtn";
      this.guiCancelBtn.Size = new Size(75, 23);
      this.guiCancelBtn.TabIndex = 30;
      this.guiCancelBtn.Text = "Cancel";
      this.guiCancelBtn.UseVisualStyleBackColor = true;
      this.guiCancelBtn.Click += new EventHandler(this.onCancelClick);
      this.guiOkBtn.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.guiOkBtn.Location = new Point(451, 449);
      this.guiOkBtn.Name = "guiOkBtn";
      this.guiOkBtn.Size = new Size(75, 23);
      this.guiOkBtn.TabIndex = 29;
      this.guiOkBtn.Text = "Ok";
      this.guiOkBtn.UseVisualStyleBackColor = true;
      this.guiOkBtn.Click += new EventHandler(this.onOkClick);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(616, 484);
      this.Controls.Add((Control) this.guiCancelBtn);
      this.Controls.Add((Control) this.guiOkBtn);
      this.Controls.Add((Control) this.guiSearchCriteriaTbl);
      this.Controls.Add((Control) this.guiRemoveSearchCriteriaBtn);
      this.Controls.Add((Control) this.guiEditSearchCriteriaBtn);
      this.Controls.Add((Control) this.guiAddSearchCriteriaBtn);
      this.Controls.Add((Control) this.guiSearchBtn);
      this.Controls.Add((Control) this.guiResultLbl);
      this.Controls.Add((Control) this.guiResultTbl);
      this.Name = nameof (DlgSearch);
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = nameof (DlgSearch);
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public DlgSearch()
    {
      this.InitializeComponent();
      this.guiResultLbl.Text = "";
      this.m_columns = (Enum[]) null;
      this.m_adjustedColumnWidths = false;
      this.m_dlgSearchCriteria = (DlgSearchCriteria) null;
      this.lvwColumnSorter = new ListViewColumnSorter();
      this.guiResultTbl.ListViewItemSorter = (IComparer) this.lvwColumnSorter;
    }

    protected override void OnShown(EventArgs e)
    {
      base.OnShown(e);
      this.m_selectedId = "";
    }

    public string getSelectedId()
    {
      return this.m_selectedId;
    }

    private void onAddClick(object sender, EventArgs e)
    {
      int num = (int) this.m_dlgSearchCriteria.ShowDialog((IWin32Window) this);
      SearchCriteria searchCriteria = this.m_dlgSearchCriteria.getSearchCriteria();
      if (searchCriteria.IsEmpty())
        return;
      this.guiSearchCriteriaTbl.SelectedIndex = this.guiSearchCriteriaTbl.Items.Add((object) searchCriteria);
    }

    private void onEditClick(object sender, EventArgs e)
    {
      SearchCriteria selectedItem = (SearchCriteria) this.guiSearchCriteriaTbl.SelectedItem;
      if (selectedItem == null)
        return;
      this.m_dlgSearchCriteria.setSearchCriteria(selectedItem);
      int num = (int) this.m_dlgSearchCriteria.ShowDialog((IWin32Window) this);
      SearchCriteria searchCriteria = this.m_dlgSearchCriteria.getSearchCriteria();
      if (!searchCriteria.IsEmpty())
        this.guiSearchCriteriaTbl.Items[this.guiSearchCriteriaTbl.SelectedIndex] = (object) searchCriteria;
    }

    private void onRemoveClick(object sender, EventArgs e)
    {
      SearchCriteria selectedItem = (SearchCriteria) this.guiSearchCriteriaTbl.SelectedItem;
      if (selectedItem == null)
        return;
      int selectedIndex = this.guiSearchCriteriaTbl.SelectedIndex;
      this.guiSearchCriteriaTbl.Items.Remove((object) selectedItem);
      if (selectedIndex >= this.guiResultTbl.Items.Count)
        --selectedIndex;
      if (this.guiSearchCriteriaTbl.Items.Count != 0)
        this.guiSearchCriteriaTbl.SelectedIndex = selectedIndex;
    }

    public void configure(Net7.Tables table)
    {
      if (!this.m_table.Equals((object) table))
      {
        this.guiSearchCriteriaTbl.Items.Clear();
        this.guiResultTbl.Items.Clear();
        this.guiResultLbl.Text = "";
      }
      this.m_table = table;
      ColumnData.ColumnDataInfo[] columnDataInfo;
      switch (this.m_table)
      {
        case Net7.Tables.effects:
          this.m_columns = new Enum[3]
          {
            (Enum) Net7.Table_effects._effect_id,
            (Enum) Net7.Table_effects._description,
            (Enum) Net7.Table_effects._base_asset_id
          };
          columnDataInfo = Enumeration.ToColumnDataInfo<Net7.Table_effects>(true);
          break;
        case Net7.Tables.factions:
          this.m_columns = new Enum[2]
          {
            (Enum) Net7.Table_factions._faction_id,
            (Enum) Net7.Table_factions._name
          };
          columnDataInfo = Enumeration.ToColumnDataInfo<Net7.Table_factions>(true);
          break;
        case Net7.Tables.item_base:
          this.m_columns = new Enum[6]
          {
            (Enum) Net7.Table_item_base._id,
            (Enum) Net7.Table_item_base._name,
            (Enum) Net7.Table_item_base._category,
            (Enum) Net7.Table_item_base._sub_category,
            (Enum) Net7.Table_item_base._level,
            (Enum) Net7.Table_item_base._status
          };
          columnDataInfo = Enumeration.ToColumnDataInfo<Net7.Table_item_base>(true);
          break;
        case Net7.Tables.missions:
          this.m_columns = new Enum[4]
          {
            (Enum) Net7.Table_missions._mission_id,
            (Enum) Net7.Table_missions._mission_name,
            (Enum) Net7.Table_missions._mission_type,
            (Enum) Net7.Table_missions._mission_key
          };
          columnDataInfo = Enumeration.ToColumnDataInfo<Net7.Table_missions>(true);
          break;
        case Net7.Tables.mob_base:
          this.m_columns = new Enum[2]
          {
            (Enum) Net7.Table_mob_base._mob_id,
            (Enum) Net7.Table_mob_base._name
          };
          columnDataInfo = Enumeration.ToColumnDataInfo<Net7.Table_mob_base>(true);
          break;
        case Net7.Tables.sector_objects:
          this.m_columns = new Enum[4]
          {
            (Enum) Net7.Table_sector_objects._sector_object_id,
            (Enum) Net7.Table_sector_objects._name,
            (Enum) Net7.Table_sector_objects._base_asset_id,
            (Enum) Net7.Table_sector_objects._sector_id
          };
          columnDataInfo = Enumeration.ToColumnDataInfo<Net7.Table_sector_objects>(true);
          break;
        case Net7.Tables.sectors:
          this.m_columns = new Enum[2]
          {
            (Enum) Net7.Table_sectors._sector_id,
            (Enum) Net7.Table_sectors._name
          };
          columnDataInfo = Enumeration.ToColumnDataInfo<Net7.Table_sectors>(true);
          break;
        case Net7.Tables.skills:
          this.m_columns = new Enum[2]
          {
            (Enum) Net7.Table_skills._skill_id,
            (Enum) Net7.Table_skills._name
          };
          columnDataInfo = Enumeration.ToColumnDataInfo<Net7.Table_skills>(true);
          break;
        case Net7.Tables.starbase_npcs:
          this.m_columns = new Enum[3]
          {
            (Enum) Net7.Table_starbase_npcs._npc_Id,
            (Enum) Net7.Table_starbase_npcs._first_name,
            (Enum) Net7.Table_starbase_npcs._last_name
          };
          columnDataInfo = Enumeration.ToColumnDataInfo<Net7.Table_starbase_npcs>(true);
          break;
        default:
          throw new Exception("DlgSearch.configure() does not handle the table " + table.ToString());
      }
      this.guiResultTbl.Columns.Clear();
      this.m_adjustedColumnWidths = false;
      for (int index = 0; index < this.m_columns.Length; ++index)
        this.guiResultTbl.Columns.Add(new ColumnHeader()
        {
          DisplayIndex = index,
          Text = ColumnData.GetName(this.m_columns[index]),
          Width = -2
        });
      this.m_dlgSearchCriteria = new DlgSearchCriteria();
      this.m_dlgSearchCriteria.configure(columnDataInfo);
    }

    private void onSearchClick(object sender, EventArgs e)
    {
      string str1 = "";
      List<string> stringList1 = new List<string>();
      List<string> stringList2 = new List<string>();
      int num = 0;
      foreach (SearchCriteria searchCriteria in this.guiSearchCriteriaTbl.Items)
      {
        if (str1.Length != 0)
          str1 += " AND ";
        str1 = str1 + searchCriteria.getQuery() + num.ToString();
        stringList1.Add(searchCriteria.sqlParameter + num.ToString());
        stringList2.Add(searchCriteria.sqlValue);
        ++num;
      }
      if (str1.Length == 0)
        return;
      string str2 = "";
      foreach (Enum column in this.m_columns)
        str2 = (str2.Length != 0 ? str2 + "," : "SELECT ") + ColumnData.GetName(column);
      DataTable dataTable = Get<DB>.Instance.executeQuery(str2 + " FROM " + this.m_table.ToString() + " WHERE " + str1, stringList1.ToArray(), stringList2.ToArray());
      this.guiResultTbl.Items.Clear();
      this.guiResultLbl.Text = dataTable.Rows.Count.ToString() + " items";
      List<string> stringList3 = new List<string>();
      foreach (DataRow row in (InternalDataCollectionBase) dataTable.Rows)
      {
        stringList3.Clear();
        for (int index = 0; index < dataTable.Columns.Count; ++index)
          stringList3.Add(row[index].ToString());
        this.guiResultTbl.Items.Add(new ListViewItem(stringList3.ToArray()));
      }
      if (!this.m_adjustedColumnWidths)
      {
        List<int> intList = new List<int>();
        this.m_adjustedColumnWidths = true;
        foreach (ColumnHeader column in this.guiResultTbl.Columns)
        {
          column.Width = -2;
          intList.Add(column.Width);
        }
      }
    }

    private void onResultColumnClick(object sender, ColumnClickEventArgs e)
    {
      if (e.Column == this.lvwColumnSorter.SortColumn)
      {
        this.lvwColumnSorter.Order = this.lvwColumnSorter.Order != SortOrder.Ascending ? SortOrder.Ascending : SortOrder.Descending;
      }
      else
      {
        this.lvwColumnSorter.SortColumn = e.Column;
        this.lvwColumnSorter.Order = SortOrder.Ascending;
      }
      this.guiResultTbl.Sort();
    }

    private void onOkClick(object sender, EventArgs e)
    {
      if (this.guiResultTbl.SelectedItems == null || this.guiResultTbl.SelectedItems.Count == 0)
        return;
      this.m_selectedId = this.guiResultTbl.SelectedItems[0].SubItems[0].Text;
      this.Close();
    }

    private void onCancelClick(object sender, EventArgs e)
    {
      this.Close();
    }
  }
}
