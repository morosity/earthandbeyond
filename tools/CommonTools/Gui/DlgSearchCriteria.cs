﻿// Decompiled with JetBrains decompiler
// Type: CommonTools.Gui.DlgSearchCriteria
// Assembly: CommonTools, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 665008E1-3954-4D3F-8606-DD931B438B82
// Assembly location: D:\Server\eab\Tools\CommonTools.DLL

using CommonTools.Database;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace CommonTools.Gui
{
  public class DlgSearchCriteria : Form
  {
    private SearchCriteria m_searchCriteria = new SearchCriteria();
    private IContainer components = (IContainer) null;
    private Button guiOkBtn;
    private Button guiCancelBtn;
    private ComboBox guiFieldCbo;
    private Label guiFieldLbl;
    private Button guiPatternTipBtn;
    private TextBox guiPatternTxt;
    private Label guiPatternLbl;
    private ComboBox guiComparisonCbo;
    private TextBox guiComparisonTxt;
    private Label guiComparisonLbl;

    public DlgSearchCriteria()
    {
      this.InitializeComponent();
      this.guiComparisonCbo.Items.Clear();
      this.guiComparisonCbo.Items.Add((object) (SearchCriteria.getComparatorSymbol(0) + "   (lesser than)"));
      this.guiComparisonCbo.Items.Add((object) (SearchCriteria.getComparatorSymbol(1) + "  (lesser or equal)"));
      this.guiComparisonCbo.Items.Add((object) (SearchCriteria.getComparatorSymbol(2) + "   (equal)"));
      this.guiComparisonCbo.Items.Add((object) (SearchCriteria.getComparatorSymbol(3) + "  (greater or equal)"));
      this.guiComparisonCbo.Items.Add((object) (SearchCriteria.getComparatorSymbol(4) + "   (greater than)"));
      this.guiComparisonCbo.SelectedItem = this.guiComparisonCbo.Items[0];
    }

    protected override void OnShown(EventArgs e)
    {
      base.OnShown(e);
      this.m_searchCriteria = new SearchCriteria();
    }

    public void configure(ColumnData.ColumnDataInfo[] fieldNames)
    {
      this.guiFieldCbo.Items.Clear();
      foreach (object fieldName in fieldNames)
        this.guiFieldCbo.Items.Add(fieldName);
      if (this.guiFieldCbo.Items.Count == 0)
        return;
      this.guiFieldCbo.SelectedItem = this.guiFieldCbo.Items[0];
      this.onFieldSelected((object) null, (EventArgs) null);
    }

    public SearchCriteria getSearchCriteria()
    {
      return this.m_searchCriteria;
    }

    public void setSearchCriteria(SearchCriteria searchCriteria)
    {
      this.m_searchCriteria = searchCriteria;
      for (int index = 0; index < this.guiFieldCbo.Items.Count; ++index)
      {
        if (((ColumnData.ColumnDataInfo) this.guiFieldCbo.Items[index]).Name.Equals(this.m_searchCriteria.column))
        {
          this.guiFieldCbo.SelectedIndex = index;
          break;
        }
      }
      this.onFieldSelected((object) null, (EventArgs) null);
    }

    private void onPatternTip_Click(object sender, EventArgs e)
    {
      int num = (int) MessageBox.Show("Use the % character to match against 'any'.\nFor example:\n\tchavez%\t\t to find names beginning with 'chavez'\n\t%chemical%\t to find names containing 'chemical'\n\t%missile\t\t to find names ending with 'missile'.");
    }

    private void onFieldSelected(object sender, EventArgs e)
    {
      bool typeIsString = ((ColumnData.ColumnDataInfo) this.guiFieldCbo.SelectedItem).TypeIsString;
      this.guiPatternTxt.Enabled = typeIsString;
      this.guiPatternTxt.Text = this.guiPatternTxt.Enabled ? this.m_searchCriteria.criteria : "";
      this.guiPatternTipBtn.Enabled = typeIsString;
      this.guiComparisonCbo.Enabled = !typeIsString;
      if (this.guiComparisonCbo.Enabled)
        this.guiComparisonCbo.SelectedItem = this.guiComparisonCbo.Items[this.m_searchCriteria.comparisonSymbol];
      else
        this.guiComparisonCbo.Text = "";
      this.guiComparisonTxt.Enabled = this.guiComparisonCbo.Enabled;
      this.guiComparisonTxt.Text = this.guiComparisonTxt.Enabled ? this.m_searchCriteria.criteria : "";
    }

    private void onOk(object sender, EventArgs e)
    {
      ColumnData.ColumnDataInfo selectedItem = (ColumnData.ColumnDataInfo) this.guiFieldCbo.SelectedItem;
      if (selectedItem.TypeIsString)
      {
        this.m_searchCriteria.comparisonSymbol = this.guiPatternTxt.Text.Length == 0 ? 2 : 5;
        this.m_searchCriteria.criteria = this.guiPatternTxt.Text;
        this.m_searchCriteria.subQuery = selectedItem.Name + " " + this.m_searchCriteria.getComparatorSymbol() + " ?" + selectedItem.Name;
        this.m_searchCriteria.sqlParameter = selectedItem.Name;
        this.m_searchCriteria.sqlValue = this.guiPatternTxt.Text;
      }
      else
      {
        if (this.guiComparisonTxt.Text.Length == 0)
        {
          int num = (int) MessageBox.Show("You must enter a comparison value");
          this.guiComparisonTxt.Focus();
          return;
        }
        this.m_searchCriteria.comparisonSymbol = this.guiComparisonCbo.SelectedIndex;
        this.m_searchCriteria.criteria = this.guiComparisonTxt.Text;
        this.m_searchCriteria.subQuery = selectedItem.Name + " " + this.m_searchCriteria.getComparatorSymbol() + " ?" + selectedItem.Name;
        this.m_searchCriteria.sqlParameter = selectedItem.Name;
        this.m_searchCriteria.sqlValue = this.guiComparisonTxt.Text;
      }
      this.m_searchCriteria.column = selectedItem.Name;
      this.Close();
    }

    private void onCancel(object sender, EventArgs e)
    {
      this.Close();
    }

    private void onKeyDown(object sender, KeyEventArgs e)
    {
      if (sender.Equals((object) this.guiFieldCbo) && this.guiFieldCbo.DroppedDown || sender.Equals((object) this.guiComparisonCbo) && this.guiComparisonCbo.DroppedDown)
        return;
      if (e.KeyCode.Equals((object) Keys.Return))
      {
        this.onOk((object) null, (EventArgs) null);
      }
      else
      {
        if (!e.KeyCode.Equals((object) Keys.Escape))
          return;
        this.onCancel((object) null, (EventArgs) null);
      }
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.guiOkBtn = new Button();
      this.guiCancelBtn = new Button();
      this.guiFieldCbo = new ComboBox();
      this.guiFieldLbl = new Label();
      this.guiPatternTipBtn = new Button();
      this.guiPatternTxt = new TextBox();
      this.guiPatternLbl = new Label();
      this.guiComparisonCbo = new ComboBox();
      this.guiComparisonTxt = new TextBox();
      this.guiComparisonLbl = new Label();
      this.SuspendLayout();
      this.guiOkBtn.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.guiOkBtn.Location = new Point(302, 94);
      this.guiOkBtn.Name = "guiOkBtn";
      this.guiOkBtn.Size = new Size(75, 23);
      this.guiOkBtn.TabIndex = 9;
      this.guiOkBtn.Text = "Ok";
      this.guiOkBtn.UseVisualStyleBackColor = true;
      this.guiOkBtn.Click += new EventHandler(this.onOk);
      this.guiOkBtn.KeyDown += new KeyEventHandler(this.onKeyDown);
      this.guiCancelBtn.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.guiCancelBtn.Location = new Point(382, 94);
      this.guiCancelBtn.Name = "guiCancelBtn";
      this.guiCancelBtn.Size = new Size(75, 23);
      this.guiCancelBtn.TabIndex = 10;
      this.guiCancelBtn.Text = "Cancel";
      this.guiCancelBtn.UseVisualStyleBackColor = true;
      this.guiCancelBtn.Click += new EventHandler(this.onCancel);
      this.guiCancelBtn.KeyDown += new KeyEventHandler(this.onKeyDown);
      this.guiFieldCbo.DropDownStyle = ComboBoxStyle.DropDownList;
      this.guiFieldCbo.FormattingEnabled = true;
      this.guiFieldCbo.Location = new Point(79, 12);
      this.guiFieldCbo.MaxDropDownItems = 20;
      this.guiFieldCbo.Name = "guiFieldCbo";
      this.guiFieldCbo.Size = new Size(378, 21);
      this.guiFieldCbo.TabIndex = 1;
      this.guiFieldCbo.SelectionChangeCommitted += new EventHandler(this.onFieldSelected);
      this.guiFieldCbo.KeyDown += new KeyEventHandler(this.onKeyDown);
      this.guiFieldLbl.AutoSize = true;
      this.guiFieldLbl.Location = new Point(41, 15);
      this.guiFieldLbl.Name = "guiFieldLbl";
      this.guiFieldLbl.Size = new Size(32, 13);
      this.guiFieldLbl.TabIndex = 0;
      this.guiFieldLbl.Text = "Field:";
      this.guiFieldLbl.TextAlign = ContentAlignment.MiddleRight;
      this.guiPatternTipBtn.Anchor = AnchorStyles.Top | AnchorStyles.Right;
      this.guiPatternTipBtn.BackColor = Color.LightSteelBlue;
      this.guiPatternTipBtn.Location = new Point(438, 34);
      this.guiPatternTipBtn.Name = "guiPatternTipBtn";
      this.guiPatternTipBtn.Size = new Size(19, 23);
      this.guiPatternTipBtn.TabIndex = 4;
      this.guiPatternTipBtn.Text = "?";
      this.guiPatternTipBtn.UseVisualStyleBackColor = false;
      this.guiPatternTipBtn.Click += new EventHandler(this.onPatternTip_Click);
      this.guiPatternTipBtn.KeyDown += new KeyEventHandler(this.onKeyDown);
      this.guiPatternTxt.Anchor = AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right;
      this.guiPatternTxt.Location = new Point(79, 36);
      this.guiPatternTxt.Name = "guiPatternTxt";
      this.guiPatternTxt.Size = new Size(353, 20);
      this.guiPatternTxt.TabIndex = 3;
      this.guiPatternTxt.KeyDown += new KeyEventHandler(this.onKeyDown);
      this.guiPatternLbl.AutoSize = true;
      this.guiPatternLbl.Location = new Point(29, 39);
      this.guiPatternLbl.Name = "guiPatternLbl";
      this.guiPatternLbl.Size = new Size(44, 13);
      this.guiPatternLbl.TabIndex = 2;
      this.guiPatternLbl.Text = "Pattern:";
      this.guiPatternLbl.TextAlign = ContentAlignment.MiddleRight;
      this.guiComparisonCbo.DropDownStyle = ComboBoxStyle.DropDownList;
      this.guiComparisonCbo.Font = new Font("Courier New", 8.25f, FontStyle.Regular, GraphicsUnit.Point, (byte) 0);
      this.guiComparisonCbo.FormattingEnabled = true;
      this.guiComparisonCbo.Location = new Point(79, 62);
      this.guiComparisonCbo.Name = "guiComparisonCbo";
      this.guiComparisonCbo.Size = new Size(297, 22);
      this.guiComparisonCbo.TabIndex = 6;
      this.guiComparisonCbo.KeyDown += new KeyEventHandler(this.onKeyDown);
      this.guiComparisonTxt.Location = new Point(382, 62);
      this.guiComparisonTxt.Name = "guiComparisonTxt";
      this.guiComparisonTxt.Size = new Size(75, 20);
      this.guiComparisonTxt.TabIndex = 7;
      this.guiComparisonTxt.KeyDown += new KeyEventHandler(this.onKeyDown);
      this.guiComparisonLbl.AutoSize = true;
      this.guiComparisonLbl.Location = new Point(8, 65);
      this.guiComparisonLbl.Name = "guiComparisonLbl";
      this.guiComparisonLbl.Size = new Size(65, 13);
      this.guiComparisonLbl.TabIndex = 5;
      this.guiComparisonLbl.Text = "Comparison:";
      this.guiComparisonLbl.TextAlign = ContentAlignment.MiddleRight;
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(469, 129);
      this.Controls.Add((Control) this.guiComparisonLbl);
      this.Controls.Add((Control) this.guiComparisonTxt);
      this.Controls.Add((Control) this.guiComparisonCbo);
      this.Controls.Add((Control) this.guiPatternTipBtn);
      this.Controls.Add((Control) this.guiPatternTxt);
      this.Controls.Add((Control) this.guiPatternLbl);
      this.Controls.Add((Control) this.guiFieldLbl);
      this.Controls.Add((Control) this.guiFieldCbo);
      this.Controls.Add((Control) this.guiCancelBtn);
      this.Controls.Add((Control) this.guiOkBtn);
      this.Name = "FrmSearchCriteria";
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "Search Criteria";
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
