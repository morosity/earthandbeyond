﻿// Decompiled with JetBrains decompiler
// Type: CommonTools.Gui.DlgEditXml
// Assembly: CommonTools, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 665008E1-3954-4D3F-8606-DD931B438B82
// Assembly location: D:\Server\eab\Tools\CommonTools.DLL

using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace CommonTools.Gui
{
  public class DlgEditXml : Form
  {
    private IContainer components = (IContainer) null;
    private bool m_madeSelection;
    private string m_xml;
    private Button guiOkBtn;
    private Button guiCancelBtn;
    private TextBox guiXmlTxt;

    public DlgEditXml()
    {
      this.InitializeComponent();
      this.m_xml = "";
    }

    public void setXml(string xml)
    {
      this.m_xml = xml;
      this.guiXmlTxt.Text = this.m_xml;
    }

    protected override void OnShown(EventArgs e)
    {
      base.OnShown(e);
      this.m_madeSelection = false;
      this.guiXmlTxt.Focus();
      this.guiXmlTxt.Select(0, 0);
      this.guiXmlTxt.TextChanged += new EventHandler(this.onTextChanged);
    }

    private void onOk(object sender, EventArgs e)
    {
      this.m_madeSelection = true;
      this.m_xml = this.guiXmlTxt.Text;
      this.Close();
    }

    private void onCancel(object sender, EventArgs e)
    {
      this.Close();
    }

    public bool getValues(out string xml)
    {
      xml = this.m_xml;
      return this.m_madeSelection;
    }

    private void onTextChanged(object sender, EventArgs e)
    {
      this.guiOkBtn.Enabled = true;
      this.guiXmlTxt.TextChanged -= new EventHandler(this.onTextChanged);
    }

    private void onKeyDown(object sender, KeyEventArgs e)
    {
      if (!e.Control || e.KeyCode != Keys.A)
        return;
      this.guiXmlTxt.SelectAll();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.guiOkBtn = new Button();
      this.guiCancelBtn = new Button();
      this.guiXmlTxt = new TextBox();
      this.SuspendLayout();
      this.guiOkBtn.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.guiOkBtn.Enabled = false;
      this.guiOkBtn.Location = new Point(460, 573);
      this.guiOkBtn.Name = "guiOkBtn";
      this.guiOkBtn.Size = new Size(75, 23);
      this.guiOkBtn.TabIndex = 1;
      this.guiOkBtn.Text = "Ok";
      this.guiOkBtn.UseVisualStyleBackColor = true;
      this.guiOkBtn.Click += new EventHandler(this.onOk);
      this.guiCancelBtn.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
      this.guiCancelBtn.Location = new Point(541, 573);
      this.guiCancelBtn.Name = "guiCancelBtn";
      this.guiCancelBtn.Size = new Size(75, 23);
      this.guiCancelBtn.TabIndex = 2;
      this.guiCancelBtn.Text = "Cancel";
      this.guiCancelBtn.UseVisualStyleBackColor = true;
      this.guiCancelBtn.Click += new EventHandler(this.onCancel);
      this.guiXmlTxt.AcceptsReturn = true;
      this.guiXmlTxt.AcceptsTab = true;
      this.guiXmlTxt.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.guiXmlTxt.Location = new Point(11, 12);
      this.guiXmlTxt.Multiline = true;
      this.guiXmlTxt.Name = "guiXmlTxt";
      this.guiXmlTxt.ScrollBars = ScrollBars.Both;
      this.guiXmlTxt.Size = new Size(604, 549);
      this.guiXmlTxt.TabIndex = 0;
      this.guiXmlTxt.WordWrap = false;
      this.guiXmlTxt.KeyDown += new KeyEventHandler(this.onKeyDown);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(628, 608);
      this.Controls.Add((Control) this.guiXmlTxt);
      this.Controls.Add((Control) this.guiCancelBtn);
      this.Controls.Add((Control) this.guiOkBtn);
      this.Name = nameof (DlgEditXml);
      this.StartPosition = FormStartPosition.CenterParent;
      this.Text = "Edit XML";
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
