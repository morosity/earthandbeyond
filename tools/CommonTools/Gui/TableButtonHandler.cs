﻿// Decompiled with JetBrains decompiler
// Type: CommonTools.Gui.TableButtonHandler
// Assembly: CommonTools, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 665008E1-3954-4D3F-8606-DD931B438B82
// Assembly location: D:\Server\eab\Tools\CommonTools.DLL

using System;
using System.Windows.Forms;

namespace CommonTools.Gui
{
  public class TableButtonHandler
  {
    private ListView table;
    private Button add;
    private Button delete;
    private Button edit;
    private Button up;
    private Button down;

    public TableButtonHandler(ListView table, Button add, Button delete, Button edit, Button up, Button down)
    {
      this.table = table;
      this.add = add;
      this.delete = delete;
      this.edit = edit;
      this.up = up;
      this.down = down;
      table.SelectedIndexChanged += new EventHandler(this.onTableRowSelected);
      up.Click += new EventHandler(this.onMoveUp);
      down.Click += new EventHandler(this.onMoveDown);
    }

    private void onTableRowSelected(object sender, EventArgs e)
    {
      this.delete.Enabled = this.table.SelectedItems.Count != 0;
      if (this.edit != null)
        this.edit.Enabled = false;
      this.up.Enabled = this.table.SelectedItems.Count == 1 && this.table.SelectedIndices[0] != 0;
      this.down.Enabled = this.table.SelectedItems.Count == 1 && this.table.SelectedIndices[0] != this.table.Items.Count - 1;
    }

    private void onMoveUp(object sender, EventArgs e)
    {
      this.table.BeginUpdate();
      int index = this.table.SelectedItems[0].Index;
      ListViewItem listViewItem = this.table.Items[index];
      this.table.Items.Remove(listViewItem);
      this.table.Items.Insert(index - 1, listViewItem);
      this.table.EndUpdate();
    }

    private void onMoveDown(object sender, EventArgs e)
    {
      this.table.BeginUpdate();
      int index = this.table.SelectedItems[0].Index;
      ListViewItem listViewItem = this.table.Items[index];
      this.table.Items.Remove(listViewItem);
      this.table.Items.Insert(index + 1, listViewItem);
      this.table.EndUpdate();
    }
  }
}
