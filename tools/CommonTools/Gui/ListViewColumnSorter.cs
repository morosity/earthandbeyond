﻿// Decompiled with JetBrains decompiler
// Type: CommonTools.Gui.ListViewColumnSorter
// Assembly: CommonTools, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 665008E1-3954-4D3F-8606-DD931B438B82
// Assembly location: D:\Server\eab\Tools\CommonTools.DLL

using System.Collections;
using System.Windows.Forms;

namespace CommonTools.Gui
{
  public class ListViewColumnSorter : IComparer
  {
    private int ColumnToSort;
    private SortOrder OrderOfSort;
    private CaseInsensitiveComparer ObjectCompare;

    public ListViewColumnSorter()
    {
      this.ColumnToSort = 0;
      this.OrderOfSort = SortOrder.None;
      this.ObjectCompare = new CaseInsensitiveComparer();
    }

    public int Compare(object x, object y)
    {
      int num = this.ObjectCompare.Compare((object) ((ListViewItem) x).SubItems[this.ColumnToSort].Text, (object) ((ListViewItem) y).SubItems[this.ColumnToSort].Text);
      if (this.OrderOfSort == SortOrder.Ascending)
        return num;
      if (this.OrderOfSort == SortOrder.Descending)
        return -num;
      return 0;
    }

    public int SortColumn
    {
      set
      {
        this.ColumnToSort = value;
      }
      get
      {
        return this.ColumnToSort;
      }
    }

    public SortOrder Order
    {
      set
      {
        this.OrderOfSort = value;
      }
      get
      {
        return this.OrderOfSort;
      }
    }
  }
}
