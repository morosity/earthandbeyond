﻿// Decompiled with JetBrains decompiler
// Type: CommonTools.ConditionType
// Assembly: CommonTools, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 665008E1-3954-4D3F-8606-DD931B438B82
// Assembly location: D:\Server\eab\Tools\CommonTools.DLL

namespace CommonTools
{
  public enum ConditionType
  {
    Overall_Level = 1,
    Combat_Level = 2,
    Explore_Level = 3,
    Trade_Level = 4,
    Race = 5,
    Profession = 6,
    Hull_Level = 7,
    Faction_Required = 8,
    Item_Required = 9,
    Mission_Required = 10, // 0x0000000A
  }
}
