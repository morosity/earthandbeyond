﻿// Decompiled with JetBrains decompiler
// Type: CommonTools.Database.DataType
// Assembly: CommonTools, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 665008E1-3954-4D3F-8606-DD931B438B82
// Assembly location: D:\Server\eab\Tools\CommonTools.DLL

using System;

namespace CommonTools.Database
{
  internal class DataType : Attribute
  {
    public readonly string Text;

    public DataType(string text)
    {
      this.Text = text;
    }
  }
}
