﻿// Decompiled with JetBrains decompiler
// Type: CommonTools.Database.ColumnData
// Assembly: CommonTools, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 665008E1-3954-4D3F-8606-DD931B438B82
// Assembly location: D:\Server\eab\Tools\CommonTools.DLL

using System;
using System.Data;
using System.Reflection;

namespace CommonTools.Database
{
  public class ColumnData
  {
    public static string GetName(Enum tableEnumValue, Enum columnEnumValue)
    {
      return tableEnumValue.ToString() + "." + ColumnData.GetName(columnEnumValue);
    }

    public static string GetName(Enum enumValue)
    {
      string str = "";
      MemberInfo[] member = enumValue.GetType().GetMember(enumValue.ToString());
      if (member != null && member.Length == 1)
      {
        object[] customAttributes = member[0].GetCustomAttributes(typeof (ColName), false);
        if (customAttributes.Length == 1)
          str = ((ColName) customAttributes[0]).Text;
      }
      return str;
    }

    public static string GetDataType(Enum enumValue)
    {
      string str = "";
      MemberInfo[] member = enumValue.GetType().GetMember(enumValue.ToString());
      if (member != null && member.Length == 1)
      {
        object[] customAttributes = member[0].GetCustomAttributes(typeof (DataType), false);
        if (customAttributes.Length == 1)
          str = ((DataType) customAttributes[0]).Text;
      }
      return str;
    }

    public static bool IsDataTypeString(Enum enumValue)
    {
      string dataType = ColumnData.GetDataType(enumValue);
      return dataType.CompareTo("char") == 0 || dataType.CompareTo("longtext") == 0 || dataType.CompareTo("text") == 0 || dataType.CompareTo("varchar") == 0;
    }

    public static string GetString(DataRow dataRow, Enum enumValue)
    {
      string name = ColumnData.GetName(enumValue);
      return dataRow[name].ToString();
    }

    public static int GetInt32(DataRow dataRow, Enum enumValue)
    {
      string name = ColumnData.GetName(enumValue);
      string tableName = dataRow.Table.TableName;
      return int.Parse(dataRow[name].ToString());
    }

    public static bool GetBoolean(DataRow dataRow, Enum enumValue)
    {
      string name = ColumnData.GetName(enumValue);
      string str = dataRow[name].ToString();
      return str == "true" || str == "1";
    }

    public class ColumnDataInfo
    {
      public string DataType;
      public bool TypeIsString;
      public string Name;

      public override string ToString()
      {
        return this.Name;
      }
    }
  }
}
