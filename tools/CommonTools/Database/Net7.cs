﻿// Decompiled with JetBrains decompiler
// Type: CommonTools.Database.Net7
// Assembly: CommonTools, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 665008E1-3954-4D3F-8606-DD931B438B82
// Assembly location: D:\Server\eab\Tools\CommonTools.DLL

namespace CommonTools.Database
{
  public static class Net7
  {
    public enum Tables
    {
      assets,
      buffs,
      effects,
      faction_matrix,
      factions,
      item_ammo,
      item_ammo_type,
      item_base,
      item_beam,
      item_device,
      item_effect_base,
      item_effect_container,
      item_effect_stats,
      item_effects,
      item_engine,
      item_manufacture,
      item_manufacturer_base,
      item_missile,
      item_other_req,
      item_projectile,
      item_reactor,
      item_refine,
      item_shield,
      item_type,
      level_xp,
      manufacturers,
      missions,
      mob_base,
      mob_items,
      mob_spawn_group,
      sector_nav_points,
      sector_objects,
      sector_objects_harvestable,
      sector_objects_harvestable_restypes,
      sector_objects_mob,
      sector_objects_planets,
      sector_objects_starbases,
      sector_objects_stargates,
      sectors,
      skill_abilities,
      skill_levels,
      skills,
      starbase_npc_avatar_templates,
      starbase_npcs,
      starbase_rooms,
      starbase_terminals,
      starbase_vender_groups,
      starbase_vender_inventory,
      starbase_vendors,
      starbases,
      systems,
      table_changes,
      versions,
    }

    public enum Table_assets
    {
      [DataType("int"), ColName("base_id")] _base_id,
      [DataType("text"), ColName("descr")] _descr,
      [DataType("text"), ColName("main_cat")] _main_cat,
      [ColName("sub_cat"), DataType("text")] _sub_cat,
      [ColName("filename"), DataType("text")] _filename,
      [DataType("int"), ColName("rslid")] _rslid,
    }

    public enum Table_buffs
    {
      [DataType("bigint"), ColName("buff_id")] _buff_id,
      [DataType("varchar"), ColName("buff_name")] _buff_name,
      [ColName("StatName"), DataType("varchar")] _StatName,
      [DataType("int"), ColName("StatType")] _StatType,
      [DataType("int"), ColName("EffectID")] _EffectID,
      [ColName("tooltip"), DataType("text")] _tooltip,
      [DataType("text"), ColName("description")] _description,
      [ColName("is_good_buff"), DataType("tinyint")] _is_good_buff,
    }

    public enum Table_effects
    {
      [DataType("bigint"), ColName("effect_id")] _effect_id,
      [ColName("effect_class"), DataType("text")] _effect_class,
      [DataType("text"), ColName("description")] _description,
      [ColName("start_link_id"), DataType("bigint")] _start_link_id,
      [ColName("next_link_id"), DataType("bigint")] _next_link_id,
      [ColName("base_asset_id"), DataType("bigint")] _base_asset_id,
      [ColName("sound_fx_file"), DataType("text")] _sound_fx_file,
    }

    public enum Table_factions
    {
      [DataType("bigint"), ColName("faction_id")] _faction_id,
      [DataType("varchar"), ColName("name")] _name,
      [DataType("text"), ColName("description")] _description,
    }

    public enum Table_faction_matrix
    {
      [ColName("id"), DataType("bigint")] _id,
      [DataType("bigint"), ColName("faction_id")] _faction_id,
      [ColName("faction_entry_id"), DataType("bigint")] _faction_entry_id,
      [ColName("base_value"), DataType("int")] _base_value,
      [DataType("int"), ColName("current_value")] _current_value,
    }

    public enum Table_item_ammo
    {
      [DataType("int"), ColName("item_id")] _item_id,
      [ColName("ammo_type_id"), DataType("int")] _ammo_type_id,
      [ColName("damage_type"), DataType("tinyint")] _damage_type,
      [DataType("int"), ColName("fire_effect")] _fire_effect,
      [DataType("float unsigned"), ColName("maneuv_100")] _maneuv_100,
      [DataType("float unsigned"), ColName("damage_100")] _damage_100,
      [DataType("float unsigned"), ColName("range_100")] _range_100,
    }

    public enum Table_item_ammo_type
    {
      [ColName("id"), DataType("int")] _id,
      [DataType("smallint"), ColName("sub_category")] _sub_category,
      [DataType("varchar"), ColName("name")] _name,
    }

    public enum Table_item_base
    {
      [ColName("id"), DataType("int")] _id,
      [DataType("tinyint"), ColName("level")] _level,
      [DataType("smallint"), ColName("category")] _category,
      [ColName("sub_category"), DataType("smallint")] _sub_category,
      [DataType("tinyint"), ColName("type")] _type,
      [ColName("price"), DataType("int")] _price,
      [DataType("smallint"), ColName("max_stack")] _max_stack,
      [ColName("name"), DataType("varchar")] _name,
      [DataType("text"), ColName("description")] _description,
      [ColName("manufacturer"), DataType("int")] _manufacturer,
      [ColName("2d_asset"), DataType("int")] _2d_asset,
      [ColName("3d_asset"), DataType("int")] _3d_asset,
      [ColName("no_trade"), DataType("tinyint")] _no_trade,
      [DataType("tinyint"), ColName("no_store")] _no_store,
      [ColName("no_destroy"), DataType("tinyint")] _no_destroy,
      [DataType("tinyint"), ColName("no_manu")] _no_manu,
      [ColName("unique"), DataType("tinyint")] _unique,
      [ColName("item_base_id"), DataType("int")] _item_base_id,
      [DataType("tinyint"), ColName("custom_flag")] _custom_flag,
      [ColName("status"), DataType("tinyint")] _status,
      [DataType("bigint"), ColName("effect_id")] _effect_id,
    }

    public enum Table_item_beam
    {
      [ColName("item_id"), DataType("int")] _item_id,
      [DataType("tinyint"), ColName("rest_prof")] _rest_prof,
      [ColName("rest_race"), DataType("tinyint")] _rest_race,
      [ColName("damage_type"), DataType("tinyint")] _damage_type,
      [DataType("smallint"), ColName("fire_effect")] _fire_effect,
      [ColName("damage_100"), DataType("float unsigned")] _damage_100,
      [DataType("float unsigned"), ColName("range_100")] _range_100,
      [ColName("energy_100"), DataType("float unsigned")] _energy_100,
      [DataType("float unsigned"), ColName("reload_100")] _reload_100,
    }

    public enum Table_item_device
    {
      [DataType("int"), ColName("item_id")] _item_id,
      [DataType("tinyint"), ColName("rest_prof")] _rest_prof,
      [ColName("rest_race"), DataType("tinyint")] _rest_race,
      [ColName("energy_100"), DataType("float unsigned")] _energy_100,
      [DataType("smallint"), ColName("range_100")] _range_100,
    }

    public enum Table_item_effects
    {
      [ColName("ItemEffectID"), DataType("int")] _ItemEffectID,
      [DataType("int"), ColName("ItemID")] _ItemID,
      [DataType("int"), ColName("item_effect_base_id")] _item_effect_base_id,
      [ColName("Var1Data"), DataType("float")] _Var1Data,
      [DataType("float"), ColName("Var2Data")] _Var2Data,
      [DataType("float"), ColName("Var3Data")] _Var3Data,
    }

    public enum Table_item_effect_base
    {
      [DataType("int"), ColName("EffectID")] _EffectID,
      [DataType("int"), ColName("EffectType")] _EffectType,
      [DataType("varchar"), ColName("Name")] _Name,
      [DataType("varchar"), ColName("Description")] _Description,
      [ColName("Tooltip"), DataType("varchar")] _Tooltip,
      [ColName("flag1"), DataType("int")] _flag1,
      [DataType("int"), ColName("flag2")] _flag2,
      [ColName("Constant1Value"), DataType("float")] _Constant1Value,
      [DataType("varchar"), ColName("Constant1Stat")] _Constant1Stat,
      [ColName("Constant1Type"), DataType("int")] _Constant1Type,
      [ColName("Constant2Value"), DataType("float")] _Constant2Value,
      [ColName("Constant2Stat"), DataType("varchar")] _Constant2Stat,
      [DataType("int"), ColName("Constant2Type")] _Constant2Type,
      [ColName("Var1Stat"), DataType("varchar")] _Var1Stat,
      [DataType("int"), ColName("Var1Type")] _Var1Type,
      [DataType("varchar"), ColName("Var2Stat")] _Var2Stat,
      [DataType("int"), ColName("Var2Type")] _Var2Type,
      [ColName("Var3Stat"), DataType("varchar")] _Var3Stat,
      [ColName("Var3Type"), DataType("int")] _Var3Type,
      [DataType("varchar"), ColName("Buff_Name")] _Buff_Name,
    }

    public enum Table_item_effect_container
    {
      [ColName("EffectContainerID"), DataType("int")] _EffectContainerID,
      [ColName("ItemID"), DataType("int")] _ItemID,
      [DataType("int"), ColName("EquipEffect")] _EquipEffect,
      [ColName("RechargeTime"), DataType("int")] _RechargeTime,
      [ColName("Unknown2"), DataType("int")] _Unknown2,
      [DataType("int"), ColName("_Range")] __Range,
      [ColName("Unknown4"), DataType("int")] _Unknown4,
    }

    public enum Table_item_effect_stats
    {
      [DataType("varchar"), ColName("Stat_Name")] _Stat_Name,
      [ColName("Stat_Description"), DataType("varchar")] _Stat_Description,
    }

    public enum Table_item_engine
    {
      [DataType("int"), ColName("item_id")] _item_id,
      [ColName("rest_prof"), DataType("tinyint")] _rest_prof,
      [DataType("tinyint"), ColName("rest_race")] _rest_race,
      [ColName("warp"), DataType("smallint")] _warp,
      [ColName("warp_drain_100"), DataType("float unsigned")] _warp_drain_100,
      [ColName("thrust_100"), DataType("smallint")] _thrust_100,
      [DataType("smallint"), ColName("signature_100")] _signature_100,
      [DataType("float unsigned"), ColName("energy_100")] _energy_100,
      [ColName("range_100"), DataType("smallint")] _range_100,
    }

    public enum Table_item_manufacture
    {
      [ColName("item_id"), DataType("int")] _item_id,
      [DataType("int"), ColName("comp_1")] _comp_1,
      [ColName("comp_2"), DataType("int")] _comp_2,
      [ColName("comp_3"), DataType("int")] _comp_3,
      [DataType("int"), ColName("comp_4")] _comp_4,
      [ColName("comp_5"), DataType("int")] _comp_5,
      [ColName("comp_6"), DataType("int")] _comp_6,
      [DataType("int"), ColName("difficulty")] _difficulty,
    }

    public enum Table_item_manufacturer_base
    {
      [ColName("id"), DataType("int")] _id,
      [DataType("varchar"), ColName("name")] _name,
    }

    public enum Table_item_missile
    {
      [ColName("item_id"), DataType("int")] _item_id,
      [ColName("rest_prof"), DataType("tinyint")] _rest_prof,
      [DataType("tinyint"), ColName("rest_race")] _rest_race,
      [DataType("varchar"), ColName("ammo")] _ammo,
      [ColName("ammo_per_shot"), DataType("tinyint")] _ammo_per_shot,
      [DataType("float unsigned"), ColName("energy_100")] _energy_100,
      [DataType("float unsigned"), ColName("reload_100")] _reload_100,
      [ColName("ammo_type_id"), DataType("int")] _ammo_type_id,
    }

    public enum Table_item_other_req
    {
      [ColName("item_id"), DataType("int")] _item_id,
      [DataType("smallint"), ColName("overall_lvl")] _overall_lvl,
      [DataType("smallint"), ColName("combat_lvl")] _combat_lvl,
      [ColName("explore_lvl"), DataType("smallint")] _explore_lvl,
      [DataType("smallint"), ColName("trade_level")] _trade_level,
      [DataType("varchar"), ColName("other_skill")] _other_skill,
      [ColName("over_skill_lvl"), DataType("tinyint")] _over_skill_lvl,
      [ColName("energy_drain"), DataType("float unsigned")] _energy_drain,
      [DataType("float unsigned"), ColName("shield_drain")] _shield_drain,
    }

    public enum Table_item_projectile
    {
      [DataType("int"), ColName("item_id")] _item_id,
      [ColName("rest_prof"), DataType("tinyint")] _rest_prof,
      [DataType("tinyint"), ColName("rest_race")] _rest_race,
      [ColName("ammo"), DataType("varchar")] _ammo,
      [DataType("tinyint"), ColName("ammo_per_shot")] _ammo_per_shot,
      [ColName("range_100"), DataType("float unsigned")] _range_100,
      [DataType("float unsigned"), ColName("energy_100")] _energy_100,
      [DataType("float unsigned"), ColName("reload_100")] _reload_100,
      [DataType("int"), ColName("ammo_type_id")] _ammo_type_id,
    }

    public enum Table_item_reactor
    {
      [DataType("int"), ColName("item_id")] _item_id,
      [ColName("rest_prof"), DataType("tinyint")] _rest_prof,
      [ColName("rest_race"), DataType("tinyint")] _rest_race,
      [DataType("int"), ColName("cap_100")] _cap_100,
      [ColName("recharge_100"), DataType("float unsigned")] _recharge_100,
      [DataType("float unsigned"), ColName("energy_100")] _energy_100,
      [ColName("range_100"), DataType("smallint")] _range_100,
    }

    public enum Table_item_refine
    {
      [ColName("item_id"), DataType("int")] _item_id,
      [DataType("int"), ColName("refine_id")] _refine_id,
    }

    public enum Table_item_shield
    {
      [ColName("item_id"), DataType("int")] _item_id,
      [ColName("rest_prof"), DataType("tinyint")] _rest_prof,
      [DataType("tinyint"), ColName("rest_race")] _rest_race,
      [ColName("cap_100"), DataType("int")] _cap_100,
      [DataType("float unsigned"), ColName("recharge_100")] _recharge_100,
      [ColName("energy_100"), DataType("float unsigned")] _energy_100,
      [DataType("smallint"), ColName("range_100")] _range_100,
    }

    public enum Table_item_type
    {
      [ColName("id"), DataType("int")] _id,
      [DataType("varchar"), ColName("name")] _name,
    }

    public enum Table_level_xp
    {
      [DataType("bigint"), ColName("level")] _level,
      [ColName("trade_xp"), DataType("bigint")] _trade_xp,
      [ColName("explore_xp"), DataType("bigint")] _explore_xp,
      [DataType("bigint"), ColName("combat_xp")] _combat_xp,
    }

    public enum Table_manufacturers
    {
      [DataType("bigint"), ColName("manufacturer_id")] _manufacturer_id,
      [ColName("name"), DataType("varchar")] _name,
      [DataType("text"), ColName("slogan")] _slogan,
      [DataType("bigint"), ColName("faction_id")] _faction_id,
    }

    public enum Table_missions
    {
      [ColName("mission_id"), DataType("int")] _mission_id,
      [ColName("mission_minSecurityLevel"), DataType("int")] _mission_MinSecLvl,
      [DataType("text"), ColName("mission_XML")] _mission_XML,
      [DataType("text"), ColName("mission_name")] _mission_name,
      [DataType("int"), ColName("mission_key")] _mission_key,
      [ColName("mission_type"), DataType("smallint")] _mission_type,
    }

    public enum Table_mob_base
    {
      [DataType("bigint"), ColName("mob_id")] _mob_id,
      [ColName("name"), DataType("text")] _name,
      [DataType("int"), ColName("level")] _level,
      [DataType("float"), ColName("intelligence")] _intelligence,
      [DataType("float"), ColName("bravery")] _bravery,
      [ColName("type"), DataType("int")] _type,
      [ColName("faction_id"), DataType("bigint")] _faction_id,
      [DataType("bigint"), ColName("base_asset_id")] _base_asset_id,
      [ColName("altruism"), DataType("float")] _altruism,
      [DataType("float"), ColName("aggressiveness")] _aggressiveness,
      [DataType("text"), ColName("ai")] _ai,
      [ColName("h"), DataType("float")] _h,
      [ColName("s"), DataType("float")] _s,
      [ColName("v"), DataType("float")] _v,
      [DataType("float"), ColName("scale")] _scale,
    }

    public enum Table_mob_items
    {
      [DataType("bigint"), ColName("id")] _id,
      [ColName("mob_id"), DataType("bigint")] _mob_id,
      [ColName("item_base_id"), DataType("bigint")] _item_base_id,
      [DataType("int"), ColName("usage_chance")] _usage_chance,
      [DataType("int"), ColName("drop_chance")] _drop_chance,
      [DataType("int"), ColName("type")] _type,
      [ColName("qty"), DataType("int")] _qty,
    }

    public enum Table_mob_spawn_group
    {
      [ColName("id"), DataType("bigint")] _id,
      [DataType("bigint"), ColName("spawn_group_id")] _spawn_group_id,
      [ColName("mob_id"), DataType("bigint")] _mob_id,
      [DataType("bigint"), ColName("group_index")] _group_index,
    }

    public enum Table_sectors
    {
      [DataType("bigint"), ColName("sector_id")] _sector_id,
      [DataType("text"), ColName("name")] _name,
      [ColName("x_min"), DataType("float")] _x_min,
      [ColName("y_min"), DataType("float")] _y_min,
      [DataType("float"), ColName("z_min")] _z_min,
      [ColName("x_max"), DataType("float")] _x_max,
      [ColName("y_max"), DataType("float")] _y_max,
      [DataType("float"), ColName("z_max")] _z_max,
      [DataType("int"), ColName("grid_x")] _grid_x,
      [ColName("grid_y"), DataType("int")] _grid_y,
      [DataType("int"), ColName("grid_z")] _grid_z,
      [ColName("fog_near"), DataType("float")] _fog_near,
      [DataType("float"), ColName("fog_far")] _fog_far,
      [ColName("debris_mode"), DataType("int")] _debris_mode,
      [ColName("light_backdrop"), DataType("tinyint")] _light_backdrop,
      [ColName("fog_backdrop"), DataType("tinyint")] _fog_backdrop,
      [DataType("tinyint"), ColName("swap_backdrop")] _swap_backdrop,
      [DataType("float"), ColName("backdrop_fog_near")] _backdrop_fog_near,
      [ColName("backdrop_fog_far"), DataType("float")] _backdrop_fog_far,
      [ColName("max_tilt"), DataType("float")] _max_tilt,
      [DataType("tinyint"), ColName("auto_level")] _auto_level,
      [DataType("float"), ColName("impulse_rate")] _impulse_rate,
      [DataType("float"), ColName("decay_velocity")] _decay_velocity,
      [ColName("decay_spin"), DataType("float")] _decay_spin,
      [DataType("int"), ColName("backdrop_asset")] _backdrop_asset,
      [ColName("greetings"), DataType("text")] _greetings,
      [DataType("text"), ColName("notes")] _notes,
      [ColName("system_id"), DataType("int")] _system_id,
      [ColName("galaxy_x"), DataType("float")] _galaxy_x,
      [DataType("float"), ColName("galaxy_y")] _galaxy_y,
      [ColName("sector_ip_addr"), DataType("bigint")] _sector_ip_addr,
      [DataType("float"), ColName("galaxy_z")] _galaxy_z,
      [DataType("int"), ColName("sector_type")] _sector_type,
    }

    public enum Table_sector_nav_points
    {
      [DataType("bigint"), ColName("sector_object_id")] _sector_object_id,
      [ColName("nav_type"), DataType("int")] _nav_type,
      [DataType("float"), ColName("signature")] _signature,
      [DataType("tinyint"), ColName("is_huge")] _is_huge,
      [ColName("sector_id"), DataType("bigint")] _sector_id,
      [ColName("base_xp"), DataType("int")] _base_xp,
      [DataType("float"), ColName("exploration_range")] _exploration_range,
    }

    public enum Table_sector_objects
    {
      [DataType("bigint"), ColName("sector_object_id")] _sector_object_id,
      [DataType("int"), ColName("base_asset_id")] _base_asset_id,
      [DataType("float"), ColName("h")] _h,
      [ColName("s"), DataType("float")] _s,
      [DataType("float"), ColName("v")] _v,
      [ColName("type"), DataType("tinyint")] _type,
      [ColName("scale"), DataType("float")] _scale,
      [ColName("position_x"), DataType("float")] _position_x,
      [DataType("float"), ColName("position_y")] _position_y,
      [DataType("float"), ColName("position_z")] _position_z,
      [DataType("float"), ColName("orientation_u")] _orientation_u,
      [DataType("float"), ColName("orientation_v")] _orientation_v,
      [ColName("orientation_w"), DataType("float")] _orientation_w,
      [DataType("float"), ColName("orientation_z")] _orientation_z,
      [ColName("name"), DataType("text")] _name,
      [ColName("appears_in_radar"), DataType("tinyint")] _appears_in_radar,
      [DataType("float"), ColName("radar_range")] _radar_range,
      [DataType("bigint"), ColName("sector_id")] _sector_id,
      [ColName("gate_to"), DataType("bigint")] _gate_to,
      [ColName("sound_effect_id"), DataType("bigint")] _sound_effect_id,
      [DataType("float"), ColName("sound_effect_range")] _sound_effect_range,
    }

    public enum Table_sector_objects_harvestable
    {
      [DataType("bigint"), ColName("resource_id")] _resource_id,
      [ColName("level"), DataType("int")] _level,
      [DataType("int"), ColName("field")] _field,
      [ColName("res_count"), DataType("int")] _res_count,
      [ColName("spawn_radius"), DataType("float")] _spawn_radius,
      [DataType("float"), ColName("pop_rock_chance")] _pop_rock_chance,
      [ColName("max_field_radius"), DataType("float")] _max_field_radius,
      [ColName("respawn_timer"), DataType("int")] _respawn_timer,
    }

    public enum Table_sector_objects_harvestable_restypes
    {
      [DataType("bigint"), ColName("id")] _id,
      [ColName("group_id"), DataType("bigint")] _group_id,
      [DataType("int"), ColName("type")] _type,
    }

    public enum Table_sector_objects_mob
    {
      [ColName("mob_id"), DataType("bigint")] _mob_id,
      [DataType("int"), ColName("mob_count")] _mob_count,
      [ColName("mob_spawn_radius"), DataType("float")] _mob_spawn_radius,
      [DataType("float"), ColName("respawn_time")] _respawn_time,
      [DataType("tinyint"), ColName("delayed_spawn")] _delayed_spawn,
      [ColName("group_aggro"), DataType("tinyint")] _group_aggro,
    }

    public enum Table_sector_objects_planets
    {
      [DataType("bigint"), ColName("planet_id")] _planet_id,
      [DataType("int"), ColName("orbit_id")] _orbit_id,
      [ColName("orbit_dist"), DataType("float")] _orbit_dist,
      [DataType("float"), ColName("orbit_angle")] _orbit_angle,
      [ColName("orbit_rate"), DataType("float")] _orbit_rate,
      [DataType("float"), ColName("rotate_angle")] _rotate_angle,
      [ColName("rotate_rate"), DataType("float")] _rotate_rate,
      [DataType("float"), ColName("tilt_angle")] _tilt_angle,
      [ColName("is_landable"), DataType("tinyint")] _is_landable,
    }

    public enum Table_sector_objects_starbases
    {
      [ColName("starbase_id"), DataType("bigint")] _starbase_id,
      [ColName("capShip"), DataType("tinyint")] _capShip,
      [ColName("dockable"), DataType("tinyint")] _dockable,
    }

    public enum Table_sector_objects_stargates
    {
      [DataType("bigint"), ColName("stargate_id")] _stargate_id,
      [ColName("classSpecific"), DataType("tinyint")] _classSpecific,
      [DataType("bigint"), ColName("faction_id")] _faction_id,
    }

    public enum Table_sector_objects_turrets
    {
      [ColName("turret_id"), DataType("bigint")] _turret_id,
      [DataType("bigint"), ColName("turret_mob_id")] _turret_mob_id,
      [DataType("tinyint"), ColName("treat_as_mob")] _treat_as_mob,
    }

    public enum Table_skills
    {
      [DataType("bigint"), ColName("skill_id")] _skill_id,
      [ColName("name"), DataType("text")] _name,
      [ColName("description"), DataType("text")] _description,
      [DataType("tinyint"), ColName("is_activated")] _is_activated,
      [ColName("category"), DataType("text")] _category,
      [DataType("int"), ColName("warrior_max_level")] _warrior_max_level,
      [DataType("int"), ColName("sentinal_max_level")] _sentinal_max_level,
      [ColName("privateer_max_level"), DataType("int")] _privateer_max_level,
      [DataType("int"), ColName("defender_max_level")] _defender_max_level,
      [DataType("int"), ColName("explorer_max_level")] _explorer_max_level,
      [DataType("int"), ColName("seeker_max_level")] _seeker_max_level,
      [ColName("enforcer_max_level"), DataType("int")] _enforcer_max_level,
      [DataType("int"), ColName("scout_max_level")] _scout_max_level,
      [ColName("tradesman_max_level"), DataType("int")] _tradesman_max_level,
    }

    public enum Table_skill_abilities
    {
      [ColName("ability_id"), DataType("bigint")] _ability_id,
      [DataType("bigint"), ColName("skill_id")] _skill_id,
      [ColName("min_level"), DataType("int")] _min_level,
      [DataType("text"), ColName("description")] _description,
      [ColName("activation_cost"), DataType("int")] _activation_cost,
      [ColName("name"), DataType("text")] _name,
    }

    public enum Table_skill_levels
    {
      [DataType("bigint"), ColName("skill_level_id")] _skill_level_id,
      [ColName("skill_id"), DataType("bigint")] _skill_id,
      [DataType("int"), ColName("level")] _level,
      [DataType("text"), ColName("description")] _description,
    }

    public enum Table_starbases
    {
      [DataType("bigint"), ColName("starbase_id")] _starbase_id,
      [ColName("sector_id"), DataType("int")] _sector_id,
      [DataType("varchar"), ColName("name")] _name,
      [DataType("int"), ColName("type")] _type,
      [ColName("is_active"), DataType("int")] _is_active,
      [ColName("description"), DataType("varchar")] _description,
      [DataType("varchar"), ColName("welcome_message")] _welcome_message,
      [ColName("target_sector_object"), DataType("bigint")] _target_sector_object,
      [ColName("faction_id"), DataType("bigint")] _faction_id,
      [DataType("bigint"), ColName("starbase_sector_id")] _starbase_sector_id,
    }

    public enum Table_starbase_npcs
    {
      [ColName("npc_Id"), DataType("bigint")] _npc_Id,
      [DataType("varchar"), ColName("first_name")] _first_name,
      [ColName("last_name"), DataType("varchar")] _last_name,
      [DataType("int"), ColName("location")] _location,
      [ColName("faction_id"), DataType("bigint")] _faction_id,
      [DataType("varchar"), ColName("description")] _description,
      [ColName("talk_tree_handle"), DataType("varchar")] _talk_tree_handle,
      [ColName("room_id"), DataType("bigint")] _room_id,
      [ColName("npc_index"), DataType("int")] _npc_index,
    }

    public enum Table_starbase_npc_avatar_templates
    {
      [ColName("avatar_template_id"), DataType("bigint")] _avatar_template_id,
      [DataType("int"), ColName("avatar_type")] _avatar_type,
      [ColName("avatar_version"), DataType("tinyint")] _avatar_version,
      [DataType("int"), ColName("race")] _race,
      [ColName("profession"), DataType("int")] _profession,
      [ColName("gender"), DataType("int")] _gender,
      [DataType("int"), ColName("mood_type")] _mood_type,
      [ColName("personality"), DataType("tinyint")] _personality,
      [ColName("nlp"), DataType("tinyint")] _nlp,
      [DataType("tinyint"), ColName("shirt_type")] _shirt_type,
      [ColName("pants_type"), DataType("tinyint")] _pants_type,
      [DataType("tinyint"), ColName("head_type")] _head_type,
      [ColName("hair_type"), DataType("tinyint")] _hair_type,
      [ColName("ear_type"), DataType("tinyint")] _ear_type,
      [DataType("tinyint"), ColName("goggle_num")] _goggle_num,
      [DataType("tinyint"), ColName("beard_num")] _beard_num,
      [ColName("weapon_hip_num"), DataType("tinyint")] _weapon_hip_num,
      [DataType("tinyint"), ColName("weapon_unique_num")] _weapon_unique_num,
      [ColName("weapon_back_num"), DataType("tinyint")] _weapon_back_num,
      [DataType("tinyint"), ColName("head_texture_num")] _head_texture_num,
      [DataType("tinyint"), ColName("tatoo_texture_num")] _tatoo_texture_num,
      [DataType("float"), ColName("tatoo_x")] _tatoo_x,
      [DataType("float"), ColName("tatoo_y")] _tatoo_y,
      [DataType("float"), ColName("tatoo_z")] _tatoo_z,
      [DataType("float"), ColName("hair_color_0")] _hair_color_0,
      [ColName("hair_color_1"), DataType("float")] _hair_color_1,
      [ColName("hair_color_2"), DataType("float")] _hair_color_2,
      [DataType("float"), ColName("beard_color_0")] _beard_color_0,
      [DataType("float"), ColName("beard_color_1")] _beard_color_1,
      [ColName("beard_color_2"), DataType("float")] _beard_color_2,
      [DataType("float"), ColName("eye_color_0")] _eye_color_0,
      [ColName("eye_color_1"), DataType("float")] _eye_color_1,
      [ColName("eye_color_2"), DataType("float")] _eye_color_2,
      [DataType("float"), ColName("skin_color_0")] _skin_color_0,
      [DataType("float"), ColName("skin_color_1")] _skin_color_1,
      [DataType("float"), ColName("skin_color_2")] _skin_color_2,
      [DataType("float"), ColName("shirt1_color_0")] _shirt1_color_0,
      [DataType("float"), ColName("shirt1_color_1")] _shirt1_color_1,
      [DataType("float"), ColName("shirt1_color_2")] _shirt1_color_2,
      [DataType("float"), ColName("shirt2_color_0")] _shirt2_color_0,
      [DataType("float"), ColName("shirt2_color_1")] _shirt2_color_1,
      [DataType("float"), ColName("shirt2_color_2")] _shirt2_color_2,
      [ColName("pants1_color_0"), DataType("float")] _pants1_color_0,
      [ColName("pants1_color_1"), DataType("float")] _pants1_color_1,
      [DataType("float"), ColName("pants1_color_2")] _pants1_color_2,
      [ColName("pants2_color_0"), DataType("float")] _pants2_color_0,
      [ColName("pants2_color_1"), DataType("float")] _pants2_color_1,
      [DataType("float"), ColName("pants2_color_2")] _pants2_color_2,
      [ColName("shirt1_metal"), DataType("int")] _shirt1_metal,
      [DataType("int"), ColName("shirt2_metal")] _shirt2_metal,
      [ColName("pants1_metal"), DataType("int")] _pants1_metal,
      [DataType("int"), ColName("pants2_metal")] _pants2_metal,
      [ColName("body_weight0"), DataType("float")] _body_weight0,
      [DataType("float"), ColName("body_weight1")] _body_weight1,
      [ColName("body_weight2"), DataType("float")] _body_weight2,
      [DataType("float"), ColName("body_weight3")] _body_weight3,
      [DataType("float"), ColName("body_weight4")] _body_weight4,
      [DataType("float"), ColName("head_weight0")] _head_weight0,
      [DataType("float"), ColName("head_weight1")] _head_weight1,
      [DataType("float"), ColName("head_weight2")] _head_weight2,
      [DataType("float"), ColName("head_weight3")] _head_weight3,
      [ColName("head_weight4"), DataType("float")] _head_weight4,
    }

    public enum Table_starbase_rooms
    {
      [ColName("room_id"), DataType("bigint")] _room_id,
      [DataType("int"), ColName("type")] _type,
      [DataType("int"), ColName("style")] _style,
      [DataType("int"), ColName("fog_near")] _fog_near,
      [ColName("fog_far"), DataType("int")] _fog_far,
      [DataType("int"), ColName("fog_red")] _fog_red,
      [ColName("fog_green"), DataType("int")] _fog_green,
      [DataType("int"), ColName("fog_blue")] _fog_blue,
      [DataType("varchar"), ColName("description")] _description,
      [DataType("bigint"), ColName("starbase_id")] _starbase_id,
    }

    public enum Table_starbase_terminals
    {
      [ColName("terminal_id"), DataType("bigint")] _terminal_id,
      [ColName("location"), DataType("int")] _location,
      [DataType("int"), ColName("type")] _type,
      [ColName("attribute"), DataType("int")] _attribute,
      [DataType("varchar"), ColName("description")] _description,
      [DataType("bigint"), ColName("room_id")] _room_id,
      [ColName("terminal_index"), DataType("int")] _terminal_index,
    }

    public enum Table_starbase_vender_groups
    {
      [ColName("GroupID"), DataType("int")] _GroupID,
      [ColName("GroupName"), DataType("varchar")] _GroupName,
      [ColName("SellMultiplyer"), DataType("float")] _SellMultiplyer,
      [DataType("float"), ColName("BuyMultiplyer")] _BuyMultiplyer,
      [ColName("BuyOnlyList"), DataType("int")] _BuyOnlyList,
    }

    public enum Table_starbase_vender_inventory
    {
      [DataType("int"), ColName("id")] _id,
      [DataType("int"), ColName("groupid")] _groupid,
      [DataType("int"), ColName("itemid")] _itemid,
      [DataType("int"), ColName("sell_price")] _sell_price,
      [DataType("int"), ColName("buy_price")] _buy_price,
      [ColName("quanity"), DataType("int")] _quanity,
    }

    public enum Table_starbase_vendors
    {
      [ColName("vendor_id"), DataType("bigint")] _vendor_id,
      [DataType("int"), ColName("level")] _level,
      [ColName("booth_type"), DataType("int")] _booth_type,
      [DataType("int"), ColName("groupid")] _groupid,
    }

    public enum Table_systems
    {
      [DataType("int"), ColName("system_id")] _system_id,
      [DataType("text"), ColName("name")] _name,
      [ColName("galaxy_x"), DataType("float")] _galaxy_x,
      [ColName("galaxy_y"), DataType("float")] _galaxy_y,
      [DataType("float"), ColName("galaxy_z")] _galaxy_z,
      [DataType("float"), ColName("color_r")] _color_r,
      [ColName("color_g"), DataType("float")] _color_g,
      [DataType("float"), ColName("color_b")] _color_b,
      [ColName("notes"), DataType("text")] _notes,
    }

    public enum Table_table_changes
    {
      [DataType("int"), ColName("id")] _id,
      [ColName("tablename"), DataType("varchar")] _tablename,
      [ColName("modification"), DataType("varchar")] _modification,
      [ColName("username"), DataType("varchar")] _username,
      [DataType("varchar"), ColName("modified")] _modified,
      [ColName("new_values"), DataType("varchar")] _new_values,
      [DataType("varchar"), ColName("old_values")] _old_values,
    }

    public enum Table_versions
    {
      [DataType("varchar"), ColName("EName")] _EName,
      [DataType("varchar"), ColName("Version")] _Version,
    }

    public enum Enum_item_type
    {
      _Systems,
      _Weapon,
      _Shields,
      _Sensor,
      _Ejector,
      _Turret,
      _Engine,
      _Reactor,
      _Controler,
      _Robot,
      _Ammo,
      _Devices,
      _System,
      _Base,
      _Beam_Weapon,
      _Missile_Launcher,
      _Projectile_Weapon,
      _Countermeasure,
      _Over_Rides,
    }
  }
}
