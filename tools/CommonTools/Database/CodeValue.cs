﻿// Decompiled with JetBrains decompiler
// Type: CommonTools.Database.CodeValue
// Assembly: CommonTools, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 665008E1-3954-4D3F-8606-DD931B438B82
// Assembly location: D:\Server\eab\Tools\CommonTools.DLL

namespace CommonTools.Database
{
  public class CodeValue
  {
    public int code;
    public string value;

    public CodeValue()
    {
      this.code = 0;
      this.value = "";
    }

    public CodeValue(int code)
    {
      this.code = code;
      this.value = "";
    }

    public CodeValue(int code, string value)
    {
      this.code = code;
      this.value = value;
    }

    public static CodeValue Formatted(int code, string value)
    {
      string str = value + " (" + code.ToString() + ")";
      return new CodeValue(code, str);
    }

    public static CodeValue FormattedLeft(int code, string value)
    {
      string str = "(" + code.ToString() + ") " + value;
      return new CodeValue(code, str);
    }

    public override string ToString()
    {
      return this.value;
    }

    public override bool Equals(object obj)
    {
      if (obj.GetType() != typeof (CodeValue))
        return false;
      return this.code.Equals(((CodeValue) obj).code);
    }

    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
  }
}
