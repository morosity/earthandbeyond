﻿// Decompiled with JetBrains decompiler
// Type: CommonTools.Database.DB
// Assembly: CommonTools, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 665008E1-3954-4D3F-8606-DD931B438B82
// Assembly location: D:\Server\eab\Tools\CommonTools.DLL

using CommonTools.Gui;
using MySql.Data.MySqlClient;
using Singleton;
using System;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace CommonTools.Database
{
  public class DB : Get<DB>
  {
    private bool m_showExecutionTime = false;
    private MySqlConnection m_mySqlConnection = (MySqlConnection) null;
    public const string DATABASE_NAME = "net7";
    public const string SELECT = "SELECT ";
    public const string FROM = " FROM ";
    public const string WHERE = " WHERE ";
    public const string EQUALS = " = ";
    public const string ORDER_BY = " ORDER BY ";
    public const string LIMIT = " LIMIT ";
    public const string LIMIT1 = " LIMIT 1";
    public const string QueryParameterCharacter = "?";
    private MySqlTransaction m_mySqlTransaction;

    public MySqlConnection openConnection()
    {
      if (this.m_mySqlConnection == null)
        this.m_mySqlConnection = new MySqlConnection(LoginData.ConnStr("net7"));
      if (this.m_mySqlConnection.State.ToString().Equals("Closed"))
        this.m_mySqlConnection.Open();
      return this.m_mySqlConnection;
    }

    public void closeConnection()
    {
      this.m_mySqlConnection.Close();
    }

    public void startTransaction()
    {
      this.openConnection();
      this.m_mySqlTransaction = this.m_mySqlConnection.BeginTransaction();
    }

    public void commitTransaction()
    {
      this.m_mySqlTransaction.Commit();
      this.m_mySqlTransaction = (MySqlTransaction) null;
    }

    public void rollbackTransaction()
    {
      this.m_mySqlTransaction.Rollback();
      this.m_mySqlTransaction = (MySqlTransaction) null;
    }

    public DataTable executeQuery(string query, string[] parameter, string[] value)
    {
      DataTable dataTable = (DataTable) null;
      try
      {
        dataTable = new DataTable();
        this.openConnection();
        MySqlDataAdapter mySqlDataAdapter = new MySqlDataAdapter(query, this.m_mySqlConnection);
        mySqlDataAdapter.MissingSchemaAction = MissingSchemaAction.AddWithKey;
        if (parameter != null && parameter.Length != 0)
        {
          for (int index = 0; index < parameter.Length; ++index)
            mySqlDataAdapter.SelectCommand.Parameters.Add(new MySqlParameter(parameter[index], (object) value[index]));
        }
        if (this.m_mySqlTransaction != null)
          mySqlDataAdapter.SelectCommand.Transaction = this.m_mySqlTransaction;
        DateTime now = DateTime.Now;
        mySqlDataAdapter.Fill(dataTable);
        if (this.m_showExecutionTime)
        {
          TimeSpan timeSpan = DateTime.Now - now;
          Console.WriteLine(query + ": {0} milliseconds, {1} rows.", (object) timeSpan.TotalMilliseconds, (object) dataTable.Rows.Count);
        }
      }
      catch (Exception ex)
      {
        string str1 = "";
        if (value != null)
        {
          foreach (string str2 in value)
          {
            if (str1.Length != 0)
              str1 += ", ";
            str1 += str2;
          }
        }
        int num = (int) MessageBox.Show(ex.Message + "\n\n" + ex.StackTrace + "\n\n" + query + "\n" + str1 + "\n\n" + query, "Error within DB.executeQuery()");
      }
      finally
      {
        if (this.m_mySqlConnection == null)
          ;
      }
      return dataTable;
    }

    public int executeCommand(string query, string[] parameter, string[] value)
    {
      int num1 = 0;
      try
      {
        this.openConnection();
        MySqlCommand mySqlCommand = new MySqlCommand(query, this.m_mySqlConnection);
        if (parameter != null && parameter.Length != 0)
        {
          for (int index = 0; index < parameter.Length; ++index)
            mySqlCommand.Parameters.Add(new MySqlParameter(parameter[index], (object) value[index]));
        }
        if (this.m_mySqlTransaction != null)
          mySqlCommand.Transaction = this.m_mySqlTransaction;
        DateTime now = DateTime.Now;
        num1 = mySqlCommand.ExecuteNonQuery();
        if (this.m_showExecutionTime)
        {
          TimeSpan timeSpan = DateTime.Now - now;
          Console.WriteLine(query + ": {0} milliseconds, {1} rows.", (object) timeSpan.TotalMilliseconds, (object) num1);
        }
      }
      catch (Exception ex)
      {
        string str1 = "";
        if (value != null)
        {
          foreach (string str2 in value)
          {
            if (str1.Length != 0)
              str1 += ", ";
            str1 += str2;
          }
        }
        int num2 = (int) MessageBox.Show(ex.Message + "\n\n" + ex.StackTrace + "\n\n" + query + "\n" + str1 + "\n\n" + query, "Error within DB.executeCommand()");
      }
      finally
      {
        if (this.m_mySqlConnection == null)
          ;
      }
      return num1;
    }

    public void showExecutionTime(bool show)
    {
      this.m_showExecutionTime = show;
    }

    public string createSelect(Enum[] field, Net7.Tables table, Enum idField, string value, int queryCount)
    {
      string str = "";
      foreach (Enum enumValue in field)
        str = (str.Length != 0 ? str + "," : "SELECT ") + ColumnData.GetName(enumValue);
      return str + " FROM " + table.ToString() + " WHERE " + ColumnData.GetName(idField) + " = ?" + idField.ToString() + queryCount.ToString() + ";";
    }

    public DataTable select(Enum[] field, Net7.Tables table, Enum idField, string value)
    {
      return Get<DB>.Instance.executeQuery(this.createSelect(field, table, idField, value, 0), new string[1]
      {
        idField.ToString() + "0"
      }, new string[1]{ value });
    }

    public string getVersion(string toolName)
    {
      string str = "ToolName";
      DataTable dataTable = Get<DB>.Instance.executeQuery("SELECT " + ColumnData.GetName((Enum) Net7.Table_versions._Version) + " FROM " + Net7.Tables.versions.ToString() + " WHERE " + ColumnData.GetName((Enum) Net7.Table_versions._EName) + " = ?" + str, new string[1]
      {
        str
      }, new string[1]{ toolName });
      return dataTable.Rows.Count == 0 ? "" : ColumnData.GetString(dataTable.Rows[0], (Enum) Net7.Table_versions._Version);
    }

    public void setVersion(string toolName, string version)
    {
      string str1 = "ToolName";
      string str2 = "ToolVersion";
      if (Get<DB>.Instance.executeCommand("UPDATE " + (object) Net7.Tables.versions + " SET " + ColumnData.GetName((Enum) Net7.Table_versions._Version) + " = ?" + str2 + " WHERE " + ColumnData.GetName((Enum) Net7.Table_versions._EName) + " = ?" + str1, new string[2]
      {
        str2,
        str1
      }, new string[2]{ version, toolName }) != 0)
        return;
      if (Get<DB>.Instance.executeCommand("INSERT INTO " + (object) Net7.Tables.versions + " (" + ColumnData.GetName((Enum) Net7.Table_versions._EName) + "," + ColumnData.GetName((Enum) Net7.Table_versions._Version) + ") VALUES (?" + str1 + ",?" + str2 + ")", new string[2]
      {
        str2,
        str1
      }, new string[2]{ version, toolName }) == 0)
      {
        int num = (int) MessageBox.Show("Unable to update/create the version information into the " + (object) Net7.Tables.versions + " table");
      }
    }

    public void importValues(Net7.Tables table, string valuesFile)
    {
      StreamReader streamReader = new StreamReader(valuesFile);
      while (!streamReader.EndOfStream)
      {
        string str = streamReader.ReadLine();
        if (Get<DB>.Instance.executeCommand("INSERT INTO " + (object) table + " VALUES (" + str + ")", (string[]) null, (string[]) null) == 0)
        {
          int num = (int) MessageBox.Show("Error inserting the following row:\n" + str);
        }
      }
      streamReader.Close();
    }

    public void makeDatabaseVariables()
    {
      DataTable dataTable = this.executeQuery("SELECT DISTINCT table_name FROM information_schema.columns WHERE table_schema = 'net7'", (string[]) null, (string[]) null);
      StreamWriter text = new FileInfo("..\\..\\..\\..\\CommonTools\\Database\\net7.cs").CreateText();
      text.WriteLine("// This file was automatically generated by Database.makeDatabaseVariables() on " + DateTime.Now.Year.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Day.ToString() + " " + DateTime.Now.Hour.ToString() + ":" + DateTime.Now.Minute.ToString() + ":" + DateTime.Now.Second.ToString());
      text.WriteLine("namespace CommonTools.Database");
      text.WriteLine("{");
      text.WriteLine("    public static class net7");
      text.WriteLine("    {");
      string str1 = "        public enum Tables { ";
      string columnAlignedPosition = new string(' ', str1.Length);
      text.Write(str1);
      for (int index = 0; index < dataTable.Rows.Count; ++index)
      {
        string str2 = dataTable.Rows[index]["table_name"].ToString();
        if (index != dataTable.Rows.Count - 1)
          str2 += ", ";
        text.Write(str2);
        if (index != 0 && index % 5 == 0)
        {
          text.WriteLine("");
          text.Write(columnAlignedPosition);
        }
      }
      text.WriteLine(" };");
      text.WriteLine("");
      string query1 = "SELECT table_name, column_name, data_type FROM information_schema.columns WHERE table_schema = 'net7' ORDER BY table_name, ordinal_position";
      this.makeDatabaseEnum(text, query1, columnAlignedPosition, false, true);
      string query2 = "SELECT 'item_type' as 'table_name', name as 'column_name', id as 'data_type' FROM " + Net7.Tables.item_type.ToString() + " ORDER BY " + ColumnData.GetName((Enum) Net7.Table_item_type._id);
      this.makeDatabaseEnum(text, query2, columnAlignedPosition, true, true);
      text.WriteLine("    }");
      text.WriteLine("}");
      text.Close();
    }

    public void makeDatabaseEnum(StreamWriter streamWriter, string query, string columnAlignedPosition, bool tableContent, bool forCSharp)
    {
      string str1 = tableContent ? "Enum_" : "Table_";
      DataTable dataTable = this.executeQuery(query, (string[]) null, (string[]) null);
      string str2 = (string) null;
      for (int index = 0; index < dataTable.Rows.Count; ++index)
      {
        DataRow row = dataTable.Rows[index];
        string str3 = row["table_name"].ToString();
        string str4 = row["column_name"].ToString();
        string str5 = row["data_type"].ToString();
        string str6 = ("_" + str4).Replace(" ", "_");
        string str7;
        if (tableContent)
          str7 = str6 + " = " + str5;
        else
          str7 = "[ColName(\"" + str4 + "\")] [DataType(\"" + str5 + "\")] " + str6;
        if (str2 != null && str2.Equals(str3) || index == dataTable.Rows.Count - 1)
        {
          streamWriter.WriteLine(",");
          streamWriter.Write(columnAlignedPosition + str7);
        }
        if (str2 != null && (index == dataTable.Rows.Count - 1 || !str2.Equals(str3)))
        {
          streamWriter.WriteLine(" };");
          streamWriter.WriteLine("");
          str2 = (string) null;
        }
        if (str2 == null && index != dataTable.Rows.Count - 1)
        {
          str2 = str3;
          string str8 = "        " + (forCSharp ? "public " : "") + "enum " + str1 + str3 + " { ";
          columnAlignedPosition = new string(' ', str8.Length);
          streamWriter.Write(str8 + str7);
        }
      }
    }

    public static void printDataSetContents(DataSet dataSet, string title)
    {
      Console.WriteLine("\n" + title + ": " + (object) dataSet.Tables.Count + " tables in the dataset\n" + new string('=', 20));
      foreach (DataTable table in (InternalDataCollectionBase) dataSet.Tables)
      {
        Console.WriteLine(table.TableName + ": " + table.Rows.Count.ToString() + " rows");
        Console.WriteLine(new string('-', table.TableName.Length));
        int num = 10;
        foreach (DataRow row in (InternalDataCollectionBase) table.Rows)
        {
          if (--num != 0)
            Console.WriteLine(row[0].ToString() + "\t" + row[1].ToString());
          else
            break;
        }
        Console.WriteLine("");
      }
    }
  }
}
