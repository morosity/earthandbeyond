﻿// Decompiled with JetBrains decompiler
// Type: CommonTools.XmlAttributes
// Assembly: CommonTools, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 665008E1-3954-4D3F-8606-DD931B438B82
// Assembly location: D:\Server\eab\Tools\CommonTools.DLL

namespace CommonTools
{
  public static class XmlAttributes
  {
    public const string ID = "ID";
    public const string GAME_ID = "GameID";
    public const string BASE_ASSET = "BaseAsset";
    public const string TYPE = "Type";
    public const string HSV = "HSV";
    public const string RENDER_STATE_ID = "RenderStateID";
    public const string BITMASK = "Bitmask";
    public const string EFFECT_DESC_ID = "EffectDescID";
    public const string EFFECT_ID = "EffectID";
    public const string TIME_STAMP = "TimeStamp";
    public const string DURATION = "Duration";
    public const string SCALE = "Scale";
    public const string HSVSHIFT = "HSVShift";
    public const string REACTION = "Reaction";
    public const string IS_ATTACKING = "IsAttacking";
    public const string POSITION = "Position";
    public const string ORIENTATION = "Orientation";
    public const string VELOCITY = "Velocity";
    public const string MOVEMENT_ID = "MovementID";
    public const string CURRENT_SPEED = "CurrentSpeed";
    public const string SET_SPEED = "SetSpeed";
    public const string ACCELERATION = "Acceleration";
    public const string ROT_Y = "RotY";
    public const string DESIRED_Y = "DesiredY";
    public const string ROT_Z = "RotZ";
    public const string DESIRED_Z = "DesiredZ";
    public const string IMPARTED_VELOCITY = "ImpartedVelocity";
    public const string IMPARTED_SPIN = "ImpartedSpin";
    public const string IMPARTED_ROLL = "ImpartedRoll";
    public const string IMPARTED_PITCH = "ImpartedPitch";
    public const string UPDATE_PERIOD = "UpdatePeriod";
    public const string ORBIT_ID = "OrbitID";
    public const string ORBIT_DIST = "OrbitDist";
    public const string ORBIT_ANGLE = "OrbitAngle";
    public const string ORBIT_RATE = "OrbitRate";
    public const string ROTATE_ANGLE = "RotateAngle";
    public const string ROTATE_RATE = "RotateRate";
    public const string TILT_ANGLE = "TiltAngle";
    public const string IMPARTED_DECAY = "ImpartedDecay";
    public const string TRACTOR_SPEED = "TractorSpeed";
    public const string TRACTOR_ID = "TractorID";
    public const string TRACTOR_EFFECT_ID = "TractorEffectID";
    public const string SIGNATURE = "Signature";
    public const string PLAYER_HAS_VISITED = "PlayerHasVisited";
    public const string NAV_TYPE = "NavType";
    public const string IS_HUGE = "IsHuge";
    public const string IS_CAP_SHIP = "IsCapShip";
    public const string APPEARS_IN_RADAR = "AppearsInRadar";
    public const string SOUND_TEXT = "Text";
    public const string ITEM_TEMPLATE_ID = "ItemTemplateID";
    public const string CATEGORY = "Category";
    public const string SUBCATEGORY = "Subcategory";
    public const string ITEM_TYPE = "ItemType";
    public const string GAME_BASSET = "GameBasset";
    public const string ICON_BASE_ASSET = "IconBaseAsset";
    public const string TECH_LEVEL = "TechLevel";
    public const string COST = "Cost";
    public const string MAX_STACK = "MaxStack";
    public const string EFFECT_USAGE = "EffectUsage";
    public const string FLAGS = "Flags";
    public const string FLAG_1 = "flag1";
    public const string FLAG_2 = "flag2";
    public const string ITEM_FIELD_TYPE = "type";
    public const string UNKNOWN_1 = "unknown1";
    public const string UNKNOWN_2 = "unknown2";
    public const string UNKNOWN_3 = "unknown3";
    public const string UNKNOWN_4 = "unknown4";
    public const string NUMBER = "Number";
    public const string ACTIVATE = "Activate";
    public const string EQUIP = "Equip";
    public const string TARGET = "Target";
    public const string STARBASEID = "starbaseid";
    public const string BOOTH = "booth";
    public const string LOCATION = "location";
    public const string ROOM = "room";
    public const string X = "x";
    public const string Y = "y";
    public const string ZOOM = "zoom";
    public const string R = "r";
    public const string G = "g";
    public const string B = "b";
    public const string LEARN_LVL = "LearnLvl";
    public const string LVL_TYPE = "LvlType";
    public const string QUEST = "Quest";
    public const string MAX = "Max";
    public const string NAME = "Name";
    public const string FORFEITABLE = "forfeitable";
    public const string TIME = "time";
    public const string SECTORID = "SectorID";
    public const string TREENODEID = "Node";
    public const string DATA = "Data";
    public const string COUNT = "Count";
    public const string SOUND = "Sound";
    public const string SELECTIONID = "SelectionID";
    public const string POSITION_X = "PositionX";
    public const string POSITION_Y = "PositionY";
    public const string POSITION_Z = "PositionZ";
    public const string RANGE = "Range";
    public const string EXPLORE = "Explore";
    public const string COMBAT = "Combat";
    public const string TRADE = "Trade";
    public const string OVERALL = "Overall";
  }
}
