﻿// Decompiled with JetBrains decompiler
// Type: DataImport.Program
// Assembly: DataImport, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B88192A9-82FA-44FC-A06F-C66B9585339A
// Assembly location: D:\Server\eab\Tools\DataImport.exe

using CommonTools.Database;
using CommonTools.Gui;
using Singleton;
using System;
using System.Windows.Forms;

namespace DataImport
{
  internal static class Program
  {
    public static DataImport.DataImport m_dataImport = (DataImport.DataImport) null;

    [STAThread]
    private static void Main()
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      Program.ApplicationAction applicationAction = Program.ApplicationAction.Run;
      Login login = new Login();
      if (applicationAction.Equals((object) Program.ApplicationAction.UpdateVersion))
        login.updateVersion();
      int num1 = (int) login.ShowDialog();
      if (!login.isValid())
        return;
      switch (applicationAction)
      {
        case Program.ApplicationAction.GenerateDatabaseStructure:
          Get<DB>.Instance.makeDatabaseVariables();
          break;
        case Program.ApplicationAction.Run:
          Program.m_dataImport = new DataImport.DataImport();
          int num2 = (int) Program.m_dataImport.ShowDialog();
          break;
      }
    }

    private enum ApplicationAction
    {
      GenerateDatabaseStructure,
      UpdateVersion,
      Run,
    }
  }
}
