﻿// Decompiled with JetBrains decompiler
// Type: DataImport.DataImport
// Assembly: DataImport, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B88192A9-82FA-44FC-A06F-C66B9585339A
// Assembly location: D:\Server\eab\Tools\DataImport.exe

using CommonTools;
using CommonTools.Database;
using CommonTools.Gui;
using Singleton;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace DataImport
{
  public class DataImport : Form
  {
    private IContainer components = (IContainer) null;
    private ComboBox guiTableCbo;
    private Label guiTableLbl;
    private TextBox guiFileTxt;
    private Button guiFileBtn;
    private Label guiFileLbl;
    private Button guiImportBtn;
    private Button guiCloseBtn;

    public DataImport()
    {
      this.InitializeComponent();
      this.Text = this.Text + " " + LoginData.ApplicationVersion;
    }

    protected override void OnShown(EventArgs e)
    {
      base.OnShown(e);
      Enumeration.AddSortedByName<net7.Tables>(this.guiTableCbo);
      if (this.guiTableCbo.Items.Count == 0)
        return;
      this.guiTableCbo.SelectedIndex = 0;
    }

    private void onBrowse(object sender, EventArgs e)
    {
      OpenFileDialog openFileDialog = new OpenFileDialog();
      if (openFileDialog.ShowDialog() != DialogResult.OK)
        return;
      this.guiFileTxt.Text = openFileDialog.FileName;
    }

    private void onImport(object sender, EventArgs e)
    {
      Get<DB>.Instance.importValues((net7.Tables) this.guiTableCbo.SelectedItem, this.guiFileTxt.Text);
    }

    private void onClose(object sender, EventArgs e)
    {
      this.Close();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.guiTableCbo = new ComboBox();
      this.guiTableLbl = new Label();
      this.guiFileTxt = new TextBox();
      this.guiFileBtn = new Button();
      this.guiFileLbl = new Label();
      this.guiImportBtn = new Button();
      this.guiCloseBtn = new Button();
      this.SuspendLayout();
      this.guiTableCbo.DropDownStyle = ComboBoxStyle.DropDownList;
      this.guiTableCbo.FormattingEnabled = true;
      this.guiTableCbo.Location = new Point(51, 12);
      this.guiTableCbo.Name = "guiTableCbo";
      this.guiTableCbo.Size = new Size(342, 21);
      this.guiTableCbo.TabIndex = 0;
      this.guiTableLbl.AutoSize = true;
      this.guiTableLbl.Location = new Point(8, 15);
      this.guiTableLbl.Name = "guiTableLbl";
      this.guiTableLbl.Size = new Size(37, 13);
      this.guiTableLbl.TabIndex = 1;
      this.guiTableLbl.Text = "Table:";
      this.guiFileTxt.Location = new Point(51, 39);
      this.guiFileTxt.Name = "guiFileTxt";
      this.guiFileTxt.Size = new Size(342, 20);
      this.guiFileTxt.TabIndex = 2;
      this.guiFileBtn.Location = new Point(399, 37);
      this.guiFileBtn.Name = "guiFileBtn";
      this.guiFileBtn.Size = new Size(75, 23);
      this.guiFileBtn.TabIndex = 3;
      this.guiFileBtn.Text = "Browse";
      this.guiFileBtn.UseVisualStyleBackColor = true;
      this.guiFileBtn.Click += new EventHandler(this.onBrowse);
      this.guiFileLbl.AutoSize = true;
      this.guiFileLbl.Location = new Point(19, 42);
      this.guiFileLbl.Name = "guiFileLbl";
      this.guiFileLbl.Size = new Size(26, 13);
      this.guiFileLbl.TabIndex = 4;
      this.guiFileLbl.Text = "File:";
      this.guiImportBtn.Location = new Point(318, 248);
      this.guiImportBtn.Name = "guiImportBtn";
      this.guiImportBtn.Size = new Size(75, 23);
      this.guiImportBtn.TabIndex = 5;
      this.guiImportBtn.Text = "Import";
      this.guiImportBtn.UseVisualStyleBackColor = true;
      this.guiImportBtn.Click += new EventHandler(this.onImport);
      this.guiCloseBtn.Location = new Point(399, 248);
      this.guiCloseBtn.Name = "guiCloseBtn";
      this.guiCloseBtn.Size = new Size(75, 23);
      this.guiCloseBtn.TabIndex = 6;
      this.guiCloseBtn.Text = "Close";
      this.guiCloseBtn.UseVisualStyleBackColor = true;
      this.guiCloseBtn.Click += new EventHandler(this.onClose);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(486, 283);
      this.Controls.Add((Control) this.guiCloseBtn);
      this.Controls.Add((Control) this.guiImportBtn);
      this.Controls.Add((Control) this.guiFileLbl);
      this.Controls.Add((Control) this.guiFileBtn);
      this.Controls.Add((Control) this.guiFileTxt);
      this.Controls.Add((Control) this.guiTableLbl);
      this.Controls.Add((Control) this.guiTableCbo);
      this.Name = nameof (DataImport);
      this.StartPosition = FormStartPosition.CenterScreen;
      this.Text = "Data Import";
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
