﻿// Decompiled with JetBrains decompiler
// Type: DataImport.Properties.Resources
// Assembly: DataImport, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B88192A9-82FA-44FC-A06F-C66B9585339A
// Assembly location: D:\Server\eab\Tools\DataImport.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace DataImport.Properties
{
  [DebuggerNonUserCode]
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "2.0.0.0")]
  [CompilerGenerated]
  internal class Resources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal Resources()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (DataImport.Properties.Resources.resourceMan == null)
          DataImport.Properties.Resources.resourceMan = new ResourceManager("DataImport.Properties.Resources", typeof (DataImport.Properties.Resources).Assembly);
        return DataImport.Properties.Resources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return DataImport.Properties.Resources.resourceCulture;
      }
      set
      {
        DataImport.Properties.Resources.resourceCulture = value;
      }
    }
  }
}
