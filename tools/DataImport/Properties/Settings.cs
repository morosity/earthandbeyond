﻿// Decompiled with JetBrains decompiler
// Type: DataImport.Properties.Settings
// Assembly: DataImport, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: B88192A9-82FA-44FC-A06F-C66B9585339A
// Assembly location: D:\Server\eab\Tools\DataImport.exe

using System.CodeDom.Compiler;
using System.Configuration;
using System.Runtime.CompilerServices;

namespace DataImport.Properties
{
  [CompilerGenerated]
  [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "9.0.0.0")]
  internal sealed class Settings : ApplicationSettingsBase
  {
    private static Settings defaultInstance = (Settings) SettingsBase.Synchronized((SettingsBase) new Settings());

    public static Settings Default
    {
      get
      {
        Settings defaultInstance = Settings.defaultInstance;
        return defaultInstance;
      }
    }
  }
}
