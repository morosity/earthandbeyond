﻿// Decompiled with JetBrains decompiler
// Type: SmartAssembly.SmartExceptionsCore.UploadReportLoginService
// Assembly: NewItemEditor, Version=1.0.0.7, Culture=neutral, PublicKeyToken=9a6515a4c4e1fd2c
// MVID: 9D5F0B9A-C985-40A9-9939-E28C808E997E
// Assembly location: D:\Server\eab\Tools\Net7 Item Editor.exe

using \u0005;
using \u0007;
using System;
using System.Web.Services;
using System.Web.Services.Protocols;

namespace SmartAssembly.SmartExceptionsCore
{
  [WebServiceBinding(Name = "LoginServiceSoap", Namespace = "http://www.smartassembly.com/webservices/UploadReportLogin/")]
  internal sealed class UploadReportLoginService : SoapHttpClientProtocol
  {
    [NonSerialized]
    internal static \u0004 \u0001;

    public UploadReportLoginService()
    {
      \u001C\u0005.\u0084\u0004((object) this, UploadReportLoginService.\u0001(5174));
      \u0011\u0004.\u0086\u0004((object) this, 30000);
    }

    [SoapDocumentMethod("http://www.smartassembly.com/webservices/UploadReportLogin/GetServerURL")]
    public string GetServerURL(string licenseID)
    {
      return (string) \u009F\u0004.\u0089\u0004((object) this, UploadReportLoginService.\u0001(5259), new object[1]
      {
        (object) licenseID
      })[0];
    }

    static UploadReportLoginService()
    {
      \u0006.\u0080\u0002();
    }
  }
}
