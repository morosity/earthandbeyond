﻿// Decompiled with JetBrains decompiler
// Type: SmartAssembly.SmartExceptionsCore.ReportingService
// Assembly: NewItemEditor, Version=1.0.0.7, Culture=neutral, PublicKeyToken=9a6515a4c4e1fd2c
// MVID: 9D5F0B9A-C985-40A9-9939-E28C808E997E
// Assembly location: D:\Server\eab\Tools\Net7 Item Editor.exe

using \u0005;
using \u0007;
using System;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Xml.Serialization;

namespace SmartAssembly.SmartExceptionsCore
{
  [WebServiceBinding(Name = "ReportingServiceSoap", Namespace = "http://www.smartassembly.com/webservices/Reporting/")]
  internal sealed class ReportingService : SoapHttpClientProtocol
  {
    [NonSerialized]
    internal static \u0004 \u0001;

    public ReportingService(string serverURL)
    {
      \u001C\u0005.\u0084\u0004((object) this, \u0006\u0003.\u0083\u0005(serverURL, ReportingService.\u0001(5277)));
      \u0011\u0004.\u0086\u0004((object) this, 180000);
    }

    [SoapDocumentMethod("http://www.smartassembly.com/webservices/Reporting/UploadReport2")]
    public string UploadReport2(string licenseID, [XmlElement(DataType = "base64Binary")] byte[] data)
    {
      return (string) \u009F\u0004.\u0089\u0004((object) this, ReportingService.\u0001(5298), new object[2]
      {
        (object) licenseID,
        (object) data
      })[0];
    }

    static ReportingService()
    {
      \u0006.\u0080\u0002();
    }
  }
}
