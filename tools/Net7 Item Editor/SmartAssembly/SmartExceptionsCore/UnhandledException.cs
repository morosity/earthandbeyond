﻿// Decompiled with JetBrains decompiler
// Type: SmartAssembly.SmartExceptionsCore.UnhandledException
// Assembly: NewItemEditor, Version=1.0.0.7, Culture=neutral, PublicKeyToken=9a6515a4c4e1fd2c
// MVID: 9D5F0B9A-C985-40A9-9939-E28C808E997E
// Assembly location: D:\Server\eab\Tools\Net7 Item Editor.exe

using \u0005;
using \u0007;
using \u0080;
using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace SmartAssembly.SmartExceptionsCore
{
  [Serializable]
  public sealed class UnhandledException : Exception
  {
    [NonSerialized]
    internal static \u0004 \u0001;
    public int MethodID;
    public object[] Objects;
    public int ILOffset;
    private Exception previousException;

    internal Exception \u0013()
    {
      return this.previousException;
    }

    public static Exception \u0007\u0003([In] Exception obj0)
    {
      return UnhandledException.\u0017\u0003(obj0, new object[0]);
    }

    public static Exception \u0008\u0003([In] Exception obj0, [In] object obj1)
    {
      return UnhandledException.\u0017\u0003(obj0, new object[1]
      {
        obj1
      });
    }

    public static Exception \u000E\u0003([In] Exception obj0, [In] object obj1, [In] object obj2)
    {
      return UnhandledException.\u0017\u0003(obj0, new object[2]
      {
        obj1,
        obj2
      });
    }

    public static Exception \u000F\u0003([In] Exception obj0, [In] object obj1, [In] object obj2, [In] object obj3)
    {
      return UnhandledException.\u0017\u0003(obj0, new object[3]
      {
        obj1,
        obj2,
        obj3
      });
    }

    public static Exception \u0010\u0003([In] Exception obj0, [In] object obj1, [In] object obj2, [In] object obj3, [In] object obj4)
    {
      return UnhandledException.\u0017\u0003(obj0, new object[4]
      {
        obj1,
        obj2,
        obj3,
        obj4
      });
    }

    public static Exception \u0011\u0003([In] Exception obj0, [In] object obj1, [In] object obj2, [In] object obj3, [In] object obj4, [In] object obj5)
    {
      return UnhandledException.\u0017\u0003(obj0, new object[5]
      {
        obj1,
        obj2,
        obj3,
        obj4,
        obj5
      });
    }

    public static Exception \u0012\u0003([In] Exception obj0, [In] object obj1, [In] object obj2, [In] object obj3, [In] object obj4, [In] object obj5, [In] object obj6)
    {
      return UnhandledException.\u0017\u0003(obj0, new object[6]
      {
        obj1,
        obj2,
        obj3,
        obj4,
        obj5,
        obj6
      });
    }

    public static Exception \u0013\u0003([In] Exception obj0, [In] object obj1, [In] object obj2, [In] object obj3, [In] object obj4, [In] object obj5, [In] object obj6, [In] object obj7)
    {
      return UnhandledException.\u0017\u0003(obj0, new object[7]
      {
        obj1,
        obj2,
        obj3,
        obj4,
        obj5,
        obj6,
        obj7
      });
    }

    public static Exception \u0014\u0003([In] Exception obj0, [In] object obj1, [In] object obj2, [In] object obj3, [In] object obj4, [In] object obj5, [In] object obj6, [In] object obj7, [In] object obj8)
    {
      return UnhandledException.\u0017\u0003(obj0, new object[8]
      {
        obj1,
        obj2,
        obj3,
        obj4,
        obj5,
        obj6,
        obj7,
        obj8
      });
    }

    public static Exception \u0015\u0003([In] Exception obj0, [In] object obj1, [In] object obj2, [In] object obj3, [In] object obj4, [In] object obj5, [In] object obj6, [In] object obj7, [In] object obj8, [In] object obj9)
    {
      return UnhandledException.\u0017\u0003(obj0, new object[9]
      {
        obj1,
        obj2,
        obj3,
        obj4,
        obj5,
        obj6,
        obj7,
        obj8,
        obj9
      });
    }

    public static Exception \u0016\u0003([In] Exception obj0, [In] object obj1, [In] object obj2, [In] object obj3, [In] object obj4, [In] object obj5, [In] object obj6, [In] object obj7, [In] object obj8, [In] object obj9, [In] object obj10)
    {
      return UnhandledException.\u0017\u0003(obj0, new object[10]
      {
        obj1,
        obj2,
        obj3,
        obj4,
        obj5,
        obj6,
        obj7,
        obj8,
        obj9,
        obj10
      });
    }

    public static Exception \u0017\u0003([In] Exception obj0, [In] object[] obj1)
    {
      if (\u0084.\u0002)
        return (Exception) null;
      int num1 = -1;
      int num2 = -1;
      try
      {
        StackTrace stackTrace = new StackTrace(obj0);
        if (\u0083\u0006.\u007E\u0001\u0008((object) stackTrace) > 0)
        {
          StackFrame stackFrame = \u0014\u0005.\u007E\u0002\u0008((object) stackTrace, \u0083\u0006.\u007E\u0001\u0008((object) stackTrace) - 1);
          num2 = (\u0083\u0006.\u007E\u0096\u0006((object) \u001B\u0005.\u007E\u0003\u0008((object) stackFrame)) & 16777215) - 1;
          num1 = \u0083\u0006.\u007E\u0004\u0008((object) stackFrame);
        }
      }
      catch
      {
      }
      UnhandledException unhandledException = new UnhandledException(num2, obj1, num1, obj0);
      if (obj0 is UnhandledException)
      {
        Exception exception = (obj0 as UnhandledException).\u0013();
        if (exception != null)
          obj0 = exception;
      }
      Exception exception1 = obj0;
      while (\u008B\u0003.\u007E\u0091\u0005((object) exception1) != null)
        exception1 = \u008B\u0003.\u007E\u0091\u0005((object) exception1);
      try
      {
        // ISSUE: type reference
        FieldInfo fieldInfo = \u0098\u0004.\u007E\u0002\u0007((object) \u001F\u0002.\u009A\u0006(__typeref (Exception)), UnhandledException.\u0001(5622), BindingFlags.Instance | BindingFlags.NonPublic);
        \u008C\u0005.\u007E\u0088\u0008((object) fieldInfo, (object) exception1, (object) unhandledException);
      }
      catch
      {
      }
      return obj0;
    }

    public override void GetObjectData([In] SerializationInfo obj0, [In] StreamingContext obj1)
    {
      \u0093\u0004.\u0093\u0005((object) this, obj0, obj1);
      // ISSUE: type reference
      \u001E\u0004.\u007E\u008E\u0008((object) obj0, UnhandledException.\u0001(5643), (object) this.MethodID, \u001F\u0002.\u009A\u0006(__typeref (int)));
      // ISSUE: type reference
      \u001E\u0004.\u007E\u008E\u0008((object) obj0, UnhandledException.\u0001(5680), (object) this.ILOffset, \u001F\u0002.\u009A\u0006(__typeref (int)));
      // ISSUE: type reference
      \u001E\u0004.\u007E\u008E\u0008((object) obj0, UnhandledException.\u0001(5717), (object) this.previousException, \u001F\u0002.\u009A\u0006(__typeref (Exception)));
      int num = this.Objects == null ? 0 : this.Objects.Length;
      // ISSUE: type reference
      \u001E\u0004.\u007E\u008E\u0008((object) obj0, UnhandledException.\u0001(5766), (object) num, \u001F\u0002.\u009A\u0006(__typeref (int)));
      for (int index = 0; index < num; ++index)
      {
        // ISSUE: type reference
        \u001E\u0004.\u007E\u008E\u0008((object) obj0, \u0019\u0006.\u0080\u0005(UnhandledException.\u0001(5811), (object) index), this.Objects[index], \u001F\u0002.\u009A\u0006(__typeref (object)));
      }
    }

    internal UnhandledException([In] SerializationInfo obj0, [In] StreamingContext obj1)
      : base(obj0, obj1)
    {
      this.MethodID = \u0011\u0003.\u007E\u0090\u0008((object) obj0, UnhandledException.\u0001(5643));
      this.ILOffset = \u0011\u0003.\u007E\u0090\u0008((object) obj0, UnhandledException.\u0001(5680));
      // ISSUE: type reference
      this.previousException = (Exception) \u0007\u0006.\u007E\u008F\u0008((object) obj0, UnhandledException.\u0001(5717), \u001F\u0002.\u009A\u0006(__typeref (Exception)));
      int length = \u0011\u0003.\u007E\u0090\u0008((object) obj0, UnhandledException.\u0001(5766));
      this.Objects = new object[length];
      for (int index = 0; index < length; ++index)
      {
        // ISSUE: type reference
        this.Objects[index] = \u0007\u0006.\u007E\u008F\u0008((object) obj0, \u0019\u0006.\u0080\u0005(UnhandledException.\u0001(5811), (object) index), \u001F\u0002.\u009A\u0006(__typeref (object)));
      }
    }

    internal UnhandledException([In] int obj0, [In] object[] obj1, [In] int obj2, [In] Exception obj3)
      : base(string.Format(UnhandledException.\u0001(5856), (object) obj0, (object) obj2))
    {
      this.MethodID = obj0;
      this.Objects = obj1;
      this.ILOffset = obj2;
      this.previousException = obj3;
    }

    static UnhandledException()
    {
      \u0006.\u0080\u0002();
    }
  }
}
