﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: NewItemEditor, Version=1.0.0.7, Culture=neutral, PublicKeyToken=9a6515a4c4e1fd2c
// MVID: 9D5F0B9A-C985-40A9-9939-E28C808E997E
// Assembly location: D:\Server\eab\Tools\Net7 Item Editor.exe

using \u0003;
using SmartAssembly.SmartExceptionsCore;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace \u0003
{
  internal sealed class \u0002
  {
    private static \u0002 \u0001;
    private long \u0002;

    [DllImport("kernel32", EntryPoint = "SetProcessWorkingSetSize")]
    private static extern int \u0013(IntPtr process, int minimumWorkingSetSize, int maximumWorkingSetSize);

    private void \u0013()
    {
      Process currentProcess;
      try
      {
        try
        {
          currentProcess = Process.GetCurrentProcess();
          try
          {
            \u0002.\u0013(currentProcess.Handle, -1, -1);
          }
          finally
          {
            if (currentProcess != null)
              currentProcess.Dispose();
          }
        }
        catch
        {
        }
      }
      catch (Exception ex)
      {
        Process process = currentProcess;
        throw UnhandledException.\u000E\u0003(ex, (object) process, (object) this);
      }
    }

    private void \u0013(object sender, EventArgs e)
    {
      DateTime now;
      long ticks;
      try
      {
        try
        {
          now = DateTime.Now;
          ticks = now.Ticks;
          if (ticks - this.\u0002 <= 10000000L)
            return;
          this.\u0002 = ticks;
          this.\u0013();
        }
        catch
        {
        }
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<long> local1 = (ValueType) ticks;
        // ISSUE: variable of a boxed type
        __Boxed<DateTime> local2 = (ValueType) now;
        object obj = sender;
        EventArgs eventArgs = e;
        throw UnhandledException.\u0011\u0003(ex, (object) local1, (object) local2, (object) this, obj, (object) eventArgs);
      }
    }

    private \u0002()
    {
      DateTime now = DateTime.Now;
      this.\u0002 = now.Ticks;
      // ISSUE: explicit constructor call
      base.\u002Ector();
      try
      {
        // ISSUE: method pointer
        Application.Idle += new EventHandler((object) this, __methodptr(\u0013));
        this.\u0013();
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<DateTime> local = (ValueType) now;
        throw UnhandledException.\u000E\u0003(ex, (object) local, (object) this);
      }
    }

    public static void \u007F\u0002()
    {
      try
      {
        try
        {
          if (Environment.OSVersion.Platform != PlatformID.Win32NT)
            return;
          \u0002.\u0001 = new \u0002();
        }
        catch
        {
        }
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0007\u0003(ex);
      }
    }
  }
}
