﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: NewItemEditor, Version=1.0.0.7, Culture=neutral, PublicKeyToken=9a6515a4c4e1fd2c
// MVID: 9D5F0B9A-C985-40A9-9939-E28C808E997E
// Assembly location: D:\Server\eab\Tools\Net7 Item Editor.exe

using \u0005;
using \u0007;
using \u000E\u0006;
using MySql.Data.MySqlClient;
using SmartAssembly.SmartExceptionsCore;
using System;
using System.Data;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace \u000E\u0006
{
  internal static class \u0017\u0002
  {
    [NonSerialized]
    internal static \u0004 \u0001;
    private static MySqlConnection \u0001;
    private static Timer \u0002;
    private static string \u0003;
    private static string \u0004;
    private static string \u0005;
    private static int \u0006;
    private static bool \u0007;

    [SpecialName]
    public static string \u0080\u0005()
    {
      try
      {
        return \u0017\u0002.\u0003;
      }
      catch (Exception ex)
      {
        string str1;
        string str2 = str1;
        throw UnhandledException.\u0008\u0003(ex, (object) str2);
      }
    }

    [SpecialName]
    public static void \u0081\u0005([In] string obj0)
    {
      try
      {
        \u0017\u0002.\u0003 = obj0;
      }
      catch (Exception ex)
      {
        string str = obj0;
        throw UnhandledException.\u0008\u0003(ex, (object) str);
      }
    }

    [SpecialName]
    public static string \u0082\u0005()
    {
      try
      {
        return \u0017\u0002.\u0004;
      }
      catch (Exception ex)
      {
        string str1;
        string str2 = str1;
        throw UnhandledException.\u0008\u0003(ex, (object) str2);
      }
    }

    [SpecialName]
    public static void \u0083\u0005([In] string obj0)
    {
      try
      {
        \u0017\u0002.\u0004 = obj0;
      }
      catch (Exception ex)
      {
        string str = obj0;
        throw UnhandledException.\u0008\u0003(ex, (object) str);
      }
    }

    [SpecialName]
    public static string \u0084\u0005()
    {
      try
      {
        return \u0017\u0002.\u0005;
      }
      catch (Exception ex)
      {
        string str1;
        string str2 = str1;
        throw UnhandledException.\u0008\u0003(ex, (object) str2);
      }
    }

    [SpecialName]
    public static void \u0086\u0005([In] string obj0)
    {
      try
      {
        \u0017\u0002.\u0005 = obj0;
      }
      catch (Exception ex)
      {
        string str = obj0;
        throw UnhandledException.\u0008\u0003(ex, (object) str);
      }
    }

    [SpecialName]
    public static int \u0087\u0005()
    {
      try
      {
        return \u0017\u0002.\u0006;
      }
      catch (Exception ex)
      {
        int num;
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) num;
        throw UnhandledException.\u0008\u0003(ex, (object) local);
      }
    }

    [SpecialName]
    public static void \u0088\u0005([In] int obj0)
    {
      try
      {
        \u0017\u0002.\u0006 = obj0;
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) obj0;
        throw UnhandledException.\u0008\u0003(ex, (object) local);
      }
    }

    public static string \u0089\u0005([In] string obj0)
    {
      string[] strArray1;
      try
      {
        strArray1 = new string[11]
        {
          \u0017\u0002.\u0001(8164),
          obj0,
          \u0017\u0002.\u0001(8241),
          \u0017\u0002.\u0003,
          \u0017\u0002.\u0001(8250),
          \u0017\u0002.\u0006.ToString(),
          \u0017\u0002.\u0001(8259),
          \u0017\u0002.\u0004,
          \u0017\u0002.\u0001(8276),
          \u0017\u0002.\u0005,
          \u0017\u0002.\u0001(8293)
        };
        return \u0004\u0004.\u0087\u0005(strArray1);
      }
      catch (Exception ex)
      {
        string str1;
        string str2 = str1;
        string[] strArray2 = strArray1;
        string str3 = obj0;
        throw UnhandledException.\u000F\u0003(ex, (object) str2, (object) strArray2, (object) str3);
      }
    }

    public static void \u008A\u0005()
    {
      try
      {
        \u0017\u0002.\u0001 = new MySqlConnection(\u0017\u0002.\u0089\u0005(\u0017\u0002.\u0001(8330)));
        \u001B\u0006.\u007E\u0083\u0012((object) \u0017\u0002.\u0001);
        \u0017\u0002.\u0007 = true;
        \u0017\u0002.\u0002 = new Timer();
        \u0011\u0004.\u007E\u0095\u0011((object) \u0017\u0002.\u0002, 120000);
        \u009C\u0006.\u007E\u0094\u0011((object) \u0017\u0002.\u0002, true);
        \u001D\u0005.\u007E\u0093\u0011((object) \u0017\u0002.\u0002, new EventHandler(\u0017\u0002.\u008B\u0005));
        \u001B\u0006.\u007E\u0096\u0011((object) \u0017\u0002.\u0002);
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0007\u0003(ex);
      }
    }

    private static void \u008B\u0005([In] object obj0, [In] EventArgs obj1)
    {
      try
      {
        \u0017\u0002.\u008C\u0005();
      }
      catch (Exception ex)
      {
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u000E\u0003(ex, obj, (object) eventArgs);
      }
    }

    public static void \u008C\u0005()
    {
      bool flag;
      try
      {
        flag = !\u0017\u0002.\u0007;
        if (flag)
          return;
        flag = \u0006\u0007.\u007E\u0001\u0003((object) \u0017\u0002.\u0001);
        if (!flag)
          \u0017\u0002.\u008A\u0005();
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<bool> local = (ValueType) flag;
        throw UnhandledException.\u0008\u0003(ex, (object) local);
      }
    }

    public static void \u008D\u0005([In] DataSet obj0)
    {
      int num1;
      int num2;
      bool flag;
      try
      {
        flag = obj0 != null;
        if (!flag)
          return;
        num1 = 0;
        while (true)
        {
          flag = num1 < \u0083\u0006.\u007E\u0010\u0012((object) \u0099\u0002.\u007E\u0017\u0012((object) obj0));
          if (flag)
          {
            num2 = 0;
            while (true)
            {
              flag = num2 < \u0083\u0006.\u007E\u0010\u0012((object) \u009D\u0004.\u007E\u001C\u0012((object) \u007F\u0006.\u007E\u001E\u0012((object) \u0099\u0002.\u007E\u0017\u0012((object) obj0), num1)));
              if (flag)
              {
                flag = !\u0088\u0003.\u007E\u0014\u0012((object) \u0095\u0006.\u007E\u0015\u0012((object) \u009D\u0004.\u007E\u001C\u0012((object) \u007F\u0006.\u007E\u001E\u0012((object) \u0099\u0002.\u007E\u0017\u0012((object) obj0), num1)), num2), DataRowVersion.Proposed);
                if (!flag)
                  \u001B\u0006.\u007E\u0013\u0012((object) \u0095\u0006.\u007E\u0015\u0012((object) \u009D\u0004.\u007E\u001C\u0012((object) \u007F\u0006.\u007E\u001E\u0012((object) \u0099\u0002.\u007E\u0017\u0012((object) obj0), num1)), num2));
                ++num2;
              }
              else
                break;
            }
            ++num1;
          }
          else
            break;
        }
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<int> local1 = (ValueType) num1;
        // ISSUE: variable of a boxed type
        __Boxed<int> local2 = (ValueType) num2;
        // ISSUE: variable of a boxed type
        __Boxed<bool> local3 = (ValueType) flag;
        DataSet dataSet = obj0;
        throw UnhandledException.\u0010\u0003(ex, (object) local1, (object) local2, (object) local3, (object) dataSet);
      }
    }

    public static void \u008E\u0005([In] DataTable obj0, [In] string obj1)
    {
      bool flag;
      MySqlDataAdapter adapter;
      MySqlCommandBuilder sqlCommandBuilder1;
      try
      {
        flag = !\u009E\u0002.\u0012\u0005(\u0013\u0007.\u007E\u0080\u0012((object) \u0017\u0002.\u0001), \u0017\u0002.\u0001(8330));
        if (!flag)
          \u001C\u0005.\u007E\u0082\u0012((object) \u0017\u0002.\u0001, \u0017\u0002.\u0001(8330));
        adapter = new MySqlDataAdapter(obj1, \u0017\u0002.\u0001);
        sqlCommandBuilder1 = new MySqlCommandBuilder(adapter);
        \u0089\u0004.\u007E\u007F\u0012((object) sqlCommandBuilder1, ConflictOption.OverwriteChanges);
        int num = \u0012\u0006.\u007E\u0088\u0012((object) adapter, obj0);
      }
      catch (Exception ex)
      {
        MySqlDataAdapter mySqlDataAdapter = adapter;
        MySqlCommandBuilder sqlCommandBuilder2 = sqlCommandBuilder1;
        // ISSUE: variable of a boxed type
        __Boxed<bool> local = (ValueType) flag;
        DataTable dataTable = obj0;
        string str = obj1;
        throw UnhandledException.\u0011\u0003(ex, (object) mySqlDataAdapter, (object) sqlCommandBuilder2, (object) local, (object) dataTable, (object) str);
      }
    }

    public static DataSet \u008F\u0005([In] string obj0, [In] string obj1, [In] string obj2)
    {
      bool flag;
      MySqlDataAdapter adapter;
      MySqlCommandBuilder sqlCommandBuilder1;
      DataSet dataSet1;
      try
      {
        dataSet1 = new DataSet();
        flag = !\u009E\u0002.\u0011\u0005(obj2, \u0017\u0002.\u0001(8339));
        if (!flag)
          obj2 = \u0017\u0002.\u0001(8340);
        flag = !\u009E\u0002.\u0012\u0005(\u0013\u0007.\u007E\u0080\u0012((object) \u0017\u0002.\u0001), obj1);
        if (!flag)
          \u001C\u0005.\u007E\u0082\u0012((object) \u0017\u0002.\u0001, obj1);
        adapter = new MySqlDataAdapter(obj0, \u0017\u0002.\u0001);
        sqlCommandBuilder1 = new MySqlCommandBuilder(adapter);
        DataTable[] dataTableArray = \u009A\u0002.\u007E\u0084\u0012((object) adapter, dataSet1, SchemaType.Source, obj2);
        int num = \u008F\u0003.\u007E\u0086\u0012((object) adapter, dataSet1, obj2);
        return dataSet1;
      }
      catch (Exception ex)
      {
        DataSet dataSet2 = dataSet1;
        MySqlDataAdapter mySqlDataAdapter = adapter;
        MySqlCommandBuilder sqlCommandBuilder2 = sqlCommandBuilder1;
        DataSet dataSet3;
        DataSet dataSet4 = dataSet3;
        // ISSUE: variable of a boxed type
        __Boxed<bool> local = (ValueType) flag;
        string str1 = obj0;
        string str2 = obj1;
        string str3 = obj2;
        throw UnhandledException.\u0014\u0003(ex, (object) dataSet2, (object) mySqlDataAdapter, (object) sqlCommandBuilder2, (object) dataSet4, (object) local, (object) str1, (object) str2, (object) str3);
      }
    }

    public static DataTable \u0090\u0005([In] string obj0)
    {
      bool flag;
      MySqlDataAdapter mySqlDataAdapter1;
      DataTable dataTable1;
      try
      {
        dataTable1 = new DataTable();
        flag = !\u009E\u0002.\u0012\u0005(\u0013\u0007.\u007E\u0080\u0012((object) \u0017\u0002.\u0001), \u0017\u0002.\u0001(8330));
        if (!flag)
          \u001C\u0005.\u007E\u0082\u0012((object) \u0017\u0002.\u0001, \u0017\u0002.\u0001(8330));
        mySqlDataAdapter1 = new MySqlDataAdapter(obj0, \u0017\u0002.\u0001);
        int num = \u0012\u0006.\u007E\u0087\u0012((object) mySqlDataAdapter1, dataTable1);
        return dataTable1;
      }
      catch (Exception ex)
      {
        DataTable dataTable2 = dataTable1;
        MySqlDataAdapter mySqlDataAdapter2 = mySqlDataAdapter1;
        DataTable dataTable3;
        DataTable dataTable4 = dataTable3;
        // ISSUE: variable of a boxed type
        __Boxed<bool> local = (ValueType) flag;
        string str = obj0;
        throw UnhandledException.\u0011\u0003(ex, (object) dataTable2, (object) mySqlDataAdapter2, (object) dataTable4, (object) local, (object) str);
      }
    }

    static \u0017\u0002()
    {
      \u0006.\u0080\u0002();
    }
  }
}
