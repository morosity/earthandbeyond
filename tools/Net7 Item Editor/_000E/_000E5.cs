﻿// Decompiled with JetBrains decompiler
// Type: .5
// Assembly: NewItemEditor, Version=1.0.0.7, Culture=neutral, PublicKeyToken=9a6515a4c4e1fd2c
// MVID: 9D5F0B9A-C985-40A9-9939-E28C808E997E
// Assembly location: D:\Server\eab\Tools\Net7 Item Editor.exe

using \u000E;
using \u0010;
using SmartAssembly.SmartExceptionsCore;
using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Threading;

namespace \u000E
{
  internal sealed class \u000E5
  {
    private static Assembly \u0001;
    private static string[] \u0002;

    internal static void \u0013()
    {
      try
      {
        try
        {
          AppDomain.CurrentDomain.ResourceResolve += new ResolveEventHandler(\u000E5.\u0013);
        }
        catch (Exception ex)
        {
        }
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0007\u0003(ex);
      }
    }

    internal static Assembly \u0013([In] object obj0, [In] ResolveEventArgs obj1)
    {
      string[] strArray1;
      string name;
      int index;
      try
      {
        if (\u000E5.\u0001 == null)
        {
          Monitor.Enter((object) (strArray1 = \u000E5.\u0002));
          try
          {
            \u000E5.\u0001 = Assembly.Load(\u0006.\u0082\u0002(166));
            if (\u000E5.\u0001 != null)
              \u000E5.\u0002 = \u000E5.\u0001.GetManifestResourceNames();
          }
          finally
          {
            Monitor.Exit((object) strArray1);
          }
        }
        name = obj1.Name;
        for (index = 0; index < \u000E5.\u0002.Length; ++index)
        {
          if (\u000E5.\u0002[index] == name)
            return \u000E5.\u0001;
        }
        return (Assembly) null;
      }
      catch (Exception ex)
      {
        string str = name;
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) index;
        string[] strArray2 = strArray1;
        object obj = obj0;
        ResolveEventArgs resolveEventArgs = obj1;
        throw UnhandledException.\u0011\u0003(ex, (object) str, (object) local, (object) strArray2, obj, (object) resolveEventArgs);
      }
    }

    static \u000E5()
    {
      try
      {
        \u000E5.\u0001 = (Assembly) null;
        \u000E5.\u0002 = new string[0];
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0007\u0003(ex);
      }
    }
  }
}
