﻿// Decompiled with JetBrains decompiler
// Type: .4
// Assembly: NewItemEditor, Version=1.0.0.7, Culture=neutral, PublicKeyToken=9a6515a4c4e1fd2c
// MVID: 9D5F0B9A-C985-40A9-9939-E28C808E997E
// Assembly location: D:\Server\eab\Tools\Net7 Item Editor.exe

using \u000E;
using \u0010;
using \u0013;
using \u001C;
using SmartAssembly.SmartExceptionsCore;
using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace \u000E
{
  internal sealed class \u000E4
  {
    private static Hashtable \u0001;

    [DllImport("kernel32", EntryPoint = "MoveFileEx")]
    private static extern bool \u0014([In] string obj0, [In] string obj1, [In] int obj2);

    [SpecialName]
    internal static bool \u0014()
    {
      string lower;
      try
      {
        bool flag;
        try
        {
          lower = Process.GetCurrentProcess().MainModule.ModuleName.ToLower();
          if (lower == \u0006.\u0082\u0002(263))
          {
            flag = true;
            goto label_6;
          }
          else if (lower == \u0006.\u0082\u0002(276))
          {
            flag = true;
            goto label_6;
          }
        }
        catch (Exception ex)
        {
        }
        return false;
label_6:
        return flag;
      }
      catch (Exception ex)
      {
        string str = lower;
        bool flag;
        // ISSUE: variable of a boxed type
        __Boxed<bool> local = (ValueType) flag;
        throw UnhandledException.\u000E\u0003(ex, (object) str, (object) local);
      }
    }

    internal static void \u0014()
    {
      char[] chArray1;
      string[] strArray;
      int index;
      string s;
      string str1;
      string name;
      int num;
      string str2;
      bool flag;
      Stream manifestResourceStream;
      int length;
      byte[] buffer;
      string path1;
      \u000E4.\u000E obj;
      string path2;
      FileStream fileStream;
      try
      {
        try
        {
          AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(\u000E4.\u0014);
          if (!Assembly.GetExecutingAssembly().GlobalAssemblyCache || !\u000E4.\u0014())
            return;
          string str3 = \u0006.\u0082\u0002(297);
          chArray1 = new char[1]{ ',' };
          char[] chArray2 = chArray1;
          strArray = str3.Split(chArray2);
          index = 0;
          while (index < strArray.Length - 1)
          {
            try
            {
              s = strArray[index];
              str1 = Encoding.UTF8.GetString(Convert.FromBase64String(s));
              name = strArray[index + 1];
              if (name.Length > 0)
              {
                if (name[0] == '[')
                {
                  num = name.IndexOf(']');
                  str2 = name.Substring(1, num - 1);
                  name = name.Substring(num + 1);
                  flag = str2.IndexOf('z') >= 0;
                  if (str2.IndexOf('f') >= 0)
                  {
                    manifestResourceStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(name);
                    if (manifestResourceStream != null)
                    {
                      length = (int) manifestResourceStream.Length;
                      buffer = new byte[length];
                      manifestResourceStream.Read(buffer, 0, length);
                      if (flag)
                        buffer = \u001B.\u0083\u0002(buffer);
                      try
                      {
                        path1 = string.Format(\u0006.\u0082\u0002(2523), (object) Path.GetTempPath(), (object) name);
                        Directory.CreateDirectory(path1);
                        obj = new \u000E4.\u000E(str1);
                        path2 = path1 + obj.\u0001 + \u0006.\u0082\u0002(2536);
                        if (!File.Exists(path2))
                        {
                          fileStream = File.OpenWrite(path2);
                          fileStream.Write(buffer, 0, buffer.Length);
                          fileStream.Close();
                        }
                        \u000E2.\u0014(path2);
                        try
                        {
                          File.Delete(path2);
                          Directory.Delete(path1);
                        }
                        catch
                        {
                        }
                      }
                      catch (Exception ex)
                      {
                      }
                    }
                  }
                }
              }
            }
            catch (Exception ex)
            {
            }
            index += 2;
          }
        }
        catch (Exception ex)
        {
        }
      }
      catch (Exception ex)
      {
        object[] objArray = new object[16]
        {
          (object) strArray,
          (object) index,
          (object) s,
          (object) str1,
          (object) name,
          (object) num,
          (object) str2,
          (object) flag,
          (object) manifestResourceStream,
          (object) length,
          (object) buffer,
          (object) path1,
          (object) obj,
          (object) path2,
          (object) fileStream,
          (object) chArray1
        };
        throw UnhandledException.\u0017\u0003(ex, objArray);
      }
    }

    internal static Assembly \u0014([In] object obj0, [In] ResolveEventArgs obj1)
    {
      \u000E4.\u000E obj;
      string s;
      string base64String;
      char[] chArray1;
      string[] strArray;
      string name;
      bool flag1;
      bool flag2;
      bool flag3;
      int index1;
      int index2;
      int num;
      string str1;
      Hashtable hashtable1;
      Assembly assembly1;
      Stream manifestResourceStream;
      int length;
      byte[] numArray;
      string path1;
      string path2;
      Assembly assembly2;
      FileStream fileStream1;
      Hashtable hashtable2;
      Assembly assembly3;
      string path3;
      string path4;
      FileStream fileStream2;
      Hashtable hashtable3;
      try
      {
        obj = new \u000E4.\u000E(obj1.Name);
        s = obj.\u0014(false);
        base64String = Convert.ToBase64String(Encoding.UTF8.GetBytes(s));
        string str2 = \u0006.\u0082\u0002(297);
        chArray1 = new char[1]{ ',' };
        char[] chArray2 = chArray1;
        strArray = str2.Split(chArray2);
        name = string.Empty;
        flag1 = false;
        flag2 = false;
        flag3 = false;
        index1 = 0;
        while (index1 < strArray.Length - 1)
        {
          if (strArray[index1] == base64String)
          {
            name = strArray[index1 + 1];
            break;
          }
          index1 += 2;
        }
        if (name.Length == 0 && obj.\u0004.Length == 0)
        {
          base64String = Convert.ToBase64String(Encoding.UTF8.GetBytes(obj.\u0001));
          index2 = 0;
          while (index2 < strArray.Length - 1)
          {
            if (strArray[index2] == base64String)
            {
              name = strArray[index2 + 1];
              break;
            }
            index2 += 2;
          }
        }
        if (name.Length > 0)
        {
          if (name[0] == '[')
          {
            num = name.IndexOf(']');
            str1 = name.Substring(1, num - 1);
            flag1 = str1.IndexOf('z') >= 0;
            flag2 = str1.IndexOf('g') >= 0;
            flag3 = str1.IndexOf('t') >= 0;
            name = name.Substring(num + 1);
          }
          Monitor.Enter((object) (hashtable1 = \u000E4.\u0001));
          try
          {
            if (\u000E4.\u0001.ContainsKey((object) name))
            {
              assembly1 = (Assembly) \u000E4.\u0001[(object) name];
              goto label_47;
            }
          }
          finally
          {
            Monitor.Exit((object) hashtable1);
          }
          manifestResourceStream = Assembly.GetExecutingAssembly().GetManifestResourceStream(name);
          if (manifestResourceStream != null)
          {
            length = (int) manifestResourceStream.Length;
            numArray = new byte[length];
            manifestResourceStream.Read(numArray, 0, length);
            if (flag1)
              numArray = \u001B.\u0083\u0002(numArray);
            if (flag2)
            {
              try
              {
                path1 = string.Format(\u0006.\u0082\u0002(2523), (object) Path.GetTempPath(), (object) name);
                Directory.CreateDirectory(path1);
                path2 = path1 + obj.\u0001 + \u0006.\u0082\u0002(2536);
                if (!File.Exists(path2))
                {
                  assembly2 = (Assembly) null;
                  fileStream1 = File.OpenWrite(path2);
                  fileStream1.Write(numArray, 0, numArray.Length);
                  fileStream1.Close();
                  if (\u000E2.\u0014(path2))
                    assembly2 = Assembly.Load(obj.\u0014(true));
                  File.Delete(path2);
                  Directory.Delete(path1);
                  if (assembly2 != null)
                  {
                    Monitor.Enter((object) (hashtable2 = \u000E4.\u0001));
                    try
                    {
                      if (\u000E4.\u0001.ContainsKey((object) name))
                        assembly2 = (Assembly) \u000E4.\u0001[(object) name];
                      else
                        \u000E4.\u0001.Add((object) name, (object) assembly2);
                    }
                    finally
                    {
                      Monitor.Exit((object) hashtable2);
                    }
                    assembly1 = assembly2;
                    goto label_47;
                  }
                }
              }
              catch
              {
              }
            }
            assembly3 = (Assembly) null;
            if (!flag3)
            {
              try
              {
                assembly3 = Assembly.Load(numArray);
              }
              catch (FileLoadException ex)
              {
                flag3 = true;
              }
              catch (BadImageFormatException ex)
              {
                flag3 = true;
              }
            }
            if (flag3)
            {
              try
              {
                path3 = string.Format(\u0006.\u0082\u0002(2523), (object) Path.GetTempPath(), (object) name);
                Directory.CreateDirectory(path3);
                path4 = path3 + obj.\u0001 + \u0006.\u0082\u0002(2536);
                if (!File.Exists(path4))
                {
                  fileStream2 = File.OpenWrite(path4);
                  fileStream2.Write(numArray, 0, numArray.Length);
                  fileStream2.Close();
                  \u000E4.\u0014(path4, (string) null, 4);
                  \u000E4.\u0014(path3, (string) null, 4);
                }
                assembly3 = Assembly.LoadFile(path4);
              }
              catch
              {
              }
            }
            Monitor.Enter((object) (hashtable3 = \u000E4.\u0001));
            try
            {
              \u000E4.\u0001.Add((object) name, (object) assembly3);
            }
            finally
            {
              Monitor.Exit((object) hashtable3);
            }
            return assembly3;
          }
          goto label_46;
label_47:
          return assembly1;
        }
label_46:
        return (Assembly) null;
      }
      catch (Exception ex)
      {
        object[] objArray = new object[30]
        {
          (object) obj,
          (object) s,
          (object) base64String,
          (object) strArray,
          (object) name,
          (object) flag1,
          (object) flag2,
          (object) flag3,
          (object) index1,
          (object) index2,
          (object) num,
          (object) str1,
          (object) manifestResourceStream,
          (object) length,
          (object) numArray,
          (object) path1,
          (object) path2,
          (object) assembly2,
          (object) fileStream1,
          (object) assembly3,
          (object) path3,
          (object) path4,
          (object) fileStream2,
          (object) assembly1,
          (object) chArray1,
          (object) hashtable1,
          (object) hashtable2,
          (object) hashtable3,
          obj0,
          (object) obj1
        };
        throw UnhandledException.\u0017\u0003(ex, objArray);
      }
    }

    public \u000E4()
    {
      try
      {
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0008\u0003(ex, (object) this);
      }
    }

    static \u000E4()
    {
      try
      {
        \u000E4.\u0001 = new Hashtable();
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0007\u0003(ex);
      }
    }

    internal struct \u000E
    {
      public string \u0001;
      public Version \u0002;
      public string \u0003;
      public string \u0004;

      public string \u0014([In] bool obj0)
      {
        StringBuilder stringBuilder1;
        try
        {
          stringBuilder1 = new StringBuilder();
          stringBuilder1.Append(this.\u0001);
          if (obj0)
          {
            stringBuilder1.Append(\u0006.\u0082\u0002(2545));
            stringBuilder1.Append((object) this.\u0002);
          }
          stringBuilder1.Append(\u0006.\u0082\u0002(2562));
          stringBuilder1.Append(this.\u0003.Length == 0 ? \u0006.\u0082\u0002(2579) : this.\u0003);
          stringBuilder1.Append(\u0006.\u0082\u0002(2592));
          stringBuilder1.Append(this.\u0004.Length == 0 ? \u0006.\u0082\u0002(2617) : this.\u0004);
          return stringBuilder1.ToString();
        }
        catch (Exception ex)
        {
          StringBuilder stringBuilder2 = stringBuilder1;
          // ISSUE: variable of a boxed type
          __Boxed<\u000E4.\u000E> local1 = (ValueType) this;
          // ISSUE: variable of a boxed type
          __Boxed<bool> local2 = (ValueType) obj0;
          throw UnhandledException.\u000F\u0003(ex, (object) stringBuilder2, (object) local1, (object) local2);
        }
      }

      public \u000E([In] string obj0)
      {
        char[] chArray1;
        string[] strArray1;
        int index;
        string str1;
        string str2;
        try
        {
          this.\u0002 = new Version();
          this.\u0003 = string.Empty;
          this.\u0004 = string.Empty;
          this.\u0001 = string.Empty;
          string str3 = obj0;
          chArray1 = new char[1]{ ',' };
          char[] chArray2 = chArray1;
          strArray1 = str3.Split(chArray2);
          for (index = 0; index < strArray1.Length; ++index)
          {
            str1 = strArray1[index];
            str2 = str1.Trim();
            if (str2.StartsWith(\u0006.\u0082\u0002(2626)))
              this.\u0002 = new Version(str2.Substring(8));
            else if (str2.StartsWith(\u0006.\u0082\u0002(2639)))
            {
              this.\u0003 = str2.Substring(8);
              if (this.\u0003 == \u0006.\u0082\u0002(2579))
                this.\u0003 = string.Empty;
            }
            else if (str2.StartsWith(\u0006.\u0082\u0002(2652)))
            {
              this.\u0004 = str2.Substring(15);
              if (this.\u0004 == \u0006.\u0082\u0002(2617))
                this.\u0004 = string.Empty;
            }
            else
              this.\u0001 = str2;
          }
        }
        catch (Exception ex)
        {
          string str3 = str1;
          string str4 = str2;
          char[] chArray2 = chArray1;
          string[] strArray2 = strArray1;
          // ISSUE: variable of a boxed type
          __Boxed<int> local1 = (ValueType) index;
          // ISSUE: variable of a boxed type
          __Boxed<\u000E4.\u000E> local2 = (ValueType) this;
          string str5 = obj0;
          throw UnhandledException.\u0013\u0003(ex, (object) str3, (object) str4, (object) chArray2, (object) strArray2, (object) local1, (object) local2, (object) str5);
        }
      }
    }
  }
}
