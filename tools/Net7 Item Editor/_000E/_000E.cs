﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: NewItemEditor, Version=1.0.0.7, Culture=neutral, PublicKeyToken=9a6515a4c4e1fd2c
// MVID: 9D5F0B9A-C985-40A9-9939-E28C808E997E
// Assembly location: D:\Server\eab\Tools\Net7 Item Editor.exe

using \u0005;
using \u0007;
using \u0080;
using \u0082;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace \u000E
{
  internal sealed class \u000E : Form
  {
    private CheckBox \u0004 = new CheckBox();
    private Label \u0005 = new Label();
    private Button \u0006 = new Button();
    private Button \u0007 = new Button();
    private Label \u0008 = new Label();
    private \u0081 \u000E = new \u0081();
    private Panel \u000F = new Panel();
    private Panel \u0010 = new Panel();
    private Button \u0011 = new Button();
    private \u0083 \u0012 = new \u0083();
    private \u008B \u0013 = new \u008B(\u000E.\u000E.\u0001(6514));
    private \u008B \u0014 = new \u008B(\u000E.\u000E.\u0001(6551));
    private \u008B \u0015 = new \u008B(\u000E.\u000E.\u0001(6580));
    private \u008B \u0016 = new \u008B(\u000E.\u000E.\u0001(6609));
    private Button \u0017 = new Button();
    private Button \u0018 = new Button();
    private \u0086 \u0019 = new \u0086(string.Format(\u000E.\u000E.\u0001(6662), (object) \u000E.\u000E.\u0001(6472)));
    private \u0086 \u001A = new \u0086(string.Format(\u000E.\u000E.\u0001(6751), (object) \u000E.\u000E.\u0001(6472), (object) \u000E.\u000E.\u0001(6159)));
    private \u008E \u001B = new \u008E();
    private Button \u001C = new Button();
    [NonSerialized]
    internal static \u0004 \u0001;
    private \u0084 \u0001;
    private \u008C \u0002;
    private Thread \u0003;

    private void \u0013()
    {
      \u001B\u0006.\u007E\u009A\u0010((object) this.\u000F);
      \u001B\u0006.\u007E\u009A\u0010((object) this.\u0010);
      \u001B\u0006.\u009A\u0010((object) this);
      this.\u0019.\u0091\u0002(\u008F.\u0002);
      this.\u001A.\u0091\u0002(\u008F.\u0002);
      \u0092\u0004.\u007E\u009D\u000F((object) this.\u0004, AnchorStyles.Bottom | AnchorStyles.Left);
      \u001D\u0006.\u007E\u0002\u0011((object) this.\u0004, FlatStyle.System);
      \u001A\u0005.\u007E\u0017\u0010((object) this.\u0004, new Point(22, 98));
      \u0087\u0006.\u007E\u001C\u0010((object) this.\u0004, new Size(226, 16));
      \u0011\u0004.\u007E\u001D\u0010((object) this.\u0004, 13);
      \u001C\u0005.\u007E\u007F\u0010((object) this.\u0004, \u000E.\u000E.\u0001(6020));
      \u001D\u0005.\u007E\u0007\u0011((object) this.\u0004, new EventHandler(this.\u0017));
      \u0092\u0004.\u007E\u009D\u000F((object) this.\u0005, AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
      \u001D\u0006.\u007E\u0088\u0011((object) this.\u0005, FlatStyle.System);
      \u0014\u0004.\u007E\u0010\u0010((object) this.\u0005, new Font(\u000E.\u000E.\u0001(6081), 8.25f, FontStyle.Bold, GraphicsUnit.Point, (byte) 0));
      \u001A\u0005.\u007E\u0017\u0010((object) this.\u0005, new Point(20, 124));
      \u0087\u0006.\u007E\u001C\u0010((object) this.\u0005, new Size(381, 16));
      \u001C\u0005.\u007E\u007F\u0010((object) this.\u0005, \u0019\u0006.\u0080\u0005(\u000E.\u000E.\u0001(6110), (object) \u000E.\u000E.\u0001(6159)));
      \u0092\u0004.\u007E\u009D\u000F((object) this.\u0006, AnchorStyles.Bottom | AnchorStyles.Right);
      \u001D\u0006.\u007E\u0002\u0011((object) this.\u0006, FlatStyle.System);
      \u0087\u0006.\u007E\u001C\u0010((object) this.\u0006, new Size(75, 24));
      \u001A\u0005.\u007E\u0017\u0010((object) this.\u0006, new Point(400 - \u0083\u0006.\u007E\u0083\u0010((object) this.\u0006), 205));
      \u0011\u0004.\u007E\u001D\u0010((object) this.\u0006, 4);
      \u001C\u0005.\u007E\u007F\u0010((object) this.\u0006, \u000E.\u000E.\u0001(6184));
      \u001D\u0005.\u007E\u0084\u0010((object) this.\u0006, new EventHandler(this.\u0014));
      \u0092\u0004.\u007E\u009D\u000F((object) this.\u0007, AnchorStyles.Bottom | AnchorStyles.Right);
      \u001D\u0006.\u007E\u0002\u0011((object) this.\u0007, FlatStyle.System);
      \u0087\u0006.\u007E\u001C\u0010((object) this.\u0007, new Size(105, 24));
      \u001A\u0005.\u007E\u0017\u0010((object) this.\u0007, new Point(\u0083\u0006.\u007E\u0015\u0010((object) this.\u0006) - \u0083\u0006.\u007E\u0083\u0010((object) this.\u0007) - 6, 205));
      \u0011\u0004.\u007E\u001D\u0010((object) this.\u0007, 3);
      \u001C\u0005.\u007E\u007F\u0010((object) this.\u0007, \u000E.\u000E.\u0001(6201));
      \u001D\u0005.\u007E\u0084\u0010((object) this.\u0007, new EventHandler(this.\u0013));
      \u0092\u0004.\u007E\u009D\u000F((object) this.\u001C, AnchorStyles.Bottom | AnchorStyles.Right);
      \u001D\u0006.\u007E\u0002\u0011((object) this.\u001C, FlatStyle.System);
      \u0087\u0006.\u007E\u001C\u0010((object) this.\u001C, new Size(64, 24));
      \u001A\u0005.\u007E\u0017\u0010((object) this.\u001C, new Point(\u0083\u0006.\u007E\u0015\u0010((object) this.\u0007) - \u0083\u0006.\u007E\u0083\u0010((object) this.\u001C) - 6, 205));
      \u0011\u0004.\u007E\u001D\u0010((object) this.\u001C, 14);
      \u001C\u0005.\u007E\u007F\u0010((object) this.\u001C, \u000E.\u000E.\u0001(6226));
      \u009C\u0006.\u007E\u0082\u0010((object) this.\u001C, false);
      \u001D\u0005.\u007E\u0084\u0010((object) this.\u001C, new EventHandler(this.\u001F));
      \u0092\u0004.\u007E\u009D\u000F((object) this.\u0008, AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
      \u001D\u0006.\u007E\u0088\u0011((object) this.\u0008, FlatStyle.System);
      \u001A\u0005.\u007E\u0017\u0010((object) this.\u0008, new Point(20, 140));
      \u0087\u0006.\u007E\u001C\u0010((object) this.\u0008, new Size(381, 55));
      \u001C\u0005.\u007E\u007F\u0010((object) this.\u0008, \u0019\u0006.\u0080\u0005(\u000E.\u000E.\u0001(6235), (object) \u000E.\u000E.\u0001(6159)));
      \u0092\u0004.\u007E\u009D\u000F((object) this.\u000E, AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right);
      \u001A\u0005.\u007E\u0017\u0010((object) this.\u000E, new Point(20, 69));
      \u0087\u0006.\u007E\u001C\u0010((object) this.\u000E, new Size(381, 13));
      \u0092\u0004.\u007E\u009D\u000F((object) this.\u0011, AnchorStyles.Bottom | AnchorStyles.Right);
      \u001D\u0006.\u007E\u0002\u0011((object) this.\u0011, FlatStyle.System);
      \u0087\u0006.\u007E\u001C\u0010((object) this.\u0011, new Size(80, 24));
      \u001A\u0005.\u007E\u0017\u0010((object) this.\u0011, new Point(400 - \u0083\u0006.\u007E\u0083\u0010((object) this.\u0011), 205));
      \u0011\u0004.\u007E\u001D\u0010((object) this.\u0011, 7);
      \u001C\u0005.\u007E\u007F\u0010((object) this.\u0011, \u000E.\u000E.\u0001(6445));
      \u001D\u0005.\u007E\u0084\u0010((object) this.\u0011, new EventHandler(this.\u0015));
      \u0092\u0004.\u007E\u009D\u000F((object) this.\u0017, AnchorStyles.Bottom | AnchorStyles.Right);
      \u009C\u0006.\u007E\u000E\u0010((object) this.\u0017, false);
      \u001D\u0006.\u007E\u0002\u0011((object) this.\u0017, FlatStyle.System);
      \u0087\u0006.\u007E\u001C\u0010((object) this.\u0017, new Size(80, 24));
      \u001A\u0005.\u007E\u0017\u0010((object) this.\u0017, new Point(\u0083\u0006.\u007E\u0015\u0010((object) this.\u0011) - \u0083\u0006.\u007E\u0083\u0010((object) this.\u0017) - 6, 205));
      \u0011\u0004.\u007E\u001D\u0010((object) this.\u0017, 6);
      \u001C\u0005.\u007E\u007F\u0010((object) this.\u0017, \u000E.\u000E.\u0001(6458));
      \u001D\u0005.\u007E\u0084\u0010((object) this.\u0017, new EventHandler(this.\u0016));
      \u0092\u0004.\u007E\u009D\u000F((object) this.\u0018, AnchorStyles.Bottom | AnchorStyles.Right);
      \u001D\u0006.\u007E\u0002\u0011((object) this.\u0018, FlatStyle.System);
      \u001A\u0005.\u007E\u0017\u0010((object) this.\u0018, \u0081\u0006.\u007E\u0016\u0010((object) this.\u0017));
      \u0087\u0006.\u007E\u001C\u0010((object) this.\u0018, \u001C\u0006.\u007E\u001B\u0010((object) this.\u0017));
      \u0011\u0004.\u007E\u001D\u0010((object) this.\u0018, 5);
      \u001C\u0005.\u007E\u007F\u0010((object) this.\u0018, \u000E.\u000E.\u0001(6463));
      \u009C\u0006.\u007E\u0082\u0010((object) this.\u0018, false);
      \u001D\u0005.\u007E\u0084\u0010((object) this.\u0018, new EventHandler(this.\u001E));
      \u001A\u0005.\u007E\u0017\u0010((object) this.\u0012, new Point(87, 146));
      \u009C\u0006.\u007E\u0082\u0010((object) this.\u0012, false);
      \u007F\u0003.\u007E\u0097\u0010((object) this.\u0013, 24, 72, 368, 16);
      \u007F\u0003.\u007E\u0097\u0010((object) this.\u0014, 24, 96, 368, 16);
      \u007F\u0003.\u007E\u0097\u0010((object) this.\u0015, 24, 120, 368, 16);
      \u007F\u0003.\u007E\u0097\u0010((object) this.\u0016, 24, 144, 368, 16);
      \u0092\u0004.\u007E\u009D\u000F((object) this.\u001B, AnchorStyles.Bottom | AnchorStyles.Left);
      \u007F\u0003.\u007E\u0097\u0010((object) this.\u001B, 6, 450, 120, 32);
      \u0096\u0004.\u007E\u009C\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u000F), new Control[8]
      {
        (Control) this.\u001C,
        (Control) this.\u0004,
        (Control) this.\u0005,
        (Control) this.\u0006,
        (Control) this.\u0007,
        (Control) this.\u0008,
        (Control) this.\u000E,
        (Control) this.\u0019
      });
      \u0087\u0006.\u007E\u001C\u0010((object) this.\u000F, new Size(413, 240));
      \u0011\u0004.\u007E\u001D\u0010((object) this.\u000F, 0);
      \u0096\u0004.\u007E\u009C\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0010), new Control[9]
      {
        (Control) this.\u0011,
        (Control) this.\u0017,
        (Control) this.\u0018,
        (Control) this.\u0012,
        (Control) this.\u001A,
        (Control) this.\u0013,
        (Control) this.\u0014,
        (Control) this.\u0015,
        (Control) this.\u0016
      });
      \u0087\u0006.\u007E\u001C\u0010((object) this.\u0010, new Size(413, 240));
      \u0011\u0004.\u007E\u001D\u0010((object) this.\u0010, 2);
      \u009C\u0006.\u007E\u0082\u0010((object) this.\u0010, false);
      \u0087\u0006.\u007E\u0013\u0011((object) this, new Size(5, 13));
      \u0087\u0006.\u0016\u0011((object) this, new Size(434, 488));
      \u009C\u0006.\u0017\u0011((object) this, false);
      \u0096\u0004.\u007E\u009C\u0010((object) \u001A\u0003.\u0005\u0010((object) this), new Control[3]
      {
        (Control) this.\u001B,
        (Control) this.\u000F,
        (Control) this.\u0010
      });
      \u000E\u0004.\u0014\u0011((object) this, FormBorderStyle.FixedSingle);
      \u009C\u0006.\u001B\u0011((object) this, false);
      \u009C\u0006.\u001A\u0011((object) this, false);
      \u009C\u0006.\u0019\u0011((object) this, false);
      \u008A\u0005.\u001E\u0011((object) this, FormStartPosition.CenterScreen);
      \u001C\u0005.\u007E\u007F\u0010((object) this, \u000E.\u000E.\u0001(6472));
      if (\u0083\u0006.\u007E\u0014\u0005((object) \u0013\u0007.\u007E\u001F\u0010((object) this)) == 0)
        \u001C\u0005.\u007E\u007F\u0010((object) this, \u000E.\u000E.\u0001(6493));
      try
      {
        \u009C\u0006.\u001F\u0011((object) this, true);
      }
      catch
      {
      }
      \u009C\u0006.\u007E\u0095\u0010((object) this.\u000F, false);
      \u009C\u0006.\u007E\u0095\u0010((object) this.\u0010, false);
      \u009C\u0006.\u0095\u0010((object) this, false);
      \u001B\u0006.\u007E\u0089\u0010((object) this.\u0018);
      \u0087\u0006.\u001D\u0011((object) this, new Size(419, 264));
      \u0012\u0004.\u007E\u0008\u0010((object) this.\u0010, DockStyle.Fill);
      \u0012\u0004.\u007E\u0008\u0010((object) this.\u000F, DockStyle.Fill);
    }

    private void \u0013([In] object obj0, [In] EventArgs obj1)
    {
      try
      {
        \u009C\u0006.\u007E\u0082\u0010((object) this.\u000F, false);
        \u009C\u0006.\u007E\u0082\u0010((object) this.\u0010, true);
        \u009C\u0006.\u007E\u0082\u0010((object) this.\u001B, true);
        if (this.\u0002 == null)
          return;
        this.\u0013(new ThreadStart(this.\u0014));
      }
      catch
      {
      }
    }

    private void \u0013([In] ThreadStart obj0)
    {
      this.\u0003 = new Thread(obj0);
      \u001B\u0006.\u007E\u0084\u0007((object) this.\u0003);
    }

    private void \u0014([In] object obj0, [In] EventArgs obj1)
    {
      \u001B\u0006.\u0080\u0011((object) this);
    }

    private void \u0015([In] object obj0, [In] EventArgs obj1)
    {
      try
      {
        if (this.\u0003 != null)
          \u001B\u0006.\u007E\u0086\u0007((object) this.\u0003);
      }
      catch
      {
      }
      \u001B\u0006.\u0080\u0011((object) this);
    }

    private void \u0016([In] object obj0, [In] EventArgs obj1)
    {
      \u001B\u0006.\u0080\u0011((object) this);
    }

    private void \u0017([In] object obj0, [In] EventArgs obj1)
    {
      this.\u0002.\u009A\u0002(\u0006\u0007.\u007E\u0006\u0011((object) this.\u0004));
    }

    private void \u0013([In] object obj0, [In] \u0088 obj1)
    {
      try
      {
        // ISSUE: method pointer
        object obj = \u000E\u0005.\u008E\u0010((object) this, (Delegate) new \u0087((object) this, __methodptr(\u0014)), new object[2]
        {
          obj0,
          (object) obj1
        });
      }
      catch (InvalidOperationException ex)
      {
      }
    }

    private void \u0018([In] object obj0, [In] EventArgs obj1)
    {
      try
      {
        // ISSUE: method pointer
        object obj = \u000E\u0005.\u008E\u0010((object) this, (Delegate) new EventHandler((object) this, __methodptr(\u001D)), new object[2]
        {
          obj0,
          (object) obj1
        });
      }
      catch (InvalidOperationException ex)
      {
      }
    }

    protected override void OnClosing([In] CancelEventArgs obj0)
    {
      if (this.\u0003 != null && \u0006\u0007.\u007E\u0087\u0007((object) this.\u0003))
        \u001B\u0006.\u007E\u0086\u0007((object) this.\u0003);
      \u0016\u0007.\u0082\u0011((object) this, obj0);
    }

    private void \u0014([In] object obj0, [In] \u0088 obj1)
    {
      switch (obj1.\u0092\u0002())
      {
        case \u007F.\u0001:
          if (obj1.\u0093\u0002())
          {
            this.\u0013.\u009F\u0002(obj1.\u0094\u0002());
            \u009C\u0006.\u007E\u0082\u0010((object) this.\u0018, true);
            int num = \u0006\u0007.\u007E\u008C\u0010((object) this.\u0018) ? 1 : 0;
            break;
          }
          this.\u0013.\u009E\u0002();
          break;
        case \u007F.\u0002:
          if (obj1.\u0093\u0002())
          {
            this.\u0014.\u009F\u0002(obj1.\u0094\u0002());
            \u009C\u0006.\u007E\u0082\u0010((object) this.\u0018, true);
            int num = \u0006\u0007.\u007E\u008C\u0010((object) this.\u0018) ? 1 : 0;
            break;
          }
          this.\u0013.\u009F\u0002();
          this.\u0014.\u009E\u0002();
          break;
        case \u007F.\u0003:
          if (obj1.\u0093\u0002())
          {
            \u009C\u0006.\u007E\u0082\u0010((object) this.\u0012, false);
            this.\u0015.\u009F\u0002(obj1.\u0094\u0002());
            \u009C\u0006.\u007E\u0082\u0010((object) this.\u0018, true);
            int num = \u0006\u0007.\u007E\u008C\u0010((object) this.\u0018) ? 1 : 0;
            break;
          }
          this.\u0014.\u009F\u0002();
          this.\u0015.\u009E\u0002();
          \u009C\u0006.\u007E\u0082\u0010((object) this.\u0012, true);
          break;
        case \u007F.\u0004:
          \u009C\u0006.\u007E\u0082\u0010((object) this.\u0012, false);
          this.\u0015.\u009F\u0002();
          this.\u0016.\u009F\u0002();
          \u009C\u0006.\u007E\u000E\u0010((object) this.\u0017, true);
          int num1 = \u0006\u0007.\u007E\u008C\u0010((object) this.\u0017) ? 1 : 0;
          \u009C\u0006.\u007E\u000E\u0010((object) this.\u0011, false);
          break;
      }
    }

    private void \u001D([In] object obj0, [In] EventArgs obj1)
    {
      \u001B\u0006.\u0080\u0011((object) this);
    }

    private void \u001E([In] object obj0, [In] EventArgs obj1)
    {
      \u009C\u0006.\u007E\u0082\u0010((object) this.\u0018, false);
      this.\u0013.\u009D\u0002();
      this.\u0014.\u009D\u0002();
      this.\u0015.\u009D\u0002();
      if (this.\u0002 == null)
        return;
      this.\u0013(new ThreadStart(this.\u0014));
    }

    private void \u0014()
    {
      this.\u0002.\u0005\u0003();
    }

    private void \u001F([In] object obj0, [In] EventArgs obj1)
    {
      if (this.\u0002 == null)
        return;
      this.\u0013(new ThreadStart(this.\u0002.\u0004\u0003));
    }

    public \u000E([In] \u0084 obj0, [In] \u008C obj1)
    {
      this.\u0013();
      int height = this.Height;
      this.\u0002 = obj1;
      this.\u0001 = obj0;
      this.\u000E.Text = obj1.\u0001\u0003().Message;
      int num1 = height + (\u0083\u0006.\u007E\u0013\u0010((object) this.\u000E) - \u0083\u0006.\u0011\u0010((object) this));
      if (!obj1.\u0098\u0002())
      {
        \u009C\u0006.\u007E\u0082\u0010((object) this.\u0004, false);
        num1 -= \u0083\u0006.\u007E\u0013\u0010((object) this.\u0004);
      }
      if (num1 > \u0083\u0006.\u0013\u0010((object) this))
        \u0011\u0004.\u0014\u0010((object) this, num1);
      if (obj1.\u0002\u0003())
      {
        obj0.\u0086\u0002(new EventHandler(this.\u0018));
        \u009C\u0006.\u007E\u0082\u0010((object) this.\u001C, true);
        if (\u0083\u0006.\u007E\u0015\u0010((object) this.\u001C) < \u0083\u0006.\u007E\u001A\u0010((object) this.\u001B))
          \u009C\u0006.\u007E\u0082\u0010((object) this.\u001B, false);
      }
      if (!obj1.\u0003\u0003())
      {
        \u009C\u0006.\u007E\u000E\u0010((object) this.\u0007, false);
        if (\u0006\u0007.\u007E\u0003\u0010((object) this.\u0006))
        {
          int num2 = \u0006\u0007.\u007E\u008C\u0010((object) this.\u0006) ? 1 : 0;
        }
      }
      obj0.\u0087\u0002(new \u0087(this.\u0013));
    }

    static \u000E()
    {
      \u0006.\u0080\u0002();
    }
  }
}
