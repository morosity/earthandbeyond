﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: NewItemEditor, Version=1.0.0.7, Culture=neutral, PublicKeyToken=9a6515a4c4e1fd2c
// MVID: 9D5F0B9A-C985-40A9-9939-E28C808E997E
// Assembly location: D:\Server\eab\Tools\Net7 Item Editor.exe

using \u0005;
using \u0007;
using \u000E;
using SmartAssembly.SmartExceptionsCore;
using System;
using System.Net;
using System.Runtime.InteropServices;

namespace \u000E
{
  internal sealed class \u0013
  {
    private string \u0002 = string.Empty;
    [NonSerialized]
    internal static \u0004 \u0001;
    private string \u0001;
    private IWebProxy \u0003;

    public void \u0013([In] IWebProxy obj0)
    {
      this.\u0003 = obj0;
    }

    public string \u0013()
    {
      try
      {
        UploadReportLoginService reportLoginService = new UploadReportLoginService();
        if (this.\u0003 != null)
          \u001A\u0007.\u007E\u0087\u0004((object) reportLoginService, this.\u0003);
        this.\u0002 = reportLoginService.GetServerURL(this.\u0001);
        if (\u0083\u0006.\u007E\u0014\u0005((object) this.\u0002) == 0)
          throw new ApplicationException(\u0013.\u0001(5320));
        return \u0089\u0003.\u007E\u001C\u0005((object) this.\u0002, \u0013.\u0001(4892)) ? this.\u0002 : \u0013.\u0001(4887);
      }
      catch (Exception ex)
      {
        return \u0006\u0003.\u0083\u0005(\u0013.\u0001(5361), \u0013\u0007.\u007E\u0090\u0005((object) ex));
      }
    }

    public string \u0013([In] byte[] obj0)
    {
      try
      {
        ReportingService reportingService = new ReportingService(this.\u0002);
        if (this.\u0003 != null)
          \u001A\u0007.\u007E\u0087\u0004((object) reportingService, this.\u0003);
        return reportingService.UploadReport2(this.\u0001, obj0);
      }
      catch (Exception ex)
      {
        return \u0006\u0003.\u0083\u0005(\u0013.\u0001(5378), \u0013\u0007.\u007E\u0090\u0005((object) ex));
      }
    }

    public \u0013([In] string obj0)
    {
      this.\u0001 = obj0;
    }

    static \u0013()
    {
      \u0006.\u0080\u0002();
    }
  }
}
