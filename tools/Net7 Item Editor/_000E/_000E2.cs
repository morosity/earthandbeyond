﻿// Decompiled with JetBrains decompiler
// Type: .2
// Assembly: NewItemEditor, Version=1.0.0.7, Culture=neutral, PublicKeyToken=9a6515a4c4e1fd2c
// MVID: 9D5F0B9A-C985-40A9-9939-E28C808E997E
// Assembly location: D:\Server\eab\Tools\Net7 Item Editor.exe

using \u0005;
using \u0007;
using \u000E;
using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Cryptography;

namespace \u000E
{
  internal sealed class \u000E2
  {
    [NonSerialized]
    internal static \u0004 \u0001;
    public static string \u0001;

    public static byte[] \u0013([In] byte[] obj0, [In] string obj1)
    {
      if (\u0089\u0003.\u007E\u001C\u0005((object) obj1, \u000E2.\u0001(2852)))
      {
        \u000E2.\u0001 = \u000E2.\u0001(2857);
        return (byte[]) null;
      }
      try
      {
        RijndaelManaged rijndaelManaged = new RijndaelManaged();
        RSACryptoServiceProvider cryptoServiceProvider = new RSACryptoServiceProvider();
        \u001C\u0005.\u007E\u0088\u000E((object) cryptoServiceProvider, obj1);
        \u001B\u0006.\u007E\u0092\u000E((object) rijndaelManaged);
        \u001B\u0006.\u007E\u0093\u000E((object) rijndaelManaged);
        byte[] numArray1 = new byte[48];
        \u008B\u0002.\u0012\u0006((Array) \u0016\u0003.\u007E\u008E\u000E((object) rijndaelManaged), 0, (Array) numArray1, 0, 32);
        \u008B\u0002.\u0012\u0006((Array) \u0016\u0003.\u007E\u008C\u000E((object) rijndaelManaged), 0, (Array) numArray1, 32, 16);
        MemoryStream memoryStream = new MemoryStream();
        try
        {
          byte[] numArray2 = \u0097\u0002.\u007E\u0096\u000E((object) cryptoServiceProvider, numArray1, false);
          \u0006\u0006.\u007E\u0010\u000E((object) memoryStream, (byte) 1);
          \u0006\u0006.\u007E\u0010\u000E((object) memoryStream, \u0090\u0003.\u0015\u0006(numArray2.Length / 8));
          \u009D\u0006.\u007E\u000F\u000E((object) memoryStream, numArray2, 0, numArray2.Length);
        }
        catch (CryptographicException ex1)
        {
          try
          {
            byte[] numArray2 = new byte[16];
            byte[] numArray3 = new byte[16];
            \u008B\u0002.\u0012\u0006((Array) \u0016\u0003.\u007E\u008E\u000E((object) rijndaelManaged), 0, (Array) numArray2, 0, 16);
            \u008B\u0002.\u0012\u0006((Array) \u0016\u0003.\u007E\u008E\u000E((object) rijndaelManaged), 16, (Array) numArray3, 0, 16);
            byte[] numArray4 = \u0097\u0002.\u007E\u0096\u000E((object) cryptoServiceProvider, numArray2, false);
            byte[] numArray5 = \u0097\u0002.\u007E\u0096\u000E((object) cryptoServiceProvider, numArray3, false);
            byte[] numArray6 = \u0097\u0002.\u007E\u0096\u000E((object) cryptoServiceProvider, \u0016\u0003.\u007E\u008C\u000E((object) rijndaelManaged), false);
            \u0006\u0006.\u007E\u0010\u000E((object) memoryStream, (byte) 2);
            \u0006\u0006.\u007E\u0010\u000E((object) memoryStream, \u0090\u0003.\u0015\u0006(numArray4.Length / 8));
            \u009D\u0006.\u007E\u000F\u000E((object) memoryStream, numArray4, 0, numArray4.Length);
            \u009D\u0006.\u007E\u000F\u000E((object) memoryStream, numArray5, 0, numArray5.Length);
            \u009D\u0006.\u007E\u000F\u000E((object) memoryStream, numArray6, 0, numArray6.Length);
          }
          catch (CryptographicException ex2)
          {
            \u000E2.\u0001 = \u000E2.\u0001(2950);
            return (byte[]) null;
          }
        }
        CryptoStream cryptoStream = new CryptoStream((Stream) memoryStream, \u0017\u0005.\u007E\u0090\u000E((object) rijndaelManaged), CryptoStreamMode.Write);
        \u009D\u0006.\u007E\u000F\u000E((object) cryptoStream, obj0, 0, obj0.Length);
        \u001B\u0006.\u007E\u008A\u000E((object) cryptoStream);
        return \u0016\u0003.\u007E\u0018\u000E((object) memoryStream);
      }
      catch (Exception ex)
      {
        \u000E2.\u0001 = \u0006\u0003.\u0083\u0005(\u000E2.\u0001(3156), \u0013\u0007.\u007E\u0090\u0005((object) ex));
        return (byte[]) null;
      }
    }

    static \u000E2()
    {
      \u0006.\u0080\u0002();
    }
  }
}
