﻿using SmartAssembly.Attributes;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyFileVersion("1.0.0.7")]
[assembly: Guid("fcc5b1ee-b789-41ca-8794-c287a54f325f")]
[assembly: PoweredBy("Powered by {smartassembly}")]
[assembly: SuppressIldasm]
[assembly: AssemblyTitle("NewItemEditor")]
[assembly: AssemblyCopyright("Copyright © Microsoft 2009")]
[assembly: AssemblyProduct("NewItemEditor")]
[assembly: AssemblyCompany("Microsoft")]
[assembly: AssemblyVersion("1.0.0.7")]
