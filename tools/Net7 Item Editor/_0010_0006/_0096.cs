﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: NewItemEditor, Version=1.0.0.7, Culture=neutral, PublicKeyToken=9a6515a4c4e1fd2c
// MVID: 9D5F0B9A-C985-40A9-9939-E28C808E997E
// Assembly location: D:\Server\eab\Tools\Net7 Item Editor.exe

using \u0005;
using \u0007;
using \u000E\u0006;
using \u000F\u0006;
using \u0010\u0006;
using \u0080;
using \u0091;
using System;
using System.Data;
using System.Windows.Forms;

namespace \u0010\u0006
{
  internal static class \u0096
  {
    [NonSerialized]
    internal static \u0004 \u0001;
    public static \u009C \u0001;

    [STAThread]
    private static void \u009C()
    {
      \u0098 obj1;
      DataSet dataSet;
      DataTable dataTable;
      DataRow dataRow;
      \u009E obj2;
      int num1;
      bool flag;
      try
      {
        if (!\u0090.\u007F\u0002())
          return;
        \u0001\u0006.\u0098\u000F();
        \u0007\u0005.\u009B\u000F(false);
        obj1 = new \u0098();
        try
        {
          dataSet = new DataSet();
          int num2 = (int) \u008D\u0002.\u007E\u0019\u0012((object) dataSet, \u0006\u0003.\u0083\u0005(\u0008\u0004.\u0095\u000F(), \u0096.\u0001(9010)));
          dataTable = \u007F\u0006.\u007E\u001E\u0012((object) \u0099\u0002.\u007E\u0017\u0012((object) dataSet), 0);
          dataRow = \u0095\u0006.\u007E\u0015\u0012((object) \u009D\u0004.\u007E\u001C\u0012((object) dataTable), 0);
          \u0017\u0002.\u0081\u0005(\u0013\u0007.\u007E\u009C\u0004(\u0013\u0004.\u007E\u0012\u0012((object) dataRow)[0]));
          \u0017\u0002.\u0088\u0005(\u0088\u0006.\u008A\u0006(\u0013\u0007.\u007E\u009C\u0004(\u0013\u0004.\u007E\u0012\u0012((object) dataRow)[1])));
          \u0017\u0002.\u0083\u0005(\u0013\u0007.\u007E\u009C\u0004(\u0013\u0004.\u007E\u0012\u0012((object) dataRow)[2]));
          \u0017\u0002.\u0086\u0005(\u0013\u0007.\u007E\u009C\u0004(\u0013\u0004.\u007E\u0012\u0012((object) dataRow)[3]));
        }
        catch (Exception ex)
        {
          \u0017\u0002.\u0081\u0005(\u0096.\u0001(26590));
          \u0017\u0002.\u0088\u0005(3307);
          \u0017\u0002.\u0083\u0005(\u0096.\u0001(8645));
          \u0017\u0002.\u0086\u0005(\u0096.\u0001(8645));
        }
        obj2 = new \u009E();
        \u001C\u0005.\u007E\u007F\u0010((object) obj2.\u0004, \u0017\u0002.\u0082\u0005());
        \u001C\u0005.\u007E\u007F\u0010((object) obj2.\u0005, \u0017\u0002.\u0084\u0005());
        \u001C\u0005.\u007E\u007F\u0010((object) obj2.\u000E, \u0017\u0002.\u0080\u0005());
        \u001C\u0005 obj3 = \u001C\u0005.\u007E\u007F\u0010;
        TextBox textBox = obj2.\u000F;
        num1 = \u0017\u0002.\u0087\u0005();
        string str = num1.ToString();
        obj3((object) textBox, str);
        int num3 = (int) \u0096\u0002.\u007E\u0083\u0011((object) obj2);
        flag = obj2.\u0011;
        if (flag)
          return;
        obj1.\u0083\u0003(\u0096.\u0001(26611));
        \u001B\u0006.\u007E\u0099\u0010((object) obj1);
        \u0017\u0002.\u008A\u0005();
        obj1.\u0083\u0003(\u0096.\u0001(26640));
        \u0096.\u0001 = new \u009C(ref obj1);
        \u0004\u0006.\u009A\u000F((Form) \u0096.\u0001);
      }
      catch (Exception ex)
      {
        object[] objArray = new object[7]
        {
          (object) obj1,
          (object) dataSet,
          (object) dataTable,
          (object) dataRow,
          (object) obj2,
          (object) num1,
          (object) flag
        };
        \u0084.\u008D\u0002(ex, objArray);
      }
    }

    static \u0096()
    {
      \u0006.\u0080\u0002();
    }
  }
}
