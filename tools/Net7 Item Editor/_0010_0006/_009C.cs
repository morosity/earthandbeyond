﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: NewItemEditor, Version=1.0.0.7, Culture=neutral, PublicKeyToken=9a6515a4c4e1fd2c
// MVID: 9D5F0B9A-C985-40A9-9939-E28C808E997E
// Assembly location: D:\Server\eab\Tools\Net7 Item Editor.exe

using \u0005;
using \u0007;
using \u000E\u0006;
using \u000F;
using \u000F\u0006;
using \u0010\u0006;
using \u0013\u0006;
using DevExpress.LookAndFeel;
using DevExpress.Utils;
using DevExpress.Utils.Menu;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraEditors.Repository;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTab;
using SmartAssembly.SmartExceptionsCore;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace \u0010\u0006
{
  internal sealed class \u009C : XtraForm
  {
    private IContainer \u0001 = (IContainer) null;
    private DataSet \u0087\u0005 = new DataSet();
    private int \u0090\u0005 = 0;
    private \u0012\u0006 \u0091\u0005 = new \u0012\u0006();
    [NonSerialized]
    internal static \u0004 \u0001;
    private BarManager \u0002;
    private Bar \u0003;
    private Bar \u0004;
    private BarDockControl \u0005;
    private BarDockControl \u0006;
    private BarDockControl \u0007;
    private BarDockControl \u0008;
    private BarButtonItem \u000E;
    private BarButtonItem \u000F;
    private BarButtonItem \u0010;
    private BarButtonItem \u0011;
    private BarButtonItem \u0012;
    private BarEditItem \u0013;
    private RepositoryItemTextEdit \u0014;
    private BarStaticItem \u0015;
    private XtraTabControl \u0016;
    private XtraTabPage \u0017;
    private XtraTabPage \u0018;
    private XtraTabPage \u0019;
    private XtraTabPage \u001A;
    private XtraTabPage \u001B;
    private XtraTabPage \u001C;
    private XtraTabPage \u001D;
    private XtraTabPage \u001E;
    private XtraTabPage \u001F;
    private XtraTabPage \u007F;
    private XtraTabPage \u0080;
    private XtraTabPage \u0081;
    private TextEdit \u0082;
    private TextEdit \u0083;
    private LabelControl \u0084;
    private LabelControl \u0086;
    private LabelControl \u0087;
    private LabelControl \u0088;
    private LabelControl \u0089;
    private LabelControl \u008A;
    private LabelControl \u008B;
    private LabelControl \u008C;
    private TextEdit \u008D;
    private ComboBoxEdit \u008E;
    private LookUpEdit \u008F;
    private LookUpEdit \u0090;
    private LookUpEdit \u0091;
    private LookUpEdit \u0092;
    private LabelControl \u0093;
    private ButtonEdit \u0094;
    private ButtonEdit \u0095;
    private LabelControl \u0096;
    private GroupControl \u0097;
    private CheckEdit \u0098;
    private CheckEdit \u0099;
    private CheckEdit \u009A;
    private CheckEdit \u009B;
    private CheckEdit \u009C;
    private CheckEdit \u009D;
    private LabelControl \u009E;
    private LabelControl \u009F;
    private TextEdit \u0001\u0002;
    private LabelControl \u0002\u0002;
    private ButtonEdit \u0003\u0002;
    private ButtonEdit \u0004\u0002;
    private LabelControl \u0005\u0002;
    private LabelControl \u0006\u0002;
    private GroupControl \u0007\u0002;
    private MemoEdit \u0008\u0002;
    private CheckEdit \u000E\u0002;
    private CheckEdit \u000F\u0002;
    private CheckEdit \u0010\u0002;
    private CheckEdit \u0011\u0002;
    private CheckEdit \u0012\u0002;
    private LabelControl \u0013\u0002;
    private TextEdit \u0014\u0002;
    private TextEdit \u0015\u0002;
    private TextEdit \u0016\u0002;
    private TextEdit \u0017\u0002;
    private ButtonEdit \u0018\u0002;
    private LabelControl \u0019\u0002;
    private LabelControl \u001A\u0002;
    private LabelControl \u001B\u0002;
    private LabelControl \u001C\u0002;
    private LabelControl \u001D\u0002;
    private LabelControl \u001E\u0002;
    private LookUpEdit \u001F\u0002;
    private LabelControl \u007F\u0002;
    private TextEdit \u0080\u0002;
    private TextEdit \u0081\u0002;
    private TextEdit \u0082\u0002;
    private TextEdit \u0083\u0002;
    private LabelControl \u0084\u0002;
    private LabelControl \u0086\u0002;
    private LabelControl \u0087\u0002;
    private LabelControl \u0088\u0002;
    private LabelControl \u0089\u0002;
    private LookUpEdit \u008A\u0002;
    private TextEdit \u008B\u0002;
    private TextEdit \u008C\u0002;
    private TextEdit \u008D\u0002;
    private LabelControl \u008E\u0002;
    private LabelControl \u008F\u0002;
    private LabelControl \u0090\u0002;
    private LabelControl \u0091\u0002;
    private LookUpEdit \u0092\u0002;
    private TextEdit \u0093\u0002;
    private LabelControl \u0094\u0002;
    private LabelControl \u0095\u0002;
    private LookUpEdit \u0096\u0002;
    private TextEdit \u0097\u0002;
    private LabelControl \u0098\u0002;
    private LabelControl \u0099\u0002;
    private LookUpEdit \u009A\u0002;
    private TextEdit \u009B\u0002;
    private TextEdit \u009C\u0002;
    private LabelControl \u009D\u0002;
    private LabelControl \u009E\u0002;
    private TextEdit \u009F\u0002;
    private TextEdit \u0001\u0003;
    private LabelControl \u0002\u0003;
    private LabelControl \u0003\u0003;
    private TextEdit \u0004\u0003;
    private TextEdit \u0005\u0003;
    private TextEdit \u0006\u0003;
    private TextEdit \u0007\u0003;
    private LabelControl \u0008\u0003;
    private LabelControl \u000E\u0003;
    private LabelControl \u000F\u0003;
    private LabelControl \u0010\u0003;
    private LookUpEdit \u0011\u0003;
    private LabelControl \u0012\u0003;
    private Bar \u0013\u0003;
    private BarSubItem \u0014\u0003;
    private BarSubItem \u0015\u0003;
    private BarSubItem \u0016\u0003;
    private CheckEdit \u0017\u0003;
    private XtraTabPage \u0018\u0003;
    private TextEdit \u0019\u0003;
    private TextEdit \u001A\u0003;
    private LabelControl \u001B\u0003;
    private LabelControl \u001C\u0003;
    private ButtonEdit \u001D\u0003;
    private LabelControl \u001E\u0003;
    private TextEdit \u001F\u0003;
    private LabelControl \u007F\u0003;
    private ButtonEdit \u0080\u0003;
    private TextEdit \u0081\u0003;
    private LabelControl \u0082\u0003;
    private ButtonEdit \u0083\u0003;
    private TextEdit \u0084\u0003;
    private LabelControl \u0086\u0003;
    private ButtonEdit \u0087\u0003;
    private TextEdit \u0088\u0003;
    private LabelControl \u0089\u0003;
    private ButtonEdit \u008A\u0003;
    private TextEdit \u008B\u0003;
    private LabelControl \u008C\u0003;
    private ButtonEdit \u008D\u0003;
    private TextEdit \u008E\u0003;
    private LabelControl \u008F\u0003;
    private ButtonEdit \u0090\u0003;
    private LabelControl \u0091\u0003;
    private TextEdit \u0092\u0003;
    private TextEdit \u0093\u0003;
    private TextEdit \u0094\u0003;
    private TextEdit \u0095\u0003;
    private TextEdit \u0096\u0003;
    private TextEdit \u0097\u0003;
    private GroupControl \u0098\u0003;
    private LabelControl \u0099\u0003;
    private LabelControl \u009A\u0003;
    private LabelControl \u009B\u0003;
    private LabelControl \u009C\u0003;
    private LabelControl \u009D\u0003;
    private LabelControl \u009E\u0003;
    private TextEdit \u009F\u0003;
    private TextEdit \u0001\u0004;
    private TextEdit \u0002\u0004;
    private TrackBarControl \u0003\u0004;
    private TrackBarControl \u0004\u0004;
    private TrackBarControl \u0005\u0004;
    private DefaultLookAndFeel \u0006\u0004;
    private TextEdit \u0007\u0004;
    private LabelControl \u0008\u0004;
    private GroupControl \u000E\u0004;
    private TextEdit \u000F\u0004;
    private LabelControl \u0010\u0004;
    private TextEdit \u0011\u0004;
    private LabelControl \u0012\u0004;
    private GroupControl \u0013\u0004;
    private SpinEdit \u0014\u0004;
    private LabelControl \u0015\u0004;
    private TextEdit \u0016\u0004;
    private LabelControl \u0017\u0004;
    private TextEdit \u0018\u0004;
    private LabelControl \u0019\u0004;
    private SpinEdit \u001A\u0004;
    private LabelControl \u001B\u0004;
    private SpinEdit \u001C\u0004;
    private LabelControl \u001D\u0004;
    private TextEdit \u001E\u0004;
    private TextEdit \u001F\u0004;
    private SimpleButton \u007F\u0004;
    private LabelControl \u0080\u0004;
    private LabelControl \u0081\u0004;
    private LabelControl \u0082\u0004;
    private LabelControl \u0083\u0004;
    private TextEdit \u0084\u0004;
    private TextEdit \u0086\u0004;
    private TextEdit \u0087\u0004;
    private LookUpEdit \u0088\u0004;
    private LabelControl \u0089\u0004;
    private LabelControl \u008A\u0004;
    private LabelControl \u008B\u0004;
    private LabelControl \u008C\u0004;
    private TextEdit \u008D\u0004;
    private TextEdit \u008E\u0004;
    private TextEdit \u008F\u0004;
    private LookUpEdit \u0090\u0004;
    private LabelControl \u0091\u0004;
    private LabelControl \u0092\u0004;
    private LabelControl \u0093\u0004;
    private LabelControl \u0094\u0004;
    private TextEdit \u0095\u0004;
    private TextEdit \u0096\u0004;
    private TextEdit \u0097\u0004;
    private LookUpEdit \u0098\u0004;
    private LabelControl \u0099\u0004;
    private LabelControl \u009A\u0004;
    private LabelControl \u009B\u0004;
    private LabelControl \u009C\u0004;
    private TextEdit \u009D\u0004;
    private TextEdit \u009E\u0004;
    private TextEdit \u009F\u0004;
    private LookUpEdit \u0001\u0005;
    private LabelControl \u0002\u0005;
    private LabelControl \u0003\u0005;
    private LabelControl \u0004\u0005;
    private LabelControl \u0005\u0005;
    private TextEdit \u0006\u0005;
    private TextEdit \u0007\u0005;
    private TextEdit \u0008\u0005;
    private LookUpEdit \u000E\u0005;
    private LabelControl \u000F\u0005;
    private LabelControl \u0010\u0005;
    private LabelControl \u0011\u0005;
    private LabelControl \u0012\u0005;
    private TextEdit \u0013\u0005;
    private TextEdit \u0014\u0005;
    private TextEdit \u0015\u0005;
    private LookUpEdit \u0016\u0005;
    private BarButtonItem \u0017\u0005;
    private BarButtonItem \u0018\u0005;
    private BarButtonItem \u0019\u0005;
    private BarButtonItem \u001A\u0005;
    private PopupMenu \u001B\u0005;
    private ToolTipController \u001C\u0005;
    private GridControl \u001D\u0005;
    private GridView \u001E\u0005;
    private GridColumn \u001F\u0005;
    private RepositoryItemLookUpEdit \u007F\u0005;
    private GridColumn \u0080\u0005;
    private GridColumn \u0081\u0005;
    private GridColumn \u0082\u0005;
    private GridColumn \u0083\u0005;
    private GridColumn \u0084\u0005;
    private RepositoryItemTextEdit \u0086\u0005;
    public DataTable \u0088\u0005;
    private DataTable \u0089\u0005;
    private DataTable \u008A\u0005;
    private DataTable \u008B\u0005;
    private DataTable \u008C\u0005;
    private DataTable \u008D\u0005;
    private DataTable \u008E\u0005;
    private DataTable \u008F\u0005;
    private int \u0092\u0005;
    private \u0098 \u0093\u0005;

    protected override void Dispose([In] bool obj0)
    {
      bool flag;
      try
      {
        flag = !obj0 || this.\u0001 == null;
        if (!flag)
          \u001B\u0006.\u007E\u000F\u0005((object) this.\u0001);
        \u009C\u0006.\u000E\u0004((object) this, obj0);
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<bool> local1 = (ValueType) flag;
        // ISSUE: variable of a boxed type
        __Boxed<bool> local2 = (ValueType) obj0;
        throw UnhandledException.\u000F\u0003(ex, (object) local1, (object) this, (object) local2);
      }
    }

    private void \u0081\u0003()
    {
      ComponentResourceManager componentResourceManager;
      Bar[] barArray1;
      BarItem[] barItemArray1;
      RepositoryItem[] repositoryItemArray1;
      LinkPersistInfo[] linkPersistInfoArray1;
      XtraTabPage[] xtraTabPageArray1;
      int[] bits;
      EditorButton[] editorButtonArray1;
      object[] objArray1;
      BaseView[] baseViewArray1;
      GridColumn[] gridColumnArray1;
      try
      {
        this.\u0001 = (IContainer) new Container();
        // ISSUE: type reference
        componentResourceManager = new ComponentResourceManager(\u001F\u0002.\u009A\u0006(__typeref (\u009C)));
        this.\u0002 = new BarManager(this.\u0001);
        this.\u0003 = new Bar();
        this.\u000E = new BarButtonItem();
        this.\u000F = new BarButtonItem();
        this.\u0010 = new BarButtonItem();
        this.\u0011 = new BarButtonItem();
        this.\u0012 = new BarButtonItem();
        this.\u0015 = new BarStaticItem();
        this.\u0013 = new BarEditItem();
        this.\u0014 = new RepositoryItemTextEdit();
        this.\u0004 = new Bar();
        this.\u0013\u0003 = new Bar();
        this.\u0014\u0003 = new BarSubItem();
        this.\u0015\u0003 = new BarSubItem();
        this.\u0016\u0003 = new BarSubItem();
        this.\u0005 = new BarDockControl();
        this.\u0006 = new BarDockControl();
        this.\u0007 = new BarDockControl();
        this.\u0008 = new BarDockControl();
        this.\u0017\u0005 = new BarButtonItem();
        this.\u0018\u0005 = new BarButtonItem();
        this.\u0019\u0005 = new BarButtonItem();
        this.\u001A\u0005 = new BarButtonItem();
        this.\u0016 = new XtraTabControl();
        this.\u0017 = new XtraTabPage();
        this.\u000E\u0004 = new GroupControl();
        this.\u001A\u0004 = new SpinEdit();
        this.\u000F\u0004 = new TextEdit();
        this.\u001B\u0004 = new LabelControl();
        this.\u0010\u0004 = new LabelControl();
        this.\u001C\u0004 = new SpinEdit();
        this.\u001D\u0004 = new LabelControl();
        this.\u0011\u0004 = new TextEdit();
        this.\u0012\u0004 = new LabelControl();
        this.\u0007\u0004 = new TextEdit();
        this.\u0008\u0004 = new LabelControl();
        this.\u0017\u0003 = new CheckEdit();
        this.\u0011\u0003 = new LookUpEdit();
        this.\u0012\u0003 = new LabelControl();
        this.\u0013\u0002 = new LabelControl();
        this.\u0008\u0002 = new MemoEdit();
        this.\u0007\u0002 = new GroupControl();
        this.\u000E\u0002 = new CheckEdit();
        this.\u000F\u0002 = new CheckEdit();
        this.\u0010\u0002 = new CheckEdit();
        this.\u0011\u0002 = new CheckEdit();
        this.\u0012\u0002 = new CheckEdit();
        this.\u0001\u0002 = new TextEdit();
        this.\u0002\u0002 = new LabelControl();
        this.\u0003\u0002 = new ButtonEdit();
        this.\u0004\u0002 = new ButtonEdit();
        this.\u0005\u0002 = new LabelControl();
        this.\u0006\u0002 = new LabelControl();
        this.\u0097 = new GroupControl();
        this.\u009E = new LabelControl();
        this.\u009F = new LabelControl();
        this.\u0098 = new CheckEdit();
        this.\u0099 = new CheckEdit();
        this.\u009A = new CheckEdit();
        this.\u009B = new CheckEdit();
        this.\u009C = new CheckEdit();
        this.\u009D = new CheckEdit();
        this.\u0094 = new ButtonEdit();
        this.\u0095 = new ButtonEdit();
        this.\u0096 = new LabelControl();
        this.\u0093 = new LabelControl();
        this.\u008D = new TextEdit();
        this.\u008E = new ComboBoxEdit();
        this.\u008F = new LookUpEdit();
        this.\u0090 = new LookUpEdit();
        this.\u0091 = new LookUpEdit();
        this.\u0092 = new LookUpEdit();
        this.\u0082 = new TextEdit();
        this.\u0083 = new TextEdit();
        this.\u0084 = new LabelControl();
        this.\u0086 = new LabelControl();
        this.\u0087 = new LabelControl();
        this.\u0088 = new LabelControl();
        this.\u0089 = new LabelControl();
        this.\u008A = new LabelControl();
        this.\u008B = new LabelControl();
        this.\u008C = new LabelControl();
        this.\u0018 = new XtraTabPage();
        this.\u007F\u0002 = new LabelControl();
        this.\u0014\u0002 = new TextEdit();
        this.\u0015\u0002 = new TextEdit();
        this.\u0016\u0002 = new TextEdit();
        this.\u0017\u0002 = new TextEdit();
        this.\u0018\u0002 = new ButtonEdit();
        this.\u0019\u0002 = new LabelControl();
        this.\u001A\u0002 = new LabelControl();
        this.\u001B\u0002 = new LabelControl();
        this.\u001C\u0002 = new LabelControl();
        this.\u001D\u0002 = new LabelControl();
        this.\u001E\u0002 = new LabelControl();
        this.\u001F\u0002 = new LookUpEdit();
        this.\u0019 = new XtraTabPage();
        this.\u0080\u0002 = new TextEdit();
        this.\u0081\u0002 = new TextEdit();
        this.\u0082\u0002 = new TextEdit();
        this.\u0083\u0002 = new TextEdit();
        this.\u0084\u0002 = new LabelControl();
        this.\u0086\u0002 = new LabelControl();
        this.\u0087\u0002 = new LabelControl();
        this.\u0088\u0002 = new LabelControl();
        this.\u0089\u0002 = new LabelControl();
        this.\u008A\u0002 = new LookUpEdit();
        this.\u001A = new XtraTabPage();
        this.\u008B\u0002 = new TextEdit();
        this.\u008C\u0002 = new TextEdit();
        this.\u008D\u0002 = new TextEdit();
        this.\u008E\u0002 = new LabelControl();
        this.\u008F\u0002 = new LabelControl();
        this.\u0090\u0002 = new LabelControl();
        this.\u0091\u0002 = new LabelControl();
        this.\u0092\u0002 = new LookUpEdit();
        this.\u001B = new XtraTabPage();
        this.\u001D\u0003 = new ButtonEdit();
        this.\u001E\u0003 = new LabelControl();
        this.\u0093\u0002 = new TextEdit();
        this.\u0094\u0002 = new LabelControl();
        this.\u0095\u0002 = new LabelControl();
        this.\u0096\u0002 = new LookUpEdit();
        this.\u0097\u0002 = new TextEdit();
        this.\u0098\u0002 = new LabelControl();
        this.\u0099\u0002 = new LabelControl();
        this.\u009A\u0002 = new LookUpEdit();
        this.\u001C = new XtraTabPage();
        this.\u009B\u0002 = new TextEdit();
        this.\u009C\u0002 = new TextEdit();
        this.\u009D\u0002 = new LabelControl();
        this.\u009E\u0002 = new LabelControl();
        this.\u001D = new XtraTabPage();
        this.\u009F\u0002 = new TextEdit();
        this.\u0001\u0003 = new TextEdit();
        this.\u0002\u0003 = new LabelControl();
        this.\u0003\u0003 = new LabelControl();
        this.\u0018\u0003 = new XtraTabPage();
        this.\u0019\u0003 = new TextEdit();
        this.\u001A\u0003 = new TextEdit();
        this.\u001B\u0003 = new LabelControl();
        this.\u001C\u0003 = new LabelControl();
        this.\u001E = new XtraTabPage();
        this.\u0004\u0003 = new TextEdit();
        this.\u0005\u0003 = new TextEdit();
        this.\u0006\u0003 = new TextEdit();
        this.\u0007\u0003 = new TextEdit();
        this.\u0008\u0003 = new LabelControl();
        this.\u000E\u0003 = new LabelControl();
        this.\u000F\u0003 = new LabelControl();
        this.\u0010\u0003 = new LabelControl();
        this.\u001F = new XtraTabPage();
        this.\u007F = new XtraTabPage();
        this.\u0013\u0004 = new GroupControl();
        this.\u0014\u0004 = new SpinEdit();
        this.\u0015\u0004 = new LabelControl();
        this.\u0016\u0004 = new TextEdit();
        this.\u0017\u0004 = new LabelControl();
        this.\u0018\u0004 = new TextEdit();
        this.\u0019\u0004 = new LabelControl();
        this.\u0098\u0003 = new GroupControl();
        this.\u0099\u0003 = new LabelControl();
        this.\u009A\u0003 = new LabelControl();
        this.\u009B\u0003 = new LabelControl();
        this.\u009C\u0003 = new LabelControl();
        this.\u009D\u0003 = new LabelControl();
        this.\u009E\u0003 = new LabelControl();
        this.\u009F\u0003 = new TextEdit();
        this.\u0001\u0004 = new TextEdit();
        this.\u0002\u0004 = new TextEdit();
        this.\u0003\u0004 = new TrackBarControl();
        this.\u0004\u0004 = new TrackBarControl();
        this.\u0005\u0004 = new TrackBarControl();
        this.\u0092\u0003 = new TextEdit();
        this.\u0093\u0003 = new TextEdit();
        this.\u0094\u0003 = new TextEdit();
        this.\u0095\u0003 = new TextEdit();
        this.\u0096\u0003 = new TextEdit();
        this.\u0097\u0003 = new TextEdit();
        this.\u0091\u0003 = new LabelControl();
        this.\u001F\u0003 = new TextEdit();
        this.\u007F\u0003 = new LabelControl();
        this.\u0080\u0003 = new ButtonEdit();
        this.\u0081\u0003 = new TextEdit();
        this.\u0082\u0003 = new LabelControl();
        this.\u0083\u0003 = new ButtonEdit();
        this.\u0084\u0003 = new TextEdit();
        this.\u0086\u0003 = new LabelControl();
        this.\u0087\u0003 = new ButtonEdit();
        this.\u0088\u0003 = new TextEdit();
        this.\u0089\u0003 = new LabelControl();
        this.\u008A\u0003 = new ButtonEdit();
        this.\u008B\u0003 = new TextEdit();
        this.\u008C\u0003 = new LabelControl();
        this.\u008D\u0003 = new ButtonEdit();
        this.\u008E\u0003 = new TextEdit();
        this.\u008F\u0003 = new LabelControl();
        this.\u0090\u0003 = new ButtonEdit();
        this.\u0080 = new XtraTabPage();
        this.\u001D\u0005 = new GridControl();
        this.\u001E\u0005 = new GridView();
        this.\u001F\u0005 = new GridColumn();
        this.\u007F\u0005 = new RepositoryItemLookUpEdit();
        this.\u0080\u0005 = new GridColumn();
        this.\u0086\u0005 = new RepositoryItemTextEdit();
        this.\u0081\u0005 = new GridColumn();
        this.\u0082\u0005 = new GridColumn();
        this.\u0083\u0005 = new GridColumn();
        this.\u0084\u0005 = new GridColumn();
        this.\u0081 = new XtraTabPage();
        this.\u001E\u0004 = new TextEdit();
        this.\u001F\u0004 = new TextEdit();
        this.\u007F\u0004 = new SimpleButton();
        this.\u0080\u0004 = new LabelControl();
        this.\u0081\u0004 = new LabelControl();
        this.\u0082\u0004 = new LabelControl();
        this.\u0083\u0004 = new LabelControl();
        this.\u0084\u0004 = new TextEdit();
        this.\u0086\u0004 = new TextEdit();
        this.\u0087\u0004 = new TextEdit();
        this.\u0088\u0004 = new LookUpEdit();
        this.\u0089\u0004 = new LabelControl();
        this.\u008A\u0004 = new LabelControl();
        this.\u008B\u0004 = new LabelControl();
        this.\u008C\u0004 = new LabelControl();
        this.\u008D\u0004 = new TextEdit();
        this.\u008E\u0004 = new TextEdit();
        this.\u008F\u0004 = new TextEdit();
        this.\u0090\u0004 = new LookUpEdit();
        this.\u0091\u0004 = new LabelControl();
        this.\u0092\u0004 = new LabelControl();
        this.\u0093\u0004 = new LabelControl();
        this.\u0094\u0004 = new LabelControl();
        this.\u0095\u0004 = new TextEdit();
        this.\u0096\u0004 = new TextEdit();
        this.\u0097\u0004 = new TextEdit();
        this.\u0098\u0004 = new LookUpEdit();
        this.\u0099\u0004 = new LabelControl();
        this.\u009A\u0004 = new LabelControl();
        this.\u009B\u0004 = new LabelControl();
        this.\u009C\u0004 = new LabelControl();
        this.\u009D\u0004 = new TextEdit();
        this.\u009E\u0004 = new TextEdit();
        this.\u009F\u0004 = new TextEdit();
        this.\u0001\u0005 = new LookUpEdit();
        this.\u0002\u0005 = new LabelControl();
        this.\u0003\u0005 = new LabelControl();
        this.\u0004\u0005 = new LabelControl();
        this.\u0005\u0005 = new LabelControl();
        this.\u0006\u0005 = new TextEdit();
        this.\u0007\u0005 = new TextEdit();
        this.\u0008\u0005 = new TextEdit();
        this.\u000E\u0005 = new LookUpEdit();
        this.\u000F\u0005 = new LabelControl();
        this.\u0010\u0005 = new LabelControl();
        this.\u0011\u0005 = new LabelControl();
        this.\u0012\u0005 = new LabelControl();
        this.\u0013\u0005 = new TextEdit();
        this.\u0014\u0005 = new TextEdit();
        this.\u0015\u0005 = new TextEdit();
        this.\u0016\u0005 = new LookUpEdit();
        this.\u0006\u0004 = new DefaultLookAndFeel(this.\u0001);
        this.\u001B\u0005 = new PopupMenu(this.\u0001);
        this.\u001C\u0005 = new ToolTipController(this.\u0001);
        \u001B\u0006.\u007E\u009D\u0011((object) this.\u0002);
        \u001B\u0006.\u007E\u009D\u0011((object) this.\u0014);
        \u001B\u0006.\u007E\u009D\u0011((object) this.\u0016);
        \u001B\u0006.\u007E\u009A\u0010((object) this.\u0016);
        \u001B\u0006.\u007E\u009A\u0010((object) this.\u0017);
        \u001B\u0006.\u007E\u009D\u0011((object) this.\u000E\u0004);
        \u001B\u0006.\u007E\u009A\u0010((object) this.\u000E\u0004);
        \u001B\u0006.\u007E\u009D\u0011((object) \u001C\u0003.\u007E\u0003\u0004((object) this.\u001A\u0004));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u000F\u0004));
        \u001B\u0006.\u007E\u009D\u0011((object) \u001C\u0003.\u007E\u0003\u0004((object) this.\u001C\u0004));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0011\u0004));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0007\u0004));
        \u001B\u0006.\u007E\u009D\u0011((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u0017\u0003));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0011\u0003));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0082\u0006.\u007E\u008B\u0003((object) this.\u0008\u0002));
        \u001B\u0006.\u007E\u009D\u0011((object) this.\u0007\u0002);
        \u001B\u0006.\u007E\u009A\u0010((object) this.\u0007\u0002);
        \u001B\u0006.\u007E\u009D\u0011((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u000E\u0002));
        \u001B\u0006.\u007E\u009D\u0011((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u000F\u0002));
        \u001B\u0006.\u007E\u009D\u0011((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u0010\u0002));
        \u001B\u0006.\u007E\u009D\u0011((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u0011\u0002));
        \u001B\u0006.\u007E\u009D\u0011((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u0012\u0002));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0001\u0002));
        \u001B\u0006.\u007E\u009D\u0011((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u0003\u0002));
        \u001B\u0006.\u007E\u009D\u0011((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u0004\u0002));
        \u001B\u0006.\u007E\u009D\u0011((object) this.\u0097);
        \u001B\u0006.\u007E\u009A\u0010((object) this.\u0097);
        \u001B\u0006.\u007E\u009D\u0011((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u0098));
        \u001B\u0006.\u007E\u009D\u0011((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u0099));
        \u001B\u0006.\u007E\u009D\u0011((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u009A));
        \u001B\u0006.\u007E\u009D\u0011((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u009B));
        \u001B\u0006.\u007E\u009D\u0011((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u009C));
        \u001B\u0006.\u007E\u009D\u0011((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u009D));
        \u001B\u0006.\u007E\u009D\u0011((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u0094));
        \u001B\u0006.\u007E\u009D\u0011((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u0095));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u008D));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0084\u0002.\u007E\u0090\u0003((object) this.\u008E));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u008F));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0090));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0091));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0092));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0082));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0083));
        \u001B\u0006.\u007E\u009A\u0010((object) this.\u0018);
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0014\u0002));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0015\u0002));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0016\u0002));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0017\u0002));
        \u001B\u0006.\u007E\u009D\u0011((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u0018\u0002));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u001F\u0002));
        \u001B\u0006.\u007E\u009A\u0010((object) this.\u0019);
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0080\u0002));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0081\u0002));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0082\u0002));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0083\u0002));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u008A\u0002));
        \u001B\u0006.\u007E\u009A\u0010((object) this.\u001A);
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u008B\u0002));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u008C\u0002));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u008D\u0002));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0092\u0002));
        \u001B\u0006.\u007E\u009A\u0010((object) this.\u001B);
        \u001B\u0006.\u007E\u009D\u0011((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u001D\u0003));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0093\u0002));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0096\u0002));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0097\u0002));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u009A\u0002));
        \u001B\u0006.\u007E\u009A\u0010((object) this.\u001C);
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u009B\u0002));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u009C\u0002));
        \u001B\u0006.\u007E\u009A\u0010((object) this.\u001D);
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u009F\u0002));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0001\u0003));
        \u001B\u0006.\u007E\u009A\u0010((object) this.\u0018\u0003);
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0019\u0003));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u001A\u0003));
        \u001B\u0006.\u007E\u009A\u0010((object) this.\u001E);
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0004\u0003));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0005\u0003));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0006\u0003));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0007\u0003));
        \u001B\u0006.\u007E\u009A\u0010((object) this.\u007F);
        \u001B\u0006.\u007E\u009D\u0011((object) this.\u0013\u0004);
        \u001B\u0006.\u007E\u009A\u0010((object) this.\u0013\u0004);
        \u001B\u0006.\u007E\u009D\u0011((object) \u001C\u0003.\u007E\u0003\u0004((object) this.\u0014\u0004));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0016\u0004));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0018\u0004));
        \u001B\u0006.\u007E\u009D\u0011((object) this.\u0098\u0003);
        \u001B\u0006.\u007E\u009A\u0010((object) this.\u0098\u0003);
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u009F\u0003));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0001\u0004));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0002\u0004));
        \u001B\u0006.\u007E\u009D\u0011((object) this.\u0003\u0004);
        \u001B\u0006.\u007E\u009D\u0011((object) \u0091\u0006.\u007E\u009D\u0003((object) this.\u0003\u0004));
        \u001B\u0006.\u007E\u009D\u0011((object) this.\u0004\u0004);
        \u001B\u0006.\u007E\u009D\u0011((object) \u0091\u0006.\u007E\u009D\u0003((object) this.\u0004\u0004));
        \u001B\u0006.\u007E\u009D\u0011((object) this.\u0005\u0004);
        \u001B\u0006.\u007E\u009D\u0011((object) \u0091\u0006.\u007E\u009D\u0003((object) this.\u0005\u0004));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0092\u0003));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0093\u0003));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0094\u0003));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0095\u0003));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0096\u0003));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0097\u0003));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u001F\u0003));
        \u001B\u0006.\u007E\u009D\u0011((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u0080\u0003));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0081\u0003));
        \u001B\u0006.\u007E\u009D\u0011((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u0083\u0003));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0084\u0003));
        \u001B\u0006.\u007E\u009D\u0011((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u0087\u0003));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0088\u0003));
        \u001B\u0006.\u007E\u009D\u0011((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u008A\u0003));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u008B\u0003));
        \u001B\u0006.\u007E\u009D\u0011((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u008D\u0003));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u008E\u0003));
        \u001B\u0006.\u007E\u009D\u0011((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u0090\u0003));
        \u001B\u0006.\u007E\u009A\u0010((object) this.\u0080);
        \u001B\u0006.\u007E\u009D\u0011((object) this.\u001D\u0005);
        \u001B\u0006.\u007E\u009D\u0011((object) this.\u001E\u0005);
        \u001B\u0006.\u007E\u009D\u0011((object) this.\u007F\u0005);
        \u001B\u0006.\u007E\u009D\u0011((object) this.\u0086\u0005);
        \u001B\u0006.\u007E\u009A\u0010((object) this.\u0081);
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u001E\u0004));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u001F\u0004));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0084\u0004));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0086\u0004));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0087\u0004));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0088\u0004));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u008D\u0004));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u008E\u0004));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u008F\u0004));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0090\u0004));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0095\u0004));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0096\u0004));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0097\u0004));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0098\u0004));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u009D\u0004));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u009E\u0004));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u009F\u0004));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0001\u0005));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0006\u0005));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0007\u0005));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0008\u0005));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u000E\u0005));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0013\u0005));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0014\u0005));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0015\u0005));
        \u001B\u0006.\u007E\u009D\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0016\u0005));
        \u001B\u0006.\u007E\u009D\u0011((object) this.\u001B\u0005);
        \u001B\u0006.\u000F\u0004((object) this);
        \u001B\u0004 obj1 = \u001B\u0004.\u007E\u0011\u0002;
        Bars bars = \u0092\u0003.\u007E\u001D\u0002((object) this.\u0002);
        barArray1 = new Bar[3]
        {
          this.\u0003,
          this.\u0004,
          this.\u0013\u0003
        };
        Bar[] barArray2 = barArray1;
        obj1((object) bars, barArray2);
        int num1 = \u0090\u0002.\u007E\u0090\u0002((object) \u009B\u0003.\u007E\u001C\u0002((object) this.\u0002), this.\u0005);
        int num2 = \u0090\u0002.\u007E\u0090\u0002((object) \u009B\u0003.\u007E\u001C\u0002((object) this.\u0002), this.\u0006);
        int num3 = \u0090\u0002.\u007E\u0090\u0002((object) \u009B\u0003.\u007E\u001C\u0002((object) this.\u0002), this.\u0007);
        int num4 = \u0090\u0002.\u007E\u0090\u0002((object) \u009B\u0003.\u007E\u001C\u0002((object) this.\u0002), this.\u0008);
        \u0007\u0004.\u007E\u001E\u0002((object) this.\u0002, (Control) this);
        \u001F\u0003 obj2 = \u001F\u0003.\u007E\u0097\u0002;
        BarItems barItems = \u001E\u0006.\u007E\u001F\u0002((object) this.\u0002);
        barItemArray1 = new BarItem[14]
        {
          (BarItem) this.\u000E,
          (BarItem) this.\u000F,
          (BarItem) this.\u0010,
          (BarItem) this.\u0011,
          (BarItem) this.\u0012,
          (BarItem) this.\u0013,
          (BarItem) this.\u0015,
          (BarItem) this.\u0014\u0003,
          (BarItem) this.\u0015\u0003,
          (BarItem) this.\u0016\u0003,
          (BarItem) this.\u0017\u0005,
          (BarItem) this.\u0018\u0005,
          (BarItem) this.\u0019\u0005,
          (BarItem) this.\u001A\u0005
        };
        BarItem[] barItemArray2 = barItemArray1;
        obj2((object) barItems, barItemArray2);
        \u0005\u0006.\u007E\u007F\u0002((object) this.\u0002, this.\u0003);
        \u0011\u0004.\u007E\u001B\u0002((object) this.\u0002, 15);
        \u008F\u0002 obj3 = \u008F\u0002.\u007E\u009E\u0003;
        RepositoryItemCollection repositoryItemCollection1 = \u0082\u0004.\u007E\u0086\u0003((object) this.\u0002);
        repositoryItemArray1 = new RepositoryItem[1]
        {
          (RepositoryItem) this.\u0014
        };
        RepositoryItem[] repositoryItemArray2 = repositoryItemArray1;
        obj3((object) repositoryItemCollection1, repositoryItemArray2);
        \u0005\u0006.\u007E\u0080\u0002((object) this.\u0002, this.\u0004);
        \u001C\u0005.\u007E\u0005\u0002((object) this.\u0003, \u009C.\u0001(8788));
        \u0011\u0004.\u007E\u0010\u0002((object) this.\u0003, 0);
        \u0011\u0004.\u007E\u000F\u0002((object) this.\u0003, 1);
        \u0001\u0007.\u007E\u0008\u0002((object) this.\u0003, BarDockStyle.Top);
        \u001A\u0005.\u007E\u0004\u0002((object) this.\u0003, new Point(842, 291));
        \u009D\u0002 obj4 = \u009D\u0002.\u007E\u009A\u0002;
        LinksInfo linksInfo1 = \u0008\u0005.\u007E\u000E\u0002((object) this.\u0003);
        linkPersistInfoArray1 = new LinkPersistInfo[7]
        {
          new LinkPersistInfo((BarItem) this.\u000E),
          new LinkPersistInfo((BarItem) this.\u000F),
          new LinkPersistInfo((BarItem) this.\u0010),
          new LinkPersistInfo((BarItem) this.\u0011),
          new LinkPersistInfo((BarItem) this.\u0012),
          new LinkPersistInfo((BarItem) this.\u0015),
          new LinkPersistInfo(BarLinkUserDefines.Width, (BarItem) this.\u0013, \u009C.\u0001(8361), false, true, true, 82)
        };
        LinkPersistInfo[] linkPersistInfoArray2 = linkPersistInfoArray1;
        obj4((object) linksInfo1, linkPersistInfoArray2);
        \u009C\u0006.\u007E\u0013\u0002((object) \u0017\u0004.\u007E\u0003\u0002((object) this.\u0003), false);
        \u009C\u0006.\u007E\u0014\u0002((object) \u0017\u0004.\u007E\u0003\u0002((object) this.\u0003), true);
        \u009C\u0006.\u007E\u0015\u0002((object) \u0017\u0004.\u007E\u0003\u0002((object) this.\u0003), true);
        \u009C\u0006.\u007E\u0017\u0002((object) \u0017\u0004.\u007E\u0003\u0002((object) this.\u0003), true);
        \u009C\u0006.\u007E\u0018\u0002((object) \u0017\u0004.\u007E\u0003\u0002((object) this.\u0003), true);
        \u001C\u0005.\u007E\u0006\u0002((object) this.\u0003, \u009C.\u0001(8788));
        \u001C\u0005.\u007E\u0088\u0002((object) this.\u000E, \u009C.\u0001(8801));
        \u009C\u0006.\u007E\u0084\u0002((object) this.\u000E, false);
        // ISSUE: reference to a compiler-generated method
        \u0097\u0004.\u007E\u0086\u0002((object) this.\u000E, (Image) \u001A\u0002.\u0099\u0005());
        \u0011\u0004.\u007E\u0082\u0002((object) this.\u000E, 0);
        \u001C\u0005.\u007E\u0081\u0002((object) this.\u000E, \u009C.\u0001(8801));
        \u001C\u0005.\u007E\u0088\u0002((object) this.\u000F, \u009C.\u0001(8810));
        \u009C\u0006.\u007E\u0084\u0002((object) this.\u000F, false);
        // ISSUE: reference to a compiler-generated method
        \u0097\u0004.\u007E\u0086\u0002((object) this.\u000F, (Image) \u001A\u0002.\u009B\u0006());
        \u0011\u0004.\u007E\u0082\u0002((object) this.\u000F, 1);
        \u001C\u0005.\u007E\u0081\u0002((object) this.\u000F, \u009C.\u0001(8823));
        \u001C\u0005.\u007E\u0088\u0002((object) this.\u0010, \u009C.\u0001(8836));
        // ISSUE: reference to a compiler-generated method
        \u0097\u0004.\u007E\u0086\u0002((object) this.\u0010, (Image) \u001A\u0002.\u009A\u0005());
        \u0011\u0004.\u007E\u0082\u0002((object) this.\u0010, 2);
        \u001C\u0005.\u007E\u0081\u0002((object) this.\u0010, \u009C.\u0001(8836));
        \u0017\u0007.\u007E\u0089\u0002((object) this.\u0010, new ItemClickEventHandler(this.\u0018\u0006));
        \u001C\u0005.\u007E\u0088\u0002((object) this.\u0011, \u009C.\u0001(8845));
        // ISSUE: reference to a compiler-generated method
        \u0097\u0004.\u007E\u0086\u0002((object) this.\u0011, (Image) \u001A\u0002.\u009D\u0006());
        \u0011\u0004.\u007E\u0082\u0002((object) this.\u0011, 3);
        \u001C\u0005.\u007E\u0081\u0002((object) this.\u0011, \u009C.\u0001(8845));
        \u0017\u0007.\u007E\u0089\u0002((object) this.\u0011, new ItemClickEventHandler(this.\u009A\u0003));
        \u001C\u0005.\u007E\u0088\u0002((object) this.\u0012, \u009C.\u0001(8858));
        // ISSUE: reference to a compiler-generated method
        \u0097\u0004.\u007E\u0086\u0002((object) this.\u0012, (Image) \u001A\u0002.\u0005\u0006());
        \u0011\u0004.\u007E\u0082\u0002((object) this.\u0012, 4);
        \u001C\u0005.\u007E\u0081\u0002((object) this.\u0012, \u009C.\u0001(8867));
        \u0017\u0007.\u007E\u0089\u0002((object) this.\u0012, new ItemClickEventHandler(this.\u0019\u0006));
        \u007F\u0005.\u007E\u0083\u0002((object) this.\u0015, BorderStyles.NoBorder);
        \u001C\u0005.\u007E\u0088\u0002((object) this.\u0015, \u009C.\u0001(8880));
        \u0011\u0004.\u007E\u0082\u0002((object) this.\u0015, 6);
        \u001C\u0005.\u007E\u0081\u0002((object) this.\u0015, \u009C.\u0001(8893));
        \u0084\u0006.\u007E\u0099\u0002((object) this.\u0015, StringAlignment.Near);
        \u001C\u0005.\u007E\u0088\u0002((object) this.\u0013, \u009C.\u0001(8914));
        \u0081\u0002.\u007E\u008E\u0002((object) this.\u0013, (RepositoryItem) this.\u0014);
        \u0011\u0004.\u007E\u0082\u0002((object) this.\u0013, 5);
        \u001C\u0005.\u007E\u0081\u0002((object) this.\u0013, \u009C.\u0001(8927));
        \u001D\u0005.\u007E\u008F\u0002((object) this.\u0013, new EventHandler(this.\u0015\u0006));
        \u009C\u0006.\u007E\u0004\u0003((object) this.\u0014, false);
        \u001C\u0005.\u007E\u0002\u0003((object) this.\u0014, \u009C.\u0001(8948));
        \u001C\u0005.\u007E\u0005\u0002((object) this.\u0004, \u009C.\u0001(8981));
        \u0081\u0004.\u007E\u0007\u0002((object) this.\u0004, BarCanDockStyle.Bottom);
        \u0011\u0004.\u007E\u0010\u0002((object) this.\u0004, 0);
        \u0011\u0004.\u007E\u000F\u0002((object) this.\u0004, 0);
        \u0001\u0007.\u007E\u0008\u0002((object) this.\u0004, BarDockStyle.Bottom);
        \u009C\u0006.\u007E\u0013\u0002((object) \u0017\u0004.\u007E\u0003\u0002((object) this.\u0004), false);
        \u009C\u0006.\u007E\u0016\u0002((object) \u0017\u0004.\u007E\u0003\u0002((object) this.\u0004), false);
        \u009C\u0006.\u007E\u0018\u0002((object) \u0017\u0004.\u007E\u0003\u0002((object) this.\u0004), true);
        \u001C\u0005.\u007E\u0006\u0002((object) this.\u0004, \u009C.\u0001(8981));
        \u001C\u0005.\u007E\u0005\u0002((object) this.\u0013\u0003, \u009C.\u0001(8998));
        \u0011\u0004.\u007E\u0010\u0002((object) this.\u0013\u0003, 0);
        \u0011\u0004.\u007E\u000F\u0002((object) this.\u0013\u0003, 0);
        \u0001\u0007.\u007E\u0008\u0002((object) this.\u0013\u0003, BarDockStyle.Top);
        \u009D\u0002 obj5 = \u009D\u0002.\u007E\u009A\u0002;
        LinksInfo linksInfo2 = \u0008\u0005.\u007E\u000E\u0002((object) this.\u0013\u0003);
        linkPersistInfoArray1 = new LinkPersistInfo[3]
        {
          new LinkPersistInfo((BarItem) this.\u0014\u0003),
          new LinkPersistInfo((BarItem) this.\u0015\u0003),
          new LinkPersistInfo((BarItem) this.\u0016\u0003)
        };
        LinkPersistInfo[] linkPersistInfoArray3 = linkPersistInfoArray1;
        obj5((object) linksInfo2, linkPersistInfoArray3);
        \u009C\u0006.\u007E\u0013\u0002((object) \u0017\u0004.\u007E\u0003\u0002((object) this.\u0013\u0003), false);
        \u009C\u0006.\u007E\u0012\u0002((object) \u0017\u0004.\u007E\u0003\u0002((object) this.\u0013\u0003), true);
        \u009C\u0006.\u007E\u0014\u0002((object) \u0017\u0004.\u007E\u0003\u0002((object) this.\u0013\u0003), true);
        \u009C\u0006.\u007E\u0018\u0002((object) \u0017\u0004.\u007E\u0003\u0002((object) this.\u0013\u0003), true);
        \u001C\u0005.\u007E\u0006\u0002((object) this.\u0013\u0003, \u009C.\u0001(8998));
        \u001C\u0005.\u007E\u0088\u0002((object) this.\u0014\u0003, \u009C.\u0001(9011));
        \u0011\u0004.\u007E\u0082\u0002((object) this.\u0014\u0003, 7);
        \u001C\u0005.\u007E\u0081\u0002((object) this.\u0014\u0003, \u009C.\u0001(9020));
        \u001C\u0005.\u007E\u0088\u0002((object) this.\u0015\u0003, \u009C.\u0001(9037));
        \u0011\u0004.\u007E\u0082\u0002((object) this.\u0015\u0003, 8);
        \u001C\u0005.\u007E\u0081\u0002((object) this.\u0015\u0003, \u009C.\u0001(9046));
        \u001C\u0005.\u007E\u0088\u0002((object) this.\u0016\u0003, \u009C.\u0001(9063));
        \u0011\u0004.\u007E\u0082\u0002((object) this.\u0016\u0003, 9);
        \u001C\u0005.\u007E\u0081\u0002((object) this.\u0016\u0003, \u009C.\u0001(9076));
        \u001C\u0005.\u007E\u0088\u0002((object) this.\u0017\u0005, \u009C.\u0001(9093));
        \u001C\u0005.\u007E\u0087\u0002((object) this.\u0017\u0005, \u009C.\u0001(9093));
        // ISSUE: reference to a compiler-generated method
        \u0097\u0004.\u007E\u0086\u0002((object) this.\u0017\u0005, (Image) \u001A\u0002.\u0099\u0005());
        \u0011\u0004.\u007E\u0082\u0002((object) this.\u0017\u0005, 11);
        \u001C\u0005.\u007E\u0081\u0002((object) this.\u0017\u0005, \u009C.\u0001(9110));
        \u001C\u0005.\u007E\u0088\u0002((object) this.\u0018\u0005, \u009C.\u0001(9127));
        \u001C\u0005.\u007E\u0087\u0002((object) this.\u0018\u0005, \u009C.\u0001(9127));
        // ISSUE: reference to a compiler-generated method
        \u0097\u0004.\u007E\u0086\u0002((object) this.\u0018\u0005, (Image) \u001A\u0002.\u009B\u0006());
        \u0011\u0004.\u007E\u0082\u0002((object) this.\u0018\u0005, 12);
        \u001C\u0005.\u007E\u0081\u0002((object) this.\u0018\u0005, \u009C.\u0001(9148));
        \u001C\u0005.\u007E\u0088\u0002((object) this.\u0019\u0005, \u009C.\u0001(9127));
        \u0011\u0004.\u007E\u0082\u0002((object) this.\u0019\u0005, 13);
        \u001C\u0005.\u007E\u0081\u0002((object) this.\u0019\u0005, \u009C.\u0001(9169));
        \u001C\u0005.\u007E\u0088\u0002((object) this.\u001A\u0005, \u009C.\u0001(9093));
        \u0011\u0004.\u007E\u0082\u0002((object) this.\u001A\u0005, 14);
        \u001C\u0005.\u007E\u0081\u0002((object) this.\u001A\u0005, \u009C.\u0001(9190));
        \u0012\u0004.\u007E\u0008\u0010((object) this.\u0016, DockStyle.Fill);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0016, new Point(0, 67));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0016, \u009C.\u0001(9211));
        global::\u000E\u0006.\u007E\u008D\u0003((object) this.\u0016, this.\u0017);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0016, new Size(710, 540));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0016, 4);
        \u0001\u0003 obj6 = \u0001\u0003.\u007E\u0007\u0004;
        XtraTabPageCollection tabPageCollection = \u0010\u0004.\u007E\u008E\u0003((object) this.\u0016);
        xtraTabPageArray1 = new XtraTabPage[13]
        {
          this.\u0017,
          this.\u0018,
          this.\u0019,
          this.\u001A,
          this.\u001B,
          this.\u001C,
          this.\u001D,
          this.\u0018\u0003,
          this.\u001E,
          this.\u001F,
          this.\u007F,
          this.\u0080,
          this.\u0081
        };
        XtraTabPage[] xtraTabPageArray2 = xtraTabPageArray1;
        obj6((object) tabPageCollection, xtraTabPageArray2);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u000E\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u0017\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u0011\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u0012\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u0013\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u0008\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u0007\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u0001\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u0002\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u0003\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u0004\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u0005\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u0006\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u0097);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u0094);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u0095);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u0096);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u0093);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u008D);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u008E);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u008F);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u0090);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u0091);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u0092);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u0082);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u0083);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u0084);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u0086);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u0087);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u0088);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u0089);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u008A);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u008B);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0017), (Control) this.\u008C);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0017, \u009C.\u0001(9220));
        \u0087\u0006.\u007E\u0006\u0004((object) this.\u0017, new Size(706, 513));
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0017, \u009C.\u0001(9229));
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u000E\u0004), (Control) this.\u001A\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u000E\u0004), (Control) this.\u000F\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u000E\u0004), (Control) this.\u001B\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u000E\u0004), (Control) this.\u0010\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u000E\u0004), (Control) this.\u001C\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u000E\u0004), (Control) this.\u001D\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u000E\u0004), (Control) this.\u0011\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u000E\u0004), (Control) this.\u0012\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u000E\u0004), (Control) this.\u0007\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u000E\u0004), (Control) this.\u0008\u0004);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u000E\u0004, new Point(385, 15));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u000E\u0004, \u009C.\u0001(9238));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u000E\u0004, new Size(311, 111));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u000E\u0004, 35);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u000E\u0004, \u009C.\u0001(9259));
        \u0080\u0002 obj7 = \u0080\u0002.\u007E\u0012\u0003;
        SpinEdit spinEdit1 = this.\u001A\u0004;
        bits = new int[4];
        // ISSUE: variable of a boxed type
        __Boxed<Decimal> local1 = (ValueType) new Decimal(bits);
        obj7((object) spinEdit1, (object) local1);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u001A\u0004, new Point(224, 51));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u001A\u0004, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u001A\u0004, \u009C.\u0001(9284));
        \u0005\u0005 obj8 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection1 = \u0091\u0004.\u007E\u000F\u0003((object) \u001C\u0003.\u007E\u0003\u0004((object) this.\u001A\u0004));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton()
        };
        EditorButton[] editorButtonArray2 = editorButtonArray1;
        obj8((object) buttonCollection1, editorButtonArray2);
        \u009C\u0006.\u007E\u0001\u0004((object) \u001C\u0003.\u007E\u0003\u0004((object) this.\u001A\u0004), false);
        \u001C\u0005.\u007E\u0089\u0003((object) \u0080\u0003.\u007E\u0008\u0003((object) \u001C\u0003.\u007E\u0003\u0004((object) this.\u001A\u0004)), \u009C.\u0001(9297));
        \u009C\u0002 obj9 = \u009C\u0002.\u007E\u009F\u0003;
        RepositoryItemSpinEdit repositoryItemSpinEdit1 = \u001C\u0003.\u007E\u0003\u0004((object) this.\u001A\u0004);
        bits = new int[4]{ 200, 0, 0, 0 };
        Decimal num5 = new Decimal(bits);
        obj9((object) repositoryItemSpinEdit1, num5);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u001A\u0004, new Size(80, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u001A\u0004, 44);
        \u001D\u0005.\u007E\u0013\u0003((object) this.\u001A\u0004, new EventHandler(this.\u0091\u0006));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u000F\u0004, new Point(63, 77));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u000F\u0004, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u000F\u0004, \u009C.\u0001(9302));
        \u0012\u0007.\u007E\u0014\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u000F\u0004)), \u008D\u0004.\u0014\u000F());
        \u009C\u0006.\u007E\u0017\u0004((object) global::\u0010\u0006.\u007E\u0012\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u000F\u0004))), true);
        \u009C\u0006.\u007E\u0005\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u000F\u0004), true);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u000F\u0004, new Size(97, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u000F\u0004, 38);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u001B\u0004, new Point(173, 54));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u001B\u0004, \u009C.\u0001(9319));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u001B\u0004, new Size(45, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u001B\u0004, 43);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u001B\u0004, \u009C.\u0001(9340));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0010\u0004, new Point(11, 80));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0010\u0004, \u009C.\u0001(9353));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0010\u0004, new Size(46, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0010\u0004, 37);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0010\u0004, \u009C.\u0001(9374));
        \u0080\u0002 obj10 = \u0080\u0002.\u007E\u0012\u0003;
        SpinEdit spinEdit2 = this.\u001C\u0004;
        bits = new int[4];
        // ISSUE: variable of a boxed type
        __Boxed<Decimal> local2 = (ValueType) new Decimal(bits);
        obj10((object) spinEdit2, (object) local2);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u001C\u0004, new Point(224, 25));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u001C\u0004, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u001C\u0004, \u009C.\u0001(9391));
        \u0005\u0005 obj11 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection2 = \u0091\u0004.\u007E\u000F\u0003((object) \u001C\u0003.\u007E\u0003\u0004((object) this.\u001C\u0004));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton()
        };
        EditorButton[] editorButtonArray3 = editorButtonArray1;
        obj11((object) buttonCollection2, editorButtonArray3);
        \u009C\u0006.\u007E\u0001\u0004((object) \u001C\u0003.\u007E\u0003\u0004((object) this.\u001C\u0004), false);
        \u001C\u0005.\u007E\u0089\u0003((object) \u0080\u0003.\u007E\u0008\u0003((object) \u001C\u0003.\u007E\u0003\u0004((object) this.\u001C\u0004)), \u009C.\u0001(9297));
        \u009C\u0002 obj12 = \u009C\u0002.\u007E\u009F\u0003;
        RepositoryItemSpinEdit repositoryItemSpinEdit2 = \u001C\u0003.\u007E\u0003\u0004((object) this.\u001C\u0004);
        bits = new int[4]{ 200, 0, 0, 0 };
        Decimal num6 = new Decimal(bits);
        obj12((object) repositoryItemSpinEdit2, num6);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u001C\u0004, new Size(80, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u001C\u0004, 42);
        \u001D\u0005.\u007E\u0013\u0003((object) this.\u001C\u0004, new EventHandler(this.\u0092\u0006));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u001D\u0004, new Point(175, 28));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u001D\u0004, \u009C.\u0001(9404));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u001D\u0004, new Size(43, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u001D\u0004, 41);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u001D\u0004, \u009C.\u0001(9425));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0011\u0004, new Point(63, 51));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0011\u0004, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0011\u0004, \u009C.\u0001(9438));
        \u0012\u0007.\u007E\u0014\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0011\u0004)), \u008D\u0004.\u0014\u000F());
        \u009C\u0006.\u007E\u0017\u0004((object) global::\u0010\u0006.\u007E\u0012\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0011\u0004))), true);
        \u009C\u0006.\u007E\u0005\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0011\u0004), true);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0011\u0004, new Size(97, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0011\u0004, 36);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0012\u0004, new Point(9, 54));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0012\u0004, \u009C.\u0001(9451));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0012\u0004, new Size(48, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0012\u0004, 35);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0012\u0004, \u009C.\u0001(9472));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0007\u0004, new Point(63, 25));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0007\u0004, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0007\u0004, \u009C.\u0001(9489));
        \u0012\u0007.\u007E\u0014\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0007\u0004)), \u008D\u0004.\u0014\u000F());
        \u009C\u0006.\u007E\u0017\u0004((object) global::\u0010\u0006.\u007E\u0012\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0007\u0004))), true);
        \u009C\u0006.\u007E\u0005\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0007\u0004), true);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0007\u0004, new Size(97, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0007\u0004, 34);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0008\u0004, new Point(7, 28));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0008\u0004, \u009C.\u0001(9506));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0008\u0004, new Size(50, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0008\u0004, 33);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0008\u0004, \u009C.\u0001(9527));
        \u0080\u0002.\u007E\u0012\u0003((object) this.\u0017\u0003, (object) (byte) 0);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0017\u0003, new Point(277, 274));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0017\u0003, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0017\u0003, \u009C.\u0001(9544));
        \u001C\u0005.\u007E\u001E\u0003((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u0017\u0003), \u009C.\u0001(9557));
        \u0080\u0002.\u007E\u001F\u0003((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u0017\u0003), (object) (byte) 1);
        \u0080\u0002.\u007E\u007F\u0003((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u0017\u0003), (object) (byte) 0);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0017\u0003, new Size(96, 18));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0017\u0003, 32);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0011\u0003, new Point(249, 12));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0011\u0003, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0011\u0003, \u009C.\u0001(9574));
        \u0005\u0005 obj13 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection3 = \u0091\u0004.\u007E\u000F\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0011\u0003));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray4 = editorButtonArray1;
        obj13((object) buttonCollection3, editorButtonArray4);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0011\u0003, new Size(130, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0011\u0003, 31);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0012\u0003, new Point(208, 15));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0012\u0003, \u009C.\u0001(9587));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0012\u0003, new Size(35, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0012\u0003, 30);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0012\u0003, \u009C.\u0001(9608));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0013\u0002, new Point(36, 391));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0013\u0002, \u009C.\u0001(9621));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0013\u0002, new Size(57, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0013\u0002, 29);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0013\u0002, \u009C.\u0001(9642));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0008\u0002, new Point(35, 409));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0008\u0002, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0008\u0002, \u009C.\u0001(9659));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0008\u0002, new Size(476, 95));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0008\u0002, 28);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0007\u0002), (Control) this.\u000E\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0007\u0002), (Control) this.\u000F\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0007\u0002), (Control) this.\u0010\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0007\u0002), (Control) this.\u0011\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0007\u0002), (Control) this.\u0012\u0002);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0007\u0002, new Point(385, 132));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0007\u0002, \u009C.\u0001(9668));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0007\u0002, new Size(126, 149));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0007\u0002, 27);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0007\u0002, \u009C.\u0001(9689));
        \u0080\u0002.\u007E\u0012\u0003((object) this.\u000E\u0002, (object) (byte) 0);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u000E\u0002, new Point(6, 126));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u000E\u0002, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u000E\u0002, \u009C.\u0001(9714));
        \u001C\u0005.\u007E\u001E\u0003((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u000E\u0002), \u009C.\u0001(9727));
        \u0080\u0002.\u007E\u001F\u0003((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u000E\u0002), (object) (byte) 1);
        \u0080\u0002.\u007E\u007F\u0003((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u000E\u0002), (object) (byte) 0);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u000E\u0002, new Size(75, 18));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u000E\u0002, 4);
        \u0080\u0002.\u007E\u0012\u0003((object) this.\u000F\u0002, (object) (byte) 0);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u000F\u0002, new Point(6, 101));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u000F\u0002, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u000F\u0002, \u009C.\u0001(9736));
        \u001C\u0005.\u007E\u001E\u0003((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u000F\u0002), \u009C.\u0001(9753));
        \u0080\u0002.\u007E\u001F\u0003((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u000F\u0002), (object) (byte) 1);
        \u0080\u0002.\u007E\u007F\u0003((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u000F\u0002), (object) (byte) 0);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u000F\u0002, new Size(123, 18));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u000F\u0002, 3);
        \u0080\u0002.\u007E\u0012\u0003((object) this.\u0010\u0002, (object) (byte) 0);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0010\u0002, new Point(5, 75));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0010\u0002, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0010\u0002, \u009C.\u0001(9770));
        \u001C\u0005.\u007E\u001E\u0003((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u0010\u0002), \u009C.\u0001(9783));
        \u0080\u0002.\u007E\u001F\u0003((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u0010\u0002), (object) (byte) 1);
        \u0080\u0002.\u007E\u007F\u0003((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u0010\u0002), (object) (byte) 0);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0010\u0002, new Size(124, 18));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0010\u0002, 2);
        \u001D\u0005.\u007E\u0013\u0003((object) this.\u0010\u0002, new EventHandler(this.\u008E\u0006));
        \u0080\u0002.\u007E\u0012\u0003((object) this.\u0011\u0002, (object) (byte) 0);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0011\u0002, new Point(5, 50));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0011\u0002, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0011\u0002, \u009C.\u0001(9808));
        \u001C\u0005.\u007E\u001E\u0003((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u0011\u0002), \u009C.\u0001(9829));
        \u0080\u0002.\u007E\u001F\u0003((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u0011\u0002), (object) (byte) 1);
        \u0080\u0002.\u007E\u007F\u0003((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u0011\u0002), (object) (byte) 0);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0011\u0002, new Size(124, 18));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0011\u0002, 1);
        \u0080\u0002.\u007E\u0012\u0003((object) this.\u0012\u0002, (object) (byte) 0);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0012\u0002, new Point(5, 25));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0012\u0002, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0012\u0002, \u009C.\u0001(9850));
        \u001C\u0005.\u007E\u001E\u0003((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u0012\u0002), \u009C.\u0001(9863));
        \u0080\u0002.\u007E\u001F\u0003((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u0012\u0002), (object) (byte) 1);
        \u0080\u0002.\u007E\u007F\u0003((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u0012\u0002), (object) (byte) 0);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0012\u0002, new Size(124, 18));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0012\u0002, 0);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0001\u0002, new Point(106, 222));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0001\u0002, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0001\u0002, \u009C.\u0001(9884));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0001\u0002, new Size(100, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0001\u0002, 26);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0002\u0002, new Point(73, 224));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0002\u0002, \u009C.\u0001(9893));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0002\u0002, new Size(27, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0002\u0002, 25);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0002\u0002, \u009C.\u0001(9914));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0003\u0002, new Point(279, 248));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0003\u0002, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0003\u0002, \u009C.\u0001(9923));
        \u0005\u0005 obj14 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection4 = \u0091\u0004.\u007E\u000F\u0003((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u0003\u0002));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton()
        };
        EditorButton[] editorButtonArray5 = editorButtonArray1;
        obj14((object) buttonCollection4, editorButtonArray5);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0003\u0002, new Size(100, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0003\u0002, 24);
        \u009C\u0006.\u007E\u000E\u0010((object) this.\u0004\u0002, false);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0004\u0002, new Point(279, 221));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0004\u0002, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0004\u0002, \u009C.\u0001(9944));
        \u0005\u0005 obj15 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection5 = \u0091\u0004.\u007E\u000F\u0003((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u0004\u0002));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton()
        };
        EditorButton[] editorButtonArray6 = editorButtonArray1;
        obj15((object) buttonCollection5, editorButtonArray6);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0004\u0002, new Size(100, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0004\u0002, 23);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0005\u0002, new Point(195, 251));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0005\u0002, \u009C.\u0001(9957));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0005\u0002, new Size(78, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0005\u0002, 22);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0005\u0002, \u009C.\u0001(9978));
        \u009C\u0006.\u007E\u000E\u0010((object) this.\u0006\u0002, false);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0006\u0002, new Point(221, 224));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0006\u0002, \u009C.\u0001(10003));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0006\u0002, new Size(52, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0006\u0002, 21);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0006\u0002, \u009C.\u0001(10024));
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0097), (Control) this.\u009E);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0097), (Control) this.\u009F);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0097), (Control) this.\u0098);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0097), (Control) this.\u0099);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0097), (Control) this.\u009A);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0097), (Control) this.\u009B);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0097), (Control) this.\u009C);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0097), (Control) this.\u009D);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0097, new Point(36, 300));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0097, \u009C.\u0001(10041));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0097, new Size(475, 85));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0097, 20);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0097, \u009C.\u0001(10062));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u009E, new Point(278, 55));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u009E, \u009C.\u0001(10079));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u009E, new Size(42, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u009E, 21);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u009E, \u009C.\u0001(10100));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u009F, new Point(278, 28));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u009F, \u009C.\u0001(10113));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u009F, new Size(42, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u009F, 6);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u009F, \u009C.\u0001(10100));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0098, new Point(170, 52));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0098, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0098, \u009C.\u0001(10126));
        \u001C\u0005.\u007E\u001E\u0003((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u0098), \u009C.\u0001(10139));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0098, new Size(75, 18));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0098, 5);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0099, new Point(86, 52));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0099, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0099, \u009C.\u0001(10152));
        \u001C\u0005.\u007E\u001E\u0003((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u0099), \u009C.\u0001(10165));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0099, new Size(75, 18));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0099, 4);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u009A, new Point(5, 52));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u009A, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u009A, \u009C.\u0001(10174));
        \u001C\u0005.\u007E\u001E\u0003((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u009A), \u009C.\u0001(10187));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u009A, new Size(75, 18));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u009A, 3);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u009B, new Point(170, 25));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u009B, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u009B, \u009C.\u0001(10200));
        \u001C\u0005.\u007E\u001E\u0003((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u009B), \u009C.\u0001(10213));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u009B, new Size(75, 18));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u009B, 2);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u009C, new Point(86, 25));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u009C, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u009C, \u009C.\u0001(10222));
        \u001C\u0005.\u007E\u001E\u0003((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u009C), \u009C.\u0001(10235));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u009C, new Size(75, 18));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u009C, 1);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u009D, new Point(5, 25));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u009D, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u009D, \u009C.\u0001(10248));
        \u001C\u0005.\u007E\u001E\u0003((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u009D), \u009C.\u0001(10261));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u009D, new Size(75, 18));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u009D, 0);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0094, new Point(279, 195));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0094, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0094, \u009C.\u0001(10270));
        \u0005\u0005 obj16 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection6 = \u0091\u0004.\u007E\u000F\u0003((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u0094));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton()
        };
        EditorButton[] editorButtonArray7 = editorButtonArray1;
        obj16((object) buttonCollection6, editorButtonArray7);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0094, new Size(100, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0094, 19);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0095, new Point(279, 169));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0095, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0095, \u009C.\u0001(10283));
        \u0005\u0005 obj17 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection7 = \u0091\u0004.\u007E\u000F\u0003((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u0095));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton()
        };
        EditorButton[] editorButtonArray8 = editorButtonArray1;
        obj17((object) buttonCollection7, editorButtonArray8);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0095, new Size(100, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0095, 18);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0096, new Point(226, 199));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0096, \u009C.\u0001(10296));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0096, new Size(47, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0096, 17);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0096, \u009C.\u0001(10317));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0093, new Point(226, 172));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0093, \u009C.\u0001(10330));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0093, new Size(47, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0093, 16);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0093, \u009C.\u0001(10351));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u008D, new Point(106, 196));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u008D, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u008D, \u009C.\u0001(10364));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u008D, new Size(100, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u008D, 15);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u008E, new Point(106, 169));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u008E, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u008E, \u009C.\u0001(10381));
        \u0005\u0005 obj18 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection8 = \u0091\u0004.\u007E\u000F\u0003((object) \u0084\u0002.\u007E\u0090\u0003((object) this.\u008E));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray9 = editorButtonArray1;
        obj18((object) buttonCollection8, editorButtonArray9);
        \u0016\u0005 obj19 = \u0016\u0005.\u007E\u0092\u0003;
        ComboBoxItemCollection boxItemCollection = \u009B\u0002.\u007E\u008F\u0003((object) \u0084\u0002.\u007E\u0090\u0003((object) this.\u008E));
        objArray1 = new object[9]
        {
          (object) \u009C.\u0001(3344),
          (object) \u009C.\u0001(10390),
          (object) \u009C.\u0001(10395),
          (object) \u009C.\u0001(10400),
          (object) \u009C.\u0001(10405),
          (object) \u009C.\u0001(10410),
          (object) \u009C.\u0001(10415),
          (object) \u009C.\u0001(10420),
          (object) \u009C.\u0001(10425)
        };
        object[] objArray2 = objArray1;
        obj19((object) boxItemCollection, objArray2);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u008E, new Size(100, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u008E, 14);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u008F, new Point(106, 143));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u008F, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u008F, \u009C.\u0001(10430));
        \u0005\u0005 obj20 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection9 = \u0091\u0004.\u007E\u000F\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u008F));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray10 = editorButtonArray1;
        obj20((object) buttonCollection9, editorButtonArray10);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u008F, new Size(273, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u008F, 13);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0090, new Point(106, 117));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0090, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0090, \u009C.\u0001(10447));
        \u0005\u0005 obj21 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection10 = \u0091\u0004.\u007E\u000F\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0090));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray11 = editorButtonArray1;
        obj21((object) buttonCollection10, editorButtonArray11);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0090, new Size(273, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0090, 12);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0091, new Point(106, 90));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0091, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0091, \u009C.\u0001(10456));
        \u0005\u0005 obj22 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection11 = \u0091\u0004.\u007E\u000F\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0091));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray12 = editorButtonArray1;
        obj22((object) buttonCollection11, editorButtonArray12);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0091, new Size(273, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0091, 11);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0092, new Point(106, 64));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0092, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0092, \u009C.\u0001(10473));
        \u0005\u0005 obj23 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection12 = \u0091\u0004.\u007E\u000F\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0092));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray13 = editorButtonArray1;
        obj23((object) buttonCollection12, editorButtonArray13);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0092, new Size(273, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0092, 10);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0082, new Point(106, 38));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0082, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0082, \u009C.\u0001(10486));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0082, new Size(273, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0082, 9);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0083, new Point(106, 12));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0083, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0083, \u009C.\u0001(8914));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0083, new Size(73, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0083, 8);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0084, new Point(48, 199));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0084, \u009C.\u0001(10495));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0084, new Size(52, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0084, 7);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0084, \u009C.\u0001(10516));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0086, new Point(71, 172));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0086, \u009C.\u0001(10533));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0086, new Size(29, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0086, 6);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0086, \u009C.\u0001(10554));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0087, new Point(31, 146));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0087, \u009C.\u0001(10563));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0087, new Size(69, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0087, 5);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0087, \u009C.\u0001(10584));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0088, new Point(69, 120));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0088, \u009C.\u0001(10605));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0088, new Size(28, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0088, 4);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0088, \u009C.\u0001(10626));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0089, new Point(29, 93));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0089, \u009C.\u0001(10635));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0089, new Size(71, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0089, 3);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0089, \u009C.\u0001(10656));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u008A, new Point(51, 67));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u008A, \u009C.\u0001(10677));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u008A, new Size(49, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u008A, 2);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u008A, \u009C.\u0001(10698));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u008B, new Point(69, 41));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u008B, \u009C.\u0001(10711));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u008B, new Size(31, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u008B, 1);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u008B, \u009C.\u0001(10732));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u008C, new Point(63, 15));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u008C, \u009C.\u0001(10741));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u008C, new Size(37, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u008C, 0);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u008C, \u009C.\u0001(10762));
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0018), (Control) this.\u007F\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0018), (Control) this.\u0014\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0018), (Control) this.\u0015\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0018), (Control) this.\u0016\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0018), (Control) this.\u0017\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0018), (Control) this.\u0018\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0018), (Control) this.\u0019\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0018), (Control) this.\u001A\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0018), (Control) this.\u001B\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0018), (Control) this.\u001C\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0018), (Control) this.\u001D\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0018), (Control) this.\u001E\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0018), (Control) this.\u001F\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0018, \u009C.\u0001(10775));
        \u0087\u0006.\u007E\u0006\u0004((object) this.\u0018, new Size(706, 513));
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0018, \u009C.\u0001(10784));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u007F\u0002, new Point(42, 18));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u007F\u0002, \u009C.\u0001(10793));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u007F\u0002, new Size(670, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u007F\u0002, 12);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u007F\u0002, \u009C.\u0001(10814));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0014\u0002, new Point(188, 214));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0014\u0002, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0014\u0002, \u009C.\u0001(10992));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0014\u0002, new Size(135, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0014\u0002, 11);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0015\u0002, new Point(188, 187));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0015\u0002, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0015\u0002, \u009C.\u0001(11013));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0015\u0002, new Size(135, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0015\u0002, 10);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0016\u0002, new Point(188, 160));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0016\u0002, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0016\u0002, \u009C.\u0001(11030));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0016\u0002, new Size(135, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0016\u0002, 9);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0017\u0002, new Point(188, 133));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0017\u0002, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0017\u0002, \u009C.\u0001(11047));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0017\u0002, new Size(135, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0017\u0002, 8);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0018\u0002, new Point(188, 106));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0018\u0002, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0018\u0002, \u009C.\u0001(11064));
        \u0005\u0005 obj24 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection13 = \u0091\u0004.\u007E\u000F\u0003((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u0018\u0002));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton()
        };
        EditorButton[] editorButtonArray14 = editorButtonArray1;
        obj24((object) buttonCollection13, editorButtonArray14);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0018\u0002, new Size(135, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0018\u0002, 7);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0019\u0002, new Point((int) sbyte.MaxValue, 217));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0019\u0002, \u009C.\u0001(11089));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0019\u0002, new Size(55, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0019\u0002, 6);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0019\u0002, \u009C.\u0001(11110));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u001A\u0002, new Point(144, 163));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u001A\u0002, \u009C.\u0001(11127));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u001A\u0002, new Size(38, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u001A\u0002, 5);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u001A\u0002, \u009C.\u0001(11148));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u001B\u0002, new Point(139, 190));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u001B\u0002, \u009C.\u0001(11161));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u001B\u0002, new Size(43, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u001B\u0002, 4);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u001B\u0002, \u009C.\u0001(11182));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u001C\u0002, new Point(147, 136));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u001C\u0002, \u009C.\u0001(11195));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u001C\u0002, new Size(35, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u001C\u0002, 3);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u001C\u0002, \u009C.\u0001(11216));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u001D\u0002, new Point(122, 109));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u001D\u0002, \u009C.\u0001(11225));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u001D\u0002, new Size(63, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u001D\u0002, 2);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u001D\u0002, \u009C.\u0001(11246));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u001E\u0002, new Point(112, 82));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u001E\u0002, \u009C.\u0001(11267));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u001E\u0002, new Size(70, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u001E\u0002, 1);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u001E\u0002, \u009C.\u0001(11288));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u001F\u0002, new Point(188, 79));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u001F\u0002, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u001F\u0002, \u009C.\u0001(11305));
        \u0005\u0005 obj25 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection14 = \u0091\u0004.\u007E\u000F\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u001F\u0002));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray15 = editorButtonArray1;
        obj25((object) buttonCollection14, editorButtonArray15);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u001F\u0002, new Size(135, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u001F\u0002, 0);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0019), (Control) this.\u0080\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0019), (Control) this.\u0081\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0019), (Control) this.\u0082\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0019), (Control) this.\u0083\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0019), (Control) this.\u0084\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0019), (Control) this.\u0086\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0019), (Control) this.\u0087\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0019), (Control) this.\u0088\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0019), (Control) this.\u0089\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0019), (Control) this.\u008A\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0019, \u009C.\u0001(11326));
        \u0087\u0006.\u007E\u0006\u0004((object) this.\u0019, new Size(706, 513));
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0019, \u009C.\u0001(11343));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0080\u0002, new Point(176, 185));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0080\u0002, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0080\u0002, \u009C.\u0001(11372));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0080\u0002, new Size(135, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0080\u0002, 23);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0081\u0002, new Point(176, 158));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0081\u0002, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0081\u0002, \u009C.\u0001(11389));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0081\u0002, new Size(135, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0081\u0002, 22);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0082\u0002, new Point(176, 131));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0082\u0002, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0082\u0002, \u009C.\u0001(11410));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0082\u0002, new Size(135, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0082\u0002, 21);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0083\u0002, new Point(176, 104));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0083\u0002, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0083\u0002, \u009C.\u0001(11423));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0083\u0002, new Size(135, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0083\u0002, 20);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0084\u0002, new Point(115, 188));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0084\u0002, \u009C.\u0001(11436));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0084\u0002, new Size(55, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0084\u0002, 18);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0084\u0002, \u009C.\u0001(11110));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0086\u0002, new Point(132, 134));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0086\u0002, \u009C.\u0001(11457));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0086\u0002, new Size(38, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0086\u0002, 17);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0086\u0002, \u009C.\u0001(11148));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0087\u0002, new Point(103, 161));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0087\u0002, \u009C.\u0001(11478));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0087\u0002, new Size(67, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0087\u0002, 16);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0087\u0002, \u009C.\u0001(11499));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0088\u0002, new Point(135, 107));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0088\u0002, \u009C.\u0001(11520));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0088\u0002, new Size(35, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0088\u0002, 15);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0088\u0002, \u009C.\u0001(11216));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0089\u0002, new Point(110, 81));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0089\u0002, \u009C.\u0001(11541));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0089\u0002, new Size(60, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0089\u0002, 13);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0089\u0002, \u009C.\u0001(11562));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u008A\u0002, new Point(176, 78));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u008A\u0002, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u008A\u0002, \u009C.\u0001(11579));
        \u0005\u0005 obj26 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection15 = \u0091\u0004.\u007E\u000F\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u008A\u0002));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray16 = editorButtonArray1;
        obj26((object) buttonCollection15, editorButtonArray16);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u008A\u0002, new Size(222, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u008A\u0002, 12);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001A), (Control) this.\u008B\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001A), (Control) this.\u008C\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001A), (Control) this.\u008D\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001A), (Control) this.\u008E\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001A), (Control) this.\u008F\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001A), (Control) this.\u0090\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001A), (Control) this.\u0091\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001A), (Control) this.\u0092\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u001A, \u009C.\u0001(11596));
        \u0087\u0006.\u007E\u0006\u0004((object) this.\u001A, new Size(706, 513));
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u001A, \u009C.\u0001(11609));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u008B\u0002, new Point(165, 154));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u008B\u0002, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u008B\u0002, \u009C.\u0001(11634));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u008B\u0002, new Size(135, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u008B\u0002, 33);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u008C\u0002, new Point(165, (int) sbyte.MaxValue));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u008C\u0002, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u008C\u0002, \u009C.\u0001(11651));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u008C\u0002, new Size(135, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u008C\u0002, 32);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u008D\u0002, new Point(165, 100));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u008D\u0002, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u008D\u0002, \u009C.\u0001(11672));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u008D\u0002, new Size(135, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u008D\u0002, 31);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u008E\u0002, new Point(104, 157));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u008E\u0002, \u009C.\u0001(11685));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u008E\u0002, new Size(55, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u008E\u0002, 29);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u008E\u0002, \u009C.\u0001(11110));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u008F\u0002, new Point(121, 103));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u008F\u0002, \u009C.\u0001(11706));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u008F\u0002, new Size(38, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u008F\u0002, 28);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u008F\u0002, \u009C.\u0001(11148));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0090\u0002, new Point(92, 130));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0090\u0002, \u009C.\u0001(11727));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0090\u0002, new Size(67, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0090\u0002, 27);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0090\u0002, \u009C.\u0001(11499));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0091\u0002, new Point(99, 77));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0091\u0002, \u009C.\u0001(11748));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0091\u0002, new Size(60, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0091\u0002, 25);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0091\u0002, \u009C.\u0001(11562));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0092\u0002, new Point(165, 74));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0092\u0002, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0092\u0002, \u009C.\u0001(11769));
        \u0005\u0005 obj27 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection16 = \u0091\u0004.\u007E\u000F\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0092\u0002));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray17 = editorButtonArray1;
        obj27((object) buttonCollection16, editorButtonArray17);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0092\u0002, new Size(222, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0092\u0002, 24);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001B), (Control) this.\u001D\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001B), (Control) this.\u001E\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001B), (Control) this.\u0093\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001B), (Control) this.\u0094\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001B), (Control) this.\u0095\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001B), (Control) this.\u0096\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001B), (Control) this.\u0097\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001B), (Control) this.\u0098\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001B), (Control) this.\u0099\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001B), (Control) this.\u009A\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u001B, \u009C.\u0001(11786));
        \u0087\u0006.\u007E\u0006\u0004((object) this.\u001B, new Size(706, 513));
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u001B, \u009C.\u0001(11795));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u001D\u0003, new Point(171, 161));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u001D\u0003, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u001D\u0003, \u009C.\u0001(11804));
        \u0005\u0005 obj28 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection17 = \u0091\u0004.\u007E\u000F\u0003((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u001D\u0003));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton()
        };
        EditorButton[] editorButtonArray18 = editorButtonArray1;
        obj28((object) buttonCollection17, editorButtonArray18);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u001D\u0003, new Size(135, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u001D\u0003, 39);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u001E\u0003, new Point(102, 164));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u001E\u0003, \u009C.\u0001(11821));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u001E\u0003, new Size(63, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u001E\u0003, 38);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u001E\u0003, \u009C.\u0001(11246));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0093\u0002, new Point(171, 135));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0093\u0002, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0093\u0002, \u009C.\u0001(11842));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0093\u0002, new Size(135, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0093\u0002, 37);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0094\u0002, new Point(122, 138));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0094\u0002, \u009C.\u0001(11855));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0094\u0002, new Size(43, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0094\u0002, 36);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0094\u0002, \u009C.\u0001(11182));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0095\u0002, new Point(95, 86));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0095\u0002, \u009C.\u0001(11876));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0095\u0002, new Size(70, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0095\u0002, 35);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0095\u0002, \u009C.\u0001(11288));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0096\u0002, new Point(171, 83));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0096\u0002, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0096\u0002, \u009C.\u0001(11897));
        \u0005\u0005 obj29 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection18 = \u0091\u0004.\u007E\u000F\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0096\u0002));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray19 = editorButtonArray1;
        obj29((object) buttonCollection18, editorButtonArray19);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0096\u0002, new Size(135, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0096\u0002, 34);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0097\u0002, new Point(171, 109));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0097\u0002, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0097\u0002, \u009C.\u0001(11914));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0097\u0002, new Size(135, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0097\u0002, 30);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0098\u0002, new Point(130, 112));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0098\u0002, \u009C.\u0001(11927));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0098\u0002, new Size(35, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0098\u0002, 26);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0098\u0002, \u009C.\u0001(11216));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0099\u0002, new Point(105, 60));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0099\u0002, \u009C.\u0001(11948));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0099\u0002, new Size(60, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0099\u0002, 25);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0099\u0002, \u009C.\u0001(11562));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u009A\u0002, new Point(171, 57));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u009A\u0002, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u009A\u0002, \u009C.\u0001(11969));
        \u0005\u0005 obj30 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection19 = \u0091\u0004.\u007E\u000F\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u009A\u0002));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray20 = editorButtonArray1;
        obj30((object) buttonCollection19, editorButtonArray20);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u009A\u0002, new Size(222, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u009A\u0002, 24);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001C), (Control) this.\u009B\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001C), (Control) this.\u009C\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001C), (Control) this.\u009D\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001C), (Control) this.\u009E\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u001C, \u009C.\u0001(11986));
        \u0087\u0006.\u007E\u0006\u0004((object) this.\u001C, new Size(706, 513));
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u001C, \u009C.\u0001(11999));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u009B\u0002, new Point(159, 94));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u009B\u0002, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u009B\u0002, \u009C.\u0001(12008));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u009B\u0002, new Size(135, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u009B\u0002, 25);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u009C\u0002, new Point(159, 67));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u009C\u0002, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u009C\u0002, \u009C.\u0001(12025));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u009C\u0002, new Size(135, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u009C\u0002, 24);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u009D\u0002, new Point(115, 97));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u009D\u0002, \u009C.\u0001(12042));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u009D\u0002, new Size(38, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u009D\u0002, 23);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u009D\u0002, \u009C.\u0001(11148));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u009E\u0002, new Point(118, 70));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u009E\u0002, \u009C.\u0001(12063));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u009E\u0002, new Size(35, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u009E\u0002, 22);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u009E\u0002, \u009C.\u0001(11216));
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001D), (Control) this.\u009F\u0002);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001D), (Control) this.\u0001\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001D), (Control) this.\u0002\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001D), (Control) this.\u0003\u0003);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u001D, \u009C.\u0001(12084));
        \u0087\u0006.\u007E\u0006\u0004((object) this.\u001D, new Size(706, 513));
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u001D, \u009C.\u0001(12097));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u009F\u0002, new Point(159, 93));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u009F\u0002, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u009F\u0002, \u009C.\u0001(12110));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u009F\u0002, new Size(135, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u009F\u0002, 25);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0001\u0003, new Point(159, 66));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0001\u0003, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0001\u0003, \u009C.\u0001(12127));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0001\u0003, new Size(135, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0001\u0003, 24);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0002\u0003, new Point(103, 96));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0002\u0003, \u009C.\u0001(12144));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0002\u0003, new Size(50, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0002\u0003, 23);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0002\u0003, \u009C.\u0001(12165));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0003\u0003, new Point(107, 69));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0003\u0003, \u009C.\u0001(12178));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0003\u0003, new Size(46, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0003\u0003, 22);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0003\u0003, \u009C.\u0001(12199));
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0018\u0003), (Control) this.\u0019\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0018\u0003), (Control) this.\u001A\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0018\u0003), (Control) this.\u001B\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0018\u0003), (Control) this.\u001C\u0003);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0018\u0003, \u009C.\u0001(12212));
        \u0087\u0006.\u007E\u0006\u0004((object) this.\u0018\u0003, new Size(706, 513));
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0018\u0003, \u009C.\u0001(12225));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0019\u0003, new Point(174, 107));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0019\u0003, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0019\u0003, \u009C.\u0001(12234));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0019\u0003, new Size(135, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0019\u0003, 29);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u001A\u0003, new Point(174, 80));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u001A\u0003, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u001A\u0003, \u009C.\u0001(12251));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u001A\u0003, new Size(135, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u001A\u0003, 28);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u001B\u0003, new Point(118, 110));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u001B\u0003, \u009C.\u0001(12268));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u001B\u0003, new Size(50, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u001B\u0003, 27);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u001B\u0003, \u009C.\u0001(12165));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u001C\u0003, new Point(122, 83));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u001C\u0003, \u009C.\u0001(12289));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u001C\u0003, new Size(46, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u001C\u0003, 26);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u001C\u0003, \u009C.\u0001(12199));
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001E), (Control) this.\u0004\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001E), (Control) this.\u0005\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001E), (Control) this.\u0006\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001E), (Control) this.\u0007\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001E), (Control) this.\u0008\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001E), (Control) this.\u000E\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001E), (Control) this.\u000F\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u001E), (Control) this.\u0010\u0003);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u001E, \u009C.\u0001(12310));
        \u0087\u0006.\u007E\u0006\u0004((object) this.\u001E, new Size(706, 513));
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u001E, \u009C.\u0001(12323));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0004\u0003, new Point(168, 145));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0004\u0003, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0004\u0003, \u009C.\u0001(12332));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0004\u0003, new Size(135, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0004\u0003, 31);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0005\u0003, new Point(168, 118));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0005\u0003, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0005\u0003, \u009C.\u0001(12349));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0005\u0003, new Size(135, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0005\u0003, 30);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0006\u0003, new Point(168, 91));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0006\u0003, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0006\u0003, \u009C.\u0001(12366));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0006\u0003, new Size(135, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0006\u0003, 29);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0007\u0003, new Point(168, 64));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0007\u0003, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0007\u0003, \u009C.\u0001(12379));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0007\u0003, new Size(135, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0007\u0003, 28);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0008\u0003, new Point(112, 148));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0008\u0003, \u009C.\u0001(12396));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0008\u0003, new Size(50, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0008\u0003, 27);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0008\u0003, \u009C.\u0001(12417));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u000E\u0003, new Point((int) sbyte.MaxValue, 94));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u000E\u0003, \u009C.\u0001(12434));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u000E\u0003, new Size(35, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u000E\u0003, 26);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u000E\u0003, \u009C.\u0001(12455));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u000F\u0003, new Point(105, 121));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u000F\u0003, \u009C.\u0001(12468));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u000F\u0003, new Size(57, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u000F\u0003, 25);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u000F\u0003, \u009C.\u0001(12489));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0010\u0003, new Point(99, 67));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0010\u0003, \u009C.\u0001(12506));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0010\u0003, new Size(63, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0010\u0003, 24);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0010\u0003, \u009C.\u0001(12527));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u001F, \u009C.\u0001(12544));
        \u0087\u0006.\u007E\u0006\u0004((object) this.\u001F, new Size(706, 513));
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u001F, \u009C.\u0001(12557));
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u007F), (Control) this.\u0013\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u007F), (Control) this.\u0098\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u007F), (Control) this.\u0092\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u007F), (Control) this.\u0093\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u007F), (Control) this.\u0094\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u007F), (Control) this.\u0095\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u007F), (Control) this.\u0096\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u007F), (Control) this.\u0097\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u007F), (Control) this.\u0091\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u007F), (Control) this.\u001F\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u007F), (Control) this.\u007F\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u007F), (Control) this.\u0080\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u007F), (Control) this.\u0081\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u007F), (Control) this.\u0082\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u007F), (Control) this.\u0083\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u007F), (Control) this.\u0084\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u007F), (Control) this.\u0086\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u007F), (Control) this.\u0087\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u007F), (Control) this.\u0088\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u007F), (Control) this.\u0089\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u007F), (Control) this.\u008A\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u007F), (Control) this.\u008B\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u007F), (Control) this.\u008C\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u007F), (Control) this.\u008D\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u007F), (Control) this.\u008E\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u007F), (Control) this.\u008F\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u007F), (Control) this.\u0090\u0003);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u007F, \u009C.\u0001(12570));
        \u0087\u0006.\u007E\u0006\u0004((object) this.\u007F, new Size(706, 513));
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u007F, \u009C.\u0001(12591));
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0013\u0004), (Control) this.\u0014\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0013\u0004), (Control) this.\u0015\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0013\u0004), (Control) this.\u0016\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0013\u0004), (Control) this.\u0017\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0013\u0004), (Control) this.\u0018\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0013\u0004), (Control) this.\u0019\u0004);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0013\u0004, new Point(49, 321));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0013\u0004, \u009C.\u0001(12612));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0013\u0004, new Size(229, (int) sbyte.MaxValue));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0013\u0004, 28);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0013\u0004, \u009C.\u0001(12633));
        \u0080\u0002 obj31 = \u0080\u0002.\u007E\u0012\u0003;
        SpinEdit spinEdit3 = this.\u0014\u0004;
        bits = new int[4];
        // ISSUE: variable of a boxed type
        __Boxed<Decimal> local3 = (ValueType) new Decimal(bits);
        obj31((object) spinEdit3, (object) local3);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0014\u0004, new Point(110, 90));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0014\u0004, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0014\u0004, \u009C.\u0001(12662));
        \u0005\u0005 obj32 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection20 = \u0091\u0004.\u007E\u000F\u0003((object) \u001C\u0003.\u007E\u0003\u0004((object) this.\u0014\u0004));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton()
        };
        EditorButton[] editorButtonArray21 = editorButtonArray1;
        obj32((object) buttonCollection20, editorButtonArray21);
        \u009C\u0006.\u007E\u0001\u0004((object) \u001C\u0003.\u007E\u0003\u0004((object) this.\u0014\u0004), false);
        \u001C\u0005.\u007E\u0089\u0003((object) \u0080\u0003.\u007E\u0008\u0003((object) \u001C\u0003.\u007E\u0003\u0004((object) this.\u0014\u0004)), \u009C.\u0001(9297));
        \u009C\u0002 obj33 = \u009C\u0002.\u007E\u009F\u0003;
        RepositoryItemSpinEdit repositoryItemSpinEdit3 = \u001C\u0003.\u007E\u0003\u0004((object) this.\u0014\u0004);
        bits = new int[4]{ 200, 0, 0, 0 };
        Decimal num7 = new Decimal(bits);
        obj33((object) repositoryItemSpinEdit3, num7);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0014\u0004, new Size(107, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0014\u0004, 40);
        \u001D\u0005.\u007E\u0013\u0003((object) this.\u0014\u0004, new EventHandler(this.\u0090\u0006));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0015\u0004, new Point(51, 93));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0015\u0004, \u009C.\u0001(12679));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0015\u0004, new Size(53, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0015\u0004, 39);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0015\u0004, \u009C.\u0001(12700));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0016\u0004, new Point(110, 64));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0016\u0004, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0016\u0004, \u009C.\u0001(12713));
        \u0012\u0007.\u007E\u0014\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0016\u0004)), \u008D\u0004.\u0014\u000F());
        \u009C\u0006.\u007E\u0017\u0004((object) global::\u0010\u0006.\u007E\u0012\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0016\u0004))), true);
        \u009C\u0006.\u007E\u0005\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0016\u0004), true);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0016\u0004, new Size(107, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0016\u0004, 38);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0017\u0004, new Point(49, 67));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0017\u0004, \u009C.\u0001(12726));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0017\u0004, new Size(55, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0017\u0004, 37);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0017\u0004, \u009C.\u0001(12747));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0018\u0004, new Point(110, 38));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0018\u0004, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0018\u0004, \u009C.\u0001(12764));
        \u0012\u0007.\u007E\u0014\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0018\u0004)), \u008D\u0004.\u0014\u000F());
        \u009C\u0006.\u007E\u0017\u0004((object) global::\u0010\u0006.\u007E\u0012\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0018\u0004))), true);
        \u009C\u0006.\u007E\u0005\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0018\u0004), true);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0018\u0004, new Size(107, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0018\u0004, 36);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0019\u0004, new Point(23, 41));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0019\u0004, \u009C.\u0001(12785));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0019\u0004, new Size(81, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0019\u0004, 35);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0019\u0004, \u009C.\u0001(12806));
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0098\u0003), (Control) this.\u0099\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0098\u0003), (Control) this.\u009A\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0098\u0003), (Control) this.\u009B\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0098\u0003), (Control) this.\u009C\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0098\u0003), (Control) this.\u009D\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0098\u0003), (Control) this.\u009E\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0098\u0003), (Control) this.\u009F\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0098\u0003), (Control) this.\u0001\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0098\u0003), (Control) this.\u0002\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0098\u0003), (Control) this.\u0003\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0098\u0003), (Control) this.\u0004\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0098\u0003), (Control) this.\u0005\u0004);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0098\u0003, new Point(319, 231));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0098\u0003, \u009C.\u0001(12827));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0098\u0003, new Size(282, 189));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0098\u0003, 27);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0098\u0003, \u009C.\u0001(12848));
        \u0012\u0007.\u007E\u0013\u0004((object) \u0088\u0005.\u007E\u0088\u0003((object) this.\u0099\u0003), \u008D\u0004.\u0013\u000F());
        \u009C\u0006.\u007E\u0016\u0004((object) global::\u0010\u0006.\u007E\u0012\u0004((object) \u0088\u0005.\u007E\u0088\u0003((object) this.\u0099\u0003)), true);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0099\u0003, new Point(234, 90));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0099\u0003, \u009C.\u0001(12881));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0099\u0003, new Size(33, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0099\u0003, 51);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0099\u0003, \u009C.\u0001(12902));
        \u0012\u0007.\u007E\u0013\u0004((object) \u0088\u0005.\u007E\u0088\u0003((object) this.\u009A\u0003), \u008D\u0004.\u0015\u000F());
        \u009C\u0006.\u007E\u0016\u0004((object) global::\u0010\u0006.\u007E\u0012\u0004((object) \u0088\u0005.\u007E\u0088\u0003((object) this.\u009A\u0003)), true);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u009A\u0003, new Point(234, 44));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u009A\u0003, \u009C.\u0001(12911));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u009A\u0003, new Size(23, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u009A\u0003, 50);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u009A\u0003, \u009C.\u0001(12932));
        \u0012\u0007.\u007E\u0013\u0004((object) \u0088\u0005.\u007E\u0088\u0003((object) this.\u009B\u0003), \u008D\u0004.\u0012\u000F());
        \u009C\u0006.\u007E\u0016\u0004((object) global::\u0010\u0006.\u007E\u0012\u0004((object) \u0088\u0005.\u007E\u0088\u0003((object) this.\u009B\u0003)), true);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u009B\u0003, new Point(234, 135));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u009B\u0003, \u009C.\u0001(12941));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u009B\u0003, new Size(23, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u009B\u0003, 49);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u009B\u0003, \u009C.\u0001(12962));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u009C\u0003, new Point(174, 25));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u009C\u0003, \u009C.\u0001(12971));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u009C\u0003, new Size(54, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u009C\u0003, 48);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u009C\u0003, \u009C.\u0001(12992));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u009D\u0003, new Point(113, 25));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u009D\u0003, \u009C.\u0001(13009));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u009D\u0003, new Size(46, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u009D\u0003, 47);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u009D\u0003, \u009C.\u0001(13030));
        \u008A\u0004.\u007E\u0015\u0004((object) \u0088\u0005.\u007E\u0088\u0003((object) this.\u009E\u0003), LinearGradientMode.Vertical);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u009E\u0003, new Point(26, 25));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u009E\u0003, \u009C.\u0001(13043));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u009E\u0003, new Size(69, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u009E\u0003, 46);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u009E\u0003, \u009C.\u0001(12591));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u009F\u0003, new Point(183, 154));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u009F\u0003, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u009F\u0003, \u009C.\u0001(13064));
        \u0012\u0007.\u007E\u0014\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u009F\u0003)), \u008D\u0004.\u0014\u000F());
        \u009C\u0006.\u007E\u0017\u0004((object) global::\u0010\u0006.\u007E\u0012\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u009F\u0003))), true);
        \u009C\u0006.\u007E\u0005\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u009F\u0003), true);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u009F\u0003, new Size(27, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u009F\u0003, 45);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0001\u0004, new Point(113, 154));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0001\u0004, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0001\u0004, \u009C.\u0001(13077));
        \u0012\u0007.\u007E\u0014\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0001\u0004)), \u008D\u0004.\u0014\u000F());
        \u009C\u0006.\u007E\u0017\u0004((object) global::\u0010\u0006.\u007E\u0012\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0001\u0004))), true);
        \u009C\u0006.\u007E\u0005\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0001\u0004), true);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0001\u0004, new Size(27, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0001\u0004, 44);
        \u0080\u0002.\u007E\u0012\u0003((object) this.\u0002\u0004, (object) \u009C.\u0001(8361));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0002\u0004, new Point(38, 154));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0002\u0004, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0002\u0004, \u009C.\u0001(13090));
        \u0012\u0007.\u007E\u0014\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0002\u0004)), \u008D\u0004.\u0014\u000F());
        \u009C\u0006.\u007E\u0017\u0004((object) global::\u0010\u0006.\u007E\u0012\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0002\u0004))), true);
        \u009C\u0006.\u007E\u0005\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0002\u0004), true);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0002\u0004, new Size(27, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0002\u0004, 43);
        \u0080\u0002.\u007E\u0012\u0003((object) this.\u0003\u0004, (object) null);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0003\u0004, new Point(183, 44));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0003\u0004, \u009C.\u0001(13103));
        \u0011\u0004.\u007E\u009A\u0003((object) \u0091\u0006.\u007E\u009D\u0003((object) this.\u0003\u0004), 100);
        \u001A\u0004.\u007E\u009B\u0003((object) \u0091\u0006.\u007E\u009D\u0003((object) this.\u0003\u0004), Orientation.Vertical);
        \u009C\u0006.\u007E\u0099\u0003((object) \u0091\u0006.\u007E\u009D\u0003((object) this.\u0003\u0004), true);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0003\u0004, new Size(45, 104));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0003\u0004, 42);
        \u001D\u0005.\u007E\u0013\u0003((object) this.\u0003\u0004, new EventHandler(this.\u008D\u0006));
        \u0080\u0002.\u007E\u0012\u0003((object) this.\u0004\u0004, (object) null);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0004\u0004, new Point(113, 44));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0004\u0004, \u009C.\u0001(13116));
        \u0011\u0004.\u007E\u009A\u0003((object) \u0091\u0006.\u007E\u009D\u0003((object) this.\u0004\u0004), 100);
        \u001A\u0004.\u007E\u009B\u0003((object) \u0091\u0006.\u007E\u009D\u0003((object) this.\u0004\u0004), Orientation.Vertical);
        \u009C\u0006.\u007E\u0099\u0003((object) \u0091\u0006.\u007E\u009D\u0003((object) this.\u0004\u0004), true);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0004\u0004, new Size(45, 104));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0004\u0004, 41);
        \u001D\u0005.\u007E\u0013\u0003((object) this.\u0004\u0004, new EventHandler(this.\u008C\u0006));
        \u0080\u0002.\u007E\u0012\u0003((object) this.\u0005\u0004, (object) null);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0005\u0004, new Point(38, 44));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0005\u0004, \u009C.\u0001(13129));
        \u0011\u0004.\u007E\u009A\u0003((object) \u0091\u0006.\u007E\u009D\u0003((object) this.\u0005\u0004), 100);
        \u001A\u0004.\u007E\u009B\u0003((object) \u0091\u0006.\u007E\u009D\u0003((object) this.\u0005\u0004), Orientation.Vertical);
        \u009C\u0006.\u007E\u0099\u0003((object) \u0091\u0006.\u007E\u009D\u0003((object) this.\u0005\u0004), true);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0005\u0004, new Size(45, 104));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0005\u0004, 40);
        \u001D\u0005.\u007E\u0013\u0003((object) this.\u0005\u0004, new EventHandler(this.\u008B\u0006));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0092\u0003, new Point(543, 193));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0092\u0003, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0092\u0003, \u009C.\u0001(13142));
        \u0012\u0007.\u007E\u0014\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0092\u0003)), \u008D\u0004.\u0014\u000F());
        \u009C\u0006.\u007E\u0017\u0004((object) global::\u0010\u0006.\u007E\u0012\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0092\u0003))), true);
        \u009C\u0006.\u007E\u0005\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0092\u0003), true);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0092\u0003, new Size(88, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0092\u0003, 26);
        \u001D\u0005.\u007E\u0013\u0003((object) this.\u0092\u0003, new EventHandler(this.\u008F\u0006));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0093\u0003, new Point(543, 167));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0093\u0003, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0093\u0003, \u009C.\u0001(13159));
        \u0012\u0007.\u007E\u0014\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0093\u0003)), \u008D\u0004.\u0014\u000F());
        \u009C\u0006.\u007E\u0017\u0004((object) global::\u0010\u0006.\u007E\u0012\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0093\u0003))), true);
        \u009C\u0006.\u007E\u0005\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0093\u0003), true);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0093\u0003, new Size(88, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0093\u0003, 25);
        \u001D\u0005.\u007E\u0013\u0003((object) this.\u0093\u0003, new EventHandler(this.\u008F\u0006));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0094\u0003, new Point(543, 141));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0094\u0003, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0094\u0003, \u009C.\u0001(13176));
        \u0012\u0007.\u007E\u0014\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0094\u0003)), \u008D\u0004.\u0014\u000F());
        \u009C\u0006.\u007E\u0017\u0004((object) global::\u0010\u0006.\u007E\u0012\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0094\u0003))), true);
        \u009C\u0006.\u007E\u0005\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0094\u0003), true);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0094\u0003, new Size(88, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0094\u0003, 24);
        \u001D\u0005.\u007E\u0013\u0003((object) this.\u0094\u0003, new EventHandler(this.\u008F\u0006));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0095\u0003, new Point(543, 115));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0095\u0003, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0095\u0003, \u009C.\u0001(13193));
        \u0012\u0007.\u007E\u0014\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0095\u0003)), \u008D\u0004.\u0014\u000F());
        \u009C\u0006.\u007E\u0017\u0004((object) global::\u0010\u0006.\u007E\u0012\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0095\u0003))), true);
        \u009C\u0006.\u007E\u0005\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0095\u0003), true);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0095\u0003, new Size(88, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0095\u0003, 23);
        \u001D\u0005.\u007E\u0013\u0003((object) this.\u0095\u0003, new EventHandler(this.\u008F\u0006));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0096\u0003, new Point(543, 89));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0096\u0003, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0096\u0003, \u009C.\u0001(13210));
        \u0012\u0007.\u007E\u0014\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0096\u0003)), \u008D\u0004.\u0014\u000F());
        \u009C\u0006.\u007E\u0017\u0004((object) global::\u0010\u0006.\u007E\u0012\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0096\u0003))), true);
        \u009C\u0006.\u007E\u0005\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0096\u0003), true);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0096\u0003, new Size(88, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0096\u0003, 22);
        \u001D\u0005.\u007E\u0013\u0003((object) this.\u0096\u0003, new EventHandler(this.\u008F\u0006));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0097\u0003, new Point(543, 63));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0097\u0003, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0097\u0003, \u009C.\u0001(13227));
        \u0012\u0007.\u007E\u0014\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0097\u0003)), \u008D\u0004.\u0014\u000F());
        \u009C\u0006.\u007E\u0017\u0004((object) global::\u0010\u0006.\u007E\u0012\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0097\u0003))), true);
        \u009C\u0006.\u007E\u0005\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0097\u0003), true);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0097\u0003, new Size(88, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0097\u0003, 21);
        \u001D\u0005.\u007E\u0013\u0003((object) this.\u0097\u0003, new EventHandler(this.\u008F\u0006));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0091\u0003, new Point(39, 277));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0091\u0003, \u009C.\u0001(13244));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0091\u0003, new Size(227, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0091\u0003, 18);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0091\u0003, \u009C.\u0001(13265));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u001F\u0003, new Point(241, 193));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u001F\u0003, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u001F\u0003, \u009C.\u0001(13326));
        \u0012\u0007.\u007E\u0014\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u001F\u0003)), \u008D\u0004.\u0014\u000F());
        \u009C\u0006.\u007E\u0017\u0004((object) global::\u0010\u0006.\u007E\u0012\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u001F\u0003))), true);
        \u009C\u0006.\u007E\u0005\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u001F\u0003), true);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u001F\u0003, new Size(296, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u001F\u0003, 17);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u007F\u0003, new Point(39, 196));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u007F\u0003, \u009C.\u0001(13343));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u007F\u0003, new Size(68, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u007F\u0003, 16);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u007F\u0003, \u009C.\u0001(13364));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0080\u0003, new Point(113, 193));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0080\u0003, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0080\u0003, \u009C.\u0001(13381));
        \u0005\u0005 obj34 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection21 = \u0091\u0004.\u007E\u000F\u0003((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u0080\u0003));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton()
        };
        EditorButton[] editorButtonArray22 = editorButtonArray1;
        obj34((object) buttonCollection21, editorButtonArray22);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0080\u0003, new Size(122, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0080\u0003, 15);
        \u0093\u0006.\u007E\u0018\u0003((object) this.\u0080\u0003, new ButtonPressedEventHandler(this.\u001A\u0006));
        \u001D\u0005.\u007E\u0013\u0003((object) this.\u0080\u0003, new EventHandler(this.\u007F\u0006));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0081\u0003, new Point(241, 167));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0081\u0003, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0081\u0003, \u009C.\u0001(13390));
        \u0012\u0007.\u007E\u0014\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0081\u0003)), \u008D\u0004.\u0014\u000F());
        \u009C\u0006.\u007E\u0017\u0004((object) global::\u0010\u0006.\u007E\u0012\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0081\u0003))), true);
        \u009C\u0006.\u007E\u0005\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0081\u0003), true);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0081\u0003, new Size(296, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0081\u0003, 14);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0082\u0003, new Point(39, 170));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0082\u0003, \u009C.\u0001(13407));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0082\u0003, new Size(68, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0082\u0003, 13);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0082\u0003, \u009C.\u0001(13428));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0083\u0003, new Point(113, 167));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0083\u0003, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0083\u0003, \u009C.\u0001(13445));
        \u0005\u0005 obj35 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection22 = \u0091\u0004.\u007E\u000F\u0003((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u0083\u0003));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton()
        };
        EditorButton[] editorButtonArray23 = editorButtonArray1;
        obj35((object) buttonCollection22, editorButtonArray23);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0083\u0003, new Size(122, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0083\u0003, 12);
        \u0093\u0006.\u007E\u0018\u0003((object) this.\u0083\u0003, new ButtonPressedEventHandler(this.\u001A\u0006));
        \u001D\u0005.\u007E\u0013\u0003((object) this.\u0083\u0003, new EventHandler(this.\u001F\u0006));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0084\u0003, new Point(241, 141));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0084\u0003, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0084\u0003, \u009C.\u0001(13454));
        \u0012\u0007.\u007E\u0014\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0084\u0003)), \u008D\u0004.\u0014\u000F());
        \u009C\u0006.\u007E\u0017\u0004((object) global::\u0010\u0006.\u007E\u0012\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0084\u0003))), true);
        \u009C\u0006.\u007E\u0005\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0084\u0003), true);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0084\u0003, new Size(296, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0084\u0003, 11);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0086\u0003, new Point(39, 144));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0086\u0003, \u009C.\u0001(13471));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0086\u0003, new Size(68, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0086\u0003, 10);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0086\u0003, \u009C.\u0001(13492));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0087\u0003, new Point(113, 141));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0087\u0003, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0087\u0003, \u009C.\u0001(13509));
        \u0005\u0005 obj36 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection23 = \u0091\u0004.\u007E\u000F\u0003((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u0087\u0003));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton()
        };
        EditorButton[] editorButtonArray24 = editorButtonArray1;
        obj36((object) buttonCollection23, editorButtonArray24);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0087\u0003, new Size(122, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0087\u0003, 9);
        \u0093\u0006.\u007E\u0018\u0003((object) this.\u0087\u0003, new ButtonPressedEventHandler(this.\u001A\u0006));
        \u001D\u0005.\u007E\u0013\u0003((object) this.\u0087\u0003, new EventHandler(this.\u001E\u0006));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0088\u0003, new Point(241, 115));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0088\u0003, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0088\u0003, \u009C.\u0001(13518));
        \u0012\u0007.\u007E\u0014\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0088\u0003)), \u008D\u0004.\u0014\u000F());
        \u009C\u0006.\u007E\u0017\u0004((object) global::\u0010\u0006.\u007E\u0012\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0088\u0003))), true);
        \u009C\u0006.\u007E\u0005\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0088\u0003), true);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0088\u0003, new Size(296, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0088\u0003, 8);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0089\u0003, new Point(39, 118));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0089\u0003, \u009C.\u0001(13535));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0089\u0003, new Size(68, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0089\u0003, 7);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0089\u0003, \u009C.\u0001(13556));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u008A\u0003, new Point(113, 115));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u008A\u0003, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u008A\u0003, \u009C.\u0001(13573));
        \u0005\u0005 obj37 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection24 = \u0091\u0004.\u007E\u000F\u0003((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u008A\u0003));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton()
        };
        EditorButton[] editorButtonArray25 = editorButtonArray1;
        obj37((object) buttonCollection24, editorButtonArray25);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u008A\u0003, new Size(122, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u008A\u0003, 6);
        \u0093\u0006.\u007E\u0018\u0003((object) this.\u008A\u0003, new ButtonPressedEventHandler(this.\u001A\u0006));
        \u001D\u0005.\u007E\u0013\u0003((object) this.\u008A\u0003, new EventHandler(this.\u001D\u0006));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u008B\u0003, new Point(241, 89));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u008B\u0003, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u008B\u0003, \u009C.\u0001(13582));
        \u0012\u0007.\u007E\u0014\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u008B\u0003)), \u008D\u0004.\u0014\u000F());
        \u009C\u0006.\u007E\u0017\u0004((object) global::\u0010\u0006.\u007E\u0012\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u008B\u0003))), true);
        \u009C\u0006.\u007E\u0005\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u008B\u0003), true);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u008B\u0003, new Size(296, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u008B\u0003, 5);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u008C\u0003, new Point(39, 92));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u008C\u0003, \u009C.\u0001(13599));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u008C\u0003, new Size(68, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u008C\u0003, 4);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u008C\u0003, \u009C.\u0001(13620));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u008D\u0003, new Point(113, 89));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u008D\u0003, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u008D\u0003, \u009C.\u0001(13637));
        \u0005\u0005 obj38 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection25 = \u0091\u0004.\u007E\u000F\u0003((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u008D\u0003));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton()
        };
        EditorButton[] editorButtonArray26 = editorButtonArray1;
        obj38((object) buttonCollection25, editorButtonArray26);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u008D\u0003, new Size(122, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u008D\u0003, 3);
        \u0093\u0006.\u007E\u0018\u0003((object) this.\u008D\u0003, new ButtonPressedEventHandler(this.\u001A\u0006));
        \u001D\u0005.\u007E\u0013\u0003((object) this.\u008D\u0003, new EventHandler(this.\u001C\u0006));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u008E\u0003, new Point(241, 63));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u008E\u0003, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u008E\u0003, \u009C.\u0001(13646));
        \u0012\u0007.\u007E\u0014\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u008E\u0003)), \u008D\u0004.\u0014\u000F());
        \u009C\u0006.\u007E\u0017\u0004((object) global::\u0010\u0006.\u007E\u0012\u0004((object) \u0018\u0005.\u007E\u0006\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u008E\u0003))), true);
        \u009C\u0006.\u007E\u0005\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u008E\u0003), true);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u008E\u0003, new Size(296, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u008E\u0003, 2);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u008F\u0003, new Point(39, 66));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u008F\u0003, \u009C.\u0001(13663));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u008F\u0003, new Size(68, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u008F\u0003, 1);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u008F\u0003, \u009C.\u0001(13684));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0090\u0003, new Point(113, 63));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0090\u0003, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0090\u0003, \u009C.\u0001(13701));
        \u0005\u0005 obj39 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection26 = \u0091\u0004.\u007E\u000F\u0003((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u0090\u0003));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton()
        };
        EditorButton[] editorButtonArray27 = editorButtonArray1;
        obj39((object) buttonCollection26, editorButtonArray27);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0090\u0003, new Size(122, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0090\u0003, 0);
        \u0093\u0006.\u007E\u0018\u0003((object) this.\u0090\u0003, new ButtonPressedEventHandler(this.\u001A\u0006));
        \u001D\u0005.\u007E\u0013\u0003((object) this.\u0090\u0003, new EventHandler(this.\u001B\u0006));
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0080), (Control) this.\u001D\u0005);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0080, \u009C.\u0001(13710));
        \u0087\u0006.\u007E\u0006\u0004((object) this.\u0080, new Size(706, 515));
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0080, \u009C.\u0001(13723));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u001D\u0005, new Point(3, 3));
        \u0096\u0006.\u007E\u0093((object) this.\u001D\u0005, (BaseView) this.\u001E\u0005);
        \u001A\u0006.\u007E\u0083\u0003((object) this.\u001D\u0005, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u001D\u0005, \u009C.\u0001(13736));
        \u008F\u0002 obj40 = \u008F\u0002.\u007E\u009E\u0003;
        RepositoryItemCollection repositoryItemCollection2 = \u0082\u0004.\u007E\u0084\u0003((object) this.\u001D\u0005);
        repositoryItemArray1 = new RepositoryItem[2]
        {
          (RepositoryItem) this.\u007F\u0005,
          (RepositoryItem) this.\u0086\u0005
        };
        RepositoryItem[] repositoryItemArray3 = repositoryItemArray1;
        obj40((object) repositoryItemCollection2, repositoryItemArray3);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u001D\u0005, new Size(700, 508));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u001D\u0005, 0);
        \u0006\u0004 obj41 = \u0006\u0004.\u007E\u0001\u0002;
        ViewRepositoryCollection repositoryCollection = \u0098\u0003.\u007E\u0092((object) this.\u001D\u0005);
        baseViewArray1 = new BaseView[1]
        {
          (BaseView) this.\u001E\u0005
        };
        BaseView[] baseViewArray2 = baseViewArray1;
        obj41((object) repositoryCollection, baseViewArray2);
        \u000F\u0003 obj42 = \u000F\u0003.\u007E\u008B;
        GridColumnCollection columnCollection = \u0099\u0003.\u007E\u0012((object) this.\u001E\u0005);
        gridColumnArray1 = new GridColumn[6]
        {
          this.\u001F\u0005,
          this.\u0080\u0005,
          this.\u0081\u0005,
          this.\u0082\u0005,
          this.\u0083\u0005,
          this.\u0084\u0005
        };
        GridColumn[] gridColumnArray2 = gridColumnArray1;
        obj42((object) columnCollection, gridColumnArray2);
        \u009B\u0005.\u007E\u000E((object) this.\u001E\u0005, this.\u001D\u0005);
        \u001C\u0005.\u007E\u0008((object) this.\u001E\u0005, \u009C.\u0001(13757));
        \u009C\u0006.\u007E\u008C((object) \u009C\u0003.\u007E\u001D((object) this.\u001E\u0005), false);
        \u009C\u0006.\u007E\u0097((object) \u0097\u0003.\u007E\u001F((object) this.\u001E\u0005), false);
        \u009C\u0006.\u007E\u0098((object) \u0097\u0003.\u007E\u001F((object) this.\u001E\u0005), false);
        \u009C\u0006.\u007E\u0099((object) \u0097\u0003.\u007E\u001F((object) this.\u001E\u0005), false);
        \u009C\u0006.\u007E\u0006((object) \u009F\u0003.\u007E\u001E((object) this.\u001E\u0005), false);
        \u0099\u0006.\u007E\u0005((object) \u009F\u0003.\u007E\u001E((object) this.\u001E\u0005), NewItemRowPosition.Bottom);
        \u009C\u0006.\u007E\u0007((object) \u009F\u0003.\u007E\u001E((object) this.\u001E\u0005), false);
        \u009A\u0005.\u007E\u001B((object) this.\u001E\u0005, new InitNewRowEventHandler(this.\u0005\u0011));
        \u0007\u0003.\u007E\u007F((object) this.\u001E\u0005, new GridMenuEventHandler(this.\u0093\u0006));
        \u001C\u0005.\u007E\u0083((object) this.\u001F\u0005, \u009C.\u0001(13774));
        \u0081\u0002.\u007E\u0086((object) this.\u001F\u0005, (RepositoryItem) this.\u007F\u0005);
        \u001C\u0005.\u007E\u0084((object) this.\u001F\u0005, \u009C.\u0001(13783));
        \u001C\u0005.\u007E\u0081((object) this.\u001F\u0005, \u009C.\u0001(13812));
        \u009C\u0006.\u007E\u0088((object) this.\u001F\u0005, true);
        \u0011\u0004.\u007E\u0087((object) this.\u001F\u0005, 0);
        \u0011\u0004.\u007E\u0089((object) this.\u001F\u0005, 254);
        \u009C\u0006.\u007E\u0004\u0003((object) this.\u007F\u0005, false);
        \u0005\u0005 obj43 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection27 = \u0091\u0004.\u007E\u000F\u0003((object) this.\u007F\u0005);
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray28 = editorButtonArray1;
        obj43((object) buttonCollection27, editorButtonArray28);
        \u001C\u0005.\u007E\u0002\u0003((object) this.\u007F\u0005, \u009C.\u0001(13829));
        \u001C\u0005.\u007E\u0083((object) this.\u0080\u0005, \u009C.\u0001(13850));
        \u0081\u0002.\u007E\u0086((object) this.\u0080\u0005, (RepositoryItem) this.\u0086\u0005);
        \u001C\u0005.\u007E\u0084((object) this.\u0080\u0005, \u009C.\u0001(13859));
        \u001C\u0005.\u007E\u0081((object) this.\u0080\u0005, \u009C.\u0001(13872));
        \u009C\u0006.\u007E\u0088((object) this.\u0080\u0005, true);
        \u0011\u0004.\u007E\u0087((object) this.\u0080\u0005, 1);
        \u0011\u0004.\u007E\u0089((object) this.\u0080\u0005, 99);
        \u0080\u0006.\u007E\u000E\u0003((object) this.\u0086\u0005, DefaultBoolean.True);
        \u009C\u0006.\u007E\u0004\u0003((object) this.\u0086\u0005, false);
        \u001C\u0005.\u007E\u0002\u0003((object) this.\u0086\u0005, \u009C.\u0001(13889));
        \u001C\u0005.\u007E\u0003\u0003((object) this.\u0086\u0005, \u009C.\u0001(13922));
        \u001C\u0005.\u007E\u0083((object) this.\u0081\u0005, \u009C.\u0001(13927));
        \u0081\u0002.\u007E\u0086((object) this.\u0081\u0005, (RepositoryItem) this.\u0086\u0005);
        \u001C\u0005.\u007E\u0084((object) this.\u0081\u0005, \u009C.\u0001(13936));
        \u001C\u0005.\u007E\u0081((object) this.\u0081\u0005, \u009C.\u0001(13949));
        \u009C\u0006.\u007E\u0088((object) this.\u0081\u0005, true);
        \u0011\u0004.\u007E\u0087((object) this.\u0081\u0005, 2);
        \u0011\u0004.\u007E\u0089((object) this.\u0081\u0005, 97);
        \u001C\u0005.\u007E\u0083((object) this.\u0082\u0005, \u009C.\u0001(13966));
        \u0081\u0002.\u007E\u0086((object) this.\u0082\u0005, (RepositoryItem) this.\u0086\u0005);
        \u001C\u0005.\u007E\u0084((object) this.\u0082\u0005, \u009C.\u0001(13975));
        \u001C\u0005.\u007E\u0081((object) this.\u0082\u0005, \u009C.\u0001(13988));
        \u009C\u0006.\u007E\u0088((object) this.\u0082\u0005, true);
        \u0011\u0004.\u007E\u0087((object) this.\u0082\u0005, 3);
        \u0011\u0004.\u007E\u0089((object) this.\u0082\u0005, 93);
        \u001C\u0005.\u007E\u0083((object) this.\u0083\u0005, \u009C.\u0001(14005));
        \u001C\u0005.\u007E\u0084((object) this.\u0083\u0005, \u009C.\u0001(14005));
        \u001C\u0005.\u007E\u0081((object) this.\u0083\u0005, \u009C.\u0001(14022));
        \u001C\u0005.\u007E\u0083((object) this.\u0084\u0005, \u009C.\u0001(14039));
        \u001C\u0005.\u007E\u0084((object) this.\u0084\u0005, \u009C.\u0001(14039));
        \u001C\u0005.\u007E\u0081((object) this.\u0084\u0005, \u009C.\u0001(14048));
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u001E\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u001F\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u007F\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0080\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0081\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0082\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0083\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0084\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0086\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0087\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0088\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0089\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u008A\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u008B\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u008C\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u008D\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u008E\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u008F\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0090\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0091\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0092\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0093\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0094\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0095\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0096\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0097\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0098\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0099\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u009A\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u009B\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u009C\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u009D\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u009E\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u009F\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0001\u0005);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0002\u0005);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0003\u0005);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0004\u0005);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0005\u0005);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0006\u0005);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0007\u0005);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0008\u0005);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u000E\u0005);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u000F\u0005);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0010\u0005);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0011\u0005);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0012\u0005);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0013\u0005);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0014\u0005);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0015\u0005);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u007E\u0005\u0010((object) this.\u0081), (Control) this.\u0016\u0005);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0081, \u009C.\u0001(14065));
        \u0087\u0006.\u007E\u0006\u0004((object) this.\u0081, new Size(706, 513));
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0081, \u009C.\u0001(14078));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u001E\u0004, new Point(414, 125));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u001E\u0004, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u001E\u0004, \u009C.\u0001(14103));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u001E\u0004, new Size(100, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u001E\u0004, 101);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u001F\u0004, new Point(414, 58));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u001F\u0004, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u001F\u0004, \u009C.\u0001(14116));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u001F\u0004, new Size(100, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u001F\u0004, 100);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u007F\u0004, new Point(63, 19));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u007F\u0004, \u009C.\u0001(14129));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u007F\u0004, new Size(95, 23));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u007F\u0004, 99);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u007F\u0004, \u009C.\u0001(14150));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0080\u0004, new Point(234, 433));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0080\u0004, \u009C.\u0001(14171));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0080\u0004, new Size(26, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0080\u0004, 98);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0080\u0004, \u009C.\u0001(14192));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0081\u0004, new Point(132, 433));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0081\u0004, \u009C.\u0001(14201));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0081\u0004, new Size(26, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0081\u0004, 97);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0081\u0004, \u009C.\u0001(14222));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0082\u0004, new Point(31, 433));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0082\u0004, \u009C.\u0001(14231));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0082\u0004, new Size(26, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0082\u0004, 96);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0082\u0004, \u009C.\u0001(14252));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0083\u0004, new Point(24, 407));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0083\u0004, \u009C.\u0001(14261));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0083\u0004, new Size(33, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0083\u0004, 95);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0083\u0004, \u009C.\u0001(14282));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0084\u0004, new Point(266, 430));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0084\u0004, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0084\u0004, \u009C.\u0001(14295));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0084\u0004, new Size(64, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0084\u0004, 94);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0086\u0004, new Point(164, 430));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0086\u0004, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0086\u0004, \u009C.\u0001(14312));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0086\u0004, new Size(64, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0086\u0004, 93);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0087\u0004, new Point(63, 430));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0087\u0004, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0087\u0004, \u009C.\u0001(14329));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0087\u0004, new Size(64, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0087\u0004, 92);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0088\u0004, new Point(63, 404));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0088\u0004, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0088\u0004, \u009C.\u0001(14346));
        \u0005\u0005 obj44 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection28 = \u0091\u0004.\u007E\u000F\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0088\u0004));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray29 = editorButtonArray1;
        obj44((object) buttonCollection28, editorButtonArray29);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0088\u0004, new Size(267, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0088\u0004, 91);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0089\u0004, new Point(234, 362));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0089\u0004, \u009C.\u0001(14359));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0089\u0004, new Size(26, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0089\u0004, 90);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0089\u0004, \u009C.\u0001(14192));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u008A\u0004, new Point(132, 362));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u008A\u0004, \u009C.\u0001(14380));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u008A\u0004, new Size(26, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u008A\u0004, 89);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u008A\u0004, \u009C.\u0001(14222));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u008B\u0004, new Point(31, 362));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u008B\u0004, \u009C.\u0001(14401));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u008B\u0004, new Size(26, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u008B\u0004, 88);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u008B\u0004, \u009C.\u0001(14252));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u008C\u0004, new Point(24, 336));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u008C\u0004, \u009C.\u0001(14422));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u008C\u0004, new Size(33, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u008C\u0004, 87);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u008C\u0004, \u009C.\u0001(14282));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u008D\u0004, new Point(266, 359));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u008D\u0004, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u008D\u0004, \u009C.\u0001(14443));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u008D\u0004, new Size(64, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u008D\u0004, 86);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u008E\u0004, new Point(164, 359));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u008E\u0004, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u008E\u0004, \u009C.\u0001(14460));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u008E\u0004, new Size(64, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u008E\u0004, 85);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u008F\u0004, new Point(63, 359));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u008F\u0004, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u008F\u0004, \u009C.\u0001(14477));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u008F\u0004, new Size(64, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u008F\u0004, 84);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0090\u0004, new Point(63, 333));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0090\u0004, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0090\u0004, \u009C.\u0001(14494));
        \u0005\u0005 obj45 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection29 = \u0091\u0004.\u007E\u000F\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0090\u0004));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray30 = editorButtonArray1;
        obj45((object) buttonCollection29, editorButtonArray30);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0090\u0004, new Size(267, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0090\u0004, 83);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0091\u0004, new Point(234, 293));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0091\u0004, \u009C.\u0001(14507));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0091\u0004, new Size(26, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0091\u0004, 82);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0091\u0004, \u009C.\u0001(14192));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0092\u0004, new Point(132, 293));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0092\u0004, \u009C.\u0001(14528));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0092\u0004, new Size(26, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0092\u0004, 81);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0092\u0004, \u009C.\u0001(14222));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0093\u0004, new Point(31, 293));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0093\u0004, \u009C.\u0001(14549));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0093\u0004, new Size(26, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0093\u0004, 80);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0093\u0004, \u009C.\u0001(14252));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0094\u0004, new Point(24, 267));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0094\u0004, \u009C.\u0001(14570));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0094\u0004, new Size(33, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0094\u0004, 79);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0094\u0004, \u009C.\u0001(14282));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0095\u0004, new Point(266, 290));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0095\u0004, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0095\u0004, \u009C.\u0001(14591));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0095\u0004, new Size(64, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0095\u0004, 78);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0096\u0004, new Point(164, 290));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0096\u0004, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0096\u0004, \u009C.\u0001(14608));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0096\u0004, new Size(64, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0096\u0004, 77);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0097\u0004, new Point(63, 290));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0097\u0004, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0097\u0004, \u009C.\u0001(14625));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0097\u0004, new Size(64, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0097\u0004, 76);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0098\u0004, new Point(63, 264));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0098\u0004, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0098\u0004, \u009C.\u0001(14642));
        \u0005\u0005 obj46 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection30 = \u0091\u0004.\u007E\u000F\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0098\u0004));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray31 = editorButtonArray1;
        obj46((object) buttonCollection30, editorButtonArray31);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0098\u0004, new Size(267, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0098\u0004, 75);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0099\u0004, new Point(234, 224));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0099\u0004, \u009C.\u0001(14655));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0099\u0004, new Size(26, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0099\u0004, 74);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0099\u0004, \u009C.\u0001(14192));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u009A\u0004, new Point(132, 224));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u009A\u0004, \u009C.\u0001(14676));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u009A\u0004, new Size(26, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u009A\u0004, 73);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u009A\u0004, \u009C.\u0001(14222));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u009B\u0004, new Point(31, 224));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u009B\u0004, \u009C.\u0001(14697));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u009B\u0004, new Size(26, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u009B\u0004, 72);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u009B\u0004, \u009C.\u0001(14252));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u009C\u0004, new Point(24, 198));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u009C\u0004, \u009C.\u0001(14718));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u009C\u0004, new Size(33, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u009C\u0004, 71);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u009C\u0004, \u009C.\u0001(14282));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u009D\u0004, new Point(266, 221));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u009D\u0004, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u009D\u0004, \u009C.\u0001(14739));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u009D\u0004, new Size(64, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u009D\u0004, 70);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u009E\u0004, new Point(164, 221));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u009E\u0004, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u009E\u0004, \u009C.\u0001(14756));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u009E\u0004, new Size(64, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u009E\u0004, 69);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u009F\u0004, new Point(63, 221));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u009F\u0004, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u009F\u0004, \u009C.\u0001(14773));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u009F\u0004, new Size(64, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u009F\u0004, 68);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0001\u0005, new Point(63, 195));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0001\u0005, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0001\u0005, \u009C.\u0001(14790));
        \u0005\u0005 obj47 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection31 = \u0091\u0004.\u007E\u000F\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0001\u0005));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray32 = editorButtonArray1;
        obj47((object) buttonCollection31, editorButtonArray32);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0001\u0005, new Size(267, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0001\u0005, 67);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0002\u0005, new Point(234, 154));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0002\u0005, \u009C.\u0001(14803));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0002\u0005, new Size(26, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0002\u0005, 66);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0002\u0005, \u009C.\u0001(14192));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0003\u0005, new Point(132, 154));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0003\u0005, \u009C.\u0001(14824));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0003\u0005, new Size(26, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0003\u0005, 65);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0003\u0005, \u009C.\u0001(14222));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0004\u0005, new Point(31, 154));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0004\u0005, \u009C.\u0001(14845));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0004\u0005, new Size(26, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0004\u0005, 64);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0004\u0005, \u009C.\u0001(14252));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0005\u0005, new Point(24, 128));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0005\u0005, \u009C.\u0001(14866));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0005\u0005, new Size(33, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0005\u0005, 63);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0005\u0005, \u009C.\u0001(14282));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0006\u0005, new Point(266, 151));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0006\u0005, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0006\u0005, \u009C.\u0001(14887));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0006\u0005, new Size(64, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0006\u0005, 62);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0007\u0005, new Point(164, 151));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0007\u0005, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0007\u0005, \u009C.\u0001(14904));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0007\u0005, new Size(64, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0007\u0005, 61);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0008\u0005, new Point(63, 151));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0008\u0005, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0008\u0005, \u009C.\u0001(14921));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0008\u0005, new Size(64, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0008\u0005, 60);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u000E\u0005, new Point(63, 125));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u000E\u0005, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u000E\u0005, \u009C.\u0001(14938));
        \u0005\u0005 obj48 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection32 = \u0091\u0004.\u007E\u000F\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u000E\u0005));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray33 = editorButtonArray1;
        obj48((object) buttonCollection32, editorButtonArray33);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u000E\u0005, new Size(267, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u000E\u0005, 59);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u000F\u0005, new Point(234, 87));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u000F\u0005, \u009C.\u0001(14951));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u000F\u0005, new Size(26, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u000F\u0005, 58);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u000F\u0005, \u009C.\u0001(14192));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0010\u0005, new Point(132, 87));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0010\u0005, \u009C.\u0001(14972));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0010\u0005, new Size(26, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0010\u0005, 57);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0010\u0005, \u009C.\u0001(14222));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0011\u0005, new Point(31, 87));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0011\u0005, \u009C.\u0001(14993));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0011\u0005, new Size(26, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0011\u0005, 56);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0011\u0005, \u009C.\u0001(14252));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0012\u0005, new Point(24, 61));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0012\u0005, \u009C.\u0001(15014));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0012\u0005, new Size(33, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0012\u0005, 55);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0012\u0005, \u009C.\u0001(14282));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0013\u0005, new Point(266, 84));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0013\u0005, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0013\u0005, \u009C.\u0001(15035));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0013\u0005, new Size(64, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0013\u0005, 54);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0014\u0005, new Point(164, 84));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0014\u0005, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0014\u0005, \u009C.\u0001(15052));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0014\u0005, new Size(64, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0014\u0005, 53);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0015\u0005, new Point(63, 84));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0015\u0005, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0015\u0005, \u009C.\u0001(15069));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0015\u0005, new Size(64, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0015\u0005, 52);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0016\u0005, new Point(63, 58));
        \u001A\u0006.\u007E\u0010\u0003((object) this.\u0016\u0005, (IDXMenuManager) this.\u0002);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0016\u0005, \u009C.\u0001(15086));
        \u0005\u0005 obj49 = \u0005\u0005.\u007E\u001B\u0003;
        EditorButtonCollection buttonCollection33 = \u0091\u0004.\u007E\u000F\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0016\u0005));
        editorButtonArray1 = new EditorButton[1]
        {
          new EditorButton(ButtonPredefines.Combo)
        };
        EditorButton[] editorButtonArray34 = editorButtonArray1;
        obj49((object) buttonCollection33, editorButtonArray34);
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0016\u0005, new Size(267, 20));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0016\u0005, 51);
        \u001C\u0005.\u007E\u001A\u0004((object) \u007F\u0004.\u007E\u001C\u0004((object) this.\u0006\u0004), \u009C.\u0001(8592));
        \u009D\u0002 obj50 = \u009D\u0002.\u007E\u009A\u0002;
        LinksInfo linksInfo3 = \u0008\u0005.\u007E\u0095\u0002((object) this.\u001B\u0005);
        linkPersistInfoArray1 = new LinkPersistInfo[2]
        {
          new LinkPersistInfo((BarItem) this.\u0019\u0005),
          new LinkPersistInfo((BarItem) this.\u001A\u0005)
        };
        LinkPersistInfo[] linkPersistInfoArray4 = linkPersistInfoArray1;
        obj50((object) linksInfo3, linkPersistInfoArray4);
        \u0090\u0004.\u007E\u0094\u0002((object) this.\u001B\u0005, this.\u0002);
        \u001C\u0005.\u007E\u0093\u0002((object) this.\u001B\u0005, \u009C.\u0001(15099));
        \u008B\u0004.\u009E\u0010((object) this, new SizeF(6f, 13f));
        \u0002\u0004.\u009F\u0010((object) this, AutoScaleMode.Font);
        \u0087\u0006.\u0016\u0011((object) this, new Size(710, 629));
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u0005\u0010((object) this), (Control) this.\u0016);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u0005\u0010((object) this), (Control) this.\u0007);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u0005\u0010((object) this), (Control) this.\u0008);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u0005\u0010((object) this), (Control) this.\u0006);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u0005\u0010((object) this), (Control) this.\u0005);
        \u0095\u0004.\u0018\u0011((object) this, (Icon) \u008E\u0002.\u007E\u0098\u0008((object) componentResourceManager, \u009C.\u0001(8575)));
        \u0087\u0006.\u007E\u0018\u0010((object) this, new Size(626, 667));
        \u001C\u0005.\u0019\u0010((object) this, \u009C.\u0001(15120));
        \u001C\u0005.\u007E\u007F\u0010((object) this, \u009C.\u0001(15129));
        \u001D\u0005.\u007F\u0011((object) this, new EventHandler(this.\u0095\u0003));
        \u001B\u0006.\u007E\u009E\u0011((object) this.\u0002);
        \u001B\u0006.\u007E\u009E\u0011((object) this.\u0014);
        \u001B\u0006.\u007E\u009E\u0011((object) this.\u0016);
        \u009C\u0006.\u007E\u0095\u0010((object) this.\u0016, false);
        \u009C\u0006.\u007E\u0095\u0010((object) this.\u0017, false);
        \u001B\u0006.\u007E\u0093\u0010((object) this.\u0017);
        \u001B\u0006.\u007E\u009E\u0011((object) this.\u000E\u0004);
        \u009C\u0006.\u007E\u0095\u0010((object) this.\u000E\u0004, false);
        \u001B\u0006.\u007E\u0093\u0010((object) this.\u000E\u0004);
        \u001B\u0006.\u007E\u009E\u0011((object) \u001C\u0003.\u007E\u0003\u0004((object) this.\u001A\u0004));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u000F\u0004));
        \u001B\u0006.\u007E\u009E\u0011((object) \u001C\u0003.\u007E\u0003\u0004((object) this.\u001C\u0004));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0011\u0004));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0007\u0004));
        \u001B\u0006.\u007E\u009E\u0011((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u0017\u0003));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0011\u0003));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0082\u0006.\u007E\u008B\u0003((object) this.\u0008\u0002));
        \u001B\u0006.\u007E\u009E\u0011((object) this.\u0007\u0002);
        \u009C\u0006.\u007E\u0095\u0010((object) this.\u0007\u0002, false);
        \u001B\u0006.\u007E\u009E\u0011((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u000E\u0002));
        \u001B\u0006.\u007E\u009E\u0011((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u000F\u0002));
        \u001B\u0006.\u007E\u009E\u0011((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u0010\u0002));
        \u001B\u0006.\u007E\u009E\u0011((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u0011\u0002));
        \u001B\u0006.\u007E\u009E\u0011((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u0012\u0002));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0001\u0002));
        \u001B\u0006.\u007E\u009E\u0011((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u0003\u0002));
        \u001B\u0006.\u007E\u009E\u0011((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u0004\u0002));
        \u001B\u0006.\u007E\u009E\u0011((object) this.\u0097);
        \u009C\u0006.\u007E\u0095\u0010((object) this.\u0097, false);
        \u001B\u0006.\u007E\u0093\u0010((object) this.\u0097);
        \u001B\u0006.\u007E\u009E\u0011((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u0098));
        \u001B\u0006.\u007E\u009E\u0011((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u0099));
        \u001B\u0006.\u007E\u009E\u0011((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u009A));
        \u001B\u0006.\u007E\u009E\u0011((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u009B));
        \u001B\u0006.\u007E\u009E\u0011((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u009C));
        \u001B\u0006.\u007E\u009E\u0011((object) \u008A\u0003.\u007E\u0081\u0003((object) this.\u009D));
        \u001B\u0006.\u007E\u009E\u0011((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u0094));
        \u001B\u0006.\u007E\u009E\u0011((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u0095));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u008D));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0084\u0002.\u007E\u0090\u0003((object) this.\u008E));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u008F));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0090));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0091));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0092));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0082));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0083));
        \u009C\u0006.\u007E\u0095\u0010((object) this.\u0018, false);
        \u001B\u0006.\u007E\u0093\u0010((object) this.\u0018);
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0014\u0002));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0015\u0002));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0016\u0002));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0017\u0002));
        \u001B\u0006.\u007E\u009E\u0011((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u0018\u0002));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u001F\u0002));
        \u009C\u0006.\u007E\u0095\u0010((object) this.\u0019, false);
        \u001B\u0006.\u007E\u0093\u0010((object) this.\u0019);
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0080\u0002));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0081\u0002));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0082\u0002));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0083\u0002));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u008A\u0002));
        \u009C\u0006.\u007E\u0095\u0010((object) this.\u001A, false);
        \u001B\u0006.\u007E\u0093\u0010((object) this.\u001A);
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u008B\u0002));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u008C\u0002));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u008D\u0002));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0092\u0002));
        \u009C\u0006.\u007E\u0095\u0010((object) this.\u001B, false);
        \u001B\u0006.\u007E\u0093\u0010((object) this.\u001B);
        \u001B\u0006.\u007E\u009E\u0011((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u001D\u0003));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0093\u0002));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0096\u0002));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0097\u0002));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u009A\u0002));
        \u009C\u0006.\u007E\u0095\u0010((object) this.\u001C, false);
        \u001B\u0006.\u007E\u0093\u0010((object) this.\u001C);
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u009B\u0002));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u009C\u0002));
        \u009C\u0006.\u007E\u0095\u0010((object) this.\u001D, false);
        \u001B\u0006.\u007E\u0093\u0010((object) this.\u001D);
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u009F\u0002));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0001\u0003));
        \u009C\u0006.\u007E\u0095\u0010((object) this.\u0018\u0003, false);
        \u001B\u0006.\u007E\u0093\u0010((object) this.\u0018\u0003);
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0019\u0003));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u001A\u0003));
        \u009C\u0006.\u007E\u0095\u0010((object) this.\u001E, false);
        \u001B\u0006.\u007E\u0093\u0010((object) this.\u001E);
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0004\u0003));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0005\u0003));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0006\u0003));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0007\u0003));
        \u009C\u0006.\u007E\u0095\u0010((object) this.\u007F, false);
        \u001B\u0006.\u007E\u0093\u0010((object) this.\u007F);
        \u001B\u0006.\u007E\u009E\u0011((object) this.\u0013\u0004);
        \u009C\u0006.\u007E\u0095\u0010((object) this.\u0013\u0004, false);
        \u001B\u0006.\u007E\u0093\u0010((object) this.\u0013\u0004);
        \u001B\u0006.\u007E\u009E\u0011((object) \u001C\u0003.\u007E\u0003\u0004((object) this.\u0014\u0004));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0016\u0004));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0018\u0004));
        \u001B\u0006.\u007E\u009E\u0011((object) this.\u0098\u0003);
        \u009C\u0006.\u007E\u0095\u0010((object) this.\u0098\u0003, false);
        \u001B\u0006.\u007E\u0093\u0010((object) this.\u0098\u0003);
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u009F\u0003));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0001\u0004));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0002\u0004));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0091\u0006.\u007E\u009D\u0003((object) this.\u0003\u0004));
        \u001B\u0006.\u007E\u009E\u0011((object) this.\u0003\u0004);
        \u001B\u0006.\u007E\u009E\u0011((object) \u0091\u0006.\u007E\u009D\u0003((object) this.\u0004\u0004));
        \u001B\u0006.\u007E\u009E\u0011((object) this.\u0004\u0004);
        \u001B\u0006.\u007E\u009E\u0011((object) \u0091\u0006.\u007E\u009D\u0003((object) this.\u0005\u0004));
        \u001B\u0006.\u007E\u009E\u0011((object) this.\u0005\u0004);
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0092\u0003));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0093\u0003));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0094\u0003));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0095\u0003));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0096\u0003));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0097\u0003));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u001F\u0003));
        \u001B\u0006.\u007E\u009E\u0011((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u0080\u0003));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0081\u0003));
        \u001B\u0006.\u007E\u009E\u0011((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u0083\u0003));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0084\u0003));
        \u001B\u0006.\u007E\u009E\u0011((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u0087\u0003));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0088\u0003));
        \u001B\u0006.\u007E\u009E\u0011((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u008A\u0003));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u008B\u0003));
        \u001B\u0006.\u007E\u009E\u0011((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u008D\u0003));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u008E\u0003));
        \u001B\u0006.\u007E\u009E\u0011((object) \u008B\u0005.\u007E\u0017\u0003((object) this.\u0090\u0003));
        \u009C\u0006.\u007E\u0095\u0010((object) this.\u0080, false);
        \u001B\u0006.\u007E\u009E\u0011((object) this.\u001D\u0005);
        \u001B\u0006.\u007E\u009E\u0011((object) this.\u001E\u0005);
        \u001B\u0006.\u007E\u009E\u0011((object) this.\u007F\u0005);
        \u001B\u0006.\u007E\u009E\u0011((object) this.\u0086\u0005);
        \u009C\u0006.\u007E\u0095\u0010((object) this.\u0081, false);
        \u001B\u0006.\u007E\u0093\u0010((object) this.\u0081);
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u001E\u0004));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u001F\u0004));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0084\u0004));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0086\u0004));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0087\u0004));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0088\u0004));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u008D\u0004));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u008E\u0004));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u008F\u0004));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0090\u0004));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0095\u0004));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0096\u0004));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0097\u0004));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0098\u0004));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u009D\u0004));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u009E\u0004));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u009F\u0004));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0001\u0005));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0006\u0005));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0007\u0005));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0008\u0005));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u000E\u0005));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0013\u0005));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0014\u0005));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0015\u0005));
        \u001B\u0006.\u007E\u009E\u0011((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0016\u0005));
        \u001B\u0006.\u007E\u009E\u0011((object) this.\u001B\u0005);
        \u009C\u0006.\u0010\u0004((object) this, false);
      }
      catch (Exception ex)
      {
        object[] objArray2 = new object[12]
        {
          (object) componentResourceManager,
          (object) barArray1,
          (object) barItemArray1,
          (object) repositoryItemArray1,
          (object) linkPersistInfoArray1,
          (object) xtraTabPageArray1,
          (object) bits,
          (object) editorButtonArray1,
          (object) objArray1,
          (object) baseViewArray1,
          (object) gridColumnArray1,
          (object) this
        };
        throw UnhandledException.\u0017\u0003(ex, objArray2);
      }
    }

    public \u009C([In] ref \u0098 obj0)
    {
      try
      {
        this.\u0081\u0003();
        this.\u0093\u0005 = obj0;
      }
      catch (Exception ex)
      {
        \u0098 obj = obj0;
        throw UnhandledException.\u000E\u0003(ex, (object) this, (object) obj);
      }
    }

    private void \u0015\u0006([In] object obj0, [In] EventArgs obj1)
    {
      bool flag;
      int num;
      try
      {
        flag = !\u009E\u0002.\u0012\u0005(\u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u008C\u0002((object) this.\u0013)), \u009C.\u0001(8361));
        if (flag)
          return;
        flag = !\u000E\u0003.\u008B\u0006(\u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u008C\u0002((object) this.\u0013)), ref num);
        if (!flag)
          this.\u0016\u0006(num);
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<int> local1 = (ValueType) num;
        // ISSUE: variable of a boxed type
        __Boxed<bool> local2 = (ValueType) flag;
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0011\u0003(ex, (object) local1, (object) local2, (object) this, obj, (object) eventArgs);
      }
    }

    public void \u0016\u0006([In] int obj0)
    {
      bool flag;
      string key;
      int num1;
      try
      {
        this.\u0092\u0005 = obj0;
        \u0080\u0002.\u007E\u008D\u0002((object) this.\u0013, (object) obj0.ToString());
        \u001B\u0006.\u007E\u0018\u0012((object) this.\u0087\u0005);
        \u008C\u0006.\u007E\u001A\u0012((object) this.\u0087\u0005, \u0017\u0002.\u008F\u0005(\u008F\u0006.\u0084\u0005(\u009C.\u0001(15146), obj0.ToString(), \u009C.\u0001(16076)), \u009C.\u0001(8352), \u009C.\u0001(16081)));
        flag = \u0083\u0006.\u007E\u0010\u0012((object) \u009D\u0004.\u007E\u001C\u0012((object) \u0012\u0003.\u007E\u001F\u0012((object) \u0099\u0002.\u007E\u0017\u0012((object) this.\u0087\u0005), \u009C.\u0001(16081)))) != 0;
        if (!flag)
        {
          int num2 = (int) \u0095\u0003.\u008C\u0011(\u009C.\u0001(16094), \u009C.\u0001(4053), MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }
        else
        {
          flag = \u0083\u0006.\u007E\u0001\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0083)) != 0;
          if (!flag)
          {
            Binding binding1 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0083), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(16132), true, DataSourceUpdateMode.OnPropertyChanged);
            Binding binding2 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u008E), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(16149), true, DataSourceUpdateMode.OnPropertyChanged);
            Binding binding3 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0092), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(16170), true, DataSourceUpdateMode.OnPropertyChanged);
            Binding binding4 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0091), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(16195), true, DataSourceUpdateMode.OnPropertyChanged);
            Binding binding5 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0090), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(16228), true, DataSourceUpdateMode.OnPropertyChanged);
            Binding binding6 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u008D), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(16249), true, DataSourceUpdateMode.OnPropertyChanged);
            Binding binding7 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0082), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(16278), true, DataSourceUpdateMode.OnPropertyChanged);
            Binding binding8 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0008\u0002), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(16299), true, DataSourceUpdateMode.OnPropertyChanged);
            Binding binding9 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u008F), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(16328), true, DataSourceUpdateMode.OnPropertyChanged);
            Binding binding10 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0004\u0002), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(16361), true, DataSourceUpdateMode.OnPropertyChanged);
            Binding binding11 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0003\u0002), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(16394), true, DataSourceUpdateMode.OnPropertyChanged);
            Binding binding12 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0095), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(16423), true, DataSourceUpdateMode.OnPropertyChanged);
            Binding binding13 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0094), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(16448), true, DataSourceUpdateMode.OnPropertyChanged);
            Binding binding14 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0011\u0003), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(16473), true, DataSourceUpdateMode.OnPropertyChanged);
            Binding binding15 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0012\u0002), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(16498), true, DataSourceUpdateMode.OnPropertyChanged);
            Binding binding16 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u000F\u0002), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(16523), true, DataSourceUpdateMode.OnPropertyChanged);
            Binding binding17 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0011\u0002), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(16548), true, DataSourceUpdateMode.OnPropertyChanged);
            Binding binding18 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0010\u0002), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(16577), true, DataSourceUpdateMode.OnPropertyChanged);
            Binding binding19 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u000E\u0002), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(16602), true, DataSourceUpdateMode.OnPropertyChanged);
            Binding binding20 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0017\u0003), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(16627), true, DataSourceUpdateMode.OnPropertyChanged);
            Binding binding21 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0001\u0002), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(16656), true, DataSourceUpdateMode.OnPropertyChanged);
            Binding binding22 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u000F\u0004), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(16677), true, DataSourceUpdateMode.OnPropertyChanged);
            Binding binding23 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0011\u0004), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(16710), true, DataSourceUpdateMode.OnPropertyChanged);
            Binding binding24 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u001C\u0004), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(16743), true, DataSourceUpdateMode.OnPropertyChanged);
            Binding binding25 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u001A\u0004), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(16768), true, DataSourceUpdateMode.OnPropertyChanged);
            Binding binding26 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0005\u0004), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(16793), true, DataSourceUpdateMode.OnPropertyChanged);
            Binding binding27 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0004\u0004), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(16818), true, DataSourceUpdateMode.OnPropertyChanged);
            Binding binding28 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0003\u0004), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(16843), true, DataSourceUpdateMode.OnPropertyChanged);
            Binding binding29 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0014\u0004), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(16868), true, DataSourceUpdateMode.OnPropertyChanged);
            Binding binding30 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0018\u0004), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(16901), true, DataSourceUpdateMode.OnPropertyChanged);
            Binding binding31 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0016\u0004), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(16934), true, DataSourceUpdateMode.OnPropertyChanged);
          }
          this.\u0017\u0006();
          key = \u0013\u0007.\u007E\u009C\u0004(\u008E\u0002.\u007E\u0011\u0012((object) \u0095\u0006.\u007E\u0015\u0012((object) \u009D\u0004.\u007E\u001C\u0012((object) \u0012\u0003.\u007E\u001F\u0012((object) \u0099\u0002.\u007E\u0017\u0012((object) this.\u0087\u0005), \u009C.\u0001(16081))), 0), \u009C.\u0001(16959)));
          if (key != null)
          {
            // ISSUE: reference to a compiler-generated field
            if (\u0003\u0011.\u0001 == null)
            {
              // ISSUE: reference to a compiler-generated field
              \u0003\u0011.\u0001 = new Dictionary<string, int>(8)
              {
                {
                  \u009C.\u0001(16968),
                  0
                },
                {
                  \u009C.\u0001(16973),
                  1
                },
                {
                  \u009C.\u0001(10390),
                  2
                },
                {
                  \u009C.\u0001(10415),
                  3
                },
                {
                  \u009C.\u0001(10410),
                  4
                },
                {
                  \u009C.\u0001(16978),
                  5
                },
                {
                  \u009C.\u0001(16983),
                  6
                },
                {
                  \u009C.\u0001(16988),
                  7
                }
              };
            }
            // ISSUE: reference to a compiler-generated field
            // ISSUE: explicit non-virtual call
            if (__nonvirtual (\u0003\u0011.\u0001.TryGetValue(key, out num1)))
            {
              switch (num1)
              {
                case 0:
                  this.\u0080\u0006(obj0);
                  break;
                case 1:
                  this.\u0081\u0006(obj0);
                  break;
                case 2:
                  this.\u0082\u0006(obj0);
                  break;
                case 3:
                  this.\u0083\u0006(obj0);
                  break;
                case 4:
                  this.\u0084\u0006(obj0);
                  break;
                case 5:
                  this.\u0086\u0006(obj0);
                  break;
                case 6:
                  this.\u0087\u0006(obj0);
                  break;
                case 7:
                  this.\u0088\u0006(obj0);
                  break;
              }
            }
          }
          this.\u0089\u0006(obj0);
          this.\u008A\u0006(obj0);
        }
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<bool> local1 = (ValueType) flag;
        string str = key;
        // ISSUE: variable of a boxed type
        __Boxed<int> local2 = (ValueType) num1;
        // ISSUE: variable of a boxed type
        __Boxed<int> local3 = (ValueType) obj0;
        throw UnhandledException.\u0011\u0003(ex, (object) local1, (object) str, (object) local2, (object) this, (object) local3);
      }
    }

    private void \u0095\u0003([In] object obj0, [In] EventArgs obj1)
    {
      DataTable dataTable1;
      try
      {
        this.\u0093\u0005.\u0083\u0003(\u009C.\u0001(16993));
        this.\u0088\u0005 = \u0017\u0002.\u0090\u0005(\u009C.\u0001(17018));
        this.\u0093\u0005.\u0083\u0003(\u009C.\u0001(17516));
        this.\u0089\u0005 = \u0017\u0002.\u0090\u0005(\u009C.\u0001(17545));
        \u0080\u0002.\u007E\u0093\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0090), (object) this.\u0089\u0005);
        \u001C\u0005.\u007E\u0094\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0090), \u009C.\u0001(17634));
        \u001C\u0005.\u007E\u0095\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0090), \u009C.\u0001(17639));
        this.\u0093\u0005.\u0083\u0003(\u009C.\u0001(17648));
        this.\u008B\u0005 = \u0017\u0002.\u0090\u0005(\u009C.\u0001(17685));
        \u0080\u0002.\u007E\u0093\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0092), (object) this.\u008B\u0005);
        \u001C\u0005.\u007E\u0094\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0092), \u009C.\u0001(17634));
        \u001C\u0005.\u007E\u0095\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0092), \u009C.\u0001(17782));
        this.\u0093\u0005.\u0083\u0003(\u009C.\u0001(17795));
        this.\u008A\u0005 = \u0017\u0002.\u0090\u0005(\u009C.\u0001(17836));
        \u0080\u0002.\u007E\u0093\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0091), (object) this.\u008A\u0005);
        \u001C\u0005.\u007E\u0094\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0091), \u009C.\u0001(17634));
        \u001C\u0005.\u007E\u0095\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0091), \u009C.\u0001(17949));
        this.\u0093\u0005.\u0083\u0003(\u009C.\u0001(17966));
        dataTable1 = \u0017\u0002.\u0090\u0005(\u009C.\u0001(17999));
        \u001C\u0005.\u007E\u0095\u0003((object) this.\u007F\u0005, \u009C.\u0001(3609));
        \u001C\u0005.\u007E\u0094\u0003((object) this.\u007F\u0005, \u009C.\u0001(18104));
        \u0080\u0002.\u007E\u0093\u0003((object) this.\u007F\u0005, (object) dataTable1);
        this.\u0093\u0005.\u0083\u0003(\u009C.\u0001(18117));
        this.\u008C\u0005 = \u0017\u0002.\u0090\u0005(\u009C.\u0001(18158));
        \u0080\u0002.\u007E\u0093\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u008F), (object) this.\u008C\u0005);
        \u001C\u0005.\u007E\u0094\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u008F), \u009C.\u0001(17634));
        \u001C\u0005.\u007E\u0095\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u008F), \u009C.\u0001(17639));
        this.\u008D\u0005 = \u0017\u0002.\u0090\u0005(\u009C.\u0001(18279));
        \u0080\u0002.\u007E\u0093\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0011\u0003), (object) this.\u008D\u0005);
        \u001C\u0005.\u007E\u0094\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0011\u0003), \u009C.\u0001(17634));
        \u001C\u0005.\u007E\u0095\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0011\u0003), \u009C.\u0001(18413));
        this.\u008F\u0005 = \u0017\u0002.\u0090\u0005(\u009C.\u0001(18422));
        this.\u008E\u0005 = \u0017\u0002.\u0090\u0005(\u009C.\u0001(18511));
        this.\u0093\u0005.\u0083\u0003(\u009C.\u0001(18592));
        \u0080\u0002.\u007E\u0093\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u001F\u0002), (object) this.\u008E\u0005);
        \u001C\u0005.\u007E\u0094\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u001F\u0002), \u009C.\u0001(17634));
        \u001C\u0005.\u007E\u0095\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u001F\u0002), \u009C.\u0001(18637));
        \u0080\u0002.\u007E\u0093\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0096\u0002), (object) this.\u008E\u0005);
        \u001C\u0005.\u007E\u0094\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0096\u0002), \u009C.\u0001(17634));
        \u001C\u0005.\u007E\u0095\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0096\u0002), \u009C.\u0001(18637));
        \u0080\u0002.\u007E\u0093\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u008A\u0002), (object) this.\u008F\u0005);
        \u001C\u0005.\u007E\u0094\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u008A\u0002), \u009C.\u0001(17634));
        \u001C\u0005.\u007E\u0095\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u008A\u0002), \u009C.\u0001(17639));
        \u0080\u0002.\u007E\u0093\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0092\u0002), (object) this.\u008F\u0005);
        \u001C\u0005.\u007E\u0094\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0092\u0002), \u009C.\u0001(17634));
        \u001C\u0005.\u007E\u0095\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u0092\u0002), \u009C.\u0001(17639));
        \u0080\u0002.\u007E\u0093\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u009A\u0002), (object) this.\u008F\u0005);
        \u001C\u0005.\u007E\u0094\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u009A\u0002), \u009C.\u0001(17634));
        \u001C\u0005.\u007E\u0095\u0003((object) \u0081\u0005.\u007E\u0098\u0003((object) this.\u009A\u0002), \u009C.\u0001(17639));
        this.\u0017\u0006();
        this.\u0016\u0006(\u0088\u0006.\u008A\u0006(\u0013\u0007.\u007E\u009C\u0004(\u008E\u0002.\u007E\u0011\u0012((object) \u0095\u0006.\u007E\u0015\u0012((object) \u009D\u0004.\u007E\u001C\u0012((object) this.\u0088\u0005), 0), \u009C.\u0001(17634)))));
        \u001B\u0006.\u007E\u0080\u0011((object) this.\u0093\u0005);
      }
      catch (Exception ex)
      {
        DataTable dataTable2 = dataTable1;
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0010\u0003(ex, (object) dataTable2, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u0017\u0006()
    {
      try
      {
        \u009C\u0006.\u007E\u0005\u0004((object) this.\u0018, false);
        \u009C\u0006.\u007E\u0005\u0004((object) this.\u0081, false);
        \u009C\u0006.\u007E\u0005\u0004((object) this.\u001B, false);
        \u009C\u0006.\u007E\u0005\u0004((object) this.\u001C, false);
        \u009C\u0006.\u007E\u0005\u0004((object) this.\u0080, false);
        \u009C\u0006.\u007E\u0005\u0004((object) this.\u001E, false);
        \u009C\u0006.\u007E\u0005\u0004((object) this.\u007F, false);
        \u009C\u0006.\u007E\u0005\u0004((object) this.\u001A, false);
        \u009C\u0006.\u007E\u0005\u0004((object) this.\u0019, false);
        \u009C\u0006.\u007E\u0005\u0004((object) this.\u001D, false);
        \u009C\u0006.\u007E\u0005\u0004((object) this.\u001F, false);
        \u009C\u0006.\u007E\u0005\u0004((object) this.\u0018\u0003, false);
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0008\u0003(ex, (object) this);
      }
    }

    private void \u009A\u0003([In] object obj0, [In] ItemClickEventArgs obj1)
    {
      bool flag;
      try
      {
        flag = \u0097\u0005.\u007E\u008C\u0002((object) this.\u0013) == null || !\u009E\u0002.\u0012\u0005(\u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u008C\u0002((object) this.\u0013)), \u009C.\u0001(8361));
        if (flag)
          return;
        this.\u0016\u0006(\u0088\u0006.\u008A\u0006(\u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u008C\u0002((object) this.\u0013))));
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<bool> local = (ValueType) flag;
        object obj = obj0;
        ItemClickEventArgs itemClickEventArgs = obj1;
        throw UnhandledException.\u0010\u0003(ex, (object) local, (object) this, obj, (object) itemClickEventArgs);
      }
    }

    private void \u0018\u0006([In] object obj0, [In] ItemClickEventArgs obj1)
    {
      string key;
      int num1;
      try
      {
        \u001B\u0006.\u007E\u0010((object) this.\u001E\u0005);
        int num2 = \u0006\u0007.\u007E\u000F((object) this.\u001E\u0005) ? 1 : 0;
        \u0017\u0002.\u008D\u0005(this.\u0087\u0005);
        \u0017\u0002.\u008E\u0005(\u0012\u0003.\u007E\u001F\u0012((object) \u0099\u0002.\u007E\u0017\u0012((object) this.\u0087\u0005), \u009C.\u0001(16081)), \u009C.\u0001(18646));
        key = \u0013\u0007.\u007E\u009C\u0004(\u008E\u0002.\u007E\u0011\u0012((object) \u0095\u0006.\u007E\u0015\u0012((object) \u009D\u0004.\u007E\u001C\u0012((object) \u0012\u0003.\u007E\u001F\u0012((object) \u0099\u0002.\u007E\u0017\u0012((object) this.\u0087\u0005), \u009C.\u0001(16081))), 0), \u009C.\u0001(16959)));
        if (key != null)
        {
          // ISSUE: reference to a compiler-generated field
          if (\u0003\u0011.\u0002 == null)
          {
            // ISSUE: reference to a compiler-generated field
            \u0003\u0011.\u0002 = new Dictionary<string, int>(8)
            {
              {
                \u009C.\u0001(16968),
                0
              },
              {
                \u009C.\u0001(16973),
                1
              },
              {
                \u009C.\u0001(10390),
                2
              },
              {
                \u009C.\u0001(10415),
                3
              },
              {
                \u009C.\u0001(10410),
                4
              },
              {
                \u009C.\u0001(16978),
                5
              },
              {
                \u009C.\u0001(16983),
                6
              },
              {
                \u009C.\u0001(16988),
                7
              }
            };
          }
          // ISSUE: reference to a compiler-generated field
          // ISSUE: explicit non-virtual call
          if (__nonvirtual (\u0003\u0011.\u0002.TryGetValue(key, out num1)))
          {
            switch (num1)
            {
              case 0:
                \u0017\u0002.\u008E\u0005(\u0012\u0003.\u007E\u001F\u0012((object) \u0099\u0002.\u007E\u0017\u0012((object) this.\u0087\u0005), \u009C.\u0001(19544)), \u009C.\u0001(19557));
                break;
              case 1:
                \u0017\u0002.\u008E\u0005(\u0012\u0003.\u007E\u001F\u0012((object) \u0099\u0002.\u007E\u0017\u0012((object) this.\u0087\u0005), \u009C.\u0001(19835)), \u009C.\u0001(19848));
                break;
              case 2:
                \u0017\u0002.\u008E\u0005(\u0012\u0003.\u007E\u001F\u0012((object) \u0099\u0002.\u007E\u0017\u0012((object) this.\u0087\u0005), \u009C.\u0001(20074)), \u009C.\u0001(20091));
                break;
              case 3:
                \u0017\u0002.\u008E\u0005(\u0012\u0003.\u007E\u001F\u0012((object) \u0099\u0002.\u007E\u0017\u0012((object) this.\u0087\u0005), \u009C.\u0001(20329)), \u009C.\u0001(20346));
                break;
              case 4:
                \u0017\u0002.\u008E\u0005(\u0012\u0003.\u007E\u001F\u0012((object) \u0099\u0002.\u007E\u0017\u0012((object) this.\u0087\u0005), \u009C.\u0001(20596)), \u009C.\u0001(20613));
                break;
              case 5:
                \u0017\u0002.\u008E\u0005(\u0012\u0003.\u007E\u001F\u0012((object) \u0099\u0002.\u007E\u0017\u0012((object) this.\u0087\u0005), \u009C.\u0001(20915)), \u009C.\u0001(20932));
                break;
              case 6:
                \u0017\u0002.\u008E\u0005(\u0012\u0003.\u007E\u001F\u0012((object) \u0099\u0002.\u007E\u0017\u0012((object) this.\u0087\u0005), \u009C.\u0001(21214)), \u009C.\u0001(21235));
                break;
            }
          }
        }
        \u0017\u0002.\u008E\u0005(\u0012\u0003.\u007E\u001F\u0012((object) \u0099\u0002.\u007E\u0017\u0012((object) this.\u0087\u0005), \u009C.\u0001(21589)), \u009C.\u0001(21614));
        \u0017\u0002.\u008E\u0005(\u0012\u0003.\u007E\u001F\u0012((object) \u0099\u0002.\u007E\u0017\u0012((object) this.\u0087\u0005), \u009C.\u0001(21928)), \u009C.\u0001(21945));
      }
      catch (Exception ex)
      {
        string str = key;
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) num1;
        object obj = obj0;
        ItemClickEventArgs itemClickEventArgs = obj1;
        throw UnhandledException.\u0011\u0003(ex, (object) str, (object) local, (object) this, obj, (object) itemClickEventArgs);
      }
    }

    private void \u0019\u0006([In] object obj0, [In] ItemClickEventArgs obj1)
    {
      bool flag;
      try
      {
        \u001B\u0006.\u007E\u0099\u0010((object) this.\u0091\u0005);
        this.\u0091\u0005.\u0097\u0006(this.\u0092\u0005);
        \u001B\u0006.\u007E\u008D\u0010((object) this.\u0091\u0005);
        int num = (int) \u0096\u0002.\u007E\u0083\u0011((object) this.\u0091\u0005);
        flag = this.\u0092\u0005 == this.\u0091\u0005.\u0098\u0006();
        if (flag)
          return;
        this.\u0016\u0006(this.\u0091\u0005.\u0098\u0006());
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<bool> local = (ValueType) flag;
        object obj = obj0;
        ItemClickEventArgs itemClickEventArgs = obj1;
        throw UnhandledException.\u0010\u0003(ex, (object) local, (object) this, obj, (object) itemClickEventArgs);
      }
    }

    private void \u001A\u0006([In] object obj0, [In] ButtonPressedEventArgs obj1)
    {
      int num1;
      bool flag;
      ButtonEdit buttonEdit1;
      int num2;
      try
      {
        buttonEdit1 = obj0 as ButtonEdit;
        num1 = \u0088\u0006.\u008A\u0006(\u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u0011\u0003((object) buttonEdit1)));
        \u001B\u0006.\u007E\u0099\u0010((object) this.\u0091\u0005);
        this.\u0091\u0005.\u0097\u0006(num1);
        \u001B\u0006.\u007E\u008D\u0010((object) this.\u0091\u0005);
        int num3 = (int) \u0096\u0002.\u007E\u0083\u0011((object) this.\u0091\u0005);
        flag = num1 == this.\u0091\u0005.\u0098\u0006();
        if (flag)
          return;
        \u0080\u0002 obj = \u0080\u0002.\u007E\u0012\u0003;
        ButtonEdit buttonEdit2 = buttonEdit1;
        num2 = this.\u0091\u0005.\u0098\u0006();
        string str = num2.ToString();
        obj((object) buttonEdit2, (object) str);
      }
      catch (Exception ex)
      {
        ButtonEdit buttonEdit2 = buttonEdit1;
        // ISSUE: variable of a boxed type
        __Boxed<int> local1 = (ValueType) num1;
        // ISSUE: variable of a boxed type
        __Boxed<bool> local2 = (ValueType) flag;
        // ISSUE: variable of a boxed type
        __Boxed<int> local3 = (ValueType) num2;
        object obj = obj0;
        ButtonPressedEventArgs pressedEventArgs = obj1;
        throw UnhandledException.\u0013\u0003(ex, (object) buttonEdit2, (object) local1, (object) local2, (object) local3, (object) this, obj, (object) pressedEventArgs);
      }
    }

    private int \u0004\u0011()
    {
      DataTable dataTable1;
      try
      {
        dataTable1 = \u0017\u0002.\u0090\u0005(\u009C.\u0001(22175));
        return \u0088\u0006.\u008A\u0006(\u0013\u0007.\u007E\u009C\u0004(\u008E\u0002.\u007E\u0011\u0012((object) \u0095\u0006.\u007E\u0015\u0012((object) \u009D\u0004.\u007E\u001C\u0012((object) dataTable1), 0), \u009C.\u0001(22236))));
      }
      catch (Exception ex)
      {
        DataTable dataTable2 = dataTable1;
        int num;
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) num;
        throw UnhandledException.\u000F\u0003(ex, (object) dataTable2, (object) local, (object) this);
      }
    }

    private void \u001B\u0006([In] object obj0, [In] EventArgs obj1)
    {
      ButtonEdit buttonEdit1;
      DataRow[] dataRowArray1;
      bool flag;
      try
      {
        buttonEdit1 = obj0 as ButtonEdit;
        dataRowArray1 = \u001D\u0003.\u007E\u001D\u0012((object) this.\u0088\u0005, \u0006\u0003.\u0083\u0005(\u009C.\u0001(22261), \u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u0011\u0003((object) buttonEdit1))));
        flag = dataRowArray1.Length == 0;
        if (!flag)
        {
          \u0080\u0002.\u007E\u0012\u0003((object) this.\u008E\u0003, (object) \u0013\u0007.\u007E\u009C\u0004(\u008E\u0002.\u007E\u0011\u0012((object) dataRowArray1[0], \u009C.\u0001(17639))));
          \u0080\u0002.\u007E\u0012\u0003((object) this.\u0097\u0003, (object) \u0013\u0007.\u007E\u009C\u0004(\u008E\u0002.\u007E\u0011\u0012((object) dataRowArray1[0], \u009C.\u0001(22270))));
        }
        else
        {
          \u0080\u0002.\u007E\u0012\u0003((object) this.\u008E\u0003, (object) \u009C.\u0001(8361));
          \u0080\u0002.\u007E\u0012\u0003((object) this.\u0097\u0003, (object) \u009C.\u0001(8361));
        }
      }
      catch (Exception ex)
      {
        ButtonEdit buttonEdit2 = buttonEdit1;
        DataRow[] dataRowArray2 = dataRowArray1;
        // ISSUE: variable of a boxed type
        __Boxed<bool> local = (ValueType) flag;
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0012\u0003(ex, (object) buttonEdit2, (object) dataRowArray2, (object) local, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u001C\u0006([In] object obj0, [In] EventArgs obj1)
    {
      ButtonEdit buttonEdit1;
      DataRow[] dataRowArray1;
      bool flag;
      try
      {
        buttonEdit1 = obj0 as ButtonEdit;
        dataRowArray1 = \u001D\u0003.\u007E\u001D\u0012((object) this.\u0088\u0005, \u0006\u0003.\u0083\u0005(\u009C.\u0001(22261), \u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u0011\u0003((object) buttonEdit1))));
        flag = dataRowArray1.Length == 0;
        if (!flag)
        {
          \u0080\u0002.\u007E\u0012\u0003((object) this.\u008B\u0003, (object) \u0013\u0007.\u007E\u009C\u0004(\u008E\u0002.\u007E\u0011\u0012((object) dataRowArray1[0], \u009C.\u0001(17639))));
          \u0080\u0002.\u007E\u0012\u0003((object) this.\u0096\u0003, (object) \u0013\u0007.\u007E\u009C\u0004(\u008E\u0002.\u007E\u0011\u0012((object) dataRowArray1[0], \u009C.\u0001(22270))));
        }
        else
        {
          \u0080\u0002.\u007E\u0012\u0003((object) this.\u008B\u0003, (object) \u009C.\u0001(8361));
          \u0080\u0002.\u007E\u0012\u0003((object) this.\u0096\u0003, (object) \u009C.\u0001(8361));
        }
      }
      catch (Exception ex)
      {
        ButtonEdit buttonEdit2 = buttonEdit1;
        DataRow[] dataRowArray2 = dataRowArray1;
        // ISSUE: variable of a boxed type
        __Boxed<bool> local = (ValueType) flag;
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0012\u0003(ex, (object) buttonEdit2, (object) dataRowArray2, (object) local, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u001D\u0006([In] object obj0, [In] EventArgs obj1)
    {
      ButtonEdit buttonEdit1;
      DataRow[] dataRowArray1;
      bool flag;
      try
      {
        buttonEdit1 = obj0 as ButtonEdit;
        dataRowArray1 = \u001D\u0003.\u007E\u001D\u0012((object) this.\u0088\u0005, \u0006\u0003.\u0083\u0005(\u009C.\u0001(22261), \u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u0011\u0003((object) buttonEdit1))));
        flag = dataRowArray1.Length == 0;
        if (!flag)
        {
          \u0080\u0002.\u007E\u0012\u0003((object) this.\u0088\u0003, (object) \u0013\u0007.\u007E\u009C\u0004(\u008E\u0002.\u007E\u0011\u0012((object) dataRowArray1[0], \u009C.\u0001(17639))));
          \u0080\u0002.\u007E\u0012\u0003((object) this.\u0095\u0003, (object) \u0013\u0007.\u007E\u009C\u0004(\u008E\u0002.\u007E\u0011\u0012((object) dataRowArray1[0], \u009C.\u0001(22270))));
        }
        else
        {
          \u0080\u0002.\u007E\u0012\u0003((object) this.\u0088\u0003, (object) \u009C.\u0001(8361));
          \u0080\u0002.\u007E\u0012\u0003((object) this.\u0095\u0003, (object) \u009C.\u0001(8361));
        }
      }
      catch (Exception ex)
      {
        ButtonEdit buttonEdit2 = buttonEdit1;
        DataRow[] dataRowArray2 = dataRowArray1;
        // ISSUE: variable of a boxed type
        __Boxed<bool> local = (ValueType) flag;
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0012\u0003(ex, (object) buttonEdit2, (object) dataRowArray2, (object) local, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u001E\u0006([In] object obj0, [In] EventArgs obj1)
    {
      ButtonEdit buttonEdit1;
      DataRow[] dataRowArray1;
      bool flag;
      try
      {
        buttonEdit1 = obj0 as ButtonEdit;
        dataRowArray1 = \u001D\u0003.\u007E\u001D\u0012((object) this.\u0088\u0005, \u0006\u0003.\u0083\u0005(\u009C.\u0001(22261), \u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u0011\u0003((object) buttonEdit1))));
        flag = dataRowArray1.Length == 0;
        if (!flag)
        {
          \u0080\u0002.\u007E\u0012\u0003((object) this.\u0084\u0003, (object) \u0013\u0007.\u007E\u009C\u0004(\u008E\u0002.\u007E\u0011\u0012((object) dataRowArray1[0], \u009C.\u0001(17639))));
          \u0080\u0002.\u007E\u0012\u0003((object) this.\u0094\u0003, (object) \u0013\u0007.\u007E\u009C\u0004(\u008E\u0002.\u007E\u0011\u0012((object) dataRowArray1[0], \u009C.\u0001(22270))));
        }
        else
        {
          \u0080\u0002.\u007E\u0012\u0003((object) this.\u0084\u0003, (object) \u009C.\u0001(8361));
          \u0080\u0002.\u007E\u0012\u0003((object) this.\u0094\u0003, (object) \u009C.\u0001(8361));
        }
      }
      catch (Exception ex)
      {
        ButtonEdit buttonEdit2 = buttonEdit1;
        DataRow[] dataRowArray2 = dataRowArray1;
        // ISSUE: variable of a boxed type
        __Boxed<bool> local = (ValueType) flag;
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0012\u0003(ex, (object) buttonEdit2, (object) dataRowArray2, (object) local, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u001F\u0006([In] object obj0, [In] EventArgs obj1)
    {
      ButtonEdit buttonEdit1;
      DataRow[] dataRowArray1;
      bool flag;
      try
      {
        buttonEdit1 = obj0 as ButtonEdit;
        dataRowArray1 = \u001D\u0003.\u007E\u001D\u0012((object) this.\u0088\u0005, \u0006\u0003.\u0083\u0005(\u009C.\u0001(22261), \u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u0011\u0003((object) buttonEdit1))));
        flag = dataRowArray1.Length == 0;
        if (!flag)
        {
          \u0080\u0002.\u007E\u0012\u0003((object) this.\u0081\u0003, (object) \u0013\u0007.\u007E\u009C\u0004(\u008E\u0002.\u007E\u0011\u0012((object) dataRowArray1[0], \u009C.\u0001(17639))));
          \u0080\u0002.\u007E\u0012\u0003((object) this.\u0093\u0003, (object) \u0013\u0007.\u007E\u009C\u0004(\u008E\u0002.\u007E\u0011\u0012((object) dataRowArray1[0], \u009C.\u0001(22270))));
        }
        else
        {
          \u0080\u0002.\u007E\u0012\u0003((object) this.\u0081\u0003, (object) \u009C.\u0001(8361));
          \u0080\u0002.\u007E\u0012\u0003((object) this.\u0093\u0003, (object) \u009C.\u0001(8361));
        }
      }
      catch (Exception ex)
      {
        ButtonEdit buttonEdit2 = buttonEdit1;
        DataRow[] dataRowArray2 = dataRowArray1;
        // ISSUE: variable of a boxed type
        __Boxed<bool> local = (ValueType) flag;
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0012\u0003(ex, (object) buttonEdit2, (object) dataRowArray2, (object) local, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u007F\u0006([In] object obj0, [In] EventArgs obj1)
    {
      ButtonEdit buttonEdit1;
      DataRow[] dataRowArray1;
      bool flag;
      try
      {
        buttonEdit1 = obj0 as ButtonEdit;
        dataRowArray1 = \u001D\u0003.\u007E\u001D\u0012((object) this.\u0088\u0005, \u0006\u0003.\u0083\u0005(\u009C.\u0001(22261), \u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u0011\u0003((object) buttonEdit1))));
        flag = dataRowArray1.Length == 0;
        if (!flag)
        {
          \u0080\u0002.\u007E\u0012\u0003((object) this.\u001F\u0003, (object) \u0013\u0007.\u007E\u009C\u0004(\u008E\u0002.\u007E\u0011\u0012((object) dataRowArray1[0], \u009C.\u0001(17639))));
          \u0080\u0002.\u007E\u0012\u0003((object) this.\u0092\u0003, (object) \u0013\u0007.\u007E\u009C\u0004(\u008E\u0002.\u007E\u0011\u0012((object) dataRowArray1[0], \u009C.\u0001(22270))));
        }
        else
        {
          \u0080\u0002.\u007E\u0012\u0003((object) this.\u001F\u0003, (object) \u009C.\u0001(8361));
          \u0080\u0002.\u007E\u0012\u0003((object) this.\u0092\u0003, (object) \u009C.\u0001(8361));
        }
      }
      catch (Exception ex)
      {
        ButtonEdit buttonEdit2 = buttonEdit1;
        DataRow[] dataRowArray2 = dataRowArray1;
        // ISSUE: variable of a boxed type
        __Boxed<bool> local = (ValueType) flag;
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0012\u0003(ex, (object) buttonEdit2, (object) dataRowArray2, (object) local, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u0080\u0006([In] int obj0)
    {
      bool flag;
      try
      {
        \u008C\u0006.\u007E\u001A\u0012((object) this.\u0087\u0005, \u0017\u0002.\u008F\u0005(\u008F\u0006.\u0084\u0005(\u009C.\u0001(22279), obj0.ToString(), \u009C.\u0001(16076)), \u009C.\u0001(8352), \u009C.\u0001(19544)));
        flag = \u0083\u0006.\u007E\u0001\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u001F\u0002)) != 0;
        if (!flag)
        {
          Binding binding1 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u001F\u0002), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(22597), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding2 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0018\u0002), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(22626), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding3 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0015\u0002), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(22655), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding4 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0017\u0002), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(22684), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding5 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0016\u0002), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(22713), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding6 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0014\u0002), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(22742), true, DataSourceUpdateMode.OnPropertyChanged);
        }
        \u009C\u0006.\u007E\u0005\u0004((object) this.\u0018, true);
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<bool> local1 = (ValueType) flag;
        // ISSUE: variable of a boxed type
        __Boxed<int> local2 = (ValueType) obj0;
        throw UnhandledException.\u000F\u0003(ex, (object) local1, (object) this, (object) local2);
      }
    }

    private void \u0081\u0006([In] int obj0)
    {
      bool flag;
      try
      {
        \u008C\u0006.\u007E\u001A\u0012((object) this.\u0087\u0005, \u0017\u0002.\u008F\u0005(\u008F\u0006.\u0084\u0005(\u009C.\u0001(22771), obj0.ToString(), \u009C.\u0001(16076)), \u009C.\u0001(8352), \u009C.\u0001(19835)));
        flag = \u0083\u0006.\u007E\u0001\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u009A\u0002)) != 0;
        if (!flag)
        {
          Binding binding1 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u009A\u0002), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(23037), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding2 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0096\u0002), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(23070), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding3 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0093\u0002), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(23099), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding4 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0097\u0002), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(23128), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding5 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u001D\u0003), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(23157), true, DataSourceUpdateMode.OnPropertyChanged);
        }
        \u009C\u0006.\u007E\u0005\u0004((object) this.\u001B, true);
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<bool> local1 = (ValueType) flag;
        // ISSUE: variable of a boxed type
        __Boxed<int> local2 = (ValueType) obj0;
        throw UnhandledException.\u000F\u0003(ex, (object) local1, (object) this, (object) local2);
      }
    }

    private void \u0082\u0006([In] int obj0)
    {
      bool flag;
      try
      {
        \u008C\u0006.\u007E\u001A\u0012((object) this.\u0087\u0005, \u0017\u0002.\u008F\u0005(\u008F\u0006.\u0084\u0005(\u009C.\u0001(23186), obj0.ToString(), \u009C.\u0001(16076)), \u009C.\u0001(8352), \u009C.\u0001(20074)));
        flag = \u0083\u0006.\u007E\u0001\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u001A\u0003)) != 0;
        if (!flag)
        {
          Binding binding1 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u001A\u0003), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(23464), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding2 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0019\u0003), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(23493), true, DataSourceUpdateMode.OnPropertyChanged);
        }
        \u009C\u0006.\u007E\u0005\u0004((object) this.\u0018\u0003, true);
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<bool> local1 = (ValueType) flag;
        // ISSUE: variable of a boxed type
        __Boxed<int> local2 = (ValueType) obj0;
        throw UnhandledException.\u000F\u0003(ex, (object) local1, (object) this, (object) local2);
      }
    }

    private void \u0083\u0006([In] int obj0)
    {
      bool flag;
      try
      {
        \u008C\u0006.\u007E\u001A\u0012((object) this.\u0087\u0005, \u0017\u0002.\u008F\u0005(\u008F\u0006.\u0084\u0005(\u009C.\u0001(23526), obj0.ToString(), \u009C.\u0001(16076)), \u009C.\u0001(8352), \u009C.\u0001(20329)));
        flag = \u0083\u0006.\u007E\u0001\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0001\u0003)) != 0;
        if (!flag)
        {
          Binding binding1 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0001\u0003), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(23820), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding2 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u009F\u0002), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(23849), true, DataSourceUpdateMode.OnPropertyChanged);
        }
        \u009C\u0006.\u007E\u0005\u0004((object) this.\u001D, true);
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<bool> local1 = (ValueType) flag;
        // ISSUE: variable of a boxed type
        __Boxed<int> local2 = (ValueType) obj0;
        throw UnhandledException.\u000F\u0003(ex, (object) local1, (object) this, (object) local2);
      }
    }

    private void \u0084\u0006([In] int obj0)
    {
      bool flag;
      try
      {
        \u008C\u0006.\u007E\u001A\u0012((object) this.\u0087\u0005, \u0017\u0002.\u008F\u0005(\u008F\u0006.\u0084\u0005(\u009C.\u0001(23886), obj0.ToString(), \u009C.\u0001(16076)), \u009C.\u0001(8352), \u009C.\u0001(20596)));
        flag = \u0083\u0006.\u007E\u0001\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0005\u0003)) != 0;
        if (!flag)
        {
          Binding binding1 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0005\u0003), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(24232), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding2 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0007\u0003), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(24269), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding3 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0006\u0003), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(24294), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding4 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0004\u0003), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(24327), true, DataSourceUpdateMode.OnPropertyChanged);
        }
        \u009C\u0006.\u007E\u0005\u0004((object) this.\u001E, true);
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<bool> local1 = (ValueType) flag;
        // ISSUE: variable of a boxed type
        __Boxed<int> local2 = (ValueType) obj0;
        throw UnhandledException.\u000F\u0003(ex, (object) local1, (object) this, (object) local2);
      }
    }

    private void \u0086\u0006([In] int obj0)
    {
      bool flag;
      try
      {
        \u008C\u0006.\u007E\u001A\u0012((object) this.\u0087\u0005, \u0017\u0002.\u008F\u0005(\u008F\u0006.\u0084\u0005(\u009C.\u0001(24364), obj0.ToString(), \u009C.\u0001(16076)), \u009C.\u0001(8352), \u009C.\u0001(20915)));
        flag = \u0083\u0006.\u007E\u0001\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0092\u0002)) != 0;
        if (!flag)
        {
          Binding binding1 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0092\u0002), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(24690), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding2 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u008C\u0002), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(24727), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding3 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u008D\u0002), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(24764), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding4 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u008B\u0002), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(24797), true, DataSourceUpdateMode.OnPropertyChanged);
        }
        \u009C\u0006.\u007E\u0005\u0004((object) this.\u001A, true);
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<bool> local1 = (ValueType) flag;
        // ISSUE: variable of a boxed type
        __Boxed<int> local2 = (ValueType) obj0;
        throw UnhandledException.\u000F\u0003(ex, (object) local1, (object) this, (object) local2);
      }
    }

    private void \u0087\u0006([In] int obj0)
    {
      bool flag;
      try
      {
        \u008C\u0006.\u007E\u001A\u0012((object) this.\u0087\u0005, \u0017\u0002.\u008F\u0005(\u008F\u0006.\u0084\u0005(\u009C.\u0001(24830), obj0.ToString(), \u009C.\u0001(16076)), \u009C.\u0001(8352), \u009C.\u0001(21214)));
        flag = \u0083\u0006.\u007E\u0001\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u008A\u0002)) != 0;
        if (!flag)
        {
          Binding binding1 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u008A\u0002), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(25232), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding2 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0081\u0002), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(25273), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding3 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0082\u0002), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(25314), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding4 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0080\u0002), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(25351), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding5 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0083\u0002), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(25388), true, DataSourceUpdateMode.OnPropertyChanged);
        }
        \u009C\u0006.\u007E\u0005\u0004((object) this.\u0019, true);
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<bool> local1 = (ValueType) flag;
        // ISSUE: variable of a boxed type
        __Boxed<int> local2 = (ValueType) obj0;
        throw UnhandledException.\u000F\u0003(ex, (object) local1, (object) this, (object) local2);
      }
    }

    private void \u0088\u0006([In] int obj0)
    {
    }

    private void \u0089\u0006([In] int obj0)
    {
      bool flag;
      try
      {
        \u008C\u0006.\u007E\u001A\u0012((object) this.\u0087\u0005, \u0017\u0002.\u008F\u0005(\u008F\u0006.\u0084\u0005(\u009C.\u0001(25425), obj0.ToString(), \u009C.\u0001(16076)), \u009C.\u0001(8352), \u009C.\u0001(21589)));
        flag = \u0083\u0006.\u007E\u0001\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0090\u0003)) != 0;
        if (!flag)
        {
          Binding binding1 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0090\u0003), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(25787), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding2 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u008D\u0003), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(25820), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding3 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u008A\u0003), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(25853), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding4 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0087\u0003), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(25886), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding5 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0083\u0003), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(25919), true, DataSourceUpdateMode.OnPropertyChanged);
          Binding binding6 = \u008E\u0005.\u007E\u0008\u0011((object) \u0099\u0004.\u007E\u0007\u0010((object) this.\u0080\u0003), \u009C.\u0001(16119), (object) this.\u0087\u0005, \u009C.\u0001(25952), true, DataSourceUpdateMode.OnPropertyChanged);
          \u001B\u0006.\u007E\u0099\u0010((object) this.\u007F);
          \u001B\u0006.\u007E\u008D\u0010((object) this.\u007F);
        }
        \u009C\u0006.\u007E\u0005\u0004((object) this.\u007F, true);
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<bool> local1 = (ValueType) flag;
        // ISSUE: variable of a boxed type
        __Boxed<int> local2 = (ValueType) obj0;
        throw UnhandledException.\u000F\u0003(ex, (object) local1, (object) this, (object) local2);
      }
    }

    private void \u008A\u0006([In] int obj0)
    {
      try
      {
        \u008C\u0006.\u007E\u001A\u0012((object) this.\u0087\u0005, \u0017\u0002.\u008F\u0005(\u008F\u0006.\u0084\u0005(\u009C.\u0001(25985), obj0.ToString(), \u009C.\u0001(16076)), \u009C.\u0001(8352), \u009C.\u0001(21928)));
        \u0080\u0002.\u007E\u0095((object) this.\u001D\u0005, (object) \u0012\u0003.\u007E\u001F\u0012((object) \u0099\u0002.\u007E\u0017\u0012((object) this.\u0087\u0005), \u009C.\u0001(21928)));
        this.\u0090\u0005 = this.\u0004\u0011() + 1;
        \u009C\u0006.\u007E\u0005\u0004((object) this.\u0080, true);
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) obj0;
        throw UnhandledException.\u000E\u0003(ex, (object) this, (object) local);
      }
    }

    private void \u008B\u0006([In] object obj0, [In] EventArgs obj1)
    {
      TrackBarControl trackBarControl1;
      try
      {
        trackBarControl1 = obj0 as TrackBarControl;
        \u0080\u0002.\u007E\u0012\u0003((object) this.\u0002\u0004, (object) \u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u0011\u0003((object) trackBarControl1)));
      }
      catch (Exception ex)
      {
        TrackBarControl trackBarControl2 = trackBarControl1;
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0010\u0003(ex, (object) trackBarControl2, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u008C\u0006([In] object obj0, [In] EventArgs obj1)
    {
      TrackBarControl trackBarControl1;
      try
      {
        trackBarControl1 = obj0 as TrackBarControl;
        \u0080\u0002.\u007E\u0012\u0003((object) this.\u0001\u0004, (object) \u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u0011\u0003((object) trackBarControl1)));
      }
      catch (Exception ex)
      {
        TrackBarControl trackBarControl2 = trackBarControl1;
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0010\u0003(ex, (object) trackBarControl2, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u008D\u0006([In] object obj0, [In] EventArgs obj1)
    {
      TrackBarControl trackBarControl1;
      try
      {
        trackBarControl1 = obj0 as TrackBarControl;
        \u0080\u0002.\u007E\u0012\u0003((object) this.\u009F\u0003, (object) \u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u0011\u0003((object) trackBarControl1)));
      }
      catch (Exception ex)
      {
        TrackBarControl trackBarControl2 = trackBarControl1;
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0010\u0003(ex, (object) trackBarControl2, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u008E\u0006([In] object obj0, [In] EventArgs obj1)
    {
      CheckEdit checkEdit1;
      try
      {
        checkEdit1 = obj0 as CheckEdit;
        \u009C\u0006.\u007E\u0005\u0003((object) \u0095\u0002.\u007E\u0015\u0003((object) this.\u0001\u0002), !\u0006\u0007.\u007E\u0082\u0003((object) checkEdit1));
      }
      catch (Exception ex)
      {
        CheckEdit checkEdit2 = checkEdit1;
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0010\u0003(ex, (object) checkEdit2, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u008F\u0006([In] object obj0, [In] EventArgs obj1)
    {
      int num1;
      bool flag;
      int num2;
      int num3;
      int num4;
      try
      {
        num1 = 0;
        num2 = 0;
        num3 = 0;
        flag = !\u000E\u0003.\u008B\u0006(\u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u0011\u0003((object) this.\u0097\u0003)), ref num1);
        if (!flag)
        {
          num2 += num1;
          ++num3;
        }
        flag = !\u000E\u0003.\u008B\u0006(\u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u0011\u0003((object) this.\u0096\u0003)), ref num1);
        if (!flag)
        {
          num2 += num1;
          ++num3;
        }
        flag = !\u000E\u0003.\u008B\u0006(\u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u0011\u0003((object) this.\u0095\u0003)), ref num1);
        if (!flag)
        {
          num2 += num1;
          ++num3;
        }
        flag = !\u000E\u0003.\u008B\u0006(\u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u0011\u0003((object) this.\u0094\u0003)), ref num1);
        if (!flag)
        {
          num2 += num1;
          ++num3;
        }
        flag = !\u000E\u0003.\u008B\u0006(\u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u0011\u0003((object) this.\u0093\u0003)), ref num1);
        if (!flag)
        {
          num2 += num1;
          ++num3;
        }
        flag = !\u000E\u0003.\u008B\u0006(\u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u0011\u0003((object) this.\u0092\u0003)), ref num1);
        if (!flag)
        {
          num2 += num1;
          ++num3;
        }
        \u0080\u0002.\u007E\u0012\u0003((object) this.\u0007\u0004, (object) (((double) num2 + \u0011\u0006.\u001E\u0006(\u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u0011\u0003((object) this.\u0018\u0004)))) * (\u0011\u0006.\u001E\u0006(\u0013\u0007.\u007E\u009C\u0004(\u008E\u0002.\u007E\u0011\u0012((object) \u0095\u0006.\u007E\u0015\u0012((object) \u009D\u0004.\u007E\u001C\u0012((object) \u0012\u0003.\u007E\u001F\u0012((object) \u0099\u0002.\u007E\u0017\u0012((object) this.\u0087\u0005), \u009C.\u0001(16081))), 0), \u009C.\u0001(26255)))) / 100.0)));
        \u0080\u0002.\u007E\u0012\u0003((object) this.\u0011\u0004, (object) (\u0011\u0006.\u001E\u0006(\u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u0011\u0003((object) this.\u0007\u0004))) * (\u0011\u0006.\u001E\u0006(\u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u0011\u0003((object) this.\u001A\u0004))) / 100.0)));
        \u0080\u0002.\u007E\u0012\u0003((object) this.\u000F\u0004, (object) (\u0011\u0006.\u001E\u0006(\u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u0011\u0003((object) this.\u0007\u0004))) * (\u0011\u0006.\u001E\u0006(\u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u0011\u0003((object) this.\u001C\u0004))) / 100.0)));
        \u0080\u0002 obj = \u0080\u0002.\u007E\u0012\u0003;
        TextEdit textEdit = this.\u0018\u0004;
        num4 = num2 / num3;
        string str = num4.ToString();
        obj((object) textEdit, (object) str);
        \u0080\u0002.\u007E\u0012\u0003((object) this.\u0016\u0004, (object) (\u0011\u0006.\u001E\u0006(\u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u0011\u0003((object) this.\u0018\u0004))) * (\u0011\u0006.\u001E\u0006(\u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u0011\u0003((object) this.\u0014\u0004))) / 100.0)));
        flag = \u0006\u0007.\u007E\u0082\u0003((object) this.\u0010\u0002);
        if (flag)
          return;
        \u0080\u0002.\u007E\u0012\u0003((object) this.\u0001\u0002, \u0097\u0005.\u007E\u0011\u0003((object) this.\u0007\u0004));
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<int> local1 = (ValueType) num1;
        // ISSUE: variable of a boxed type
        __Boxed<int> local2 = (ValueType) num2;
        // ISSUE: variable of a boxed type
        __Boxed<int> local3 = (ValueType) num3;
        // ISSUE: variable of a boxed type
        __Boxed<bool> local4 = (ValueType) flag;
        // ISSUE: variable of a boxed type
        __Boxed<int> local5 = (ValueType) num4;
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0014\u0003(ex, (object) local1, (object) local2, (object) local3, (object) local4, (object) local5, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u0090\u0006([In] object obj0, [In] EventArgs obj1)
    {
      try
      {
        \u0080\u0002.\u007E\u0012\u0003((object) this.\u0016\u0004, (object) (\u0011\u0006.\u001E\u0006(\u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u0011\u0003((object) this.\u0018\u0004))) * (\u0011\u0006.\u001E\u0006(\u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u0011\u0003((object) this.\u0014\u0004))) / 100.0)));
      }
      catch (Exception ex)
      {
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u000F\u0003(ex, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u0091\u0006([In] object obj0, [In] EventArgs obj1)
    {
      try
      {
        \u0080\u0002.\u007E\u0012\u0003((object) this.\u0011\u0004, (object) (\u0011\u0006.\u001E\u0006(\u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u0011\u0003((object) this.\u0007\u0004))) * (\u0011\u0006.\u001E\u0006(\u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u0011\u0003((object) this.\u001A\u0004))) / 100.0)));
      }
      catch (Exception ex)
      {
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u000F\u0003(ex, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u0092\u0006([In] object obj0, [In] EventArgs obj1)
    {
      try
      {
        \u0080\u0002.\u007E\u0012\u0003((object) this.\u000F\u0004, (object) (\u0011\u0006.\u001E\u0006(\u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u0011\u0003((object) this.\u0007\u0004))) * (\u0011\u0006.\u001E\u0006(\u0013\u0007.\u007E\u009C\u0004(\u0097\u0005.\u007E\u0011\u0003((object) this.\u001C\u0004))) / 100.0)));
      }
      catch (Exception ex)
      {
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u000F\u0003(ex, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u0093\u0006([In] object obj0, [In] GridMenuEventArgs obj1)
    {
      bool flag;
      int rowHandle;
      DXMenuItem dxMenuItem1;
      GridView gridView1;
      try
      {
        gridView1 = obj0 as GridView;
        flag = \u0017\u0003.\u007E\u009D((object) obj1) != GridMenuType.Row;
        if (flag)
          return;
        rowHandle = \u009D\u0003.\u007E\u009C((object) obj1).RowHandle;
        \u001B\u0006.\u007E\u0091\u0007((object) \u0098\u0002.\u007E\u0080\u0004((object) \u008C\u0003.\u007E\u009E((object) obj1)));
        // ISSUE: reference to a compiler-generated method
        dxMenuItem1 = new DXMenuItem(\u009C.\u0001(26272), new EventHandler(this.\u0094\u0006), (Image) \u001A\u0002.\u009E\u0006());
        \u0080\u0002.\u007E\u007F\u0004((object) dxMenuItem1, (object) new \u009C.\u0011\u0006(gridView1, rowHandle));
        int num = \u0090\u0005.\u007E\u001D\u0004((object) \u0098\u0002.\u007E\u0080\u0004((object) \u008C\u0003.\u007E\u009E((object) obj1)), dxMenuItem1);
      }
      catch (Exception ex)
      {
        GridView gridView2 = gridView1;
        // ISSUE: variable of a boxed type
        __Boxed<int> local1 = (ValueType) rowHandle;
        DXMenuItem dxMenuItem2 = dxMenuItem1;
        // ISSUE: variable of a boxed type
        __Boxed<bool> local2 = (ValueType) flag;
        object obj = obj0;
        GridMenuEventArgs gridMenuEventArgs = obj1;
        throw UnhandledException.\u0013\u0003(ex, (object) gridView2, (object) local1, (object) dxMenuItem2, (object) local2, (object) this, obj, (object) gridMenuEventArgs);
      }
    }

    private void \u0094\u0006([In] object obj0, [In] EventArgs obj1)
    {
      \u009C.\u0011\u0006 obj2;
      DXMenuItem dxMenuItem1;
      try
      {
        dxMenuItem1 = obj0 as DXMenuItem;
        obj2 = \u0097\u0005.\u007E\u001F\u0004((object) dxMenuItem1) as \u009C.\u0011\u0006;
        \u0011\u0004.\u007E\u001A((object) obj2.\u0001, obj2.\u0002);
      }
      catch (Exception ex)
      {
        DXMenuItem dxMenuItem2 = dxMenuItem1;
        \u009C.\u0011\u0006 obj3 = obj2;
        object obj4 = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0011\u0003(ex, (object) dxMenuItem2, (object) obj3, (object) this, obj4, (object) eventArgs);
      }
    }

    private void \u0005\u0011([In] object obj0, [In] InitNewRowEventArgs obj1)
    {
      int num1;
      GridView gridView1;
      int num2;
      try
      {
        gridView1 = obj0 as GridView;
        num1 = \u0083\u0006.\u007E\u009A((object) obj1);
        \u001C\u0004.\u007E\u0019((object) gridView1, num1, \u0003\u0004.\u007E\u008A((object) \u0099\u0003.\u007E\u0012((object) gridView1), \u009C.\u0001(14039)), (object) this.\u0092\u0005);
        \u001C\u0004 obj = \u001C\u0004.\u007E\u0019;
        GridView gridView2 = gridView1;
        int num3 = num1;
        GridColumn gridColumn = \u0003\u0004.\u007E\u008A((object) \u0099\u0003.\u007E\u0012((object) gridView1), \u009C.\u0001(14005));
        num2 = ++this.\u0090\u0005;
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) num2;
        obj((object) gridView2, num3, gridColumn, (object) local);
        \u001C\u0004.\u007E\u0019((object) gridView1, num1, \u0003\u0004.\u007E\u008A((object) \u0099\u0003.\u007E\u0012((object) gridView1), \u009C.\u0001(13859)), (object) 0);
        \u001C\u0004.\u007E\u0019((object) gridView1, num1, \u0003\u0004.\u007E\u008A((object) \u0099\u0003.\u007E\u0012((object) gridView1), \u009C.\u0001(13936)), (object) 0);
        \u001C\u0004.\u007E\u0019((object) gridView1, num1, \u0003\u0004.\u007E\u008A((object) \u0099\u0003.\u007E\u0012((object) gridView1), \u009C.\u0001(13975)), (object) 0);
      }
      catch (Exception ex)
      {
        GridView gridView2 = gridView1;
        // ISSUE: variable of a boxed type
        __Boxed<int> local1 = (ValueType) num1;
        // ISSUE: variable of a boxed type
        __Boxed<int> local2 = (ValueType) num2;
        object obj = obj0;
        InitNewRowEventArgs initNewRowEventArgs = obj1;
        throw UnhandledException.\u0012\u0003(ex, (object) gridView2, (object) local1, (object) local2, (object) this, obj, (object) initNewRowEventArgs);
      }
    }

    static \u009C()
    {
      \u0006.\u0080\u0002();
    }

    private sealed class \u0011\u0006
    {
      public GridView \u0001;
      public int \u0002;

      public \u0011\u0006([In] GridView obj0, [In] int obj1)
      {
        try
        {
          this.\u0002 = obj1;
          this.\u0001 = obj0;
        }
        catch (Exception ex)
        {
          GridView gridView = obj0;
          // ISSUE: variable of a boxed type
          __Boxed<int> local = (ValueType) obj1;
          throw UnhandledException.\u000F\u0003(ex, (object) this, (object) gridView, (object) local);
        }
      }
    }
  }
}
