﻿// Decompiled with JetBrains decompiler
// Type: NewItemEditor.Properties.Settings
// Assembly: NewItemEditor, Version=1.0.0.7, Culture=neutral, PublicKeyToken=9a6515a4c4e1fd2c
// MVID: 9D5F0B9A-C985-40A9-9939-E28C808E997E
// Assembly location: D:\Server\eab\Tools\Net7 Item Editor.exe

using SmartAssembly.SmartExceptionsCore;
using System;
using System.CodeDom.Compiler;
using System.Configuration;
using System.Runtime.CompilerServices;

namespace NewItemEditor.Properties
{
  [CompilerGenerated]
  [GeneratedCode("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "9.0.0.0")]
  internal sealed class Settings : ApplicationSettingsBase
  {
    private static Settings \u0001;

    public static Settings Default
    {
      get
      {
        try
        {
          Settings settings1 = Settings.\u0001;
          Settings settings2 = settings1;
          return settings2;
        }
        catch (Exception ex)
        {
          Settings settings1;
          Settings settings2 = settings1;
          throw UnhandledException.\u0008\u0003(ex, (object) settings2);
        }
      }
    }

    public Settings()
    {
      try
      {
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0008\u0003(ex, (object) this);
      }
    }

    static Settings()
    {
      try
      {
        // ISSUE: reference to a compiler-generated field
        // ISSUE: object of a compiler-generated type is created
        Settings.\u0001 = (Settings) \u0013\u0005.\u0003\u0012((SettingsBase) new Settings());
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0007\u0003(ex);
      }
    }
  }
}
