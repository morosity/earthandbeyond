﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: NewItemEditor, Version=1.0.0.7, Culture=neutral, PublicKeyToken=9a6515a4c4e1fd2c
// MVID: 9D5F0B9A-C985-40A9-9939-E28C808E997E
// Assembly location: D:\Server\eab\Tools\Net7 Item Editor.exe

using \u0005;
using \u0010;
using SmartAssembly.SmartExceptionsCore;
using System;
using System.Diagnostics;
using System.Reflection;
using System.Reflection.Emit;

namespace \u0007
{
  internal static class \u0006
  {
    public static void \u0080\u0002()
    {
      StackFrame frame;
      Type declaringType;
      FieldInfo[] fields;
      int index1;
      FieldInfo fieldInfo;
      Type[] typeArray;
      DynamicMethod dynamicMethod;
      ILGenerator ilGenerator;
      MethodInfo[] methods;
      int index2;
      MethodInfo meth;
      StackTrace stackTrace;
      try
      {
        stackTrace = new StackTrace();
        frame = stackTrace.GetFrame(1);
        declaringType = frame.GetMethod().DeclaringType;
        fields = declaringType.GetFields(BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.GetField);
        for (index1 = 0; index1 < fields.Length; ++index1)
        {
          fieldInfo = fields[index1];
          try
          {
            if (fieldInfo.FieldType == typeof (\u0004))
            {
              string empty = string.Empty;
              Type returnType = typeof (string);
              typeArray = new Type[1]{ typeof (int) };
              Type[] parameterTypes = typeArray;
              Type owner = declaringType;
              int num = 1;
              dynamicMethod = new DynamicMethod(empty, returnType, parameterTypes, owner, num != 0);
              ilGenerator = dynamicMethod.GetILGenerator();
              ilGenerator.Emit(OpCodes.Ldarg_0);
              methods = typeof (\u0006).GetMethods(BindingFlags.Static | BindingFlags.Public);
              for (index2 = 0; index2 < methods.Length; ++index2)
              {
                meth = methods[index2];
                if (meth.ReturnType == typeof (string))
                {
                  ilGenerator.Emit(OpCodes.Ldc_I4, fieldInfo.MetadataToken & 16777215);
                  ilGenerator.Emit(OpCodes.Sub);
                  ilGenerator.Emit(OpCodes.Call, meth);
                  break;
                }
              }
              ilGenerator.Emit(OpCodes.Ret);
              fieldInfo.SetValue((object) null, (object) (\u0004) dynamicMethod.CreateDelegate(typeof (\u0004)));
              break;
            }
          }
          catch
          {
          }
        }
      }
      catch (Exception ex)
      {
        object[] objArray = new object[12]
        {
          (object) stackTrace,
          (object) frame,
          (object) declaringType,
          (object) fieldInfo,
          (object) dynamicMethod,
          (object) ilGenerator,
          (object) meth,
          (object) fields,
          (object) index1,
          (object) typeArray,
          (object) methods,
          (object) index2
        };
        throw UnhandledException.\u0017\u0003(ex, objArray);
      }
    }
  }
}
