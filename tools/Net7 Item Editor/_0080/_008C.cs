﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: NewItemEditor, Version=1.0.0.7, Culture=neutral, PublicKeyToken=9a6515a4c4e1fd2c
// MVID: 9D5F0B9A-C985-40A9-9939-E28C808E997E
// Assembly location: D:\Server\eab\Tools\Net7 Item Editor.exe

using \u0080;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace \u0080
{
  internal sealed class \u008C : EventArgs
  {
    private bool \u0004 = true;
    private bool \u0005 = true;
    private \u0084 \u0001;
    private Exception \u0002;
    private bool \u0003;
    private bool \u0006;

    [SpecialName]
    public Exception \u0001\u0003()
    {
      return this.\u0002;
    }

    [SpecialName]
    public bool \u0002\u0003()
    {
      return this.\u0003;
    }

    [SpecialName]
    public bool \u0003\u0003()
    {
      return this.\u0004;
    }

    [SpecialName]
    public bool \u0098\u0002()
    {
      return this.\u0005;
    }

    internal void \u0013()
    {
      this.\u0003 = true;
    }

    internal void \u0014()
    {
      this.\u0004 = false;
    }

    internal void \u0015()
    {
      this.\u0005 = false;
    }

    [SpecialName]
    public bool \u0099\u0002()
    {
      return this.\u0006;
    }

    [SpecialName]
    public void \u009A\u0002([In] bool obj0)
    {
      this.\u0006 = obj0;
    }

    public void \u0004\u0003()
    {
      if (!this.\u0003)
        return;
      this.\u0001.\u0013();
    }

    public bool \u0005\u0003()
    {
      if (!this.\u0004)
        return false;
      return this.\u0001.\u0013();
    }

    internal \u008C([In] \u0084 obj0, [In] Exception obj1)
    {
      this.\u0001 = obj0;
      this.\u0002 = obj1;
    }
  }
}
