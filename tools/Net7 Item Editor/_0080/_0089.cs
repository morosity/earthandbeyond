﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: NewItemEditor, Version=1.0.0.7, Culture=neutral, PublicKeyToken=9a6515a4c4e1fd2c
// MVID: 9D5F0B9A-C985-40A9-9939-E28C808E997E
// Assembly location: D:\Server\eab\Tools\Net7 Item Editor.exe

using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;

namespace \u0080
{
  internal sealed class \u0089 : EventArgs
  {
    private string \u0002 = string.Empty;
    private bool \u0005 = true;
    private SecurityException \u0001;
    private bool \u0003;
    private bool \u0004;

    [SpecialName]
    public SecurityException \u0096\u0002()
    {
      return this.\u0001;
    }

    [SpecialName]
    public string \u0097\u0002()
    {
      return this.\u0002;
    }

    [SpecialName]
    public bool \u0098\u0002()
    {
      return this.\u0005;
    }

    [SpecialName]
    public bool \u0099\u0002()
    {
      return this.\u0003;
    }

    [SpecialName]
    public void \u009A\u0002([In] bool obj0)
    {
      this.\u0003 = obj0;
    }

    [SpecialName]
    public bool \u009B\u0002()
    {
      return this.\u0004;
    }

    public \u0089([In] SecurityException obj0)
    {
      this.\u0001 = obj0;
    }

    public \u0089([In] SecurityException obj0, [In] bool obj1)
      : this(obj0)
    {
      this.\u0005 = obj1;
    }

    public \u0089([In] string obj0, [In] bool obj1)
      : this(new SecurityException(obj0), obj1)
    {
      this.\u0002 = obj0;
    }
  }
}
