﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: NewItemEditor, Version=1.0.0.7, Culture=neutral, PublicKeyToken=9a6515a4c4e1fd2c
// MVID: 9D5F0B9A-C985-40A9-9939-E28C808E997E
// Assembly location: D:\Server\eab\Tools\Net7 Item Editor.exe

using \u0005;
using \u0007;
using \u0080;
using System;
using System.Drawing;
using System.Runtime.InteropServices;

namespace \u0080
{
  internal sealed class \u008D
  {
    [NonSerialized]
    internal static \u0004 \u0001;

    [DllImport("shell32", EntryPoint = "ExtractIconEx")]
    private static extern int \u0013([In] string obj0, [In] int obj1, [In] ref int obj2, [In] ref int obj3, [In] int obj4);

    [DllImport("user32", EntryPoint = "DrawText", CharSet = CharSet.Unicode)]
    private static extern int \u0013([In] IntPtr obj0, [In] string obj1, [In] int obj2, [In] ref \u008D.\u000E obj3, [In] int obj4);

    [DllImport("gdi32.dll", EntryPoint = "SelectObject")]
    private static extern IntPtr \u0013([In] IntPtr obj0, [In] IntPtr obj1);

    [DllImport("kernel32.Dll", EntryPoint = "GetVersionEx")]
    private static extern short \u0013([In] ref \u008D.\u0013 obj0);

    public static Icon \u0006\u0003()
    {
      try
      {
        return \u008D.\u0013();
      }
      catch (Exception ex)
      {
        return \u0013.\u0013.\u0013(\u008D.\u0001(5456));
      }
    }

    private static Icon \u0013()
    {
      int num1 = 0;
      int num2 = 0;
      if (\u008D.\u0013(\u0008\u0004.\u0094\u000F(), -1, ref num2, ref num2, 1) > 0)
      {
        \u008D.\u0013(\u0008\u0004.\u0094\u000F(), 0, ref num1, ref num2, 1);
        if (num1 != 0)
          return \u0005\u0007.\u0081\u000F(new IntPtr(num1));
      }
      return (Icon) null;
    }

    internal static string \u0013()
    {
      try
      {
        return \u008D.\u0014();
      }
      catch (Exception ex)
      {
        return string.Empty;
      }
    }

    private static string \u0014()
    {
      \u008D.\u0013 obj = new \u008D.\u0013();
      // ISSUE: type reference
      obj.\u0001 = \u0089\u0002.\u0001\u000E(\u001F\u0002.\u009A\u0006(__typeref (\u008D.\u0013)));
      int num = (int) \u008D.\u0013(ref obj);
      return obj.\u0006;
    }

    internal static int \u0013([In] Graphics obj0, [In] string obj1, [In] Font obj2, [In] int obj3)
    {
      try
      {
        return \u008D.\u0015(obj0, obj1, obj2, obj3);
      }
      catch (Exception ex1)
      {
        try
        {
          return \u008E\u0004.\u0018\u0006((double) \u008D.\u0014(obj0, obj1, obj2, obj3) * 1.1);
        }
        catch (Exception ex2)
        {
        }
      }
      return 0;
    }

    private static int \u0014([In] Graphics obj0, [In] string obj1, [In] Font obj2, [In] int obj3)
    {
      return \u0093\u0003.\u008C\u000F(\u0084\u0004.\u007E\u001A\u000F((object) obj0, obj1, obj2, obj3)).Height;
    }

    private static int \u0015([In] Graphics obj0, [In] string obj1, [In] Font obj2, [In] int obj3)
    {
      \u008D.\u000E obj = new \u008D.\u000E(new Rectangle(0, 0, obj3, 10000));
      int num1 = 3088;
      IntPtr num2 = \u0008\u0007.\u007E\u0017\u000F((object) obj0);
      IntPtr num3 = \u0008\u0007.\u007E\u0092\u000F((object) obj2);
      IntPtr num4 = \u008D.\u0013(num2, num3);
      \u008D.\u0013(num2, obj1, -1, ref obj, num1);
      \u008D.\u0013(num2, num4);
      \u0086\u0002.\u007E\u0018\u000F((object) obj0, num2);
      return obj.\u0004 - obj.\u0002;
    }

    static \u008D()
    {
      \u0006.\u0080\u0002();
    }

    private struct \u000E
    {
      public int \u0001;
      public int \u0002;
      public int \u0003;
      public int \u0004;

      public \u000E([In] Rectangle obj0)
      {
        this.\u0001 = obj0.Left;
        this.\u0002 = obj0.Top;
        this.\u0004 = obj0.Bottom;
        this.\u0003 = obj0.Right;
      }
    }

    private struct \u0013
    {
      public int \u0001;
      public int \u0002;
      public int \u0003;
      public int \u0004;
      public int \u0005;
      [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 128)]
      public string \u0006;
    }
  }
}
