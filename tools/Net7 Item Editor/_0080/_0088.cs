﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: NewItemEditor, Version=1.0.0.7, Culture=neutral, PublicKeyToken=9a6515a4c4e1fd2c
// MVID: 9D5F0B9A-C985-40A9-9939-E28C808E997E
// Assembly location: D:\Server\eab\Tools\Net7 Item Editor.exe

using \u0080;
using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace \u0080
{
  internal sealed class \u0088 : EventArgs
  {
    private string \u0003 = string.Empty;
    private string \u0004 = string.Empty;
    private \u007F \u0001;
    private bool \u0002;

    [SpecialName]
    public \u007F \u0092\u0002()
    {
      return this.\u0001;
    }

    [SpecialName]
    public bool \u0093\u0002()
    {
      return this.\u0002;
    }

    [SpecialName]
    public string \u0094\u0002()
    {
      return this.\u0003;
    }

    [SpecialName]
    public void \u0095\u0002([In] string obj0)
    {
      this.\u0004 = obj0;
    }

    internal \u0088([In] \u007F obj0)
      : this(obj0, string.Empty)
    {
    }

    internal \u0088([In] \u007F obj0, [In] string obj1)
    {
      this.\u0001 = obj0;
      this.\u0002 = obj1 != null && \u0083\u0006.\u007E\u0014\u0005((object) obj1) > 0;
      this.\u0003 = obj1;
    }
  }
}
