﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: NewItemEditor, Version=1.0.0.7, Culture=neutral, PublicKeyToken=9a6515a4c4e1fd2c
// MVID: 9D5F0B9A-C985-40A9-9939-E28C808E997E
// Assembly location: D:\Server\eab\Tools\Net7 Item Editor.exe

using \u0005;
using \u0007;
using \u000E;
using \u001C;
using \u0080;
using Microsoft.Win32;
using SmartAssembly.SmartExceptionsCore;
using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;
using System.Text;
using System.Threading;
using System.Web.Services.Protocols;
using System.Xml;

namespace \u0080
{
  internal abstract class \u0084
  {
    private char[] \u0004 = new char[0];
    private ArrayList \u000F = new ArrayList();
    private ArrayList \u0010 = new ArrayList();
    private Hashtable \u0011 = new Hashtable();
    private ArrayList \u0012 = new ArrayList();
    private Hashtable \u0013 = new Hashtable();
    [NonSerialized]
    internal static \u0004 \u0001;
    private static string \u0001;
    internal static bool \u0002;
    private IWebProxy \u0003;
    private static \u0084 \u0005;
    private Exception \u0006;
    private Hashtable \u0007;
    private Hashtable \u0008;
    private XmlTextWriter \u000E;
    private EventHandler \u0014;
    private \u0087 \u0015;

    [SpecialName]
    [MethodImpl(MethodImplOptions.Synchronized)]
    public void \u0086\u0002([In] EventHandler obj0)
    {
      this.\u0014 = (EventHandler) \u0008\u0003.\u009E\u0005((Delegate) this.\u0014, (Delegate) obj0);
    }

    [SpecialName]
    [MethodImpl(MethodImplOptions.Synchronized)]
    public void \u0087\u0002([In] \u0087 obj0)
    {
      this.\u0015 = (\u0087) \u0008\u0003.\u009E\u0005((Delegate) this.\u0015, (Delegate) obj0);
    }

    protected abstract void \u0088\u0002([In] \u008C obj0);

    protected abstract void \u0089\u0002([In] \u008A obj0);

    protected abstract void \u008A\u0002([In] \u0089 obj0);

    [SecurityPermission(SecurityAction.Demand, UnmanagedCode = true)]
    public static void \u008B\u0002([In] \u0084 obj0)
    {
      if (obj0 == null)
        return;
      \u0084.\u0005 = obj0;
      \u008D\u0005.\u007E\u000F\u0006((object) \u0016\u0006.\u0008\u0006(), new UnhandledExceptionEventHandler(obj0.\u0013));
      \u0001\u0005.\u0097\u000F(new ThreadExceptionEventHandler(obj0.\u0013));
    }

    [SpecialName]
    public char[] \u008C\u0002()
    {
      string str;
      if (this.\u0004.Length == 0 && (str = \u0013\u0007.\u007E\u001E\u0005((object) \u0084.\u0001(3175))) != null)
      {
        if (!\u009E\u0002.\u0011\u0005(str, \u0084.\u0001(3188)))
        {
          if (\u009E\u0002.\u0011\u0005(str, \u0084.\u0001(3175)))
            this.\u0004 = new char[58]
            {
              '\x0001',
              '\x0002',
              '\x0003',
              '\x0004',
              '\x0005',
              '\x0006',
              '\a',
              '\b',
              '\x000E',
              '\x000F',
              '\x0010',
              '\x0011',
              '\x0012',
              '\x0013',
              '\x0014',
              '\x0015',
              '\x0016',
              '\x0017',
              '\x0018',
              '\x0019',
              '\x001A',
              '\x001B',
              '\x001C',
              '\x001D',
              '\x001E',
              '\x001F',
              '\x007F',
              '\x0080',
              '\x0081',
              '\x0082',
              '\x0083',
              '\x0084',
              '\x0086',
              '\x0087',
              '\x0088',
              '\x0089',
              '\x008A',
              '\x008B',
              '\x008C',
              '\x008D',
              '\x008E',
              '\x008F',
              '\x0090',
              '\x0091',
              '\x0092',
              '\x0093',
              '\x0094',
              '\x0095',
              '\x0096',
              '\x0097',
              '\x0098',
              '\x0099',
              '\x009A',
              '\x009B',
              '\x009C',
              '\x009D',
              '\x009E',
              '\x009F'
            };
        }
        else
          this.\u0004 = new char[62]
          {
            'a',
            'b',
            'c',
            'd',
            'e',
            'f',
            'g',
            'h',
            'i',
            'j',
            'k',
            'l',
            'm',
            'n',
            'o',
            'p',
            'q',
            'r',
            's',
            't',
            'u',
            'v',
            'w',
            'x',
            'y',
            'z',
            'A',
            'B',
            'C',
            'D',
            'E',
            'F',
            'G',
            'H',
            'I',
            'J',
            'K',
            'L',
            'M',
            'N',
            'O',
            'P',
            'Q',
            'R',
            'S',
            'T',
            'U',
            'V',
            'W',
            'X',
            'Y',
            'Z',
            '0',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9'
          };
      }
      return this.\u0004;
    }

    public static void \u008D\u0002([In] Exception obj0, [In] object[] obj1)
    {
      try
      {
        if (obj0 != null && obj0 is SecurityException && (\u009E\u0002.\u0011\u0005(\u0084.\u0001, \u0084.\u0001(3197)) && \u0084.\u0005.\u0013((SecurityException) obj0)))
          return;
        \u0084.\u0005.\u0013(UnhandledException.\u0017\u0003(obj0, obj1), false);
      }
      catch
      {
        \u0001\u0006.\u0099\u000F();
      }
    }

    public static Exception \u008E\u0002([In] Exception obj0, [In] object[] obj1)
    {
      \u0084.\u008F\u0002(obj0, obj1);
      return (Exception) new SoapException(\u0013\u0007.\u007E\u0090\u0005((object) obj0), SoapException.ServerFaultCode);
    }

    public static void \u008F\u0002([In] Exception obj0, [In] object[] obj1)
    {
      try
      {
        if (\u0084.\u0005 == null)
        {
          foreach (Type type in \u0015\u0003.\u007E\u000E\u0008((object) \u001B\u0003.\u0016\u0008()))
          {
            if (type != null && \u008D\u0003.\u007E\u009E\u0006((object) type) != null)
            {
              // ISSUE: type reference
              if (\u008D\u0003.\u007E\u009E\u0006((object) type) == \u001F\u0002.\u009A\u0006(__typeref (\u0084)))
              {
                try
                {
                  \u0084.\u0005 = (\u0084) \u008C\u0004.\u0001\u0006(type, true);
                  if (\u0084.\u0005 != null)
                    break;
                }
                catch
                {
                }
              }
            }
          }
        }
        if (\u0084.\u0005 == null)
          return;
        \u0084.\u0005.\u0013(UnhandledException.\u0017\u0003(obj0, obj1), true);
      }
      catch
      {
      }
    }

    private void \u0013([In] object obj0, [In] ThreadExceptionEventArgs obj1)
    {
      try
      {
        if (\u008B\u0003.\u007E\u0001\u0012((object) obj1) is SecurityException && (\u009E\u0002.\u0011\u0005(\u0084.\u0001, \u0084.\u0001(3197)) && this.\u0013(\u008B\u0003.\u007E\u0001\u0012((object) obj1) as SecurityException)))
          return;
        this.\u0013(\u008B\u0003.\u007E\u0001\u0012((object) obj1), true);
      }
      catch
      {
      }
    }

    private void \u0013([In] object obj0, [In] UnhandledExceptionEventArgs obj1)
    {
      try
      {
        if (\u0097\u0005.\u007E\u001A\u0007((object) obj1) is SecurityException && (\u009E\u0002.\u0011\u0005(\u0084.\u0001, \u0084.\u0001(3197)) && this.\u0013(\u0097\u0005.\u007E\u001A\u0007((object) obj1) as SecurityException)) || !(\u0097\u0005.\u007E\u001A\u0007((object) obj1) is Exception))
          return;
        this.\u0013((Exception) \u0097\u0005.\u007E\u001A\u0007((object) obj1), !\u0006\u0007.\u007E\u001B\u0007((object) obj1));
      }
      catch
      {
      }
    }

    private string \u0013([In] object obj0)
    {
      try
      {
        if (obj0 == null)
          return string.Empty;
        if (obj0 is int)
          return ((int) obj0).ToString(\u0084.\u0001(3202));
        if (obj0 is long)
          return ((long) obj0).ToString(\u0084.\u0001(3202));
        if (obj0 is short)
          return ((short) obj0).ToString(\u0084.\u0001(3202));
        if (obj0 is uint)
          return ((uint) obj0).ToString(\u0084.\u0001(3202));
        if (obj0 is ulong)
          return ((ulong) obj0).ToString(\u0084.\u0001(3202));
        if (obj0 is ushort)
          return ((ushort) obj0).ToString(\u0084.\u0001(3202));
        if (obj0 is byte)
          return ((byte) obj0).ToString(\u0084.\u0001(3202));
        if (obj0 is sbyte)
          return ((sbyte) obj0).ToString(\u0084.\u0001(3202));
        if (obj0 is IntPtr)
          return ((IntPtr) obj0).ToInt64().ToString(\u0084.\u0001(3202));
        if (obj0 is UIntPtr)
          return ((UIntPtr) obj0).ToUInt64().ToString(\u0084.\u0001(3202));
      }
      catch
      {
      }
      return string.Empty;
    }

    private string \u0013([In] string obj0)
    {
      if (\u0089\u0003.\u007E\u001C\u0005((object) obj0, \u0084.\u0001(3207)) && \u0089\u0003.\u007E\u0018\u0005((object) obj0, \u0084.\u0001(3228)))
        return \u0084.\u0001(3249);
      return obj0;
    }

    private void \u0013([In] object obj0, [In] FieldInfo obj1)
    {
      string str1 = obj1 == null ? (string) null : \u0013\u0007.\u007E\u0094\u0006((object) obj1);
      string str2 = obj1 == null ? \u0084.\u0001(3331) : \u0084.\u0001(3322);
      if (obj0 == null)
      {
        \u001C\u0005.\u007E\u009A\u000E((object) this.\u000E, str2);
        if (obj1 != null)
        {
          if (\u0006\u0007.\u007E\u0089\u0008((object) obj1))
            \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3340), \u0084.\u0001(3197));
          Type type = \u008D\u0003.\u007E\u0086\u0008((object) obj1);
          if (type != null && \u0006\u0007.\u007E\u0011\u0007((object) type))
          {
            this.\u0013(\u008D\u0003.\u007E\u0012\u0007((object) type));
            if (\u0006\u0007.\u007E\u000E\u0007((object) type))
              \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3349), \u0084.\u0001(3197));
            if (\u0006\u0007.\u007E\u000F\u0007((object) type))
              \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3358), \u0084.\u0001(3197));
            if (\u0006\u0007.\u007E\u0008\u0007((object) type))
              \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3371), \u0083\u0006.\u007E\u009D\u0006((object) type).ToString());
          }
          else
            this.\u0013(type);
        }
        if (str1 != null)
          this.\u0013(str1);
        \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3380), \u0084.\u0001(3197));
        \u001B\u0006.\u007E\u009B\u000E((object) this.\u000E);
      }
      else
      {
        Type type = \u008D\u0003.\u007E\u009F\u0004(obj0);
        string str3 = (string) null;
        string str4 = (string) null;
        if (obj0 is string)
          str3 = \u0084.\u0001(3389);
        if (str3 == null)
        {
          if (\u0006\u0007.\u007E\u0010\u0007((object) type) || obj0 is IntPtr || obj0 is UIntPtr)
          {
            str3 = \u0013\u0007.\u007E\u009B\u0006((object) type);
            if (obj0 is char)
            {
              int num = (int) (char) obj0;
              StringBuilder stringBuilder1 = new StringBuilder();
              if (num >= 32)
              {
                StringBuilder stringBuilder2 = \u0015\u0004.\u007E\u008B\u0005((object) stringBuilder1, '\'');
                StringBuilder stringBuilder3 = \u0015\u0004.\u007E\u008B\u0005((object) stringBuilder1, (char) obj0);
                StringBuilder stringBuilder4 = \u008A\u0006.\u007E\u008A\u0005((object) stringBuilder1, \u0084.\u0001(3410));
              }
              StringBuilder stringBuilder5 = \u008A\u0006.\u007E\u008A\u0005((object) stringBuilder1, \u0084.\u0001(3415));
              StringBuilder stringBuilder6 = \u008A\u0006.\u007E\u008A\u0005((object) stringBuilder1, num.ToString(\u0084.\u0001(3202)));
              StringBuilder stringBuilder7 = \u0015\u0004.\u007E\u008B\u0005((object) stringBuilder1, ')');
              str4 = \u0013\u0007.\u007E\u009C\u0004((object) stringBuilder1);
            }
            if (obj0 is bool)
              str4 = \u0013\u0007.\u007E\u001D\u0005((object) \u0013\u0007.\u007E\u009C\u0004(obj0));
            if (str4 == null)
            {
              string str5 = string.Empty;
              try
              {
                str5 = this.\u0013(obj0);
              }
              catch
              {
              }
              if (\u0083\u0006.\u007E\u0014\u0005((object) str5) > 0)
              {
                StringBuilder stringBuilder1 = new StringBuilder();
                StringBuilder stringBuilder2 = \u008A\u0006.\u007E\u008A\u0005((object) stringBuilder1, \u0013\u0007.\u007E\u009C\u0004(obj0));
                StringBuilder stringBuilder3 = \u008A\u0006.\u007E\u008A\u0005((object) stringBuilder1, \u0084.\u0001(3420));
                StringBuilder stringBuilder4 = \u008A\u0006.\u007E\u008A\u0005((object) stringBuilder1, str5);
                StringBuilder stringBuilder5 = \u0015\u0004.\u007E\u008B\u0005((object) stringBuilder1, ')');
                str4 = \u0013\u0007.\u007E\u009C\u0004((object) stringBuilder1);
              }
              else
                str4 = \u0013\u0007.\u007E\u009C\u0004(obj0);
            }
          }
          else if (\u0006\u0007.\u007E\u0006\u0007((object) type) && \u0087\u0003.\u007E\u0098\u0006((object) type) != \u0087\u0003.\u007E\u0098\u0006((object) \u008D\u0003.\u009F\u0004((object) this)))
            str3 = \u0013\u0007.\u007E\u009B\u0006((object) type);
        }
        if (str3 != null)
        {
          \u001C\u0005.\u007E\u009A\u000E((object) this.\u000E, str2);
          if (obj1 != null && \u0006\u0007.\u007E\u0089\u0008((object) obj1))
            \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3340), \u0084.\u0001(3197));
          this.\u0013(type);
          if (str1 != null)
            this.\u0013(str1);
          if (\u0006\u0007.\u007E\u0007\u0007((object) type))
            str4 = \u0013\u0007.\u007E\u009C\u0004(obj0);
          if (obj0 is Guid)
            str4 = \u008F\u0006.\u0084\u0005(\u0084.\u0001(2854), \u0013\u0007.\u007E\u009C\u0004(obj0), \u0084.\u0001(3429));
          if (str4 == null)
            str4 = \u008F\u0006.\u0084\u0005(\u0084.\u0001(3434), \u0013\u0007.\u007E\u009C\u0004(obj0), \u0084.\u0001(3434));
          \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3439), this.\u0013(str4));
          \u001B\u0006.\u007E\u009B\u000E((object) this.\u000E);
        }
        else
        {
          \u001C\u0005.\u007E\u009A\u000E((object) this.\u000E, str2);
          if (obj1 != null && \u0006\u0007.\u007E\u0089\u0008((object) obj1))
            \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3340), \u0084.\u0001(3197));
          int num = -1;
          for (int index = 0; index < \u0083\u0006.\u007E\u008B\u0007((object) this.\u000F); ++index)
          {
            try
            {
              if (\u001E\u0005.\u007E\u009D\u0004(\u0013\u0006.\u007E\u008C\u0007((object) this.\u000F, index), obj0))
              {
                num = index;
                break;
              }
            }
            catch
            {
            }
          }
          if (num == -1)
            num = \u009C\u0004.\u007E\u008D\u0007((object) this.\u000F, obj0);
          \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3448), num.ToString());
          if (str1 != null)
            this.\u0013(str1);
          \u001B\u0006.\u007E\u009B\u000E((object) this.\u000E);
        }
      }
    }

    private void \u0013([In] string obj0)
    {
      int num = this.\u0013(obj0);
      if (num != -1)
        \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3453), num.ToString());
      else
        \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3462), obj0);
    }

    private \u0084.\u0014 \u0013([In] Type obj0)
    {
      \u0084.\u0014 obj = \u0084.\u0014.\u0013();
      if (obj0 != null && \u0088\u0002.\u007E\u0008\u0008((object) \u0018\u0007.\u007E\u0099\u0006((object) obj0), \u0084.\u0001(3471)) != null)
      {
        obj.\u0001 = ((\u0083\u0006.\u007E\u0096\u0006((object) obj0) & 16777215) - 1).ToString();
        Assembly assembly = \u0018\u0007.\u007E\u0099\u0006((object) obj0);
        obj.\u0002 = new \u0084.\u0013(\u0010\u0007.\u007E\u001F\u0008((object) \u0087\u0003.\u007E\u0010\u0008((object) assembly)).ToString(\u0084.\u0001(3532)), \u0013\u0007.\u007E\u0007\u0008((object) assembly));
      }
      return obj;
    }

    private int \u0013([In] \u0084.\u0014 obj0)
    {
      string str = \u0013\u0007.\u007E\u001E\u0005((object) obj0.\u0002.\u0001);
      if (\u001E\u0005.\u007E\u0095\u0007((object) this.\u0013, (object) str))
        return (int) \u001D\u0004.\u007E\u0096\u0007((object) this.\u0013, (object) str);
      int num = \u009C\u0004.\u007E\u008D\u0007((object) this.\u0012, (object) obj0.\u0002);
      \u008C\u0005.\u007E\u0093\u0007((object) this.\u0013, (object) str, (object) num);
      return num;
    }

    private void \u0013([In] Type obj0)
    {
      if (obj0 == null)
        return;
      try
      {
        \u0084.\u0014 obj = this.\u0013(obj0);
        if (!obj.\u0013())
        {
          \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3537), obj.\u0001);
          int num = this.\u0013(obj);
          if (num <= 0)
            return;
          \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3550), num.ToString());
        }
        else
        {
          string str1 = \u0013\u0007.\u007E\u009B\u0006((object) obj0);
          int num1;
          if (\u001E\u0005.\u007E\u0095\u0007((object) this.\u0011, (object) str1))
          {
            num1 = (int) \u001D\u0004.\u007E\u0096\u0007((object) this.\u0011, (object) str1);
          }
          else
          {
            StringBuilder stringBuilder1 = new StringBuilder();
            string str2 = \u0013\u0007.\u007E\u001D\u0008((object) \u0086\u0004.\u007E\u0006\u0008((object) \u0018\u0007.\u007E\u0099\u0006((object) obj0)));
            if (\u0083\u0006.\u007E\u0014\u0005((object) str2) > 0 && \u009E\u0002.\u0012\u0005(str2, \u0084.\u0001(3563)))
            {
              StringBuilder stringBuilder2 = \u0015\u0004.\u007E\u008B\u0005((object) stringBuilder1, '[');
              StringBuilder stringBuilder3 = \u008A\u0006.\u007E\u008A\u0005((object) stringBuilder1, str2);
              StringBuilder stringBuilder4 = \u0015\u0004.\u007E\u008B\u0005((object) stringBuilder1, ']');
            }
            string str3 = \u0013\u0007.\u007E\u009C\u0006((object) obj0);
            if (\u0083\u0006.\u007E\u0014\u0005((object) str3) > 0)
            {
              StringBuilder stringBuilder2 = \u008A\u0006.\u007E\u008A\u0005((object) stringBuilder1, str3);
              StringBuilder stringBuilder3 = \u0015\u0004.\u007E\u008B\u0005((object) stringBuilder1, '.');
            }
            if (\u0006\u0007.\u007E\u0011\u0007((object) obj0))
              obj0 = \u008D\u0003.\u007E\u0012\u0007((object) obj0);
            int num2 = \u0011\u0003.\u007E\u001B\u0005((object) str1, \u0084.\u0001(3576));
            if (num2 > 0)
            {
              string str4 = \u008A\u0002.\u007E\u007F\u0005((object) \u001F\u0005.\u007E\u0017\u0005((object) str1, \u0083\u0006.\u007E\u0014\u0005((object) str3) + 1, num2 - \u0083\u0006.\u007E\u0014\u0005((object) str3)), \u0084.\u0001(3576), \u0084.\u0001(3581));
              StringBuilder stringBuilder2 = \u008A\u0006.\u007E\u008A\u0005((object) stringBuilder1, str4);
            }
            StringBuilder stringBuilder5 = \u008A\u0006.\u007E\u008A\u0005((object) stringBuilder1, \u0013\u0007.\u007E\u0094\u0006((object) obj0));
            num1 = \u009C\u0004.\u007E\u008D\u0007((object) this.\u0010, (object) \u0013\u0007.\u007E\u009C\u0004((object) stringBuilder1));
            \u008C\u0005.\u007E\u0093\u0007((object) this.\u0011, (object) str1, (object) num1);
          }
          \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3586), num1.ToString());
        }
      }
      catch
      {
      }
    }

    private int \u0013([In] string obj0)
    {
      try
      {
        bool flag1 = this.\u008C\u0002()[0] == '\x0001';
        if (obj0 == null || \u0083\u0006.\u007E\u0014\u0005((object) obj0) == 0 || flag1 && \u0083\u0006.\u007E\u0014\u0005((object) obj0) > 4 || !flag1 && \u0086\u0005.\u007E\u0013\u0005((object) obj0, 0) != '#')
          return -1;
        int num = 0;
        for (int index1 = \u0083\u0006.\u007E\u0014\u0005((object) obj0) - 1; index1 >= 0 && (flag1 || index1 != 0); --index1)
        {
          char ch = \u0086\u0005.\u007E\u0013\u0005((object) obj0, index1);
          bool flag2 = false;
          for (int index2 = 0; index2 < this.\u008C\u0002().Length; ++index2)
          {
            if ((int) this.\u008C\u0002()[index2] == (int) ch)
            {
              num = num * this.\u008C\u0002().Length + index2;
              flag2 = true;
              break;
            }
          }
          if (!flag2)
            return -1;
        }
        return num;
      }
      catch
      {
        return -1;
      }
    }

    protected virtual Guid \u0090\u0002()
    {
      return Guid.Empty;
    }

    private string \u0013()
    {
      try
      {
        return \u0008\u0004.\u0094\u000F();
      }
      catch
      {
        return \u0084.\u0001(3599);
      }
    }

    private Assembly[] \u0013()
    {
      try
      {
        return \u0098\u0005.\u007E\u000E\u0006((object) \u0016\u0006.\u0008\u0006());
      }
      catch
      {
        return new Assembly[1]{ this.\u0013() };
      }
    }

    private Assembly \u0013()
    {
      try
      {
        return \u001B\u0003.\u0016\u0008();
      }
      catch
      {
        return (Assembly) null;
      }
    }

    private byte[] \u0013([In] string obj0)
    {
      MemoryStream memoryStream = new MemoryStream();
      this.\u000E = new XmlTextWriter((Stream) memoryStream, (Encoding) new UTF8Encoding(false));
      \u001B\u0006.\u007E\u0098\u000E((object) this.\u000E);
      \u001C\u0005.\u007E\u009A\u000E((object) this.\u000E, \u0084.\u0001(3604));
      \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3637), \u0013\u0007.\u007E\u001E\u0005((object) \u0084.\u0001(3654)));
      \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3707), \u009F\u0006.\u0099\u0005().ToString(\u0084.\u0001(3720)));
      \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3725), this.\u0013());
      Guid guid = this.\u0090\u0002();
      if (\u0082\u0002.\u0083\u0006(guid, Guid.Empty))
        \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3734), guid.ToString(\u0084.\u0001(3532)));
      if (\u0083\u0006.\u007E\u0014\u0005((object) obj0) > 0)
        \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3743), obj0);
      if (\u0083\u0006.\u007E\u008B\u0007((object) this.\u0012) > 0)
        \u001B\u0006.\u007E\u008E\u0007((object) this.\u0012);
      int num1 = \u009C\u0004.\u007E\u008D\u0007((object) this.\u0012, (object) new \u0084.\u0013(\u0084.\u0001(3654), string.Empty));
      if (\u0083\u0006.\u007E\u0098\u0007((object) this.\u0013) > 0)
        \u001B\u0006.\u007E\u0094\u0007((object) this.\u0013);
      \u008C\u0005.\u007E\u0093\u0007((object) this.\u0013, (object) \u0084.\u0001(3654), (object) 0);
      \u001C\u0005.\u007E\u009A\u000E((object) this.\u000E, \u0084.\u0001(3756));
      Assembly assembly1 = this.\u0013();
      foreach (Assembly assembly2 in this.\u0013())
      {
        if (assembly2 != null)
        {
          \u001C\u0005.\u007E\u009A\u000E((object) this.\u000E, \u0084.\u0001(3550));
          try
          {
            \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3462), \u0013\u0007.\u007E\u0007\u0008((object) assembly2));
            \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3773), \u0013\u0007.\u007E\u0005\u0008((object) assembly2));
            if (assembly2 == assembly1)
              \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3786), \u0084.\u0001(3197));
          }
          catch
          {
          }
          \u001B\u0006.\u007E\u009B\u000E((object) this.\u000E);
        }
      }
      \u001B\u0006.\u007E\u009B\u000E((object) this.\u000E);
      \u001C\u0005.\u007E\u009A\u000E((object) this.\u000E, \u0084.\u0001(3795));
      if (this.\u0007 != null && \u0083\u0006.\u007E\u0098\u0007((object) this.\u0007) > 0)
      {
        IEnumerator enumerator = \u0014\u0006.\u007E\u0001\u0005((object) \u0011\u0007.\u007E\u0097\u0007((object) this.\u0007));
        try
        {
          while (\u0006\u0007.\u007E\u0008\u0005((object) enumerator))
          {
            string str1 = (string) \u0097\u0005.\u007E\u000E\u0005((object) enumerator);
            \u001C\u0005.\u007E\u009A\u000E((object) this.\u000E, \u0084.\u0001(3820));
            \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3462), str1);
            string str2 = (string) \u001D\u0004.\u007E\u0096\u0007((object) this.\u0007, (object) str1);
            if (str2 == null)
              \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3380), \u0084.\u0001(3197));
            else
              \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3439), \u008F\u0006.\u0084\u0005(\u0084.\u0001(3434), str2, \u0084.\u0001(3434)));
            \u001B\u0006.\u007E\u009B\u000E((object) this.\u000E);
          }
        }
        finally
        {
          IDisposable disposable = enumerator as IDisposable;
          if (disposable != null)
            \u001B\u0006.\u007E\u000F\u0005((object) disposable);
        }
      }
      \u001B\u0006.\u007E\u009B\u000E((object) this.\u000E);
      if (this.\u0008 != null && \u0083\u0006.\u007E\u0098\u0007((object) this.\u0008) > 0)
      {
        \u001C\u0005.\u007E\u009A\u000E((object) this.\u000E, \u0084.\u0001(3841));
        IEnumerator enumerator = \u0014\u0006.\u007E\u0001\u0005((object) \u0011\u0007.\u007E\u0097\u0007((object) this.\u0008));
        try
        {
          while (\u0006\u0007.\u007E\u0008\u0005((object) enumerator))
          {
            string str = (string) \u0097\u0005.\u007E\u000E\u0005((object) enumerator);
            \u001C\u0005.\u007E\u009A\u000E((object) this.\u000E, \u0084.\u0001(3862));
            \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3879), str);
            \u0084.\u000E obj = (\u0084.\u000E) \u001D\u0004.\u007E\u0096\u0007((object) this.\u0008, (object) str);
            \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3884), obj.\u0001);
            \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3897), obj.\u0004.ToString());
            if (\u0083\u0006.\u007E\u0014\u0005((object) obj.\u0003) > 0)
              \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3906), obj.\u0003);
            else
              \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3915), obj.\u0002);
            \u001B\u0006.\u007E\u009B\u000E((object) this.\u000E);
          }
        }
        finally
        {
          IDisposable disposable = enumerator as IDisposable;
          if (disposable != null)
            \u001B\u0006.\u007E\u000F\u0005((object) disposable);
        }
        \u001B\u0006.\u007E\u009B\u000E((object) this.\u000E);
      }
      \u001C\u0005.\u007E\u009A\u000E((object) this.\u000E, \u0084.\u0001(3924));
      try
      {
        \u0006\u0005.\u007E\u0001\u000F((object) this.\u000E, \u0084.\u0001(3949), \u0013\u0007.\u007E\u009C\u0004((object) \u0001\u0004.\u007E\u0093\u0006((object) \u0083\u0002.\u007F\u0006())));
        \u0006\u0005.\u007E\u0001\u000F((object) this.\u000E, \u0084.\u0001(3962), \u0013\u0007.\u007E\u009C\u0004((object) \u0004\u0007.\u007E\u0092\u0006((object) \u0083\u0002.\u007F\u0006())));
        \u0006\u0005.\u007E\u0001\u000F((object) this.\u000E, \u0084.\u0001(3979), \u008D.\u0013());
      }
      catch
      {
      }
      \u001B\u0006.\u007E\u009B\u000E((object) this.\u000E);
      ArrayList arrayList = new ArrayList();
      for (Exception exception = this.\u0006; exception != null; exception = \u008B\u0003.\u007E\u0091\u0005((object) exception))
      {
        Type type = \u008D\u0003.\u007E\u0094\u0005((object) exception);
        if (\u009E\u0002.\u0011\u0005(\u0013\u0007.\u007E\u0094\u0006((object) type), \u0084.\u0001(3996)) && \u009E\u0002.\u0011\u0005(\u0013\u0007.\u007E\u009C\u0006((object) type), \u0084.\u0001(4021)))
        {
          int num2 = \u009C\u0004.\u007E\u008D\u0007((object) arrayList, (object) exception);
        }
        else
          \u0093\u0002.\u007E\u0090\u0007((object) arrayList, 0, (object) exception);
      }
      \u001C\u0005.\u007E\u009A\u000E((object) this.\u000E, \u0084.\u0001(4066));
      int num3 = \u0083\u0006.\u007E\u008B\u0007((object) arrayList);
      int num4 = 0;
      IEnumerator enumerator1 = \u0014\u0006.\u007E\u008F\u0007((object) arrayList);
      try
      {
        while (\u0006\u0007.\u007E\u0008\u0005((object) enumerator1))
        {
          Exception exception = (Exception) \u0097\u0005.\u007E\u000E\u0005((object) enumerator1);
          ++num4;
          if (num4 > 100 && num4 == num3 - 100)
          {
            \u001C\u0005.\u007E\u009A\u000E((object) this.\u000E, \u0084.\u0001(4083));
            \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(4104), num3.ToString());
            \u001B\u0006.\u007E\u009B\u000E((object) this.\u000E);
          }
          else if (num4 <= 100 || num4 > num3 - 100)
          {
            Type type = \u008D\u0003.\u007E\u0094\u0005((object) exception);
            if (\u009E\u0002.\u0011\u0005(\u0013\u0007.\u007E\u0094\u0006((object) type), \u0084.\u0001(3996)) && \u009E\u0002.\u0011\u0005(\u0013\u0007.\u007E\u009C\u0006((object) type), \u0084.\u0001(4021)))
            {
              int num2 = 0;
              int num5 = -1;
              object[] objArray = (object[]) null;
              \u0084.\u0014 obj1 = \u0084.\u0014.\u0013();
              bool flag = true;
              try
              {
                num2 = (int) \u001D\u0004.\u007E\u0087\u0008((object) \u0093\u0005.\u007E\u0003\u0007((object) type, \u0084.\u0001(4129)), (object) exception);
                num5 = (int) \u001D\u0004.\u007E\u0087\u0008((object) \u0093\u0005.\u007E\u0003\u0007((object) type, \u0084.\u0001(4142)), (object) exception);
                objArray = (object[]) \u001D\u0004.\u007E\u0087\u0008((object) \u0093\u0005.\u007E\u0003\u0007((object) type, \u0084.\u0001(4155)), (object) exception);
                obj1 = this.\u0013(type);
              }
              catch
              {
                flag = false;
              }
              if (flag && !obj1.\u0013())
              {
                \u001C\u0005.\u007E\u009A\u000E((object) this.\u000E, \u0084.\u0001(4168));
                \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(4129), num2.ToString());
                int num6 = this.\u0013(obj1);
                if (num6 > 0)
                  \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3550), num6.ToString());
                if (num5 != -1)
                  \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(4142), num5.ToString());
                foreach (object obj2 in objArray)
                {
                  try
                  {
                    this.\u0013(obj2, (FieldInfo) null);
                  }
                  catch
                  {
                  }
                }
                \u001B\u0006.\u007E\u009B\u000E((object) this.\u000E);
              }
            }
            else
            {
              \u001C\u0005.\u007E\u009A\u000E((object) this.\u000E, \u0084.\u0001(4185));
              try
              {
                this.\u0013(\u008D\u0003.\u007E\u0094\u0005((object) exception));
                string str1 = \u0084.\u0001(3599);
                try
                {
                  str1 = \u0013\u0007.\u007E\u0090\u0005((object) exception);
                }
                catch
                {
                }
                \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(4198), str1);
                string str2 = \u0013\u0007.\u007E\u001F\u0005((object) \u0013\u0007.\u007E\u0092\u0005((object) exception));
                \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(4211), str2);
                int num2 = \u008F\u0005.\u007E\u0019\u0005((object) str2, ' ');
                string str3 = \u008C\u0002.\u007E\u0016\u0005((object) str2, num2 + 1);
                int num5 = \u0011\u0003.\u007E\u001A\u0005((object) str3, \u0084.\u0001(4240));
                if (num5 != -1)
                  str3 = \u001F\u0005.\u007E\u0017\u0005((object) str3, 0, num5);
                \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(4245), str3);
              }
              catch
              {
              }
              \u001B\u0006.\u007E\u009B\u000E((object) this.\u000E);
            }
          }
        }
      }
      finally
      {
        IDisposable disposable = enumerator1 as IDisposable;
        if (disposable != null)
          \u001B\u0006.\u007E\u000F\u0005((object) disposable);
      }
      \u001B\u0006.\u007E\u009B\u000E((object) this.\u000E);
      \u001C\u0005.\u007E\u009A\u000E((object) this.\u000E, \u0084.\u0001(4155));
      int num7 = \u0083\u0006.\u007E\u008B\u0007((object) this.\u000F);
      for (int index1 = 0; index1 < \u0083\u0006.\u007E\u008B\u0007((object) this.\u000F); ++index1)
      {
        object obj1 = \u0013\u0006.\u007E\u008C\u0007((object) this.\u000F, index1);
        Type type = \u008D\u0003.\u007E\u009F\u0004(obj1);
        \u001C\u0005.\u007E\u009A\u000E((object) this.\u000E, \u0084.\u0001(4254));
        \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3448), index1.ToString());
        string str = (string) null;
        try
        {
          str = \u0013\u0007.\u007E\u009C\u0004(obj1);
          str = !\u009E\u0002.\u0011\u0005(str, \u0013\u0007.\u007E\u009B\u0006((object) type)) ? (!\u0006\u0007.\u007E\u0007\u0007((object) type) ? (!(obj1 is Guid) ? \u008F\u0006.\u0084\u0005(\u0084.\u0001(3434), str, \u0084.\u0001(3434)) : \u008F\u0006.\u0084\u0005(\u0084.\u0001(2854), str, \u0084.\u0001(3429))) : \u0008\u0006.\u0010\u0005(type, obj1, \u0084.\u0001(4267))) : (string) null;
        }
        catch
        {
        }
        if (str != null)
          \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3439), this.\u0013(str));
        if (\u0006\u0007.\u007E\u0011\u0007((object) type))
        {
          this.\u0013(\u008D\u0003.\u007E\u0012\u0007((object) type));
          if (\u0006\u0007.\u007E\u000E\u0007((object) type))
            \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3349), \u0084.\u0001(3197));
          if (\u0006\u0007.\u007E\u000F\u0007((object) type))
            \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3358), \u0084.\u0001(3197));
          if (\u0006\u0007.\u007E\u0008\u0007((object) type))
          {
            Array array = (Array) obj1;
            \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3371), \u0083\u0006.\u007E\u0007\u0005((object) array).ToString());
            StringBuilder stringBuilder1 = new StringBuilder();
            for (int index2 = 0; index2 < \u0083\u0006.\u007E\u0007\u0005((object) array); ++index2)
            {
              if (index2 > 0)
              {
                StringBuilder stringBuilder2 = \u0015\u0004.\u007E\u008B\u0005((object) stringBuilder1, ',');
              }
              StringBuilder stringBuilder3 = \u0089\u0005.\u007E\u008C\u0005((object) stringBuilder1, \u009B\u0006.\u007E\u0006\u0005((object) array, index2));
            }
            \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3897), \u0013\u0007.\u007E\u009C\u0004((object) stringBuilder1));
            if (\u0083\u0006.\u007E\u0007\u0005((object) array) == 1)
            {
              int num2 = \u0083\u0006.\u007E\u0005\u0005((object) array);
              for (int index2 = 0; index2 < num2; ++index2)
              {
                if (index2 == 10)
                {
                  if (num2 > 16)
                    index2 = num2 - 5;
                }
                try
                {
                  this.\u0013(\u0013\u0006.\u007E\u0004\u0005((object) array, index2), (FieldInfo) null);
                }
                catch
                {
                }
              }
            }
          }
        }
        else
        {
          this.\u0013(type);
          if (index1 < num7)
          {
            if (obj1 is IEnumerable)
            {
              \u001C\u0005.\u007E\u009A\u000E((object) this.\u000E, \u0084.\u0001(4272));
              try
              {
                int num2 = 0;
                IEnumerator enumerator2 = \u0014\u0006.\u007E\u0001\u0005((object) (IEnumerable) obj1);
                try
                {
                  while (\u0006\u0007.\u007E\u0008\u0005((object) enumerator2))
                  {
                    object obj2 = \u0097\u0005.\u007E\u000E\u0005((object) enumerator2);
                    if (num2 > 20)
                    {
                      \u0006\u0005.\u007E\u0001\u000F((object) this.\u000E, \u0084.\u0001(4289), string.Empty);
                      break;
                    }
                    this.\u0013(obj2, (FieldInfo) null);
                    ++num2;
                  }
                }
                finally
                {
                  IDisposable disposable = enumerator2 as IDisposable;
                  if (disposable != null)
                    \u001B\u0006.\u007E\u000F\u0005((object) disposable);
                }
              }
              catch
              {
              }
              \u001B\u0006.\u007E\u009B\u000E((object) this.\u000E);
            }
            bool flag = true;
            while (type != null)
            {
              if (!flag)
              {
                \u001C\u0005.\u007E\u009A\u000E((object) this.\u000E, \u0084.\u0001(4298));
                this.\u0013(type);
                \u001B\u0006.\u007E\u009B\u000E((object) this.\u000E);
              }
              FieldInfo[] fieldInfoArray = \u0011\u0005.\u007E\u0004\u0007((object) type, BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
              if (fieldInfoArray.Length > 0)
              {
                for (int index2 = 0; index2 < fieldInfoArray.Length; ++index2)
                {
                  try
                  {
                    if (!\u0006\u0007.\u007E\u008B\u0008((object) fieldInfoArray[index2]))
                    {
                      if (\u0006\u0007.\u007E\u0089\u0008((object) fieldInfoArray[index2]))
                      {
                        if (\u0006\u0007.\u007E\u008A\u0008((object) fieldInfoArray[index2]))
                          continue;
                      }
                      this.\u0013(\u001D\u0004.\u007E\u0087\u0008((object) fieldInfoArray[index2], obj1), fieldInfoArray[index2]);
                    }
                  }
                  catch
                  {
                  }
                }
              }
              type = \u008D\u0003.\u007E\u009E\u0006((object) type);
              flag = false;
              if (this.\u0013(type).\u0013())
                break;
            }
          }
        }
        \u001B\u0006.\u007E\u009B\u000E((object) this.\u000E);
      }
      \u001B\u0006.\u007E\u009B\u000E((object) this.\u000E);
      \u001C\u0005.\u007E\u009A\u000E((object) this.\u000E, \u0084.\u0001(4315));
      \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(4328), \u0083\u0006.\u007E\u008B\u0007((object) this.\u0010).ToString());
      for (int index = 0; index < \u0083\u0006.\u007E\u008B\u0007((object) this.\u0010); ++index)
      {
        string empty = string.Empty;
        string str;
        try
        {
          str = \u0013\u0007.\u007E\u009C\u0004(\u0013\u0006.\u007E\u008C\u0007((object) this.\u0010, index));
        }
        catch (Exception ex)
        {
          str = \u0095\u0005.\u0082\u0005((object) '"', (object) \u0013\u0007.\u007E\u0090\u0005((object) ex), (object) '"');
        }
        \u0006\u0005.\u007E\u0001\u000F((object) this.\u000E, \u0084.\u0001(3586), str);
      }
      \u001B\u0006.\u007E\u009B\u000E((object) this.\u000E);
      \u001C\u0005.\u007E\u009A\u000E((object) this.\u000E, \u0084.\u0001(4337));
      \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(4328), \u0083\u0006.\u007E\u008B\u0007((object) this.\u0012).ToString());
      for (int index = 0; index < \u0083\u0006.\u007E\u008B\u0007((object) this.\u0012); ++index)
      {
        \u001C\u0005.\u007E\u009A\u000E((object) this.\u000E, \u0084.\u0001(3637));
        \u0084.\u0013 obj = (\u0084.\u0013) \u0013\u0006.\u007E\u008C\u0007((object) this.\u0012, index);
        \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(3448), obj.\u0001);
        if (\u0083\u0006.\u007E\u0014\u0005((object) obj.\u0002) > 0)
          \u0006\u0005.\u007E\u009C\u000E((object) this.\u000E, \u0084.\u0001(4354), obj.\u0002);
        \u001B\u0006.\u007E\u009B\u000E((object) this.\u000E);
      }
      \u001B\u0006.\u007E\u009B\u000E((object) this.\u000E);
      \u001B\u0006.\u007E\u009B\u000E((object) this.\u000E);
      \u001B\u0006.\u007E\u0099\u000E((object) this.\u000E);
      \u001B\u0006.\u007E\u009F\u000E((object) this.\u000E);
      \u001B\u0006.\u007E\u0006\u000E((object) memoryStream);
      return \u0016\u0003.\u007E\u0018\u000E((object) memoryStream);
    }

    internal bool \u0013([In] string obj0)
    {
      try
      {
        byte[] numArray1 = this.\u0013(\u0018\u0006.\u0084\u0006().ToString(\u0084.\u0001(3532)));
        FileStream fileStream = \u0004\u0005.\u0015\u000E(obj0);
        byte[] numArray2 = \u000E2.\u0013(\u001B.\u0084\u0002(numArray1), \u0084.\u0001(4367));
        byte[] numArray3 = \u0003\u0006.\u007E\u0092\u0008((object) \u0003\u0003.\u0091\u0008(), \u0084.\u0001(3654));
        \u009D\u0006.\u007E\u000F\u000E((object) fileStream, numArray3, 0, numArray3.Length);
        \u009D\u0006.\u007E\u000F\u000E((object) fileStream, numArray2, 0, numArray2.Length);
        \u001B\u0006.\u007E\u0005\u000E((object) fileStream);
        return true;
      }
      catch (ThreadAbortException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        return false;
      }
    }

    internal void \u0013()
    {
      try
      {
        string str1 = \u0008\u0004.\u001A\u000E();
        this.\u0013(str1);
        string str2 = this.\u0014(\u0084.\u0001(3725));
        if (\u0083\u0006.\u007E\u0014\u0005((object) str2) > 0 && !\u0089\u0003.\u007E\u0018\u0005((object) str2, \u0084.\u0001(4693)))
          str2 = \u0006\u0003.\u0083\u0005(str2, \u0084.\u0001(4693));
        Process process = \u0010\u0003.\u000E\u0012(\u0006\u0003.\u0083\u0005(str2, \u0084.\u0001(4698)), \u008F\u0006.\u0084\u0005(\u0084.\u0001(4727), str1, \u0084.\u0001(3434)));
        if (this.\u0014 == null)
          return;
        \u0086\u0006.\u007E\u0081\u0006((object) this.\u0014, (object) this, EventArgs.Empty);
      }
      catch (ThreadAbortException ex)
      {
      }
      catch (Exception ex)
      {
        this.\u0089\u0002(new \u008A(ex));
      }
    }

    internal bool \u0013()
    {
      try
      {
        if (this.\u0015 != null)
          goto label_35;
label_1:
        byte[] numArray1;
        try
        {
          numArray1 = this.\u0013(string.Empty);
        }
        catch (Exception ex)
        {
          int num = -1;
          try
          {
            StackTrace stackTrace = new StackTrace(ex);
            if (\u0083\u0006.\u007E\u0001\u0008((object) stackTrace) > 0)
            {
              StackFrame stackFrame = \u0014\u0005.\u007E\u0002\u0008((object) stackTrace, \u0083\u0006.\u007E\u0001\u0008((object) stackTrace) - 1);
              num = \u0083\u0006.\u007E\u0004\u0008((object) stackFrame);
            }
          }
          catch
          {
          }
          if (this.\u0015 != null)
            this.\u0015((object) this, new \u0088(\u007F.\u0001, \u008D\u0006.\u0081\u0005(\u0084.\u0001(4756), (object) \u0013\u0007.\u007E\u0090\u0005((object) ex), (object) num)));
          return false;
        }
        byte[] numArray2 = \u001B.\u0084\u0002(numArray1);
        if (numArray2 == null)
        {
          if (this.\u0015 != null)
            this.\u0015((object) this, new \u0088(\u007F.\u0001, \u001B.\u0001));
          return false;
        }
        byte[] numArray3 = \u000E2.\u0013(numArray2, \u0084.\u0001(4367));
        if (numArray3 == null)
        {
          if (this.\u0015 != null)
            this.\u0015((object) this, new \u0088(\u007F.\u0001, \u000E2.\u0001));
          return false;
        }
        if (this.\u0015 != null)
          this.\u0015((object) this, new \u0088(\u007F.\u0002));
        \u0013 obj = new \u0013(\u0084.\u0001(4789));
        if (this.\u0003 != null)
          obj.\u0013(this.\u0003);
        string str1 = obj.\u0013();
        if (\u009E\u0002.\u0011\u0005(str1, \u0084.\u0001(4842)))
        {
          if (this.\u0015 != null)
            this.\u0015((object) this, new \u0088(\u007F.\u0003));
          byte[] numArray4 = \u0003\u0006.\u007E\u0092\u0008((object) \u0003\u0003.\u0091\u0008(), \u0084.\u0001(3654));
          byte[] numArray5 = new byte[numArray4.Length + numArray3.Length];
          \u0003\u0005.\u0002\u0005((Array) numArray4, (Array) numArray5, numArray4.Length);
          \u008B\u0002.\u0003\u0005((Array) numArray3, 0, (Array) numArray5, numArray4.Length, numArray3.Length);
          string str2 = obj.\u0013(numArray5);
          if (\u0089\u0003.\u007E\u001C\u0005((object) str2, \u0084.\u0001(4847)))
          {
            if (this.\u0015 != null)
              this.\u0015((object) this, new \u0088(\u007F.\u0003, str2));
            return false;
          }
          if (this.\u0015 != null)
          {
            \u0088 e = new \u0088(\u007F.\u0004);
            e.\u0095\u0002(str2);
            this.\u0015((object) this, e);
          }
          return true;
        }
        string str3 = str1;
        if (this.\u0015 != null)
          this.\u0015((object) this, new \u0088(\u007F.\u0002, str3));
        return false;
label_35:
        this.\u0015((object) this, new \u0088(\u007F.\u0001));
        goto label_1;
      }
      catch (ThreadAbortException ex)
      {
        return false;
      }
      catch (Exception ex)
      {
        this.\u0089\u0002(new \u008A(ex));
        return false;
      }
    }

    private string \u0014([In] string obj0)
    {
      try
      {
        RegistryKey registryKey = \u000E\u0007.\u007E\u009B\u0008((object) Registry.LocalMachine, \u0084.\u0001(4852)) ?? \u000E\u0007.\u007E\u009B\u0008((object) Registry.LocalMachine, \u0084.\u0001(4885));
        if (registryKey == null)
          return string.Empty;
        string str = (string) \u0094\u0004.\u007E\u009C\u0008((object) registryKey, obj0, (object) string.Empty);
        \u001B\u0006.\u007E\u009A\u0008((object) registryKey);
        return str;
      }
      catch
      {
        return string.Empty;
      }
    }

    internal bool \u0013([In] SecurityException obj0)
    {
      \u0089 obj = new \u0089(obj0);
      this.\u008A\u0002(obj);
      if (obj.\u009B\u0002())
        return false;
      if (!obj.\u0099\u0002())
        \u0001\u0006.\u0099\u000F();
      return true;
    }

    internal string \u0014()
    {
      string str1 = Guid.Empty.ToString(\u0084.\u0001(3532));
      try
      {
        string str2 = this.\u0014(\u0084.\u0001(3725));
        if (\u0083\u0006.\u007E\u0014\u0005((object) str2) > 0 && !\u0089\u0003.\u007E\u0018\u0005((object) str2, \u0084.\u0001(4693)))
          str2 = \u0006\u0003.\u0083\u0005(str2, \u0084.\u0001(4693));
        string url = \u0006\u0003.\u0083\u0005(str2, \u0084.\u0001(4934));
        if (\u000F\u0006.\u0014\u000E(url))
        {
          XmlTextReader xmlTextReader = new XmlTextReader(url);
          if (\u0006\u0007.\u007E\u0003\u000F((object) xmlTextReader))
            str1 = \u0098\u0006.\u007E\u0002\u000F((object) xmlTextReader, \u0084.\u0001(4971));
          \u001B\u0006.\u007E\u0004\u000F((object) xmlTextReader);
        }
      }
      catch
      {
      }
      return str1;
    }

    internal void \u0013([In] Exception obj0, [In] bool obj1)
    {
      if (obj0 == null || obj0 is ThreadAbortException)
        return;
      \u0084.\u0002 = true;
      bool flag = true;
      try
      {
        this.\u0006 = obj0;
        this.\u0007 = (Hashtable) null;
        this.\u0008 = (Hashtable) null;
        \u008C obj2 = new \u008C(this, obj0);
        if (\u009E\u0002.\u0011\u0005(\u0013\u0007.\u007E\u001D\u0005((object) \u0084.\u0001(4789)), \u0013\u0007.\u007E\u001D\u0005((object) this.\u0014())))
          obj2.\u0013();
        \u009E\u0002 obj3 = \u009E\u0002.\u0011\u0005;
        string str1 = \u0013\u0007.\u007E\u001D\u0005((object) \u0084.\u0001(4789));
        Guid empty = Guid.Empty;
        string str2 = \u0013\u0007.\u007E\u001D\u0005((object) empty.ToString(\u0084.\u0001(3532)));
        if (obj3(str1, str2))
          obj2.\u0014();
        if (!obj1)
          obj2.\u0015();
        this.\u0088\u0002(obj2);
        flag = !obj2.\u0099\u0002();
      }
      catch (ThreadAbortException ex)
      {
      }
      catch (Exception ex)
      {
        this.\u0089\u0002(new \u008A(ex));
      }
      this.\u0006 = (Exception) null;
      this.\u0007 = (Hashtable) null;
      this.\u0008 = (Hashtable) null;
      \u0084.\u0002 = false;
      if (!flag)
        return;
      foreach (Assembly assembly in \u0098\u0005.\u007E\u000E\u0006((object) \u0016\u0006.\u0008\u0006()))
      {
        try
        {
          string str = \u0013\u0007.\u007E\u0007\u0008((object) assembly);
          if (\u0089\u0003.\u007E\u0018\u0005((object) str, \u0084.\u0001(4984)))
          {
            if (\u0089\u0003.\u007E\u001C\u0005((object) str, \u0084.\u0001(5009)))
            {
              object obj2 = \u0090\u0006.\u007E\u0082\u0008((object) \u0083\u0003.\u007E\u008C\u0008((object) \u009E\u0003.\u007E\u0005\u0007((object) \u0088\u0002.\u007E\u0008\u0008((object) assembly, \u0084.\u0001(5042)), \u0084.\u0001(5079))), (object) null, (object[]) null);
              object obj3 = \u0090\u0006.\u007E\u0082\u0008((object) \u0019\u0005.\u007E\u009F\u0006((object) \u008D\u0003.\u007E\u009F\u0004(obj2), \u0084.\u0001(5092), new Type[0]), obj2, (object[]) null);
            }
          }
        }
        catch
        {
        }
      }
      \u0001\u0006.\u0099\u000F();
      try
      {
        \u0003\u0007.\u001F\u0006(0);
      }
      catch
      {
      }
    }

    static \u0084()
    {
      \u0006.\u0080\u0002();
      \u0084.\u0001 = \u0084.\u0001(3197);
      \u0084.\u0002 = false;
      \u0084.\u0005 = (\u0084) null;
    }

    private struct \u000E
    {
      public string \u0001;
      public string \u0002;
      public string \u0003;
      public int \u0004;
    }

    private struct \u0013
    {
      public string \u0001;
      public string \u0002;

      public \u0013([In] string obj0, [In] string obj1)
      {
        this.\u0001 = obj0;
        this.\u0002 = obj1;
      }
    }

    private struct \u0014
    {
      public string \u0001;
      public \u0084.\u0013 \u0002;

      [SpecialName]
      public bool \u0013()
      {
        return \u0083\u0006.\u007E\u0014\u0005((object) this.\u0001) == 0;
      }

      [SpecialName]
      public static \u0084.\u0014 \u0013()
      {
        return new \u0084.\u0014(string.Empty, string.Empty, string.Empty);
      }

      public \u0014([In] string obj0, [In] string obj1, [In] string obj2)
      {
        this.\u0001 = obj0;
        this.\u0002 = new \u0084.\u0013(obj1, obj2);
      }
    }
  }
}
