﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: NewItemEditor, Version=1.0.0.7, Culture=neutral, PublicKeyToken=9a6515a4c4e1fd2c
// MVID: 9D5F0B9A-C985-40A9-9939-E28C808E997E
// Assembly location: D:\Server\eab\Tools\Net7 Item Editor.exe

using \u0005;
using \u0007;
using \u000F\u0006;
using \u0010\u0006;
using DevExpress.Data;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Repository;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using SmartAssembly.SmartExceptionsCore;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace \u000F\u0006
{
  internal sealed class \u0012\u0006 : XtraForm
  {
    private IContainer \u0001 = (IContainer) null;
    private int \u000F = 0;
    [NonSerialized]
    internal static \u0004 \u0001;
    private GridControl \u0002;
    private GridView \u0003;
    private GridColumn \u0004;
    private GridColumn \u0005;
    private GridColumn \u0006;
    private GridColumn \u0007;
    private GridColumn \u0008;
    private GridColumn \u000E;

    protected override void Dispose([In] bool obj0)
    {
      bool flag;
      try
      {
        flag = !obj0 || this.\u0001 == null;
        if (!flag)
          \u001B\u0006.\u007E\u000F\u0005((object) this.\u0001);
        \u009C\u0006.\u000E\u0004((object) this, obj0);
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<bool> local1 = (ValueType) flag;
        // ISSUE: variable of a boxed type
        __Boxed<bool> local2 = (ValueType) obj0;
        throw UnhandledException.\u000F\u0003(ex, (object) local1, (object) this, (object) local2);
      }
    }

    private void \u0081\u0003()
    {
      BaseView[] baseViewArray1;
      GridColumn[] gridColumnArray1;
      GridColumnSortInfo[] gridColumnSortInfoArray1;
      try
      {
        this.\u0002 = new GridControl();
        this.\u0003 = new GridView();
        this.\u0004 = new GridColumn();
        this.\u0005 = new GridColumn();
        this.\u0006 = new GridColumn();
        this.\u0007 = new GridColumn();
        this.\u0008 = new GridColumn();
        this.\u000E = new GridColumn();
        \u001B\u0006.\u007E\u009D\u0011((object) this.\u0002);
        \u001B\u0006.\u007E\u009D\u0011((object) this.\u0003);
        \u001B\u0006.\u000F\u0004((object) this);
        \u0092\u0004.\u007E\u009D\u000F((object) this.\u0002, AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0002, new Point(0, 3));
        \u0096\u0006.\u007E\u0093((object) this.\u0002, (BaseView) this.\u0003);
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0002, \u0012\u0006.\u0001(26672));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0002, new Size(592, 365));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0002, 0);
        \u0006\u0004 obj1 = \u0006\u0004.\u007E\u0001\u0002;
        ViewRepositoryCollection repositoryCollection = \u0098\u0003.\u007E\u0092((object) this.\u0002);
        baseViewArray1 = new BaseView[1]
        {
          (BaseView) this.\u0003
        };
        BaseView[] baseViewArray2 = baseViewArray1;
        obj1((object) repositoryCollection, baseViewArray2);
        \u001D\u0005.\u007E\u0087\u0010((object) this.\u0002, new EventHandler(this.\u009A\u0006));
        \u000F\u0003 obj2 = \u000F\u0003.\u007E\u008B;
        GridColumnCollection columnCollection = \u0099\u0003.\u007E\u0012((object) this.\u0003);
        gridColumnArray1 = new GridColumn[6]
        {
          this.\u0004,
          this.\u0005,
          this.\u0006,
          this.\u0007,
          this.\u0008,
          this.\u000E
        };
        GridColumn[] gridColumnArray2 = gridColumnArray1;
        obj2((object) columnCollection, gridColumnArray2);
        \u009B\u0005.\u007E\u000E((object) this.\u0003, this.\u0002);
        \u0011\u0004.\u007E\u0014((object) this.\u0003, 4);
        \u001C\u0005.\u007E\u0008((object) this.\u0003, \u0012\u0006.\u0001(26685));
        \u009C\u0006.\u007E\u0006((object) \u009F\u0003.\u007E\u001E((object) this.\u0003), false);
        \u0013\u0003 obj3 = \u0013\u0003.\u007E\u008F;
        GridColumnSortInfoCollection sortInfoCollection = \u009A\u0003.\u007E\u0013((object) this.\u0003);
        gridColumnSortInfoArray1 = new GridColumnSortInfo[4]
        {
          new GridColumnSortInfo(this.\u0005, ColumnSortOrder.Ascending),
          new GridColumnSortInfo(this.\u0004, ColumnSortOrder.Ascending),
          new GridColumnSortInfo(this.\u0006, ColumnSortOrder.Ascending),
          new GridColumnSortInfo(this.\u000E, ColumnSortOrder.Ascending)
        };
        GridColumnSortInfo[] gridColumnSortInfoArray2 = gridColumnSortInfoArray1;
        obj3((object) sortInfoCollection, gridColumnSortInfoArray2);
        \u001C\u0005.\u007E\u0083((object) this.\u0004, \u0012\u0006.\u0001(26698));
        \u001C\u0005.\u007E\u0084((object) this.\u0004, \u0012\u0006.\u0001(4020));
        \u001C\u0005.\u007E\u0081((object) this.\u0004, \u0012\u0006.\u0001(14099));
        \u009C\u0006.\u007E\u008D((object) \u0096\u0003.\u007E\u0082((object) this.\u0004), false);
        \u009C\u0006.\u007E\u008E((object) \u0096\u0003.\u007E\u0082((object) this.\u0004), true);
        \u001C\u0005.\u007E\u0083((object) this.\u0005, \u0012\u0006.\u0001(26711));
        \u001C\u0005.\u007E\u0084((object) this.\u0005, \u0012\u0006.\u0001(18069));
        \u001C\u0005.\u007E\u0081((object) this.\u0005, \u0012\u0006.\u0001(14159));
        \u009C\u0006.\u007E\u008D((object) \u0096\u0003.\u007E\u0082((object) this.\u0005), false);
        \u009C\u0006.\u007E\u008E((object) \u0096\u0003.\u007E\u0082((object) this.\u0005), true);
        \u001C\u0005.\u007E\u0083((object) this.\u0006, \u0012\u0006.\u0001(26724));
        \u001C\u0005.\u007E\u0084((object) this.\u0006, \u0012\u0006.\u0001(18236));
        \u001C\u0005.\u007E\u0081((object) this.\u0006, \u0012\u0006.\u0001(14236));
        \u009C\u0006.\u007E\u008D((object) \u0096\u0003.\u007E\u0082((object) this.\u0006), false);
        \u009C\u0006.\u007E\u008E((object) \u0096\u0003.\u007E\u0082((object) this.\u0006), true);
        \u001C\u0005.\u007E\u0083((object) this.\u0007, \u0012\u0006.\u0001(26741));
        \u001C\u0005.\u007E\u0084((object) this.\u0007, \u0012\u0006.\u0001(17921));
        \u001C\u0005.\u007E\u0081((object) this.\u0007, \u0012\u0006.\u0001(14275));
        \u009C\u0006.\u007E\u008D((object) \u0096\u0003.\u007E\u0082((object) this.\u0007), false);
        \u009C\u0006.\u007E\u008E((object) \u0096\u0003.\u007E\u0082((object) this.\u0007), true);
        \u009C\u0006.\u007E\u0088((object) this.\u0007, true);
        \u0011\u0004.\u007E\u0087((object) this.\u0007, 0);
        \u0011\u0004.\u007E\u0089((object) this.\u0007, 146);
        \u001C\u0005.\u007E\u0083((object) this.\u0008, \u0012\u0006.\u0001(26750));
        \u001C\u0005.\u007E\u0084((object) this.\u0008, \u0012\u0006.\u0001(17926));
        \u001C\u0005.\u007E\u0081((object) this.\u0008, \u0012\u0006.\u0001(14309));
        \u009C\u0006.\u007E\u008D((object) \u0096\u0003.\u007E\u0082((object) this.\u0008), false);
        \u009C\u0006.\u007E\u008E((object) \u0096\u0003.\u007E\u0082((object) this.\u0008), true);
        \u009C\u0006.\u007E\u0088((object) this.\u0008, true);
        \u0011\u0004.\u007E\u0087((object) this.\u0008, 1);
        \u0011\u0004.\u007E\u0089((object) this.\u0008, 273);
        \u001C\u0005.\u007E\u0083((object) this.\u000E, \u0012\u0006.\u0001(26763));
        \u001C\u0005.\u007E\u0084((object) this.\u000E, \u0012\u0006.\u0001(26772));
        \u001C\u0005.\u007E\u0081((object) this.\u000E, \u0012\u0006.\u0001(14335));
        \u009C\u0006.\u007E\u008D((object) \u0096\u0003.\u007E\u0082((object) this.\u000E), false);
        \u009C\u0006.\u007E\u008E((object) \u0096\u0003.\u007E\u0082((object) this.\u000E), true);
        \u009C\u0006.\u007E\u0088((object) this.\u000E, true);
        \u0011\u0004.\u007E\u0087((object) this.\u000E, 1);
        \u008B\u0004.\u009E\u0010((object) this, new SizeF(6f, 13f));
        \u0002\u0004.\u009F\u0010((object) this, AutoScaleMode.Font);
        \u0087\u0006.\u0016\u0011((object) this, new Size(592, 368));
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u0005\u0010((object) this), (Control) this.\u0002);
        \u001C\u0005.\u0019\u0010((object) this, \u0012\u0006.\u0001(26781));
        \u001C\u0005.\u007E\u007F\u0010((object) this, \u0012\u0006.\u0001(26806));
        \u001D\u0005.\u007F\u0011((object) this, new EventHandler(this.\u0096\u0006));
        \u001B\u0006.\u007E\u009E\u0011((object) this.\u0002);
        \u001B\u0006.\u007E\u009E\u0011((object) this.\u0003);
        \u009C\u0006.\u0010\u0004((object) this, false);
      }
      catch (Exception ex)
      {
        BaseView[] baseViewArray2 = baseViewArray1;
        GridColumn[] gridColumnArray2 = gridColumnArray1;
        GridColumnSortInfo[] gridColumnSortInfoArray2 = gridColumnSortInfoArray1;
        throw UnhandledException.\u0010\u0003(ex, (object) baseViewArray2, (object) gridColumnArray2, (object) gridColumnSortInfoArray2, (object) this);
      }
    }

    public \u0012\u0006()
    {
      try
      {
        this.\u0081\u0003();
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0008\u0003(ex, (object) this);
      }
    }

    private void \u0096\u0006([In] object obj0, [In] EventArgs obj1)
    {
      try
      {
        \u0080\u0002.\u007E\u0095((object) this.\u0002, (object) \u0096.\u0001.\u0088\u0005);
      }
      catch (Exception ex)
      {
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u000F\u0003(ex, (object) this, obj, (object) eventArgs);
      }
    }

    public void \u0097\u0006([In] int obj0)
    {
      try
      {
        this.\u000F = obj0;
        this.\u0099\u0006(obj0);
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) obj0;
        throw UnhandledException.\u000E\u0003(ex, (object) this, (object) local);
      }
    }

    public int \u0098\u0006()
    {
      try
      {
        return this.\u000F;
      }
      catch (Exception ex)
      {
        int num;
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) num;
        throw UnhandledException.\u000E\u0003(ex, (object) local, (object) this);
      }
    }

    private void \u0099\u0006([In] int obj0)
    {
      GridColumn gridColumn1;
      bool flag;
      int num;
      try
      {
        gridColumn1 = \u0003\u0004.\u007E\u008A((object) \u0099\u0003.\u007E\u0012((object) this.\u0003), \u0012\u0006.\u0001(17921));
        flag = gridColumn1 == null;
        if (flag)
          return;
        num = \u0094\u0006.\u007E\u0011((object) this.\u0003, 0, gridColumn1, obj0.ToString());
        flag = num < 0;
        if (!flag)
        {
          \u0011\u0004.\u007E\u0016((object) this.\u0003, num);
          \u0015\u0006.\u007E\u0017((object) this.\u0003, gridColumn1);
        }
      }
      catch (Exception ex)
      {
        GridColumn gridColumn2 = gridColumn1;
        // ISSUE: variable of a boxed type
        __Boxed<int> local1 = (ValueType) num;
        // ISSUE: variable of a boxed type
        __Boxed<bool> local2 = (ValueType) flag;
        // ISSUE: variable of a boxed type
        __Boxed<int> local3 = (ValueType) obj0;
        throw UnhandledException.\u0011\u0003(ex, (object) gridColumn2, (object) local1, (object) local2, (object) this, (object) local3);
      }
    }

    private void \u009A\u0006([In] object obj0, [In] EventArgs obj1)
    {
      GridView gridView1;
      int num;
      bool flag;
      GridControl gridControl1;
      try
      {
        gridControl1 = obj0 as GridControl;
        gridView1 = \u0012\u0005.\u007E\u0096((object) \u0094\u0003.\u007E\u0094((object) gridControl1), 0) as GridView;
        num = \u0083\u0006.\u007E\u0015((object) gridView1);
        flag = num >= 0;
        if (!flag)
          return;
        this.\u000F = \u0088\u0006.\u008A\u0006(\u0013\u0007.\u007E\u009C\u0004(\u0099\u0005.\u007E\u0018((object) gridView1, num, \u0003\u0004.\u007E\u008A((object) \u0099\u0003.\u007E\u0012((object) gridView1), \u0012\u0006.\u0001(17921)))));
        \u001B\u0006.\u0080\u0011((object) this);
      }
      catch (Exception ex)
      {
        GridControl gridControl2 = gridControl1;
        GridView gridView2 = gridView1;
        // ISSUE: variable of a boxed type
        __Boxed<int> local1 = (ValueType) num;
        // ISSUE: variable of a boxed type
        __Boxed<bool> local2 = (ValueType) flag;
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0013\u0003(ex, (object) gridControl2, (object) gridView2, (object) local1, (object) local2, (object) this, obj, (object) eventArgs);
      }
    }

    static \u0012\u0006()
    {
      \u0006.\u0080\u0002();
    }
  }
}
