﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: NewItemEditor, Version=1.0.0.7, Culture=neutral, PublicKeyToken=9a6515a4c4e1fd2c
// MVID: 9D5F0B9A-C985-40A9-9939-E28C808E997E
// Assembly location: D:\Server\eab\Tools\Net7 Item Editor.exe

using \u0005;
using \u0007;
using \u000E\u0006;
using \u000F\u0006;
using DevExpress.XtraEditors;
using MySql.Data.MySqlClient;
using SmartAssembly.SmartExceptionsCore;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace \u000F\u0006
{
  internal sealed class \u009E : XtraForm
  {
    private IContainer \u0001 = (IContainer) null;
    public bool \u0011 = true;
    [NonSerialized]
    internal static \u0004 \u0001;
    private Label \u0002;
    private Label \u0003;
    public TextBox \u0004;
    public TextBox \u0005;
    private Button \u0006;
    private Button \u0007;
    private Label \u0008;
    public TextBox \u000E;
    public TextBox \u000F;
    private Label \u0010;
    public bool \u0012;

    protected override void Dispose([In] bool obj0)
    {
      bool flag;
      try
      {
        flag = !obj0 || this.\u0001 == null;
        if (!flag)
          \u001B\u0006.\u007E\u000F\u0005((object) this.\u0001);
        \u009C\u0006.\u000E\u0004((object) this, obj0);
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<bool> local1 = (ValueType) flag;
        // ISSUE: variable of a boxed type
        __Boxed<bool> local2 = (ValueType) obj0;
        throw UnhandledException.\u000F\u0003(ex, (object) local1, (object) this, (object) local2);
      }
    }

    private void \u0081\u0003()
    {
      ComponentResourceManager componentResourceManager1;
      try
      {
        // ISSUE: type reference
        componentResourceManager1 = new ComponentResourceManager(\u001F\u0002.\u009A\u0006(__typeref (\u009E)));
        this.\u0002 = new Label();
        this.\u0003 = new Label();
        this.\u0004 = new TextBox();
        this.\u0005 = new TextBox();
        this.\u0006 = new Button();
        this.\u0007 = new Button();
        this.\u0008 = new Label();
        this.\u000E = new TextBox();
        this.\u000F = new TextBox();
        this.\u0010 = new Label();
        \u001B\u0006.\u000F\u0004((object) this);
        \u009C\u0006.\u007E\u009E\u000F((object) this.\u0002, true);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0002, new Point(37, 15));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0002, \u009E.\u0001(8357));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0002, new Size(59, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0002, 0);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0002, \u009E.\u0001(8366));
        \u009C\u0006.\u007E\u009E\u000F((object) this.\u0003, true);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0003, new Point(39, 41));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0003, \u009E.\u0001(8379));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0003, new Size(57, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0003, 1);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0003, \u009E.\u0001(8388));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0004, new Point(101, 12));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0004, \u009E.\u0001(8401));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0004, new Size(142, 21));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0004, 3);
        \u001D\u0005.\u007E\u0080\u0010((object) this.\u0004, new EventHandler(this.\u008B\u0004));
        \u0015\u0005.\u007E\u0088\u0010((object) this.\u0004, new KeyPressEventHandler(this.\u008C\u0004));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0005, new Point(101, 38));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0005, \u009E.\u0001(8422));
        \u0014\u0003.\u007E\u0010\u0011((object) this.\u0005, '*');
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0005, new Size(142, 21));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0005, 4);
        \u001D\u0005.\u007E\u0080\u0010((object) this.\u0005, new EventHandler(this.\u008B\u0004));
        \u0015\u0005.\u007E\u0088\u0010((object) this.\u0005, new KeyPressEventHandler(this.\u008C\u0004));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0006, new Point(18, 123));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0006, \u009E.\u0001(8443));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0006, new Size(77, 28));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0006, 8);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0006, \u009E.\u0001(8456));
        \u009C\u0006.\u007E\u0003\u0011((object) this.\u0006, true);
        \u001D\u0005.\u007E\u0084\u0010((object) this.\u0006, new EventHandler(this.\u008A\u0004));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0007, new Point(189, 123));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0007, \u009E.\u0001(8465));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0007, new Size(77, 28));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0007, 7);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0007, \u009E.\u0001(8482));
        \u009C\u0006.\u007E\u0003\u0011((object) this.\u0007, true);
        \u001D\u0005.\u007E\u0084\u0010((object) this.\u0007, new EventHandler(this.\u0089\u0004));
        \u009C\u0006.\u007E\u009E\u000F((object) this.\u0008, true);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0008, new Point(35, 67));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0008, \u009E.\u0001(8491));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0008, new Size(55, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0008, 2);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0008, \u009E.\u0001(8500));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u000E, new Point(101, 64));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u000E, \u009E.\u0001(8513));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u000E, new Size(142, 21));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u000E, 5);
        \u001D\u0005.\u007E\u0080\u0010((object) this.\u000E, new EventHandler(this.\u008B\u0004));
        \u0015\u0005.\u007E\u0088\u0010((object) this.\u000E, new KeyPressEventHandler(this.\u008C\u0004));
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u000F, new Point(101, 90));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u000F, \u009E.\u0001(8526));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u000F, new Size(61, 21));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u000F, 6);
        \u001D\u0005.\u007E\u0080\u0010((object) this.\u000F, new EventHandler(this.\u008B\u0004));
        \u0015\u0005.\u007E\u0088\u0010((object) this.\u000F, new KeyPressEventHandler(this.\u008C\u0004));
        \u009C\u0006.\u007E\u009E\u000F((object) this.\u0010, true);
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0010, new Point(35, 93));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0010, \u009E.\u0001(8539));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0010, new Size(53, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0010, 8);
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0010, \u009E.\u0001(8548));
        \u008B\u0004.\u009E\u0010((object) this, new SizeF(6f, 13f));
        \u0002\u0004.\u009F\u0010((object) this, AutoScaleMode.Font);
        \u0087\u0006.\u0016\u0011((object) this, new Size(278, 158));
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u0005\u0010((object) this), (Control) this.\u000F);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u0005\u0010((object) this), (Control) this.\u0010);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u0005\u0010((object) this), (Control) this.\u0007);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u0005\u0010((object) this), (Control) this.\u0006);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u0005\u0010((object) this), (Control) this.\u000E);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u0005\u0010((object) this), (Control) this.\u0005);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u0005\u0010((object) this), (Control) this.\u0004);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u0005\u0010((object) this), (Control) this.\u0008);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u0005\u0010((object) this), (Control) this.\u0003);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u0005\u0010((object) this), (Control) this.\u0002);
        \u000E\u0004.\u0014\u0011((object) this, FormBorderStyle.FixedDialog);
        \u0095\u0004.\u0018\u0011((object) this, (Icon) \u008E\u0002.\u007E\u0098\u0008((object) componentResourceManager1, \u009E.\u0001(8561)));
        \u001C\u0005.\u007E\u001A\u0004((object) \u007F\u0004.\u0011\u0004((object) this), \u009E.\u0001(8578));
        \u009C\u0006.\u007E\u0019\u0004((object) \u007F\u0004.\u0011\u0004((object) this), true);
        \u009C\u0006.\u0019\u0011((object) this, false);
        \u009C\u0006.\u001A\u0011((object) this, false);
        \u001C\u0005.\u0019\u0010((object) this, \u009E.\u0001(8482));
        \u008A\u0005.\u001E\u0011((object) this, FormStartPosition.CenterScreen);
        \u001C\u0005.\u007E\u007F\u0010((object) this, \u009E.\u0001(8595));
        \u001D\u0005.\u007F\u0011((object) this, new EventHandler(this.\u008E\u0004));
        \u009C\u0006.\u0010\u0004((object) this, false);
        \u001B\u0006.\u0093\u0010((object) this);
      }
      catch (Exception ex)
      {
        ComponentResourceManager componentResourceManager2 = componentResourceManager1;
        throw UnhandledException.\u000E\u0003(ex, (object) componentResourceManager2, (object) this);
      }
    }

    public \u009E()
    {
      try
      {
        this.\u0081\u0003();
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0008\u0003(ex, (object) this);
      }
    }

    private void \u0089\u0004([In] object obj0, [In] EventArgs obj1)
    {
      MySqlConnection mySqlConnection1;
      try
      {
        try
        {
          this.\u008F\u0004();
          mySqlConnection1 = new MySqlConnection(\u0017\u0002.\u0089\u0005(\u009E.\u0001(8338)));
          \u001B\u0006.\u007E\u0083\u0012((object) mySqlConnection1);
          \u001B\u0006.\u007E\u0081\u0012((object) mySqlConnection1);
          this.\u0011 = false;
          \u001B\u0006.\u0080\u0011((object) this);
        }
        catch (Exception ex)
        {
          int num = (int) \u0014\u0007.\u008D\u0011(\u0006\u0003.\u0083\u0005(\u009E.\u0001(8624), \u0013\u0007.\u007E\u0090\u0005((object) ex)));
        }
      }
      catch (Exception ex)
      {
        MySqlConnection mySqlConnection2 = mySqlConnection1;
        Exception exception = ex;
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0011\u0003(ex, (object) mySqlConnection2, (object) exception, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u008A\u0004([In] object obj0, [In] EventArgs obj1)
    {
      try
      {
        this.\u0011 = true;
        \u001B\u0006.\u0080\u0011((object) this);
      }
      catch (Exception ex)
      {
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u000F\u0003(ex, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u008B\u0004([In] object obj0, [In] EventArgs obj1)
    {
      try
      {
        this.\u0012 = true;
      }
      catch (Exception ex)
      {
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u000F\u0003(ex, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u008C\u0004([In] object obj0, [In] KeyPressEventArgs obj1)
    {
      char ch;
      bool flag;
      MySqlConnection mySqlConnection1;
      try
      {
        ch = \u007F\u0002.\u007E\u0084\u0011((object) obj1);
        flag = !ch.Equals('\r');
        if (flag)
          return;
        try
        {
          this.\u008F\u0004();
          mySqlConnection1 = new MySqlConnection(\u0017\u0002.\u0089\u0005(\u009E.\u0001(8338)));
          \u001B\u0006.\u007E\u0083\u0012((object) mySqlConnection1);
          \u001B\u0006.\u007E\u0081\u0012((object) mySqlConnection1);
          this.\u0011 = false;
          \u001B\u0006.\u0080\u0011((object) this);
        }
        catch (Exception ex)
        {
          int num = (int) \u0014\u0007.\u008D\u0011(\u0006\u0003.\u0083\u0005(\u009E.\u0001(8624), \u0013\u0007.\u007E\u0090\u0005((object) ex)));
        }
      }
      catch (Exception ex)
      {
        MySqlConnection mySqlConnection2 = mySqlConnection1;
        Exception exception = ex;
        // ISSUE: variable of a boxed type
        __Boxed<char> local1 = (ValueType) ch;
        // ISSUE: variable of a boxed type
        __Boxed<bool> local2 = (ValueType) flag;
        object obj = obj0;
        KeyPressEventArgs keyPressEventArgs = obj1;
        throw UnhandledException.\u0013\u0003(ex, (object) mySqlConnection2, (object) exception, (object) local1, (object) local2, (object) this, obj, (object) keyPressEventArgs);
      }
    }

    private void \u008E\u0004([In] object obj0, [In] EventArgs obj1)
    {
      string[] strArray1;
      int num;
      try
      {
        \u001C\u0005 obj = \u001C\u0005.\u007E\u007F\u0010;
        strArray1 = new string[7];
        strArray1[0] = \u0013\u0007.\u007E\u001F\u0010((object) this);
        strArray1[1] = \u009E.\u0001(8689);
        string[] strArray2 = strArray1;
        int index1 = 2;
        num = \u0083\u0006.\u007E\u001F\u0007((object) \u0001\u0004.\u007E\u001E\u0008((object) \u0086\u0004.\u007E\u0006\u0008((object) \u001B\u0003.\u0016\u0008())));
        string str1 = num.ToString();
        strArray2[index1] = str1;
        strArray1[3] = \u009E.\u0001(8694);
        string[] strArray3 = strArray1;
        int index2 = 4;
        num = \u0083\u0006.\u007E\u007F\u0007((object) \u0001\u0004.\u007E\u001E\u0008((object) \u0086\u0004.\u007E\u0006\u0008((object) \u001B\u0003.\u0016\u0008())));
        string str2 = num.ToString();
        strArray3[index2] = str2;
        strArray1[5] = \u009E.\u0001(8699);
        string[] strArray4 = strArray1;
        int index3 = 6;
        num = \u0083\u0006.\u007E\u0080\u0007((object) \u0001\u0004.\u007E\u001E\u0008((object) \u0086\u0004.\u007E\u0006\u0008((object) \u001B\u0003.\u0016\u0008())));
        string str3 = num.ToString();
        strArray4[index3] = str3;
        string str4 = \u0004\u0004.\u0087\u0005(strArray1);
        obj((object) this, str4);
      }
      catch (Exception ex)
      {
        string[] strArray2 = strArray1;
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) num;
        object obj = obj0;
        EventArgs eventArgs = obj1;
        throw UnhandledException.\u0011\u0003(ex, (object) strArray2, (object) local, (object) this, obj, (object) eventArgs);
      }
    }

    private void \u008F\u0004()
    {
      XmlTextWriter xmlTextWriter1;
      int num;
      try
      {
        \u0017\u0002.\u0083\u0005(\u0013\u0007.\u007E\u001F\u0010((object) this.\u0004));
        \u0017\u0002.\u0086\u0005(\u0013\u0007.\u007E\u001F\u0010((object) this.\u0005));
        \u0017\u0002.\u0081\u0005(\u0013\u0007.\u007E\u001F\u0010((object) this.\u000E));
        \u0017\u0002.\u0088\u0005(\u0002\u0006.\u001A\u0006(\u0013\u0007.\u007E\u001F\u0010((object) this.\u000F), 10));
        xmlTextWriter1 = new XmlTextWriter(\u0006\u0003.\u0083\u0005(\u0008\u0004.\u0095\u000F(), \u009E.\u0001(8712)), (Encoding) null);
        \u0015\u0007.\u007E\u0008\u000F((object) xmlTextWriter1, Formatting.Indented);
        \u001B\u0006.\u007E\u0098\u000E((object) xmlTextWriter1);
        \u001C\u0005.\u007E\u009A\u000E((object) xmlTextWriter1, \u009E.\u0001(8729));
        \u001C\u0005.\u007E\u009A\u000E((object) xmlTextWriter1, \u009E.\u0001(8738));
        \u001C\u0005.\u007E\u009D\u000E((object) xmlTextWriter1, \u0013\u0007.\u007E\u009C\u0004((object) \u0017\u0002.\u0080\u0005()));
        \u001B\u0006.\u007E\u009B\u000E((object) xmlTextWriter1);
        \u001C\u0005.\u007E\u009A\u000E((object) xmlTextWriter1, \u009E.\u0001(8747));
        \u001C\u0005 obj = \u001C\u0005.\u007E\u009D\u000E;
        XmlTextWriter xmlTextWriter2 = xmlTextWriter1;
        num = \u0017\u0002.\u0087\u0005();
        string str = num.ToString();
        obj((object) xmlTextWriter2, str);
        \u001B\u0006.\u007E\u009B\u000E((object) xmlTextWriter1);
        \u001C\u0005.\u007E\u009A\u000E((object) xmlTextWriter1, \u009E.\u0001(8756));
        \u001C\u0005.\u007E\u009D\u000E((object) xmlTextWriter1, \u0013\u0007.\u007E\u001F\u0010((object) this.\u0004));
        \u001B\u0006.\u007E\u009B\u000E((object) xmlTextWriter1);
        \u001C\u0005.\u007E\u009A\u000E((object) xmlTextWriter1, \u009E.\u0001(8765));
        \u001C\u0005.\u007E\u009D\u000E((object) xmlTextWriter1, \u0013\u0007.\u007E\u001F\u0010((object) this.\u0005));
        \u001B\u0006.\u007E\u009B\u000E((object) xmlTextWriter1);
        \u001B\u0006.\u007E\u009B\u000E((object) xmlTextWriter1);
        \u001B\u0006.\u007E\u0099\u000E((object) xmlTextWriter1);
        \u001B\u0006.\u007E\u009E\u000E((object) xmlTextWriter1);
      }
      catch (Exception ex)
      {
        XmlTextWriter xmlTextWriter2 = xmlTextWriter1;
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) num;
        throw UnhandledException.\u000F\u0003(ex, (object) xmlTextWriter2, (object) local, (object) this);
      }
    }

    static \u009E()
    {
      \u0006.\u0080\u0002();
    }
  }
}
