﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: NewItemEditor, Version=1.0.0.7, Culture=neutral, PublicKeyToken=9a6515a4c4e1fd2c
// MVID: 9D5F0B9A-C985-40A9-9939-E28C808E997E
// Assembly location: D:\Server\eab\Tools\Net7 Item Editor.exe

using \u0005;
using \u0007;
using \u000F\u0006;
using \u0013\u0006;
using SmartAssembly.SmartExceptionsCore;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace \u000F\u0006
{
  internal sealed class \u0098 : Form
  {
    private IContainer \u0001 = (IContainer) null;
    [NonSerialized]
    internal static \u0004 \u0001;
    private Label \u0002;

    protected override void Dispose([In] bool obj0)
    {
      bool flag;
      try
      {
        flag = !obj0 || this.\u0001 == null;
        if (!flag)
          \u001B\u0006.\u007E\u000F\u0005((object) this.\u0001);
        \u009C\u0006.\u0081\u0011((object) this, obj0);
      }
      catch (Exception ex)
      {
        // ISSUE: variable of a boxed type
        __Boxed<bool> local1 = (ValueType) flag;
        // ISSUE: variable of a boxed type
        __Boxed<bool> local2 = (ValueType) obj0;
        throw UnhandledException.\u000F\u0003(ex, (object) local1, (object) this, (object) local2);
      }
    }

    private void \u0081\u0003()
    {
      try
      {
        this.\u0002 = new Label();
        \u001B\u0006.\u009A\u0010((object) this);
        \u009C\u0006.\u007E\u009E\u000F((object) this.\u0002, true);
        \u0012\u0007.\u007E\u009F\u000F((object) this.\u0002, \u008D\u0004.\u0011\u000F());
        \u0012\u0007.\u007E\u0012\u0010((object) this.\u0002, \u008D\u0004.\u0016\u000F());
        \u001A\u0005.\u007E\u0017\u0010((object) this.\u0002, new Point(13, 254));
        \u001C\u0005.\u007E\u0019\u0010((object) this.\u0002, \u0098.\u0001(8652));
        \u0087\u0006.\u007E\u001C\u0010((object) this.\u0002, new Size(0, 13));
        \u0011\u0004.\u007E\u001D\u0010((object) this.\u0002, 0);
        \u008B\u0004.\u009E\u0010((object) this, new SizeF(6f, 13f));
        \u0002\u0004.\u009F\u0010((object) this, AutoScaleMode.Font);
        \u009F\u0002.\u007E\u0001\u0010((object) this, ImageLayout.None);
        \u0087\u0006.\u0016\u0011((object) this, new Size(453, 279));
        \u009C\u0006.\u0017\u0011((object) this, false);
        \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u0005\u0010((object) this), (Control) this.\u0002);
        \u000E\u0004.\u0014\u0011((object) this, FormBorderStyle.None);
        \u001C\u0005.\u0019\u0010((object) this, \u0098.\u0001(26570));
        \u009C\u0006.\u001C\u0011((object) this, false);
        \u009C\u0006.\u001B\u0011((object) this, false);
        \u008A\u0005.\u001E\u0011((object) this, FormStartPosition.CenterScreen);
        \u001C\u0005.\u007E\u007F\u0010((object) this, \u0098.\u0001(26570));
        \u009C\u0006.\u001F\u0011((object) this, true);
        \u0091\u0003.\u0086\u0010((object) this, new PaintEventHandler(this.\u0087\u0003));
        \u009C\u0006.\u0095\u0010((object) this, false);
        \u001B\u0006.\u0093\u0010((object) this);
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0008\u0003(ex, (object) this);
      }
    }

    public \u0098()
    {
      try
      {
        this.\u0081\u0003();
      }
      catch (Exception ex)
      {
        throw UnhandledException.\u0008\u0003(ex, (object) this);
      }
    }

    public void \u0083\u0003([In] string obj0)
    {
      try
      {
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0002, obj0);
        \u001B\u0006.\u007E\u0094\u0010((object) this);
        this.\u0086\u0003();
      }
      catch (Exception ex)
      {
        string str = obj0;
        throw UnhandledException.\u000E\u0003(ex, (object) this, (object) str);
      }
    }

    private void \u0086\u0003()
    {
    }

    private void \u0087\u0003([In] object obj0, [In] PaintEventArgs obj1)
    {
    }

    protected override void OnPaintBackground([In] PaintEventArgs obj0)
    {
      Graphics graphics1;
      try
      {
        graphics1 = \u000F\u0005.\u007E\u009D\u0010((object) obj0);
        // ISSUE: reference to a compiler-generated method
        \u009F\u0005.\u007E\u001C\u000F((object) graphics1, (Image) \u001A\u0002.\u009C\u0006(), new Rectangle(0, 0, \u0083\u0006.\u0083\u0010((object) this), \u0083\u0006.\u0013\u0010((object) this)));
      }
      catch (Exception ex)
      {
        Graphics graphics2 = graphics1;
        PaintEventArgs paintEventArgs = obj0;
        throw UnhandledException.\u000F\u0003(ex, (object) graphics2, (object) this, (object) paintEventArgs);
      }
    }

    static \u0098()
    {
      \u0006.\u0080\u0002();
    }
  }
}
