﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: NewItemEditor, Version=1.0.0.7, Culture=neutral, PublicKeyToken=9a6515a4c4e1fd2c
// MVID: 9D5F0B9A-C985-40A9-9939-E28C808E997E
// Assembly location: D:\Server\eab\Tools\Net7 Item Editor.exe

using \u0005;
using \u0007;
using \u0010;
using SmartAssembly.SmartExceptionsCore;
using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

namespace \u0010
{
  internal sealed class \u0006
  {
    [NonSerialized]
    internal static \u0004 \u0001;
    private static string \u0001;
    private static string \u0002;
    private static byte[] \u0003;
    private static Hashtable \u0004;
    private static bool \u0005;
    private static int \u0006;

    public static string \u0082\u0002([In] int obj0)
    {
      string str1;
      int num1;
      int num2;
      int num3;
      byte[] numArray1;
      string str2;
      try
      {
        obj0 -= \u0006.\u0006;
        if (\u0006.\u0005)
        {
          str1 = (string) \u001D\u0004.\u007E\u0096\u0007((object) \u0006.\u0004, (object) obj0);
          if (str1 != null)
            return str1;
        }
        num1 = 0;
        num2 = obj0;
        num3 = (int) \u0006.\u0003[num2++];
        if ((num3 & 128) == 0)
        {
          num1 = num3;
          if (num1 == 0)
            return string.Empty;
        }
        else
          num1 = (num3 & 64) != 0 ? ((num3 & 31) << 24) + ((int) \u0006.\u0003[num2++] << 16) + ((int) \u0006.\u0003[num2++] << 8) + (int) \u0006.\u0003[num2++] : ((num3 & 63) << 8) + (int) \u0006.\u0003[num2++];
        string str3;
        try
        {
          numArray1 = \u000F\u0004.\u001C\u0006(\u0081\u0003.\u007E\u0094\u0008((object) \u0003\u0003.\u0095\u0008(), \u0006.\u0003, num2, num1));
          str2 = \u0017\u0006.\u0088\u0005(\u0081\u0003.\u007E\u0094\u0008((object) \u0003\u0003.\u0095\u0008(), numArray1, 0, numArray1.Length));
          if (\u0006.\u0005)
          {
            try
            {
              \u008C\u0005.\u007E\u0093\u0007((object) \u0006.\u0004, (object) obj0, (object) str2);
            }
            catch
            {
            }
          }
          str3 = str2;
        }
        catch
        {
          str3 = (string) null;
        }
        return str3;
      }
      catch (Exception ex)
      {
        string str3 = str1;
        // ISSUE: variable of a boxed type
        __Boxed<int> local1 = (ValueType) num1;
        // ISSUE: variable of a boxed type
        __Boxed<int> local2 = (ValueType) num2;
        // ISSUE: variable of a boxed type
        __Boxed<int> local3 = (ValueType) num3;
        byte[] numArray2 = numArray1;
        string str4 = str2;
        string str5;
        string str6 = str5;
        // ISSUE: variable of a boxed type
        __Boxed<int> local4 = (ValueType) obj0;
        throw UnhandledException.\u0014\u0003(ex, (object) str3, (object) local1, (object) local2, (object) local3, (object) numArray2, (object) str4, (object) str6, (object) local4);
      }
    }

    static \u0006()
    {
      Assembly assembly1;
      Stream stream1;
      int length;
      try
      {
        \u0006.\u0080\u0002();
        \u0006.\u0001 = "0";
        \u0006.\u0002 = "120";
        \u0006.\u0003 = (byte[]) null;
        \u0006.\u0004 = (Hashtable) null;
        \u0006.\u0005 = false;
        \u0006.\u0006 = 0;
        if (\u009E\u0002.\u0011\u0005(\u0006.\u0001, "1"))
        {
          \u0006.\u0005 = true;
          \u0006.\u0004 = new Hashtable();
        }
        \u0006.\u0006 = \u0088\u0006.\u0019\u0006(\u0006.\u0002);
        assembly1 = \u001B\u0003.\u0016\u0008();
        stream1 = \u009B\u0004.\u007E\u000F\u0008((object) assembly1, "{9d5f0b9a-c985-40a9-9939-e28c808e997e}");
        try
        {
          length = \u0087\u0005.\u0016\u0006(\u0019\u0004.\u007E\u0002\u000E((object) stream1));
          \u0006.\u0003 = new byte[length];
          int num = \u0016\u0004.\u007E\u0008\u000E((object) stream1, \u0006.\u0003, 0, length);
          \u001B\u0006.\u007E\u0005\u000E((object) stream1);
        }
        finally
        {
          if (stream1 != null)
            \u001B\u0006.\u007E\u000F\u0005((object) stream1);
        }
      }
      catch (Exception ex)
      {
        Assembly assembly2 = assembly1;
        Stream stream2 = stream1;
        // ISSUE: variable of a boxed type
        __Boxed<int> local = (ValueType) length;
        throw UnhandledException.\u000F\u0003(ex, (object) assembly2, (object) stream2, (object) local);
      }
    }
  }
}
