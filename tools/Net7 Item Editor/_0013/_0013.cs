﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: NewItemEditor, Version=1.0.0.7, Culture=neutral, PublicKeyToken=9a6515a4c4e1fd2c
// MVID: 9D5F0B9A-C985-40A9-9939-E28C808E997E
// Assembly location: D:\Server\eab\Tools\Net7 Item Editor.exe

using \u0005;
using \u0007;
using System;
using System.Drawing;
using System.IO;
using System.Runtime.InteropServices;

namespace \u0013
{
  internal sealed class \u0013
  {
    [NonSerialized]
    internal static \u0004 \u0001;

    public static Bitmap \u0013([In] string obj0)
    {
      try
      {
        Stream stream = \u009B\u0004.\u007E\u000F\u0008((object) \u001B\u0003.\u0016\u0008(), \u008F\u0006.\u0084\u0005(\u0013.\u0013.\u0001(5938), obj0, \u0013.\u0013.\u0001(5999)));
        return stream == null ? (Bitmap) null : new Bitmap(stream);
      }
      catch
      {
        return (Bitmap) null;
      }
    }

    public static Icon \u0013([In] string obj0)
    {
      try
      {
        Stream stream = \u009B\u0004.\u007E\u000F\u0008((object) \u001B\u0003.\u0016\u0008(), \u008F\u0006.\u0084\u0005(\u0013.\u0013.\u0001(5938), obj0, \u0013.\u0013.\u0001(6008)));
        return stream == null ? (Icon) null : new Icon(stream);
      }
      catch
      {
        return (Icon) null;
      }
    }

    static \u0013()
    {
      \u0006.\u0080\u0002();
    }
  }
}
