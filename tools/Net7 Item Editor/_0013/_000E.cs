﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: NewItemEditor, Version=1.0.0.7, Culture=neutral, PublicKeyToken=9a6515a4c4e1fd2c
// MVID: 9D5F0B9A-C985-40A9-9939-E28C808E997E
// Assembly location: D:\Server\eab\Tools\Net7 Item Editor.exe

using \u0005;
using \u0007;
using \u0013;
using \u0080;
using \u0082;
using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;

namespace \u0013
{
  internal sealed class \u000E : Form
  {
    private \u008E \u0002 = new \u008E();
    private Button \u0003 = new Button();
    private Button \u0004 = new Button();
    private \u0086 \u0005 = new \u0086(string.Format(\u000E.\u0001(7543), (object) \u000E.\u0001(6497)));
    private \u0081 \u0006 = new \u0081();
    [NonSerialized]
    internal static \u0004 \u0001;
    private \u0089 \u0001;

    private void \u0013()
    {
      \u001B\u0006.\u009A\u0010((object) this);
      \u0092\u0004.\u007E\u009D\u000F((object) this.\u0004, AnchorStyles.Bottom | AnchorStyles.Right);
      \u001D\u0006.\u007E\u0002\u0011((object) this.\u0004, FlatStyle.System);
      \u0087\u0006.\u007E\u001C\u0010((object) this.\u0004, new Size(100, 24));
      \u001A\u0005.\u007E\u0017\u0010((object) this.\u0004, new Point(408 - \u0083\u0006.\u007E\u0083\u0010((object) this.\u0004), 188));
      \u0011\u0004.\u007E\u001D\u0010((object) this.\u0004, 0);
      \u001C\u0005.\u007E\u007F\u0010((object) this.\u0004, \u000E.\u0001(7521));
      \u001D\u0005.\u007E\u0084\u0010((object) this.\u0004, new EventHandler(this.\u0014));
      \u0092\u0004.\u007E\u009D\u000F((object) this.\u0003, AnchorStyles.Bottom | AnchorStyles.Right);
      \u001D\u0006.\u007E\u0002\u0011((object) this.\u0003, FlatStyle.System);
      \u0087\u0006.\u007E\u001C\u0010((object) this.\u0003, new Size(100, 24));
      \u001A\u0005.\u007E\u0017\u0010((object) this.\u0003, new Point(\u0083\u0006.\u007E\u0015\u0010((object) this.\u0004) - \u0083\u0006.\u007E\u0083\u0010((object) this.\u0003) - 6, 188));
      \u0011\u0004.\u007E\u001D\u0010((object) this.\u0003, 1);
      \u001C\u0005.\u007E\u007F\u0010((object) this.\u0003, \u000E.\u0001(7530));
      \u001D\u0005.\u007E\u0084\u0010((object) this.\u0003, new EventHandler(this.\u0013));
      \u0092\u0004.\u007E\u009D\u000F((object) this.\u0002, AnchorStyles.Bottom | AnchorStyles.Left);
      \u007F\u0003.\u007E\u0097\u0010((object) this.\u0002, 6, 186, 120, 32);
      this.\u0005.\u0091\u0002(\u008F.\u0003);
      \u0092\u0004.\u007E\u009D\u000F((object) this.\u0006, AnchorStyles.Top | AnchorStyles.Left | AnchorStyles.Right);
      \u001A\u0005.\u007E\u0017\u0010((object) this.\u0006, new Point(20, 72));
      \u0087\u0006.\u007E\u001C\u0010((object) this.\u0006, new Size(382, 13));
      \u0087\u0006.\u007E\u0013\u0011((object) this, new Size(5, 13));
      \u0087\u0006.\u0016\u0011((object) this, new Size(418, 224));
      \u009C\u0006.\u0017\u0011((object) this, false);
      \u0096\u0004.\u007E\u009C\u0010((object) \u001A\u0003.\u0005\u0010((object) this), new Control[5]
      {
        (Control) this.\u0002,
        (Control) this.\u0003,
        (Control) this.\u0004,
        (Control) this.\u0005,
        (Control) this.\u0006
      });
      \u000E\u0004.\u0014\u0011((object) this, FormBorderStyle.FixedSingle);
      \u009C\u0006.\u0019\u0011((object) this, false);
      \u009C\u0006.\u001A\u0011((object) this, false);
      \u009C\u0006.\u001B\u0011((object) this, false);
      \u008A\u0005.\u001E\u0011((object) this, FormStartPosition.CenterScreen);
      \u009C\u0006.\u0095\u0010((object) this, false);
    }

    private void \u0013([In] object obj0, [In] EventArgs obj1)
    {
      this.\u0001.\u009A\u0002(true);
      \u001B\u0006.\u0080\u0011((object) this);
    }

    private void \u0014([In] object obj0, [In] EventArgs obj1)
    {
      this.\u0001.\u009A\u0002(false);
      \u001B\u0006.\u0080\u0011((object) this);
    }

    public \u000E([In] \u0089 obj0)
    {
      this.\u0013();
      this.Icon = \u008D.\u0006\u0003();
      \u001C\u0005.\u007E\u007F\u0010((object) this, \u000E.\u0001(6497));
      if (\u0083\u0006.\u007E\u0014\u0005((object) \u0013\u0007.\u007E\u001F\u0010((object) this)) == 0)
        \u001C\u0005.\u007E\u007F\u0010((object) this, \u000E.\u0001(7644));
      this.\u0001 = obj0;
      if (!obj0.\u0098\u0002())
        \u009C\u0006.\u007E\u0082\u0010((object) this.\u0003, false);
      if (\u0083\u0006.\u007E\u0014\u0005((object) obj0.\u0097\u0002()) > 0)
      {
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0006, obj0.\u0097\u0002());
      }
      else
      {
        StringBuilder stringBuilder1 = new StringBuilder();
        StringBuilder stringBuilder2 = \u008A\u0006.\u007E\u008A\u0005((object) stringBuilder1, \u0019\u0006.\u0080\u0005(\u000E.\u0001(7665), (object) \u000E.\u0001(6497)));
        if (obj0.\u0098\u0002())
        {
          StringBuilder stringBuilder3 = \u008A\u0006.\u007E\u008A\u0005((object) stringBuilder1, \u000E.\u0001(7959));
        }
        StringBuilder stringBuilder4 = \u008A\u0006.\u007E\u008A\u0005((object) stringBuilder1, \u0013\u0007.\u007E\u0090\u0005((object) obj0.\u0096\u0002()));
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0006, \u0013\u0007.\u007E\u009C\u0004((object) stringBuilder1));
      }
      int height = \u0083\u0006.\u007E\u0002\u0010((object) this.\u0006) + 60;
      if (height <= \u001C\u0006.\u0015\u0011((object) this).Height)
        return;
      \u0087\u0006.\u0016\u0011((object) this, new Size(\u001C\u0006.\u0015\u0011((object) this).Width, height));
    }

    static \u000E()
    {
      \u0006.\u0080\u0002();
    }
  }
}
