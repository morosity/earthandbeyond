﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: NewItemEditor, Version=1.0.0.7, Culture=neutral, PublicKeyToken=9a6515a4c4e1fd2c
// MVID: 9D5F0B9A-C985-40A9-9939-E28C808E997E
// Assembly location: D:\Server\eab\Tools\Net7 Item Editor.exe

using \u0080;
using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace \u0082
{
  internal sealed class \u0081 : Label
  {
    private void \u0013()
    {
      try
      {
        Graphics graphics = \u000F\u0005.\u008A\u0010((object) this);
        try
        {
          int num = \u008D.\u0013(graphics, \u0013\u0007.\u007E\u001F\u0010((object) this), \u0092\u0002.\u007E\u000F\u0010((object) this), \u0083\u0006.\u0083\u0010((object) this));
          if (num <= 0)
            return;
          \u0011\u0004.\u0014\u0010((object) this, num);
        }
        finally
        {
          if (graphics != null)
            \u001B\u0006.\u007E\u000F\u0005((object) graphics);
        }
      }
      catch
      {
      }
    }

    protected override void OnFontChanged([In] EventArgs obj0)
    {
      \u009D\u0005.\u008A\u0011((object) this, obj0);
      this.\u0013();
    }

    protected override void OnResize([In] EventArgs obj0)
    {
      \u009D\u0005.\u0092\u0010((object) this, obj0);
      this.\u0013();
    }

    protected override void OnTextChanged([In] EventArgs obj0)
    {
      \u009D\u0005.\u008B\u0011((object) this, obj0);
      this.\u0013();
    }

    public \u0081()
    {
      \u001D\u0006.\u0088\u0011((object) this, FlatStyle.System);
      \u009C\u0006.\u0089\u0011((object) this, false);
    }
  }
}
