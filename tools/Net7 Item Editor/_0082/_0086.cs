﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: NewItemEditor, Version=1.0.0.7, Culture=neutral, PublicKeyToken=9a6515a4c4e1fd2c
// MVID: 9D5F0B9A-C985-40A9-9939-E28C808E997E
// Assembly location: D:\Server\eab\Tools\Net7 Item Editor.exe

using \u0005;
using \u0007;
using \u0080;
using \u0082;
using System;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace \u0082
{
  internal sealed class \u0086 : Control
  {
    private Label \u0001 = new Label();
    private float \u0006 = 1f;
    private float \u0007 = 1f;
    [NonSerialized]
    internal static \u0004 \u0001;
    private Image \u0002;
    private Icon \u0003;
    private Bitmap \u0004;
    private \u008F \u0005;

    [SpecialName]
    public void \u0091\u0002([In] \u008F obj0)
    {
      if (this.\u0005 == obj0)
        return;
      this.\u0005 = obj0;
      switch (this.\u0005)
      {
        case \u008F.\u0002:
          this.\u0004 = \u0013.\u0013.\u0013(\u0086.\u0001(5130));
          break;
        case \u008F.\u0003:
          this.\u0004 = \u0013.\u0013.\u0013(\u0086.\u0001(5143));
          break;
        default:
          this.\u0004 = (Bitmap) null;
          break;
      }
      \u001B\u0006.\u007E\u0094\u0010((object) this);
    }

    [SpecialName]
    public override string get_Text()
    {
      return \u0013\u0007.\u007E\u001F\u0010((object) this.\u0001);
    }

    [SpecialName]
    public override void set_Text([In] string obj0)
    {
      \u001C\u0005.\u007E\u007F\u0010((object) this.\u0001, obj0);
    }

    protected override void Dispose([In] bool obj0)
    {
      if (obj0)
        goto label_7;
label_6:
      \u009C\u0006.\u008B\u0010((object) this, obj0);
      return;
label_7:
      if (this.\u0003 != null)
      {
        \u001B\u0006.\u007E\u0080\u000F((object) this.\u0003);
        this.\u0003 = (Icon) null;
      }
      if (this.\u0002 != null)
      {
        \u001B\u0006.\u007E\u000F\u000F((object) this.\u0002);
        this.\u0002 = (Image) null;
      }
      if (this.\u0004 != null)
      {
        \u001B\u0006.\u007E\u000F\u000F((object) this.\u0004);
        this.\u0004 = (Bitmap) null;
        goto label_6;
      }
      else
        goto label_6;
    }

    protected override void OnResize([In] EventArgs obj0)
    {
      \u007F\u0003.\u007E\u0097\u0010((object) this.\u0001, \u0089\u0006.\u0017\u0006(13f * this.\u0006), \u0089\u0006.\u0017\u0006(15f * this.\u0007), \u0083\u0006.\u0083\u0010((object) this) - \u0089\u0006.\u0017\u0006(69f * this.\u0006), \u0083\u0006.\u0013\u0010((object) this) - \u0089\u0006.\u0017\u0006(18f * this.\u0007));
      \u009D\u0005.\u0092\u0010((object) this, obj0);
    }

    protected override void ScaleCore([In] float obj0, [In] float obj1)
    {
      this.\u0006 = obj0;
      this.\u0007 = obj1;
      \u0080\u0004.\u0096\u0010((object) this, obj0, obj1);
      \u009D\u0005.\u007E\u0092\u0010((object) this, EventArgs.Empty);
    }

    protected override void OnPaint([In] PaintEventArgs obj0)
    {
      \u0097\u0006.\u0091\u0010((object) this, obj0);
      \u0010\u0005.\u007E\u0019\u000F((object) \u000F\u0005.\u007E\u009D\u0010((object) obj0), \u000F\u0007.\u008E\u000F(), 0, \u001C\u0006.\u0004\u0010((object) this).Height - 2, \u001C\u0006.\u0004\u0010((object) this).Width, \u001C\u0006.\u0004\u0010((object) this).Height - 2);
      \u0010\u0005.\u007E\u0019\u000F((object) \u000F\u0005.\u007E\u009D\u0010((object) obj0), \u000F\u0007.\u008F\u000F(), 0, \u001C\u0006.\u0004\u0010((object) this).Height - 1, \u001C\u0006.\u0004\u0010((object) this).Width, \u001C\u0006.\u0004\u0010((object) this).Height - 1);
      Rectangle rectangle = new Rectangle(\u001C\u0006.\u0004\u0010((object) this).Width - \u0089\u0006.\u0017\u0006(48f * this.\u0006), \u0089\u0006.\u0017\u0006(11f * this.\u0007), \u0089\u0006.\u0017\u0006(32f * this.\u0006), \u0089\u0006.\u0017\u0006(32f * this.\u0007));
      if (this.\u0002 != null)
      {
        \u0087\u0002.\u007E\u001D\u000F((object) \u000F\u0005.\u007E\u009D\u0010((object) obj0), this.\u0002, rectangle, new Rectangle(0, 0, 32, 32), GraphicsUnit.Pixel);
      }
      else
      {
        if (this.\u0003 == null)
          return;
        \u008F\u0004.\u007E\u001B\u000F((object) \u000F\u0005.\u007E\u009D\u0010((object) obj0), this.\u0003, rectangle);
        if (this.\u0004 == null)
          return;
        \u0087\u0002.\u007E\u001D\u000F((object) \u000F\u0005.\u007E\u009D\u0010((object) obj0), (Image) this.\u0004, new Rectangle(rectangle.Right - \u0089\u0006.\u0017\u0006(12f * this.\u0006), rectangle.Bottom - \u0089\u0006.\u0017\u0006(12f * this.\u0007), \u0089\u0006.\u0017\u0006(16f * this.\u0006), \u0089\u0006.\u0017\u0006(16f * this.\u0007)), new Rectangle(0, 0, 16, 16), GraphicsUnit.Pixel);
      }
    }

    protected override void OnFontChanged([In] EventArgs obj0)
    {
      try
      {
        \u0014\u0004.\u007E\u0010\u0010((object) this.\u0001, new Font(\u0092\u0002.\u007E\u000F\u0010((object) this), FontStyle.Bold));
        \u009D\u0005.\u008F\u0010((object) this, obj0);
      }
      catch
      {
      }
    }

    public \u0086()
    {
      try
      {
        \u001D\u0006.\u007E\u0088\u0011((object) this.\u0001, FlatStyle.System);
        \u0014\u0004.\u007E\u0010\u0010((object) this.\u0001, new Font(\u0092\u0002.\u007E\u000F\u0010((object) this), FontStyle.Bold));
      }
      catch
      {
      }
      \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u0005\u0010((object) this), (Control) this.\u0001);
      \u0012\u0007.\u007E\u009F\u000F((object) this, \u008D\u0004.\u008D\u000F());
      \u009C\u0006.\u001E\u0010((object) this, false);
      \u0012\u0004.\u007E\u0008\u0010((object) this, DockStyle.Top);
      \u0011\u0004.\u0014\u0010((object) this, 58);
      \u009E\u0004.\u0098\u0010((object) this, ControlStyles.UserPaint | ControlStyles.ResizeRedraw | ControlStyles.SupportsTransparentBackColor | ControlStyles.AllPaintingInWmPaint | ControlStyles.DoubleBuffer, true);
      this.\u0003 = \u008D.\u0006\u0003();
      \u009D\u0005.\u007E\u0092\u0010((object) this, EventArgs.Empty);
    }

    public \u0086([In] string obj0)
      : this()
    {
      \u001C\u0005.\u007E\u007F\u0010((object) this.\u0001, obj0);
    }

    static \u0086()
    {
      \u0006.\u0080\u0002();
    }
  }
}
