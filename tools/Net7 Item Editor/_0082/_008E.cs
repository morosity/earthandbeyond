﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: NewItemEditor, Version=1.0.0.7, Culture=neutral, PublicKeyToken=9a6515a4c4e1fd2c
// MVID: 9D5F0B9A-C985-40A9-9939-E28C808E997E
// Assembly location: D:\Server\eab\Tools\Net7 Item Editor.exe

using \u0005;
using \u0007;
using \u0082;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace \u0082
{
  internal sealed class \u008E : Control
  {
    private Label \u0001 = new Label();
    private PictureBox \u0002 = new PictureBox();
    private ToolTip \u0003 = new ToolTip();
    private float \u0004 = 1f;
    private float \u0005 = 1f;
    [NonSerialized]
    internal static \u0004 \u0001;

    protected override void OnResize([In] EventArgs obj0)
    {
      \u0087\u0006.\u001C\u0010((object) this, new Size(\u0089\u0006.\u0017\u0006(120f * this.\u0004), \u0089\u0006.\u0017\u0006(32f * this.\u0005)));
      \u009D\u0005.\u0092\u0010((object) this, obj0);
    }

    protected override void ScaleCore([In] float obj0, [In] float obj1)
    {
      this.\u0004 = obj0;
      this.\u0005 = obj1;
      \u0080\u0004.\u0096\u0010((object) this, obj0, obj1);
      \u009D\u0005.\u007E\u0092\u0010((object) this, EventArgs.Empty);
    }

    protected override void Dispose([In] bool obj0)
    {
      if (obj0)
        goto label_5;
label_4:
      \u009C\u0006.\u008B\u0010((object) this, obj0);
      return;
label_5:
      if (this.\u0003 != null)
        \u001B\u0006.\u007E\u0099\u0011((object) this.\u0003);
      if (this.\u0002 != null)
      {
        \u001B\u0006.\u007E\u0099\u0011((object) this.\u0002);
        goto label_4;
      }
      else
        goto label_4;
    }

    private void \u0013([In] object obj0, [In] EventArgs obj1)
    {
      try
      {
        this.\u0013();
      }
      catch
      {
      }
    }

    private void \u0013()
    {
      Process process = \u0094\u0002.\u0008\u0012(\u008E.\u0001(5480));
    }

    public \u008E()
    {
      \u001B\u0006.\u009A\u0010((object) this);
      \u001D\u0006.\u007E\u0088\u0011((object) this.\u0001, FlatStyle.System);
      \u001A\u0005.\u007E\u0017\u0010((object) this.\u0001, new Point(10, 10));
      \u0087\u0006.\u007E\u001C\u0010((object) this.\u0001, new Size(62, 24));
      \u001C\u0005.\u007E\u007F\u0010((object) this.\u0001, \u008E.\u0001(5549));
      \u0097\u0004.\u007E\u0090\u0011((object) this.\u0002, (Image) \u0013.\u0013.\u0013(\u008E.\u0001(5566)));
      \u001A\u0005.\u007E\u0017\u0010((object) this.\u0002, new Point(72, 0));
      \u0087\u0006.\u007E\u001C\u0010((object) this.\u0002, new Size(48, 32));
      \u0094\u0005.\u007E\u0091\u0011((object) this.\u0002, PictureBoxSizeMode.StretchImage);
      \u001D\u0005.\u007E\u0084\u0010((object) this.\u0001, new EventHandler(this.\u0013));
      \u001D\u0005.\u007E\u0084\u0010((object) this.\u0002, new EventHandler(this.\u0013));
      \u001D\u0005.\u0084\u0010((object) this, new EventHandler(this.\u0013));
      \u009C\u0005.\u007E\u0006\u0010((object) this, \u008E\u0006.\u000E\u0011());
      \u009C\u0006.\u001E\u0010((object) this, false);
      \u0087\u0006.\u001C\u0010((object) this, new Size(120, 32));
      \u0096\u0004.\u007E\u009C\u0010((object) \u001A\u0003.\u0005\u0010((object) this), new Control[2]
      {
        (Control) this.\u0002,
        (Control) this.\u0001
      });
      string str = \u008E.\u0001(5575);
      \u0091\u0005.\u007E\u0098\u0011((object) this.\u0003, (Control) this, str);
      \u0091\u0005.\u007E\u0098\u0011((object) this.\u0003, (Control) this.\u0001, str);
      \u0091\u0005.\u007E\u0098\u0011((object) this.\u0003, (Control) this.\u0002, str);
      \u009C\u0006.\u0095\u0010((object) this, true);
    }

    static \u008E()
    {
      \u0006.\u0080\u0002();
    }
  }
}
