﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: NewItemEditor, Version=1.0.0.7, Culture=neutral, PublicKeyToken=9a6515a4c4e1fd2c
// MVID: 9D5F0B9A-C985-40A9-9939-E28C808E997E
// Assembly location: D:\Server\eab\Tools\Net7 Item Editor.exe

using \u0005;
using \u0007;
using \u0082;
using System;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace \u0082
{
  internal sealed class \u008B : Control
  {
    private Label \u0001 = new Label();
    private Timer \u0004 = new Timer();
    private bool \u0005 = true;
    private string \u0006 = string.Empty;
    private float \u0007 = 1f;
    private float \u0008 = 1f;
    [NonSerialized]
    internal static \u0004 \u0001;
    private Image \u0002;
    private bool \u0003;

    [SpecialName]
    public override string get_Text()
    {
      return \u0013\u0007.\u001F\u0010((object) this);
    }

    [SpecialName]
    public override void set_Text([In] string obj0)
    {
      \u001C\u0005.\u007F\u0010((object) this, obj0);
      \u001B\u0006.\u007E\u0094\u0010((object) this);
    }

    public void \u009D\u0002()
    {
      \u009C\u0006.\u007E\u0094\u0011((object) this.\u0004, false);
      this.\u0002 = (Image) null;
      this.\u0003 = false;
      this.\u0006 = string.Empty;
      \u001B\u0006.\u007E\u0094\u0010((object) this);
      \u0011\u0004.\u0014\u0010((object) this, 16);
    }

    public void \u009E\u0002()
    {
      \u009C\u0006.\u007E\u0094\u0011((object) this.\u0004, true);
      this.\u0002 = (Image) \u0013.\u0013.\u0013(\u008B.\u0001(5399));
      this.\u0003 = true;
      \u001B\u0006.\u007E\u0094\u0010((object) this);
    }

    public void \u009F\u0002()
    {
      this.\u009F\u0002(string.Empty);
    }

    public void \u009F\u0002([In] string obj0)
    {
      this.\u0006 = obj0;
      \u009C\u0006.\u007E\u0094\u0011((object) this.\u0004, false);
      this.\u0002 = (Image) \u0013.\u0013.\u0013(\u0083\u0006.\u007E\u0014\u0005((object) obj0) > 0 ? \u008B.\u0001(5417) : \u008B.\u0001(5412));
      this.\u0005 = true;
      this.\u0003 = true;
      if (\u0083\u0006.\u007E\u0014\u0005((object) obj0) > 0)
        \u0011\u0004.\u0014\u0010((object) this, 100);
      \u001B\u0006.\u007E\u0094\u0010((object) this);
    }

    protected override void OnResize([In] EventArgs obj0)
    {
      \u007F\u0003.\u007E\u0097\u0010((object) this.\u0001, \u0089\u0006.\u0017\u0006(22f * this.\u0007), \u0089\u0006.\u0017\u0006(this.\u0008), \u0083\u0006.\u0083\u0010((object) this) - \u0089\u0006.\u0017\u0006(22f * this.\u0007), \u0083\u0006.\u0013\u0010((object) this) - \u0089\u0006.\u0017\u0006(this.\u0008));
      \u009D\u0005.\u0092\u0010((object) this, obj0);
    }

    protected override void ScaleCore([In] float obj0, [In] float obj1)
    {
      this.\u0007 = obj0;
      this.\u0008 = obj1;
      \u0080\u0004.\u0096\u0010((object) this, obj0, obj1);
      \u009D\u0005.\u007E\u0092\u0010((object) this, EventArgs.Empty);
    }

    protected override void OnPaint([In] PaintEventArgs obj0)
    {
      \u0097\u0006.\u0091\u0010((object) this, obj0);
      if (\u0006\u0007.\u009A\u0011((object) this))
      {
        this.\u0002 = (Image) \u0013.\u0013.\u0013(\u008B.\u0001(5399));
        this.\u0003 = true;
      }
      if (this.\u0002 != null && this.\u0005)
        \u0087\u0002.\u007E\u001D\u000F((object) \u000F\u0005.\u007E\u009D\u0010((object) obj0), this.\u0002, new Rectangle(0, 0, \u0089\u0006.\u0017\u0006(16f * this.\u0007), \u0089\u0006.\u0017\u0006(16f * this.\u0008)), new Rectangle(0, 0, 16, 16), GraphicsUnit.Pixel);
      if (this.\u0003)
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0001, \u0083\u0006.\u007E\u0014\u0005((object) this.\u0006) > 0 ? \u0082\u0005.\u0086\u0005(\u0013\u0007.\u001F\u0010((object) this), \u008B.\u0001(5426), this.\u0006, \u008B.\u0001(5431)) : \u0013\u0007.\u001F\u0010((object) this));
      else
        \u001C\u0005.\u007E\u007F\u0010((object) this.\u0001, string.Empty);
    }

    public \u008B()
    {
      \u0011\u0004.\u007E\u0095\u0011((object) this.\u0004, 250);
      \u001D\u0005.\u007E\u0093\u0011((object) this.\u0004, new EventHandler(this.\u0013));
      \u001D\u0006.\u007E\u0088\u0011((object) this.\u0001, FlatStyle.System);
      \u0007\u0004.\u007E\u009B\u0010((object) \u001A\u0003.\u0005\u0010((object) this), (Control) this.\u0001);
      \u009E\u0004.\u0098\u0010((object) this, ControlStyles.UserPaint | ControlStyles.ResizeRedraw | ControlStyles.SupportsTransparentBackColor | ControlStyles.AllPaintingInWmPaint | ControlStyles.DoubleBuffer, true);
      \u009C\u0006.\u001E\u0010((object) this, false);
    }

    public \u008B([In] string obj0)
      : this()
    {
      \u001C\u0005.\u007F\u0010((object) this, \u0006\u0003.\u0083\u0005(\u008B.\u0001(5436), obj0));
    }

    protected override void Dispose([In] bool obj0)
    {
      if (obj0 && this.\u0002 != null)
        \u001B\u0006.\u007E\u000F\u000F((object) this.\u0002);
      \u009C\u0006.\u008B\u0010((object) this, obj0);
    }

    private void \u0013([In] object obj0, [In] EventArgs obj1)
    {
      this.\u0005 = !this.\u0005;
      \u001B\u0006.\u007E\u0094\u0010((object) this);
    }

    static \u008B()
    {
      \u0006.\u0080\u0002();
    }
  }
}
