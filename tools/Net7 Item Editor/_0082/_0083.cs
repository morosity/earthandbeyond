﻿// Decompiled with JetBrains decompiler
// Type: .
// Assembly: NewItemEditor, Version=1.0.0.7, Culture=neutral, PublicKeyToken=9a6515a4c4e1fd2c
// MVID: 9D5F0B9A-C985-40A9-9939-E28C808E997E
// Assembly location: D:\Server\eab\Tools\Net7 Item Editor.exe

using \u0005;
using \u0007;
using \u0082;
using System;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace \u0082
{
  internal sealed class \u0083 : Control
  {
    private int \u0001 = 99;
    private Bitmap \u0002 = \u0013.\u0013.\u0013(\u0083.\u0001(2823));
    private Bitmap \u0003 = \u0013.\u0013.\u0013(\u0083.\u0001(2832));
    private Timer \u0004 = new Timer();
    private float \u0005 = 1f;
    private float \u0006 = 1f;
    [NonSerialized]
    internal static \u0004 \u0001;

    protected override void OnVisibleChanged([In] EventArgs obj0)
    {
      \u009D\u0005.\u0090\u0010((object) this, obj0);
      if (\u0006\u0007.\u009A\u0011((object) this))
        return;
      this.\u0013(\u0006\u0007.\u0081\u0010((object) this));
    }

    private void \u0013([In] bool obj0)
    {
      \u009C\u0006.\u007E\u0094\u0011((object) this.\u0004, obj0);
      this.\u0001 = 0;
      \u001B\u0006.\u007E\u0094\u0010((object) this);
    }

    protected override void OnResize([In] EventArgs obj0)
    {
      \u0087\u0006.\u001C\u0010((object) this, new Size(\u0089\u0006.\u0017\u0006(250f * this.\u0005), \u0089\u0006.\u0017\u0006(42f * this.\u0006)));
      \u009D\u0005.\u0092\u0010((object) this, obj0);
    }

    protected override void ScaleCore([In] float obj0, [In] float obj1)
    {
      this.\u0005 = obj0;
      this.\u0006 = obj1;
      \u0080\u0004.\u0096\u0010((object) this, obj0, obj1);
      \u009D\u0005.\u007E\u0092\u0010((object) this, EventArgs.Empty);
    }

    protected override void Dispose([In] bool obj0)
    {
      if (obj0 && this.\u0002 != null)
        \u001B\u0006.\u007E\u000F\u000F((object) this.\u0002);
      \u009C\u0006.\u008B\u0010((object) this, obj0);
    }

    protected override void OnPaint([In] PaintEventArgs obj0)
    {
      \u0097\u0006.\u0091\u0010((object) this, obj0);
      if (this.\u0003 != null)
        \u0087\u0002.\u007E\u001D\u000F((object) \u000F\u0005.\u007E\u009D\u0010((object) obj0), (Image) this.\u0003, new Rectangle(0, 0, \u0089\u0006.\u0017\u0006(250f * this.\u0005), \u0089\u0006.\u0017\u0006(42f * this.\u0006)), new Rectangle(0, 0, 250, 42), GraphicsUnit.Pixel);
      if (this.\u0002 == null || this.\u0001 <= 0)
        return;
      \u001E\u0002.\u007E\u001F\u000F((object) \u000F\u0005.\u007E\u009D\u0010((object) obj0), new Rectangle(\u0089\u0006.\u0017\u0006(46f * this.\u0005), 0, \u0089\u0006.\u0017\u0006(165f * this.\u0005), \u0089\u0006.\u0017\u0006(34f * this.\u0006)));
      \u001F\u0004.\u007E\u001E\u000F((object) \u000F\u0005.\u007E\u009D\u0010((object) obj0), (Image) this.\u0002, new Rectangle(\u0089\u0006.\u0017\u0006((float) (this.\u0001 - 6) * this.\u0005), \u0089\u0006.\u0017\u0006(16f * this.\u0006), \u0089\u0006.\u0017\u0006(40f * this.\u0005), \u0089\u0006.\u0017\u0006(12f * this.\u0006)), 0, 0, 40, 12, GraphicsUnit.Pixel);
    }

    private void \u0013([In] object obj0, [In] EventArgs obj1)
    {
      this.\u0001 += 11;
      if (this.\u0001 > 198)
        goto label_2;
label_1:
      \u001B\u0006.\u007E\u0094\u0010((object) this);
      return;
label_2:
      this.\u0001 = 0;
      goto label_1;
    }

    public \u0083()
    {
      this.\u0004.Interval = 85;
      \u001D\u0005.\u007E\u0093\u0011((object) this.\u0004, new EventHandler(this.\u0013));
      \u0087\u0006.\u001C\u0010((object) this, new Size(250, 42));
      \u009C\u0006.\u001E\u0010((object) this, false);
      \u009E\u0004.\u0098\u0010((object) this, ControlStyles.UserPaint | ControlStyles.ResizeRedraw | ControlStyles.SupportsTransparentBackColor | ControlStyles.AllPaintingInWmPaint | ControlStyles.DoubleBuffer, true);
    }

    static \u0083()
    {
      \u0006.\u0080\u0002();
    }
  }
}
