﻿// Decompiled with JetBrains decompiler
// Type: SQLBind.Form1
// Assembly: SQLBind, Version=1.0.0.1, Culture=neutral, PublicKeyToken=null
// MVID: 8DC05ED6-E010-4553-A0DD-607EBD42D93A
// Assembly location: D:\Server\eab\Tools\Effect Editor.exe

using MySql.Data.MySqlClient;
using SQLBind.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace SQLBind
{
  public class Form1 : Form
  {
    private IContainer components;
    private ComboBox EffectType;
    private Label label1;
    private TextBox EffectName;
    private TextBox EffectDesc;
    private TextBox EffectToolTip;
    private Label label2;
    private Label label3;
    private Label label4;
    private GroupBox TargetTypeGrp;
    private RadioButton TGroupM;
    private RadioButton TEnemy;
    private RadioButton TFriend;
    private CheckBox RequireT;
    private Button button1;
    private GroupBox groupBox2;
    private ComboBox VarType3;
    private ComboBox VarStat3;
    private Label label7;
    private ComboBox VarType2;
    private ComboBox VarStat2;
    private Label label6;
    private ComboBox VarType1;
    private ComboBox VarStat1;
    private Label label5;
    private GroupBox groupBox1;
    private Label label10;
    private ComboBox ConstStat1;
    private TextBox ConstValue2;
    private Label label8;
    private ComboBox ConstType2;
    private ComboBox ConstType1;
    private ComboBox ConstStat2;
    private TextBox ConstValue1;
    private Label label11;
    private Label label9;
    private Button Save;
    private ComboBox EffectBuff;
    private Label label12;
    private Label label13;
    private Label label14;
    private Button NewEffect;
    private Label label16;
    private Label label15;
    private Button button2;
    private RadioButton TSelf;
    private Label label18;
    private TextBox VisualEffect;
    private MySqlConnection m_conn;
    private int m_EffectID;
    private EditItem ItemEditor;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.EffectType = new ComboBox();
      this.label1 = new Label();
      this.EffectName = new TextBox();
      this.EffectDesc = new TextBox();
      this.EffectToolTip = new TextBox();
      this.label2 = new Label();
      this.label3 = new Label();
      this.label4 = new Label();
      this.TargetTypeGrp = new GroupBox();
      this.VisualEffect = new TextBox();
      this.label18 = new Label();
      this.TSelf = new RadioButton();
      this.EffectBuff = new ComboBox();
      this.label12 = new Label();
      this.RequireT = new CheckBox();
      this.TGroupM = new RadioButton();
      this.TEnemy = new RadioButton();
      this.TFriend = new RadioButton();
      this.button1 = new Button();
      this.groupBox2 = new GroupBox();
      this.VarType3 = new ComboBox();
      this.VarStat3 = new ComboBox();
      this.label7 = new Label();
      this.VarType2 = new ComboBox();
      this.VarStat2 = new ComboBox();
      this.label6 = new Label();
      this.VarType1 = new ComboBox();
      this.VarStat1 = new ComboBox();
      this.label5 = new Label();
      this.groupBox1 = new GroupBox();
      this.label16 = new Label();
      this.label15 = new Label();
      this.label10 = new Label();
      this.ConstStat1 = new ComboBox();
      this.ConstValue2 = new TextBox();
      this.label8 = new Label();
      this.ConstType2 = new ComboBox();
      this.ConstType1 = new ComboBox();
      this.ConstStat2 = new ComboBox();
      this.ConstValue1 = new TextBox();
      this.label11 = new Label();
      this.label9 = new Label();
      this.Save = new Button();
      this.label13 = new Label();
      this.label14 = new Label();
      this.NewEffect = new Button();
      this.button2 = new Button();
      this.TargetTypeGrp.SuspendLayout();
      this.groupBox2.SuspendLayout();
      this.groupBox1.SuspendLayout();
      this.SuspendLayout();
      this.EffectType.DropDownStyle = ComboBoxStyle.DropDownList;
      this.EffectType.FormattingEnabled = true;
      this.EffectType.Items.AddRange(new object[2]
      {
        (object) "Equipable (0)",
        (object) "Activatable (1)"
      });
      this.EffectType.Location = new Point(85, 40);
      this.EffectType.Name = "EffectType";
      this.EffectType.Size = new Size(123, 21);
      this.EffectType.TabIndex = 0;
      this.EffectType.SelectedIndexChanged += new EventHandler(this.EffectType_SelectedIndexChanged);
      this.label1.AutoSize = true;
      this.label1.Location = new Point(14, 43);
      this.label1.Name = "label1";
      this.label1.Size = new Size(65, 13);
      this.label1.TabIndex = 1;
      this.label1.Text = "Effect Type:";
      this.EffectName.Location = new Point(85, 67);
      this.EffectName.Name = "EffectName";
      this.EffectName.Size = new Size(185, 20);
      this.EffectName.TabIndex = 2;
      this.EffectDesc.Location = new Point(85, 93);
      this.EffectDesc.Name = "EffectDesc";
      this.EffectDesc.Size = new Size(185, 20);
      this.EffectDesc.TabIndex = 3;
      this.EffectToolTip.Location = new Point(85, 119);
      this.EffectToolTip.Name = "EffectToolTip";
      this.EffectToolTip.Size = new Size(573, 20);
      this.EffectToolTip.TabIndex = 4;
      this.label2.AutoSize = true;
      this.label2.Location = new Point(41, 70);
      this.label2.Name = "label2";
      this.label2.Size = new Size(38, 13);
      this.label2.TabIndex = 5;
      this.label2.Text = "Name:";
      this.label3.AutoSize = true;
      this.label3.Location = new Point(16, 96);
      this.label3.Name = "label3";
      this.label3.Size = new Size(63, 13);
      this.label3.TabIndex = 6;
      this.label3.Text = "Description:";
      this.label4.AutoSize = true;
      this.label4.Location = new Point(37, 122);
      this.label4.Name = "label4";
      this.label4.Size = new Size(42, 13);
      this.label4.TabIndex = 7;
      this.label4.Text = "Tooltip:";
      this.TargetTypeGrp.Controls.Add((Control) this.VisualEffect);
      this.TargetTypeGrp.Controls.Add((Control) this.label18);
      this.TargetTypeGrp.Controls.Add((Control) this.TSelf);
      this.TargetTypeGrp.Controls.Add((Control) this.EffectBuff);
      this.TargetTypeGrp.Controls.Add((Control) this.label12);
      this.TargetTypeGrp.Controls.Add((Control) this.RequireT);
      this.TargetTypeGrp.Controls.Add((Control) this.TGroupM);
      this.TargetTypeGrp.Controls.Add((Control) this.TEnemy);
      this.TargetTypeGrp.Controls.Add((Control) this.TFriend);
      this.TargetTypeGrp.Location = new Point(455, 145);
      this.TargetTypeGrp.Name = "TargetTypeGrp";
      this.TargetTypeGrp.Size = new Size(273, 200);
      this.TargetTypeGrp.TabIndex = 8;
      this.TargetTypeGrp.TabStop = false;
      this.TargetTypeGrp.Text = "Activatable";
      this.VisualEffect.Location = new Point(80, 165);
      this.VisualEffect.Name = "VisualEffect";
      this.VisualEffect.Size = new Size(171, 20);
      this.VisualEffect.TabIndex = 33;
      this.label18.AutoSize = true;
      this.label18.Location = new Point(8, 168);
      this.label18.Name = "label18";
      this.label18.Size = new Size(66, 13);
      this.label18.TabIndex = 32;
      this.label18.Text = "Visual Effect";
      this.TSelf.AutoSize = true;
      this.TSelf.Location = new Point(20, 92);
      this.TSelf.Name = "TSelf";
      this.TSelf.Size = new Size(77, 17);
      this.TSelf.TabIndex = 30;
      this.TSelf.Text = "Target Self";
      this.TSelf.UseVisualStyleBackColor = true;
      this.EffectBuff.DropDownStyle = ComboBoxStyle.DropDownList;
      this.EffectBuff.FormattingEnabled = true;
      this.EffectBuff.Location = new Point(80, 138);
      this.EffectBuff.Name = "EffectBuff";
      this.EffectBuff.Size = new Size(171, 21);
      this.EffectBuff.TabIndex = 29;
      this.label12.AutoSize = true;
      this.label12.Location = new Point(45, 141);
      this.label12.Name = "label12";
      this.label12.Size = new Size(29, 13);
      this.label12.TabIndex = 28;
      this.label12.Text = "Buff:";
      this.RequireT.AutoSize = true;
      this.RequireT.Location = new Point(18, 115);
      this.RequireT.Name = "RequireT";
      this.RequireT.Size = new Size(97, 17);
      this.RequireT.TabIndex = 9;
      this.RequireT.Text = "Require Target";
      this.RequireT.UseVisualStyleBackColor = true;
      this.TGroupM.AutoSize = true;
      this.TGroupM.Location = new Point(20, 69);
      this.TGroupM.Name = "TGroupM";
      this.TGroupM.Size = new Size(129, 17);
      this.TGroupM.TabIndex = 9;
      this.TGroupM.Text = "Target Group Member";
      this.TGroupM.UseVisualStyleBackColor = true;
      this.TEnemy.AutoSize = true;
      this.TEnemy.Location = new Point(20, 46);
      this.TEnemy.Name = "TEnemy";
      this.TEnemy.Size = new Size(91, 17);
      this.TEnemy.TabIndex = 9;
      this.TEnemy.TabStop = true;
      this.TEnemy.Text = "Target Enemy";
      this.TEnemy.UseVisualStyleBackColor = true;
      this.TFriend.AutoSize = true;
      this.TFriend.Checked = true;
      this.TFriend.Location = new Point(20, 23);
      this.TFriend.Name = "TFriend";
      this.TFriend.Size = new Size(95, 17);
      this.TFriend.TabIndex = 0;
      this.TFriend.TabStop = true;
      this.TFriend.Text = "Target Friendly";
      this.TFriend.UseVisualStyleBackColor = true;
      this.button1.Location = new Point(85, 12);
      this.button1.Name = "button1";
      this.button1.Size = new Size(61, 22);
      this.button1.TabIndex = 23;
      this.button1.Text = "Search";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new EventHandler(this.button1_Click);
      this.groupBox2.Controls.Add((Control) this.VarType3);
      this.groupBox2.Controls.Add((Control) this.VarStat3);
      this.groupBox2.Controls.Add((Control) this.label7);
      this.groupBox2.Controls.Add((Control) this.VarType2);
      this.groupBox2.Controls.Add((Control) this.VarStat2);
      this.groupBox2.Controls.Add((Control) this.label6);
      this.groupBox2.Controls.Add((Control) this.VarType1);
      this.groupBox2.Controls.Add((Control) this.VarStat1);
      this.groupBox2.Controls.Add((Control) this.label5);
      this.groupBox2.Location = new Point(17, 145);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new Size(422, 122);
      this.groupBox2.TabIndex = 21;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Varable Types";
      this.VarType3.DropDownStyle = ComboBoxStyle.DropDownList;
      this.VarType3.FormattingEnabled = true;
      this.VarType3.Location = new Point(295, 82);
      this.VarType3.Name = "VarType3";
      this.VarType3.Size = new Size(121, 21);
      this.VarType3.TabIndex = 8;
      this.VarStat3.DropDownStyle = ComboBoxStyle.DropDownList;
      this.VarStat3.FormattingEnabled = true;
      this.VarStat3.Location = new Point(48, 82);
      this.VarStat3.Name = "VarStat3";
      this.VarStat3.Size = new Size(241, 21);
      this.VarStat3.TabIndex = 7;
      this.label7.AutoSize = true;
      this.label7.Location = new Point(10, 85);
      this.label7.Name = "label7";
      this.label7.Size = new Size(32, 13);
      this.label7.TabIndex = 6;
      this.label7.Text = "Var3:";
      this.VarType2.DropDownStyle = ComboBoxStyle.DropDownList;
      this.VarType2.FormattingEnabled = true;
      this.VarType2.Location = new Point(295, 55);
      this.VarType2.Name = "VarType2";
      this.VarType2.Size = new Size(121, 21);
      this.VarType2.TabIndex = 5;
      this.VarStat2.DropDownStyle = ComboBoxStyle.DropDownList;
      this.VarStat2.FormattingEnabled = true;
      this.VarStat2.Location = new Point(48, 55);
      this.VarStat2.Name = "VarStat2";
      this.VarStat2.Size = new Size(241, 21);
      this.VarStat2.TabIndex = 4;
      this.label6.AutoSize = true;
      this.label6.Location = new Point(10, 58);
      this.label6.Name = "label6";
      this.label6.Size = new Size(32, 13);
      this.label6.TabIndex = 3;
      this.label6.Text = "Var2:";
      this.VarType1.DropDownStyle = ComboBoxStyle.DropDownList;
      this.VarType1.FormattingEnabled = true;
      this.VarType1.Location = new Point(295, 28);
      this.VarType1.Name = "VarType1";
      this.VarType1.Size = new Size(121, 21);
      this.VarType1.TabIndex = 2;
      this.VarStat1.DropDownStyle = ComboBoxStyle.DropDownList;
      this.VarStat1.FormattingEnabled = true;
      this.VarStat1.Location = new Point(48, 28);
      this.VarStat1.Name = "VarStat1";
      this.VarStat1.Size = new Size(241, 21);
      this.VarStat1.TabIndex = 1;
      this.label5.AutoSize = true;
      this.label5.Location = new Point(10, 31);
      this.label5.Name = "label5";
      this.label5.Size = new Size(32, 13);
      this.label5.TabIndex = 0;
      this.label5.Text = "Var1:";
      this.groupBox1.Controls.Add((Control) this.label16);
      this.groupBox1.Controls.Add((Control) this.label15);
      this.groupBox1.Controls.Add((Control) this.label10);
      this.groupBox1.Controls.Add((Control) this.ConstStat1);
      this.groupBox1.Controls.Add((Control) this.ConstValue2);
      this.groupBox1.Controls.Add((Control) this.label8);
      this.groupBox1.Controls.Add((Control) this.ConstType2);
      this.groupBox1.Controls.Add((Control) this.ConstType1);
      this.groupBox1.Controls.Add((Control) this.ConstStat2);
      this.groupBox1.Controls.Add((Control) this.ConstValue1);
      this.groupBox1.Controls.Add((Control) this.label11);
      this.groupBox1.Controls.Add((Control) this.label9);
      this.groupBox1.Location = new Point(17, 279);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new Size(422, 129);
      this.groupBox1.TabIndex = 24;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Constants";
      this.label16.AutoSize = true;
      this.label16.Location = new Point(304, 102);
      this.label16.Name = "label16";
      this.label16.Size = new Size(84, 13);
      this.label16.TabIndex = 30;
      this.label16.Text = "(No signs or %'s)";
      this.label15.AutoSize = true;
      this.label15.Location = new Point(304, 49);
      this.label15.Name = "label15";
      this.label15.Size = new Size(84, 13);
      this.label15.TabIndex = 29;
      this.label15.Text = "(No signs or %'s)";
      this.label10.AutoSize = true;
      this.label10.Location = new Point(8, 102);
      this.label10.Name = "label10";
      this.label10.Size = new Size(37, 13);
      this.label10.TabIndex = 43;
      this.label10.Text = "Value:";
      this.ConstStat1.DropDownStyle = ComboBoxStyle.DropDownList;
      this.ConstStat1.FormattingEnabled = true;
      this.ConstStat1.Location = new Point(57, 19);
      this.ConstStat1.Name = "ConstStat1";
      this.ConstStat1.Size = new Size(241, 21);
      this.ConstStat1.TabIndex = 35;
      this.ConstValue2.Location = new Point(57, 99);
      this.ConstValue2.Name = "ConstValue2";
      this.ConstValue2.Size = new Size(241, 20);
      this.ConstValue2.TabIndex = 42;
      this.label8.AutoSize = true;
      this.label8.Location = new Point(8, 22);
      this.label8.Name = "label8";
      this.label8.Size = new Size(43, 13);
      this.label8.TabIndex = 34;
      this.label8.Text = "Const1:";
      this.ConstType2.DropDownStyle = ComboBoxStyle.DropDownList;
      this.ConstType2.FormattingEnabled = true;
      this.ConstType2.Location = new Point(304, 72);
      this.ConstType2.Name = "ConstType2";
      this.ConstType2.Size = new Size(112, 21);
      this.ConstType2.TabIndex = 41;
      this.ConstType1.DropDownStyle = ComboBoxStyle.DropDownList;
      this.ConstType1.FormattingEnabled = true;
      this.ConstType1.Location = new Point(304, 19);
      this.ConstType1.Name = "ConstType1";
      this.ConstType1.Size = new Size(112, 21);
      this.ConstType1.TabIndex = 36;
      this.ConstStat2.DropDownStyle = ComboBoxStyle.DropDownList;
      this.ConstStat2.FormattingEnabled = true;
      this.ConstStat2.Location = new Point(57, 72);
      this.ConstStat2.Name = "ConstStat2";
      this.ConstStat2.Size = new Size(241, 21);
      this.ConstStat2.TabIndex = 40;
      this.ConstValue1.Location = new Point(57, 46);
      this.ConstValue1.Name = "ConstValue1";
      this.ConstValue1.Size = new Size(241, 20);
      this.ConstValue1.TabIndex = 37;
      this.label11.AutoSize = true;
      this.label11.Location = new Point(8, 75);
      this.label11.Name = "label11";
      this.label11.Size = new Size(43, 13);
      this.label11.TabIndex = 39;
      this.label11.Text = "Const2:";
      this.label9.AutoSize = true;
      this.label9.Location = new Point(8, 49);
      this.label9.Name = "label9";
      this.label9.Size = new Size(37, 13);
      this.label9.TabIndex = 38;
      this.label9.Text = "Value:";
      this.Save.Location = new Point(12, 413);
      this.Save.Name = "Save";
      this.Save.Size = new Size(115, 36);
      this.Save.TabIndex = 25;
      this.Save.Text = "Save/Update";
      this.Save.UseVisualStyleBackColor = true;
      this.Save.Click += new EventHandler(this.Save_Click);
      this.label13.AutoSize = true;
      this.label13.Location = new Point(276, 70);
      this.label13.Name = "label13";
      this.label13.Size = new Size(123, 13);
      this.label13.TabIndex = 26;
      this.label13.Text = "(Not Displayed on Client)";
      this.label14.AutoSize = true;
      this.label14.Location = new Point(276, 96);
      this.label14.Name = "label14";
      this.label14.Size = new Size(163, 13);
      this.label14.TabIndex = 27;
      this.label14.Text = "(Name that is displayed on Client)";
      this.NewEffect.Location = new Point(133, 414);
      this.NewEffect.Name = "NewEffect";
      this.NewEffect.Size = new Size(115, 36);
      this.NewEffect.TabIndex = 28;
      this.NewEffect.Text = "New";
      this.NewEffect.UseVisualStyleBackColor = true;
      this.NewEffect.Click += new EventHandler(this.NewEffect_Click);
      this.button2.Location = new Point(553, 403);
      this.button2.Name = "button2";
      this.button2.Size = new Size(119, 46);
      this.button2.TabIndex = 29;
      this.button2.Text = "Edit Items";
      this.button2.UseVisualStyleBackColor = true;
      this.button2.Click += new EventHandler(this.button2_Click);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(740, 461);
      this.Controls.Add((Control) this.button2);
      this.Controls.Add((Control) this.NewEffect);
      this.Controls.Add((Control) this.label14);
      this.Controls.Add((Control) this.label13);
      this.Controls.Add((Control) this.Save);
      this.Controls.Add((Control) this.groupBox1);
      this.Controls.Add((Control) this.button1);
      this.Controls.Add((Control) this.groupBox2);
      this.Controls.Add((Control) this.TargetTypeGrp);
      this.Controls.Add((Control) this.label4);
      this.Controls.Add((Control) this.label3);
      this.Controls.Add((Control) this.label2);
      this.Controls.Add((Control) this.EffectToolTip);
      this.Controls.Add((Control) this.EffectDesc);
      this.Controls.Add((Control) this.EffectName);
      this.Controls.Add((Control) this.label1);
      this.Controls.Add((Control) this.EffectType);
      this.Name = nameof (Form1);
      this.Text = "Effect Editor";
      this.TargetTypeGrp.ResumeLayout(false);
      this.TargetTypeGrp.PerformLayout();
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public Form1()
    {
      this.InitializeComponent();
      this.ItemEditor = new EditItem();
      DB.Connect();
      this.m_conn = DB.GetConnection();
      this.m_EffectID = 0;
      this.EffectBuff.Items.AddRange((object[]) this.GetBuffList());
      this.VarStat1.Items.AddRange((object[]) this.GetStatsList());
      this.VarStat2.Items.AddRange((object[]) this.GetStatsList());
      this.VarStat3.Items.AddRange((object[]) this.GetStatsList());
      this.ConstStat1.Items.AddRange((object[]) this.GetStatsList());
      this.ConstStat2.Items.AddRange((object[]) this.GetStatsList());
      this.VarType1.Items.AddRange((object[]) Form1.GetVarableType());
      this.VarType2.Items.AddRange((object[]) Form1.GetVarableType());
      this.VarType3.Items.AddRange((object[]) Form1.GetVarableType());
      this.ConstType1.Items.AddRange((object[]) Form1.GetVarableType());
      this.ConstType2.Items.AddRange((object[]) Form1.GetVarableType());
      this.EffectType.SelectedIndex = 0;
      this.VarStat1.SelectedIndex = 0;
      this.VarStat2.SelectedIndex = 0;
      this.VarStat3.SelectedIndex = 0;
      this.VarType1.SelectedIndex = 0;
      this.VarType2.SelectedIndex = 0;
      this.VarType3.SelectedIndex = 0;
      this.ConstType1.SelectedIndex = 0;
      this.ConstType2.SelectedIndex = 0;
      this.ConstStat1.SelectedIndex = 0;
      this.ConstStat2.SelectedIndex = 0;
    }

    private void LoadEffect(int ID)
    {
      this.m_EffectID = ID;
      DataTable dataTable = new DataTable();
      new MySqlDataAdapter("SELECT item_effect_base.EffectType,item_effect_base.Name,item_effect_base.Description,item_effect_base.Tooltip,item_effect_base.flag1,item_effect_base.flag2,item_effect_base.Constant1Value,item_effect_base.Constant1Stat,item_effect_base.Constant1Type,item_effect_base.Constant2Value,item_effect_base.Constant2Stat,item_effect_base.Constant2Type,item_effect_base.Var1Stat,item_effect_base.Var1Type,item_effect_base.Var2Stat,item_effect_base.Var2Type,item_effect_base.Var3Stat,item_effect_base.Var3Type,item_effect_base.VisualEffect,item_effect_base.Buff_Name FROM `item_effect_base` WHERE `item_effect_base`.`EffectID` =  '" + (object) this.m_EffectID + "'", this.m_conn).Fill(dataTable);
      if (dataTable.Rows.Count <= 0)
        return;
      DataRow row = dataTable.Rows[0];
      this.EffectType.SelectedIndex = int.Parse(row["EffectType"].ToString());
      this.EffectName.Text = row["Name"].ToString();
      this.EffectToolTip.Text = row["Tooltip"].ToString();
      this.EffectDesc.Text = row["Description"].ToString();
      this.TFriend.Checked = ((int) row["Flag1"] & 16) > 0;
      this.TEnemy.Checked = ((int) row["Flag1"] & 32) > 0;
      this.TGroupM.Checked = ((int) row["Flag1"] & 64) > 0;
      this.RequireT.Checked = ((int) row["Flag2"] & 1) > 0;
      this.EffectBuff.SelectedIndex = this.EffectBuff.Items.IndexOf((object) row["Buff_Name"].ToString());
      this.VarStat1.SelectedIndex = this.VarStat1.Items.IndexOf((object) row["Var1Stat"].ToString());
      this.VarStat2.SelectedIndex = this.VarStat2.Items.IndexOf((object) row["Var2Stat"].ToString());
      this.VarStat3.SelectedIndex = this.VarStat3.Items.IndexOf((object) row["Var3Stat"].ToString());
      this.ConstStat1.SelectedIndex = this.ConstStat1.Items.IndexOf((object) row["Constant1Stat"].ToString());
      this.ConstStat2.SelectedIndex = this.ConstStat2.Items.IndexOf((object) row["Constant2Stat"].ToString());
      this.VarType1.SelectedIndex = int.Parse(row["Var1Type"].ToString());
      this.VarType2.SelectedIndex = int.Parse(row["Var2Type"].ToString());
      this.VarType3.SelectedIndex = int.Parse(row["Var3Type"].ToString());
      this.ConstType1.SelectedIndex = int.Parse(row["Constant1Type"].ToString());
      this.ConstType2.SelectedIndex = int.Parse(row["Constant2Type"].ToString());
      this.ConstValue1.Text = row["Constant1Value"].ToString();
      this.ConstValue2.Text = row["Constant2Value"].ToString();
    }

    private string[] GetStatsList()
    {
      List<string> stringList = new List<string>();
      DataTable dataTable = new DataTable();
      new MySqlDataAdapter("SELECT Stat_Name FROM `item_effect_stats`", this.m_conn).Fill(dataTable);
      foreach (DataRow row in (InternalDataCollectionBase) dataTable.Rows)
        stringList.Add(row["Stat_Name"].ToString());
      return stringList.ToArray();
    }

    private string[] GetBuffList()
    {
      List<string> stringList = new List<string>();
      DataTable dataTable = new DataTable();
      new MySqlDataAdapter("SELECT buff_name FROM buffs", this.m_conn).Fill(dataTable);
      foreach (DataRow row in (InternalDataCollectionBase) dataTable.Rows)
        stringList.Add(row["buff_name"].ToString());
      return stringList.ToArray();
    }

    private static CodeValue[] GetVarableType()
    {
      return new List<CodeValue>()
      {
        CodeValue.Formatted(0, "Not Used"),
        CodeValue.Formatted(1, "Increase Value"),
        CodeValue.Formatted(2, "Increase Percent"),
        CodeValue.Formatted(3, "Decrease Value"),
        CodeValue.Formatted(4, "Decrease Percent"),
        CodeValue.Formatted(5, "Duration")
      }.ToArray();
    }

    private void EffectType_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (int.Parse(this.EffectType.SelectedIndex.ToString()) == 0)
        this.TargetTypeGrp.Enabled = false;
      else
        this.TargetTypeGrp.Enabled = true;
    }

    private void button1_Click(object sender, EventArgs e)
    {
      EffectSearch effectSearch = new EffectSearch();
      int num = (int) effectSearch.ShowDialog();
      if (effectSearch.GetEffectID() == -1)
        return;
      this.LoadEffect(effectSearch.GetEffectID());
    }

    private void Save_Click(object sender, EventArgs e)
    {
      string str = "UPDATE `item_effect_base` SET ";
      int num1 = 0;
      int num2 = 0;
      int num3 = num1 + (this.TFriend.Checked ? 16 : 0) + (this.TEnemy.Checked ? 32 : 0) + (this.TGroupM.Checked ? 64 : 0);
      int num4 = num2 + (this.RequireT.Checked ? 1 : 0) + (this.RequireT.Checked ? 0 : 2);
      if (this.VisualEffect.Text == "")
        this.VisualEffect.Text = "0";
      new MySqlDataAdapter(str + "`EffectType` = '" + (object) this.EffectType.SelectedIndex + "'," + "`Name` = '" + this.EffectName.Text + "'," + "`Tooltip` = '" + this.EffectToolTip.Text + "'," + "`Description` = '" + this.EffectDesc.Text + "'," + "`Flag1` = '" + num3.ToString() + "'," + "`Flag2` = '" + num4.ToString() + "'," + "`Buff_Name` = '" + this.EffectBuff.Text + "'," + "`VisualEffect` = '" + this.VisualEffect.Text + "'," + "`Var1Stat` = '" + this.VarStat1.Text + "'," + "`Var2Stat` = '" + this.VarStat2.Text + "'," + "`Var3Stat` = '" + this.VarStat3.Text + "'," + "`Constant1Stat` = '" + this.ConstStat1.Text + "'," + "`Constant2Stat` = '" + this.ConstStat2.Text + "'," + "`Var1Type` = '" + (object) this.VarType1.SelectedIndex + "'," + "`Var2Type` = '" + (object) this.VarType2.SelectedIndex + "'," + "`Var3Type` = '" + (object) this.VarType3.SelectedIndex + "'," + "`Constant1Type` = '" + (object) this.ConstType1.SelectedIndex + "'," + "`Constant2Type` = '" + (object) this.ConstType2.SelectedIndex + "'," + "`Constant1Value` = '" + this.ConstValue1.Text + "'," + "`Constant2Value` = '" + this.ConstValue2.Text + "' " + "WHERE `EffectID` = '" + (object) this.m_EffectID + "'", this.m_conn).Fill(new DataTable());
    }

    private void NewEffect_Click(object sender, EventArgs e)
    {
      DataTable dataTable = new DataTable();
      string selectCommandText1 = "INSERT INTO `item_effect_base` (item_effect_base.EffectType,item_effect_base.Name,item_effect_base.Description,item_effect_base.Tooltip,item_effect_base.flag1,item_effect_base.flag2,item_effect_base.Constant1Value,item_effect_base.Constant1Stat,item_effect_base.Constant1Type,item_effect_base.Constant2Value,item_effect_base.Constant2Stat,item_effect_base.Constant2Type,item_effect_base.Var1Stat,item_effect_base.Var1Type,item_effect_base.Var2Stat,item_effect_base.Var2Type,item_effect_base.Var3Stat,item_effect_base.Var3Type,item_effect_base.Buff_Name) values (0,'none','none','none',0,0,0,'NO_STAT',0,0,'NO_STAT',0,'NO_STAT',0,'NO_STAT',0,'NO_STAT',0,'BUFF_NONE')";
      string selectCommandText2 = "select last_insert_id()";
      MySqlDataAdapter mySqlDataAdapter1 = new MySqlDataAdapter(selectCommandText1, this.m_conn);
      MySqlDataAdapter mySqlDataAdapter2 = new MySqlDataAdapter(selectCommandText2, this.m_conn);
      mySqlDataAdapter1.Fill(dataTable);
      mySqlDataAdapter2.Fill(dataTable);
      if (dataTable.Rows.Count <= 0)
        return;
      this.LoadEffect(int.Parse(dataTable.Rows[0][0].ToString()));
    }

    private void button2_Click(object sender, EventArgs e)
    {
      int num = (int) this.ItemEditor.ShowDialog();
    }
  }
}
