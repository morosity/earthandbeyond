﻿// Decompiled with JetBrains decompiler
// Type: SQLBind.ItemBrowse
// Assembly: SQLBind, Version=1.0.0.1, Culture=neutral, PublicKeyToken=null
// MVID: 8DC05ED6-E010-4553-A0DD-607EBD42D93A
// Assembly location: D:\Server\eab\Tools\Effect Editor.exe

using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace SQLBind
{
  public class ItemBrowse : Form
  {
    private string m_DataBase = "net7";
    private string[] g_ItemType = new string[20]
    {
      "System",
      "Weapon",
      "Shields",
      "Sensor",
      "Ejector",
      "Turret",
      "Engine",
      "Reactor",
      "Controler",
      "Robot",
      "Ammo",
      "Devices",
      "System",
      "Base",
      "Beam Weapon",
      "Missile Launcher",
      "Projectile Weapon",
      "Countermesure",
      "Over Rides",
      "All"
    };
    private List<string> g_Description = new List<string>();
    private int g_ItemBaseID = -1;
    private IContainer components;
    private DataGridView ItemList;
    private TextBox ItemDescription;
    private Label label1;
    private Label label2;
    private TextBox ItemNameQuery;
    private Label label3;
    private ComboBox ItemLevelQuery;
    private Label label4;
    private ComboBox ItemTypeQuery;
    private Button Search;
    private Button AddSelected;
    private Button CloseWindow;
    private DataGridViewTextBoxColumn Column6;
    private DataGridViewTextBoxColumn Column2;
    private DataGridViewTextBoxColumn Column1;
    private DataGridViewTextBoxColumn Column5;
    private DataGridViewTextBoxColumn Column3;
    private DataGridViewTextBoxColumn Column4;
    private Label RowCount;

    public ItemBrowse()
    {
      this.InitializeComponent();
      this.ItemTypeQuery.SelectedIndex = 0;
      this.ItemLevelQuery.SelectedIndex = 0;
    }

    private MySqlDataReader ExcuteSQLQuery(string SQLD)
    {
      MySqlConnection connection = new MySqlConnection(DB.GetLoginStr());
      MySqlCommand mySqlCommand = new MySqlCommand(SQLD, connection);
      connection.Open();
      return mySqlCommand.ExecuteReader();
    }

    private int ExecuteSQL(string SQLD)
    {
      string cmdText = "SELECT LAST_INSERT_ID()";
      MySqlConnection connection = new MySqlConnection(DB.GetLoginStr());
      MySqlCommand mySqlCommand1 = new MySqlCommand(SQLD, connection);
      MySqlCommand mySqlCommand2 = new MySqlCommand(cmdText, connection);
      connection.Open();
      try
      {
        mySqlCommand1.ExecuteNonQuery();
        MySqlDataReader mySqlDataReader = mySqlCommand2.ExecuteReader();
        mySqlDataReader.Read();
        return mySqlDataReader.GetInt32(0);
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("SQL Error: " + ex.Message, "SQL Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        return -1;
      }
    }

    public DialogResult ShowDialog(bool Multi)
    {
      this.ItemList.MultiSelect = Multi;
      return this.ShowDialog();
    }

    public int GetNumSelected()
    {
      if (this.ItemList.MultiSelect)
        return this.ItemList.SelectedRows.Count;
      return 0;
    }

    public int GetSelecteItemID(int Row)
    {
      if (this.ItemList.MultiSelect)
        return int.Parse(this.ItemList.SelectedRows[Row].Cells[1].Value.ToString());
      return 0;
    }

    public string GetSelecteItemName(int Row)
    {
      if (this.ItemList.MultiSelect)
        return this.ItemList.SelectedRows[Row].Cells[2].Value.ToString();
      return "";
    }

    public int GetItemBase()
    {
      if (!this.ItemList.MultiSelect)
        return this.g_ItemBaseID;
      return 0;
    }

    private void ErrorMsg(string Msg)
    {
      int num = (int) MessageBox.Show(Msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
    }

    private void AddSelected_Click(object sender, EventArgs e)
    {
      int index = this.ItemList.CurrentRow.Index;
      if (index == -1)
      {
        this.ErrorMsg("You must select a row!");
      }
      else
      {
        if (!this.ItemList.MultiSelect)
          this.g_ItemBaseID = int.Parse(this.ItemList.Rows[int.Parse(this.ItemList.Rows[index].Cells[0].Value.ToString())].Cells[1].Value.ToString());
        this.Close();
      }
    }

    private void Search_Click(object sender, EventArgs e)
    {
      bool flag = false;
      string SQLD = "SELECT `id`,`level`,`category`,`sub_category`,`type`,`price`,`max_stack`,`name`,`description`,`manufacturer` FROM `item_base` WHERE ";
      if (this.ItemNameQuery.Text != "")
      {
        SQLD = SQLD + "`item_base`.`name` like '%" + this.ItemNameQuery.Text + "%'";
        flag = true;
      }
      if (this.ItemLevelQuery.SelectedIndex != 0)
      {
        if (flag)
          SQLD += " and ";
        SQLD = SQLD + "`item_base`.`level` = '" + this.ItemLevelQuery.SelectedIndex.ToString() + "'";
        flag = true;
      }
      if (this.ItemTypeQuery.SelectedIndex != 0)
      {
        if (flag)
          SQLD += " and ";
        SQLD = SQLD + "`item_base`.`type` = '" + (object) (this.ItemTypeQuery.SelectedIndex - 1) + "'";
        flag = true;
      }
      if (!flag)
      {
        this.ErrorMsg("You need to specify atleast one critera to search on!");
      }
      else
      {
        MySqlDataReader mySqlDataReader = this.ExcuteSQLQuery(SQLD);
        if (!mySqlDataReader.HasRows)
        {
          this.ErrorMsg("No results from your query");
        }
        else
        {
          this.g_Description.Clear();
          this.ItemList.Rows.Clear();
          int num = 0;
          while (mySqlDataReader.Read())
          {
            this.g_Description.Add(mySqlDataReader.GetString(8));
            this.ItemList.Rows.Add((object) num, (object) mySqlDataReader.GetInt32(0), (object) mySqlDataReader.GetString(7), (object) this.g_ItemType[mySqlDataReader.GetInt32(4)], (object) mySqlDataReader.GetInt32(1), (object) mySqlDataReader.GetInt32(5));
            ++num;
          }
          this.RowCount.Text = "Number of Results: " + this.ItemList.RowCount.ToString();
        }
      }
    }

    private void ItemList_SelectionChanged(object sender, EventArgs e)
    {
      int index1 = this.ItemList.CurrentRow.Index;
      if (index1 == -1)
        return;
      int index2 = int.Parse(this.ItemList.Rows[index1].Cells[0].Value.ToString());
      if (this.g_Description.Count <= index2)
        return;
      this.ItemDescription.Text = this.g_Description[index2];
    }

    private void CloseWindow_Click(object sender, EventArgs e)
    {
      this.ItemList.MultiSelect = false;
      this.Close();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.ItemList = new DataGridView();
      this.Column6 = new DataGridViewTextBoxColumn();
      this.Column2 = new DataGridViewTextBoxColumn();
      this.Column1 = new DataGridViewTextBoxColumn();
      this.Column5 = new DataGridViewTextBoxColumn();
      this.Column3 = new DataGridViewTextBoxColumn();
      this.Column4 = new DataGridViewTextBoxColumn();
      this.ItemDescription = new TextBox();
      this.label1 = new Label();
      this.label2 = new Label();
      this.ItemNameQuery = new TextBox();
      this.label3 = new Label();
      this.ItemLevelQuery = new ComboBox();
      this.label4 = new Label();
      this.ItemTypeQuery = new ComboBox();
      this.Search = new Button();
      this.AddSelected = new Button();
      this.CloseWindow = new Button();
      this.RowCount = new Label();
      ((ISupportInitialize) this.ItemList).BeginInit();
      this.SuspendLayout();
      this.ItemList.AllowUserToAddRows = false;
      this.ItemList.AllowUserToDeleteRows = false;
      this.ItemList.AllowUserToOrderColumns = true;
      this.ItemList.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.ItemList.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.ItemList.Columns.AddRange((DataGridViewColumn) this.Column6, (DataGridViewColumn) this.Column2, (DataGridViewColumn) this.Column1, (DataGridViewColumn) this.Column5, (DataGridViewColumn) this.Column3, (DataGridViewColumn) this.Column4);
      this.ItemList.Location = new Point(197, 12);
      this.ItemList.MultiSelect = false;
      this.ItemList.Name = "ItemList";
      this.ItemList.ReadOnly = true;
      this.ItemList.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
      this.ItemList.Size = new Size(402, 213);
      this.ItemList.TabIndex = 0;
      this.ItemList.SelectionChanged += new EventHandler(this.ItemList_SelectionChanged);
      this.Column6.HeaderText = "ID";
      this.Column6.Name = "Column6";
      this.Column6.ReadOnly = true;
      this.Column6.Visible = false;
      this.Column2.HeaderText = "Item ID";
      this.Column2.Name = "Column2";
      this.Column2.ReadOnly = true;
      this.Column2.Width = 70;
      this.Column1.HeaderText = "Item Name";
      this.Column1.Name = "Column1";
      this.Column1.ReadOnly = true;
      this.Column1.Width = 200;
      this.Column5.HeaderText = "ItemType";
      this.Column5.Name = "Column5";
      this.Column5.ReadOnly = true;
      this.Column5.Resizable = DataGridViewTriState.True;
      this.Column3.HeaderText = "Item Level";
      this.Column3.Name = "Column3";
      this.Column3.ReadOnly = true;
      this.Column4.HeaderText = "Price";
      this.Column4.Name = "Column4";
      this.Column4.ReadOnly = true;
      this.ItemDescription.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.ItemDescription.Location = new Point(197, 244);
      this.ItemDescription.Multiline = true;
      this.ItemDescription.Name = "ItemDescription";
      this.ItemDescription.Size = new Size(401, 80);
      this.ItemDescription.TabIndex = 1;
      this.label1.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.label1.AutoSize = true;
      this.label1.Location = new Point(194, 228);
      this.label1.Name = "label1";
      this.label1.Size = new Size(63, 13);
      this.label1.TabIndex = 2;
      this.label1.Text = "Description:";
      this.label2.AutoSize = true;
      this.label2.Location = new Point(12, 14);
      this.label2.Name = "label2";
      this.label2.Size = new Size(61, 13);
      this.label2.TabIndex = 3;
      this.label2.Text = "Item Name:";
      this.ItemNameQuery.Location = new Point(15, 30);
      this.ItemNameQuery.Name = "ItemNameQuery";
      this.ItemNameQuery.Size = new Size(176, 20);
      this.ItemNameQuery.TabIndex = 4;
      this.label3.AutoSize = true;
      this.label3.Location = new Point(12, 53);
      this.label3.Name = "label3";
      this.label3.Size = new Size(56, 13);
      this.label3.TabIndex = 5;
      this.label3.Text = "Item Level";
      this.ItemLevelQuery.DropDownStyle = ComboBoxStyle.DropDownList;
      this.ItemLevelQuery.FormattingEnabled = true;
      this.ItemLevelQuery.Items.AddRange(new object[10]
      {
        (object) "Not Selected",
        (object) "1",
        (object) "2",
        (object) "3",
        (object) "4",
        (object) "5",
        (object) "6",
        (object) "7",
        (object) "8",
        (object) "9"
      });
      this.ItemLevelQuery.Location = new Point(15, 69);
      this.ItemLevelQuery.Name = "ItemLevelQuery";
      this.ItemLevelQuery.Size = new Size(71, 21);
      this.ItemLevelQuery.TabIndex = 6;
      this.label4.AutoSize = true;
      this.label4.Location = new Point(12, 93);
      this.label4.Name = "label4";
      this.label4.Size = new Size(57, 13);
      this.label4.TabIndex = 7;
      this.label4.Text = "Item Type:";
      this.ItemTypeQuery.DropDownStyle = ComboBoxStyle.DropDownList;
      this.ItemTypeQuery.FormattingEnabled = true;
      this.ItemTypeQuery.Items.AddRange(new object[20]
      {
        (object) "Not Selected",
        (object) "System",
        (object) "Weapon",
        (object) "Shields",
        (object) "Sensor",
        (object) "Ejector",
        (object) "Turret",
        (object) "Engine",
        (object) "Reactor",
        (object) "Controler",
        (object) "Robot",
        (object) "Ammo",
        (object) "Devices",
        (object) "System",
        (object) "Compontents",
        (object) "Beam Weapon",
        (object) "Missile Launcher",
        (object) "Projectile Weapon",
        (object) "Countermesure",
        (object) "Over Rides"
      });
      this.ItemTypeQuery.Location = new Point(15, 109);
      this.ItemTypeQuery.Name = "ItemTypeQuery";
      this.ItemTypeQuery.Size = new Size(176, 21);
      this.ItemTypeQuery.TabIndex = 8;
      this.Search.Location = new Point(15, 136);
      this.Search.Name = "Search";
      this.Search.Size = new Size(80, 26);
      this.Search.TabIndex = 9;
      this.Search.Text = "Search";
      this.Search.UseVisualStyleBackColor = true;
      this.Search.Click += new EventHandler(this.Search_Click);
      this.AddSelected.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
      this.AddSelected.Location = new Point(12, 239);
      this.AddSelected.Name = "AddSelected";
      this.AddSelected.Size = new Size(116, 41);
      this.AddSelected.TabIndex = 10;
      this.AddSelected.Text = "Add Selected Item";
      this.AddSelected.UseVisualStyleBackColor = true;
      this.AddSelected.Click += new EventHandler(this.AddSelected_Click);
      this.CloseWindow.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
      this.CloseWindow.Location = new Point(12, 286);
      this.CloseWindow.Name = "CloseWindow";
      this.CloseWindow.Size = new Size(116, 41);
      this.CloseWindow.TabIndex = 11;
      this.CloseWindow.Text = "Close";
      this.CloseWindow.UseVisualStyleBackColor = true;
      this.CloseWindow.Click += new EventHandler(this.CloseWindow_Click);
      this.RowCount.AutoSize = true;
      this.RowCount.Location = new Point(12, 165);
      this.RowCount.Name = "RowCount";
      this.RowCount.Size = new Size(100, 13);
      this.RowCount.TabIndex = 12;
      this.RowCount.Text = "Number of Results: ";
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(611, 339);
      this.Controls.Add((Control) this.RowCount);
      this.Controls.Add((Control) this.CloseWindow);
      this.Controls.Add((Control) this.AddSelected);
      this.Controls.Add((Control) this.Search);
      this.Controls.Add((Control) this.ItemTypeQuery);
      this.Controls.Add((Control) this.label4);
      this.Controls.Add((Control) this.ItemLevelQuery);
      this.Controls.Add((Control) this.label3);
      this.Controls.Add((Control) this.ItemNameQuery);
      this.Controls.Add((Control) this.label2);
      this.Controls.Add((Control) this.label1);
      this.Controls.Add((Control) this.ItemDescription);
      this.Controls.Add((Control) this.ItemList);
      this.MinimumSize = new Size(619, 366);
      this.Name = nameof (ItemBrowse);
      this.Text = nameof (ItemBrowse);
      ((ISupportInitialize) this.ItemList).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();
    }
  }
}
