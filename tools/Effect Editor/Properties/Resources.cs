﻿// Decompiled with JetBrains decompiler
// Type: SQLBind.Properties.Resources
// Assembly: SQLBind, Version=1.0.0.1, Culture=neutral, PublicKeyToken=null
// MVID: 8DC05ED6-E010-4553-A0DD-607EBD42D93A
// Assembly location: D:\Server\eab\Tools\Effect Editor.exe

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace SQLBind.Properties
{
  [DebuggerNonUserCode]
  [CompilerGenerated]
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "2.0.0.0")]
  internal class Resources
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal Resources()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (SQLBind.Properties.Resources.resourceMan == null)
          SQLBind.Properties.Resources.resourceMan = new ResourceManager("SQLBind.Properties.Resources", typeof (SQLBind.Properties.Resources).Assembly);
        return SQLBind.Properties.Resources.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get
      {
        return SQLBind.Properties.Resources.resourceCulture;
      }
      set
      {
        SQLBind.Properties.Resources.resourceCulture = value;
      }
    }
  }
}
