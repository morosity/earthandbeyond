﻿// Decompiled with JetBrains decompiler
// Type: SQLBind.DB
// Assembly: SQLBind, Version=1.0.0.1, Culture=neutral, PublicKeyToken=null
// MVID: 8DC05ED6-E010-4553-A0DD-607EBD42D93A
// Assembly location: D:\Server\eab\Tools\Effect Editor.exe

using MySql.Data.MySqlClient;

namespace SQLBind
{
  internal static class DB
  {
    private static MySqlConnection m_conn;
    private static string m_LoginStr;

    public static void SetLogin(string Host, string Port, string DataBase, string User, string Pass)
    {
      string connectionString = "server=" + Host + ";database=" + DataBase + ";port=" + Port + ";uid=" + User + ";password=" + Pass;
      DB.m_LoginStr = connectionString;
      DB.m_conn = new MySqlConnection(connectionString);
    }

    public static string GetLoginStr()
    {
      return DB.m_LoginStr;
    }

    public static void Connect()
    {
      DB.m_conn.Open();
    }

    public static MySqlConnection GetConnection()
    {
      return DB.m_conn;
    }
  }
}
