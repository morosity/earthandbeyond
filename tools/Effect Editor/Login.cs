﻿// Decompiled with JetBrains decompiler
// Type: SQLBind.Login
// Assembly: SQLBind, Version=1.0.0.1, Culture=neutral, PublicKeyToken=null
// MVID: 8DC05ED6-E010-4553-A0DD-607EBD42D93A
// Assembly location: D:\Server\eab\Tools\Effect Editor.exe

using MySql.Data.MySqlClient;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;

namespace SQLBind
{
  public class Login : Form
  {
    private string DataBase = "net7";
    private IContainer components;
    private Label label1;
    private Label label2;
    public TextBox LoginUsername;
    public TextBox LoginPassword;
    private Button ExitLogin;
    private Button LoginButton;
    private Label label3;
    public TextBox SQLServer;
    public TextBox SQLPort;
    private Label lable9;
    public bool m_Cancel;
    public bool m_Login;
    public bool m_HasChanged;
    private string Host;
    private string Port;
    private string User;
    private string Pass;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.label1 = new Label();
      this.label2 = new Label();
      this.LoginUsername = new TextBox();
      this.LoginPassword = new TextBox();
      this.ExitLogin = new Button();
      this.LoginButton = new Button();
      this.label3 = new Label();
      this.SQLServer = new TextBox();
      this.SQLPort = new TextBox();
      this.lable9 = new Label();
      this.SuspendLayout();
      this.label1.AutoSize = true;
      this.label1.Location = new Point(37, 15);
      this.label1.Name = "label1";
      this.label1.Size = new Size(58, 13);
      this.label1.TabIndex = 0;
      this.label1.Text = "Username:";
      this.label2.AutoSize = true;
      this.label2.Location = new Point(39, 41);
      this.label2.Name = "label2";
      this.label2.Size = new Size(56, 13);
      this.label2.TabIndex = 1;
      this.label2.Text = "Password:";
      this.LoginUsername.Location = new Point(101, 12);
      this.LoginUsername.Name = "LoginUsername";
      this.LoginUsername.Size = new Size(142, 20);
      this.LoginUsername.TabIndex = 3;
      this.LoginUsername.TextChanged += new EventHandler(this.LoginChange);
      this.LoginPassword.Location = new Point(101, 38);
      this.LoginPassword.Name = "LoginPassword";
      this.LoginPassword.PasswordChar = '*';
      this.LoginPassword.Size = new Size(142, 20);
      this.LoginPassword.TabIndex = 4;
      this.LoginPassword.TextChanged += new EventHandler(this.LoginChange);
      this.ExitLogin.Location = new Point(18, 123);
      this.ExitLogin.Name = "ExitLogin";
      this.ExitLogin.Size = new Size(77, 28);
      this.ExitLogin.TabIndex = 8;
      this.ExitLogin.Text = "Cancel";
      this.ExitLogin.UseVisualStyleBackColor = true;
      this.ExitLogin.Click += new EventHandler(this.ExitLogin_Click);
      this.LoginButton.Location = new Point(189, 123);
      this.LoginButton.Name = "LoginButton";
      this.LoginButton.Size = new Size(77, 28);
      this.LoginButton.TabIndex = 7;
      this.LoginButton.Text = nameof (Login);
      this.LoginButton.UseVisualStyleBackColor = true;
      this.LoginButton.Click += new EventHandler(this.LoginButton_Click);
      this.label3.AutoSize = true;
      this.label3.Location = new Point(39, 67);
      this.label3.Name = "label3";
      this.label3.Size = new Size(56, 13);
      this.label3.TabIndex = 2;
      this.label3.Text = "SQL Host:";
      this.SQLServer.Location = new Point(101, 64);
      this.SQLServer.Name = "SQLServer";
      this.SQLServer.Size = new Size(142, 20);
      this.SQLServer.TabIndex = 5;
      this.SQLServer.TextChanged += new EventHandler(this.LoginChange);
      this.SQLPort.Location = new Point(101, 90);
      this.SQLPort.Name = "SQLPort";
      this.SQLPort.Size = new Size(61, 20);
      this.SQLPort.TabIndex = 6;
      this.SQLPort.TextChanged += new EventHandler(this.LoginChange);
      this.lable9.AutoSize = true;
      this.lable9.Location = new Point(42, 93);
      this.lable9.Name = "lable9";
      this.lable9.Size = new Size(53, 13);
      this.lable9.TabIndex = 8;
      this.lable9.Text = "SQL Port:";
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(278, 158);
      this.Controls.Add((Control) this.SQLPort);
      this.Controls.Add((Control) this.lable9);
      this.Controls.Add((Control) this.LoginButton);
      this.Controls.Add((Control) this.ExitLogin);
      this.Controls.Add((Control) this.SQLServer);
      this.Controls.Add((Control) this.LoginPassword);
      this.Controls.Add((Control) this.LoginUsername);
      this.Controls.Add((Control) this.label3);
      this.Controls.Add((Control) this.label2);
      this.Controls.Add((Control) this.label1);
      this.FormBorderStyle = FormBorderStyle.FixedSingle;
      this.MaximizeBox = false;
      this.Name = nameof (Login);
      this.Text = nameof (Login);
      this.FormClosed += new FormClosedEventHandler(this.Login_FormClosed);
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    public Login()
    {
      this.InitializeComponent();
    }

    private void LoginButton_Click(object sender, EventArgs e)
    {
      MySqlConnection connection = (MySqlConnection) null;
      try
      {
        this.User = this.LoginUsername.Text;
        this.Pass = this.LoginPassword.Text;
        this.Host = this.SQLServer.Text;
        this.Port = this.SQLPort.Text;
        connection = new MySqlConnection("server=" + this.Host + ";database=" + this.DataBase + ";port=" + this.Port + ";uid=" + this.User + ";password=" + this.Pass);
        MySqlCommand mySqlCommand = new MySqlCommand("SELECT `versions`.`Version` FROM `versions` WHERE `versions`.`EName` = 'EffectEditor'", connection);
        connection.Open();
        MySqlDataReader mySqlDataReader = mySqlCommand.ExecuteReader();
        if (mySqlDataReader.HasRows)
        {
          mySqlDataReader.Read();
          if (mySqlDataReader.GetString(0) != Assembly.GetExecutingAssembly().GetName().Version.ToString())
          {
            int num = (int) MessageBox.Show("You are running an old version.  Please update your editor.", "Old Version", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            this.m_Cancel = true;
          }
          else
          {
            this.m_Cancel = false;
            this.m_Login = true;
          }
        }
        else
          this.m_Cancel = true;
        this.Close();
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show(ex.Message, "Connection Error");
      }
      finally
      {
        if (connection != null)
          connection.Close();
      }
    }

    private void ExitLogin_Click(object sender, EventArgs e)
    {
      this.m_Cancel = true;
      this.Close();
    }

    private void LoginChange(object sender, EventArgs e)
    {
      this.m_HasChanged = true;
    }

    private void Login_FormClosed(object sender, FormClosedEventArgs e)
    {
      if (this.m_Login)
        return;
      this.m_Cancel = true;
    }
  }
}
