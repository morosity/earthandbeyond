﻿// Decompiled with JetBrains decompiler
// Type: SQLBind.EffectComboHandel
// Assembly: SQLBind, Version=1.0.0.1, Culture=neutral, PublicKeyToken=null
// MVID: 8DC05ED6-E010-4553-A0DD-607EBD42D93A
// Assembly location: D:\Server\eab\Tools\Effect Editor.exe

using System.Windows.Forms;

namespace SQLBind
{
  internal class EffectComboHandel
  {
    public TextBox Var1;
    public TextBox Var2;
    public TextBox Var3;
    public Label Var1Lable;
    public Label Var2Lable;
    public Label Var3Lable;
    public Label EffectString;
    public ComboBox EffectBox;

    public void EnableFields(bool Enable)
    {
      this.EffectBox.Enabled = Enable;
      this.Var1.Enabled = Enable;
      this.Var2.Enabled = Enable;
      this.Var3.Enabled = Enable;
    }
  }
}
