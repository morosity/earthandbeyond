﻿// Decompiled with JetBrains decompiler
// Type: SQLBind.Program
// Assembly: SQLBind, Version=1.0.0.1, Culture=neutral, PublicKeyToken=null
// MVID: 8DC05ED6-E010-4553-A0DD-607EBD42D93A
// Assembly location: D:\Server\eab\Tools\Effect Editor.exe

using System;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace SQLBind
{
  internal static class Program
  {
    private static string DataBase = "net7";
    private static string Host;
    private static string Port;
    private static string User;
    private static string Pass;

    [STAThread]
    private static void Main()
    {
      try
      {
        DataSet dataSet = new DataSet();
        int num = (int) dataSet.ReadXml(Application.StartupPath + "\\Config.xml");
        DataRow row = dataSet.Tables[0].Rows[0];
        Program.Host = row.ItemArray[0].ToString();
        Program.Port = row.ItemArray[1].ToString();
        Program.User = row.ItemArray[2].ToString();
        Program.Pass = row.ItemArray[3].ToString();
      }
      catch
      {
        Program.Host = "net-7.org";
        Program.Port = "3307";
        Program.User = "";
        Program.Pass = "";
      }
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      Login login = new Login();
      login.LoginUsername.Text = Program.User;
      login.LoginPassword.Text = Program.Pass;
      login.SQLServer.Text = Program.Host;
      login.SQLPort.Text = Program.Port;
      int num1 = (int) login.ShowDialog();
      DB.SetLogin(Program.Host, Program.Port, Program.DataBase, Program.User, Program.Pass);
      if (!login.m_Cancel && login.m_HasChanged)
      {
        DB.SetLogin(login.SQLServer.Text, login.SQLPort.Text, Program.DataBase, login.LoginUsername.Text, login.LoginPassword.Text);
        XmlTextWriter xmlTextWriter = new XmlTextWriter(Application.StartupPath + "\\Config.xml", (Encoding) null);
        xmlTextWriter.Formatting = Formatting.Indented;
        xmlTextWriter.WriteStartDocument();
        xmlTextWriter.WriteStartElement("Confg");
        xmlTextWriter.WriteStartElement("Host");
        xmlTextWriter.WriteString(login.SQLServer.Text);
        xmlTextWriter.WriteEndElement();
        xmlTextWriter.WriteStartElement("Port");
        xmlTextWriter.WriteString(login.SQLPort.Text);
        xmlTextWriter.WriteEndElement();
        xmlTextWriter.WriteStartElement("User");
        xmlTextWriter.WriteString(login.LoginUsername.Text);
        xmlTextWriter.WriteEndElement();
        xmlTextWriter.WriteStartElement("Pass");
        xmlTextWriter.WriteString(login.LoginPassword.Text);
        xmlTextWriter.WriteEndElement();
        xmlTextWriter.WriteEndElement();
        xmlTextWriter.WriteEndDocument();
        xmlTextWriter.Close();
      }
      login.Close();
      if (login.m_Cancel)
        return;
      Application.Run((Form) new Form1());
    }
  }
}
