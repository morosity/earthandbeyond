﻿// Decompiled with JetBrains decompiler
// Type: SQLBind.Tools.CodeValue
// Assembly: SQLBind, Version=1.0.0.1, Culture=neutral, PublicKeyToken=null
// MVID: 8DC05ED6-E010-4553-A0DD-607EBD42D93A
// Assembly location: D:\Server\eab\Tools\Effect Editor.exe

namespace SQLBind.Tools
{
  public class CodeValue
  {
    public int code;
    public string value;

    public CodeValue()
    {
      this.code = 0;
      this.value = "";
    }

    public CodeValue(int code)
    {
      this.code = code;
      this.value = "";
    }

    public CodeValue(int code, string value)
    {
      this.code = code;
      this.value = value;
    }

    public static CodeValue Formatted(int code, string value)
    {
      string str = value + " (" + code.ToString() + ")";
      return new CodeValue(code, str);
    }

    public override string ToString()
    {
      return this.value;
    }

    public override bool Equals(object obj)
    {
      if (obj.GetType() != typeof (CodeValue))
        return false;
      return this.code.Equals(((CodeValue) obj).code);
    }

    public override int GetHashCode()
    {
      return base.GetHashCode();
    }
  }
}
