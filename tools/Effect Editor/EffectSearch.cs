﻿// Decompiled with JetBrains decompiler
// Type: SQLBind.EffectSearch
// Assembly: SQLBind, Version=1.0.0.1, Culture=neutral, PublicKeyToken=null
// MVID: 8DC05ED6-E010-4553-A0DD-607EBD42D93A
// Assembly location: D:\Server\eab\Tools\Effect Editor.exe

using MySql.Data.MySqlClient;
using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace SQLBind
{
  public class EffectSearch : Form
  {
    private int IDEffect = -1;
    private IContainer components;
    private DataGridView EffectList;
    private Button OkBt;
    private Button CancelBt;
    private ContextMenu popUpMenu;
    private int m_ClickRow;
    private MySqlConnection m_Conn;
    private bool m_Ok;

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.EffectList = new DataGridView();
      this.OkBt = new Button();
      this.CancelBt = new Button();
      ((ISupportInitialize) this.EffectList).BeginInit();
      this.SuspendLayout();
      this.EffectList.AllowUserToAddRows = false;
      this.EffectList.AllowUserToDeleteRows = false;
      this.EffectList.AllowUserToOrderColumns = true;
      this.EffectList.Anchor = AnchorStyles.Top | AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right;
      this.EffectList.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.AutoSize;
      this.EffectList.EditMode = DataGridViewEditMode.EditProgrammatically;
      this.EffectList.Location = new Point(160, 11);
      this.EffectList.MultiSelect = false;
      this.EffectList.Name = "EffectList";
      this.EffectList.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
      this.EffectList.Size = new Size(439, 254);
      this.EffectList.TabIndex = 0;
      this.EffectList.CellMouseClick += new DataGridViewCellMouseEventHandler(this.EffectList_CellMouseClick);
      this.OkBt.Location = new Point(12, 181);
      this.OkBt.Name = "OkBt";
      this.OkBt.Size = new Size(101, 39);
      this.OkBt.TabIndex = 1;
      this.OkBt.Text = "Ok";
      this.OkBt.UseVisualStyleBackColor = true;
      this.OkBt.Click += new EventHandler(this.OkBt_Click);
      this.CancelBt.Location = new Point(12, 226);
      this.CancelBt.Name = "CancelBt";
      this.CancelBt.Size = new Size(101, 39);
      this.CancelBt.TabIndex = 2;
      this.CancelBt.Text = "Cancel";
      this.CancelBt.UseVisualStyleBackColor = true;
      this.CancelBt.Click += new EventHandler(this.CancelBt_Click);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(611, 277);
      this.Controls.Add((Control) this.CancelBt);
      this.Controls.Add((Control) this.OkBt);
      this.Controls.Add((Control) this.EffectList);
      this.Name = nameof (EffectSearch);
      this.Text = nameof (EffectSearch);
      this.Load += new EventHandler(this.EffectSearch_Load);
      ((ISupportInitialize) this.EffectList).EndInit();
      this.ResumeLayout(false);
    }

    public EffectSearch()
    {
      this.InitializeComponent();
    }

    private void EffectSearch_Load(object sender, EventArgs e)
    {
      this.m_Conn = DB.GetConnection();
      DataTable dataTable = new DataTable();
      new MySqlDataAdapter("SELECT `EffectID`,`item_effect_base`.`Description`,`item_effect_base`.`Tooltip` FROM `item_effect_base`", this.m_Conn).Fill(dataTable);
      this.EffectList.DataSource = (object) dataTable;
    }

    public int GetEffectID()
    {
      this.IDEffect = int.Parse(this.EffectList.SelectedRows[0].Cells[0].Value.ToString());
      if (!this.m_Ok)
        return -1;
      return this.IDEffect;
    }

    private void OkBt_Click(object sender, EventArgs e)
    {
      this.m_Ok = true;
      this.Close();
    }

    private void CancelBt_Click(object sender, EventArgs e)
    {
      this.Close();
    }

    private void EffectList_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
    {
      if (e.Button != MouseButtons.Right)
        return;
      this.m_ClickRow = e.RowIndex;
      this.popUpMenu = new ContextMenu();
      try
      {
        this.ContextMenu.Dispose();
      }
      catch
      {
      }
      this.popUpMenu.MenuItems.Add("Delete Effect", new EventHandler(this.PopUpMenuDelete));
      this.ContextMenu = this.popUpMenu;
    }

    private void PopUpMenuDelete(object sender, EventArgs e)
    {
      if (((MenuItem) sender).Text == "Delete Effect" && MessageBox.Show("Are you sure you want to delete '" + this.EffectList.Rows[this.m_ClickRow].Cells[1].Value.ToString() + "'?", "Delete Conformation", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
      {
        DataTable dataTable = new DataTable();
        new MySqlDataAdapter("DELETE FROM `item_effect_base` WHERE `EffectID` = '" + this.EffectList.Rows[this.m_ClickRow].Cells[0].Value.ToString() + "';", this.m_Conn).Fill(dataTable);
        new MySqlDataAdapter("SELECT `EffectID`,`item_effect_base`.`Description`,`item_effect_base`.`Tooltip` FROM `item_effect_base`", this.m_Conn).Fill(dataTable);
        this.EffectList.DataSource = (object) dataTable;
      }
      this.ContextMenu.Dispose();
    }
  }
}
