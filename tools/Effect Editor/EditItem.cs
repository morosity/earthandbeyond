﻿// Decompiled with JetBrains decompiler
// Type: SQLBind.EditItem
// Assembly: SQLBind, Version=1.0.0.1, Culture=neutral, PublicKeyToken=null
// MVID: 8DC05ED6-E010-4553-A0DD-607EBD42D93A
// Assembly location: D:\Server\eab\Tools\Effect Editor.exe

using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace SQLBind
{
  public class EditItem : Form
  {
    private string[] OutputStr = new string[5];
    private List<EditItem.EffectList> m_EffectList = new List<EditItem.EffectList>();
    private List<int> m_EffectIDNum = new List<int>();
    private List<EffectComboHandel> m_BoxList = new List<EffectComboHandel>();
    private ItemBrowse ItemBrowser;
    private int m_CurrentItem;
    private int m_CurrentContainer;
    private IContainer components;
    private Label label1;
    private TextBox ItemIDBox;
    private Button BrowseItems;
    private GroupBox groupBox1;
    private Button LoadItem;
    private Label label2;
    private ComboBox Effect1;
    private TextBox Effect1Var1;
    private Label Var1Lable;
    private Label Var2Lable;
    private TextBox Effect1Var2;
    private Label Var3Lable;
    private TextBox Effect1Var3;
    private Label label6;
    private Label Effect1String;
    private Label label7;
    private Label ItemName;
    private Label Effect2String;
    private Label label4;
    private Label label5;
    private TextBox Effect2Var3;
    private Label label8;
    private TextBox Effect2Var2;
    private Label label9;
    private TextBox Effect2Var1;
    private ComboBox Effect2;
    private Label label10;
    private Label Effect3String;
    private Label label12;
    private Label label13;
    private TextBox Effect3Var3;
    private Label label14;
    private TextBox Effect3Var2;
    private Label label15;
    private TextBox Effect3Var1;
    private ComboBox Effect3;
    private Label label16;
    private Label Effect4String;
    private Label label18;
    private Label label19;
    private TextBox Effect4Var3;
    private Label label20;
    private TextBox Effect4Var2;
    private Label label21;
    private TextBox Effect4Var1;
    private ComboBox Effect4;
    private Label label22;
    private Label Effect5String;
    private Label label24;
    private Label label25;
    private TextBox Effect5Var3;
    private Label label26;
    private TextBox Effect5Var2;
    private Label label27;
    private TextBox Effect5Var1;
    private ComboBox Effect5;
    private Label label28;
    private Label label3;
    private GroupBox groupBox2;
    private TextBox EnergyUse;
    private Label label17;
    private TextBox CoolDown;
    private Label label11;
    private TextBox Range;
    private Button Save;

    private MySqlDataReader ExcuteSQLQuery(string SQLD)
    {
      MySqlConnection connection = new MySqlConnection(DB.GetLoginStr());
      MySqlCommand mySqlCommand = new MySqlCommand(SQLD, connection);
      MySqlDataReader mySqlDataReader = (MySqlDataReader) null;
      connection.Open();
      try
      {
        mySqlDataReader = mySqlCommand.ExecuteReader();
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("Error executing mysql: " + SQLD, ex.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Hand);
      }
      return mySqlDataReader;
    }

    private int ExecuteSQL(string SQLD)
    {
      string cmdText = "SELECT LAST_INSERT_ID()";
      MySqlConnection connection = new MySqlConnection(DB.GetLoginStr());
      MySqlCommand mySqlCommand1 = new MySqlCommand(SQLD, connection);
      MySqlCommand mySqlCommand2 = new MySqlCommand(cmdText, connection);
      int int32;
      try
      {
        connection.Open();
        mySqlCommand1.ExecuteNonQuery();
        MySqlDataReader mySqlDataReader = mySqlCommand2.ExecuteReader();
        mySqlDataReader.Read();
        int32 = mySqlDataReader.GetInt32(0);
        connection.Close();
      }
      catch (Exception ex)
      {
        int num = (int) MessageBox.Show("SQL Error: " + ex.Message, "SQL Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        return -1;
      }
      return int32;
    }

    public EditItem()
    {
      this.InitializeComponent();
      this.ItemBrowser = new ItemBrowse();
      this.LoadEffects();
      this.ManageEffectBox(this.Effect1, this.Effect1String, this.Effect1Var1, this.Effect1Var2, this.Effect1Var3);
      this.ManageEffectBox(this.Effect2, this.Effect2String, this.Effect2Var1, this.Effect2Var2, this.Effect2Var3);
      this.ManageEffectBox(this.Effect3, this.Effect3String, this.Effect3Var1, this.Effect3Var2, this.Effect3Var3);
      this.ManageEffectBox(this.Effect4, this.Effect4String, this.Effect4Var1, this.Effect4Var2, this.Effect4Var3);
      this.ManageEffectBox(this.Effect5, this.Effect5String, this.Effect5Var1, this.Effect5Var2, this.Effect5Var3);
      this.FillComboBoxs();
      this.EnableEffects(false);
    }

    private void EnableEffects(bool Enable)
    {
      for (int index = 0; index < this.m_BoxList.Count; ++index)
        this.m_BoxList[index].EnableFields(Enable);
    }

    private void FillComboBoxs()
    {
      for (int index = 0; index < this.m_BoxList.Count; ++index)
        this.m_BoxList[index].EffectBox.Items.AddRange((object[]) this.GetEffectList());
    }

    private void ManageEffectBox(ComboBox cbo, Label effString, TextBox Var1, TextBox Var2, TextBox Var3)
    {
      this.m_BoxList.Add(new EffectComboHandel()
      {
        EffectBox = cbo,
        EffectString = effString,
        Var1 = Var1,
        Var2 = Var2,
        Var3 = Var3
      });
      cbo.DropDownStyle = ComboBoxStyle.DropDownList;
      Var1.TextChanged += new EventHandler(this.VarChanged);
      Var2.TextChanged += new EventHandler(this.VarChanged);
      Var3.TextChanged += new EventHandler(this.VarChanged);
      cbo.SelectedIndexChanged += new EventHandler(this.EffectChange);
    }

    private void DisplayString(int Index)
    {
      this.m_BoxList[Index].Var1.Text = this.m_BoxList[Index].Var1.Text == "" ? "0" : this.m_BoxList[Index].Var1.Text;
      this.m_BoxList[Index].Var2.Text = this.m_BoxList[Index].Var2.Text == "" ? "0" : this.m_BoxList[Index].Var2.Text;
      this.m_BoxList[Index].Var3.Text = this.m_BoxList[Index].Var3.Text == "" ? "0" : this.m_BoxList[Index].Var3.Text;
      float num1 = float.Parse(this.m_BoxList[Index].Var1.Text);
      float num2 = float.Parse(this.m_BoxList[Index].Var2.Text);
      float num3 = float.Parse(this.m_BoxList[Index].Var3.Text);
      string effectToolList = this.m_EffectList[this.m_BoxList[Index].EffectBox.SelectedIndex].EffectToolList;
      this.OutputStr[Index] = "";
      int num4 = 0;
      for (int startIndex1 = 0; startIndex1 < effectToolList.Length; ++startIndex1)
      {
        string str = "";
        if (startIndex1 + 6 <= effectToolList.Length && "%value" == effectToolList.Substring(startIndex1, 6))
        {
          int startIndex2 = startIndex1 + 6;
          for (int startIndex3 = startIndex2; startIndex3 < effectToolList.Length - 1; ++startIndex3)
          {
            if (effectToolList.Substring(startIndex3, 2) == "f%")
            {
              str = effectToolList.Substring(startIndex2, startIndex3 - startIndex2);
              startIndex2 += startIndex3 - startIndex2;
              break;
            }
          }
          for (int index1 = 0; index1 < str.Length; ++index1)
          {
            if (str.Substring(index1, 1) == ".")
            {
              string[] outputStr1;
              IntPtr index2;
              (outputStr1 = this.OutputStr)[(int) (index2 = (IntPtr) Index)] = outputStr1[index2] + "{" + num4.ToString() + ":";
              int num5 = int.Parse(str.Substring(0, index1));
              int num6 = int.Parse(str.Substring(index1 + 1, str.Length - index1 - 1));
              for (int index3 = 0; index3 < num5; ++index3)
              {
                string[] outputStr2;
                IntPtr index4;
                (outputStr2 = this.OutputStr)[(int) (index4 = (IntPtr) Index)] = outputStr2[index4] + "0";
              }
              string[] outputStr3;
              IntPtr index5;
              (outputStr3 = this.OutputStr)[(int) (index5 = (IntPtr) Index)] = outputStr3[index5] + ".";
              for (int index3 = 0; index3 < num6; ++index3)
              {
                string[] outputStr2;
                IntPtr index4;
                (outputStr2 = this.OutputStr)[(int) (index4 = (IntPtr) Index)] = outputStr2[index4] + "0";
              }
              break;
            }
          }
          string[] outputStr;
          IntPtr index;
          (outputStr = this.OutputStr)[(int) (index = (IntPtr) Index)] = outputStr[index] + "}";
          startIndex1 = startIndex2 + 2;
          ++num4;
        }
        else
        {
          string[] outputStr;
          IntPtr index;
          (outputStr = this.OutputStr)[(int) (index = (IntPtr) Index)] = outputStr[index] + effectToolList.Substring(startIndex1, 1);
        }
      }
      if (num4 > 0)
        this.m_BoxList[Index].Var1.Enabled = true;
      else
        this.m_BoxList[Index].Var1.Enabled = false;
      if (num4 > 1)
        this.m_BoxList[Index].Var2.Enabled = true;
      else
        this.m_BoxList[Index].Var2.Enabled = false;
      if (num4 > 2)
        this.m_BoxList[Index].Var3.Enabled = true;
      else
        this.m_BoxList[Index].Var3.Enabled = false;
      this.m_BoxList[Index].EffectString.Text = string.Format(this.OutputStr[Index], (object) num1, (object) num2, (object) num3);
    }

    private void EffectChange(object sender, EventArgs e)
    {
      ComboBox comboBox = (ComboBox) sender;
      for (int Index = 0; Index < this.m_BoxList.Count; ++Index)
      {
        if (this.m_BoxList[Index].EffectBox.Name == comboBox.Name)
        {
          if (this.m_EffectList[this.m_BoxList[Index].EffectBox.SelectedIndex].EffectID == -1)
          {
            this.m_BoxList[Index].Var1.Enabled = false;
            this.m_BoxList[Index].Var2.Enabled = false;
            this.m_BoxList[Index].Var3.Enabled = false;
          }
          this.DisplayString(Index);
        }
      }
    }

    private void VarChanged(object sender, EventArgs e)
    {
      TextBox textBox = (TextBox) sender;
      for (int index = 0; index < this.m_BoxList.Count; ++index)
      {
        if (this.m_BoxList[index].Var1.Name == textBox.Name || this.m_BoxList[index].Var2.Name == textBox.Name || this.m_BoxList[index].Var3.Name == textBox.Name)
        {
          if (this.OutputStr[index] == null)
            break;
          this.m_BoxList[index].Var1.Text = this.m_BoxList[index].Var1.Text == "" ? "0" : this.m_BoxList[index].Var1.Text;
          this.m_BoxList[index].Var2.Text = this.m_BoxList[index].Var2.Text == "" ? "0" : this.m_BoxList[index].Var2.Text;
          this.m_BoxList[index].Var3.Text = this.m_BoxList[index].Var3.Text == "" ? "0" : this.m_BoxList[index].Var3.Text;
          float num1 = float.Parse(this.m_BoxList[index].Var1.Text);
          float num2 = float.Parse(this.m_BoxList[index].Var2.Text);
          float num3 = float.Parse(this.m_BoxList[index].Var3.Text);
          this.m_BoxList[index].EffectString.Text = string.Format(this.OutputStr[index], (object) num1, (object) num2, (object) num3);
        }
      }
    }

    private string[] GetEffectList()
    {
      List<string> stringList = new List<string>();
      foreach (EditItem.EffectList effect in this.m_EffectList)
        stringList.Add(effect.EffectDescList);
      return stringList.ToArray();
    }

    private void LoadEffects()
    {
      DataTable dataTable = new DataTable();
      new MySqlDataAdapter("SELECT Description,Tooltip,Constant1Value,Constant2Value,EffectID FROM item_effect_base", new MySqlConnection(DB.GetLoginStr())).Fill(dataTable);
      foreach (DataRow row in (InternalDataCollectionBase) dataTable.Rows)
        this.m_EffectList.Add(new EditItem.EffectList()
        {
          EffectDescList = row["Description"].ToString(),
          EffectID = (int) row["EffectID"],
          EffectToolList = row["Tooltip"].ToString()
        });
    }

    private int FindEffectID(int EffectID)
    {
      int num = 0;
      foreach (EditItem.EffectList effect in this.m_EffectList)
      {
        if (effect.EffectID == EffectID)
          return num;
        ++num;
      }
      return -1;
    }

    private void BrowseItems_Click(object sender, EventArgs e)
    {
      int num = (int) this.ItemBrowser.ShowDialog();
      if (this.ItemBrowser.GetItemBase() == 0)
        return;
      this.m_CurrentItem = this.ItemBrowser.GetItemBase();
      this.ItemIDBox.Text = this.m_CurrentItem.ToString();
      this.LoadItemData();
    }

    private void LoadItem_Click(object sender, EventArgs e)
    {
      this.m_CurrentItem = int.Parse(this.ItemIDBox.Text);
      this.LoadItemData();
    }

    private void SaveEffect(int EffectNum)
    {
      if (this.m_EffectIDNum.Count == 0)
        return;
      string SQLD;
      if (this.m_EffectIDNum[EffectNum] != 0 && this.m_BoxList[EffectNum].EffectBox.SelectedIndex == 0)
      {
        SQLD = "DELETE FROM `item_effects` WHERE `ItemEffectID` = '" + (object) this.m_EffectIDNum[EffectNum] + "'";
        this.m_EffectIDNum[EffectNum] = 0;
      }
      else
      {
        if (this.m_BoxList[EffectNum].EffectBox.SelectedIndex == 0 || this.m_EffectIDNum[EffectNum] == this.m_BoxList[EffectNum].EffectBox.SelectedIndex)
          return;
        string str;
        if (this.m_EffectIDNum[EffectNum] == 0)
          str = "INSERT INTO `item_effects` SET " + "`ItemID` = '" + (object) this.m_CurrentItem + "',";
        else
          str = "UPDATE `item_effects` SET ";
        this.m_BoxList[EffectNum].Var1.Text = this.m_BoxList[EffectNum].Var1.Text == "" ? "0" : this.m_BoxList[EffectNum].Var1.Text;
        this.m_BoxList[EffectNum].Var2.Text = this.m_BoxList[EffectNum].Var2.Text == "" ? "0" : this.m_BoxList[EffectNum].Var2.Text;
        this.m_BoxList[EffectNum].Var3.Text = this.m_BoxList[EffectNum].Var3.Text == "" ? "0" : this.m_BoxList[EffectNum].Var3.Text;
        SQLD = str + "`item_effect_base_id` = '" + (object) this.m_EffectList[this.m_BoxList[EffectNum].EffectBox.SelectedIndex].EffectID + "'," + "`Var1Data` = '" + this.m_BoxList[EffectNum].Var1.Text + "'," + "`Var2Data` = '" + this.m_BoxList[EffectNum].Var2.Text + "'," + "`Var3Data` = '" + this.m_BoxList[EffectNum].Var3.Text + "' ";
        if (this.m_EffectIDNum[EffectNum] != 0)
          SQLD = SQLD + "WHERE `ItemID` = '" + (object) this.m_CurrentItem + "' AND `ItemEffectID` = '" + (object) this.m_EffectIDNum[EffectNum] + "'";
      }
      int num = this.ExecuteSQL(SQLD);
      if (this.m_EffectIDNum[EffectNum] != 0)
        return;
      this.m_EffectIDNum[EffectNum] = num;
    }

    private void SaveItemData()
    {
      if (this.m_EffectIDNum.Count == 0)
        return;
      for (int EffectNum = 0; EffectNum < 5; ++EffectNum)
        this.SaveEffect(EffectNum);
      string str;
      if (this.m_CurrentContainer == 0)
        str = "INSERT INTO `item_effect_container` SET " + "`ItemID` = '" + (object) this.m_CurrentItem + "', ";
      else
        str = "UPDATE `item_effect_container` SET ";
      string SQLD = str + "`RechargeTime` = '" + this.CoolDown.Text + "', " + "`EnergyUse` = '" + this.EnergyUse.Text + "', " + "`_Range` = '" + this.Range.Text + "', " + "`EquipEffect` = '1'";
      if (this.m_CurrentContainer != 0)
        SQLD = SQLD + " WHERE `ItemID` = '" + (object) this.m_CurrentItem + "' AND `EffectContainerID` = '" + (object) this.m_CurrentContainer + "'";
      int num = this.ExecuteSQL(SQLD);
      if (this.m_CurrentContainer != 0)
        return;
      this.m_CurrentContainer = num;
    }

    private void LoadItemData()
    {
      MySqlDataReader mySqlDataReader = this.ExcuteSQLQuery("SELECT name FROM item_base WHERE id = '" + (object) this.m_CurrentItem + "'");
      if (mySqlDataReader.HasRows)
      {
        mySqlDataReader.Read();
        this.ItemName.Text = mySqlDataReader.GetString(0);
      }
      this.m_EffectIDNum.Clear();
      for (int index = 0; index < 5; ++index)
        this.m_BoxList[index].EffectBox.SelectedIndex = 0;
      DataTable dataTable1 = new DataTable();
      new MySqlDataAdapter("SELECT ItemEffectID, item_effect_base_id, Var1Data, Var2Data, Var3Data FROM item_effects WHERE ItemID = '" + (object) this.m_CurrentItem + "'", new MySqlConnection(DB.GetLoginStr())).Fill(dataTable1);
      int index1 = 0;
      foreach (DataRow row in (InternalDataCollectionBase) dataTable1.Rows)
      {
        this.m_BoxList[index1].EffectBox.Enabled = true;
        this.m_EffectIDNum.Add((int) row["ItemEffectID"]);
        int EffectID = (int) row["item_effect_base_id"];
        this.m_BoxList[index1].EffectBox.SelectedIndex = this.FindEffectID(EffectID);
        this.m_BoxList[index1].Var1.Text = row["Var1Data"].ToString();
        this.m_BoxList[index1].Var2.Text = row["Var2Data"].ToString();
        this.m_BoxList[index1].Var3.Text = row["Var3Data"].ToString();
        ++index1;
        if (index1 >= this.m_BoxList.Count)
          break;
      }
      for (; index1 < 5; ++index1)
      {
        this.m_BoxList[index1].EffectBox.Enabled = true;
        this.m_EffectIDNum.Add(0);
      }
      this.m_CurrentContainer = 0;
      DataTable dataTable2 = new DataTable();
      new MySqlDataAdapter("SELECT EffectContainerID,RechargeTime,Unknown2,_Range,Unknown4, EnergyUse FROM Item_effect_container WHERE EquipEffect = '1' AND ItemID = '" + (object) this.m_CurrentItem + "'", new MySqlConnection(DB.GetLoginStr())).Fill(dataTable2);
      if (dataTable2.Rows.Count > 0)
      {
        DataRow row = dataTable2.Rows[0];
        this.m_CurrentContainer = (int) row["EffectContainerID"];
        this.Range.Text = row["_Range"].ToString();
        this.CoolDown.Text = row["RechargeTime"].ToString();
        this.EnergyUse.Text = row["EnergyUse"].ToString();
      }
      else
      {
        this.Range.Text = "0";
        this.CoolDown.Text = "0";
        this.EnergyUse.Text = "0";
      }
    }

    private void Save_Click(object sender, EventArgs e)
    {
      this.SaveItemData();
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing && this.components != null)
        this.components.Dispose();
      base.Dispose(disposing);
    }

    private void InitializeComponent()
    {
      this.label1 = new Label();
      this.ItemIDBox = new TextBox();
      this.BrowseItems = new Button();
      this.groupBox1 = new GroupBox();
      this.ItemName = new Label();
      this.LoadItem = new Button();
      this.label7 = new Label();
      this.label2 = new Label();
      this.Effect1 = new ComboBox();
      this.Effect1Var1 = new TextBox();
      this.Var1Lable = new Label();
      this.Var2Lable = new Label();
      this.Effect1Var2 = new TextBox();
      this.Var3Lable = new Label();
      this.Effect1Var3 = new TextBox();
      this.label6 = new Label();
      this.Effect1String = new Label();
      this.Effect2String = new Label();
      this.label4 = new Label();
      this.label5 = new Label();
      this.Effect2Var3 = new TextBox();
      this.label8 = new Label();
      this.Effect2Var2 = new TextBox();
      this.label9 = new Label();
      this.Effect2Var1 = new TextBox();
      this.Effect2 = new ComboBox();
      this.label10 = new Label();
      this.Effect3String = new Label();
      this.label12 = new Label();
      this.label13 = new Label();
      this.Effect3Var3 = new TextBox();
      this.label14 = new Label();
      this.Effect3Var2 = new TextBox();
      this.label15 = new Label();
      this.Effect3Var1 = new TextBox();
      this.Effect3 = new ComboBox();
      this.label16 = new Label();
      this.Effect4String = new Label();
      this.label18 = new Label();
      this.label19 = new Label();
      this.Effect4Var3 = new TextBox();
      this.label20 = new Label();
      this.Effect4Var2 = new TextBox();
      this.label21 = new Label();
      this.Effect4Var1 = new TextBox();
      this.Effect4 = new ComboBox();
      this.label22 = new Label();
      this.Effect5String = new Label();
      this.label24 = new Label();
      this.label25 = new Label();
      this.Effect5Var3 = new TextBox();
      this.label26 = new Label();
      this.Effect5Var2 = new TextBox();
      this.label27 = new Label();
      this.Effect5Var1 = new TextBox();
      this.Effect5 = new ComboBox();
      this.label28 = new Label();
      this.label3 = new Label();
      this.groupBox2 = new GroupBox();
      this.EnergyUse = new TextBox();
      this.label17 = new Label();
      this.CoolDown = new TextBox();
      this.label11 = new Label();
      this.Range = new TextBox();
      this.Save = new Button();
      this.groupBox1.SuspendLayout();
      this.groupBox2.SuspendLayout();
      this.SuspendLayout();
      this.label1.Location = new Point(6, 22);
      this.label1.Name = "label1";
      this.label1.Size = new Size(46, 17);
      this.label1.TabIndex = 4;
      this.label1.Text = "ItemID:";
      this.ItemIDBox.Location = new Point(58, 19);
      this.ItemIDBox.Name = "ItemIDBox";
      this.ItemIDBox.Size = new Size(137, 20);
      this.ItemIDBox.TabIndex = 1;
      this.BrowseItems.Location = new Point(201, 19);
      this.BrowseItems.Name = "BrowseItems";
      this.BrowseItems.Size = new Size(58, 20);
      this.BrowseItems.TabIndex = 2;
      this.BrowseItems.Text = "Browse...";
      this.BrowseItems.UseVisualStyleBackColor = true;
      this.BrowseItems.Click += new EventHandler(this.BrowseItems_Click);
      this.groupBox1.Controls.Add((Control) this.ItemName);
      this.groupBox1.Controls.Add((Control) this.LoadItem);
      this.groupBox1.Controls.Add((Control) this.label7);
      this.groupBox1.Controls.Add((Control) this.BrowseItems);
      this.groupBox1.Controls.Add((Control) this.ItemIDBox);
      this.groupBox1.Controls.Add((Control) this.label1);
      this.groupBox1.Location = new Point(12, 12);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new Size(502, 80);
      this.groupBox1.TabIndex = 3;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Browse Items";
      this.ItemName.AutoSize = true;
      this.ItemName.Location = new Point(73, 53);
      this.ItemName.Name = "ItemName";
      this.ItemName.Size = new Size(0, 13);
      this.ItemName.TabIndex = 15;
      this.LoadItem.Location = new Point(265, 20);
      this.LoadItem.Name = "LoadItem";
      this.LoadItem.Size = new Size(70, 19);
      this.LoadItem.TabIndex = 3;
      this.LoadItem.Text = "Load Item";
      this.LoadItem.UseVisualStyleBackColor = true;
      this.LoadItem.Click += new EventHandler(this.LoadItem_Click);
      this.label7.AutoSize = true;
      this.label7.Location = new Point(6, 53);
      this.label7.Name = "label7";
      this.label7.Size = new Size(58, 13);
      this.label7.TabIndex = 14;
      this.label7.Text = "ItemName:";
      this.label2.AutoSize = true;
      this.label2.Location = new Point(17, 117);
      this.label2.Name = "label2";
      this.label2.Size = new Size(47, 13);
      this.label2.TabIndex = 4;
      this.label2.Text = "Effect 1:";
      this.Effect1.DropDownStyle = ComboBoxStyle.DropDownList;
      this.Effect1.FormattingEnabled = true;
      this.Effect1.Location = new Point(70, 114);
      this.Effect1.Name = "Effect1";
      this.Effect1.Size = new Size(263, 21);
      this.Effect1.TabIndex = 5;
      this.Effect1Var1.Location = new Point(70, 141);
      this.Effect1Var1.Name = "Effect1Var1";
      this.Effect1Var1.Size = new Size(56, 20);
      this.Effect1Var1.TabIndex = 6;
      this.Var1Lable.AutoSize = true;
      this.Var1Lable.Location = new Point(29, 144);
      this.Var1Lable.Name = "Var1Lable";
      this.Var1Lable.Size = new Size(35, 13);
      this.Var1Lable.TabIndex = 7;
      this.Var1Lable.Text = "Var 1:";
      this.Var2Lable.AutoSize = true;
      this.Var2Lable.Location = new Point(132, 144);
      this.Var2Lable.Name = "Var2Lable";
      this.Var2Lable.Size = new Size(35, 13);
      this.Var2Lable.TabIndex = 9;
      this.Var2Lable.Text = "Var 2:";
      this.Effect1Var2.Location = new Point(173, 141);
      this.Effect1Var2.Name = "Effect1Var2";
      this.Effect1Var2.Size = new Size(56, 20);
      this.Effect1Var2.TabIndex = 8;
      this.Var3Lable.AutoSize = true;
      this.Var3Lable.Location = new Point(236, 144);
      this.Var3Lable.Name = "Var3Lable";
      this.Var3Lable.Size = new Size(35, 13);
      this.Var3Lable.TabIndex = 11;
      this.Var3Lable.Text = "Var 3:";
      this.Effect1Var3.Location = new Point(277, 141);
      this.Effect1Var3.Name = "Effect1Var3";
      this.Effect1Var3.Size = new Size(56, 20);
      this.Effect1Var3.TabIndex = 10;
      this.label6.AutoSize = true;
      this.label6.Location = new Point(2, 164);
      this.label6.Name = "label6";
      this.label6.Size = new Size(46, 13);
      this.label6.TabIndex = 12;
      this.label6.Text = "ToolTip:";
      this.Effect1String.AutoSize = true;
      this.Effect1String.Location = new Point(54, 164);
      this.Effect1String.Name = "Effect1String";
      this.Effect1String.Size = new Size(57, 13);
      this.Effect1String.TabIndex = 13;
      this.Effect1String.Text = "StringHere";
      this.Effect2String.AutoSize = true;
      this.Effect2String.Location = new Point(54, 235);
      this.Effect2String.Name = "Effect2String";
      this.Effect2String.Size = new Size(57, 13);
      this.Effect2String.TabIndex = 23;
      this.Effect2String.Text = "StringHere";
      this.label4.AutoSize = true;
      this.label4.Location = new Point(2, 235);
      this.label4.Name = "label4";
      this.label4.Size = new Size(46, 13);
      this.label4.TabIndex = 22;
      this.label4.Text = "ToolTip:";
      this.label5.AutoSize = true;
      this.label5.Location = new Point(236, 215);
      this.label5.Name = "label5";
      this.label5.Size = new Size(35, 13);
      this.label5.TabIndex = 21;
      this.label5.Text = "Var 3:";
      this.Effect2Var3.Location = new Point(277, 212);
      this.Effect2Var3.Name = "Effect2Var3";
      this.Effect2Var3.Size = new Size(56, 20);
      this.Effect2Var3.TabIndex = 20;
      this.label8.AutoSize = true;
      this.label8.Location = new Point(132, 215);
      this.label8.Name = "label8";
      this.label8.Size = new Size(35, 13);
      this.label8.TabIndex = 19;
      this.label8.Text = "Var 2:";
      this.Effect2Var2.Location = new Point(173, 212);
      this.Effect2Var2.Name = "Effect2Var2";
      this.Effect2Var2.Size = new Size(56, 20);
      this.Effect2Var2.TabIndex = 18;
      this.label9.AutoSize = true;
      this.label9.Location = new Point(29, 215);
      this.label9.Name = "label9";
      this.label9.Size = new Size(35, 13);
      this.label9.TabIndex = 17;
      this.label9.Text = "Var 1:";
      this.Effect2Var1.Location = new Point(70, 212);
      this.Effect2Var1.Name = "Effect2Var1";
      this.Effect2Var1.Size = new Size(56, 20);
      this.Effect2Var1.TabIndex = 16;
      this.Effect2.DropDownStyle = ComboBoxStyle.DropDownList;
      this.Effect2.FormattingEnabled = true;
      this.Effect2.Location = new Point(70, 185);
      this.Effect2.Name = "Effect2";
      this.Effect2.Size = new Size(263, 21);
      this.Effect2.TabIndex = 15;
      this.label10.AutoSize = true;
      this.label10.Location = new Point(17, 188);
      this.label10.Name = "label10";
      this.label10.Size = new Size(47, 13);
      this.label10.TabIndex = 14;
      this.label10.Text = "Effect 2:";
      this.Effect3String.AutoSize = true;
      this.Effect3String.Location = new Point(54, 312);
      this.Effect3String.Name = "Effect3String";
      this.Effect3String.Size = new Size(57, 13);
      this.Effect3String.TabIndex = 33;
      this.Effect3String.Text = "StringHere";
      this.label12.AutoSize = true;
      this.label12.Location = new Point(2, 312);
      this.label12.Name = "label12";
      this.label12.Size = new Size(46, 13);
      this.label12.TabIndex = 32;
      this.label12.Text = "ToolTip:";
      this.label13.AutoSize = true;
      this.label13.Location = new Point(236, 292);
      this.label13.Name = "label13";
      this.label13.Size = new Size(35, 13);
      this.label13.TabIndex = 31;
      this.label13.Text = "Var 3:";
      this.Effect3Var3.Location = new Point(277, 289);
      this.Effect3Var3.Name = "Effect3Var3";
      this.Effect3Var3.Size = new Size(56, 20);
      this.Effect3Var3.TabIndex = 30;
      this.label14.AutoSize = true;
      this.label14.Location = new Point(132, 292);
      this.label14.Name = "label14";
      this.label14.Size = new Size(35, 13);
      this.label14.TabIndex = 29;
      this.label14.Text = "Var 2:";
      this.Effect3Var2.Location = new Point(173, 289);
      this.Effect3Var2.Name = "Effect3Var2";
      this.Effect3Var2.Size = new Size(56, 20);
      this.Effect3Var2.TabIndex = 28;
      this.label15.AutoSize = true;
      this.label15.Location = new Point(29, 292);
      this.label15.Name = "label15";
      this.label15.Size = new Size(35, 13);
      this.label15.TabIndex = 27;
      this.label15.Text = "Var 1:";
      this.Effect3Var1.Location = new Point(70, 289);
      this.Effect3Var1.Name = "Effect3Var1";
      this.Effect3Var1.Size = new Size(56, 20);
      this.Effect3Var1.TabIndex = 26;
      this.Effect3.DropDownStyle = ComboBoxStyle.DropDownList;
      this.Effect3.FormattingEnabled = true;
      this.Effect3.Location = new Point(70, 262);
      this.Effect3.Name = "Effect3";
      this.Effect3.Size = new Size(263, 21);
      this.Effect3.TabIndex = 25;
      this.label16.AutoSize = true;
      this.label16.Location = new Point(17, 265);
      this.label16.Name = "label16";
      this.label16.Size = new Size(47, 13);
      this.label16.TabIndex = 24;
      this.label16.Text = "Effect 3:";
      this.Effect4String.AutoSize = true;
      this.Effect4String.Location = new Point(54, 387);
      this.Effect4String.Name = "Effect4String";
      this.Effect4String.Size = new Size(57, 13);
      this.Effect4String.TabIndex = 43;
      this.Effect4String.Text = "StringHere";
      this.label18.AutoSize = true;
      this.label18.Location = new Point(2, 387);
      this.label18.Name = "label18";
      this.label18.Size = new Size(46, 13);
      this.label18.TabIndex = 42;
      this.label18.Text = "ToolTip:";
      this.label19.AutoSize = true;
      this.label19.Location = new Point(236, 367);
      this.label19.Name = "label19";
      this.label19.Size = new Size(35, 13);
      this.label19.TabIndex = 41;
      this.label19.Text = "Var 3:";
      this.Effect4Var3.Location = new Point(277, 364);
      this.Effect4Var3.Name = "Effect4Var3";
      this.Effect4Var3.Size = new Size(56, 20);
      this.Effect4Var3.TabIndex = 40;
      this.label20.AutoSize = true;
      this.label20.Location = new Point(132, 367);
      this.label20.Name = "label20";
      this.label20.Size = new Size(35, 13);
      this.label20.TabIndex = 39;
      this.label20.Text = "Var 2:";
      this.Effect4Var2.Location = new Point(173, 364);
      this.Effect4Var2.Name = "Effect4Var2";
      this.Effect4Var2.Size = new Size(56, 20);
      this.Effect4Var2.TabIndex = 38;
      this.label21.AutoSize = true;
      this.label21.Location = new Point(29, 367);
      this.label21.Name = "label21";
      this.label21.Size = new Size(35, 13);
      this.label21.TabIndex = 37;
      this.label21.Text = "Var 1:";
      this.Effect4Var1.Location = new Point(70, 364);
      this.Effect4Var1.Name = "Effect4Var1";
      this.Effect4Var1.Size = new Size(56, 20);
      this.Effect4Var1.TabIndex = 36;
      this.Effect4.DropDownStyle = ComboBoxStyle.DropDownList;
      this.Effect4.FormattingEnabled = true;
      this.Effect4.Location = new Point(70, 337);
      this.Effect4.Name = "Effect4";
      this.Effect4.Size = new Size(263, 21);
      this.Effect4.TabIndex = 35;
      this.label22.AutoSize = true;
      this.label22.Location = new Point(17, 340);
      this.label22.Name = "label22";
      this.label22.Size = new Size(47, 13);
      this.label22.TabIndex = 34;
      this.label22.Text = "Effect 4:";
      this.Effect5String.AutoSize = true;
      this.Effect5String.Location = new Point(54, 463);
      this.Effect5String.Name = "Effect5String";
      this.Effect5String.Size = new Size(57, 13);
      this.Effect5String.TabIndex = 53;
      this.Effect5String.Text = "StringHere";
      this.label24.AutoSize = true;
      this.label24.Location = new Point(2, 463);
      this.label24.Name = "label24";
      this.label24.Size = new Size(46, 13);
      this.label24.TabIndex = 52;
      this.label24.Text = "ToolTip:";
      this.label25.AutoSize = true;
      this.label25.Location = new Point(236, 442);
      this.label25.Name = "label25";
      this.label25.Size = new Size(35, 13);
      this.label25.TabIndex = 51;
      this.label25.Text = "Var 3:";
      this.Effect5Var3.Location = new Point(277, 439);
      this.Effect5Var3.Name = "Effect5Var3";
      this.Effect5Var3.Size = new Size(56, 20);
      this.Effect5Var3.TabIndex = 50;
      this.label26.AutoSize = true;
      this.label26.Location = new Point(132, 442);
      this.label26.Name = "label26";
      this.label26.Size = new Size(35, 13);
      this.label26.TabIndex = 49;
      this.label26.Text = "Var 2:";
      this.Effect5Var2.Location = new Point(173, 439);
      this.Effect5Var2.Name = "Effect5Var2";
      this.Effect5Var2.Size = new Size(56, 20);
      this.Effect5Var2.TabIndex = 48;
      this.label27.AutoSize = true;
      this.label27.Location = new Point(29, 442);
      this.label27.Name = "label27";
      this.label27.Size = new Size(35, 13);
      this.label27.TabIndex = 47;
      this.label27.Text = "Var 1:";
      this.Effect5Var1.Location = new Point(70, 439);
      this.Effect5Var1.Name = "Effect5Var1";
      this.Effect5Var1.Size = new Size(56, 20);
      this.Effect5Var1.TabIndex = 46;
      this.Effect5.DropDownStyle = ComboBoxStyle.DropDownList;
      this.Effect5.FormattingEnabled = true;
      this.Effect5.Location = new Point(70, 412);
      this.Effect5.Name = "Effect5";
      this.Effect5.Size = new Size(263, 21);
      this.Effect5.TabIndex = 45;
      this.label28.AutoSize = true;
      this.label28.Location = new Point(17, 415);
      this.label28.Name = "label28";
      this.label28.Size = new Size(47, 13);
      this.label28.TabIndex = 44;
      this.label28.Text = "Effect 5:";
      this.label3.AutoSize = true;
      this.label3.Location = new Point(25, 31);
      this.label3.Name = "label3";
      this.label3.Size = new Size(42, 13);
      this.label3.TabIndex = 54;
      this.label3.Text = "Range:";
      this.groupBox2.Controls.Add((Control) this.EnergyUse);
      this.groupBox2.Controls.Add((Control) this.label17);
      this.groupBox2.Controls.Add((Control) this.CoolDown);
      this.groupBox2.Controls.Add((Control) this.label11);
      this.groupBox2.Controls.Add((Control) this.Range);
      this.groupBox2.Controls.Add((Control) this.label3);
      this.groupBox2.Location = new Point(32, 485);
      this.groupBox2.Name = "groupBox2";
      this.groupBox2.Size = new Size(364, 90);
      this.groupBox2.TabIndex = 55;
      this.groupBox2.TabStop = false;
      this.groupBox2.Text = "Activatable Group";
      this.EnergyUse.Location = new Point(252, 28);
      this.EnergyUse.Name = "EnergyUse";
      this.EnergyUse.Size = new Size(102, 20);
      this.EnergyUse.TabIndex = 59;
      this.label17.AutoSize = true;
      this.label17.Location = new Point(181, 31);
      this.label17.Name = "label17";
      this.label17.Size = new Size(65, 13);
      this.label17.TabIndex = 58;
      this.label17.Text = "Energy Use:";
      this.CoolDown.Location = new Point(73, 54);
      this.CoolDown.Name = "CoolDown";
      this.CoolDown.Size = new Size(102, 20);
      this.CoolDown.TabIndex = 57;
      this.label11.AutoSize = true;
      this.label11.Location = new Point(8, 57);
      this.label11.Name = "label11";
      this.label11.Size = new Size(59, 13);
      this.label11.TabIndex = 56;
      this.label11.Text = "CoolDown:";
      this.Range.Location = new Point(73, 28);
      this.Range.Name = "Range";
      this.Range.Size = new Size(102, 20);
      this.Range.TabIndex = 55;
      this.Save.Location = new Point(446, 536);
      this.Save.Name = "Save";
      this.Save.Size = new Size(85, 36);
      this.Save.TabIndex = 56;
      this.Save.Text = "Save/Update";
      this.Save.UseVisualStyleBackColor = true;
      this.Save.Click += new EventHandler(this.Save_Click);
      this.AutoScaleDimensions = new SizeF(6f, 13f);
      this.AutoScaleMode = AutoScaleMode.Font;
      this.ClientSize = new Size(550, 587);
      this.Controls.Add((Control) this.Save);
      this.Controls.Add((Control) this.groupBox2);
      this.Controls.Add((Control) this.Effect5String);
      this.Controls.Add((Control) this.label24);
      this.Controls.Add((Control) this.label25);
      this.Controls.Add((Control) this.Effect5Var3);
      this.Controls.Add((Control) this.label26);
      this.Controls.Add((Control) this.Effect5Var2);
      this.Controls.Add((Control) this.label27);
      this.Controls.Add((Control) this.Effect5Var1);
      this.Controls.Add((Control) this.Effect5);
      this.Controls.Add((Control) this.label28);
      this.Controls.Add((Control) this.Effect4String);
      this.Controls.Add((Control) this.label18);
      this.Controls.Add((Control) this.label19);
      this.Controls.Add((Control) this.Effect4Var3);
      this.Controls.Add((Control) this.label20);
      this.Controls.Add((Control) this.Effect4Var2);
      this.Controls.Add((Control) this.label21);
      this.Controls.Add((Control) this.Effect4Var1);
      this.Controls.Add((Control) this.Effect4);
      this.Controls.Add((Control) this.label22);
      this.Controls.Add((Control) this.Effect3String);
      this.Controls.Add((Control) this.label12);
      this.Controls.Add((Control) this.label13);
      this.Controls.Add((Control) this.Effect3Var3);
      this.Controls.Add((Control) this.label14);
      this.Controls.Add((Control) this.Effect3Var2);
      this.Controls.Add((Control) this.label15);
      this.Controls.Add((Control) this.Effect3Var1);
      this.Controls.Add((Control) this.Effect3);
      this.Controls.Add((Control) this.label16);
      this.Controls.Add((Control) this.Effect2String);
      this.Controls.Add((Control) this.label4);
      this.Controls.Add((Control) this.label5);
      this.Controls.Add((Control) this.Effect2Var3);
      this.Controls.Add((Control) this.label8);
      this.Controls.Add((Control) this.Effect2Var2);
      this.Controls.Add((Control) this.label9);
      this.Controls.Add((Control) this.Effect2Var1);
      this.Controls.Add((Control) this.Effect2);
      this.Controls.Add((Control) this.label10);
      this.Controls.Add((Control) this.Effect1String);
      this.Controls.Add((Control) this.label6);
      this.Controls.Add((Control) this.Var3Lable);
      this.Controls.Add((Control) this.Effect1Var3);
      this.Controls.Add((Control) this.Var2Lable);
      this.Controls.Add((Control) this.Effect1Var2);
      this.Controls.Add((Control) this.Var1Lable);
      this.Controls.Add((Control) this.Effect1Var1);
      this.Controls.Add((Control) this.Effect1);
      this.Controls.Add((Control) this.label2);
      this.Controls.Add((Control) this.groupBox1);
      this.Name = nameof (EditItem);
      this.Text = nameof (EditItem);
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.groupBox2.ResumeLayout(false);
      this.groupBox2.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();
    }

    private struct EffectList
    {
      public string EffectDescList;
      public string EffectToolList;
      public int EffectID;
    }
  }
}
